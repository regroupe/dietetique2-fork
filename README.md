# Regroupécole
[![N|Solid](https://images.ecosia.org/mDKdDtej3wSpxL69e40fYnYYbYU=/0x390/smart/https%3A%2F%2F1.bp.blogspot.com%2F-Q9DGOPTr1RE%2FV7FCdRfbDPI%2FAAAAAAAAAG8%2FxXg0M_pIVLkXiW1exrX9H3fWcXR2RGIogCLcB%2Fs400%2FLaravel-getting-started.png)](https://laravel.com/)

### Description

Regroupécole un site de regroupement d'achat créé dans le cadre du cours de Conception Web au Cégep de Trois-Rivière pour le département de diététique.
Le site comporte plusieurs fonctionnalités tel que:
- Implémentation de paiements grâce à PayPal API.
- Création et modification de tableau de bord côté administrateur.
- Données et aperçu analytique des ventes.
- Système d'approbation de produit dans un environnement d'apprentissage pour les étudiants du département.
- Système de prise de commande et d'historique de traçabilité avec signature des clients.
- Système de thématisation du site avec portabilité en tête.

### Installation

Le site regroupécole requiert la plateforme de dévelopement PHP Laravel et Composer.
Voir le guide d'installation qui se trouve dans:
```
\Guide d'installation\Installation-de-l'application-web-en-local.docx
```

Création du lien symbolique pour le bon fonctionnement des images publiques sur le site en local.
```sh
php artisan storage:link
```
- Ne pas oublier de supprimer votre dossier storage qui se trouve dans le répertoire "public" de votre site en local.

### Plugins

Regroupécole utilise les packages suivant

| Plugin | Lien |
| ------ | ------ |
| Bootstrap | https://getbootstrap.com/ |
| Sortable.js | https://github.com/SortableJS/Sortable |
| SignaturePad | https://github.com/szimek/signature_pad |
| Chart.js | https://github.com/fxcosta/laravel-chartjs |
| Breadcrumbs | https://github.com/davejamesmiller/laravel-breadcrumbs |
| Laravel Lang | https://github.com/caouecs/Laravel-lang |
| PayPal API | https://github.com/thephpleague/omnipay-paypal |
| Reliese | https://github.com/reliese/laravel |
| HTML Purifier | https://github.com/mewebstudio/Purifier |
| WYSIWYG | https://summernote.org/ |

### Auteurs
- Zachary Veillette
- Jessy Walker-Mailly
- Charles-William Morency