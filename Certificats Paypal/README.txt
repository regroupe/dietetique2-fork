1- Download this file: http://curl.haxx.se/ca/cacert.pem

2- Place this file in the C:\wamp64\bin\php\php7.2.10 folder

3- Open php.iniand find this line:
------------------------------------------------------

   ;curl.cainfo

------------------------------------------------------

4- Change it to:
------------------------------------------------------

curl.cainfo = "C:\wamp64\bin\php\php7.2.10\cacert.pem"

------------------------------------------------------

5- Make sure you remove the semicolon at the beginning of the line.
   Save changes to php.ini, restart WampServer, and you're good to go!

/*
* Credits to kjdion84 : https://stackoverflow.com/questions/42094842/curl-error-60-ssl-certificate-in-laravel-5-4
*/