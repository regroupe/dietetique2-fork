$('#add_cueillette').on('shown.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var title = button.data('title');
    var button = button.data('type');
    var modal = $(this);
    modal.find('.modal-title').text(title);
    modal.find('.modal-button').text(button);
})

$(".modal").on("hidden.bs.modal", function(){
$("[name=emplacement]").val("");
$("[name=date_debut_c]").val("");
$("[name=date_fin_c]").val("");
$("[name=time_debut_c]").val("");
$("[name=time_fin_c]").val("");
$(".id-cueil").val("");

$(".err-emp").text("");
$(".err-date_debut").text("");
$(".err-time_debut").text("");
$(".err-date_fin").text("");
$(".err-time_fin").text("");
});

function ajouter(sender){
    var emp = $('#emplacement').val();
    var date_debut = $('#date_debut_c').val();
    var time_debut = $('#time_debut_c').val();;
    var date_fin = $('#date_fin_c').val();
    var time_fin = $('#time_fin_c').val();
    var leId = $(".id-cueil").val();

    if (validate(emp, date_debut, time_debut, date_fin, time_fin)){     
        if (leId == "")
        {
                emplacement.push(emp);
                debut.push(date_debut + " " + time_debut);
                fin.push(date_fin  + " " + time_fin);
        }
        else{
                emplacement[parseInt(leId)] = emp;
                debut[parseInt(leId)] = date_debut + " " + time_debut;
                fin[parseInt(leId)] = date_fin  + " " + time_fin;
        }
        $(".modal").modal('hide');
        showList();  
    }

}

function validate(emp, date_debut, time_debut, date_fin, time_fin){
    var valide = true;
    
    //Emplacement
    if (emp == "")
    {
         $('.err-emp').text('Saisissez un emplacement.');
         valide =  false;
    }
    else{
         $('.err-emp').text("");
    }

    //Date début
    if (!/^\d{4}(-)\d{2}(-)\d{2}$/.test(date_debut))
    {
        $('.err-date_debut').text('Date invalide.');
        valide =  false;
    }
    else{
        date = new Date(date_debut);
        date2 = new Date(date_now);
        if(date > date2)
        { 
            $('.err-date_debut').text("");
        }
        else{
            $('.err-date_debut').text('La date doit être après la date d\'aujourd\'hui.');
            valide =  false;
        }
    }

    //Time Début
    if (!/^\d{2}(:)\d{2}$/.test(time_debut))
    {
        $('.err-time_debut').text('Heure invalide.');
        valide =  false;
    }
    else{
         $('.err-time_debut').text("");
    }

    //Date fin
    if (!/^\d{4}(-)\d{2}(-)\d{2}$/.test(date_fin))
    {
        $('.err-date_fin').text('Date invalide.');
        valide =  false;
    }
    else{
        date = new Date(date_fin);
        date2 = new Date(date_debut);
        if(date >= date2)
        { 
            $('.err-date_fin').text("");
        }
        else{
            $('.err-date_fin').text('La date doit être après la date de début.');
            valide =  false;
        }
    }

    //Time fin
    if (!/^\d{2}(:)\d{2}$/.test(time_fin))
    {
        $('.err-time_fin').text('Heure invalide.');
        valide =  false;
    }
    else{
         $('.err-time_fin').text("");
    }


    if (valide)
    {
        return true;
    }
    else return false;
}

function modifier(sender)
{
    var root = sender.parentElement.parentElement;
    var $root = $(root);

    var exploded = $root.find('.deb').val().split(' ');

    $("[name=emplacement]").val($root.find('.emp').val());
    $("[name=date_debut_c]").val(exploded[0]);
    $("[name=time_debut_c]").val(exploded[1]);

    var exploded = $root.find('.fin').val().split(' ');
    $("[name=date_fin_c]").val(exploded[0]);
    $("[name=time_fin_c]").val(exploded[1]);

    $(".id-cueil").val($root.attr('id').substr(1));

    $(".modal").find('.modal-title').text('Modifier une période de cueillette');
    $(".modal").find('.modal-button').text('Modifier');
    $(".modal").modal('show');
}

function supprimer(sender)
{
    if (confirm('Voulez-vous vraiment supprimer cette période?'))
    {
            var toDelete = sender.parentElement.parentElement;
            var $toDelete = $(toDelete);
            var id = parseInt($toDelete.attr('id').substr(1));

            emplacement.splice(id,1);
            debut.splice(id,1);
            fin.splice(id,1);
            
            showList();
    }
}

function showList(){
    if (emplacement.length >=1)
    {
            $('#cueillette_list').empty();

            for (var i = 0; i < emplacement.length; i++)
            {
                    if (emplacement[i] == null) emplacement[i] ="";
                    if (debut[i] == null) debut[i] ="";
                    if (fin[i] == null) fin[i] ="";


                    $('#cueillette_list').append("<div class='form-row' id='c"+i+"'></div>");
                    $('#c'+i).append("<div class='form-group col-sm-12 col-md-12 col-lg-12'><h4>Cueillette #"+(i+1)+"</h4></div>");

                    $('#c'+i).append("<div class='form-group col-sm-12 col-md-4 col-lg-4'><input readonly class='form-control emp' name=c_emp[] value='"+emplacement[i]+"'></div>");
                    $('#c'+i).append("<div class='form-group col-sm-12 col-md-3 col-lg-3'><input readonly class='form-control deb' name=c_debut[] value='"+debut[i]+"'></div>");
                    $('#c'+i).append("<div class='form-group col-sm-12 col-md-3 col-lg-3'><input readonly class='form-control fin' name=c_fin[] value='"+fin[i]+"'></div>");
                    $('#c'+i).append("<div class='form-group col-sm-12 col-md-2 col-lg-2'>"+
                    "<button style='margin-right:2px;' type='button' class='btn btn-outline-primary' data-toggle='tooltip' data-placement='top' title='Modifier' onclick='modifier(this)'>"+
                    "<i class='fa fa-pencil' aria-hidden='true'></i></button>"+
                    "<button type='button' class='btn btn-outline-danger' data-toggle='tooltip' data-placement='top' title='Supprimer' onclick='supprimer(this)'>"+
                    "<i class='fa fa-ban' aria-hidden='true'></i></button>"+
                    "</div>");
            }
    }
    else{
        $('#cueillette_list').empty();
        $('#cueillette_list').append("Aucune période de cueillette");
    }
}

$( document ).ready(function() {
showList();

$(function () {
    $('body').on('click', '.submit-delete', function (e) {
        $(this.form).submit();
        $('.modal-del').modal('hide');
    });
});
});