function edit(sender){
        var body = $('.account-row');
        var data = [];
        $sender = $(sender);
         
        for (var i=0; i < body.length; i++)
        {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
            var marque = prompt("Modifier le nom de la marque",data[0]);
            if (marque != null&&marque !="")
            {

                 $('#activer').addClass('disabled');
                 $('#desactiver').addClass('disabled');
                 $('#edit').addClass('disabled');
         
            $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                type: "post",
                url: routes[0],
                data: {
                    "nom_marque": data,
                    "new_name":marque

                },
                success: function(response){
                    location.reload();

                }
            })         
        }   
        return;
        }
    }
}

 function activer(sender){
    var body = $('.account-row');
    var data = [];
    $sender = $(sender);



    for (var i=0; i < body.length; i++)
    {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
        }
    }

    if (data.length > 0)
    {
        $('#activer').addClass('disabled');
        $('#desactiver').addClass('disabled');
        $('#edit').addClass('disabled');
        $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                type: "post",
                url: routes[1],
                data: {
                    'nom_marque': data,
                   
                },
                success: function(response) {
                    location.reload();
                }
            });
    }
}

function desactiver(sender){
    var body = $('.account-row');
    var data = [];
    $sender = $(sender);



    for (var i=0; i < body.length; i++)
    {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
        }
    }

    if (data.length > 0)
    {
        $('#activer').addClass('disabled');
        $('#desactiver').addClass('disabled');
        $('#edit').addClass('disabled');

        $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                type: "post",
                url: routes[2],
                data: {
                    "nom_marque": data
                },
                success: function(response) {
                    location.reload();
                }
            });
    }
    
}
