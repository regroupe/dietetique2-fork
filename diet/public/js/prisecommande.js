var orderPickup = {
    rowid: 0,
    status: false
};

var signaturePad;

// When zoomed out to less than 100%, for some very strange reason,
// some browsers report devicePixelRatio as less than 1
// and only part of the canvas is cleared then.
function resizeCanvas() {

    var canvas = $("canvas")[0];
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

function prepareCanvas(id, status) {

    orderPickup.rowid = id;
    orderPickup.status = status;

    var canvas = $("canvas")[0];
    signaturePad = new SignaturePad(canvas);
    
    // Resize the canvas once the modal pops up.
    setTimeout(function(){ resizeCanvas(); }, 500);
}

function pickupOrder() {

    var state = $('#search-list').serialize();
    
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: "POST",
        url: routes.order.pickup,
        data: {rowid: orderPickup.rowid, signature: signaturePad.toDataURL(), status: orderPickup.status, state: state},
        cache: false,
        success: function(partial) {

            // Rebuild table.
            $("table tbody").remove();
            $("table tfoot").remove();
            $(partial).appendTo($("table"));

            window.history.replaceState(
                null, 
                '', 
                routes.order.index + "?" + state
            );

            $('#signature-modal').modal('hide');
            signaturePad.clear();
        }
    });
}

function clearCanvas() {

    signaturePad.clear();
}

$(document).ready(function() {

    window.onresize = resizeCanvas;

    /**
     * Prevent the default form behaviour and let Ajax take care of loading the partial view
     * into the main view using the request parameters (search and filter options).
     */
    $('#search-list').on('submit', function(e) {

        var state = $(this).serialize();

        e.preventDefault(); 
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "GET",
            url: routes.order.index,
            data: state,
            cache: false,
            success: function(partial) {

                // Rebuild table.
                $("table tbody").remove();
                $("table tfoot").remove();
                $(partial).appendTo($("table"));

                window.history.replaceState(
                    null, 
                    '', 
                    routes.order.index + "?" + state
                );
            }
        });
    });

    /**
     * Submit form right upon changing a menu field.
     */
    $( ".search-input" ).change(function() {
        $("#search-list").submit();
    });

    /**
     * Special filter checkboxes used for intermediate option.
     */
    $(".sort-input").click(function() {

        var filterIcon = $(this).data('filter');

        switch($(this).val()) {

            case "1":
                $(filterIcon).removeClass();
                $(filterIcon).addClass('fa fa-' + $(filterIcon).data('intermediate-2'));
                $(this).prop('checked', true);
                $(this).val(2);
                break;

            case "2":
                $(filterIcon).removeClass();
                $(filterIcon).addClass('fa fa-' + $(filterIcon).data('intermediate-0'));
                $(this).prop('checked', false);
                $(this).val(0);
                break;

            default:
                $(filterIcon).removeClass();
                $(filterIcon).addClass('fa fa-' + $(filterIcon).data('intermediate-1'));
                $(this).prop('checked', true);
                $(this).val(1);
        }

        $("#search-list").submit();
    });
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});