function edit(sender){
    var body = $('.account-row');
    var data = [];
    $sender = $(sender);
     
    for (var i=0; i < body.length; i++)
    {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
            var fournisseur = prompt("Modifier le nom du fournisseur",data[0]);
            if (fournisseur != null&&fournisseur !="")
            {
                $('#activer').addClass('disabled');
                $('#desactiver').addClass('disabled');
                $('#edit').addClass('disabled');

                $.ajax({
                    headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                    type: "post",
                    url: routes[0],
                    data: {
                        "nom_unite": data,
                        "new_name":fournisseur

                    },
                    success: function(response){
                        location.reload();
                    }
                })         
            }   
            return;
            }
        }
}

function activer(sender){
var body = $('.account-row');
var data = [];
$sender = $(sender);


for (var i=0; i < body.length; i++)
{
   var row = body[i].getElementsByTagName('th')[0];
   if (row.children[0].children[0].checked){
       data.push(row.getAttribute('data-id'));
   }
}

if (data.length > 0)
{
    $('#activer').addClass('disabled');
    $('#desactiver').addClass('disabled');
    $('#edit').addClass('disabled');
   $.ajax({
           headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
           type: "post",
           url: routes[1],
           data: {
               'nom_unite': data,
              
           },
           success: function(response) {
               location.reload();
           }
       });
}
}


function desactiver(sender){
var body = $('.account-row');
var data = [];
$sender = $(sender);


for (var i=0; i < body.length; i++)
{
    var row = body[i].getElementsByTagName('th')[0];
    if (row.children[0].children[0].checked){
        data.push(row.getAttribute('data-id'));
    }
}

if (data.length > 0)
{
    $('#activer').addClass('disabled');
    $('#desactiver').addClass('disabled');
    $('#edit').addClass('disabled');

    $.ajax({
            headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
            type: "post",
            url: routes[2],
            data: {
                "nom_unite": data
            },
            success: function(response) {
                location.reload();
            }
        });
}

}

function ajoutFournisseur()
{
var fournisseur = $("#fournisseur")[0].value;
if(confirm("Souhaitez vous confirmer l'ajout de " + fournisseur + " à la liste des fournisseurs?"))
{

   $.ajax({
        headers:{'X-CSRF-TOKEN': $('meta[name ="csrf-token"]').attr('content')},
        type:'post',
        url: "{{action('UniteMesureController@RegisterUnite')}}",
        data: {
            'nom_unite': fournisseur
        },
        success: function(data){
            alert(fournisseur + " ajouté avec succès");
            location.reload();
        }

    });


}
}


function edit_f(id, sender)
{

var fourn_modif = sender.parentNode.getElementsByTagName('input')[0].value;
if(confirm("Désirez vous confirmer la modification ?" + id)){

    $.ajax({
        headers:{'X-CSRF-TOKEN': $('meta[name ="csrf-token"]').attr('content')},
        type:'post',
        url: "{{action('UniteMesureController@EditUnite')}}",
        data: {
            'nom_unite': fourn_modif,
            'old_name': id

        },
        success: function(data){
            alert("La modification à été réaliser avec succès");
           $.get('fournisseur/listeFournisseur',function(data){
               $('#testliste').empty();
               $('#testliste').append(data['html']);
           });

        }

    });



}
}
