$(document).ready(function() {

    /**
     * Prevent the default form behaviour and let Ajax take care of loading the partial view
     * into the main view using the request parameters (search and filter options).
     */
    $('#search-list').on('submit', function(e) {

        var state = $(this).serialize();

        e.preventDefault(); 
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "GET",
            url: routes.article.index,
            data: state,
            cache: false,
            success: function(partial) {

                // Rebuild table.
                $("table tbody").remove();
                $("table tfoot").remove();
                $(partial).appendTo($("table"));

                window.history.replaceState(
                    null, 
                    '', 
                    routes.article.index + "?" + state
                );
            }
        });
    });

    /**
     * Submit form right upon changing a menu field.
     */
    $( ".search-input" ).change(function() {
        $("#search-list").submit();
    });

    /**
     * Special filter checkboxes used for intermediate option.
     */
    $(".sort-input").click(function() {

        var filterIcon = $(this).data('filter');

        switch($(this).val()) {

            case "1":
                $(filterIcon).removeClass();
                $(filterIcon).addClass('fa fa-' + $(filterIcon).data('intermediate-2'));
                $(this).prop('checked', true);
                $(this).val(2);
                break;

            case "2":
                $(filterIcon).removeClass();
                $(filterIcon).addClass('fa fa-' + $(filterIcon).data('intermediate-0'));
                $(this).prop('checked', false);
                $(this).val(0);
                break;

            default:
                $(filterIcon).removeClass();
                $(filterIcon).addClass('fa fa-' + $(filterIcon).data('intermediate-1'));
                $(this).prop('checked', true);
                $(this).val(1);
        }

        $("#search-list").submit();
    });
});

/**
 * Redirect to the create article page.
 */
function addArticle() {
    window.location.href = routes.article.create;
}

function activateArticle(rowid) {

    if (confirm("Êtes-vous certain de vouloir activer cet article?")) {
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: 'post',
            url: routes.article.activate,
            data: {
                'rowid': rowid
            },
            success: function(data) {
                $("#search-list").submit();
            }
        });
    }
}

function deactivateArticle(rowid) {

    if (confirm("Êtes-vous certain de vouloir désactiver cet article?")) {
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: 'post',
            url: routes.article.deactivate,
            data: {
                'rowid': rowid
            },
            success: function(data) {
                $("#search-list").submit();
            }
        });
    }
}