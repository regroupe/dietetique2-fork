/**
 * Enable drag and drop.
 */
var sortable = Sortable.create(sortablelist, {
    ghostClass:'card-holder'
});



 var ready = true;


 /**
  * Tooltip.
  */
function switchSortable(mobile){
    var state = sortable.option("disabled");
	sortable.option("disabled", !state);

    if (!state){
        $('.switcher').children().remove();
        if (!mobile){
            $('.switcher').append('<i class="fa fa-ban"></i><span> Déplacement desactivé</span>');
        }else $('.switcher').append('<i class="fa fa-ban"></i>');
        
        $('.switcher').removeClass('btn-success');
        $('.switcher').addClass('btn-secondary');
    }else{
        $('.switcher').children().remove();
        if (!mobile){
            $('.switcher').append('<i class="fa fa-arrows-alt"></i><span> Déplacement activé</span>');
        }else $('.switcher').append('<i class="fa fa-arrows-alt"></i>');
        $('.switcher').removeClass('btn-secondary');
        $('.switcher').addClass('btn-success');
    }
    
}

$(window).scroll(function(event) {
	function footer()
    {
        var scroll = $(document).height() - $(window).height() - $(window).scrollTop();
        if(scroll < 275)
        { 
             $(".fixed-drag").removeClass("d-block");
             $(".fixed-drag").addClass("d-none");
        }
        else
        {
            $(".fixed-drag").removeClass("d-none");
            $(".fixed-drag").addClass("d-block");
        }
        
    }
    footer();
});




 /**
  * Tooltip.
  */
$(function () { 
    $('[data-toggle="tooltip"]').tooltip(); 
});




/**
 * Add a row with only one space.
 */
function AddRow1(){
    if (ready){
        id =  guidGenerator();
        ready = false;
        $('#addrow').addClass('disabled');
        $.ajax({
            headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
            type: "post",
            url: route,
            data: {id},
            success: function(response){
                $('.row-list').append(response);
                ready = true;
                $('#addrow').removeClass('disabled');
            }
        });      
    }
}

/**
 * Change bootstrap classes on size change. 
**/
function ChangeSize(sender, index){
    $('.no-'+index).removeClass('col-md-6');
    $('.no-'+index).removeClass('col-md-12');
    $('.no-'+index).removeClass('col-lg-4');
    $('.no-'+index).removeClass('col-lg-6');
    $('.no-'+index).removeClass('col-lg-12');

    switch (sender.value){
        case 'small':{
            $('.no-'+index).addClass('col-md-6');
            $('.no-'+index).addClass('col-lg-4');
            return;
        } 
        case 'medium':{
            $('.no-'+index).addClass('col-md-12');
            $('.no-'+index).addClass('col-lg-6');
            return;
        } 
        case 'large':{
            $('.no-'+index).addClass('col-md-12');
            $('.no-'+index).addClass('col-lg-12');
            return;
        } 
        default:{
            return;
        }
    }
}

/**
 * Change bootstrap classes on color change. 
**/
function ChangeColor(sender, index){
    $('.card-'+index).removeClass('border-primary');
    $('.card-'+index).removeClass('border-secondary');
    $('.card-'+index).removeClass('border-success');
    $('.card-'+index).removeClass('border-danger');
    $('.card-'+index).removeClass('border-warning');
    $('.card-'+index).removeClass('border-info');
    $('.card-'+index).removeClass('border-dark');
    $('.card-'+index).removeClass('border-muted');

    switch (sender.value){
        case 'primary':{
            $('.card-'+index).addClass('border-primary');
            return;
        } 
        case 'secondary':{
            $('.card-'+index).addClass('border-secondary');
            return;
        } 
        case 'success':{
            $('.card-'+index).addClass('border-success');
            return;
        } 
        case 'danger':{
            $('.card-'+index).addClass('border-danger');
            return;
        } 
        case 'warning':{
            $('.card-'+index).addClass('border-warning');
            return;
        } 
        case 'info':{
            $('.card-'+index).addClass('border-info');
            return;
        } 
        case 'dark':{
            $('.card-'+index).addClass('border-dark');
            return;
        } 
        case 'muted':{
            $('.card-'+index).addClass('border-muted');
            return;
        } 
        default:{
            return;
        }
    }
}


/**
 * Remove a board row.
 */
function DeleteRow(index){
    if (confirm("Voulez-vous supprimer cette vue?"))
    {
        $('.no-'+index).remove();
    }
}