//Active les comptes sélectionnés
function activer(sender){
    var body = $('.account-row');
    var data = [];
    var tpe = "";
    $sender = $(sender);

    if ($sender.data('tpe') == 'activ-client'){
        tpe = 'client';
    }
    else if ($sender.data('tpe') == 'activ-admin'){
        tpe = 'admin';
    }
    else return;

    for (var i=0; i < body.length; i++)
    {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
        }
    }

    if (data.length > 0)
    {
        $('#activer').addClass('disabled');
        $('#desactiver').addClass('disabled');
        $('#reset').addClass('disabled');
        $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                type: "post",
                url: routes[0],
                data: {
                    "rowids": data,
                    "tpe": tpe
                },
                success: function(response) {
                    location.reload();
                }
            });
        
    }
}

//Désactive les comptes sélectionnés
function desactiver(sender){
    var body = $('.account-row');
    var data = [];
    var tpe = "";
    $sender = $(sender);

    if ($sender.data('tpe') == 'desact-client'){
        tpe = 'client';
    }
    else if ($sender.data('tpe') == 'desact-admin'){
        tpe = 'admin';
    }
    else return;

    for (var i=0; i < body.length; i++)
    {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
        }
    }

    if (data.length > 0)
    {
            $('#activer').addClass('disabled');
            $('#desactiver').addClass('disabled');
            $('#reset').addClass('disabled');

        $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                type: "post",
                url: routes[1],
                data: {
                    "rowids": data,
                    "tpe": tpe
                },
                success: function(response) {
                    location.reload();
                }
            });

    }
    
}

function reset(sender){
    var body = $('.account-row');
    var data = [];
    var tpe = "";
    $sender = $(sender);

    for (var i=0; i < body.length; i++)
    {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
        }
    }

    if (data.length > 0)
    {
            $('#activer').addClass('disabled');
            $('#desactiver').addClass('disabled');
            $('#reset').addClass('disabled');

        $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                type: "post",
                url: routes[2],
                data: {
                    "rowids": data
                },
                success: function(response) {
                    if (response['nb'] != 0)
                    {
                        var toShow = "Compte;Mot de passe\n";
                        for (var i = 0; i < response['account'].length; i++)
                        {
                            toShow += response['account'][i] + ";" + response['pass'][i] + "\n";
                        }
                            var element = document.createElement('a');
                            element.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(toShow));
                            element.setAttribute('download', "Mots de passes.csv");

                            element.style.display = 'none';
                            document.body.appendChild(element);

                            element.click();

                            document.body.removeChild(element);

                    }

                    $('#activer').removeClass('disabled');
                    $('#desactiver').removeClass('disabled');
                    $('#reset').removeClass('disabled');
                    
                }
            });

    }
    
}