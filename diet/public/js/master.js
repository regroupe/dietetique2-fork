
function parsePhoneNumber(phone) {

    phone = phone.replace(/\D/g,'');

    // Regular phone.
    if (phone.length == 10) {
        return phone.substr(0,3) + '-' + phone.substr(3,3) + '-' + phone.substr(6);
    }

    // International phone.
    if (phone.length == 11) {
        return '+' + phone.substr(0,1) + ' ' + phone.substr(1,3) + '-' + phone.substr(4,3) + '-' + phone.substr(7);
    }

    return phone;
}

$(document).ready(function() {
    $("#phone").text(parsePhoneNumber($("#phone")[0].innerHTML));
});


function guidGenerator() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}
