function offer(sender){
    var body = $('.account-row');
    var data = [];
    $sender = $(sender);

    for (var i=0; i < body.length; i++)
    {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
        }
    }

    if (data.length > 0)
    {
        $('#offer').attr("disabled", "disabled");
        $('#dont').attr("disabled", "disabled");
        $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                type: "post",
                url: routes[0],
                data: {
                    "rowids": data,
                },
                success: function(response) {
                    location.reload();
                }
            });
        $('#offer').attr("disabled", "disabled");
        $('#dont').attr("disabled", "disabled");
    }
}

function dont(sender){
    var body = $('.account-row');
    var data = [];
    $sender = $(sender);

    for (var i=0; i < body.length; i++)
    {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
        }
    }

    if (data.length > 0)
    {
        $('#offer').attr("disabled", "disabled");
        $('#dont').attr("disabled", "disabled");
        $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                type: "post",
                url: routes[1],
                data: {
                    "rowids": data,
                },
                success: function(response) {
                    location.reload();
                }
            });
        $('#offer').attr("disabled", "disabled");
        $('#dont').attr("disabled", "disabled");
    }
}


