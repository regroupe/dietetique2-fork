$(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

function printElem(periode) {
    var fournisseur = $('.fournisseur-head');
    var mywindow = window.open('', 'print' , 'height=800,width=800');

    mywindow.document.write('<html><head><title>Articles à commander - période #'+periode+'</title>');
    mywindow.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"></head>');
    mywindow.document.write('<head>');
    mywindow.document.write('<div style="padding-bottom:20px";></div>');

    mywindow.document.write('<div class="card">');
    for (var i = 0; i<fournisseur.length; i++)
    {

        mywindow.document.write('<h2  class="card-header" style="color:black">' +fournisseur[i].innerHTML + '</h2>');
        mywindow.document.write('<div class="container">');

        var articles_title = $('.at-fourn'+i);
        var articles_quant = $('.aq-fourn'+i);

        for (var j = 0; j<articles_title.length; j++)
        {
            mywindow.document.write('<div style="padding-bottom:5px"><div><h5 style="padding:0px">' +articles_title[j].innerHTML + '</div></h5>');
            mywindow.document.write('<div>' +articles_quant[j].innerHTML + '</div></div>');
        }
        mywindow.document.write('</div>');
        
    }
    mywindow.document.write('</div>');


    mywindow.document.write('<script>window.print(); window.close();</script>');
    mywindow.document.write('</body></html>');

    mywindow.document.close();
    mywindow.focus()
    return true;
}

function printSynthese(){
    $('.fournisseur-head').addClass('text-dark');
    $('.btnprint').addClass('d-none');
    $('#print_progress').printArea({ mode: 'popup', popClose: true });
    $('.fournisseur-head').removeClass('text-dark');
    $('.btnprint').removeClass('d-none');
}