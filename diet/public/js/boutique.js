
$(function () { $('[data-toggle="tooltip"]').tooltip(); })

/**
 * Update the quantity of an article and update the basket summary.
 */
function addToBasket(sender) {

    var addBasketElement = $(sender);
    var articleElement = addBasketElement.closest(".shop-product");
    var quantityElement = articleElement.find("input[name='product-quantity']");

    // Retrieve the article data from the elements.
    var productRowid = articleElement.data("id");
    var quantity = quantityElement.val();

    $.ajax({
        headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        type: "post",
        url: routes.panier.update,
        data: {
            "rowid": productRowid,
            "quantity": quantity
        },
        success: function(response) {
            
            if (response.quantitySuccess === undefined) {
                $("#addedtobasket-title").text("Veuillez-vous connecter.");
                return;
            }

            if (response.quantitySuccess > 0) $("#addedtobasket-title").text("Article ajouté au panier!");
            else $("#addedtobasket-title").text("Désolé, quantité insuffisante!");

            quantityElement.val(response.quantitySuccess);
        }
    });
}

function changeCategory(sender, all = false) {

    sender.form.filter_category.value = (all ? null : sender.value);
    $("h1").text(sender.value);
}

$('#search-list, #filter-list').on('submit', function(e) {

    var state = $("#search-list, #filter-list").serialize();

    e.preventDefault(); 
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: "GET",
        url: routes.boutique.index,
        data: state,
        cache: false,
        success: function(partial) {

            $("#results")[0].innerHTML = partial;

            window.history.replaceState(
                null, 
                '', 
                routes.boutique.index + "?" + state
            );

            $("#product-count-number").text($("#boutique-articles-count").val());
        }
    });
});