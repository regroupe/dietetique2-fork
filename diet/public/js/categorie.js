function edit(sender){
        var body = $('.account-row');
        var data = [];
        $sender = $(sender);
         
        for (var i=0; i < body.length; i++)
        {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
            var categorie = prompt("Modifier le nom de la catégorie",data[0]);
            if (categorie != null&&categorie !="")
            {
                $('#activer').addClass('disabled');
                 $('#desactiver').addClass('disabled');
                 $('#edit').addClass('disabled');
                
            $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                type: "post",
                url: routes[0],
                data: {
                    "nom_categorie": data,
                    "new_name":categorie

                },
                success: function(response){
                    location.reload();

                }
            })         
        }   
         return;
        }
        
    }
    }

     function activer(sender){
    var body = $('.account-row');
    var data = [];
    var tpe = "";
    $sender = $(sender);

    if ($sender.data('tpe') == 'activ-client'){
        tpe = 'client';
    }
    else if ($sender.data('tpe') == 'activ-admin'){
        tpe = 'admin';
    }
    else if ($sender.data('tpe') == 'activ-categorie'){
        tpe = 'categorie';
    }
    else return;

    for (var i=0; i < body.length; i++)
    {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
        }
    }

    if (data.length > 0)
    {
        $('#activer').addClass('disabled');
                 $('#desactiver').addClass('disabled');
                 $('#edit').addClass('disabled');
        $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                type: "post",
                url: routes[1],
                data: {
                    'nom_categorie': data,
                   
                },
                success: function(response) {
                    location.reload();
                }
            });
    }
}
function desactiver(sender){
    var body = $('.account-row');
    var data = [];
    var tpe = "";
    $sender = $(sender);

    if ($sender.data('tpe') == 'desact-client'){
        tpe = 'client';
    }
    else if ($sender.data('tpe') == 'desact-admin'){
        tpe = 'admin';
    }else if ($sender.data('tpe') == 'desact-categorie'){
        tpe = 'categorie';
    }
    else return;

    for (var i=0; i < body.length; i++)
    {
        var row = body[i].getElementsByTagName('th')[0];
        if (row.children[0].children[0].checked){
            data.push(row.getAttribute('data-id'));
        }
    }

    if (data.length > 0)
    {
            $('#activer').addClass('disabled');
                 $('#desactiver').addClass('disabled');
                 $('#edit').addClass('disabled');

        $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                type: "post",
                url: routes[2],
                data: {
                    "nom_categorie": data
                },
                success: function(response) {
                    location.reload();
                }
            });
    }
    
}


        function edit_c(id, sender)
        {

            var categorie_modif = sender.parentNode.getElementsByTagName('input')[0].value;
            if(confirm("Désirez vous confirmer la modification ?" + id)){

                $.ajax({
                    headers:{'X-CSRF-TOKEN': $('meta[name ="csrf-token"]').attr('content')},
                    type:'post',
                    url: "{{action('CategorieController@EditCategorie')}}",
                    data: {
                        'nom_categorie': categorie_modif,
                        'old_name': id

                    },
                    success: function(data){
                        alert("La modification à été réaliser avec succès");
                        $.get('categorie/listeCategorie',function(data){
                            $('#testliste').empty();
                            $('#testliste').append(data['html']);
                        });

                    }

                });



            }
        }