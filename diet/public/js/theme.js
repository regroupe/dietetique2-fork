$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$(document).ready(function() {

    /**
     * Parse the selected image to be displayed inside the preview area.
     */
    $("#icon").change(function() {
        
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#icon-preview').css("background-image", "url("+e.target.result+")");
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

    /**
     * Parse the selected image to be displayed inside the preview area.
     */
    $("#logo").change(function() {
        
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#logo-preview').css("background-image", "url("+e.target.result+")");
                $('#logo_footer').css("background-image", "url("+e.target.result+")");
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

    /**
     * Parse the selected image to be displayed inside the preview area.
     */
    $("#banniere").change(function() {
        
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#banniere-preview').css("background-image", "url("+e.target.result+")");
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

    $("#car1").change(function() {
        
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#car1-preview').css("background-image", "url("+e.target.result+")");
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

    $("#car2").change(function() {
        
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#car2-preview').css("background-image", "url("+e.target.result+")");
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

    $("#car3").change(function() {
        
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#car3-preview').css("background-image", "url("+e.target.result+")");
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

    /**
     * Show the file name in the custom file selection box.
     */
    $('.custom-file-input').on('change', function() {
        $(this).next('.custom-file-label').addClass("selected").html($(this).val().split('\\').pop()); 
    });

    $("#phone_input").val(parsePhoneNumber($("#phone_input")[0].value));

    
    previewFooter();
});

/**
 * Remove an image file from a file input when it is associated with a preview
 * area.
 */
function removeImageFile(element, preview, message) {

	$(preview).css("background-image", "url(/img/preview.png)");
	
	removeFile(element, message);
}

/**
 * Clear a file input field, reset its label message and mark it as
 * dirty in order to be properly handled by the controller.
 */
function removeFile(element, message) {

	// Special trick used to clear a file input once populated.
	$(element).wrap('<form>').closest('form').get(0).reset();
	$(element).unwrap();
	$(element+'_dirty').val(1);

	// Update the input label accordingly.
	$("label[for='"+element.replace("#", "")+"']").html(message);
}



function previewFooter(){
    $("#logo_footer").removeAttr('hidden');
    $("#preview_desc").text( $("#footer_desc").val());
    $("#preview_organ").text( $("#footer_entreprise").val());
    $("#preview_address").text( $("#footer_address").val());
    $("#preview_city").text( $("#footer_ville").val());
    $("#preview_cp").text( $("#footer_code_postal").val());
    $("#preview_phone").text( parsePhoneNumber($("#phone_input")[0].value));
    $("#preview_copy").text( $("#footer_copyright").val());
}