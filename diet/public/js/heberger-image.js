$(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
  
  $(document).ready(function() {

    /**
     * Show the file name in the custom file selection box.
     */
    $('.custom-file-input').on('change', function() {
        $(this).next('.custom-file-label').addClass("selected").html($(this).val().split('\\').pop()); 
    });
});  


/**
 * Clear a file input field, reset its label message and mark it as
 * dirty in order to be properly handled by the controller.
 */
function removeFile(element, message) {

	// Special trick used to clear a file input once populated.
	$(element).wrap('<form>').closest('form').get(0).reset();
	$(element).unwrap();
	$(element+'_dirty').val(1);

	// Update the input label accordingly.
	$("label[for='"+element.replace("#", "")+"']").html(message);
}

$('#heberge').submit(function(event) {
    event.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
        type: "post",
        url: route_heberge,
        data:formData,
        processData: false,
        contentType: false,
        success: function(response) {
            $('#lien').val(response);
        }
    });

});

function copyInputValue(element){
    var toCopy = $(element);

    toCopy.select();

    document.execCommand('copy');

}