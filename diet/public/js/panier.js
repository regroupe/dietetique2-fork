
$(function () { 
    
    window.scrollTo(0, 0);

    $('[data-toggle="tooltip"]').tooltip(); 

    /**
     * Callback used when changing a quantity. Control the display of the
     * quantity update controls based on if the quantity is new or not.
     */
    $(".product-quantity").change(function(sender) {

        var quantityElement = $(sender.target);
        var articleElement = quantityElement.closest(".basket-product");
        var quantityUpdateElement = articleElement.find(".product-quantity-update");

        // Retrieve the article data from the elements.
        var productRowid = articleElement.data("id");
        var oldQuantity = quantityElement.data("oldvalue");

        // Update the display status of the update quantity buttons based on the change event of the quantity.
        // If the quantity changed, show the buttons. If the quantity is the same, hide the controls.
        if (quantityElement.val() == '') quantityElement.val(1);
        if (quantityElement.val() != oldQuantity) quantityUpdateElement.show();
        else quantityUpdateElement.hide();
    });

    $('[data-toggle="affix"]').each(function() {

        var element = $(this);
        var wrapper = $('<div></div>');

        element.before(wrapper);
        $(window).on('scroll resize', function() {
            toggleAffix(element, wrapper, $(this));
        });

        // init
        toggleAffix(element, wrapper, $(window));
    });
});

function toggleAffix(affix, wrapper, scrollElement) {

    var footerOffset = $("footer").offset().top;
    var summaryHeight = $("#order-summary").outerHeight();
    var summaryOffset = $("#summary-offset").offset().top;
    var maxOffset = footerOffset - summaryHeight;

    $("#order-summary").css("margin-top", Math.min(maxOffset - summaryOffset - 30, 0));
    var top = wrapper.offset().top - 30;

    if ($(window).outerWidth() < 576) {
        $("#summary-offset").removeClass("affix");
        affix.removeClass("affix");
        wrapper.height('auto');
        affix.width(wrapper.width());
        return;
    }

    if (scrollElement.scrollTop() >= top){
        affix.addClass("affix");
        $("#summary-offset").addClass("affix");
    } else {
        $("#summary-offset").removeClass("affix");
        affix.removeClass("affix");
        wrapper.height('auto');
    }

    affix.width(wrapper.width());
}

/**
 * Update the quantity of an article and update the basket summary.
 */
function updateQuantity(sender) {

    // Retrieve every element asscociated with the quantity.
    var updateElement = $(sender);
    var articleElement = updateElement.closest(".basket-product");
    var quantityElement = articleElement.find(".product-quantity");

    $.ajax({
        headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        type: "post",
        url: routes.panier.update,
        data: {
            "rowid": articleElement.data("id"),
            "quantity": quantityElement.val(),
            "update": true
        },
        success: function(response) {

            if (response.quantitySuccess == 0) $(articleElement).remove();

            quantityElement.val(response.quantitySuccess);
            quantityElement.data("oldvalue", response.quantitySuccess);
            cancelQuantity(sender);
            updateSummary(response);

            var articlesCount = $(".basket-product").length;
            $("#basket-count").text((articlesCount > 0 ? articlesCount : "Aucun") + " article(s).");
        }
    });
}

/**
 * Remove an article from the basket and update the summary.
 */
function removeArticle(sender) {

    var articleElement = $(sender).parent();

    if (confirm("Désirez-vous retirer cet article de votre panier?")) {

        $.ajax({
            headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
            type: "post",
            url: routes.panier.remove,
            data: {
                "rowid": articleElement.data("id")
            },
            success: function(response) {

                articleElement.remove();
                updateSummary(response);

                var articlesCount = $(".basket-product").length;
                $("#basket-count").text((articlesCount > 0 ? articlesCount : "Aucun") + " article(s).");
            }
        });
    }
}

/**
 * Cancel the quantity modifications and reset elements.
 */
function cancelQuantity(sender) {

    // Retrieve every element asscociated with the quantity.
    var cancelElement = $(sender);
    var articleElement = cancelElement.closest(".basket-product");
    var quantityElement = articleElement.find(".product-quantity");

    // Reset values for the quantity.
    cancelElement.parent().hide();
    quantityElement.val(quantityElement.data("oldvalue"));
}

/**
 * Update the basket summary elements.
 */
function updateSummary(response) {
    $("#summary-subtotal").text(response.subtotal + " $");
    $("#summary-taxes").text(response.taxes + " $");
    $("#summary-total").text(response.subtotaltaxes + " $");
}