function ajoutQuestion(){
    var guid = guidGenerator();
        $('.questionlist').append(''+
        '<div class="card border-info form-row qitem question-'+guid+' mb-3 p-2">'+
            '<div class="form-group col-sm-12 ">'+
                '<label for="question">Question</label>'+
                '<div class="input-group">'+
                    '<input class="form-control" id="question" name="question[]" type="text" value="">'+
                    '<div class="input-group-append">'+
                        '<button  class="btn btn-secondary" type="button" onclick="deleteQuestion(\''+guid+'\')"><i class="fa fa-remove"></i></button>'+
                    '</div>'+
                '</div>'+
                '<div class="text-danger"></div>'+
            '</div>'+
            ''+
            '<div class="form-group col-sm-12 ">'+
                '<label for="reponse_question">Réponse</label>'+
                '<textarea class="form-control" id="reponse_question" name="reponse_question[]"></textarea>'+
                '<div class="text-danger"></div>'+
            '</div>'+
        '</div>'
        );
}


/**
 * Remove a recipe.
 * 
 */
function deleteQuestion(index){
    if (confirm("Voulez-vous supprimer cette question?"))
    {
        $('.question-'+index).remove();
    }
}