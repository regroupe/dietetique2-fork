
$(document).ready(function() {

    /**
     * Parse the selected image to be displayed inside the preview area.
     */
    $("#image").change(function() {
        
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#image-preview').css("background-image", "url("+e.target.result+")");
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

    /**
     * Show the file name in the custom file selection box.
     */
    $('.custom-file-input').on('change', function() {
        $(this).next('.custom-file-label').addClass("selected").html($(this).val().split('\\').pop()); 
    });
});

/**
 * Remove an image file from a file input when it is associated with a preview
 * area.
 */
function removeImageFile(element, preview, message) {

	$(preview).css("background-image", "url(/img/preview.png)");
	
	removeFile(element, message);
}

/**
 * Clear a file input field, reset its label message and mark it as
 * dirty in order to be properly handled by the controller.
 */
function removeFile(element, message) {

	// Special trick used to clear a file input once populated.
	$(element).wrap('<form>').closest('form').get(0).reset();
	$(element).unwrap();
	$(element+'_dirty').val(1);

	// Update the input label accordingly.
	$("label[for='"+element.replace("#", "")+"']").html(message);
}

/**
 * Create a new brand based on the prompt answer.
 * 
 */
function ajoutMarque(select){
    var nom_marque = prompt('Saisir la nouvelle marque','');
    if(nom_marque != null && nom_marque != ""){
            $.ajax({
                headers:{'X-CSRF-TOKEN': $('meta[name ="csrf-token"]').attr('content')},
                type:'post',
                url: routes.article.registerbrand,
                data: {
                    'nom_marque': nom_marque
                },
                success: function(data){
                    if (data['msg'] == "")
                    {
                        $(select).empty();
                        $(select).append(data['html']);
                    }else{
                        alert(data['msg']);
                    }
                }
            });
    }
}

/**
 * Create a new categorie based on the prompt answer.
 * 
 */
function ajoutCategorie(select){
    var nom_categorie = prompt('Saisir la nouvelle catégorie','');
    if(nom_categorie != null && nom_categorie != ""){
            $.ajax({
                headers:{'X-CSRF-TOKEN': $('meta[name ="csrf-token"]').attr('content')},
                type:'post',
                url: routes.article.registercategory,
                data: {
                    'nom_categorie': nom_categorie
                },
                success: function(data){
                    if (data['msg'] == "")
                    {
                        $(select).empty();
                        $(select).append(data['html']);
                    }else{
                        alert(data['msg']);
                    }
                }
            }); 
    }
}

/**
 * Create a new provider based on the prompt answer.
 * 
 */
function ajoutFournisseur(select){
    var nom_fournisseur = prompt('Saisir le nouveaux fournisseur','');
    if(nom_fournisseur != null && nom_fournisseur != ""){
            $.ajax({
                headers:{'X-CSRF-TOKEN': $('meta[name ="csrf-token"]').attr('content')},
                type:'post',
                url: routes.article.registersupplier,
                data: {
                    'nom_fournisseur': nom_fournisseur
                },
                success: function(data){
                    if (data['msg'] == "")
                    {
                        $(select).empty();
                        $(select).append(data['html']);
                    }else{
                        alert(data['msg']);
                    }
                }
            }); 
    }
}

/**
 * Create a new provider based on the prompt answer.
 * 
 */
function ajoutUnite(select){
    var nom_unite = prompt('Saisir la nouvelle unité de mesure','');
    if(nom_unite != null && nom_unite != ""){
            $.ajax({
                headers:{'X-CSRF-TOKEN': $('meta[name ="csrf-token"]').attr('content')},
                type:'post',
                url: routes.article.registerunite,
                data: {
                    'nom_unite': nom_unite
                },
                success: function(data){
                    if (data['msg'] == "")
                    {
                        $(select).empty();
                        $(select).append(data['html']);
                    }else{
                        alert(data['msg']);
                    }
                    
                }
            }); 
    }
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})


/**
 * Add a recipe with basic validation.
 * The inputs are arrays which will be used during the POST.
 */
function ajoutRecette(nom, le_url){
    var guid = guidGenerator();
    nom = $(nom).val();
    le_url = $(le_url).val();
    if (validate_recette(nom, le_url)){
        $('.recette').append(''+
        '<div class="form-row recette-'+guid+' mb-3">'+

            '<div class="form-group col-sm-12 col-md-4 col-lg-4">'+
                '<label>Nom de la recette</label>'+
                '<div class="input-group">'+
                    '<input class="form-control" name="nom_recette[]" value="' + nom + '"></input>'+
                '</div>'+
            '</div>'+

            '<div class="form-group col-sm-12 col-md-8 col-lg-8">'+
                '<label>URL</label>'+
                '<div class="input-group">'+

                    '<input class="form-control" name="url_recette[]" value="' + le_url + '"></input>'+
                    
                    '<div class="input-group-append">'+
                        '<button  class="btn btn-secondary" type="button" onclick="deleteRecette(\''+guid+'\')"><i class="fa fa-remove"></i></button>'+
                    '</div>'+
                '</div>'+
            '</div>'+

        '</div>');

        $('#input_recette').val('');
        $('#input_url').val('');
    }
}


/**
 * Remove a recipe.
 * 
 */
function deleteRecette(index){
    if (confirm("Voulez-vous supprimer cette recette?"))
    {
        $('.recette-'+index).remove();
    }
}

/**
 * Validate if the recipe name and/or the url is null and check the number of caracters.
 * 
 */
function validate_recette(nom, le_url)
{
    var good = 0;
    if (nom != "")
    {
        if (nom.length < 255)
        {
            good++;
            $('#error-input-recette').text('');
        } 
        else{
            $('#error-input-recette').text('Maximum de 254 caractères.');
        }
    }
    else{
        $('#error-input-recette').text('Saisir un nom.');
    }

    if (le_url != "")
    {
        if (le_url.length < 1024)
        {
            good++;
            $('#error-input-url').text('');
        } 
        else{
            $('#error-input-url').text('Maximum de 1023 caractères.');
        }
    }
    else{
        $('#error-input-url').text('Saisir un URL.');
    }


    if (good == 2) return true; else return false;
}