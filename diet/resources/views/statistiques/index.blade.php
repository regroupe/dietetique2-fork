
<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
?>

@extends('layouts.master')

@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
<style>
    #category-banner {
        background: url('/img/analytics.jpg') center center / cover no-repeat;
        color: white;
        text-shadow: 0 0 10px black;
        box-shadow: 100vw 100vh inset rgba(0, 0, 0, 0.5);
    }
</style>
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Statistiques</h1>
    </div>
</div>

{{ Breadcrumbs::render('Statistiques') }}

<div class="container">
    <div class="row">
        @include('administrateur._side_menu')
        <div class="col-12 col-sm-12 col-md-9">
            
            <form action="{{ action('StatistiquesController@Index') }}">
                <h4><i class="fa fa-filter" aria-hidden="true"></i> Filtrer les statistiques</h4>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                    </div>
                    <input name="filtre" type="text" class="form-control from" placeholder="Choisir un mois/année" autocomplete="off">
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Filtrer</button>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                        {!! Controllers\StatistiquesController::ArticlesPopulairesPartial($month, $year, true) !!}
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                    {!! Controllers\StatistiquesController::TendanceCommandesPartial($year,true) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                    {!! Controllers\StatistiquesController::UtilisateursPartial() !!}
                    {!! Controllers\StatistiquesController::AdministrateursPartial() !!}
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                    {!! Controllers\StatistiquesController::ArticlesFournisseursPartial() !!}
                    {!! Controllers\StatistiquesController::ArticlesMarquesPartial() !!}
                    {!! Controllers\StatistiquesController::ArticlesCategoriesPartial() !!}
                </div>
            </div>  
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script>
    $('.from').datepicker({
        autoclose: true,
        minViewMode: 1,
        format: 'mm/yyyy'
    });
</script>
@endsection