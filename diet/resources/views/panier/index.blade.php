
<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $articles = Controllers\PanierController::Read();
    $summary = Controllers\PanierController::ReadSummary();
    $articlesCount = count($articles);

    $subtotal = $summary["subtotal"];
    $subtotaltaxes = $summary["subtotaltaxes"];
    $taxes = $subtotaltaxes - $subtotal;
    $formatNumber = Controllers\PersonnalisationController::GetNumberFormat();
?>

@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/panier.css') }}">
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Mon panier</h1>
    </div>
</div>

{{ Breadcrumbs::render('Panier') }}

<div class="container">
    @include('periodecommande._activeperiod')

    @if (session()->has('error-paypal'))
        <div  class="alert alert-danger">
                <strong>{{session()->get('error-paypal')}}</strong>
        </div>
    @endif

    <div class="row" style="min-height: 330px;">

        <!-- Basket section -->
        <div class="col-sm-6 col-md-6 col-lg-8">
            <div id="basket">
                
                <?php foreach ($articles as $articlePanier) : $article = $articlePanier->article; $progress = Controllers\ArticleController::ReadProgress($article); ?>
                    
                    <div class="basket-product" data-id="{{ $article->rowid }}">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-3">
                                <div class="image-preview" style="background-image: url({{ $article->image ? Storage::url($article->image) : '/img/preview.png' }})"></div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-7 col-lg-8">
                                <a href="{{ action('BoutiqueController@View_GET', $article->rowid) }}"><h3>{{ $article->nom }}</h3></a>
                                <p class="product-price">{{ $article->format_quantite }} {{ $article->format }} pour {{ number_format(truncate_number(Controllers\ArticleController::ReadPricePlus($article), 2), 2, $formatNumber->decimal, $formatNumber->separator) }} $</p>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="prix-addon"><strong>Qté</strong></span>
                                    </div>
                                    <div class="input-group-prepend product-quantity-update" style="display: none">
                                        <button class="btn btn-success" type="button" onclick="updateQuantity(this)"><i class="fa fa-check" aria-hidden="true"></i></button>
                                        <button class="btn btn-secondary" type="button" onclick="cancelQuantity(this)"><i class="fa fa-times" aria-hidden="true"></i></button>
                                    </div>
                                    <input type="number" class="form-control product-quantity" value="{{ $articlePanier->quantite }}" min="0" data-oldvalue="{{ $articlePanier->quantite }}">
                                </div>
                                <div class="progress" data-toggle="tooltip" data-placement="top" title="{{ $progress['percentage'] }}% de la quantité totale requise pour regroupement d'achat présentement commandé!">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress['percentage'] >= 100 ? 'bg-success' : '' }}" style="width: {{ $progress['percentage'] }}%" role="progressbar" aria-valuenow="{{ $progress['percentage'] }}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                        <button class="basket-remove btn btn-light" type="button" onclick="removeArticle(this)"><i class="fa fa-times fa-2x" aria-hidden="true"></i></button>
                    </div>

                <?php endforeach; ?>
                <p id="basket-count">{{ ($articlesCount ==! 0 ? $articlesCount : "Aucun") }} {{Controllers\PersonnalisationController::GetSellerName(true, false)}}.</p>
            </div>
        </div>

        <!-- Order summary -->
        <div class="col-sm-6 col-md-6 col-lg-4" >
            <div id="summary-offset"></div>
            <form action="{{ action('PayPalController@checkout') }}" method="POST" enctype="multipart/form-data" data-toggle="affix">
                {{ csrf_field() }}
                <div id="order-summary">
                    <h5>Résumé</h5>
                    <table class="table">
                        <tr class="table-light">
                            <td><strong>Sous-total</strong></td>
                            <td class="text-right" id="summary-subtotal">{{ number_format(truncate_number($subtotal, 2), 2, $formatNumber->decimal, $formatNumber->separator) }} $</td>
                        </tr>
                        <tr class="table-light">
                            <td><strong>Frais</strong></td>
                            <td class="text-right" id="summary-taxes">{{ number_format(truncate_number($taxes, 2), 2, $formatNumber->decimal, $formatNumber->separator) }} $</td>
                        </tr>
                        <tr class="table-success">
                            <td><strong>Total*</strong></td>
                            <td class="text-right" id="summary-total">{{ number_format(truncate_number($subtotaltaxes, 2), 2, $formatNumber->decimal, $formatNumber->separator) }} $</td>
                        </tr>
                    </table>
                    @if (Controllers\PeriodeController::OrderPeriodActive())
                        @if (Controllers\CompteController::CheckIfAddressExist())
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Placer ma commande</button>
                            <a href="{{action('CompteController@IndexCompte')}}" class='btn btn-secondary btn-lg btn-block' style="white-space: normal;">Modifier mon adresse de facturation</a>
                        @else
                             <a href="{{action('CompteController@IndexCompte')}}" class='btn btn-warning btn-lg btn-block' style="white-space: normal;">Modifier mon adresse de facturation</a>
                        @endif
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var routes = {
        panier: {
            update: "{{ action('PanierController@Update_POST') }}",
            remove: "{{ action('PanierController@Remove_POST') }}"
        }
    };
</script>
<script src="{{ asset('js/panier.js') }}"></script>
@endsection