
@extends('layouts.master')


@section('styles')
<link rel="stylesheet" href ="{{asset('https://use.fontawesome.com/releases/v5.5.0/css/all.css')}}" integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>

@endsection

@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">Erreur {{$no}}</h1>
   </div>
</div>

{{ Breadcrumbs::render('Erreur', $no) }}

<div class="container">
	<div class="row">
		<div class="col-12 text-center">
			<div style="padding-bottom:15px;">Une erreur s'est malheureusement produite.</div>
			<a class="btn btn-success" href="{{action('BoutiqueController@Index')}}">Retour à la boutique</a>
		</div>	
	</div>
</div>

</body>
@endsection

@section('scripts')
@endsection