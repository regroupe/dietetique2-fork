<?php
	use App\Http\Controllers;
	use Carbon\Carbon;
	use Carbon\CarbonInterface;
	use Illuminate\Support\Facades\Storage;
	$mainTheme = Controllers\PersonnalisationController::ReadActif();
?>
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">


	<link rel="icon" href="{{($mainTheme->icon) ? url(Storage::url($mainTheme->icon)).'?='. time() : url('/img/backup-icon.ico')}}" />
	<title>{{($mainTheme->titre_onglet) ? $mainTheme->titre_onglet : 'Regroupement d\'achat'}}</title>

	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/fullcalendar.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/styles.css') }}">
	<link rel="stylesheet" href="{{ asset('css/cart-count.css')}}">
	<link rel="stylesheet" href="{{ asset('css/master.css')}}">
	<link rel="stylesheet" href="{{ asset('css/side_menu.css') }}">
	@yield('styles')

	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ asset('js/moment.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
	<script src="{{ asset('js/fullcalendar.min.js') }}"></script>
	<script src="{{ asset('js/locale/en-ca.js') }}"></script>
	<script src="{{ asset('js/locale/fr-ca.js') }}"></script>
	<script src="{{ asset('js/master.js') }}"></script>
	<style>	#category-banner { background: url('{{Storage::url($mainTheme->banniere)}}') center center / cover no-repeat !important; }</style>

</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="container">
				<a class="navbar-brand" href="{{ route('index') }}"><div class="master-logo" style="background-image: url({{Storage::url($mainTheme->logo)}})"></div></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link {{ Request::segment(1) == 'boutique' ? 'nav-active-clr' : '' }}" href="{{ action('BoutiqueController@Index') }}">Boutique</a>
						</li>
						<li class="nav-item">
							<a class="nav-link {{ Request::segment(1) == 'calendrier' ? 'nav-active-clr' : '' }}" href="{{ action('PeriodeController@IndexCalendar') }}">Calendrier</a>
						</li>

						<ul class = "navbar-nav"> 

						<li class="nav-item">
							<a class="nav-link {{ Request::segment(1) == 'faq' ? 'nav-active-clr' : '' }}" href="{{ action('ParametragesController@FAQ') }}">FAQ</a>
						</li>
									
						<li class="nav-item">
							<a class="nav-link {{ Request::segment(1) == 'apropos' ? 'nav-active-clr' : '' }}" href="{{ action('ParametragesController@Apropos') }}">À propos</a>
						</li>

						
						</ul>
					</ul>
					<ul class="navbar-nav">
						@if (session('client') != null)
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle {{ (Request::segment(1) == 'compte' && Request::segment(2) != 'panier') ? 'nav-active-clr' : '' }}" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> Mon Compte</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
								<a class="dropdown-item" href="{{ action('CompteController@IndexCompte') }}">Compte</a>
								<a class="dropdown-item" href="{{ action('CompteController@IndexHistorique') }}">Historique</a>
								<a class="dropdown-item" href="{{ action('ConnexionController@Logout') }}">Déconnexion</a>
							</div>
						</li>
						<a class="nav-link {{ (Request::segment(1) == 'compte' && Request::segment(2) == 'panier') ? 'nav-active-clr' : '' }}" href="{{ action('PanierController@Index') }}"  aria-haspopup="true" aria-expanded="false"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Panier</a>
						<!--<span class = "cart-items-count"><span class = "notification-counter">@$count</span></span>-->
						@elseif (session('admin') != null)
							@if (session('admin')->super == 1)
								<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle {{ Request::segment(1) == 'administrateur' ? 'nav-active-clr' : '' }}" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> Mon Compte</a>
										<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
											<a class="dropdown-item" href="{{ action('AdminController@IndexTableau') }}">Tableau de bord</a>
											<a class="dropdown-item" href="{{ action('ArticleController@Index') }}">{{Controllers\PersonnalisationController::GetSellerName(true, true)}}</a>
											<a class="dropdown-item" href="{{ action('PeriodeController@IndexPeriode') }}">Périodes</a>
											<a class="dropdown-item" href="{{ action('ConnexionController@Logout') }}">Déconnexion</a>
										</div>
								</li>
							@else
								<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle {{ Request::segment(1) == 'administrateur' ? 'nav-active-clr' : '' }}" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> Mon Compte</a>
										<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
											<a class="dropdown-item" href="{{ action('AdminController@IndexCompte') }}">Compte</a>
											<a class="dropdown-item" href="{{ action('ArticleController@Index') }}">Mes {{Controllers\PersonnalisationController::GetSellerName(true, true)}}</a>
											<a class="dropdown-item" href="{{ action('ConnexionController@Logout') }}">Déconnexion</a>
										</div>
								</li>
							@endif
						
						@else
						<li class="nav-item">
							<a class="nav-link {{ (Request::segment(1) == 'connexion') ? 'nav-active-clr' : '' }}" href="{{ action('ConnexionController@IndexConnexion') }}">Connexion</a>
						</li>
						@endif
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<main>
		@yield('content')
	</main>
	<footer class="pt-4">
		<div class="container">
				
			<div class="row">
				
				<div class="col-12 col-md-4 text-center text-md-left">
					<div class="row align-items-center align-items-md-start justify-content-center justify-content-md-start pl-md-3">
						<div class="master-logo" style="background-image: url({{Storage::url($mainTheme->logo)}})"></div>
					</div>	
					<p>{{$mainTheme->footer_desc}}</p>
				</div>
				<div class="col-12 col-md-3">
					
				</div>
				<div class="col-12 col-md-5 text-center text-md-right">
					<h5>{{$mainTheme->footer_entreprise}}</h5>
					<p>
						{{$mainTheme->footer_address}}<br>
						{{$mainTheme->footer_city}}<br>
						{{$mainTheme->footer_code_postal}}<br>
						<span id="phone">{{$mainTheme->footer_telephone}}</span><br>
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-12 col-lg-12 text-center">
					<small class="d-block text-muted">{{$mainTheme->footer_copyright}}</small>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-12 col-lg-12 text-center">
					<small class="d-block text-muted lien"><a class="d-block text-muted lien" href="{{ action('ParametragesController@TermsOfUse') }}">Conditions d'utilisation et politiques</a></small>
				</div>
			</div>
		</div>
	</footer>
	@yield('scripts')
</body>
</html>