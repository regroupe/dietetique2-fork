<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $marques = Controllers\MarqueController::Read();
    $categories = Controllers\CategorieController::Read();
    $fournisseurs = Controllers\FournisseurController::Read();
    $formatNumber = Controllers\PersonnalisationController::GetNumberFormat();
    $recettes = null;
?>

@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/articles.css') }}">
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Gestion des {{Controllers\PersonnalisationController::GetSellerName(true, true)}}</h1>
    </div>
</div>

{{ Breadcrumbs::render('GestionArticle') }}

<div class="container">
    <div class="row">
    @include('administrateur._side_menu')
        <div id="partial" class="col-sm-12 col-md-9 col-lg-9">            
            <form id="search-list">
                <h4><i class="fa fa-search-plus" aria-hidden="true"></i> Critères de recherche</h4>
                <div class="row">
                    <div class="col-4 form-group">
                        <select name="filter_brand" class="search-input form-control">
                            <option value="">Toutes les marques</option>
                            <?php foreach ($marques as $marque) : ?>
                                <option value="{{ $marque->nom_marque }}" {{ $marque->nom_marque == $req->filter_brand ? 'selected' : '' }}>{{ $marque->nom_marque }}</option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-4 form-group">
                        <select name="filter_supplier" class="search-input form-control">
                            <option value="">Tous les fournisseurs</option>
                            <?php foreach ($fournisseurs as $fournisseur) : ?>
                                <option value="{{ $fournisseur->nom_fournisseur }}" {{ $fournisseur->nom_fournisseur == $req->filter_supplier ? 'selected' : '' }}>{{ $fournisseur->nom_fournisseur }}</option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-4 form-group">
                        <select name="filter_category" class="search-input form-control">
                            <option value="">Toutes les catégories</option>
                            <?php foreach ($categories as $categorie) : ?>
                                <option value="{{ $categorie->nom_categorie }}" {{ $categorie->nom_categorie == $req->filter_category ? 'selected' : '' }}>{{ $categorie->nom_categorie }}</option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class='input-group'>
                        <input class="form-control" name="filter_search" value="{{ $req->filter_search }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-success" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Rechercher</button>
                        </div>
                    </div>
                </div>

                <h4><i class="fa fa-list" aria-hidden="true"></i> Liste des {{Controllers\PersonnalisationController::GetSellerName(true, true)}}</h4>

                <table class="table table-striped table-responsive-sm">
                    <thead class="thead-dark">
                        <th colspan="2">
                            <button type="button" class="btn btn-secondary" onclick="addArticle()">Ajouter</button>
                        </th>
                        <th colspan="2">
                            <div class="text-right">
                                <label>Afficher</label>
                                <select name="filter_pages" class="search-input form-control" style="display: inline-block; max-width: 80px">
                                    <option value="10" {{ $req->filter_pages == 10 ? 'selected' : '' }}>10</option>
                                    <option value="25" {{ $req->filter_pages == 25 ? 'selected' : '' }}>25</option>
                                    <option value="50" {{ $req->filter_pages == 50 ? 'selected' : '' }}>50</option>
                                    <option value="100" {{ $req->filter_pages == 100 ? 'selected' : '' }}>100</option>
                                </select>
                            </div>
                        </th>
                    </thead>
                    <thead class="table-dark">
                        <tr>
                            <th scope="col">
                                <input class="sort-input form-check-input" type="checkbox" value="{{ $req->sort_name }}" name="sort_name" id="sort_name" style="display: none" data-filter="#filter-name" {{ $req->sort_name ? 'checked' : '' }}>
                                <label class="search-input-label form-check-label" for="sort_name">
                                    <i id="filter-name" class="fa fa-{{ $req->sort_name ? ($req->sort_name == 1 ? 'arrow-up' : 'arrow-down') : 'filter' }}" aria-hidden="true" data-intermediate-0="filter" data-intermediate-1="arrow-up" data-intermediate-2="arrow-down"></i> 
                                    {{Controllers\PersonnalisationController::GetSellerName(true, true)}}
                                </label>
                            </th>
                            <th scope="col" class="text-right">
                                <input class="sort-input form-check-input" type="checkbox" value="{{ $req->sort_price }}" name="sort_price" id="sort_price" style="display: none" data-filter="#filter-price" {{ $req->sort_price ? 'checked' : '' }}>
                                <label class="search-input-label form-check-label" for="sort_price">
                                    <i id="filter-price" class="fa fa-{{ $req->sort_price ? ($req->sort_price == 1 ? 'arrow-up' : 'arrow-down') : 'filter' }}" aria-hidden="true" data-intermediate-0="filter" data-intermediate-1="arrow-up" data-intermediate-2="arrow-down"></i> Prix
                                </label>
                            </th>
                            <th scope="col" class="text-right">
                                <input class="sort-input form-check-input" type="checkbox" value="{{ $req->filter_active }}" name="filter_active" id="filter_active" style="display: none" data-filter="#filter-active" {{ $req->filter_active ? 'checked' : '' }}>
                                <label class="search-input-label form-check-label" for="filter_active">
                                    <i id="filter-active" class="fa fa-{{ $req->filter_active ? ($req->filter_active == 1 ? 'check-circle-o' : 'ban') : 'filter' }}" aria-hidden="true" data-intermediate-0="filter" data-intermediate-1="check-circle-o" data-intermediate-2="ban"></i> Actif
                                </label>
                            </th>
                            <th scope="col" class="text-right">Options</th>
                        </tr>
                    </thead>
                    <?php echo $partial; ?>
                </table>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var routes = {
        article: {
            index: "{{ action('ArticleController@Index') }}",
            create: "{{ action('ArticleController@Create_GET') }}",
            activate: "{{ action('ArticleController@Activate_POST') }}",
            deactivate: "{{ action('ArticleController@Delete_POST') }}",
        }
    };
</script>
<script src="{{ asset('js/articles.js') }}"></script>
@endsection