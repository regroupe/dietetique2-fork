
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Aperçu</label>
            <div id="image-preview" style="background-image: url({{ $article->image ? Storage::url($article->image) : '/img/preview.png' }})"></div>
            <div class="input-group">
                <div class="custom-file">
                	<input type="hidden" id="image_dirty" name="image_dirty" value="0">
                    <input type="file" class="custom-file-input" id="image" name="image">
                    <label class="custom-file-label" for="image">{{ $article->image ? basename($article->image) : 'Choisir une image' }}</label>
                </div>
				<div class="input-group-append">
					<button id="remove-image" class="btn btn-secondary" type="button" onclick="removeImageFile('#image', '#image-preview', 'Choisir une image')"><i class="fa fa-remove"></i></button>
				</div>
            </div>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('image') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('image') }}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for='nom'>Nom</label>
            <input class="form-control" id='nom' name='nom' value='{{ old("nom", $article->nom) }}'>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('nom') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('nom') }}
        </div>

        <div class="form-group">
            <label for='format_quantite'>Format</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="prix-addon"><strong>Qté</strong></span>
                </div>
                <input class="form-control input-right" type="number" min="1" max="1000" step="0.01" id='format_quantite' name='format_quantite' value='{{ old("format_quantite", $article->format_quantite) }}'></input>
                <select class="form-control" id="format" name="format">
                    <?php foreach ($formats as $format) : ?>
                        <option {{ old("format", $article->format) == $format->nom_unite ? 'selected' : '' }}>{{ $format->nom_unite }}</option>
                    <?php endforeach; ?>
                </select>
                <div class="input-group-append">
                    <button class="btn btn-outline-info" type="button" onclick="ajoutUnite('#format')" data-toggle="tooltip" data-placement="top" title="Ajouter une unité de mesure"><i class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('format_quantite') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('format_quantite') }}
        </div>

        <div class="form-group">
            <label for='provenance'>Provenance</label>
            <input class="form-control" id='provenance' name='provenance' value='{{ old("provenance", $article->provenance) }}'></input>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('provenance') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('provenance') }}
        </div>

        <div class="form-group">
            <label for='allergene'>Allergène(s)</label>
            <input class="form-control" id='allergene' name='allergene' value='{{ old("allergene", $article->allergene) }}'></input>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('allergene') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('allergene') }}
        </div>

        <div class="row">
            <div class="col-12" style="text-align: center">
                <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input type="hidden" name="perissable" value="0">
                        <input class="form-check-input" type="checkbox" name="perissable" value="1" {{ old('perissable', $article->perissable) ? 'checked' : '' }}>
                        <label class="form-check-label" for='perissable'>Périssable</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input type="hidden" name="bio" value="0">
                        <input class="form-check-input" type="checkbox" name="bio" value="1" {{ old('bio', $article->bio) ? 'checked' : '' }}>
                        <label class="form-check-label" for='bio'>Biologique</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input type="hidden" name="quebec" value="0">
                        <input class="form-check-input" type="checkbox" name="quebec" value="1" {{ old('quebec', $article->quebec) ? 'checked' : '' }}>
                        <label class="form-check-label" for='quebec'>Québec</label>
                    </div>
                </div>

                <div class="alert alert-danger" style="display: {{ $errors->first('perissable') ? 'block' : 'none' }}">
                    <i class="fa fa-exclamation-circle"></i>
                    {{ $errors->first('perissable') }}
                </div>

                <div class="alert alert-danger" style="display: {{ $errors->first('bio') ? 'block' : 'none' }}">
                    <i class="fa fa-exclamation-circle"></i>
                    {{ $errors->first('bio') }}
                </div>

                <div class="alert alert-danger" style="display: {{ $errors->first('quebec') ? 'block' : 'none' }}">
                    <i class="fa fa-exclamation-circle"></i>
                    {{ $errors->first('quebec') }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for='nom_marque'>Marque</label>
            <div class="input-group">
            <select class="form-control" id='nom_marque' name='nom_marque' value='{{ old("nom_marque", $article->nom_marque) }}'>
                <?php foreach ($marques as $marque) : ?>
                    <option {{ old("nom_marque", $article->nom_marque) == $marque->nom_marque ? 'selected' : '' }}>{{ $marque->nom_marque }} </option> 
                    
                <?php endforeach; ?>
            </select>
            <div class="input-group-append">
                    <button class="btn btn-outline-info" type="button" onclick="ajoutMarque('#nom_marque')" data-toggle="tooltip" data-placement="top" title="Ajouter une marque"><i class="fa fa-plus"></i></button>
                </div>
            </div>

        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('nom_marque') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('nom_marque') }}
        </div>

        <div class="form-group">
            <label for='nom_categorie'>Catégorie</label>
            <div class="input-group">
                <select class="form-control" id='nom_categorie' name='nom_categorie' value='{{ old("nom_categorie", $article->nom_categorie) }}'>
                    <?php foreach ($categories as $category) : ?>
                        <option {{ old("nom_categorie", $article->nom_categorie) == $category->nom_categorie ? 'selected' : '' }}>{{ $category->nom_categorie }}</option>
                    <?php endforeach; ?>
                </select>
                @if (session('admin')->super)
                <div class="input-group-append">
                        <button class="btn btn-outline-info" type="button" onclick="ajoutCategorie('#nom_categorie')" data-toggle="tooltip" data-placement="top" title="Ajouter une catégorie"><i class="fa fa-plus"></i></button>
                </div>
                @endif
            </div>
        </div>

        <div class="alert alert-danger" style="display: {{ $errors->first('nom_categorie') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('nom_categorie') }}
        </div>

        <div class="form-group">
            <label for='nom_fournisseur'>Fournisseur</label>
            <div class="input-group">
                <select class="form-control" id='nom_fournisseur' name='nom_fournisseur' value='{{ old("nom_fournisseur", $article->nom_fournisseur) }}'>
                    <?php foreach ($fournisseurs as $fournisseur) : ?>
                        <option {{ old("nom_fournisseur", $article->nom_fournisseur) == $fournisseur->nom_fournisseur ? 'selected' : '' }}>{{ $fournisseur->nom_fournisseur }}</option>
                    <?php endforeach; ?>
                </select>
                <div class="input-group-append">
                        <button class="btn btn-outline-info" type="button" onclick="ajoutFournisseur('#nom_fournisseur')" data-toggle="tooltip" data-placement="top" title="Ajouter un fournisseur"><i class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('nom_fournisseur') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('nom_fournisseur') }}
        </div>
    </div>
</div>

<h3 class="heading">Informations de vente</h3>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for='quantite_minimum'>Minimum</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="prix-addon"><strong>Qté</strong></span>
                </div>
                <input class="form-control" type="number" min="1" max="1000" step="1" id='quantite_minimum' name='quantite_minimum' value='{{ old("quantite_minimum", $article->quantite_minimum) }}'></input>
            </div>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('quantite_minimum') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('quantite_minimum') }}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for='quantite_maximum'>Maximum</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="prix-addon"><strong>Qté</strong></span>
                </div>
                <input class="form-control" type="number" min="1" max="1000" step="1" id='quantite_maximum' name='quantite_maximum' value='{{ old("quantite_maximum", $article->quantite_maximum) }}'></input>
            </div>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('quantite_maximum') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('quantite_maximum') }}
        </div>
    </div>
</div>

<div class="row">
        <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for='prix'>Prix</label>
                    <div class="input-group">
                        <input class="form-control input-right" type="number" min="0" max="1000" step="0.01" id='prix' name='prix' value='{{ old("prix", $article->prix) }}'></input>
                        <div class="input-group-append">
                            <span class="input-group-text" id="prix-addon"><strong>$</strong></span>
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger" style="display: {{ $errors->first('prix') ? 'block' : 'none' }}">
                    <i class="fa fa-exclamation-circle"></i>
                    {{ $errors->first('prix') }}
                </div>
            </div>
</div>

<div class="row">
    <div class="col-12 col-md-4">
        <div class="form-group">
            <label for='frais_fixe'>Frais fixe</label>
            <div class="input-group">
                <input class="form-control input-right" type="number" min="0" max="1000" step="0.01" id='frais_fixe' name='frais_fixe' value='{{ old("frais_fixe", $article->frais_fixe) }}'></input>
                <div class="input-group-append">
                    <span class="input-group-text" id="prix-addon"><strong>%</strong></span>
                </div>
            </div>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('frais_fixe') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('frais_fixe') }}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for='frais_variable'>Frais variable</label>
            <div class="input-group">
                <input class="form-control input-right" type="number" min="0" max="1000" step="0.01" id='frais_variable' name='frais_variable' value='{{ old("frais_variable", $article->frais_variable) }}'></input>
                <div class="input-group-append">
                    <span class="input-group-text" id="prix-addon"><strong>$</strong></span>
                </div>
            </div>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('frais_variable') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('frais_variable') }}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for='frais_emballage'>Frais d'emballage</label>
            <div class="input-group">
                <input class="form-control input-right" type="number" min="0" max="1000" step="0.01" id='frais_emballage' name='frais_emballage' value='{{ old("frais_emballage", $article->frais_emballage) }}'></input>
                <div class="input-group-append">
                    <span class="input-group-text" id="prix-addon"><strong>$</strong></span>
                </div>
            </div>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('frais_emballage') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('frais_emballage') }}
        </div>
    </div>
</div>

<h3 class="heading">Informations supplémentaires</h3>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for='description'>Description</label>
            <textarea class="form-control" id='description' name='description'>{{ old("description", $article->description) }}</textarea>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('description') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('description') }}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for='remarque'>Remarque</label>
            <textarea class="form-control" id='remarque' name='remarque'>{{ old("remarque", $article->remarque) }}</textarea>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('remarque') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('remarque') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Valeur Nutritive</label>
            <div class="input-group">
            	<div class="custom-file">
            		<input type="hidden" id="valeur_nutritive_dirty" name="valeur_nutritive_dirty" value="0">
                    <input data-buttonText="Parcourir" type="file" class="custom-file-input" id="valeur_nutritive" name="valeur_nutritive">
                    <label class="custom-file-label" for="valeur_nutritive">{{ $article->valeur_nutritive ? basename($article->valeur_nutritive) : 'Choisir une image' }}</label>
                </div>
                <div class="input-group-append">
					<button id="remove-nutritional" class="btn btn-secondary" type="button" onclick="removeFile('#valeur_nutritive', 'Choisir une image')"><i class="fa fa-remove"></i></button>
				</div>
            </div>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('valeur_nutritive') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('valeur_nutritive') }}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>Pièce Jointe</label>
            <div class="input-group">
                <div class="custom-file">
                	<input type="hidden" id="piece_jointe_dirty" name="piece_jointe_dirty" value="0">
                    <input type="file" class="custom-file-input" id="piece_jointe" name="piece_jointe">
                    <label class="custom-file-label" for="piece_jointe">{{ $article->piece_jointe ? basename($article->piece_jointe) : 'Choisir un document PDF' }}</label>
                </div>
                <div class="input-group-append">
					<button id="remove-pdf" class="btn btn-secondary" type="button" onclick="removeFile('#piece_jointe', 'Choisir un document PDF')"><i class="fa fa-remove"></i></button>
				</div>
			</div>
        </div>
        <div class="alert alert-danger" style="display: {{ $errors->first('piece_jointe') ? 'block' : 'none' }}">
            <i class="fa fa-exclamation-circle"></i>
            {{ $errors->first('piece_jointe') }}
        </div>
    </div>
</div>



<h3 class="heading">Recettes</h3>
    <div class="form-row">
        <div class="form-group col-sm-12 col-md-4 col-lg-4">
            <label>Nom de la recette</label>
            <div class="input-group">
                 <input class="form-control" id='input_recette' name='input_recette' value=''></input>
            </div>
             <div id="error-input-recette" class="text-danger"></div>
        </div>
        <div class="form-group col-sm-12 col-md-8 col-lg-8">
            <label>URL</label>
            <div class="input-group">

                 <input class="form-control" id='input_url' name='input_url' value=''></input>
            	
                <div class="input-group-append">
					<button class="btn btn-outline-info" type="button" onclick="ajoutRecette('#input_recette','#input_url')" data-toggle="tooltip" data-placement="top" title="Ajouter une recette"><i class="fa fa-plus"></i></button>
				</div>
            </div>
             <div id="error-input-url" class="text-danger"></div>
        </div>
    </div>

    <hr>
    <div class="alert alert-danger" style="display: {{ ($errors->first('nom_recette') || $errors->first('url_recette')) ? 'block' : 'none' }}">
        <i class="fa fa-exclamation-circle"></i>
    </div>

    <div class="recette">
        @if (old("nom_recette") != null)
            @foreach (old("nom_recette") as $index => $old_recette)
                <?php
                    $guid =GenerateGuid();
                ?>
                @include('article._recette')


            @endforeach
        @elseif ($recettes != null)
            @foreach ($recettes as $index => $recette)
                <?php
                    $guid =GenerateGuid();
                ?>
                @include('article._recette')


            @endforeach
        @endif
    </div>
<br>

<div class="row">
    <div class="col-md-6">
        <button class="btn btn-outline-success btn-lg btn-block" type="submit">Soumettre</button><br>
    </div>

    <div class="col-md-6">
        <button class="btn btn-outline-danger btn-lg btn-block" type="button" onclick="window.location.href = '{{ action('ArticleController@Index') }}'">Annuler</button>
    </div>
</div>