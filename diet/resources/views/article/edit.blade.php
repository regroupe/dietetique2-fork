<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $formats = Controllers\UniteMesureController::Read();
    $marques = Controllers\MarqueController::Read();
    $categories = Controllers\CategorieController::Read();
    $fournisseurs = Controllers\FournisseurController::Read();
    $formatNumber = Controllers\PersonnalisationController::GetNumberFormat();
    $recettes = Controllers\ArticleController::ReadRecette($article->rowid);
?>
@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/article.css') }}">
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">{{$article->nom}}</h1>
   </div>
</div>


{{ Breadcrumbs::render('ModifierArticle', $article) }}

<div class="container pt-4">
    <form action="{{ action('ArticleController@Edit_POST', $article) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="rowid" value="{{ $article->rowid }}">

        @include('article._createedit', array('form_action' => 'edit'))
    </form>
</div>
@endsection

@section('scripts')
<script>
    var routes = {
        article: {
            registerbrand: "{{ action('MarqueController@RegisterMarque') }}",
            registercategory: "{{ action('CategorieController@RegisterCategorie') }}",
            registersupplier: "{{ action('FournisseurController@RegisterFournisseur') }}",
            registerunite: "{{ action('UniteMesureController@RegisterUnit') }}"
        }
    };
</script>
<script src="{{ asset('js/article.js') }}"></script>
@endsection