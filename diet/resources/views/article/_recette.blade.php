<div class="form-row recette-{{$guid}} mb-3">

        <div class="form-group col-sm-12 col-md-4 col-lg-4 ">
            <label>Nom de la recette</label>
            <div class="input-group">
                 <input class="form-control" name='nom_recette[]' value="{{(old('nom_recette')) ? $old_recette : $recette->name}}"></input>
            </div>
            <div class="text-danger">{{$errors->first('nom_recette.'.$index)}}</div>
        </div>
        <div class="form-group col-sm-12 col-md-8 col-lg-8">
            <label>URL</label>
            <div class="input-group">

                 <input class="form-control" name='url_recette[]' value="{{(old('url_recette')) ? old('url_recette')[$index] : $recette->link}}"></input>
            	
                <div class="input-group-append">
					<button  class="btn btn-secondary" type="button" onclick="deleteRecette('{{$guid}}')"><i class="fa fa-remove"></i></button>
				</div>
            </div>
            <div class="text-danger">{{$errors->first('url_recette.'.$index)}}</div>
        </div>
    </div>