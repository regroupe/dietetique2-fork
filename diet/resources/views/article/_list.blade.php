<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $formatNumber = Controllers\PersonnalisationController::GetNumberFormat();
?>
<tbody>
    <?php foreach ($articles as $article) : ?>
    <tr>
    	<td class="align-middle">
            {{ $article->nom }}
            {{ $article->format_quantite }} {{ $article->format }}
            <?php if ($article->bio) : ?>
                <i class="fa fa-leaf" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Biologique"></i>
            <?php endif; ?>
            <?php if ($article->perissable) : ?>
                <i class="fa fa-apple" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Périssable"></i>
            <?php endif; ?>
    	</td>
    	<td class="align-middle text-right">
            {{ number_format(truncate_number($article->prix, 2), 2, $formatNumber->decimal, $formatNumber->separator) }} $
    	</td>
    	<td class="align-middle text-right">
    		{{ $article->actif ? 'Oui' : 'Non' }}
    	</td>
    	<td class="align-middle text-right">
            <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="Détails" onclick="window.location.href = '{{ action('BoutiqueController@View_GET', $article->rowid) }}'"><i class="fa fa-eye" aria-hidden="true"></i></button>
            <button type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Modifier" onclick="window.location.href = '{{ action('ArticleController@Edit_GET', $article->rowid ) }}'"><i class="fa fa-pencil" aria-hidden="true"></i></button>
            <?php if ($article->actif) : ?>
    			<button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-placement="top" title="Désactiver" onclick="deactivateArticle({{ $article->rowid }})"><i class="fa fa-ban" aria-hidden="true"></i></button>
            <?php else : ?>
    			<button type="button" class="btn btn-outline-success" data-toggle="tooltip" data-placement="top" title="Activer" onclick="activateArticle({{ $article->rowid }})"><i class="fa fa-check-circle-o" aria-hidden="true"></i></button>
            <?php endif; ?>
    	</td>
    </tr>
    <?php endforeach; ?>
</tbody>
<tfoot class="table-dark">
    <tr>
        <td colspan="4">
            {{ $articles->links() }}
        </td>
    </tr>
</tfoot>