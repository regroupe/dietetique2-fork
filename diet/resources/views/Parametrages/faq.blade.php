<?php
use App\Http\Controllers;
$theme = Controllers\PersonnalisationController::ReadActif();
$questions = Controllers\ParametragesController::ReadFAQ($theme->rowid);
?>
@extends('layouts.master')



@section('styles')
<link rel="stylesheet" href="{{ asset('css/personnalisation.css') }}">
@endsection


@section('content')
<body>

    <div id="category-banner" class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Foire aux questions</h1>
        </div>
    </div>


{{ Breadcrumbs::render('faq') }}

    <div class="container">

        <div class = "col-12">
            @if (count($questions) > 0)
                <div class="accordion" id="accordion_theme">
                    @foreach($questions as $i => $question)
                        @include('parametrages._faqpartiel')
                    @endforeach
                </div>
            @else
                Il n'y a pas de foire aux questions actuellement. 
            @endif
        </div> 
    </div>
</body>
@endsection


@section('scripts')

@endsection