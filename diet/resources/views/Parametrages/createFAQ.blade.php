<?php
use App\Http\Controllers;
$questions = Controllers\ParametragesController::ReadFAQ($theme->rowid);
?>
@extends('layouts.master')


@section('styles')
<link rel="stylesheet" href="{{ asset('css/personnalisation.css') }}">

@endsection 


@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Modifier la Foire aux questions</h1>
    </div>
</div>

{{ Breadcrumbs::render('Themes_modifier', $theme) }}

<div class="container">
    <form action="{{action('ParametragesController@EditFAQ_POST')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="rowid" value="{{ $theme->rowid }}">
        
        <div class="row">
            <div class="col-12">
                <div class="card ">
                    <div class="card-header bg-dark">
                        <h5 style="color:#fff">Section FAQ</h5>
                    </div>
                    <div class="card-body">
                        <div class="questionlist">
                    
                            @if (old("question") != null)
                                @foreach (old("question") as $index => $questOLD)
                                    <?php
                                        $guid =GenerateGuid();
                                     ?>
                                    @include('parametrages._createfaq')


                                @endforeach
                            @elseif ($questions != null)
                                @foreach ($questions as $index => $quest)
                                    <?php
                                        $guid =GenerateGuid();
                                     ?>
                                    @include('parametrages._createfaq')


                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <button id="addQuestion" type="button" name="ajout_question" class = "btn btn-primary btn-block" onclick="ajoutQuestion()">Ajouter une question</button>
                    </div>
                </div>
            </div>
        </div>

       
       <div class="row pt-2">
            <div class="col-md-6">
                <button class="btn btn-outline-success btn-lg btn-block" type="submit">Enregistrer</button><br>
            </div>

            <div class="col-md-6">
                <button class="btn btn-outline-danger btn-lg btn-block" type="button" onclick="window.location.href = '{{ action('PersonnalisationController@Index') }}'">Annuler</button>
            </div>
        </div>
	</form>
   
</div>
@endsection


@section('scripts')
<script src="{{ asset('js/faq.js') }}"></script>
@endsection
