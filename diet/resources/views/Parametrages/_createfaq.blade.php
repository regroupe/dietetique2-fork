
                <!--Question*********************************************************************************-->
            <div class="card border-info form-row qitem question-{{$guid}} mb-3 p-2">
                <div class="form-group col-sm-12 ">
                        <label for='question'>Question</label>
                        <div class="input-group">

                            <input class="form-control" id='question' name='question[]' type='text' value='{{(old("question")) ? old("question")[$index] : $quest->question}}'>
                            
                            <div class="input-group-append">
                            <button  class="btn btn-secondary" type="button" onclick="deleteQuestion('{{$guid}}')"><i class="fa fa-remove"></i></button>
                            </div>
                        </div>
                        <div class="text-danger">{{ $errors->first('question.'.$index) }}</div>
                </div> 
               
                
                <!--Réponse**********************************************************************************-->
                <div class="form-group col-sm-12">
                        <label for='reponse_question'>Réponse</label>
                        <textarea class="form-control" id='reponse_question' name='reponse_question[]'>{{(old('reponse_question')) ? old('reponse_question')[$index] : $quest->reponse}}</textarea>
                        <div class="text-danger">{{ $errors->first('reponse_question.'.$index) }}</div>
                </div> 
            </div>

            
