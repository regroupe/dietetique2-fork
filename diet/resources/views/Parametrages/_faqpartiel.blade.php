<div class="d-none d-md-block">
    <div class="card ">
        <a class="card-header btn btn-tab btn-outline-primary text-left {{($i != 0 ? 'collapsed' : '')}}" id="heading{{$i}}" data-toggle="collapse" data-target="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
            {{$question->question}}
        </a>
        <div id="collapse{{$i}}" class="collapse {{($i == 0 ? 'show' : '')}}" aria-labelledby="heading{{$i}}" data-parent="#accordion_theme">
                <div class="card-body">
                    <p>{{$question->reponse}}
                    </p>
                </div>
        </div>
    </div>
</div>

<div class="card d-block d-md-none">
    <h6 class="card-header bg-dark text-white">
        {{$question->question}}
    </h6>
    <div class="card-body">
            <p>{{$question->reponse}}
            </p>
    </div>
</div>