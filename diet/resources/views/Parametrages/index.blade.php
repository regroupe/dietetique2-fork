
<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
     $mainTheme = Controllers\PersonnalisationController::ReadActif();
?>

@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/personnalisation.css') }}">
<style>
.margin-card{
    margin-bottom:20px;
}
</style>
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Thèmes de l'application</h1>
    </div>
</div>

{{ Breadcrumbs::render('Themes') }}

<div class="container">
    <div class="row">
        @include('administrateur._side_menu')
        <div class='col-sm-12 col-md-9 col-lg-9'>

        @if (session()->has('success'))
            <div class="alert alert-success">
                <strong>{{session()->get('success')}}</strong>
            </div>
        @endif


        @if ($mainTheme != null)
            <div class="card margin-card">
                <div class="card-body">
	                <div class="row">
	                    <div class="col-sm-12 col-md-9">
	                         <h3 class="card-title">
	                            Thème actuel : {{$mainTheme->rowid}} - {{$mainTheme->nom_theme}}
	                        </h3>
	                    </div>
	                    <div class="col-sm-12 col-md-3 text-right">
	                        <button type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Modifier" onclick="window.location.href = '{{action("PersonnalisationController@EditTheme_GET", $mainTheme->rowid)}}'"><i class="fa fa-pencil" aria-hidden="true"></i></button>
	                        <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="Modifier la section à propos" onclick="window.location.href = '{{action("ParametragesController@EditAPropos_GET", $mainTheme->rowid)}}'"><i class="fa fa-info-circle" aria-hidden="true"></i></button>    
	                        <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="Modifier la section FAQ" onclick="window.location.href = '{{action("ParametragesController@EditFAQ_GET", $mainTheme->rowid)}}'"><i class="fa fa-question" aria-hidden="true"></i></button>
                            <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="Modifier les conditions d'utilisation et les politiques" onclick="window.location.href = '{{action("ParametragesController@EditTermsofuse_GET", $mainTheme->rowid)}}'"><i class="fa fa-gavel" aria-hidden="true"></i></button>                      
	                    </div>
	                </div>
                   
                    
                </div>
            </div>
        @endif

            @include('parametrages._listetheme')
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

</script>
@endsection