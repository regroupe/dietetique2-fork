<form>
    <table class='table table-md table-striped table-responsive-sm'>

            <thead class='thead-dark account-head'>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-secondary" onclick="location.href='{{action("PersonnalisationController@CreateTheme_GET")}}'">Créer un thème</button>
                    </th>
                </tr>
                <tr>
                    <th>Thème</th>

                    <th>
                    <div class="text-right">
                        Options
                    </div>
                    </th>
                </tr>
            </thead>
                @foreach($persos as $theme)
                        <tr class='account-row'>
                            <td>{{$theme->rowid}} - {{$theme->nom_theme}}</td>
                            <td>
                            <div class="text-right">
                                
                            @if ($mainTheme != null)
                                @if ($theme->rowid != $mainTheme->rowid)
                                    <button type="button" class="btn btn-outline-success" data-toggle="tooltip" data-placement="top" title="Activer" onclick="window.location.href = '{{action("PersonnalisationController@ActivateTheme_GET", $theme->rowid)}}'"><i class="fa fa-check-circle-o" aria-hidden="true"></i></button>
                                @endif
                            @else
                                <button type="button" class="btn btn-outline-success" data-toggle="tooltip" data-placement="top" title="Activer" onclick="window.location.href = '{{action("PersonnalisationController@ActivateTheme_GET", $theme->rowid)}}'"><i class="fa fa-check-circle-o" aria-hidden="true"></i></button>
                            @endif
                            <button type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Modifier le thème" onclick="window.location.href = '{{action("PersonnalisationController@EditTheme_GET", $theme->rowid)}}'"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="Modifier la section à propos" onclick="window.location.href = '{{action("ParametragesController@EditAPropos_GET", $theme->rowid)}}'"><i class="fa fa-info-circle" aria-hidden="true"></i></button>
							<button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="Modifier la section FAQ" onclick="window.location.href = '{{action("ParametragesController@EditFAQ_GET", $mainTheme->rowid)}}'"><i class="fa fa-question" aria-hidden="true"></i></button>                            
                            <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="Modifier les conditions d'utilisation et les politiques" onclick="window.location.href = '{{action("ParametragesController@EditTermsofuse_GET", $mainTheme->rowid)}}'"><i class="fa fa-gavel" aria-hidden="true"></i></button>                      
                            @if ($theme->rowid != $mainTheme->rowid)
                            <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-placement="top" title="Supprimer" onclick="window.location.href = '{{action("PersonnalisationController@DeleteTheme_GET", $theme->rowid)}}'"><i class="fa fa-times"     aria-hidden="true"></i></button>
                             @endif
                            </div>
                            </td> 
                        </tr>
                @endforeach
                <tfoot class='table-dark'>
                    <tr>

                    </tr>
                    <tr>
                            <td class="align-middle" colspan="5">{{$persos->links()}}</td>
                    </tr>
                </tfoot>
    </table>
</form>
