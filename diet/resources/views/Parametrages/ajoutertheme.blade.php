
<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $carrousel_data = json_decode(Controllers\PersonnalisationController::listsLinkName());
?>

@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/personnalisation.css') }}">
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Ajouter un thème</h1>
    </div>
</div>

{{ Breadcrumbs::render('Themes_creer') }}

<div class="container">
    <form action="{{action('PersonnalisationController@SaveTheme_POST')}}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
        <input type="hidden" name="rowid" value="0">

       @include('parametrages._createedittheme')
	</form>
   
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/theme.js') }}"></script>
@endsection