<div class="row">
    <div class="col-12">
        <div class="accordion" id="accordion_theme">

            <!--Générals***********************************************************-->

            <div class="card">
                <a class="card-header btn btn-tab btn-outline-primary " id="heading0" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse0">
                    Générals
                </a>
                <div id="collapse0" class="collapse show" aria-labelledby="heading0" data-parent="#accordion_theme">
                    <div class="head-padding"></div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label for='nom_theme'>Nom du thème</label>
                                    <input class="form-control" id='nom_theme' name='nom_theme' type='text' value='{{(old('nom_theme')) ? old('nom_theme') : $theme->nom_theme}}'>
                                    <div class="text-danger">{{ $errors->first('nom_theme') }}</div>
                                </div> 

                                <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                    <label for='nom_article'>Nom commun de vente</label>
                                    <input class="form-control" id='nom_article' name='nom_article' type='text' value='{{(old('nom_article')) ? old('nom_article') : $theme->nom_article}}' placeholder='produit'>
                                    <div class="text-danger">{{ $errors->first('nom_article') }}</div>
                                </div> 

                                <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                    <label for='nom_article_plur'>Nom au pluriel</label>
                                    <input class="form-control" id='nom_article_plur' name='nom_article_plur' type='text' value='{{(old('nom_article_plur')) ? old('nom_article_plur') : $theme->nom_article_plur}}' placeholder='produit(s)'>
                                    <div class="text-danger">{{ $errors->first('nom_article_plur') }}</div>
                                </div> 

                                <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                    <label for='affichage_decimal'>Notation de la décimale<span class="small-note">*</span></label>
                                    <input class="form-control" id='affichage_decimal' name='affichage_decimal' type='text' value='{{(old('affichage_decimal')) ? old('affichage_decimal') : $theme->affichage_decimal}}' placeholder=','>
                                    <div class="text-danger">{{ $errors->first('affichage_decimal') }}</div>
                                </div> 

                                <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                    <label for='affichage_separator'>Notation du séparateur de milliers<small class="small-note">*</small></label>
                                    <input class="form-control" id='affichage_separator' name='affichage_separator' type='text' value='{{(old('affichage_separator')) ? old('affichage_separator') : $theme->affichage_separator}}' placeholder='_'>
                                    <div class="text-danger">{{ $errors->first('affichage_separator') }}</div>
                                </div> 

                                <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                    <div class="row align-items-center justify-content-center">
                                        <div>16x16 pixels, format .ico</div>
                                    </div>
                                    <div class="row align-items-center justify-content-center">
                                        
                                        <div  id="icon-preview" class="icon-preview" style="background-image: url({{ $theme->icon ? Storage::url($theme->icon) : '/img/preview.png' }})"></div>
                                    </div>
                                    
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="hidden" id="icon_dirty" name="icon_dirty" value="0">
                                            <input type="file" class="custom-file-input" id="icon" name="icon">
                                            <label class="custom-file-label" for="icon">{{ $theme->icon ? basename($theme->icon) : 'Choisir une image' }}</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="remove-icon" class="btn btn-secondary" type="button" onclick="removeImageFile('#icon', '#icon-preview', 'Choisir une icône')"><i class="fa fa-remove"></i></button>
                                        </div>
                                    </div>
                                    <div class="text-danger">{{ $errors->first('icon') }}</div>
                                </div>

                                <div class="form-group col-sm-12 col-md-6 col-lg-6" style="padding-top:23px">
                                        <label for='titre_onglet'>Titre de l'onglet</label>
                                        <input class="form-control" id='titre_onglet' name='titre_onglet' type='text' value='{{(old('titre_onglet')) ? old('titre_onglet') : $theme->titre_onglet}}'>
                                        <div class="text-danger">{{ $errors->first('titre_onglet') }}</div>
                                </div> 
                                

                            </div>
                            <small class="small-note">* Saisir « _ » pour utiliser un espace.</small>

                        </div>
                    <div class="foot-padding"></div>
                </div>
            </div>
            
            <!--Logo***********************************************************-->

            <div class="card">
                <a class="card-header btn btn-tab btn-outline-primary collapsed" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Logo
                </a>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_theme">
                    <div class="head-padding"></div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row align-items-center justify-content-center">
                                <div>200x35 pixels</div>
                            </div>
                            <div class="row align-items-center justify-content-center">
                                
                                <div  id="logo-preview" class="logo-preview" style="background-image: url({{ $theme->logo ? Storage::url($theme->logo) : '/img/preview.png' }})"></div>
                            </div>
                            
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="hidden" id="logo_dirty" name="logo_dirty" value="0">
                                    <input type="file" class="custom-file-input" id="logo" name="logo">
                                    <label class="custom-file-label" for="logo">{{ $theme->logo ? basename($theme->logo) : 'Choisir une image' }}</label>
                                </div>
                                <div class="input-group-append">
                                    <button id="remove-logo" class="btn btn-secondary" type="button" onclick="removeImageFile('#logo', '#logo-preview', 'Choisir une image')"><i class="fa fa-remove"></i></button>
                                </div>
                            </div>
                            <div class="text-danger">{{ $errors->first('logo') }}</div>
                        </div>

                    </div>
                    <div class="foot-padding"></div>
                </div>
            </div>

             <!--Bannières***********************************************************-->

            <div class="card">
                <a class="card-header btn btn-tab btn-outline-primary collapsed" id="headingTwo"data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                    Bannières
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion_theme">
                    <div class="head-padding"></div>
                    <div class="card-body">
                        
                            <div class="form-group">

                                <div class="card bg-dark">
                                    <div class="card-header">
                                        <h5 style="color:#fff">Bannières</h5>
                                    </div>
                                </div>

                                    <div class="row align-items-center justify-content-center">
                                         
                                         <div class="banniere-preview" id="banniere-preview" style="background-image: url({{ $theme->banniere ? Storage::url($theme->banniere) : '/img/preview.png' }})"></div>
                                    </div>
                                    
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="hidden" id="banniere_dirty" name="banniere_dirty" value="0">
                                            <input type="file" class="custom-file-input" id="banniere" name="banniere">
                                            <label class="custom-file-label" for="banniere">{{ $theme->banniere ? basename($theme->banniere) : 'Choisir une image' }}</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="remove-banniere" class="btn btn-secondary" type="button" onclick="removeImageFile('#banniere', '#banniere-preview', 'Choisir une image')"><i class="fa fa-remove"></i></button>
                                        </div>
                                    </div>
                                    <div class="text-danger">{{ $errors->first('banniere') }}</div>
                            </div>

                    </div>
                    <div class="foot-padding"></div>
                </div>
            </div>

             <!--Footer***********************************************************-->

            <div class="card">
                    <a class="card-header btn btn-tab btn-outline-primary collapsed" id="heading3" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapseOne">
                        Bas de page
                    </a>
                    <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion_theme">
                        <div class="card-body">
                                                    
                               <div class="card bg-dark">
                                    <div class="card-header">
                                        <h5 style="color:#fff">Aperçu</h5>
                                    </div>
                                </div>
                                <div class="card border-light">
                                    <div class="card-body">
                                        <div class="row footer-preview">
                                            <div class="col-12 col-md-3 text-center text-md-left">
                                                    <div hidden id="logo_footer" class="logo-preview" style="background-image: url({{ $theme->logo ? Storage::url($theme->logo) : '/img/preview.png' }})"></div>
                                                <p id="preview_desc"></p>
                                            </div>
                                            <div class="col-12 col-md-5 col-lg-5 offset-md-4 offset-lg-4 text-center text-md-right text-lg-right">
                                                <h5 id="preview_organ"></h5>
                                                <p>
                                                    <span id="preview_address"></span><br>
                                                    <span id="preview_city"></span><br>
                                                    <span id="preview_cp"></span><br>
                                                    <span id="preview_phone"></span><br>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row footer-preview">
                                            <div class="col-12 col-md-12 col-lg-12 text-center">
                                                <small class="d-block text-muted" id="preview_copy"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                <label for='footer_desc'>Description</label>
                                                <textarea class="form-control" id='footer_desc' name='footer_desc' type='text' >{{(old('footer_desc')) ? old('footer_desc') : $theme->footer_desc}}</textarea>
                                                <div class="text-danger">{{ $errors->first('footer_desc') }}</div>
                                        </div> 
                                </div>

                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                <label for='footer_entreprise'>Nom de l'organisation</label>
                                                <input class="form-control" id='footer_entreprise' name='footer_entreprise' type='text' value='{{(old('footer_entreprise')) ? old('footer_entreprise') : $theme->footer_entreprise}}'>
                                                <div class="text-danger">{{ $errors->first('footer_entreprise') }}</div>
                                        </div> 
                                </div>

                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                <label for='footer_address'>Adresse</label>
                                                <input class="form-control" id='footer_address' name='footer_address' type='text' value='{{(old('footer_address')) ? old('footer_address') : $theme->footer_address}}'>
                                                <div class="text-danger">{{ $errors->first('footer_address') }}</div>
                                        </div> 
                                </div>

                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                <label for='footer_ville'>Ville/Province/Pays</label>
                                                <input class="form-control" id='footer_ville' name='footer_ville' type='text' value='{{(old('footer_ville')) ? old('footer_ville') : $theme->footer_city}}'>
                                                <div class="text-danger">{{ $errors->first('footer_ville') }}</div>
                                        </div> 
                                        
                                </div>

                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                                <label for='footer_code_postal'>Code Postal</label>
                                                <input class="form-control" id='footer_code_postal' name='footer_code_postal' type='text' value='{{(old('footer_code_postal')) ? old('footer_code_postal') : $theme->footer_code_postal}}'>
                                                <div class="text-danger">{{ $errors->first('footer_code_postal') }}</div>
                                        </div> 
                                        <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                                <label for='footer_telephone'>Téléphone</label>
                                                <input class="form-control" id='phone_input' name='footer_telephone' type='text' value='{{(old('footer_telephone')) ? old('footer_telephone') : $theme->footer_telephone}}'>
                                                <div class="text-danger">{{ $errors->first('footer_telephone') }}</div>
                                        </div> 
                                </div>

                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                <label for='footer_copyright'>Ligne de bas de page (Copyright)</label>
                                                <input class="form-control" id='footer_copyright' name='footer_copyright' type='text' value='{{(old('footer_copyright')) ? old('footer_copyright') : $theme->footer_copyright}}'>
                                                <div class="text-danger">{{ $errors->first('footer_copyright') }}</div>
                                        </div> 
                                </div>

                                <button class="btn btn-secondary" type="button" onclick="previewFooter()">Modifier l'aperçu</button>
                        </div>
                        <div class="foot-padding"></div>
                    </div>
            </div>

             <!--Caroussel***********************************************************-->

              <div class="card">
                <a class="card-header btn btn-tab btn-outline-primary collapsed" id="heading4"data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapseOne">
                    Caroussel (Accueil)
                </a>
                <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion_theme">
                    <div class="head-padding"></div>
                    <div class="card-body">
                        
                            <div class="form-group">

                               <div class="card bg-dark">
                                    <div class="card-header">
                                        <h5 style="color:#fff">Image #1</h5>
                                    </div>
                                </div>

                                    <div class="row align-items-center justify-content-center">
                                         <div class="car-preview" id="car1-preview" style="background-image: url({{ $theme->car1 ? Storage::url($theme->car1) : '/img/preview.png' }})"></div>
                                    </div>
                                    
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="hidden" id="car1_dirty" name="car1_dirty" value="0">
                                            <input type="file" class="custom-file-input" id="car1" name="car1">
                                            <label class="custom-file-label" for="car1">{{ $theme->car1 ? basename($theme->car1) : 'Choisir une image' }}</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="remove-car1" class="btn btn-secondary" type="button" onclick="removeImageFile('#car1', '#car1-preview', 'Choisir une image')"><i class="fa fa-remove"></i></button>
                                        </div>
                                    </div>
                                    <div class="text-danger">{{ $errors->first('car1') }}</div>
                                    <br>
                                    <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-8 col-lg-8">
                                                <label for='car1_msg'>Message du lien</label>
                                                <input class="form-control" id='car1_msg' name='car1_msg' type='text' value='{{(old('car1_msg')) ? old('car1_msg') : $theme->car1_msg}}'>
                                                <div class="text-danger">{{ $errors->first('car1_msg') }}</div>
                                        </div> 
                                        <div class="form-group col-sm-12 col-md-4 col-lg-4">
                                                <label for='car1_link'>Lien</label>
                                                <select class="form-control" id="car1_link" name="car1_link">
                                                    @foreach ($carrousel_data[0] as $index => $val)
                                                        <option value="{{$val}}"
                                                        @if (old('car1_link'))
                                                            @if (old('car1_link') == $val)
                                                                selected
                                                            @endif
                                                        @elseif ($theme->car1_link == $val)
                                                            selected
                                                        @endif
                                                        >{{$carrousel_data[1][$index]}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="text-danger">{{ $errors->first('car1_link') }}</div>
                                        </div> 
                                </div>
                            </div>

                            <div class="head-padding"></div>

                            <div class="form-group">

                                <div class="card bg-dark">
                                    <div class="card-header">
                                        <h5 style="color:#fff">Image #2</h5>
                                    </div>
                                </div>

                                    <div class="row align-items-center justify-content-center">
                                         <div class="car-preview" id="car2-preview" style="background-image: url({{ $theme->car2 ? Storage::url($theme->car2) : '/img/preview.png' }})"></div>
                                    </div>
                                    
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="hidden" id="car2_dirty" name="car2_dirty" value="0">
                                            <input type="file" class="custom-file-input" id="car2" name="car2">
                                            <label class="custom-file-label" for="car2">{{ $theme->car2 ? basename($theme->car2) : 'Choisir une image' }}</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="remove-car2" class="btn btn-secondary" type="button" onclick="removeImageFile('#car2', '#car2-preview', 'Choisir une image')"><i class="fa fa-remove"></i></button>
                                        </div>
                                    </div>
                                    <div class="text-danger">{{ $errors->first('car2') }}</div>
                                    <br>
                                    <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-8 col-lg-8">
                                                <label for='car2_msg'>Message du lien</label>
                                                <input class="form-control" id='car2_msg' name='car2_msg' type='text' value='{{(old('car2_msg')) ? old('car2_msg') : $theme->car2_msg}}'>
                                                <div class="text-danger">{{ $errors->first('car2_msg') }}</div>
                                        </div> 
                                        <div class="form-group col-sm-12 col-md-4 col-lg-4">
                                                <label for='car2_link'>Lien</label>
                                                <select class="form-control" id="car2_link" name="car2_link">
                                                    @foreach ($carrousel_data[0] as $index => $val)
                                                        <option value="{{$val}}"
                                                        @if (old('car2_link'))
                                                            @if (old('car2_link') == $val)
                                                                selected
                                                            @endif
                                                        @elseif ($theme->car2_link == $val)
                                                            selected
                                                        @endif
                                                        >{{$carrousel_data[1][$index]}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="text-danger">{{ $errors->first('car2_link') }}</div>
                                        </div> 
                                </div>
                            </div>

                            <div class="head-padding"></div>

                            <div class="form-group">


                                <div class="card bg-dark">
                                    <div class="card-header">
                                        <h5 style="color:#fff">Image #3</h5>
                                    </div>
                                </div>
                                

                                    <div class="row align-items-center justify-content-center">
                                        <div class="car-preview" id="car3-preview" style="background-image: url({{ $theme->car3 ? Storage::url($theme->car3) : '/img/preview.png' }})"></div>
                                    </div>
                        
                            
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="hidden" id="car3_dirty" name="car3_dirty" value="0">
                                            <input type="file" class="custom-file-input" id="car3" name="car3">
                                            <label class="custom-file-label" for="car3">{{ $theme->car3 ? basename($theme->car3) : 'Choisir une image' }}</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="remove-car3" class="btn btn-secondary" type="button" onclick="removeImageFile('#car3', '#car3-preview', 'Choisir une image')"><i class="fa fa-remove"></i></button>
                                        </div>
                                    </div>
                                    <div class="text-danger">{{ $errors->first('car3') }}</div>
                                    <br>
                                    <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-8 col-lg-8">
                                                <label for='car3_msg'>Message du lien</label>
                                                <input class="form-control" id='car3_msg' name='car3_msg' type='text' value='{{(old('car3_msg')) ? old('car3_msg') : $theme->car3_msg}}'>
                                                <div class="text-danger">{{ $errors->first('car3_msg') }}</div>
                                        </div> 
                                        <div class="form-group col-sm-12 col-md-4 col-lg-4">
                                                <label for='car3_link'>Lien</label>
                                                <select class="form-control" id="car3_link" name="car3_link">
                                                    @foreach ($carrousel_data[0] as $index => $val)
                                                        <option value="{{$val}}"
                                                        @if (old('car3_link'))
                                                            @if (old('car3_link') == $val)
                                                                selected
                                                            @endif
                                                        @elseif ($theme->car3_link == $val)
                                                            selected
                                                        @endif
                                                        >{{$carrousel_data[1][$index]}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="text-danger">{{ $errors->first('car3_link') }}</div>
                                        </div> 
                                </div>
                            </div>
                            <div class="head-padding"></div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <button class="btn btn-outline-success btn-lg btn-block" type="submit">Enregistrer</button><br>
    </div>

    <div class="col-md-6">
        <button class="btn btn-outline-danger btn-lg btn-block" type="button" onclick="window.location.href = '{{ action('PersonnalisationController@Index') }}'">Annuler</button>
    </div>
</div>