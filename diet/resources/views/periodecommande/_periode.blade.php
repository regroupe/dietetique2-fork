<?php
    use App\Http\Controllers;
    use Carbon\Carbon;
    $dateDebut = Controllers\PeriodeController::DaysLeft($period->date_debut, false);
    $dateFin = Controllers\PeriodeController::DaysLeft($period->date_fin, false);
?>
<div class='row'>
    <div class='col-sm-12 col-md-12 col-lg-12'>
        <div class="periode-container">
            <a href="{{action('PeriodeController@EditPeriode_GET', $period->rowid)}}">
            <h5>Période #{{$period->rowid}}</h5>
            <div class='start-in'>
                @if ($dateDebut != 0)
                    Débute dans {{$dateDebut}} jours.
                @else Débute dans moins d'une journée.
                @endif
            </div>

            <div class='row progress-date'>
                    <div class='col-sm-5 col-md-3 col-lg-3'>
                            <div>{{Carbon::parse($period->date_debut)->format('d M Y H:i')}}</div>
                    </div>
                    <div class='col-sm-2 col-md-6 col-lg-6'></div>
                    <div class='col-sm-5 col-md-3 col-lg-3'>
                            <div style='text-align:right;'>{{Carbon::parse($period->date_fin)->format('d M Y H:i')}}</div>
                    </div>
            </div>
            </a>
        </div>    
    </div>
</div>