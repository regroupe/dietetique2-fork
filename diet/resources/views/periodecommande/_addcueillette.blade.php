<div class="modal fade" id="add_cueillette" tabindex="-1" role="dialog" aria-labelledby="add_cueillette-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                                <h5 class="modal-title" id="add_cueillette-title"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">
                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                <label for="emplacement">Emplacement</label>
                                                <input id='emplacement' type="text" name='emplacement' class="form-control input-emplacement" value=""/>
                                                <div class="text-danger err-emp"></div>
                                        </div>
                                </div>

                                <div class="form-row">
                                        <div class="form-group col-sm-9 col-md-9 col-lg-9">
                                                <label for="date_debut_c">Date de début</label>
                                                <input id="date_debut_c" type="date" name='date_debut_c' class="form-control input-debut" value=""/>
                                                <div class="text-danger err-date_debut"></div>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                                <label for="time_debut_c">Heure</label>
                                                <input id="time_debut_c" name='time_debut_c' type="time" class="form-control input-debut-time" value=""/>
                                                <div class="text-danger err-time_debut"></div>
                                        </div>
                                </div>

                                 <div class="form-row">
                                        <div class="form-group col-sm-9 col-md-9 col-lg-9">
                                                <label for="date_fin_c">Date de fin</label>
                                                <input id="date_fin_c" type="date" name='date_fin_c' class="form-control input-fin" value=""/>
                                                <div class="text-danger err-date_fin"></div>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                                <label for="time_fin_c">Heure</label>
                                                <input id="time_fin_c" name='time_fin_c' type="time" class="form-control input-fin-time" value=""/>
                                                <div class="text-danger err-time_fin"></div>
                                        </div>
                                </div>
                        </div>

                        <input type="hidden" value="" class="id-cueil"> 

                        <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="button" onclick="ajouter(this)" class="btn btn-primary modal-button"></button>
                        </div>
                </div>
        </div>
</div>