
@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/calendrier.css') }}">
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Calendrier</h1>
    </div>
</div>

{{ Breadcrumbs::render('Calendrier') }}

<div class="container">
    @include('periodecommande._activeperiod')
    <div class="row">
        @include('periodecommande._calendrier')
    </div>
</div>
@endsection

@section('scripts')
@endsection