
{{ csrf_field() }}
<div id="calendar"></div>
<div class="col-12 pt-4">
    <table class="table table-sm">
        <thead>
            <tr>
                <th scope="col">Table des matières</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><i class="fa fa-clock-o" aria-hidden="true"></i> Pour voir les heures d'une période, cliquez sur l'évènement désiré du calendrier.</td>
            </tr>
            <tr>
                <td class="table-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Période de commande</td>
            </tr>
            <tr>
                <td class="table-success"><i class="fa fa-truck" aria-hidden="true"></i> Période de cueillette</td>
            </tr>
        </tbody>
    </table>
</div>
<div id="calendar-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="calendar-modal-title" class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="calendar-modal-info" class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>
<script>

    $(function() {

        $('#calendar').fullCalendar({
            locale: 'fr-ca',
            height: 'auto',
            eventClick: function(calEvent, jsEvent, view) {

                $('#calendar-modal-title').text(calEvent.title);
                $('#calendar-modal-info').html(
                    "<span><strong>Date de début:</strong> " + moment(calEvent.start).format("D MMMM YYYY, h:mm:ss a") + "</span></br>" +
                    "<span><strong>Date de fin:</strong> " + moment(calEvent.end).format("D MMMM YYYY, h:mm:ss a") + "</span>"
                );
                $('#calendar-modal').modal('toggle');
            },
            viewRender: function(view, element) {

                $('#calendar').fullCalendar('removeEvents');
                $('.fc-prev-button').addClass('fc-state-disabled');
                $('.fc-next-button').addClass('fc-state-disabled');

                $.ajax({
                    headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                    type: "post",
                    url: "{{ action('PeriodeController@GetCalendar') }}",
                    data: {
                        "start": moment(view.start).format("YYYY-MM-DD HH:mm:ss"),
                        "end": moment(view.end).format("YYYY-MM-DD HH:mm:ss")
                    },
                    success: function(response) {

                        if (response.periods !== undefined) {
                            var orderPeriods = [];
                            response.periods.forEach(period => {
                                orderPeriods.push({
                                    title: "Période de commande", 
                                    start: period.date_debut, 
                                    end: period.date_fin,
                                    color: '#cce5ff',
                                    borderColor: '#b8daff'
                                });
                            });
                            $('#calendar').fullCalendar('addEventSource', orderPeriods);
                        }

                        if (response.pickups !== undefined) {
                            var pickupPeriods = [];
                            response.pickups.forEach(period => {
                                pickupPeriods.push({
                                    title: "Période de cueillette " + period.emplacement, 
                                    start: period.date_debut, 
                                    end: period.date_fin,
                                    color: '#d4edda',
                                    borderColor: '#c3e6cb'
                                });
                            });
                            $('#calendar').fullCalendar('addEventSource', pickupPeriods);
                        }

                        $('.fc-prev-button').removeClass('fc-state-disabled');
                        $('.fc-next-button').removeClass('fc-state-disabled');
                    },
                    error: function() {
                        $('.fc-prev-button').removeClass('fc-state-disabled');
                        $('.fc-next-button').removeClass('fc-state-disabled');
                    }
                });
            }
        })
    });
</script>