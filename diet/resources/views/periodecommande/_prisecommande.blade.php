
<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers\CommandeController;
    use App\Http\Controllers\PeriodeController;
?>

<tbody>
    <?php foreach ($commandes as $commande) : $order = CommandeController::parseAdresse($commande); ?>
    <tr>
        <td class="align-middle">
            <b>{{ $order->nom_client }}</b>
            <br>
            {{ $order->adresse }} {{ $order->no_app }}
            <br>
            <span class="phone">{{ $order->telephone }}</span> {{ $order->poste }}
            <br>
            {{ PeriodeController::ParseDateLocale($commande->date_commande) }}
            <br>
            <b>{{ count($commande->commande_articles) }} article(s)</b> - {{ $commande->total }} $
        </td>

        <td class="align-middle text-right d-none d-sm-table-cell">
            {{ $commande->prise ? 'Oui' : 'Non' }}
        </td>

        <td class="align-middle text-right d-table-cell d-sm-none" colspan="2">
            {{ $commande->prise ? 'Oui' : 'Non' }}
        </td>

        <td class="align-middle text-right d-none d-sm-table-cell">
            @if (session('admin')->super || session('admin')->master || session('admin')->take_order)
            <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="Détails" onclick="window.location.href = '{{action("CommandeController@ViewAdmin_GET", [$order->rowid_client, $order->rowid])}}'"><i class="fa fa-eye" aria-hidden="true"></i></button>
            <?php if ($commande->prise) : ?>
                <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target=".bd-example-modal-lg" onclick="prepareCanvas({{ $commande->rowid }}, 0)"><i class="fa fa-ban" aria-hidden="true"></i></button>
            <?php else : ?>
                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target=".bd-example-modal-lg" onclick="prepareCanvas({{ $commande->rowid }}, 1)"><i class="fa fa-check-circle-o" aria-hidden="true"></i></button>
            <?php endif; ?>
            @endif
        </td>
    </tr>
    <tr class="d-table-row d-sm-none">
        <td class="align-middle" colspan="2">
            <div class="row">
                @if (session('admin')->super || session('admin')->master || session('admin')->take_order)
                <div class="col-6">
                    <button type="button" class="btn btn-outline-info btn-block" data-toggle="tooltip" data-placement="top" title="Détails" onclick="window.location.href = '{{action("CommandeController@ViewAdmin_GET", [$order->rowid_client, $order->rowid])}}'"><i class="fa fa-eye" aria-hidden="true"></i></button>
                </div>
                <div class="col-6">
                    <?php if ($commande->prise) : ?>
                        <button type="button" class="btn btn-outline-danger btn-block" data-toggle="modal" data-target=".bd-example-modal-lg" onclick="prepareCanvas({{ $commande->rowid }}, 0)"><i class="fa fa-ban" aria-hidden="true"></i></button>
                    <?php else : ?>
                        <button type="button" class="btn btn-outline-success btn-block" data-toggle="modal" data-target=".bd-example-modal-lg" onclick="prepareCanvas({{ $commande->rowid }}, 1)"><i class="fa fa-check-circle-o" aria-hidden="true"></i></button>
                    <?php endif; ?>
                </div>
                @endif
            </div>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
<tfoot class="table-dark">
    <tr>
        <td colspan="4">
            {{ $commandes->links() }}
        </td>
    </tr>
</tfoot>

<script>
    $(document).ready(function() {
        $(".phone").each(function() {
            $(this)[0].innerHTML = parsePhoneNumber($(this)[0].innerHTML);
        });
    });
</script>