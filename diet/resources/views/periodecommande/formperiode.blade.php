<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    use Carbon\Carbon;
    use Carbon\CarbonInterface;
?>

@extends('layouts.master')
@section('styles')
<link rel="stylesheet" href="{{ asset('css/periode.css') }}">
@endsection
@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
   @if ($period != null)
        <h1 class="display-4">Période #{{$period->rowid}}</h1>
   @else
        <h1 class="display-4">Ajouter une période</h1>
   @endif
   </div>
</div>

@if ($period != null)
        {{ Breadcrumbs::render('ModifierPeriode', $period) }}
@else
        {{ Breadcrumbs::render('AjouterPeriode') }}
@endif
<div class='container'>
        <div class='row'>

                @include('administrateur._side_menu')
                <div class='col-sm-12 col-md-9 col-lg-9'>
                        @if ($errors->has('error_msg'))
                        <div class="alert alert-danger">{{$errors->first('error_msg')}}</div>

                        @endif

                        @if ($period == null)
                                @include('periodecommande._ajouterperiode')
                        @elseif ($editdate == true)
                                @include('periodecommande._editperiode')
                        @else 
                                @include('periodecommande._editpicksonly')
                        @endif
                </div>
        </div>
</div>
</body>
@endsection
@section('scripts')
<script>
        var date_now = "{{Carbon::now('America/Toronto')->format('Y-m-d')}}";
        var time_now = "{{Carbon::now('America/Toronto')->format('H:i')}}";
</script>
<script src="{{ asset('js/editPeriode.js') }}"></script>
@endsection