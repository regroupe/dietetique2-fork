<div class="modal fade modal-del" id="delete_periode" tabindex="-1" role="dialog" aria-labelledby="delete_periode-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                            <h5 class="modal-title">Supprimer une période</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
                    <div class="modal-body">
                            <div>Voulez-vous supprimer cette période?</div>
                            <strong>Cette opération est irréversible.</strong>
                    </div>

                    <input type="hidden" value="" class="id-cueil"> 
                    
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                            <form action="{{action('PeriodeController@DeletePeriod', $period->rowid)}}" method='POST'>
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger modal-button submit-delete" data-dismiss="modal">Supprimer</button>
                            </form>
                        </div>
                    
            </div>
    </div>
</div>