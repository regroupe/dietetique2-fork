<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
?>

@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/periode.css') }}">
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Prise de commande - Période #{{$period->rowid}}</h1>
    </div>
</div>

{{ Breadcrumbs::render('PriseCommande', $period) }}

<div class='container'>
    <div class='row'>
        @include('administrateur._side_menu')
        <div id="partial" class="col-sm-12 col-md-9 col-lg-9">            
            <form id="search-list">
                <h4><i class="fa fa-search-plus" aria-hidden="true"></i> Recherche par nom du client</h4>

                <div class="form-group">
                    <div class='input-group'>
                        <input class="form-control" name="filter_search" value="{{ $req->filter_search }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-success" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Rechercher</button>
                        </div>
                    </div>
                </div>

                <h4><i class="fa fa-list" aria-hidden="true"></i> Liste des commandes</h4>

                <table class="table table-striped">
                    <thead class="thead-dark">
                        <th colspan="3">
                            <div class="text-right">
                                <label>Afficher</label>
                                <select name="filter_pages" class="search-input form-control" style="display: inline-block; max-width: 80px">
                                    <option value="10" {{ $req->filter_pages == 10 ? 'selected' : '' }}>10</option>
                                    <option value="25" {{ $req->filter_pages == 25 ? 'selected' : '' }}>25</option>
                                    <option value="50" {{ $req->filter_pages == 50 ? 'selected' : '' }}>50</option>
                                    <option value="100" {{ $req->filter_pages == 100 ? 'selected' : '' }}>100</option>
                                </select>
                            </div>
                        </th>
                    </thead>
                    <thead class="table-dark">
                        <tr>
                            <th scope="col">
                                <input class="sort-input form-check-input" type="checkbox" value="{{ $req->sort_name }}" name="sort_name" id="sort_name" style="display: none" data-filter="#filter-name" {{ $req->sort_name ? 'checked' : '' }}>
                                <label class="search-input-label form-check-label" for="sort_name">
                                    <i id="filter-name" class="fa fa-{{ $req->sort_name ? ($req->sort_name == 1 ? 'arrow-up' : 'arrow-down') : 'filter' }}" aria-hidden="true" data-intermediate-0="filter" data-intermediate-1="arrow-up" data-intermediate-2="arrow-down"></i> Commande
                                </label>
                            </th>
                            <th scope="col" class="text-right">
                                <input class="sort-input form-check-input" type="checkbox" value="{{ $req->filter_active }}" name="filter_active" id="filter_active" style="display: none" data-filter="#filter-active" {{ $req->filter_active ? 'checked' : '' }}>
                                <label class="search-input-label form-check-label" for="filter_active">
                                    <i id="filter-active" class="fa fa-{{ $req->filter_active ? ($req->filter_active == 1 ? 'check-circle-o' : 'ban') : 'filter' }}" aria-hidden="true" data-intermediate-0="filter" data-intermediate-1="check-circle-o" data-intermediate-2="ban"></i> Prise
                                </label>
                            </th>
                            <th scope="col" class="text-right d-none d-sm-table-cell">Options</th>
                        </tr>
                    </thead>
                    <?php echo $partial; ?>
                </table>
            </form>
        </div>
    </div>
</div>

@if (session('admin')->super || session('admin')->master || session('admin')->take_order)
<div id="signature-modal" class="modal bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Signature du client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div id="signature-wrapper">
                    <canvas id="signature-pad" class="signature-pad" width=800 height=200></canvas>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="clearCanvas()">Effacer</button>
                <button type="submit" class="btn btn-primary" onclick="pickupOrder()">Soumettre</button>
            </div>
        </div>
    </div>
</div>
@endif
@endsection

@section('scripts')
<script>
    var routes = {
        order: {
            index: "{{ action('PeriodeController@OrderPickup_GET', $period->rowid) }}",
            pickup: "{{ action('PeriodeController@OrderPickup_POST', $period->rowid) }}",
        }
    };
</script>
<script src="{{ asset('js/prisecommande.js') }}"></script>
@endsection