<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    use Carbon\Carbon;
    use Carbon\CarbonInterface;
    $periods = Controllers\PeriodeController::GetAllPeriodsAfterToday();
    $period_active = Controllers\PeriodeController::GetActivePeriod();
    $period_ecoule = Controllers\PeriodeController::GetLastFinishPeriod();
    
?>

@extends('layouts.master')
@section('styles')
<link rel="stylesheet" href="{{ asset('css/periode.css') }}">
@endsection
@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">Périodes de commande</h1>
   </div>
</div>

{{ Breadcrumbs::render('PeriodeCommande') }}

<div class='container'>
        <div class='row'>

                @include('administrateur._side_menu')
                <div class='col-sm-12 col-md-9 col-lg-9'>
                        <div class='row'>
                        <div class='col-sm-12 col-md-12 col-lg-12'>
                                
                                @if (session()->has('success'))
                                <div class="alert alert-success">
                                        <strong>{{session()->get('success')}}</strong>
                                </div>
                                @elseif (session()->has('error'))
                                <div class="alert alert-success">
                                        <strong>{{session()->get('error')}}</strong>
                                </div>
                                @endif


                                @if ($period_active != null)
                                <a href="{{action('PeriodeController@EditPeriode_GET', $period_active->rowid)}}">
                                <div class="jumbotron periode-active-container">
                                <?php
                                        $date_debut_today = Controllers\PeriodeController::DaysLeft($period_active->date_debut, true);
                                        $date_fin_today = Controllers\PeriodeController::DaysLeft($period_active->date_fin, true);
                                        $date_d_date_f = Controllers\PeriodeController::DaysLeft_2var($period_active->date_debut, $period_active->date_fin, true);
                                        
                                        if($date_fin_today == 0){
                                                $progress_days = "Termine dans moins d'une journée";
                                        }else{
                                                $progress_days = "Termine dans ".$date_fin_today." jours";
                                        }

                                        $progress = $date_debut_today / $date_d_date_f* 100;
                                        
                                ?>
                                
                                        <h3>Période de commande active (#{{$period_active->rowid}})</h3>
                                        <div class="progress" data-toggle="tooltip" data-placement="bottom" title="{{$progress_days}}">
                                                <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress >= 100 ? 'bg-success' : '' }}" style="width: {{ $progress }}%" role="progressbar" aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class='row progress-date'>
                                                <div class='col-sm-5 col-md-3 col-lg-3'>
                                                        <div>{{Controllers\PeriodeController::ParseDateLocale($period_active->date_debut)}}</div>
                                                </div>
                                                 <div class='col-sm-2 col-md-6 col-lg-6'></div>
                                                <div class='col-sm-5 col-md-3 col-lg-3'>
                                                        <div style='text-align:right;'>{{Controllers\PeriodeController::ParseDateLocale($period_active->date_fin)}}</div>
                                                </div>
                                        </div>
                                </div>
                                </a>
                                @endif


                                @if ($period_ecoule != null)
                                <a href="{{action('PeriodeController@EditPeriode_GET', $period_ecoule->rowid)}}">
                                    <div class="jumbotron periode-active-container">
                                        <h4>Dernière période écoulée (#{{$period_ecoule->rowid}})</h4>
                                        <div class="start-in">Terminée le {{Controllers\PeriodeController::ParseDateLocale($period_ecoule->date_fin)}}</div>
                                        <?php
                                                $picks = Controllers\PeriodeController::PeriodeCueillette($period_ecoule->rowid);
                                        ?>
                                        @if (count($picks) < 1)
                                                <strong class='text-danger'>Aucune période de cueillette définie.</strong>
                                        @else
                                                <h5>Cueillettes</h5>
                                                <ul>
                                                        @foreach ($picks as $p)
                                                                <li><i class="cueil">{{$p->emplacement}}</i> le <i class="cueil">{{$p->date_debut}}</i> jusqu'à <i class="cueil">{{$p->date_fin}}</i>.</li>
                                                        @endforeach
                                                </ul>
                                        @endif
                                    </div>
                                </a>
                                @endif
                                @if ($period_ecoule != null || $period_active != null)
                                @endif
                                <div class="card">
                                        <a href="{{action('PeriodeController@AjouterPeriode_GET')}}" class='btn btn btn-success'>Ajouter une période</a>
                                </div>
                                
                                <div class="card">
                                        <a href="{{action('PeriodeController@archive_GET')}}" class='btn btn btn-secondary'>Archive</a>
                                </div>
                                
                                @if (count($periods) >= 1)
                                <div class="card">
                                        <div class="card-header">
                                                <h4>Prochaine période(s) de commande</h4>
                                        </div>
                                        @foreach ($periods as $period)
                                                @if ($period != $period_active)
                                                @include('periodecommande._periode')
                                                @endif
                                        @endforeach
                                </div>
                                @endif

                            
                                
                        </div>
                        </div>
                
                </div>
        </div>
</div>
</body>
@endsection
@section('scripts')
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
@endsection