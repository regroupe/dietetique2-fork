<?php
// Prepare server side variables for the view.
use App\Http\Controllers;
use Carbon\Carbon;

$emplacement = [];
$debut = [];
$fin = [];

if (old('c_emp') != null){
        foreach(old('c_emp') as $i =>$c)
        {
                $emplacement[] = $c;
                $debut[] = old('c_debut')[$i];
                $fin[] = old('c_fin')[$i];
        } 
}

echo ("<script>emplacement = ".json_encode($emplacement).";</script>\n");
echo ("<script>debut = ".json_encode($debut).";</script>\n");
echo ("<script>fin = ".json_encode($fin).";</script>");

?>
<h3>Dates de la période</h3>
    <form action="{{ action('PeriodeController@AjouterPeriode_POST') }}" method='POST'>
        {{ csrf_field() }}
        <div class="form-row">
                <div id="date_debut" class="form-group col-sm-9 col-md-9 col-lg-9">
                        <label for="date_debut">Date de début</label>
                        <input type="date" name='date_debut' class="form-control" value="{{old('date_debut')}}"/>
                        <div class="text-danger">{{ $errors->first('date_debut') }}</div>
                </div>
                <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="time_debut">Heure</label>
                        <input id="time_debut" name='time_debut' type="time" class="form-control" value="{{old('time_debut')}}"/>
                        <div class="text-danger">{{ $errors->first('time_debut') }}</div>
                </div>
        </div>

        <div class="form-row">
                <div id="date_fin" class="form-group col-sm-9 col-md-9 col-lg-9">
                        <label for="date_fin">Date de fin</label>
                        <input type="date" name='date_fin' class="form-control" value="{{old('date_fin')}}"/>
                        <div class="text-danger">{{ $errors->first('date_fin') }}</div>
                </div>
                <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="time_fin">Heure</label>
                        <input id="time_fin" name='time_fin' type="time" class="form-control" value="{{old('time_fin')}}"/>
                        <div class="text-danger">{{ $errors->first('time_fin') }}</div>
                </div>
        </div>

        <div class="card" style="padding:10px">
                <h3>Périodes de cueillette</h3>
                <div id="cueillette_list" class="form-group">
                       
                </div>

                        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#add_cueillette" data-title="Ajouter une période de cueillette" data-type="Ajouter">
                                Ajouter une cueillette
                        </button>

        </div>

        <div style="padding-top:20px;">
                <button class="btn btn-primary" type="submit">Ajouter</button>
        </div>
</form>
@include('periodecommande._addcueillette')