<div class='row'>
    <div class='col-sm-12 col-md-12 col-lg-12'>
        <div class="periode-container">
                    <div class="row">
                        <div class="col-sm-12 col-md-9 col-lg-9">
                        <h5 style="color:black; padding-top:4px;" class="at-fourn{{$index}}">{{ $a->article->nom }}
                                        {{ $a->article->format_quantite }}{{ $a->article->format }}
                                        <?php if ($a->article->bio) : ?>
                                            <i class="fa fa-leaf" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Biologique"></i>
                                        <?php endif; ?>
                                        <?php if ($a->article->perissable) : ?>
                                            <i class="fa fa-apple" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Périssable"></i>
                                        <?php endif; ?>

                                        ({{$a->article->nom_marque}})
                                </h5>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3">
                                <button type="button" class="btn btn-outline-primary float-right btnprint" data-toggle="tooltip" data-placement="top" title="Modifier" onclick="window.location.href = '{{ action('ArticleController@Edit_GET', $a->article->rowid ) }}'"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                <button type="button" style="margin-right:2px" class="btn btn-outline-info float-right btnprint" data-toggle="tooltip" data-placement="top" title="Détails" onclick="window.location.href = '{{ action('BoutiqueController@View_GET', $a->article->rowid) }}'"><i class="fa fa-eye" aria-hidden="true"></i></button>
                        </div>
                    </div>

                    <div style="padding:2px";>

                        <?php
                        $progress = $a->quantite_commande / $a->article->quantite_minimum *100;

                        ?>

                        <div class="progress" data-toggle="tooltip" data-placement="bottom" title="{{floor($progress).'% de la quantité minimum.'}}">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated {{ $a->quantite_commande >= $a->article->quantite_minimum ? 'bg-success' : '' }}" style="width: {{ floor($progress) }}%" role="progressbar" aria-valuenow="{{ floor($progress) }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div style="color:darkslategrey" class="aq-fourn{{$index}}">Quantité commandée : {{ $a->quantite_commande }} / {{ $a->article->quantite_minimum }}
                        | Max : {{ $a->quantite_maximum }}</div>
                    </div>
        </div>    
    </div>
</div>