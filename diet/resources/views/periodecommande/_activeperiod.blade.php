<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $orderNextPeriod = Controllers\PeriodeController::GetNextPeriod();
    $orderPeriodActive = Controllers\PeriodeController::OrderPeriodActive();
    $orderPeriodEndDate = Controllers\PeriodeController::OrderPeriodEndDate();
?>

<?php if (!$orderPeriodActive) : ?>
    <div class="alert alert-warning" role="alert">
        <strong><i class="fa fa-lock" aria-hidden="true"></i> Attention!</strong> Il n'y a aucune période de commande active pour le moment. {{ $orderNextPeriod ? 'La prochaine période de commande débute le '.$orderNextPeriod->date_debut.'.' : '' }}</a>
    </div>
<?php else : ?>
    <div class="alert alert-info" role="alert">
        <strong><i class="fa fa-clock-o" aria-hidden="true"></i> Attention!</strong> La période de commande en cours prend fin le {{ $orderPeriodEndDate }}.</a>
    </div>
<?php endif; ?>