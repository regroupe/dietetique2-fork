@extends('layouts.master')
@section('styles')
<link rel="stylesheet" href="{{ asset('css/periode.css') }}">
@endsection
@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
   <h1 class="display-4">Progression des ventes (Période #{{$period->rowid}})</h1>
   </div>
</div>

{{ Breadcrumbs::render('ProgressionPeriode', $period) }}

<div class='container'>
        <div class='row'>

                @include('administrateur._side_menu')
                <div class='col-sm-12 col-md-9 col-lg-9'>
                        <div class='row'>
                            <div class='col-sm-12 col-md-12 col-lg-12'>
                                <div class='card'>
                                        <a class='btn btn-success' href="{{action('PeriodeController@EditPeriode_GET', $period->rowid)}}">Dates et périodes de cueillette</a>
                                </div>

                                <hr style="padding:5px;">

                                

                                    @if (count($articles) > 0)
                                    <button class="btn btn-secondary mb-3"  type="button" data-toggle="tooltip" data-placement="top" title="Imprimer" onclick="printSynthese()"><i class="fa fa-print"></i> Imprimer</button>
                                    <div class="card" id="print_progress">
                                        
                                        <?php
                                            $last_four = '';
                                            $index = -1;
                                        ?>
                                        
                                        @foreach ($articles as $a)
                                        @if ($a->article->fournisseur->nom_fournisseur != $last_four)
                                        <div class="card-header bg-dark">
                                            <h4 style='color:#EEEEEE;' class="fournisseur-head">{{$a->article->fournisseur->nom_fournisseur}}</h4>
                                        </div>
                                        <?php
                                            $last_four = $a->article->fournisseur->nom_fournisseur;
                                            $index++;
                                        ?>
                                        @endif
                                            @include('periodecommande._article')
                                        @endforeach
                                    </div>
                                    <button class="btn btn-secondary mt-3"  type="button" data-toggle="tooltip" data-placement="top" title="Imprimer" onclick="printSynthese()"><i class="fa fa-print"></i> Imprimer</button>
                                    @else
                                        Aucun article n'a encore été commandé.
                                    @endif
                            </div>
                        </div>
                
                </div>
        </div>
</div>
</body>
@endsection
@section('scripts')
<script src="{{ asset('js/jquery.printarea.js') }}"></script>
<script src="{{ asset('js/detail_periode.js') }}"></script>
@endsection