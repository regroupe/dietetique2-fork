<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;

    $years = Controllers\PeriodeController::YearOfPeriods();

?>

@extends('layouts.master')
@section('styles')
<link rel="stylesheet" href="{{ asset('css/periode.css') }}">
@endsection
@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">Archive des périodes de commande</h1>
   </div>
</div>

{{ Breadcrumbs::render('ArchivePeriode') }}

<div class='container'>
        <div class='row'>

                @include('administrateur._side_menu')
                <div class='col-sm-12 col-md-9 col-lg-9'>
                <h4><i class="fa fa-search-plus" aria-hidden="true"></i> Critères de recherche</h4>
                        <form method=POST action='{{action("PeriodeController@RechercheArchive_POST")}}'>
                                {{ csrf_field() }}
                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12"> 
                                                <div class='input-group'>
                                                <select class='form-control' id='annee' name=annee>
                                                        <option value="Toutes les années">Toutes les années</option>
                                                        @foreach ($years as $y)
                                                                <option value="{{$y->year}}" {{ $annee == $y->year ? 'selected' : '' }}>{{$y->year}}</option>
                                                        @endforeach
                                                </select>
                                                <div class="input-group-append">
                                                        <button class="btn btn-outline-success" type="submit">Rechercher</button>
                                                </div>
                                                </div>
                                        </div>
                                </div>
                        </form>

                        <div class='row'>
                        <div class='col-sm-12 col-md-12 col-lg-12'>
                                @if (count($periods) >= 1)
                                <div class="card">
                                        <div class="card-header bg-dark">
                                        @if (isset($annee))
                                                <h4 style='color:#EEEEEE;'>Année {{$annee}}</h4>
                                        @else
                                                <h4 style='color:#EEEEEE;'>Ancienne(s) période(s) de commande</h4>
                                        @endif
                                        </div>
                                                @foreach ($periods as $period)
                                                        @include('periodecommande._archiveperiode')
                                                @endforeach
                                </div>
                                @else
                                        Aucune période trouvée.
                                @endif

                            <div>
                                {{$periods->links()}}
                            </div>
                                
                        </div>
                        </div>
                
                </div>
        </div>
</div>
</body>
@endsection
@section('scripts')
@endsection