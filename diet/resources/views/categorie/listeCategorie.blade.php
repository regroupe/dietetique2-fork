<table class='table table-sm table-striped'>
                    <thead class='thead-dark account-head'>
                        <th></th>
                        <th>Nom des catégories</th>
                        <th>Actif</th>
                    </thead>
                        @foreach($uneCategorie->All() as $uneCategorie)
                                <tr class='account-row'>
                                    <th data-id='{{$uneCategorie->nom_categorie}}'>
                                        <label class="ck-container">
                                            <input type=checkbox class='chkbox'>
                                            <span class="checkmark"></span>
                                        </label>
                                    </th>
                                    <td>{{$uneCategorie->nom_categorie}}</td>
                                    
                                    <td>@if($uneCategorie->actif)
                                        Oui
                                        @else Non
                                        @endif
                                    </td> 
                                </tr>
                        @endforeach
                        <tfoot class='table-dark'>
                            <tr>
                                    <td class="align-middle"colspan="2"></td>
                            </tr>
                        </tfoot>
</table>
<button>Modifier</button>
<button id='activer' data-tpe='activ-categorie' onclick='activer(this)'>Activer</button>
<button id='desactiver' data-tpe='desact-categorie' onclick='desactiver(this)'>Désactiver</button>