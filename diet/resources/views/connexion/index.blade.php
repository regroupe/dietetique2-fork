@extends('layouts.master')

@section('styles')
@endsection
<link rel="stylesheet" href="{{ asset('css/connexion.css') }}">
@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Connexion</h1>
    </div>
</div>

{{ Breadcrumbs::render('Connexion') }}

<div class='container'>
    <div class='row'>
        <div class='col-sm-12 col-md-6 col-lg-6'>
        
            <h2>Connexion</h2>
            
            @if ($errors->has('connexion'))
                <div class="alert alert-danger">
                    {{ $errors->first('connexion') }}
                </div>
            @endif


            <form action="{{ action('ConnexionController@Login') }}" method='POST'>
                {{ csrf_field() }}
                
                        <div class="form-group"> 
                            <label for='emailid'>Courriel</label>                       
                            <input class="form-control" placeholder="adresse@email.com" id='emailid' name='email'>
                            <div class="text-danger">{{ $errors->first('email') }}</div>
                        </div>

                        <div class="form-group"> 
                            <label for='passid'>Mot de passe</label>
                            <input class="form-control"  id='passid' name='pass' type='password'>
                            <div class="text-danger">{{ $errors->first('pass') }}</div>
                        </div>
                <button class="btn btn-primary" type='submit'>Connexion</button>
            </form>
        </div>

        <div class='col-sm-12 col-md-6 col-lg-6'>
            <h2>Inscription</h2>

            <form action="{{ action('ConnexionController@Register') }}" method='POST'>
                {{ csrf_field() }}
                <div class="form-group"> 
                    <label for='emailidR'>Courriel</label>
                    <input class="form-control" id='emailidR' name='emaili' placeholder="adresse@email.com">
                    <div class="text-danger">{{ $errors->first('emaili') }}</div>
                </div>

                <div class="form-group"> 
                    <label for='passidR'>Mot de passe</label>
                    <input class="form-control" id='passidR' name='passi' type='password'>
                    <div class="text-danger">{{ $errors->first('passi') }}</div>
                </div>

                <div class="form-group"> 
                    <label for='passidR2'>Confirmer le mot de passe</label>
                    <input class="form-control" id='passidR2' name='passi_confirmation' type='password'>
                    <div class="text-danger">{{ $errors->first('passi_confirmation') }}</div>
                </div>

                <button class='btn btn-primary' type='submit'>S'inscrire</button>
            </form>
        </div>
    </div>

</div>
</body>

@endsection

@section('scripts')
@endsection