@extends('includes.template')
@section('content')

<body>
<form action="{{ action('ConnexionController@EditUser', $unclient['rowid']) }}" method='POST'>
    <input type="hidden" name="_method" value="PUT">
    {{ csrf_field() }}
    <label for='emailid'>Email</label>
    <input id='emailid' name='email' value='{{$unclient['email']}}'>

    <label for='passid'>Mot de passe</label>
    <input id='passid' name='pass' value='{{$unclient['password']}}'>

    <button type='submit'>Edit</button>
</form>

</body>

@endsection