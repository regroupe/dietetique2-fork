@extends('includes.template')
@section('content')

<body>

<h2>Supprimer à partir d'un email</h2>

<form action="{{ action('ConnexionController@Delete') }}" method='POST'>
    {{ csrf_field() }}
    <label for='emailid'>Email</label>
    <input id='emailid' name='email'>

    <button type='submit'>Supprimer</button>
</form>

</body>

@endsection