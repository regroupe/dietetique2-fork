
@extends('layouts.master')

@section('styles')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/fancyCheckbox.css') }}">
@endsection

@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">Liste des marques</h1>
   </div>
</div>

{{ Breadcrumbs::render('Marque') }}

<div class='container'>

<div class='row'>

@include('administrateur._side_menu')

<div class='col-sm-12 col-md-9 col-lg-9'>

<div class= "Liste_Marques">

    @if (\Session::has('success'))
            <div class="alert alert-success">
                    <strong>{!! \Session::get('success') !!}</strong>
            </div>
    @endif


    <h4><i class="fa fa-plus"></i></i> Ajouter une nouvelle marque</h4>
    <form method=POST action='{{action("MarqueController@RegisterMarque_FORM")}}'>
        {{ csrf_field() }}
        <div class="form-group"> 
            <div class='input-group'>
                <input class='form-control' id='nom' name='nom_marque' placeholder='Nom de la marque' value='{{old('nom_marque')}}'>
                <div class="input-group-append">
                    <button class="btn btn-outline-primary" type="submit">Ajouter</button>
                </div>
            </div>
            <div class="text-danger">{{ $errors->first('nom_marque') }}</div>
        </div>
    </form>



    <h4><i class="fa fa-search-plus" aria-hidden="true"></i> Critères de recherche</h4>
    <form method=POST action='{{action("MarqueController@RechercheMarque_POST")}}'>
        {{ csrf_field() }}
        <div class="form-row">
            <div class="form-group col-sm-2 col-md-2 col-lg-2"> 
                        <select class='form-control' id='actif-rech' name=actif>
                            <option value=2 {{ $searchInfo[0] == 2 ? 'selected' : '' }}>Tous</option>
                            <option value=1 {{ $searchInfo[0] == 1 ? 'selected' : '' }}>Actif</option>
                            <option value=0 {{ $searchInfo[0] == 0 ? 'selected' : '' }}>Inactif</option>
                        </select>

            </div>

            <div class="form-group col-sm-10 col-md-10 col-lg-10"> 
                    <div class='input-group'>
                    <input class="form-control" id='input-rech' name=search>
                        <div class="input-group-append">
                            <button class="btn btn-outline-success" type="submit">Rechercher</button>
                        </div>
                    </div>
            </div>
        </div>

    <div id="testliste">
    <table class='table table-md table-striped table-responsive-sm'>
                    <thead class='thead-dark account-head'>
                                <tr>
                                    <th colspan="3">
                                        <div class="text-right">
                                            <label>Afficher</label>
                                            <select name="filter_pages" class="search-input form-control" onchange="this.form.submit()" style="display: inline-block; max-width: 80px">
                                                <option value="15" {{ $searchInfo[1] == 15 ? 'selected' : '' }}>15</option>
                                                <option value="25" {{ $searchInfo[1] == 25 ? 'selected' : '' }}>25</option>
                                                <option value="50" {{ $searchInfo[1] == 50 ? 'selected' : '' }}>50</option>
                                                <option value="100" {{ $searchInfo[1] == 100 ? 'selected' : '' }}>100</option>
                                            </select>
                                        </div>
                                    </th>
                                </tr>
                            <tr>
                                <th></th>
                                <th>Nom de la marque</th>
                                <th>Actif</th>
                            </tr>
                    </thead>
                        @foreach($marques as $uneMarque)
                                <tr class='account-row'>
                                    <th data-id='{{$uneMarque->nom_marque}}'>
                                        <label class="ck-container">
                                            <input type=checkbox class='chkbox'>
                                            <span class="checkmark"></span>
                                        </label>
                                    </th>
                                    <td>{{$uneMarque->nom_marque}}</td>
                                    
                                    <td>@if($uneMarque->actif)
                                        Oui
                                        @else Non
                                        @endif
                                    </td> 
                                </tr>
                        @endforeach
                        <tfoot class='table-dark'>
                            <tr>
                                <td colspan="5">
                                    <button type="button" id='activer' data-tpe='activ-categorie' onclick='activate(this)' class="btn btn-primary">Activer</button>
                                    <button type="button" id='desactiver' data-tpe='desact-categorie' onclick='desactivate(this)' class="btn btn-secondary">Désactiver</button>
                                    <button type="button" id='edit' onclick="editit(this)" class = "btn btn-success">Modifier</button>
                                </td>
                            </tr>

                            <tr>
                                    <td class="align-middle"colspan="5">
                                        {{ $marques->links() }}
                                    </td>
                            </tr>
                        </tfoot>
            </table>
            </form>


        </div>
    </div>

</div>
</div>



</body>
@endsection

@section('scripts')
<script src="{{ asset('js/marque.js') }}"></script>
<script src="{{ asset('js/shift_checkbox.js') }}"></script>


<script>
var routes = [];
routes.push('{{ action('MarqueController@Edit') }}');
routes.push('{{ action('MarqueController@activerMarque') }}');
routes.push('{{ action('MarqueController@desactiverMarque') }}');


function activate(what){
    activer(what);
}

function desactivate(what){
    desactiver(what);
}

function editit(what){
    edit(what);
}
</script>
@endsection