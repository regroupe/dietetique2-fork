<?php
        use App\Http\Controllers;
        $formatNumber = Controllers\PersonnalisationController::GetNumberFormat();
?>
<h4><i class="fa fa-search-plus" aria-hidden="true"></i> Critères de recherche</h4>
<form method=POST action='{{action("ProduitOffertController@RecherchePOST")}}'>
    {{ csrf_field() }}
    <div class="form-row">
        <div class="form-group col-sm-3 col-md-3 col-lg-3"> 
                <select class='form-control' id='actif-rech' name="search_type">
                        <option value=nom >Nom d'article</option>
                        <option value=crea>Créateur</option>
                </select>
        </div>

        <div class="form-group col-sm-9 col-md-9 col-lg-9"> 
                <div class='input-group'>
                <input class="form-control" id='input-rech' name=search>
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" type="submit">Rechercher</button>
                    </div>
                </div>
        </div>
    </div>

@if ($searchInfo[0] == 1)
    <h4>{{Controllers\PersonnalisationController::GetSellerName(true, true)}} en vente</h4>
@else
    <h4>{{Controllers\PersonnalisationController::GetSellerName(true, true)}} non disponible</h4>
@endif

    <table class='table table-md table-striped table-responsive-sm'>
        <thead class='thead-dark account-head'>
        <tr>
            <th colspan="2">
                <select class='form-control' id='type_rech' name=actif onchange="this.form.submit()">
                        <option value=1 {{ $searchInfo[0] == 1 ? 'selected' : '' }}>En vente</option>
                        <option value=0 {{ $searchInfo[0] == 0 ? 'selected' : '' }}>Non disponible</option>
                </select>
            </th>
            
            <th colspan="3">
                <div class="text-right">
                    <label>Afficher</label>
                    <select name="filter_pages" class="search-input form-control" onchange="this.form.submit()" style="display: inline-block; max-width: 80px">
                        <option value="15" {{ $searchInfo[1] == 15 ? 'selected' : '' }}>15</option>
                        <option value="25" {{ $searchInfo[1] == 25 ? 'selected' : '' }}>25</option>
                        <option value="50" {{$searchInfo[1] == 50 ? 'selected' : '' }}>50</option>
                        <option value="100" {{ $searchInfo[1] == 100 ? 'selected' : '' }}>100</option>
                    </select>
                </div>
            </th>
        </tr>
        <tr>
                <th></th>
                <th>Nom</th>
                <th>Créateur</th>
                <th>Prix</th>
                <th class="text-right">Options</th>
        </tr>
        </thead>
        <tbody id='body-table'>
        @foreach($article as $i=>$unArticle)
                <tr class='account-row'>
                        <th data-id='{{$unArticle->rowid}}'>
                            <label class="ck-container">
                                <input type=checkbox class='chkbox'>
                                <span class="checkmark"></span>
                            </label>
                        </th>
                        <td>{!! $unArticle->nom !!}
                            {{ $unArticle->format_quantite }}
                            {{ $unArticle->format }}
                            <?php if ($unArticle->bio) : ?>
                                <i class="fa fa-leaf" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Biologique"></i>
                            <?php endif; ?>
                            <?php if ($unArticle->perissable) : ?>
                                <i class="fa fa-apple" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Périssable"></i>
                            <?php endif; ?>
                        </td>
                        <td>{!! $unArticle->created_by !!}</td>
                        <td>{{ number_format(truncate_number($unArticle->prix, 2), 2, $formatNumber->decimal, $formatNumber->separator) }} $</td>
                        

                        <td class="align-middle text-right">
                            <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="Détails" onclick="window.location.href = '{{ action('BoutiqueController@View_GET', $unArticle->rowid) }}'"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            <button type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Modifier" onclick="window.location.href = '{{ action('ArticleController@Edit_GET', $unArticle->rowid ) }}'"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                        </td>
                </tr>
        @endforeach
        </tbody>
        <tfoot class='table-dark'>
        <tr>
                <td colspan="6">
                    @if ($dispo == 1)
                        <button type='button' class='btn btn-primary' id='dont' data-tpe='dont' onclick='enlever(this)'>Enlever des {{Controllers\PersonnalisationController::GetSellerName(true, false)}} en vente</button>
                    @else
                        <button type='button'  class='btn btn-primary' id='offer' data-tpe='offer' onclick='ajouter(this)'>Mettre en vente</button>
                    @endif
        
                </td>
        </tr>
        <tr>
                <td class="align-middle"colspan="6">{{ $article->links() }}</td>
        </tr>
        </tfoot>
    </table>
</form>