<?php
        use App\Http\Controllers;
?>
@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/fancyCheckbox.css') }}">
@endsection
@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">{{Controllers\PersonnalisationController::GetSellerName(true, true)}} en vente</h1>
   </div>
</div>

{{ Breadcrumbs::render('EnVente') }}

<div class='container'>

        <div class='row'>

                @include('administrateur._side_menu')

                <div class='col-sm-12 col-md-9 col-lg-9'>
                
                        <?php
                                echo $html;
                        ?>
                </div>
        </div>
</div>
</body>
@endsection
@section('scripts')
<script>
        var routes = [];
        routes.push("{{ action('ProduitOffertController@Offrir') }}");
        routes.push("{{ action('ProduitOffertController@Enlever') }}");
</script>

<script>

function enlever(what){
    dont(what);
}

function ajouter(what){
    offer(what);
}
$(function () { $('[data-toggle="tooltip"]').tooltip(); })


</script>
<script src="{{ asset('js/produit_offert.js') }}"></script>
<script src="{{ asset('js/shift_checkbox.js') }}"></script>
@endsection