<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $mainTheme = Controllers\PersonnalisationController::ReadActif();

?>

@extends('layouts.master')

@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">


 
@endsection

@section('content')
<body>
        <div id="category-banner" class="jumbotron jumbotron-fluid">
                <div class="container">
                        <h1 class="display-4">Conditions d'utilisation et politiques</h1>
                </div>
        </div>

                {{ Breadcrumbs::render('termsofuse') }}

        <div class="container">
        @if ($mainTheme->termsofuse != null)
                {!!$mainTheme->termsofuse!!}
        @else
            Il n'y a pas de conditions d'utilisation et de politiques.
        @endif
        </div>
</body>

@endsection

@section('scripts')
@endsection