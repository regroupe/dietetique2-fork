<div class="row">
    <div class="col-12">
    
        <div class="card ">
            <div class="card-header bg-dark">
                <h5 style="color:#fff">Section à propos</h5>
            </div>
            <div class="card-body"> 
            @if  ($errors->has('summernoteInput'))
                <div class="alert alert-danger"> {{ $errors->first('summernoteInput') }}</div>
            @endif
                <textarea name="summernoteInput" class="summernote" name="summernoteInput">{!!$theme->about_html!!}</textarea>      
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <button class="btn btn-outline-success btn-lg btn-block" type="submit">Enregistrer</button><br>
    </div>

    <div class="col-md-6">
        <button class="btn btn-outline-danger btn-lg btn-block" type="button" onclick="window.location.href = '{{ action('PersonnalisationController@Index') }}'">Annuler</button>
    </div>
</div>