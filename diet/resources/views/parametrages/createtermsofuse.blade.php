
@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/personnalisation.css') }}">


<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>    

@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Modifier les conditions d'utilisation et les politiques </h1>
    </div>
</div>

{{ Breadcrumbs::render('Themes_modifier', $theme) }}

<div class="container">
        <div class="card">
                <h5 class="card-header bg-secondary text-white">Héberger une image</h5>
                <div class="card-body">
                    <form enctype="multipart/form-data" id="heberge">
                        {{ csrf_field() }}
                        <div class="form-group col-sm-12">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="hidden" id="img_dirty" name="img_dirty" value="0">
                                    <input type="file" class="custom-file-input" id="img" name="img">
                                    <label class="custom-file-label" for="img">Choisir une image</label>
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-success" type="submit"><i class="fa fa-check" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Générer un lien"></i></button>
                                    <button id="remove-img" class="btn btn-secondary" type="button" onclick="removeFile('#img', 'Choisir une image')"><i class="fa fa-remove"></i></button>
                                </div>
                            </div>
                        </div>
    
                        <div class="form-group col-sm-12">
                            <label for='lien'>Lien généré</label>
                            <div class="input-group">
                                <input readonly class="form-control" id='lien' name='lien' type='text' value=''>
                             
                                <div class="input-group-append">
                                    <button class="btn btn-info "  type="button" onclick="copyInputValue('#lien')"><i class="fa fa-copy" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Copier"></i></button>
                                </div>
                            </div>
                        </div> 
                    </form>
                </div>
        </div>



    <form action="{{action('ParametragesController@SaveTerms_POST')}}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
        <input type="hidden" name="rowid" value="{{ $theme->rowid }}">
        @include('parametrages._createtermsofuse')
       
	</form>
   
</div>
@endsection

@section('scripts')
 


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<script src="{{ asset('js/summernote-FR.js') }}"></script>

<script src="{{ asset('js/heberger-image.js') }}"></script>
<script>
    route_heberge = '{{action('ParametragesController@Heberger_POST')}}';

        $(document).ready(function() {
            $('.summernote').summernote({
            placeholder: 'À propos de nous...',
            tabsize: 2,
            minHeight: 300,
             lang: 'fr-FR',
            });
        });
</script>
@endsection