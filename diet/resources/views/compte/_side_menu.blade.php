<div class='col-sm-12 col-md-3 col-lg-3'>
        
        <ul class="nav flex-column">
                <h4><i class="fa fa-align-justify"></i> Compte</h4>
                <hr class="lehr">
                <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(1) == 'compte' && Request::segment(2) == null ? 'active-clr' : '' }}" href='{{ action("CompteController@IndexCompte") }}'>Informations de compte</a></li>
                <hr class="lehr">
                <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(2) == 'historique' ? 'active-clr' : '' }}" href='{{ action("CompteController@IndexHistorique") }}'>Historique des commandes</a></li>
                <hr class="lehr">
        </ul>
</div>