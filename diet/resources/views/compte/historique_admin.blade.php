<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $formatNumber = Controllers\PersonnalisationController::GetNumberFormat();
?>

@extends('layouts.master')

@section('styles')
<style>
    #category-banner {
        background: url('/img/checkout.jpeg') center center / cover no-repeat;
        color: white;
        text-shadow: 0 0 10px black;
        box-shadow: 100vw 100vh inset rgba(0, 0, 0, 0.5);
    }
</style>
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Historique de commande</h1>
    </div>
</div>

{{ Breadcrumbs::render('CommandesClients', $usr) }}

<div class='container'>
		<div class='row'>
			@include('administrateur._side_menu')

			<div class='col-sm-12 col-md-9 col-lg-9'>
				<table class="table">
					<thead>
						<tr class="table-active">
							<th scope="col">Commande</th>
							<th scope="col" class="text-right">Total</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($usr->commandes()->where('paye', 1)->orderBy('date_commande', 'desc')->get() as $commande) : ?>
							<tr>
								<td scope="row" class="align-middle"><strong>#{{ $commande->rowid }}</strong> {{ $commande->date_commande }}</td>
								<td class="align-middle text-right">{{ number_format(truncate_number($commande->total, 2), 2, $formatNumber->decimal, $formatNumber->separator) }}$</td>
								<td class="align-middle"><a class="btn btn-info btn-block" href="{{ action('CommandeController@ViewAdmin_GET',[$usr->rowid, $commande->rowid]) }}"><i class="fa fa-eye" aria-hidden="true"></i> Voir</a></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
@endsection