@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/compte.css') }}">
@endsection

@section('content')
<body>


<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Informations de compte</h1>
    </div>
</div>

{{ Breadcrumbs::render('InfoClient') }}

<div class='container'>
        <div class='row'>
                
                @include('compte._side_menu')

                <div class='col-sm-12 col-md-9 col-lg-9'>

                        @if (session()->has('successA'))
                                <div  class="alert alert-success">
                                        <strong>{{session()->get('successA')}}</strong>
                                </div>
                        @elseif (session()->has('success'))
                        <div  class="alert alert-success">
                                <strong>{{session()->get('success')}}</strong>
                        </div>
                        @endif

                        <h4>Bonjour {{session('client')->email}}</h4>

                        <form action="{{ action('CompteController@ResetPassword') }}" method='POST'>

                        <h5>Changer de mot de passe</h5>

                        @if ($errors->has('Motdepasse'))
                                <div  class="alert alert-danger">
                                        <strong>{{$errors->first('Motdepasse')}}</strong>
                                </div>
                        @endif
                        
                        {{ csrf_field() }}
                        <div class="form-group"> 
                                <label for='actuelpass'>Mot de passe actuel</label>
                                <input class="form-control" id='actuelpass' name='actuelpass' type='password'>
                                <div class="text-danger">{{ $errors->first('actuelpass') }}</div>
                        </div>
                        
                        <div class="form-group"> 
                                <label for='nvpass'>Nouveau mot de passe</label>
                                <input class="form-control" id='nvpass' name='nvpass' type='password'>
                                <div class="text-danger">{{ $errors->first('nvpass') }}</div>
                        </div>
                        
                        <div class="form-group"> 
                                <label for='nvpass_confirmation'>Confirmer le nouveau mot de passe</label>
                                <input class="form-control" id='nvpass_confirmation' name='nvpass_confirmation' type='password'>
                                <div class="text-danger">{{ $errors->first('nvpass_confirmation') }}</div>
                        </div>
                        <br>
                        <button class='btn btn-primary' type='submit'>Modifier</button>
                        </form>

                        <br>
                        <h5>Adresse de facturation</h5>

                        

                        <form  action="{{ action('CompteController@SaveAddress') }}" method='POST'>
                        {{ csrf_field() }}

                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                                <label for='nm'>Nom</label>
                                                <input class="form-control" id='nm' name='nm' type='text' value='{{(old('nm')) ? old('nm') : $usr->nom}}'>
                                                <div class="text-danger">{{ $errors->first('nm') }}</div>
                                        </div> 
                                        
                                        <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                                <label for='pnm'>Prénom</label>
                                                <input class="form-control" id='pnm' name='pnm' type='text' value='{{(old('pnm')) ? old('pnm') : $usr->prenom}}'>
                                                <div class="text-danger">{{ $errors->first('pnm') }}</div>
                                        </div>
                                
                                </div>

                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-10 col-lg-10">
                                                <label for='address'>Adresse</label>
                                                <input class="form-control" id='address' name='address' type='text' value='{{(old('address')) ? old('address') : $usr->adresse}}'>
                                                <div class="text-danger">{{ $errors->first('address') }}</div>
                                        </div> 
                                        
                                        <div class="form-group col-sm-12 col-md-2 col-lg-2">
                                                <label for='appart'>No. Appt.<span class="facultatif-etoile">*</span></label>
                                                <input class="form-control" id='appart' name='appart' type='text' value='{{(old('appart')) ? old('appart') : $usr->no_app}}'>
                                                <div class="text-danger">{{ $errors->first('appart') }}</div>
                                        </div>
                                </div>

                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                <label for='ville'>Ville</label>
                                                <input class="form-control" id='ville' name='ville' type='text' value='{{(old('ville')) ? old('ville') : $usr->ville}}'>
                                                <div class="text-danger" >{{ $errors->first('ville') }}</div>
                                        </div> 
                                </div>

                                @if (false == true)
                                <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                                <label for='prov'>Province</label>
                                                <input class="form-control"  id='prov' name='prov' type='text' value='{{(old('prov')) ? old('prov') : $usr->province}}'>
                                                <div class="text-danger">{{ $errors->first('prov') }}</div>
                                        </div> 

                                        <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                                <label for='pays'>Pays</label>
                                                <input class="form-control" id='pays' name='pays' type='text' value='{{(old('pays')) ? old('pays') : $usr->pays}}'>
                                                <div  class="text-danger" >{{ $errors->first('pays') }}</div>
                                        </div> 
                                </div>
                                @endif

                                <div class="form-row">
                                                <div class="form-group col-sm-12 col-md-3 col-lg-3">
                                                        <label for='code_p'>Code Postal</label>
                                                        <input class="form-control" id='code_p' name='code_p' placeholder="G0G 0G0" type='text' 
                                                        value='{{(old('code_p')) ? old('code_p') : $usr->code_postal}}'>
                                                        <div class="text-danger">{{ $errors->first('code_p') }}</div>
                                                </div> 
        
                                                <div class="form-group col-sm-12 col-md-6 col-lg-6">
                                                        <label for='tlphne'>Téléphone</label>
                                                        <input class="form-control" id='tlphne' name='tlphne' type='tel' value='{{(old('tlphne')) ? old('tlphne') : $usr->telephone}}' placeholder="123-456-7890">
                                                        <div class="text-danger">{{ $errors->first('tlphne') }}</div>
                                                </div> 
                                                <div class="form-group col-sm-12 col-md-3 col-lg-3">
                                                        <label for='poste'>Poste<span class="facultatif-etoile">*</span></label>
                                                        <input class="form-control" id='poste' name='poste' type='text' value='{{(old('poste')) ? old('poste') : $usr->poste}}'>
                                                        <div class="text-danger">{{ $errors->first('poste') }}</div>
                                                </div> 
                                </div>
                                <div class="facultatif"><small>*Champs facultatifs</small></div>
                                <button class='btn btn-primary'type='submit'>Modifier</button>

                        </form>

                        <br>

                        <form action="{{ action('ConnexionController@Logout') }}" method='GET'>
                        {{ csrf_field() }}
                        <button class='btn btn-secondary' type='submit'>Déconnexion</button>
                        </form>
                </div>
        </div>
</div>
</body>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#tlphne").val(parsePhoneNumber($("#tlphne")[0].value));
    });
</script>

@endsection