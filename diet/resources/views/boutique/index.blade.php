
<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $categories = Controllers\CategorieController::Read();
?>

@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/boutique.css') }}">
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">{{ $req->filter_category ? $req->filter_category : 'Toutes les catégories' }}</h1>
    </div>
</div>

{{ Breadcrumbs::render('Boutique') }}

<div class="container">
    @include('periodecommande._activeperiod')
    <div class="row">
        <div class="col-sm-12 col-md-5 col-lg-4 col-xl-3">
            <form id="search-list">

            <input type="hidden" name="filter_category" value="{{ $req->filter_category }}">

            <div id="product-navigation-xs">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filtrer
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <input class="dropdown-item" type="submit" value="Toutes les catégories" onclick="changeCategory(this, true)">
                        <?php foreach ($categories as $categorie) : ?>
                            <input class="dropdown-item" type="submit" value="{{ $categorie->nom_categorie }}" onclick="changeCategory(this)">
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div id="product-navigation">
                <h4><i class="fa fa-cutlery" aria-hidden="true"></i> Catégories</h4><hr>
                <input class="dropdown-item" type="submit" value="Toutes les catégories" onclick="changeCategory(this, true)">
                <?php foreach ($categories as $categorie) : ?>
                    <hr><input class="dropdown-item" type="submit" value="{{ $categorie->nom_categorie }}" onclick="changeCategory(this)">
                <?php endforeach; ?>
            </div>
            </form>
        </div>

        <div class="col-sm-12 col-md-7 col-lg-8 col-xl-9">
            <div class="row">
                <div class="col-12">
                    <form id="filter-list">
                        <div class="form-group">
                            <div class='input-group'>
                                <input class="form-control" name="filter_search" value="{{ $req->filter_search }}">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-success" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Rechercher</button>
                                </div>
                            </div>
                        </div>
                        <div id="filter-group">
                            <div id="product-count"><span id="product-count-number"></span> {{Controllers\PersonnalisationController::GetSellerName(true, false)}}</div>
                            <div class="dropdown">
                                <input type="hidden" name="filter_price" value="{{ $req->filter_price }}">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Trier
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="submit" value="0" onclick="this.form.filter_price.value=this.value">Par défaut</button>
                                    <button class="dropdown-item" type="submit" value="1" onclick="this.form.filter_price.value=this.value">Prix croissant</button>
                                    <button class="dropdown-item" type="submit" value="2" onclick="this.form.filter_price.value=this.value">Prix décroissant</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row" id="results">
                <?php echo $partial; ?>
            </div>
        </div>
    </div>
</div>
@include('boutique._addedtobasket')
@endsection

@section('scripts')
<script>
    var routes = {
		panier: {
            index: "{{ action('PanierController@Index') }}",
            update: "{{ action('PanierController@Update_POST') }}"
        },
        boutique: {
            index: "{{ action('BoutiqueController@Index') }}",
        }
    };
</script>
<script src="{{ asset('js/boutique.js') }}"></script>
@endsection