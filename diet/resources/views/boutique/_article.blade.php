<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $formatNumber = Controllers\PersonnalisationController::GetNumberFormat();
?>
<div class="product-container">
    <a href="{{ action('BoutiqueController@View_GET', $article->rowid) }}">
        <?php if ($article->quebec) : ?>
            <img class="quebec-icon" src="/img/quebec.png" alt="Quebec">
        <?php endif; ?>
        <div class="image-preview" style="background-image: url({{ $article->image ? Storage::url($article->image) : '/img/preview.png' }})"></div>
        <div class="progress" data-toggle="tooltip" data-placement="top" title="{{ $progress['percentage'] }}% de la quantité totale requise pour regroupement d'achat présentement commandé!">
            <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress['percentage'] >= 100 ? 'bg-success' : '' }}" style="width: {{ $progress['percentage'] }}%" role="progressbar" aria-valuenow="{{ $progress['percentage'] }}" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <h3 style="min-height:100px">
            {{ $article->nom }}
            <?php if ($article->bio) : ?>
                <i class="fa fa-leaf" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Biologique"></i>
            <?php endif; ?>
            <?php if ($article->perissable) : ?>
                <i class="fa fa-apple" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Périssable"></i>
            <?php endif; ?>
        </h3>
    </a>
    <p class="product-price">{{ $article->format_quantite }} {{ $article->format }} pour {{ number_format(truncate_number(Controllers\ArticleController::ReadPricePlus($article), 2), 2, $formatNumber->decimal, $formatNumber->separator)  }} $</p>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="prix-addon"><strong>Qté</strong></span>
        </div>
        <input name="product-quantity" type="number" class="form-control" value="1" min="1">
        <div class="input-group-append" data-toggle="tooltip" data-placement="top" title="Ajouter au panier">
            <button class="btn btn-outline-success" type="button" onclick="addToBasket(this)" data-toggle="modal" data-target="#addedtobasket-modal" {{ $progress["available"] == 0 ? 'disabled' : '' }}><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
        </div>
    </div>
    <p>Quantité disponible: {{ $progress["available"] }} x {{ $article->format_quantite }} {{ $article->format }}</p>
</div>