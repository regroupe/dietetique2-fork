<div class="modal fade" id="addedtobasket-modal" tabindex="-1" role="dialog" aria-labelledby="addedtobasket-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addedtobasket-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="p-3">
                <button type="button" class="btn btn-outline-secondary btn-lg btn-block" data-dismiss="modal">Continuer à magasiner</button>
                <button type="button" class="btn btn-outline-success btn-lg btn-block" onclick="location.href = routes.panier.index">Voir mon panier</button>
            </div>
        </div>
    </div>
</div>