
<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $articles = Controllers\BoutiqueController::ReadRandom($article->nom_categorie);
    $nutritional = Storage::url($article->valeur_nutritive);
    $progress = Controllers\ArticleController::ReadProgress($article);
    $recettes = Controllers\ArticleController::ReadRecette($article->rowid);
    $formatNumber = Controllers\PersonnalisationController::GetNumberFormat();
?>

@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/boutique.css') }}">
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">{{ $article->nom_categorie }}</h1>
    </div>
</div>

{{ Breadcrumbs::render('VueArticle', $article) }}

<div class="container">
    @include('periodecommande._activeperiod')
    <div class="row">

        <!-- Image section -->
        <div class="col-sm-6">
            <?php if ($article->quebec) : ?>
                <img class="quebec-icon" src="/img/quebec.png" alt="Quebec">
            <?php endif; ?>
            <div class="image-preview" style="background-image: url({{ $article->image ? Storage::url($article->image) : '/img/preview.png' }})"></div>
        </div>

        <!-- Details -->
        <div class="shop-product col-sm-6" data-id="{{ $article->rowid }}">
            <h1>
                {{ $article->nom }}
                <?php if ($article->bio) : ?>
                    <i class="fa fa-leaf" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Biologique"></i>
                <?php endif; ?>
                <?php if ($article->perissable) : ?>
                    <i class="fa fa-apple" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Périssable"></i>
                <?php endif; ?>
            </h1>
            <p class="product-price">{{ $article->format_quantite }} {{ $article->format }} pour {{ number_format(truncate_number(Controllers\ArticleController::ReadPricePlus($article), 2), 2, $formatNumber->decimal, $formatNumber->separator) }} $</p>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="prix-addon"><strong>Qté</strong></span>
                </div>
                <input name="product-quantity" type="number" class="form-control" value="1" min="1">
                <div class="input-group-append" data-toggle="tooltip" data-placement="top" title="Ajouter au panier">
                    <button class="btn btn-outline-success" type="button" onclick="addToBasket(this)" data-toggle="modal" data-target="#addedtobasket-modal" {{ $progress["available"] == 0 ? 'disabled' : '' }}><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                </div>
            </div>
            <div class="progress" data-toggle="tooltip" data-placement="top" title="{{ $progress['percentage'] }}% de la quantité totale requise pour regroupement d'achat présentement commandé!">
                <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress['percentage'] >= 100 ? 'bg-success' : '' }}" style="width: {{ $progress['percentage'] }}%" role="progressbar" aria-valuenow="{{ $progress['percentage'] }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>Quantité disponible: {{ $progress["available"] }} x {{ $article->format_quantite }} {{ $article->format }}</p>
            <table class="table">
                <tr>
                    <td>Provenance:</td>
                    <td>{{ $article->provenance }}</td>
                </tr>
                <tr>
                    <td>Marque:</td>
                    <td>{{ $article->marque->nom_marque }}</td>
                </tr>
                <tr>
                    <td>Biologique:</td>
                    <td>{{ $article->bio ? 'Oui' : 'Non'}}</td>
                </tr>
                <tr>
                    <td>Périssable:</td>
                    <td>{{ $article->perissable ? 'Oui' : 'Non'}}</td>
                </tr>
                <tr>
                    <td>Allergènes:</td>
                    <td>{{ $article->allergene ? $article->allergene : 'Aucun' }}</td>
                </tr>
            </table>
        </div>

        <!-- Description -->
        <div class="col-12 col-md-6">
            <h3  class="heading">Description</h3>
            <p>{{ $article->description ? $article->description : 'Aucune description.' }}</p>
            <?php if ($article->piece_jointe) : ?>
            <form method="get" action="{{action('ArticleController@DownloadPieceJointe', $article->rowid)}}">
                {{ csrf_field() }}
                <button class="btn btn-success" >Pièce jointe</button>
            </form>
            <?php endif; ?>
        </div>

        <!-- Information -->
        <div class="col-12 col-md-6">
            <h3  class="heading">Informations</h3>
            <p>{{ $article->remarque ? $article->remarque : 'Aucune remarque.' }}</p>
            <?php if ($article->valeur_nutritive) : ?>
                <button class="btn btn-info" data-toggle="modal" data-target="#nutritionalModal"><i class="fa fa-info-circle" aria-hidden="true"></i> Voir les valeurs nutritives</button>
            <?php endif; ?>
        </div>

         <!-- Recettes -->
        <div class="col-12">
            <h3 class="heading">Recette(s) jointe(s)</h3>
            @if (count($recettes) > 0)
                <ul class="recette-list">
                @foreach ($recettes as $index => $recette)
                    <li class="recette-list-item">
                        <a class="link" target="_blank" href="{{$recette->link}}">{{$recette->name}}</a>
                    </li>
                @endforeach
                </ul>
            @else
                Aucune recette(s).
            @endif
        </div>

        <?php if (count($articles) > 0) : ?>
            <div class="col-12">
                <h2 class="heading">{{Controllers\PersonnalisationController::GetSellerName(true, true)}} similaire(s)</h2>
                <div class="row">
                    <?php foreach ($articles as $article) : $progress = Controllers\ArticleController::ReadProgress($article); ?>
                        <div class="shop-product col-sm-12 col-md-4" data-id="{{ $article->rowid }}">
                            @include('boutique._article')
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<div class="modal fade" id="nutritionalModal" tabindex="-1" role="dialog" aria-labelledby="nutritionalModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="nutritionalModalTitle">Voir les valeurs nutritives</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img id="nutritional-preview" src="{{ $nutritional }}" alt="Nutritional">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>
@include('boutique._addedtobasket')
@endsection

@section('scripts')
<script>
    var routes = {
        panier: {
            index: "{{ action('PanierController@Index') }}",
            update: "{{ action('PanierController@Update_POST') }}"
        }
    };
</script>
<script src="{{ asset('js/boutique.js') }}"></script>
@endsection