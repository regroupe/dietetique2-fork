
<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
?>

<input id="boutique-articles-count" type="hidden" value="{{ $articlesCount }}">

<?php foreach ($articles as $article) : $progress = Controllers\ArticleController::ReadProgress($article); ?>
    <div class="shop-product col-12 col-lg-6 col-xl-4" data-id="{{ $article->rowid }}">
        @include('boutique._article')
    </div>
<?php endforeach; ?>

<div class="col-12">
	{{ $articles->links() }}
</div>

<script>
    $(document).ready(function () {
        $("#product-count-number").text($("#boutique-articles-count").val());
    });
</script>