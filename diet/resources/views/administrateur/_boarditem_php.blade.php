<div class="board-row no-{{$id}} {{GetSize($item->vue_size)}} mb-3">
    <div class="card card-{{$id}} {{GetBorderColor($item->vue_color)}}">
        <div class="card-body card-cursor">
            <div class="form-group">
                <label for='vue'>Vue</label>
                <div class="input-group">
                    <select class="custom-select" name="board_item[]">
                        @foreach ($partials_list[0] as $i => $name)
                            <option value="{{$partials_list[1][$i]}}"
                            @if (old('board_item[]'))
                                @if (old('board_item[]')[$index] == $partials_list[1][$i])
                                    selected
                                @endif
                            @elseif ($item->vue == $partials_list[1][$i])
                                selected
                            @endif
                            >
                                {{$name}}
                            </option>
                        @endforeach
                    </select>


                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button" onclick="DeleteRow('{{$id}}');"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col-6">
                    <label for='size'>Grandeur</label>
                    <select class="custom-select" name="board_size[]" onchange="ChangeSize(this, '{{$id}}')">
                        <option value="small" 
                        {{(old('board_size[]')) ? (old('board_size[]')[$i] == 'small' ? 'selected' : '') : ($item->vue_size == 'small') ? 'selected' : ''}}
                        >Petit</option>

                        <option value="medium"
                        {{(old('board_size[]')) ? (old('board_size[]')[$i] == 'medium' ? 'selected' : '') : ($item->vue_size == 'medium') ? 'selected' : ''}}
                        >Medium</option>

                        <option value="large"
                        {{(old('board_size[]')) ? (old('board_size[]')[$i] == 'large' ? 'selected' : '') : ($item->vue_size == 'large') ? 'selected' : ''}}
                        >Large</option>
                    </select>
                </div>
                <div class="col-6">
                    <label for='color'>Couleur</label>
                    <select class="custom-select" name="board_color[]" onchange="ChangeColor(this, '{{$id}}')">
                        <option value="primary"
                        {{(old('board_color[]')) ? (old('board_color[]')[$i] == 'primary' ? 'selected' : '') : ($item->vue_color == 'primary') ? 'selected' : ''}}
                        >Bleu</option>
                        <option value="info"
                        {{(old('board_color[]')) ? (old('board_color[]')[$i] == 'info' ? 'selected' : '') : ($item->vue_color == 'info') ? 'selected' : ''}}
                        >Bleu pâle</option>
                        <option value="secondary"
                        {{(old('board_color[]')) ? (old('board_color[]')[$i] == 'secondary' ? 'selected' : '') : ($item->vue_color == 'secondary') ? 'selected' : ''}}
                        >Gris</option>
                        <option value="warning"
                        {{(old('board_color[]')) ? (old('board_color[]')[$i] == 'warning' ? 'selected' : '') : ($item->vue_color == 'warning') ? 'selected' : ''}}
                        >Jaune</option>
                        <option value="dark"
                        {{(old('board_color[]')) ? (old('board_color[]')[$i] == 'dark' ? 'selected' : '') : ($item->vue_color == 'dark') ? 'selected' : ''}}
                        >Noir</option>
                        <option value="danger"
                        {{(old('board_color[]')) ? (old('board_color[]')[$i] == 'danger' ? 'selected' : '') : ($item->vue_color == 'danger') ? 'selected' : ''}}
                        >Rouge</option>
                        <option value="success"
                        {{(old('board_color[]')) ? (old('board_color[]')[$i] == 'success' ? 'selected' : '') : ($item->vue_color == 'success') ? 'selected' : ''}}
                        >Vert</option>
                    </select>
                </div>
            </div>
            
        </div>
    </div>
</div>