<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $partials_list = json_decode(ReadPartials());
    $items = Controllers\AdminController::ReadBoardItem();
?>

@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/tableaubord.css') }}">
<script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
@endsection

@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">Personnaliser le tableau de bord</h1>
   </div>
</div>

{{ Breadcrumbs::render('EditBoard') }}

<div class='container'>
        <div class="row pb-3 text-right">
           
        </div>
        <form method="POST" action="{{action('AdminController@SaveBoard_POST')}}">
            {{ csrf_field() }}
            <div class="card mb-4">
                    <div class="d-none d-md-block card-header bg-dark align-middle">
                                <h5 class="align-center" style="color:#fff">Personnaliser mon tableau de bord
                                    <button  class="switcher float-right btn btn-success" type="button" onclick="switchSortable(false)"><i class="fa fa-arrows-alt"></i> <span>Déplacement activé</span></button>
                                    
                                </h5>
                    </div>

                    <div class="d-sm-block d-md-none card-header bg-dark">
                                <h5 style="color:#fff">Tableau de bord</h5>
                    </div>

                    <div class="card-body">
                        <div class='row row-list' id="sortablelist">
                                @foreach ($items as $index => $item)
                                    <?php
                                        $id = GenerateGuid();
                                    ?>
                                    
                                
                                    @include('administrateur._boarditem_php')
                                @endforeach
                        </div>
                    </div>
                    <button id="addrow" type="button" onclick="AddRow1()" class="btn btn-primary">Ajouter une vue</button>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <button class="btn btn-outline-success btn-lg btn-block" type="submit">Enregistrer</button><br>
                </div>

                <div class="col-md-6">
                    <button class="btn btn-outline-danger btn-lg btn-block" type="button" onclick="window.location.href = '{{ action('AdminController@IndexTableau') }}'">Annuler</button>
                </div>
            </div>
            
        </form>
</div>

<div class="d-sm-block d-md-none">
    <button  class="switcher fixed-drag float-right btn btn-success" type="button" onclick="switchSortable(true)"><i class="fa fa-arrows-alt"></i></button>
</div>

</body>

@endsection

@section('scripts')
<script>
    var route = "{{action("AdminController@AddRow1_POST")}}";
</script>

<script src="{{ asset('js/board.js') }}"></script>
@endsection