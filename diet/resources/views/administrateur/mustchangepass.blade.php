@extends('layouts.master')

@section('styles')
@endsection

@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">Modifier votre mot de passe</h1>
   </div>
</div>
<div class='container'>
    <div class='row mt-3'>
        <div class='col-sm-12 col-md-12 col-lg-12'>
            <h5>Saisir un nouveau mot de passe</h5>
            @include('administrateur._modifiermdp')
        </div>
    </div>
</div>
</body>
@endsection

@section('scripts')
@endsection