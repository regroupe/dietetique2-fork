<form action="{{ action('AdminController@ResetPassword') }}" method='POST'>


        @if ($errors->has('Motdepasse'))
                <div class="alert alert-danger">
                        <strong>{{$errors->first('Motdepasse')}}</strong>
                </div>
        @endif

        @if (session()->has('success'))
                <div  class="alert alert-success">
                        <strong>{{session()->get('success')}}</strong>
                </div>
        @endif
        
        {{ csrf_field() }}
        <div class="form-group"> 
                <label for='actuelpass'>Mot de passe actuel</label>
                <input class="form-control" id='actuelpass' name='actuelpass' type='password'>
                <div class="text-danger">{{ $errors->first('actuelpass') }}</div>
        </div>

        <div class="form-group"> 
                <label for='nvpass'>Nouveau mot de passe</label>
                <input class="form-control" id='nvpass' name='nvpass' type='password'>
                <div class="text-danger">{{ $errors->first('nvpass') }}</div>
        </div>

        <div class="form-group"> 
                <label for='nvpass_confirmation'>Confirmer le nouveau mot de passe</label>
                <input class="form-control" id='nvpass_confirmation' name='nvpass_confirmation' type='password'>
                <div class="text-danger">{{ $errors->first('nvpass_confirmation') }}</div>
        </div>
        <br>
        <button class="btn btn-primary"type='submit'>Modifier</button>
</form>