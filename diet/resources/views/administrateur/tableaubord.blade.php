<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    use App\Models\AdminBoard;
    $items = Controllers\AdminController::ReadBoardItem();
?>

@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/tableaubord.css') }}">
@endsection

@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">Tableau de bord</h1>
   </div>
</div>

{{ Breadcrumbs::render('TableauBord') }}

<div class='container'>

        <div class='row'>

                @include('administrateur._side_menu')

                <div class='col-sm-12 col-md-9 col-lg-9'> 
                        <div class="row">
                                @if (count($items) > 0)
                                
                                        @foreach ($items as $index => $item)
                                                {!!GenerateView($item)!!}
                                        @endforeach
                                @else
                                <?php
                                        $default_view = new AdminBoard;
                                        $default_view->vue = 'active_period';
                                        $default_view->vue_size = 'large';
                                        $default_view->vue_color = 'success';

                                        $default_view2 = new AdminBoard;
                                        $default_view2->vue = 'ecoule_period';
                                        $default_view2->vue_size = 'large';
                                        $default_view2->vue_color = 'secondary';

                                ?>
                                        {!!GenerateView($default_view)!!}
                                        {!!GenerateView($default_view2)!!}
                                @endif
                        </div>

                        <div class="text-center">
                                <a href="{{action('AdminController@EditBoard_GET')}}" class='btn btn-outline-success'>Modifier mon tableau de bord</a>
                        </div>  
                </div>
        </div>
</div>
</body>

@endsection

@section('scripts')
<script>
$(function () { 
    
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
@endsection