
@extends('layouts.master')

@section('styles')
@endsection

@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">Informations de compte</h1>
   </div>
</div>

{{ Breadcrumbs::render('CompteAdmin') }}

<div class='container'>

        <div class='row'>

                @include('administrateur._side_menu')

                <div class='col-sm-12 col-md-9 col-lg-9'>

                        <h3>Bonjour {{session('admin')->username}}</h3>

                        <div style='padding-bottom:20px;'>Vous êtes connectés en tant <strong>
                        @if (session('admin')->super == true)
                                @if (session('admin')->master == true)
                                        qu'Administrateur.
                                @else que Super Opérateur.
                                @endif
                                
                        @elseif (session('admin')->take_order == true)
                                que Bénévole.
                        ­@else
                         qu'Opérateur.
                        @endif
                        
                        </strong></div>


                        
                        <h5>Changer de mot de passe</h5>
                        @include('administrateur._modifiermdp')

                        <br>

                        <form action="{{ action('ConnexionController@Logout') }}" method='GET'>
                        {{ csrf_field() }}
                        <button class='btn btn-secondary' type='submit'>Déconnexion</button>
                        </form>
                </div>
        </div>
</div>
</body>
@endsection

@section('scripts')
@endsection