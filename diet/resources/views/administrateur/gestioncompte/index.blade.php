@extends('layouts.master')

@section('styles')
@endsection
<style>
.pagination {
    margin-bottom: 0;
}

.link-fix{
    padding-top:10px !important;
    padding-bottom:10px !important;
}

</style>
<link rel="stylesheet" href="{{ asset('css/fancyCheckbox.css') }}">
@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">Gestion des comptes</h1>
   </div>
</div>
@if (Request::segment(4) == 'admin')
{{ Breadcrumbs::render('GestionAdmin') }}
@else
{{ Breadcrumbs::render('GestionClient') }}
@endif

<div class='container'>
        <div class='row'>

                @include('administrateur._side_menu')
                <div class='col-sm-12 col-md-9 col-lg-9'>
                    @if (true == false)
                    <div class='row'>
                        
                        <div class='col-sm-12 col-md-6 col-lg-6'>
                            <a class='btn btn-primary btn-lg btn-block' id='btn-client' name=client type=button href='{{action("GestionCompteController@ReadClient")}}'>Clients</a>
                        </div>
                        <div class='col-sm-12 col-md-6 col-lg-6'>
                        
                            <a class='btn btn-primary btn-lg btn-block' id='btn-admin' name=admin type=button href='{{action("GestionCompteController@ReadAdmin")}}'>Administrateurs</a>
                        </div>
                    </div>
                    @endif


                    <div id=liste-compte>
                        <?php
                        
                        echo $html;
                        ?>
                    </div>
                </div>
        </div>
</div>
</body>
@endsection

@section('scripts')
<script>
    var routes = [];
    routes.push("{{ action('GestionCompteController@ActiverCompte') }}");
    routes.push("{{ action('GestionCompteController@DesactiverCompte') }}");
    routes.push("{{ action('GestionCompteController@Regenerer') }}");
</script>

<script>

function activate(what){
    activer(what);
}

function desactivate(what){
    desactiver(what);
}

function regen(what){
    reset(what);
}

</script>

<script src="{{ asset('js/gestion_compte.js') }}"></script>
<script src="{{ asset('js/shift_checkbox.js') }}"></script>
@endsection