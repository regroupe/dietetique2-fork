@extends('layouts.master')

@section('styles')
@endsection

@section('content')
<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
   <div class="container">
        <h1 class="display-4">Ajouter des comptes</h1>
   </div>
</div>

{{ Breadcrumbs::render('AjouterCompte') }}

<div class='container'>

        <div class='row'>

                @include('administrateur._side_menu')
                <div class='col-sm-12 col-md-9 col-lg-9'>

                        @if (\Session::has('msg'))
                            <div class="alert alert-success">
                                    <strong>{!! \Session::get('msg') !!}</strong>
                            </div>
                        @endif

                    <div class='row'>
                        <!--Formulaire de génération de comptes-->
                        <div class='col-sm-12 col-md-6 col-lg-6'>
                            <h3>Générer des comptes</h3>
                            <form method="POST" 
                            @if (session('admin')->super == 1 && session('admin')->master == 1) 
                                action="{{action('GestionCompteController@GenererPOSTMaster')}}"
                            @else
                                action="{{action('GestionCompteController@GenererPOSTSuper')}}"
                            @endif
                            >
                                {{ csrf_field() }}
                                    <div class="form-group"> 
                                        <label for='nom'>Nom</label>
                                        <input class='form-control' id='nom' name='nomUsr' value='{{old('nomUsr')}}'>
                                        <div class="text-danger">{{ $errors->first('nomUsr') }}</div>
                                    </div>

                                    <div class="form-group"> 
                                        <label>Type</label>
                                        <select class='form-control' name=typeUsr>
                                            @if (session('admin')->super == 1 && session('admin')->master == 1)
                                                <option value=0
                                                @if (old('typeUsr') == 0)
                                                    selected
                                                @endif
                                                >Opérateur</option>
                                                <option value=1
                                                @if (old('typeUsr') == 1)
                                                    selected
                                                @endif
                                                >Super-Opérateur</option>
                                                <option value=3
                                                @if (old('typeUsr') == 3)
                                                    selected
                                                @endif
                                                >Bénévole</option>
                                            @else
                                                <option value=0
                                                @if (old('typeUsr') == 0)
                                                    selected
                                                @endif
                                                >Opérateur</option>
                                                <option value=3
                                                @if (old('typeUsr') == 3)
                                                    selected
                                                @endif
                                                >Bénévole</option>
                                            @endif
                                        </select>
                                        <div class="text-danger">{{ $errors->first('typeUsr') }}</div>
                                    </div>

                                     <div class="form-group"> 
                                            <label>Nombre</label>
                                            <input class='form-control' name=nbUsr type=number value={{old('nbUsr')}}>
                                            <div class="text-danger">{{ $errors->first('nbUsr') }}</div>
                                    </div>

                                <button class='btn btn-primary' type=submit>Générer</button>
                            </form>
                        </div>

                        <!--Formulaire de création de compte (Seulement pour master)-->
                        @if (session('admin')->super == 1 && session('admin')->master == 1) 
                        <div  class='col-sm-12 col-md-6 col-lg-6'>
                            <h3>Créer un compte</h3>
                            <form method="POST" action="{{action('GestionCompteController@CreerCompte')}}">
                                {{ csrf_field() }}

                                <div class="form-group"> 
                                    <label for='nom'>Nom d'utilisateur</label>
                                    <input class='form-control' id='nom' name='nomUsr2' value="{{old('nomUsr2')}}">
                                    <div  class="text-danger">{{ $errors->first('nomUsr2') }}</div>
                                </div>

                                <div class="form-group"> 
                                    <label>Type</label>
                                    <select class='form-control' name=typeUsr2>
                                            <option value=0
                                            @if (old('typeUsr2') == 0)
                                                selected
                                            @endif
                                            >Opérateur</option>
                                            <option value=1
                                            @if (old('typeUsr2') == 1)
                                                selected
                                            @endif
                                            >Super-Opérateur</option>
                                            <option value=2
                                            @if (old('typeUsr2') == 2)
                                                selected
                                            @endif
                                            >Administrateur</option>
                                            <option value=3
                                            @if (old('typeUsr') == 3)
                                                selected
                                            @endif
                                            >Bénévole</option>
                                    </select>
                                    <div class="text-danger">{{ $errors->first('typeUsr2') }}</div>
                                </div>
                                <button class='btn btn-primary' type=submit>Créer</button>
                            </form>
                        </div>
                        @endif
                        </div>
                    </div>
                    </div>
                </div>
        </div>
</div>
</body>
@section('scripts')
@endsection
    @if (\Session::has('passList'))
            <?php
                $toShow = "Comptes;Mots de passes\n";
                foreach(session()->get('passList') as $i => $pass)
                {
                    $toShow .= session()->get('account')[$i].';'.session()->get('passList')[$i]."\n";
                }
                echo '<script>var show = '.json_encode($toShow).'</script>'
            ?>
            <script>
                 var element = document.createElement('a');
                            element.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(show));
                            element.setAttribute('download', "Mots de passes.csv");

                            element.style.display = 'none';
                            document.body.appendChild(element);

                            element.click();

                            document.body.removeChild(element);
            </script>
    @endif
@endsection