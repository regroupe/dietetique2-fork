<h3>Administrateurs</h3>

<form method=POST action='{{action("GestionCompteController@RechercheAdminPOST")}}'>
    {{ csrf_field() }}
    <input id='input-rech' name=search>
    <button type=submit>Rechercher</button>
</form>

<table class='table table-sm table-bordered table-striped table-hover'>
                    <thead class='account-head'>
                        <th></th>
                        <th>Nom de la marque</th>
                        <th>Actif</th>
                    </thead>
                        @foreach($marque as $i=>$uneMarque)
                                <tr class='account-row'>
                                    <th data-id='{{$uneMarque->nom_marque}}'><input type=checkbox></th>
                                    <td>{{$uneMarque->nom_marque}}</td>
                                    
                                    <td>@if($uneMarque->actif)
                                        Oui
                                        @else Non
                                        @endif
                                    </td> 
                                </tr>
                        @endforeach
</table>

<div>
    <button id='activer' data-tpe='activ-admin' onclick='activer(this)'>Activer</button>
    <button id='desactiver' data-tpe='desact-admin' onclick='desactiver(this)'>Désactiver</button>
</div>
<div>
{{ $marque->links() }}
</div>