
<h4><i class="fa fa-search-plus" aria-hidden="true"></i> Critères de recherche</h4>
<form method=POST action='{{action("GestionCompteController@RechercheAdminPOST")}}'>
        {{ csrf_field() }}
        <div class="form-row">
            <div class="form-group col-sm-2 col-md-2 col-lg-2"> 
                        <select class='form-control' id='actif-rech' name=actif>
                            <option value=2 {{ $searchInfo[0] == 2 ? 'selected' : '' }}>Tous</option>
                            <option value=1 {{ $searchInfo[0] == 1 ? 'selected' : '' }}>Actif</option>
                            <option value=0 {{ $searchInfo[0] == 0 ? 'selected' : '' }}>Inactif</option>
                        </select>
            </div>

            <div class="form-group col-sm-10 col-md-10 col-lg-10"> 
                    <div class='input-group'>
                    <input class="form-control" id='input-rech' name=search>
                        <div class="input-group-append">
                            <button class="btn btn-outline-success" type="submit">Rechercher</button>
                        </div>
                    </div>
            </div>
        </div>
    <h4>Administrateurs</h4>
    <table class='table table-md table-striped table-responsive-sm'>
                        <thead class="thead-dark">
                            <tr>
                                <th colspan="2">
                                    <a type="button" class="btn btn-secondary" href="{{action('GestionCompteController@ReadAjouter')}}">Ajouter des comptes</a>
                                </th>
                                <th colspan="3">
                                    <div class="text-right">
                                        <label>Afficher</label>
                                        <select name="filter_pages" class="search-input form-control" onchange="this.form.submit()" style="display: inline-block; max-width: 80px">
                                            <option value="15" {{ $searchInfo[1] == 15 ? 'selected' : '' }}>15</option>
                                            <option value="25" {{ $searchInfo[1] == 25 ? 'selected' : '' }}>25</option>
                                            <option value="50" {{$searchInfo[1] == 50 ? 'selected' : '' }}>50</option>
                                            <option value="100" {{ $searchInfo[1] == 100 ? 'selected' : '' }}>100</option>
                                        </select>
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>Nom d'utilisateur</th>
                                <th>Droits</th>
                                <th>Dernière modification</th>
                                <th>Actif</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($compte as $i=>$unCompte)
                                    <tr class='account-row'>
                                        <th data-id='{{$unCompte->username}}'>
                                            <label class="ck-container">
                                                <input type=checkbox class='chkbox'>
                                                <span class="checkmark"></span>
                                            </label>
                                        </th>
                                        <td>{{$unCompte->username}}</td>
                                        <td>
                                        @if($unCompte->super == true)
                                            @if($unCompte->master == true)
                                                Administrateur
                                            @else Super-Opérateur
                                            @endif
                                        @else Opérateur
                                        @endif
                                        
                                        </td>

                                        <td>{{$unCompte->last_modif}}</td>   
                                        <td>@if($unCompte->actif)
                                            Oui
                                            @else Non
                                            @endif
                                        </td> 
                                    </tr>
                            @endforeach
                        </tbody>
                        <tfoot class='table-dark'>
                            <tr>
                                    <td colspan="5">
                                        <button class='btn btn-primary' type='button' id='activer' data-tpe='activ-admin' onclick='activate(this)'>Activer</button>
                                        <button class='btn btn-secondary' type='button' id='desactiver' data-tpe='desact-admin' onclick='desactivate(this)'>Désactiver</button>
                                        <button class='btn btn-success'type='button' id='reset' data-tpe='reset-admin' onclick='regen(this)'>Régénérer mots de passes</button>
                                    </td>
                            </tr>

                            <tr>
                                    <td class="align-middle"colspan="5">{{ $compte->links() }}</td>
                            </tr>
                        </tfoot>
    </table>
</form>
