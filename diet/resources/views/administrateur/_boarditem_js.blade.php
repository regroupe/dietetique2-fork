<div class="board-row no-{{$id}} col-sm-12 col-md-12 col-lg-6 mb-3">
    <div class="card card-{{$id}} border-primary">
        <div class="card-body card-cursor">
            <div class="form-group">
                <label for='vue'>Vue</label>
                <div class="input-group">
                    <select class="custom-select" name="board_item[]">
                        @foreach ($partials_list[0] as $i => $name)
                            <option value="{{$partials_list[1][$i]}}">
                                {{$name}}
                            </option>
                        @endforeach
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button" onclick="DeleteRow('{{$id}}');"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col-6">
                    <label for='size'>Grandeur</label>
                    <select class="custom-select" name="board_size[]" onchange="ChangeSize(this, '{{$id}}')">
                        <option value="small" >Petit</option>
                        <option value="medium" selected>Medium</option>
                        <option value="large">Large</option>
                    </select>
                </div>
                <div class="col-6">
                    <label for='color'>Couleur</label>
                    <select class="custom-select" name="board_color[]" onchange="ChangeColor(this, '{{$id}}')">
                        <option value="primary">Bleu</option>
                        <option value="info">Bleu pâle</option>
                        <option value="secondary">Gris</option>
                        <option value="warning">Jaune</option>
                        <option value="dark">Noir</option>
                        <option value="danger">Rouge</option>
                        <option value="success">Vert</option>
                    </select>
                </div>
            </div>
            
        </div>
    </div>
</div>