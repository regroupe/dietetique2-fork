<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
?>

@if ($partial->vue_size == 'large')  

        <?php
            $articles = Controllers\ArticleController::ReadLast(5);
        ?>

        <div class="{{GetSize($partial->vue_size)}}">
                <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Dernier {{Controllers\PersonnalisationController::GetSellerName(true, false)}} ajouté(s)</h5>
                        <div class="card-body">
                            <ul class="list-group list-group-flush">
                                @foreach ($articles as $article)
                                    <li class="list-group-item">
                                        <div class="row">
                                                <div class="col-8">
                                                        <a class="linkhov" href='{{ action('BoutiqueController@View_GET', $article->rowid) }}'>{{ $article->nom }}
                                                        {{ $article->format_quantite }} {{ $article->format }}
                                                        <?php if ($article->bio) : ?>
                                                            <i class="fa fa-leaf" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Biologique"></i>
                                                        <?php endif; ?>
                                                        <?php if ($article->perissable) : ?>
                                                            <i class="fa fa-apple" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Périssable"></i>
                                                        <?php endif; ?>
                                                        </a>
                                                </div>
                                                <div class="col-4">
                                                         {{ $article->created_by }}
                                                </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>   
                        </div>
                    <button type="button" onclick="window.location.href = '{{ action('ArticleController@Index') }}'" class="btn btn-{{$partial->vue_color}}">Voir tous les {{Controllers\PersonnalisationController::GetSellerName(true, false)}}</button>
                </div>
        </div>
@elseif ($partial->vue_size == 'medium')

        <?php
            $articles = Controllers\ArticleController::ReadLast(4);
        ?>

        <div class="{{GetSize($partial->vue_size)}}">
                <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                        <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Dernier {{Controllers\PersonnalisationController::GetSellerName(true, false)}} ajouté(s)</h5>
                        <div class="card-body">
                            <ul class="list-group list-group-flush">
                                @foreach ($articles as $article)
                                    <li class="list-group-item">
                                        <div class="row">
                                                <div class="col-8">
                                                    <a class="linkhov" href='{{ action('BoutiqueController@View_GET', $article->rowid) }}'>
                                                            <span style="font-size:11pt;">{{ $article->nom }} {{ $article->format_quantite }} {{ $article->format }}</span>
                                                    </a>
                                                </div>
                                                <div class="col-4">
                                                         {{ $article->created_by }}
                                                </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>   
                        </div>
                        <button type="button" onclick="window.location.href = '{{ action('ArticleController@Index') }}'" class="btn btn-{{$partial->vue_color}}">Voir tous les {{Controllers\PersonnalisationController::GetSellerName(true, false)}}</button>
                </div>
        </div>
@else
    <?php
        $article = Controllers\ArticleController::ReadLast(1);
    ?>

        <div class="{{GetSize($partial->vue_size)}}">
                <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                        <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Dernier {{Controllers\PersonnalisationController::GetSellerName(false, false)}} ajouté</h5>
                        @if (count($article) > 0)
                            <a class="card-link" href='{{ action('BoutiqueController@View_GET', $article[0]->rowid) }}'>
                                    <div class="card-body">
                                            <div>
                                                {{ $article[0]->nom }}
                                            </div> 
                                            <div>
                                                <small>{{ $article[0]->created_by }}</small>
                                            </div> 
                                    </div>
                            </a>
                        @else
                            <a class="card-link" href='{{ action('ArticleController@Index') }}'>
                                    <div class="card-body">
                                            <div>
                                                Ajouter des produit(s)
                                            </div> 
                                    </div>
                            </a>
                        @endif
                </div>
        </div>
@endif