<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $periode = Controllers\PeriodeController::GetActivePeriod();
    if ($periode != null)
    $articles = Controllers\PeriodeController::ArticlePeriodMostCompleted($periode->rowid);
?>



@if ($periode != null)
        @if ($partial->vue_size == 'large')  
                <div class="{{GetSize($partial->vue_size)}}">
                        <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                                <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white"> {{Controllers\PersonnalisationController::GetSellerName(true, true)}} les plus près de la quantité minimum d'achat </h5>
                                <div class="card-body ">
                                <?php $max = 4;?>
                                @for ($index = 0; $index < count($articles) && $index < $max; $index++)
                                        <?php $a = $articles[$index];?>
                                        
                                        @if ($a->quantite_commande < $a->quantite_minimum)
                                                @include('periodecommande._article')
                                        @else
                                                <?php $max += 1?>
                                        @endif
                                @endfor
                                </div>
                                <button type="button" onclick="window.location.href = '{{ action('PeriodeController@IndexProgression', $periode->rowid) }}'" class="btn btn-{{$partial->vue_color}}">Progression des ventes</button>
                        </div>
                </div>
        @elseif ($partial->vue_size == 'medium')
        <div class="{{GetSize($partial->vue_size)}}">
                <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                        <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white"> {{Controllers\PersonnalisationController::GetSellerName(true, true)}} près du minimum d'achat </h5>
                        <div class="card-body ">
                        <ul class="list-group list-group-flush">
                                <?php $max = 4;?>
                                @for ($index = 0; $index < count($articles) && $index < $max; $index++)
                                        <?php $a = $articles[$index];?>
                                        
                                        @if ($a->quantite_commande < $a->quantite_minimum)
                                        <li class="list-group-item">
                                                <div class="row">
                                                        <div class="col-9">
                                                                <a class="linkhov" href='{{ action('BoutiqueController@View_GET', $a->rowid) }}'>
                                                                <span style="font-size:11pt;">{{ $a->nom }} {{ $a->article->format_quantite }}{{ $a->article->format }}</span>
                                                                </a>
                                                        </div>
                                                        <div class="col-3">
                                                                {{ $a->quantite_commande }} / {{ $a->article->quantite_minimum }}
                                                        </div>
                                                </div>
                                        </li>
                                        @else
                                        <?php $max += 1?>
                                        @endif
                                @endfor
                        </ul>
                        </div>
                        <button type="button" onclick="window.location.href = '{{ action('PeriodeController@IndexProgression', $periode->rowid) }}'" class="btn btn-{{$partial->vue_color}}">Progression des ventes</button>
                </div>
        </div>
        @else
                <div class="{{GetSize($partial->vue_size)}}">
                        <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                                <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Progression des ventes</h5>
                                <a class="card-link" href='{{ action('PeriodeController@IndexProgression', $periode->rowid) }}'>
                                        <div class="card-body text-center">
                                                Voir la progression.
                                        </div>
                                </a>
                        </div>
                </div>
        @endif
@else
<div class="{{GetSize($partial->vue_size)}}">
        <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                <h5  class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Progression des ventes</h5>
                <a class="card-link" href='{{action('PeriodeController@IndexPeriode')}}'>
                        <div class="card-body text-center">
                                <div>Aucune période active.</div>
                                <div>Voir les périodes.</div>
                        </div>
                </a>
        </div>
</div>
@endif