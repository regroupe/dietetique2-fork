<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    use Carbon\Carbon;
    use Carbon\CarbonInterface;
    $period_active = Controllers\PeriodeController::GetActivePeriod();
?>

@if ($period_active != null)
        <?php
                //Prepare server side variables based on the active period.
                $date_debut_today = Controllers\PeriodeController::DaysLeft($period_active->date_debut, true);
                $date_fin_today = Controllers\PeriodeController::DaysLeft($period_active->date_fin, true);
                $date_d_date_f = Controllers\PeriodeController::DaysLeft_2var($period_active->date_debut, $period_active->date_fin, true);

                if($date_fin_today == 0){
                        $progress_days = "Termine dans moins d'une journée";
                }else{
                        $progress_days = "Termine dans ".$date_fin_today." jours";
                }

                $progress = $date_debut_today / $date_d_date_f* 100;

        ?>
        @if ($partial->vue_size != 'small')
                <?php
                         //Prepare string number of days left. 
                        if($date_fin_today == 0){
                                $progress_days = "Termine dans moins d'une journée";
                        }else{
                                $progress_days = "Termine dans ".$date_fin_today." jours";
                        }
                ?>
                <div class="{{GetSize($partial->vue_size)}} mb-3">
                        <div class="card {{GetBorderColor($partial->vue_color)}}">
                                <h5 class="card-header {{GetBackGroundColor($partial->vue_color)}} text-white">Période de commande active (#{{$period_active->rowid}})</h5>
                                <div class="card-body">
                                        <div class="progress" data-toggle="tooltip" data-placement="bottom" title="{{$progress_days}}">
                                                <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress >= 100 ? 'bg-success' : '' }}" style="width: {{ $progress }}%" role="progressbar" aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class='row progress-date'>
                                                <div class='col-sm-6'>
                                                        <div>{{Controllers\PeriodeController::ParseDateLocale($period_active->date_debut)}}</div>
                                                </div>
                                                <div class='col-sm-6'>
                                                        <div style='text-align:right;'>{{Controllers\PeriodeController::ParseDateLocale($period_active->date_fin)}}</div>
                                                </div>
                                        </div>
                                        <div style="float:right">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="bottom" title="Dates et cueillettes" onclick="window.location.href = '{{action('PeriodeController@EditPeriode_GET', $period_active->rowid)}}'"><i class="fa fa-info-circle" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-success" data-toggle="tooltip" data-placement="bottom" title="Progression des ventes" onclick="window.location.href = '{{action('PeriodeController@IndexProgression', $period_active->rowid)}}'"><i class="fa fa-align-left" aria-hidden="true"></i></button>
                                        </div>
                                </div>
                        </div>
                </div>
        @else
                <?php
                //Prepare string number of days left. 
                if($date_fin_today == 0){
                        $progress_days = "<1";
                }else{
                        $progress_days = $date_fin_today;
                }
                ?>

                <div class="{{GetSize($partial->vue_size)}} mb-3">
                        <div class="card {{GetBorderColor($partial->vue_color)}}">
                                <h5 class="card-header {{GetBackGroundColor($partial->vue_color)}} text-white">Période active (#{{$period_active->rowid}})</h5>
                                <a class="card-link" href='{{action('PeriodeController@EditPeriode_GET', $period_active->rowid)}}'>
                                        <div class="card-body small-card text-center align-self-center">
                                                <span class="huge-font align-middle">{{$progress_days}}</span><span> jours restants</span>
                                        </div>
                                </a>
                        </div>
                </div>
        @endif
@else
<div class="{{GetSize($partial->vue_size)}}">
        <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Aucune période active</h5>
                <a class="card-link" href='{{action('PeriodeController@IndexPeriode')}}'>
                        <div class="card-body text-center">
                                Voir les périodes.
                        </div>
                </a>
        </div>
</div>

@endif