<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    use Carbon\Carbon;
    use Carbon\CarbonInterface;
    $period_ecoule = Controllers\PeriodeController::GetLastFinishPeriod();
    
?>

@if ($period_ecoule != null)
        @if ($partial->vue_size != 'small')
                <div class="{{GetSize($partial->vue_size)}} mb-3">
                        <div class="card {{GetBorderColor($partial->vue_color)}}">
                                <h5 class="card-header {{GetBackGroundColor($partial->vue_color)}} text-white">Dernière période écoulée (#{{$period_ecoule->rowid}})</h4>
                                <div class="card-body">
                                        <div class="start-in">Terminée le {{Controllers\PeriodeController::ParseDateLocale($period_ecoule->date_fin)}}</div>
                                        <?php
                                                $picks = Controllers\PeriodeController::PeriodeCueillette($period_ecoule->rowid);
                                        ?>

                                        @if (count($picks) < 1)
                                                <strong class='text-danger'>Aucune période de cueillette définie.</strong>
                                        @else
                                        <h6>Cueillettes</h6>
                                        <ul>
                                                @foreach ($picks as $p)
                                                        <li><i class="cueil">{{$p->emplacement}}</i> le 
                                                                <i class="cueil">{{Controllers\PeriodeController::ParseDateTimeLocale($p->date_debut)}}</i> 
                                                                jusqu'au 
                                                                <i class="cueil">{{Controllers\PeriodeController::ParseDateTimeLocale($p->date_fin)}}</i>.
                                                        </li>
                                                @endforeach
                                        </ul>
                                        @endif

                                        <div style="float:right">
                                                <button type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="Dates et cueillettes" onclick="window.location.href = '{{action('PeriodeController@EditPeriode_GET', $period_ecoule->rowid)}}'"><i class="fa fa-info-circle" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-success" data-toggle="tooltip" data-placement="bottom" title="Progression des ventes" onclick="window.location.href = '{{action('PeriodeController@IndexProgression', $period_ecoule->rowid)}}'"><i class="fa fa-align-left" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="bottom" title="Prise des commandes" onclick="window.location.href = '{{action('PeriodeController@OrderPickup_GET', $period_ecoule->rowid)}}'"><i class="fa fa-check-square" aria-hidden="true"></i></button>
                                        </div>
                                </div>
                        </div>
                </div>
        @else
                <div class="{{GetSize($partial->vue_size)}} mb-3">
                        <div class="card {{GetBorderColor($partial->vue_color)}}">
                                <h5 class="card-header {{GetBackGroundColor($partial->vue_color)}} text-white">Dernière période (#{{$period_ecoule->rowid}})</h5>
                                <div class="card-body text-center align-middle">
                                        <?php
                                                $picks = Controllers\PeriodeController::PeriodeCueillette($period_ecoule->rowid);
                                        ?>

                                        @if (count($picks) < 1)
                                                <div class='text-danger mb-3'><strong>Aucune cueillette.</strong></div>
                                        @else
                                                <div class="start-in">Terminée le </div>
                                                <div class="start-in mb-1">{{Controllers\PeriodeController::ParseDateLocale($period_ecoule->date_fin)}}</div>
                                        @endif
                                        <button type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="Dates et cueillettes" onclick="window.location.href = '{{action('PeriodeController@EditPeriode_GET', $period_ecoule->rowid)}}'"><i class="fa fa-info-circle" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-outline-success" data-toggle="tooltip" data-placement="bottom" title="Progression des ventes" onclick="window.location.href = '{{action('PeriodeController@IndexProgression', $period_ecoule->rowid)}}'"><i class="fa fa-align-left" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="bottom" title="Prise des commandes" onclick="window.location.href = '{{action('PeriodeController@OrderPickup_GET', $period_ecoule->rowid)}}'"><i class="fa fa-check-square" aria-hidden="true"></i></button>
                                </div>

                        </div>
                </div>
        @endif
@else
<div class="{{GetSize($partial->vue_size)}}">
        <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                <h5 style="font-size:14pt;" class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Aucune période écoulée</h5>
                <a class="card-link" href='{{action('PeriodeController@IndexPeriode')}}'>
                        <div class="card-body text-center">
                                Voir les périodes.
                        </div>
                </a>
        </div>
</div>

@endif