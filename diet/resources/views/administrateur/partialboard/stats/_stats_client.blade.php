<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    use Carbon\Carbon;
    $now = Carbon::now();
?>
@if ($partial->vue_size != 'small')
    <div class="{{GetSize($partial->vue_size)}}">
            <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Statistique sur les clients</h5>
                <div class="card-body">
                    <div>
                        <strong class="medium-font" >{{Controllers\StatistiquesController::ClientActifMois($now->year,$now->month)}}</strong>
                        clients actifs ce mois-ci.
                    </div>
                    <div>
                        <strong class="medium-font">{{Controllers\StatistiquesController::NbClients()}}</strong>
                        clients au total.
                    </div>
                </div>
            </div>
    </div>
@else
    <div class="{{GetSize($partial->vue_size)}}">
            <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Stats sur les clients</h5>
                <div class="card-body">
                    <div>
                        <strong class="medium-font" >{{Controllers\StatistiquesController::ClientActifMois($now->year,$now->month)}}</strong> 
                         clients actifs ce mois-ci.
                    </div>
                    <div>
                        <strong class="medium-font">{{Controllers\StatistiquesController::NbClients()}}</strong>
                        clients au total.
                    </div>
                </div>
            </div>
    </div>
@endif