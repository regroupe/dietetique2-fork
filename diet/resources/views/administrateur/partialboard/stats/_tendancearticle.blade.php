<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    use Carbon\Carbon;
    $now = Carbon::now();
?>

@if ($partial->vue_size == 'large')  
        <div class="{{GetSize($partial->vue_size)}}">
                <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                    <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Tendance des ventes de cette année</h5>
                    <div class="card-body text-center">
                            <div class="col-8 offset-2">
                                    {!! Controllers\StatistiquesController::TendanceCommandesPartial($now->year,false) !!}
                            </div>
                    </div>
                </div>
        </div>
@elseif ($partial->vue_size == 'medium')
        <div class="{{GetSize($partial->vue_size)}}">
                <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                        <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Tendance des ventes de cette année</h5>
                        <div class="card-body small-card text-center">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-10">
                                        {!! Controllers\StatistiquesController::TendanceCommandesPartial($now->year,false) !!}
                                </div>
                            </div>
                        </div>
                </div>
        </div>
@else
        <div class="{{GetSize($partial->vue_size)}}">
                <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                        <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Tendance des ventes de cette année</h5>
                        <div class="card-body small-card text-center">
                            <div class="col-12">
                                    {!! Controllers\StatistiquesController::TendanceCommandesPartial($now->year,false) !!}
                            </div>
                        </div>
                </div>
        </div>
@endif