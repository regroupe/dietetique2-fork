
<!--Set different view for different sizes-->
@if ($partial->vue_size == 'large')  

        <!--Get corresponding bootstrap columns with the GetSize function-->
        <div class="{{GetSize($partial->vue_size)}}">

                <!--Set the card border color with the GetBorderColor function and set a margin bottom so your view doesn't collide (mb-3).-->
                <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">

                        <!--Set the card-header background color with the GetBackgroundColor function-->
                        <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Vue large</h5>

                        <a class="card-link" href='{{action('AdminController@EditBoard_GET')}}'>
                                <div class="card-body text-center">
                                        Modifier le tableau de bord.
                                </div>
                        </a>
                </div>
        </div>
@elseif ($partial->vue_size == 'medium')
        <div class="{{GetSize($partial->vue_size)}}">
                <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                        <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Vue Moyenne</h5>
                        <a class="card-link" href='{{action('AdminController@EditBoard_GET')}}'>
                                <div class="card-body text-center">
                                        Modifier le tableau de bord.
                                </div>
                        </a>
                </div>
        </div>
@else
        <div class="{{GetSize($partial->vue_size)}}">
                <div class="card {{GetBorderColor($partial->vue_color)}} mb-3">
                        <h5 class="card-header {{GetBackgroundColor($partial->vue_color)}} text-white">Petite vue</h5>
                        <a class="card-link" href='{{action('AdminController@EditBoard_GET')}}'>
                                <div class="card-body text-center">
                                        Modifier le tableau de bord.
                                </div>
                        </a>
                </div>
        </div>
@endif