<?php
        use App\Http\Controllers;
?>
<div class='col-sm-12 col-md-3 col-lg-3'>
        <ul class="nav flex-column">
                <h4><i class="fa fa-align-justify"></i> Mon compte</h4>
                <hr class="lehr">
                @if (session('admin')->super == 1)
                <li class="nav-item">
                        <a class="nav-link itm-clr  {{ Request::segment(2) == 'tableau-de-bord' ? 'active-clr' : '' }}" href='{{ action("AdminController@IndexTableau") }}'>Tableau de bord</a>
                </li>      
                @endif
                <hr class="lehr">
                <li class="nav-item">
                        <a class="nav-link itm-clr  {{ Request::segment(2) == 'compte' ? 'active-clr' : '' }}" href='{{ action("AdminController@IndexCompte") }}'>Informations de compte</a>
                </li>        
                <hr class="lehr">
                <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(3) == 'articles' ? 'active-clr' : '' }}" href='{{ action("ArticleController@Index") }}'>{{Controllers\PersonnalisationController::GetSellerName(true, true)}}</a></li>
                <hr class="lehr">
       
                <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(3) == 'marque' ? 'active-clr' : '' }}" href='{{ action("MarqueController@Index") }}'>Marques</a></li>
                <hr class="lehr">
                <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(3) == 'fournisseur' ? 'active-clr' : '' }}" href='{{ action("FournisseurController@ReadFournisseur") }}'>Fournisseurs</a></li>
                <hr class="lehr">
                <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(3) == 'unite-mesure' ? 'active-clr' : '' }}" href='{{ action("UniteMesureController@Index") }}'>Unités de mesure</a></li>
                <hr class="lehr">
@if (session('admin')->super == 1)
                <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(3) == 'categorie' ? 'active-clr' : '' }}" href='{{ action("CategorieController@Index") }}'>Catégories</a></li>         
                <hr class="lehr">
 
                <li class="nav-item">
                        <a class="nav-link itm-clr {{ Request::segment(3) == 'compte' ? 'active-clr' : '' }}" data-toggle="collapse" href="#item-1">Comptes</a>
                        <div id="item-1" class="collapse">
                                <ul class="nav flex-column ml-3">
                                        <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(4) == 'client' ? 'active-clr' : '' }}" href='{{ action("GestionCompteController@ReadClient") }}'>Clients</a></li>
                                        <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(4) == 'admin' ? 'active-clr' : '' }}" href='{{ action("GestionCompteController@ReadAdmin") }}'>Admins</a></li>
                                        <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(4) == 'ajouter' ? 'active-clr' : '' }}" href='{{ action("GestionCompteController@ReadAjouter") }}'>Ajouter des comptes</a></li>
                                </ul>
                        </div>
                </li>
                        <hr class="lehr">
                        <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(3) == 'articles-en-vente' ? 'active-clr' : '' }}" href='{{ action("ProduitOffertController@IndexProduit") }}'>{{Controllers\PersonnalisationController::GetSellerName(true, true)}} en vente</a></li>
                        <hr class="lehr">
@endif
@if (session('admin')->take_order)
                        <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(3) == 'periodes' ? 'active-clr' : '' }}" href='{{ action("PeriodeController@IndexPeriode") }}'>Périodes de commande</a></li>
                        <hr class="lehr">
@endif
@if (session('admin')->super == 1)
                        <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(2) == 'statistiques' ? 'active-clr' : '' }}" href='{{ action("StatistiquesController@Index") }}'>Statistiques</a></li>
                        <hr class="lehr">
@endif
        

@if (session('admin')->super == 1 && session('admin')->master ==1)

                <li class="nav-item"><a class="nav-link itm-clr {{ Request::segment(3) == 'themes' ? 'active-clr' : '' }}" href='{{ action("PersonnalisationController@Index") }}'>Thèmes</a></li>
                <hr class="lehr">
@endif

        </ul>
</div>