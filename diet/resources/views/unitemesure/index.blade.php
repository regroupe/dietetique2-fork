
@extends('layouts.master')

@section('styles')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/fancyCheckbox.css') }}">
@endsection

@section('content')


<body>
<div id="category-banner" class="jumbotron jumbotron-fluid">
        <div class="container">
                <h1 class="display-4">Liste des unités de mesure</h1>
        </div>
    </div>

{{ Breadcrumbs::render('UniteMesure') }}

<div class = "container">
<div class = "row">

@include('administrateur._side_menu')

<div class='col-sm-12 col-md-9 col-lg-9'>


<div id= "liste_fournisseur">
    
    @if (\Session::has('success'))
            <div class="alert alert-success">
                    <strong>{!! \Session::get('success') !!}</strong>
            </div>
    @endif


    <h4><i class="fa fa-plus"></i></i> Ajouter une nouvelle unité de mesure</h4>
    <form method=POST action='{{action("UniteMesureController@RegisterUnite_FORM")}}'>
        {{ csrf_field() }}
        <div class="form-group"> 
            <div class='input-group'>
                <input class='form-control' id='nom' name='nom_unite' placeholder="Nom de l'unité de mesure" value='{{old('nom_unite')}}'>
                <div class="input-group-append">
                    <button class="btn btn-outline-primary" type="submit">Ajouter</button>
                </div>
            </div>
            <div class="text-danger">{{ $errors->first('nom_unite') }}</div>
        </div>
    </form>



    <h4><i class="fa fa-search-plus" aria-hidden="true"></i> Critères de recherche</h4>
    <form method=POST action='{{action("UniteMesureController@RechercheUnite_POST")}}'>
        {{ csrf_field() }}
        <div class="form-row">
            <div class="form-group col-sm-2 col-md-2 col-lg-2"> 
                        <select class='form-control' id='actif-rech' name=actif>
                            <option value=2 {{ $searchInfo[0] == 2 ? 'selected' : '' }}>Tous</option>
                            <option value=1 {{ $searchInfo[0] == 1 ? 'selected' : '' }}>Actif</option>
                            <option value=0 {{ $searchInfo[0] == 0 ? 'selected' : '' }}>Inactif</option>
                        </select>

            </div>

            <div class="form-group col-sm-10 col-md-10 col-lg-10"> 
                    <div class='input-group'>
                    <input class="form-control" id='input-rech' name=search>
                        <div class="input-group-append">
                            <button class="btn btn-outline-success" type="submit">Rechercher</button>
                        </div>
                    </div>
            </div>
        </div>



    <div id="testliste">
    <table class='table table-md table-striped table-responsive-sm'>
                    <thead class='thead-dark account-head'>
                        <tr>
                            <th colspan="3">
                                <div class="text-right">
                                    <label>Afficher</label>
                                    <select name="filter_pages" class="search-input form-control" onchange="this.form.submit()" style="display: inline-block; max-width: 80px">
                                        <option value="15" {{ $searchInfo[1] == 15 ? 'selected' : '' }}>15</option>
                                        <option value="25" {{ $searchInfo[1] == 25 ? 'selected' : '' }}>25</option>
                                        <option value="50" {{ $searchInfo[1] == 50 ? 'selected' : '' }}>50</option>
                                        <option value="100" {{ $searchInfo[1] == 100 ? 'selected' : '' }}>100</option>
                                    </select>
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Nom de l'unité de mesure</th>
                            
                            <th>Actif</th>
                        </tr>
                    </thead>
                        @foreach($unites as $unite)
                                <tr class='account-row'>
                                    <th data-id='{{$unite->nom_unite}}'>
                                        <label class="ck-container">
                                            <input type=checkbox class='chkbox'>
                                            <span class="checkmark"></span>
                                        </label>
                                    </th>
                                    <td>{{$unite->nom_unite}}</td>
                                    <td>@if($unite->actif)
                                        Oui
                                        @else Non
                                        @endif
                                    </td> 
                                </tr>
                        @endforeach
                        <tfoot class='table-dark'>
                            <tr>
                                <td colspan="5">
                                    <button type="button" id='activer' data-tpe='activ-categorie' onclick='activate(this)' class="btn btn-primary">Activer</button>
                                    <button type="button" id='desactiver' data-tpe='desact-categorie' onclick='desactivate(this)' class = "btn btn-secondary">Désactiver</button>
                                    <button type="button" id="edit" onclick="editit(this)" class = "btn btn-success">Modifier</button>
                                </td>

                            </tr>
                            <tr>
                                    <td class="align-middle"colspan="5">{{$unites->links()}}</td>
                            </tr>
                        </tfoot>
</table>
</form>

    </div>
</div>
</div>
</div>


</body>
@endsection
@section('scripts')
<script src="{{ asset('js/shift_checkbox.js') }}"></script>
<script src="{{ asset('js/unite.js') }}"></script>

<script>
var routes = [];
routes.push('{{ action('UniteMesureController@EditUnite') }}');
routes.push('{{ action('UniteMesureController@ActiverUnite') }}');
routes.push('{{ action('UniteMesureController@DesactiverUnite') }}');


function activate(what){
    activer(what);
}

function desactivate(what){
    desactiver(what);
}

function editit(what){
    edit(what);
}
</script>
@endsection