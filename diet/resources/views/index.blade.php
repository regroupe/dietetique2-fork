<?php
    use App\Http\Controllers;
    $articles = Controllers\BoutiqueController::ReadRandomAccueil();
    $mainTheme = Controllers\PersonnalisationController::ReadActif();
?>
@extends('layouts.master')


@section('styles')
<!--
<link rel="stylesheet" href="{{ asset('css/accueil.css') }}">-->
<link rel="stylesheet" href="{{asset('css/periode.css')}}">
<link rel="stylesheet" href="{{ asset('css/slideshow.css') }}">
<link rel="stylesheet" href="{{ asset('css/boutique.css') }}">

@endsection

@section('content')
<body>


@if ($mainTheme->car1 || $mainTheme->car2 || $mainTheme->car3)
    <div id="carouselExampleIndicators" class="carousel slide carou-marge" data-ride="carousel">
      @if ($mainTheme->car1)
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class = "bgCarousel" style='background-image: url({{Storage::url($mainTheme->car1)}})'>
              @if ($mainTheme->car1_link  != '#' && $mainTheme->car1_msg)
                <a href="{{Controllers\PersonnalisationController::linkNameToLink($mainTheme->car1_link)}}" class="bouton btn btn-secondary"> {{$mainTheme->car1_msg}}</a>
              @elseif ($mainTheme->car1_msg != "" && $mainTheme->car1_msg)
                <span class="slide-text">{{$mainTheme->car1_msg}}</span>
              @endif
            </div>
          </div>
      @endif

      @if ($mainTheme->car2)
          <div class="carousel-item {{($mainTheme->car1) ? 'carousel-item' : 'carousel-inner active' }}">      
          <div class = "bgCarousel" style='background-image: url({{Storage::url($mainTheme->car2)}})'>
            @if ($mainTheme->car2_link  != '#' && $mainTheme->car2_msg)
              <a href="{{Controllers\PersonnalisationController::linkNameToLink($mainTheme->car2_link)}}" class="bouton btn btn-secondary">{{$mainTheme->car2_msg}}</a>
            @elseif ($mainTheme->car2_msg != "" && $mainTheme->car2_msg)
                <span class="slide-text">{{$mainTheme->car2_msg}}</span>
            @endif
            </div>    
          </div>
      @endif

      @if ($mainTheme->car3)
          <div class="carousel-item {{($mainTheme->car2) ? '' : 'active' }}">
              <div class = "bgCarousel" style='background-image: url({{Storage::url($mainTheme->car3)}})'>
                @if ($mainTheme->car3_link  != '#' && $mainTheme->car3_msg)
                  <a href="{{Controllers\PersonnalisationController::linkNameToLink($mainTheme->car3_link)}}" class="bouton btn btn-secondary">{{$mainTheme->car3_msg}}</a>
                @elseif ($mainTheme->car3_msg != "" && $mainTheme->car3_msg)
                  <span class="slide-text">{{$mainTheme->car3_msg}}</span>
                @endif
              </div>
          </div>
      </div>
      @endif

        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
    </div>

@else
  <div id="category-banner" class="jumbotron jumbotron-fluid">
        <div class="container">
                <h1 class="display-4">Accueil</h1>
        </div>
    </div>
@endif

    
{{ Breadcrumbs::render('Accueil') }}


<div class = "container">
  <div class= "row">

        <?php if (count($articles) > 0) : ?>
            <div class="col-12">
                <h2>{{Controllers\PersonnalisationController::GetSellerName(true, true)}} suggérés</h2>
                <div class="row">
                    <?php foreach ($articles as $article) : $progress = Controllers\ArticleController::ReadProgress($article); ?>
                        <div class="shop-product col-sm-12 col-md-4" data-id="{{ $article->rowid }}">
                            @include('boutique._article')
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        
    <div class = "col-12">
      <h2>Calendrier des commandes</h2>
          <div class="container">
            @include('periodecommande._activeperiod')
              <div class="row">
                  @include('periodecommande._calendrier')
              </div>
          </div>
    </div>
  </div>
</div>

  
@include('boutique._addedtobasket')
</body>

@endsection

@section('scripts')
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

var routes = {
    panier: {
        index: "{{ action('PanierController@Index') }}",
        update: "{{ action('PanierController@Update_POST') }}"
    }
};

</script>
<script src="{{ asset('js/boutique.js') }}"></script>
@endsection