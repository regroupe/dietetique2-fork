<?php
    // Prepare server side variables for the view.
    use App\Http\Controllers;
    $formatNumber = Controllers\PersonnalisationController::GetNumberFormat();
?>

@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/commande.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.printarea.css') }}">
@endsection

@section('content')
<div id="category-banner" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Commande #{{ $commande->rowid }}</h1>
    </div>
</div>

{{ Breadcrumbs::render('DetailCommande', $client, $commande) }}

<div class="container">
@if ($commande->prise)
    <div class="alert alert-info" role="alert">
        <strong>Cette commande a été récupérée.</strong>
    </div>
@endif
    <button class="btn btn-secondary mb-3"  type="button" data-toggle="tooltip" data-placement="top" title="Imprimer" onclick="printCommande()"><i class="fa fa-print"></i> Imprimer</button>
    <div id="toprint">
        <h2 class="text-center d-none" id="toprintheader">Commande #{{ $commande->rowid }}</h2>
        <div class="row">
                <div class="col-12">
                    <table class="table">
                        <thead class="bg-dark text-white">
                            <tr ><th colspan="2">Informations de facturation</th></tr>
                        </thead>
                        <tr>
                            <td> Numéro de référence:</td>
                            <td><b>{{ $commande->identifiant }}</b></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-user" aria-hidden="true"></i> Nom:</td>
                            <td><b>{{ $client->prenom }} {{ $client->nom }}</b></td>
                        </tr>
                        <tr>
                            <td>Courriel:</td>
                            <td><b>{{ $commande->email }}</b></td>
                        </tr>
                        <tr>
                            <td>Adresse:</td>
                            <td><b>{{ $commande->adresse }}</b></td>
                        </tr>
                        <tr>
                            <td>No Appt:</td>
                            <td><b>{{ $commande->no_app }}</b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>{{ $commande->ville }} {{ $commande->province }} {{ $commande->pays }}</b></td>
                        </tr>
                        <tr>
                            <td>Code Postal:</td>
                            <td><b>{{ $commande->code_postal }}</b></td>
                        </tr>
                        <tr>
                            <td>Téléphone:</td>
                            <td><b id="format-phone">{{ $commande->telephone }}</b></td>
                        </tr>
                        <tr>
                            <td>Poste:</td>
                            <td><b>{{ $commande->poste }}</td>
                        </tr>
                    </table>
                </div>
        
                <!--Medium Screen and + -->
                <div class="col-12 d-none d-md-block">
                    <table class="table">
                        <thead>
                            <tr class="table-info">
                                <th scope="col">#</th>
                                <th scope="col">{{Controllers\PersonnalisationController::GetSellerName(true, true)}}</th>
                                <th scope="col" class="hidden-sm-up">Qté</th>
                                <th scope="col" class="text-right">Prix</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $articleCount = 1; foreach($commande->commande_articles as $articleCommande) : ?>
                                <tr>
                                    <th scope="row">{{ $articleCount }}</th>
                                    <td>{{ $articleCommande->nom }}</td>
                                    <td>{{ $articleCommande->quantite_commande }} x {{ $articleCommande->format_quantite }}{{ $articleCommande->format }}</td>
                                    <!--<td class="text-right">{{ sprintf("%0.2f", Controllers\ArticleController::ReadPricePlus($articleCommande)) }}$</td>-->
                                    <td class="text-right">{{ number_format(truncate_number($articleCommande->prix * $articleCommande->quantite_commande, 1, 2), 2, $formatNumber->decimal, $formatNumber->separator) }}$</td>
                                </tr>
                            <?php $articleCount++; endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr class="table-light">
                                <td></td><td></td>
                                <td><strong>Sous-Total</strong></td>
                                <!--<td class="text-right">{{ sprintf("%0.2f", $commande->sous_total) }}$</td>-->
                                <td class="text-right">{{ number_format(truncate_number($commande->sous_total, 2), 2, $formatNumber->decimal, $formatNumber->separator) }}$</td>
                            </tr>
                            <tr class="table-light">
                                <td></td><td></td>
                                <td><strong>Frais</strong></td>
                                <!--<td class="text-right">{{ sprintf("%0.2f", $commande->total - $commande->sous_total) }}$</td>-->
                                <td class="text-right">{{ number_format(truncate_number($commande->total - $commande->sous_total, 1, 2), 2, $formatNumber->decimal, $formatNumber->separator) }}$</td>
                            </tr>
                            <tr class="table-success">
                                <td></td><td></td>
                                <td><strong>Total</strong></td>
                                <!--<td class="text-right">{{ sprintf("%0.2f", $commande->total) }}$</td>-->
                                <td class="text-right">{{ number_format(truncate_number($commande->total, 2), 2, $formatNumber->decimal, $formatNumber->separator) }}$</td>
                            </tr>
                        </tfoot>
                    </table>
                    
                        <div class="card" style="margin-top:20px">
                            <h6 class="card-header bg-dark text-white" >Signature du reçu</h6>
                            <div class="card-body">
                                <div class="text-center">
                                    <img class="img-fluid" src="{{ $commande->signature }}">
                                </div>
                            </div>
                        </div>
                    
                </div>
        
                <!-- Mobile -->
                <div class="col-12 d-block d-md-none">
                    <div class="card">
                        @foreach ($commande->commande_articles as $key => $articleCommande)
                            <h5 class="card-header bg-secondary text-white" >#{{$key + 1}} {{$articleCommande->nom}}</h5>
                                <div class="card-body">
        
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                Quantité
                                                <strong class="float-right">{{ $articleCommande->quantite_commande }}<small> x {{ $articleCommande->format_quantite }} {{ $articleCommande->format }}</small></strong>
                                            </li>
                                            <li class="list-group-item">
                                                Prix
                                                <small class="float-right">{{ number_format(truncate_number($articleCommande->prix * $articleCommande->quantite_commande, 2), 2, $formatNumber->decimal, $formatNumber->separator) }}$</small>
                                            </li>
                                            <li class="list-group-item">
                                                Frais
                                                <small class="float-right">{{ number_format(Controllers\ArticleController::ReadPriceFee($articleCommande, $articleCommande->quantite_commande),2, $formatNumber->decimal, $formatNumber->separator) }}$</small>
                                            </li>
                                            <li class="list-group-item">
                                                Total
                                                <strong class="float-right">{{ number_format(truncate_number($articleCommande->prix * $articleCommande->quantite_commande + Controllers\ArticleController::ReadPriceFee($articleCommande, $articleCommande->quantite_commande), 2), 2, $formatNumber->decimal, $formatNumber->separator) }}$</strong>
                                            </li>
                                        </ul>
                                </div>
                                <div class="card-footer" style="padding:0px;"></div>
                            
                        @endforeach
                    </div>
        
                    <div class="card" style="margin-top:20px">
                        <h4 class="card-header bg-info text-white" >Sommaire</h4>
                        <div class="card-body">
                                <ul class="list-group list-group-flush">
                                        <li class="list-group-item">
                                            Sous-Total
                                            <span class="float-right">{{ number_format(truncate_number($commande->sous_total, 2), 2, $formatNumber->decimal, $formatNumber->separator) }}$</span>
                                        </li>
                                        <li class="list-group-item">
                                            Frais
                                            <span class="float-right">{{ number_format(truncate_number($commande->total - $commande->sous_total, 2), 2, $formatNumber->decimal, $formatNumber->separator) }}$</span>
                                        </li>
                                        <li class="list-group-item">
                                            Total
                                            <strong class="float-right">{{ number_format(truncate_number($commande->total, 2), 2, $formatNumber->decimal, $formatNumber->separator) }}$</strong>
                                        </li>
                                    </ul>
                        </div>
                    </div>
                        <div class="card" style="margin-top:20px">
                            <h5 class="card-header bg-dark text-white" >Signature du reçu</h5>
                            <div class="card-body">
                                <div class="text-center">
                                    <img class="img-fluid" src="{{ $commande->signature }}">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('js/jquery.printarea.js') }}"></script>
<script>
 $(document).ready(function() {
        $("#format-phone").text(parsePhoneNumber($("#format-phone").text()));
    });

    function printCommande(){
        $('#toprintheader').removeClass('d-none');
        $('#toprint').printArea({ mode: 'popup', popClose: true });
        $('#toprintheader').addClass('d-none');
    }
</script>
@endsection