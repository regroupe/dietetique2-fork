<?php
use App\Http\Controllers;

// Accueil
Breadcrumbs::register('Accueil', function ($trail) {
    $trail->push('Accueil', route('index'));
});

// Page d'erreur
Breadcrumbs::register('Erreur', function ($trail, $no) {
    $trail->parent('Accueil');
    $trail->push('Erreur '.$no, route('error', $no));
});

/*
*Boutique
*********************************************/

Breadcrumbs::register('Boutique', function ($trail) {
    $trail->parent('Accueil');
    $trail->push('Boutique', route('boutique'));
});

Breadcrumbs::register('VueArticle', function ($trail, $article) {
    $trail->parent('Boutique','Toutes les catégories', null);
    $trail->push($article->nom, action('BoutiqueController@View_GET', $article->rowid));
});

/*
*À propos
*********************************************/

Breadcrumbs::register('apropos', function ($trail) {
    $trail->parent('Accueil');
    $trail->push('À propos', action('ParametragesController@Apropos'));
});
/*
*Conditions et politiques
*********************************************/

Breadcrumbs::register('termsofuse', function ($trail) {
    $trail->parent('Accueil');
    $trail->push('Conditions d\'utilisations et politiques', action('ParametragesController@TermsOfUse'));
});


/*
*FAQ
*********************************************/

Breadcrumbs::register('faq', function ($trail) {
    $trail->parent('Accueil');
    $trail->push('Foire aux questions', action('ParametragesController@FAQ'));
});

/*
*Calendrier
*********************************************/

Breadcrumbs::register('Calendrier', function ($trail) {
    $trail->parent('Accueil');
    $trail->push('Calendrier', action('PeriodeController@IndexCalendar'));
});

/*
*Panier
*********************************************/

Breadcrumbs::register('Panier', function ($trail) {
    $trail->parent('Accueil');
    $trail->push('Panier', action('PanierController@Index'));
});

/*
*Connexion
*********************************************/

Breadcrumbs::register('Connexion', function ($trail) {
    $trail->parent('Accueil');
    $trail->push('Connexion', action('ConnexionController@IndexConnexion'));
});

/*
*Compte client
*********************************************/

Breadcrumbs::register('InfoClient', function ($trail) {
    $trail->parent('Accueil');
    $trail->push('Informations de compte', action('CompteController@IndexCompte'));
});

Breadcrumbs::register('HistoriqueClient', function ($trail) {
    $trail->parent('Accueil');
    $trail->push('Historique', action('CompteController@IndexHistorique'));
});

Breadcrumbs::register('CommandeClient', function ($trail, $commande) {
    $trail->parent('HistoriqueClient');
    $trail->push('Commande #'.$commande, action('CommandeController@View_GET', $commande));
});

/*
*Compte admin
*********************************************/

/*
*Tableau de bord et compte
*/

Breadcrumbs::register('MonCompte', function ($trail) {
    $trail->parent('Accueil');
    $trail->push('Mon compte', action('AdminController@IndexCompte'));
});

Breadcrumbs::register('CompteAdmin', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Informations de compte', action('AdminController@IndexCompte'));
});

Breadcrumbs::register('TableauBord', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Tableau de bord', action('AdminController@IndexTableau'));
});

Breadcrumbs::register('EditBoard', function ($trail) {
    $trail->parent('TableauBord');
    $trail->push('Modifier', action('AdminController@EditBoard_GET'));
});

/*
*Articles
*/

Breadcrumbs::register('GestionArticle', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push(Controllers\PersonnalisationController::GetSellerName(true, true), action('ArticleController@Index'));
});

Breadcrumbs::register('CreerArticle', function ($trail) {
    $trail->parent('GestionArticle');
    $trail->push('Créer', action('ArticleController@Create_GET'));
});

Breadcrumbs::register('ModifierArticle', function ($trail, $article) {
    $trail->parent('GestionArticle');
    $trail->push($article->nom, action('ArticleController@Edit_GET', $article->rowid));
});

/*
*Marques
*/

Breadcrumbs::register('Marque', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Marques', action('MarqueController@Index'));
});

/*
*Fournisseurs
*/

Breadcrumbs::register('Fournisseur', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Fournisseurs', action('FournisseurController@ReadFournisseur'));
});

/*
*Categories
*/

Breadcrumbs::register('Categorie', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Catégories', action('CategorieController@Index'));
});

/*
*Unités de mesure
*/

Breadcrumbs::register('UniteMesure', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Unités de mesure', action('UniteMesureController@Index'));
});

/*
*Gestion des comptes
*/

Breadcrumbs::register('GestionClient', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Clients', action('GestionCompteController@ReadClient'));
});

Breadcrumbs::register('GestionAdmin', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Admins', action('GestionCompteController@ReadAdmin'));
});

Breadcrumbs::register('AjouterCompte', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Ajouter des comptes', action('GestionCompteController@ReadAjouter'));
});

Breadcrumbs::register('CommandesClients', function ($trail,$usr) {
    $trail->parent('GestionClient');
    $trail->push('Commandes du client #'.$usr->rowid, action('CompteController@IndexHistoriqueAdmin', $usr->rowid));
});

Breadcrumbs::register('DetailCommande', function ($trail, $client, $commande) {
    $trail->parent('CommandesClients', $client);
    $trail->push('Commandes #'.$commande->rowid, action('CommandeController@ViewAdmin_GET', [$client->rowid, $commande->rowid]));
});

/*
*Articles en vente
*/

Breadcrumbs::register('EnVente', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push(Controllers\PersonnalisationController::GetSellerName(true, true).' en vente', action('ProduitOffertController@IndexProduit'));
});

/*
*Périodes
*/

Breadcrumbs::register('PeriodeCommande', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Périodes de commande', action('PeriodeController@IndexPeriode'));
});

Breadcrumbs::register('AjouterPeriode', function ($trail) {
    $trail->parent('PeriodeCommande');
    $trail->push('Ajouter une période', action('PeriodeController@AjouterPeriode_GET'));
});

Breadcrumbs::register('ModifierPeriode', function ($trail, $periode) {
    $trail->parent('PeriodeCommande');
    $trail->push('Période #'.$periode->rowid, action('PeriodeController@EditPeriode_GET', $periode->rowid));
});

Breadcrumbs::register('ArchivePeriode', function ($trail) {
    $trail->parent('PeriodeCommande');
    $trail->push('Archives des périodes', action('PeriodeController@archive_GET'));
});

Breadcrumbs::register('ProgressionPeriode', function ($trail, $periode) {
    $trail->parent('ModifierPeriode',$periode);
    $trail->push('Progression des '.Controllers\PersonnalisationController::GetSellerName(true, false), action('PeriodeController@IndexProgression', $periode->rowid));
});

Breadcrumbs::register('PriseCommande', function ($trail, $periode) {
    $trail->parent('ModifierPeriode',$periode);
    $trail->push('Prise de commande ', action('PeriodeController@OrderPickup_GET', $periode->rowid));
});


/*
* Stats
*/
Breadcrumbs::register('Statistiques', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Statistiques', action('StatistiquesController@Index'));
});



/*
* Thèmes
*/

Breadcrumbs::register('Themes', function ($trail) {
    $trail->parent('MonCompte');
    $trail->push('Thèmes', action('PersonnalisationController@Index'));
});

Breadcrumbs::register('Themes_creer', function ($trail) {
    $trail->parent('Themes');
    $trail->push('Créer', action('PersonnalisationController@CreateTheme_GET'));
});

Breadcrumbs::register('Themes_modifier', function ($trail, $theme) {
    $trail->parent('Themes');
    $trail->push('Thème #'.$theme->rowid, action('PersonnalisationController@EditTheme_GET',$theme->rowid));
});

?>
