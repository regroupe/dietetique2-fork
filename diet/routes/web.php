<?php
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/erreur/{no}', function($no){
    return view('errors.default', compact('no'));
})->name('error');
 
Route::get('/', function () {
    return view('index');
})->name('index');
 
/**
 * Routes de base de connexion
 */
Route::get('/connexion', 'ConnexionController@IndexConnexion');
Route::get('/deconnexion', 'ConnexionController@Logout');
Route::post('/inscription', 'ConnexionController@Register');
Route::post('/connexion/login', 'ConnexionController@Login');
 
/**
 * Routes de base de la boutique en ligne
 */
Route::get('/boutique', 'BoutiqueController@Index')->name('boutique');
Route::get('/boutique/view/{rowid}', 'BoutiqueController@View_GET')->middleware('article_en_vente');
Route::get('/boutique/view/{rowid}/telechargement', 'ArticleController@DownloadPieceJointe')->middleware('article_en_vente');
Route::get('/calendrier', 'PeriodeController@IndexCalendar');
Route::post('/calendrier/view', 'PeriodeController@GetCalendar');

/**
 * Routes FAQ et à propos
 */
Route::get('/apropos','ParametragesController@Apropos');
Route::get('/faq','ParametragesController@FAQ');
Route::get('/politiques','ParametragesController@TermsOfUse');

/**
 * Routes du compte client
 */
Route::group(['middleware' => ['cltauth']], function () {

    // Routes du compte
    Route::get('/compte', 'CompteController@IndexCompte');
    Route::get('/compte/historique', 'CompteController@IndexHistorique');
    Route::get('/compte/historique/commande/{rowid}', 'CommandeController@View_GET');
    Route::post('/compte/changermotdepasse', 'CompteController@ResetPassword');
    Route::post('/compte/changeradresse', 'CompteController@SaveAddress');
 
    // Routes du panier client
    Route::get('/compte/panier', 'PanierController@Index');
    Route::post('/compte/panier/update', 'PanierController@Update_POST');
    Route::post('/compte/panier/remove', 'PanierController@Remove_POST');
 
    // Routes de la commande du client
    Route::group(['middleware' => ['orderperiodactive']], function () {
        Route::post('/compte/commande/commander', 'CommandeController@Order_POST');
    });

    /*PAYPAL*/
    Route::get('/{order?}', [
        'name' => 'PayPal Express Checkout',
        'as' => 'app.home',
        'uses' => 'PayPalController@form',
    ]);

    Route::post('/checkout/payment/paypal', [
        'name' => 'PayPal Express Checkout',
        'as' => 'checkout.payment.paypal',
        'uses' => 'PayPalController@checkout',
    ]);

    Route::get('/paypal/checkout/{order}/completed', [
        'name' => 'PayPal Express Checkout',
        'as' => 'paypal.checkout.completed',
        'uses' => 'PayPalController@completed',
    ]);

    Route::get('/paypal/checkout/{order}/cancelled', [
        'name' => 'PayPal Express Checkout',
        'as' => 'paypal.checkout.cancelled',
        'uses' => 'PayPalController@cancelled',
    ]);
});
 
/**
 * Routes administrative de base
 */
Route::group(['middleware' => ['admauth']], function () {
    Route::get('/administrateur/modifier-motdepasse', 'AdminController@MustReset');
    Route::post('/administrateur/compte/changermdp', 'AdminController@ResetPassword');
});
 
/**
 * Routes administrative des admins (Étudiants du département)
 */
Route::group(['middleware' => ['admauth', 'changepass']], function () {
 
    // Compte administratif
    Route::get('/administrateur/compte', 'AdminController@IndexCompte');

    // Gestion des articles
    Route::get('/administrateur/gestion/articles', 'ArticleController@Index');
    Route::get('/administrateur/gestion/articles/create', 'ArticleController@Create_GET');
    Route::get('/administrateur/gestion/articles/edit/{rowid}', 'ArticleController@Edit_GET');
    Route::post('/administrateur/gestion/articles/create', 'ArticleController@Create_POST');
    Route::post('/administrateur/gestion/articles/edit', 'ArticleController@Edit_POST');
    Route::post('/administrateur/gestion/articles/delete', 'ArticleController@Delete_POST');
    Route::post('/administrateur/gestion/articles/activate', 'ArticleController@Activate_POST');

    // Gestion des marques
    Route::get('/administrateur/gestion/marque','MarqueController@Index');
    Route::get('/administrateur/gestion/creerMarque','MarqueController@Creer');
    Route::post('/administrateur/gestion/marque/create','MarqueController@RegisterMarque');
    Route::post('/administrateur/gestion/marque/create_form','MarqueController@RegisterMarque_FORM');

    //Recherche marque
    Route::get('/administrateur/gestion/marque/recherche/actif={a}&par-page={nb}', 'MarqueController@RechercheMarqueActif');
    Route::get('/administrateur/gestion/marque/recherche/{rech}&actif={a}&par-page={nb}', 'MarqueController@RechercheMarque_GET');
    Route::post('/administrateur/gestion/marque/recherche', 'MarqueController@RechercheMarque_POST');

    // Gestion des fournisseurs
    Route::get('/administrateur/gestion/fournisseur','FournisseurController@ReadFournisseur');
    Route::post('/administrateur/gestion/fournisseur/create','FournisseurController@RegisterFournisseur');
    Route::post('/administrateur/gestion/fournisseur/create_form','FournisseurController@RegisterFournisseur_FORM');

    //Recherche fournisseur
    Route::get('/administrateur/gestion/fournisseur/recherche/actif={a}&par-page={nb}', 'FournisseurController@RechercheFournisseurActif');
    Route::get('/administrateur/gestion/fournisseur/recherche/{rech}&actif={a}&par-page={nb}', 'FournisseurController@RechercheFournisseur_GET');
    Route::post('/administrateur/gestion/fournisseur/recherche', 'FournisseurController@RechercheFournisseur_POST');

    // Gestion des unités de mesure
    Route::get('/administrateur/gestion/unite-mesure','UniteMesureController@Index');
    Route::post('/administrateur/gestion/unite-mesure/create','UniteMesureController@RegisterUnit');
    Route::post('/administrateur/gestion/unite-mesure/create_form','UniteMesureController@RegisterUnite_FORM');

    //Recherche unité de mesure
    Route::get('/administrateur/gestion/unite-mesure/recherche/actif={a}&par-page={nb}', 'UniteMesureController@RechercheUniteActif');
    Route::get('/administrateur/gestion/unite-mesure/recherche/{rech}&actif={a}&par-page={nb}', 'UniteMesureController@RechercheUnite_GET');
    Route::post('/administrateur/gestion/unite-mesure/recherche', 'UniteMesureController@RechercheUnite_POST');
 
    // Routes pour bénévoles
    Route::group(['middleware' => ['admvolunteer']], function () {

        // Routes pour prise de commandes
        Route::get('/administrateur/gestion/periodes', 'PeriodeController@IndexPeriode');
        Route::get('/administrateur/gestion/periodes/no-{id}', 'PeriodeController@EditPeriode_GET');
        Route::get('/administrateur/gestion/periodes/no-{id}/orders', 'PeriodeController@OrderPickup_GET');
        Route::post('/administrateur/gestion/periodes/no-{id}/pickup', 'PeriodeController@OrderPickup_POST');

        //Routes commandes des clients
        Route::get('/administrateur/gestion/compte/client/{id}/commandes', 'CompteController@IndexHistoriqueAdmin');
        Route::get('/administrateur/gestion/compte/client/{id}/commandes/{no}', 'CommandeController@ViewAdmin_GET');
    });

    /**
     * Routes administrative des super admins (Enseignants du département) (Super admin > Admin)
     */
    Route::group(['middleware' => ['admsuper']], function () {

        //Tableau de bord
        Route::get('/administrateur/tableau-de-bord', 'AdminController@IndexTableau');

        /*
        * Edit board
        */
        Route::get('/administrateur/tableau-de-bord/modifier', 'AdminController@EditBoard_GET');
        Route::post('/administrateur/tableau-de-bord/modifier/addrow', 'AdminController@AddRow1_POST');
        Route::post('/administrateur/tableau-de-bord/modifier/save', 'AdminController@SaveBoard_POST');

        // Gestion des comptes
        Route::get('/administrateur/gestion/compte/admin', 'GestionCompteController@ReadAdmin');
        Route::get('/administrateur/gestion/compte/client', 'GestionCompteController@ReadClient');
        Route::get('/administrateur/gestion/compte/ajouter', 'GestionCompteController@ReadAjouter');
        Route::post('/administrateur/gestion/compte/ajouter/generer', 'GestionCompteController@GenererPOSTSuper');
        Route::post('/administrateur/gestion/compte/activer', 'GestionCompteController@ActiverCompte');
        Route::post('/administrateur/gestion/compte/desactiver', 'GestionCompteController@DesactiverCompte');
        Route::post('/administrateur/gestion/compte/regenerer', 'GestionCompteController@Regenerer');
 
        // Routes de recherche des clients
        Route::get('/administrateur/gestion/compte/client/recherche/actif={a}&par-page={nb}', 'GestionCompteController@RechercheClientActif');
        Route::get('/administrateur/gestion/compte/client/recherche/{rech}&actif={a}&par-page={nb}', 'GestionCompteController@RechercheClient');
        Route::post('/administrateur/gestion/compte/client/recherche', 'GestionCompteController@RechercheClientPOST');
       
        // Routes de recherche des administrateurs
        Route::get('/administrateur/gestion/compte/admin/recherche/actif={a}&par-page={nb}', 'GestionCompteController@RechercheAdminActif');
        Route::get('/administrateur/gestion/compte/admin/recherche/{rech}&actif={a}&par-page={nb}', 'GestionCompteController@RechercheAdmin');
        Route::post('/administrateur/gestion/compte/admin/recherche', 'GestionCompteController@RechercheAdminPOST');
        
        // Gestion des marques
        //Route::get('/administrateur/gestion/marque/editMarque/{id}','MarqueController@Modification');
        Route::post('/administrateur/gestion/marque/desactiver','MarqueController@desactiverMarque');
        Route::post('/administrateur/gestion/marque/activer','MarqueController@activerMarque');
        Route::post('/administrateur/gestion/editMarque','MarqueController@Edit');
        Route::post('/administrateur/gestion/marque/deleteMarque','MarqueController@deleteMarque');
 
        // Gestion des fournisseurs
        //Route::get('/administrateur/gestion/fournisseur/editFournisseur/{id}','FournisseurController@ModificationFournisseur');
        Route::post('/administrateur/gestion/fournisseur/desactiver','FournisseurController@desactiverFournisseur');
        Route::post('/administrateur/gestion/fournisseur/activer','FournisseurController@activerFournisseur');
        Route::post('/administrateur/gestion/fournisseur/edit','FournisseurController@EditFourn');
        Route::post('/administrateur/gestion/fournisseur/deleteFournisseur','FournisseurController@deleteFournisseur');

        // Gestion des unités de mesure
        Route::post('/administrateur/gestion/unite-mesure/desactiver','UniteMesureController@DesactiverUnite');
        Route::post('/administrateur/gestion/unite-mesure/activer','UniteMesureController@ActiverUnite');
        Route::post('/administrateur/gestion/unite-mesure/edit','UniteMesureController@EditUnite');
 
        // Get Catégories
        Route::get('/administrateur/gestion/categorie','CategorieController@Index');
        Route::get('/administrateur/gestion/creerCategorie','CategorieController@CreerC');
        Route::get('/administrateur/gestion/categorie/listeCategorie','CategorieController@ViewListCategorie');
        Route::post('/administrateur/gestion/categorie/create','CategorieController@RegisterCategorie');
        Route::post('/administrateur/gestion/categorie/create_form','CategorieController@RegisterCategorie_FORM');

        // Recherche catégorie
        Route::get('/administrateur/gestion/categorie/recherche/actif={a}&par-page={nb}', 'CategorieController@RechercheCategorieActif');
        Route::get('/administrateur/gestion/categorie/recherche/{rech}&actif={a}&par-page={nb}', 'CategorieController@RechercheCategorie_GET');
        Route::post('/administrateur/gestion/categorie/recherche', 'CategorieController@RechercheCategorie_POST');

        //Route::get('/administrateur/gestion/categorie/editCategorie/{id}','CategorieController@ModificationCategorie');
        Route::post('/administrateur/gestion/categorie/activer','CategorieController@activerCategorie');
        Route::post('/administrateur/gestion/categorie/desactiver','CategorieController@desactiverCategorie');
        Route::post('/administrateur/gestion/editCategorie','CategorieController@EditCategorie');
        Route::post('/administrateur/gestion/categorie/deleteCategorie','CategorieController@deleteCategorie');
 
        // Système d'approbation des articles de la boutique
        Route::get('/administrateur/gestion/articles-en-vente', 'ProduitOffertController@IndexProduit');
        Route::get('/administrateur/gestion/articles-en-vente/recherche/disponible={a}&par-page={nb}', 'ProduitOffertController@RechercheActifVente');
        Route::get('/administrateur/gestion/articles-en-vente/recherche/{type}&{rech}&disponible={a}&par-page={nb}', 'ProduitOffertController@Recherche');
        Route::post('/administrateur/gestion/articles-en-vente/recherche', 'ProduitOffertController@RecherchePOST');
        Route::post('/administrateur/gestion/articles-en-vente/offrir', 'ProduitOffertController@Offrir');
        Route::post('/administrateur/gestion/articles-en-vente/enlever', 'ProduitOffertController@Enlever');

        // Gestion des périodes de commande
        Route::get('/administrateur/gestion/periodes/ajouter', 'PeriodeController@AjouterPeriode_GET');
        Route::post('/administrateur/gestion/periodes/ajouter/post', 'PeriodeController@AjouterPeriode_POST');
        Route::post('/administrateur/gestion/periodes/no-{id}/edit', 'PeriodeController@EditPeriode_POST');
        Route::post('/administrateur/gestion/periodes/no-{id}/edit-picks', 'PeriodeController@EditPicksOnly_POST');
        Route::post('/administrateur/gestion/periodes/no-{id}/delete', 'PeriodeController@DeletePeriod');

        //Progression des articles par période
        Route::get('/administrateur/gestion/periodes/no-{id}/progression', 'PeriodeController@IndexProgression');

        //Archive des périodes
        Route::get('/administrateur/gestion/periodes/archive', 'PeriodeController@archive_GET');
        Route::post('/administrateur/gestion/periodes/archive/recherche', 'PeriodeController@RechercheArchive_POST');
        Route::get('/administrateur/gestion/periodes/archive/recherche/{year}', 'PeriodeController@RechercheArchive_GET');

         //Stats
        Route::get('/administrateur/statistiques', 'StatistiquesController@Index');

        /**
         * Routes administrative des webmestres (Gestionnaires du site) (Webmasters > Super admin > Admin)
         */
        Route::group(['middleware' => ['admmaster']], function () {
            //Créer/Générer des comptes de hauts droits
            Route::post('/administrateur/gestion/compte/ajouter/creer', 'GestionCompteController@CreerCompte');
            Route::post('/administrateur/gestion/compte/ajouter/master-generer', 'GestionCompteController@GenererPOSTMaster');

            //Thèmes
            Route::get('/administrateur/gestion/themes', 'PersonnalisationController@Index');

            //Paramétrages des informations du site (La section À propos, FAQ,etc)
            Route::get('/administrateur/gestion/parametres','ParametragesController@Index');
            //Créer ou modifier un thème
            Route::get('/administrateur/gestion/themes/creer', 'PersonnalisationController@CreateTheme_GET');
            Route::get('/administrateur/gestion/themes/{id}', 'PersonnalisationController@EditTheme_GET');
            Route::post('/administrateur/gestion/themes/sauvegarder', 'PersonnalisationController@SaveTheme_POST');

            //Activate theme
            Route::get('/administrateur/gestion/themes/{id}/activer', 'PersonnalisationController@ActivateTheme_GET');

            //Delete theme
            Route::get('/administrateur/gestion/themes/{id}/delete', 'PersonnalisationController@DeleteTheme_GET');

            //À propos
            Route::get('/administrateur/gestion/themes/{id}/a-propos', 'ParametragesController@EditAPropos_GET');
            Route::post('/administrateur/gestion/themes/a-propos/modifier', 'ParametragesController@SaveAbout_POST');
            Route::post('/administrateur/gestion/themes/heberger-image', 'ParametragesController@Heberger_POST');

            //FAQ
            Route::get('/administrateur/gestion/theme/{id}/faq','ParametragesController@EditFAQ_GET');
            Route::post('/administrateur/gestion/theme/faq/create','ParametragesController@EditFAQ_POST');
            
            //Terms of use
            Route::get('/administrateur/gestion/themes/{id}/conditions-politiques','ParametragesController@EditTermsofuse_GET');
            Route::post('/administrateur/gestion/themes/conditions-politiques/modifier','ParametragesController@SaveTerms_POST');
            
        });
    });
});
?>