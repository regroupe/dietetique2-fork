<?php

namespace App;

use Omnipay\Omnipay;

class PayPal
{

    public function gateway()
    {
        $gateway = Omnipay::create('PayPal_Rest');
	
		$gateway->initialize(array(
			'clientId' => config('omnipay.credentials.clientId'),
			'secret' => config('omnipay.credentials.secret'),
			'testMode' => config('omnipay.credentials.testMode')
		));

        return $gateway;
    }

    public function purchase(array $parameters)
    {
        $response = $this->gateway()
            ->purchase($parameters)
            ->send();

        return $response;
    }

    public function complete(array $parameters)
    {
        $response = $this->gateway()
            ->completePurchase($parameters)
            ->send();

        return $response;
    }

    public function formatAmount($amount)
    {
        return number_format($amount, 2, '.', '');
    }

    public function getCancelUrl($order)
    {
        return route('paypal.checkout.cancelled', $order);
    }

    public function getReturnUrl($order)
    {
        return route('paypal.checkout.completed', $order);
    }
}