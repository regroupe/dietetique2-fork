<?php 
use App\Http\Controllers;

/**
 * See "ReadPartials" and "GenerateView" to learn how to add your view.
 */


/**
 * Fills the select lists when editing a board.
 * 
 * The partial name is seen by the user.
 * The partial value is the value stored in the database and used to generate the view.
 */
function ReadPartials() {
    $partials_name = [
        'Aucune',
        'Vue d\'essai',
        'Période active',
        'Période écoulée',
        'Progression des ventes',
        'Dernier '.Controllers\PersonnalisationController::GetSellerName(true, false).' ajouté(s)', //Dernier articles ajoutés
        Controllers\PersonnalisationController::GetSellerName(true, true).' populaires du mois', //Top 10 articles
        'Tendance des ventes sur un an',
        'Statistiques sur les clients'
        ];

    $partials_value = [
        'scaffold_view',
        'test_view',
        'active_period',
        'ecoule_period',
        'sales_progress',
        'last_added',
        'top_article',
        'tendance_article',
        'stats_client'
        ];

return json_encode([$partials_name, $partials_value]);
}


/**
 * Create a view from the partial value given.
 * 
 * Add a case with your value and return the partial view you created in ../administrateur/partialboard/.
 * 
 * When creating a view, you can add different sizing using the "GetSize" functions.
 * You can also defines the color with the "GetBorderColor" and "GetBackgroundColor" functions.
 * 
 * The partial view "_example_view" is a great example.
 */
function GenerateView($partial) {
    switch ($partial->vue)
    {
        case 'scaffold_view':return View::make('administrateur.partialboard._scaffold_view', compact('partial'))->render();
        case 'test_view':return View::make('administrateur.partialboard._example_view', compact('partial'))->render();
        case 'active_period':return View::make('administrateur.partialboard.periode._periodeactive', compact('partial'))->render();
        case 'ecoule_period':return View::make('administrateur.partialboard.periode._periodeecoule', compact('partial'))->render();
        case 'sales_progress':return View::make('administrateur.partialboard.periode._progression_periodeactive', compact('partial'))->render();
        case 'last_added':return View::make('administrateur.partialboard._dernierarticleajoute', compact('partial'))->render();
        case 'top_article':return View::make('administrateur.partialboard.stats._toparticle', compact('partial'))->render();
        case 'tendance_article':return View::make('administrateur.partialboard.stats._tendancearticle', compact('partial'))->render();
        case 'stats_client':return View::make('administrateur.partialboard.stats._stats_client', compact('partial'))->render();

        default: return View::make('administrateur.partialboard._example_view', compact('partial'))->render();
    }
}

/**
 * Defines the bootstrap column setup to be used foreach sizes.
 */
function GetSize($size) {
    switch ($size)
    {
        case 'small':return 'col-sm-12 col-md-6 col-lg-4';
        case 'medium':return 'col-sm-12 col-md-12 col-lg-6';
        case 'large':return 'col-sm-12 col-md-12 col-lg-12';
        default: return 'col-sm-12 col-md-12 col-lg-4';
    }
}

/**
 * Get the boostrap background color.
 */
function GetBackGroundColor($color) {
    switch ($color)
    {
        case 'primary':return 'bg-primary';
        case 'secondary':return 'bg-secondary';
        case 'success':return 'bg-success';
        case 'danger':return 'bg-danger';
        case 'warning':return 'bg-warning';
        case 'info':return 'bg-info';
        case 'dark':return 'bg-dark';
        case 'muted':return 'bg-muted';
        default: return 'bg-primary';
    }
}

/**
 * Get the boostrap border color.
 */
function GetBorderColor($color) {
    switch ($color)
    {
        case 'primary':return 'border-primary';
        case 'secondary':return 'border-secondary';
        case 'success':return 'border-success';
        case 'danger':return 'border-danger';
        case 'warning':return 'border-warning';
        case 'info':return 'border-info';
        case 'dark':return 'border-dark';
        case 'muted':return 'border-muted';
        default: return 'border-primary';
    }
}

?>