<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PeriodePlage
 * 
 * @property int $rowid
 * @property int $rowid_periode
 * @property \Carbon\Carbon $date_debut
 * @property \Carbon\Carbon $date_fin
 * @property string $emplacement
 * 
 * @property \App\Models\PeriodeCommande $periode_commande
 *
 * @package App\Models
 */
class PeriodePlage extends Eloquent
{
	protected $table = 'periode_plage';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $casts = [
		'rowid_periode' => 'int'
	];

	protected $dates = [
		'date_debut',
		'date_fin'
	];

	protected $fillable = [
		'rowid_periode',
		'date_debut',
		'date_fin',
		'emplacement'
	];

	public function periode_commande()
	{
		return $this->belongsTo(\App\Models\PeriodeCommande::class, 'rowid_periode');
	}
}
