<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PeriodeArticle
 * 
 * @property int $rowid
 * @property int $article_rowid
 * @property int $periode_rowid
 * @property int $quantite_commande
 * 
 * @property \App\Models\Article $article
 * @property \App\Models\PeriodeCommande $periode_commande
 *
 * @package App\Models
 */
class PeriodeArticle extends Eloquent
{
	protected $table = 'periode_article';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $casts = [
		'article_rowid' => 'int',
		'periode_rowid' => 'int',
		'quantite_commande' => 'int'
	];

	protected $fillable = [
		'article_rowid',
		'periode_rowid',
		'quantite_commande'
	];

	public function article()
	{
		return $this->belongsTo(\App\Models\Article::class, 'article_rowid');
	}

	public function periode_commande()
	{
		return $this->belongsTo(\App\Models\PeriodeCommande::class, 'periode_rowid');
	}
}
