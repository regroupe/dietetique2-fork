<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdminBoard
 * 
 * @property int $rowid
 * @property string $username_admin
 * @property string $vue
 * @property string $vue_size
 * @property string $vue_color
 * 
 * @property \App\Models\Admin $admin
 *
 * @package App\Models
 */
class AdminBoard extends Eloquent
{
	protected $table = 'admin_board';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $fillable = [
		'username_admin',
		'vue',
		'vue_size',
		'vue_color'
	];

	public function admin()
	{
		return $this->belongsTo(\App\Models\Admin::class, 'username_admin');
	}
}
