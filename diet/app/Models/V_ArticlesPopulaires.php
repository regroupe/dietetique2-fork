<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V_ArticlesPopulaires extends Model
{
	protected $table = 'v_stat_articles_populaires';

	protected $casts = [
		'data' => 'int',
		'dataset_month' => 'int',
		'dataset_year' => 'int'
	];

	protected $dates = [
		'dataset_date'
	];

	protected $fillable = [
		'label',
		'data',
		'dataset_date',
		'dataset_month',
		'dataset_month_full',
		'dataset_year'
	];
}
