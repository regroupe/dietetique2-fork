<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V_Utilisateurs extends Model
{
	protected $table = 'v_stat_utilisateurs';

	protected $casts = [
		'data' => 'int'
	];

	protected $fillable = [
		'label',
		'data'
	];
}
