<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V_NbClientActif extends Model
{
	protected $table = 'v_stat_nb_client_actif';

	protected $casts = [
        'nb' => 'int',
        'dataset_year' => 'int',
        'dataset_month' => 'int'
	];

	protected $fillable = [
        'nb',
        'dataset_year',
        'dataset_month',
	];
}
