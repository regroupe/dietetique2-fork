<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Personnaliser
 * 
 * @property int $rowid
 * @property string $logo
 * @property string $banniere
 * @property string $footer_desc
 * @property string $footer_entreprise
 * @property string $footer_address
 * @property string $footer_city
 * @property string $footer_code_postal
 * @property string $footer_telephone
 * @property string $footer_copyright
 * @property string $car1
 * @property string $car2
 * @property string $car3
 * @property string $car1_msg
 * @property string $car2_msg
 * @property string $car3_msg
 * @property string $car1_link
 * @property string $car2_link
 * @property string $car3_link
 * @property string $nom_theme
 * @property string $nom_article
 * @property string $nom_article_plur
 * @property string $titre_onglet
 * @property string $icon
 * @property string $about_html
 * @property string $termsofuse
 * @property string $affichage_decimal
 * @property string $affichage_separator
 * @property bool $actif
 * 
 * @property \Illuminate\Database\Eloquent\Collection $faqs
 *
 * @package App\Models
 */
class Personnaliser extends Eloquent
{
	protected $table = 'personnaliser';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $casts = [
		'actif' => 'bool'
	];

	protected $fillable = [
		'logo',
		'banniere',
		'footer_desc',
		'footer_entreprise',
		'footer_address',
		'footer_city',
		'footer_code_postal',
		'footer_telephone',
		'footer_copyright',
		'car1',
		'car2',
		'car3',
		'car1_msg',
		'car2_msg',
		'car3_msg',
		'car1_link',
		'car2_link',
		'car3_link',
		'nom_theme',
		'nom_article',
		'nom_article_plur',
		'titre_onglet',
		'icon',
		'about_html',
		'termsofuse',
		'affichage_decimal',
		'affichage_separator',
		'actif'
	];

	public function faqs()
	{
		return $this->hasMany(\App\Models\Faq::class, 'rowid_theme');
	}
}
