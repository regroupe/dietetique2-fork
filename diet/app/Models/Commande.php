<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Commande
 * 
 * @property int $rowid
 * @property int $rowid_client
 * @property int $rowid_periode
 * @property string $nom_client
 * @property float $sous_total
 * @property float $total
 * @property \Carbon\Carbon $date_commande
 * @property string $email
 * @property string $adresse
 * @property string $no_app
 * @property string $ville
 * @property string $province
 * @property string $pays
 * @property string $code_postal
 * @property string $telephone
 * @property string $poste
 * @property bool $notify
 * @property boolean $facture
 * @property string $identifiant
 * @property bool $paye
 * @property bool $prise
 * @property boolean $signature
 * 
 * @property \App\Models\Client $client
 * @property \App\Models\PeriodeCommande $periode_commande
 * @property \Illuminate\Database\Eloquent\Collection $commande_articles
 *
 * @package App\Models
 */
class Commande extends Eloquent
{
	protected $table = 'commande';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $casts = [
		'rowid_client' => 'int',
		'rowid_periode' => 'int',
		'sous_total' => 'float',
		'total' => 'float',
		'notify' => 'bool',
		'facture' => 'boolean',
		'paye' => 'bool',
		'prise' => 'bool'
	];

	protected $dates = [
		'date_commande'
	];

	protected $fillable = [
		'rowid_client',
		'rowid_periode',
		'nom_client',
		'sous_total',
		'total',
		'date_commande',
		'email',
		'adresse',
		'no_app',
		'ville',
		'province',
		'pays',
		'code_postal',
		'telephone',
		'poste',
		'notify',
		'facture',
		'identifiant',
		'paye',
		'prise',
		'signature'
	];

	public function client()
	{
		return $this->belongsTo(\App\Models\Client::class, 'rowid_client');
	}

	public function periode_commande()
	{
		return $this->belongsTo(\App\Models\PeriodeCommande::class, 'rowid_periode');
	}

	public function commande_articles()
	{
		return $this->hasMany(\App\Models\CommandeArticle::class, 'commande_rowid');
	}
}
