<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Categorie
 * 
 * @property string $nom_categorie
 * @property bool $actif
 * 
 * @property \Illuminate\Database\Eloquent\Collection $articles
 *
 * @package App\Models
 */
class Categorie extends Eloquent
{
	protected $table = 'categorie';
	protected $primaryKey = 'nom_categorie';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'actif' => 'bool'
	];

	protected $fillable = [
		'actif'
	];

	public function articles()
	{
		return $this->hasMany(\App\Models\Article::class, 'nom_categorie');
	}
}
