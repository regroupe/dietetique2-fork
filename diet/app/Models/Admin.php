<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Admin
 * 
 * @property bool $super
 * @property bool $master
 * @property bool $take_order
 * @property string $username
 * @property string $password
 * @property string $last_modif
 * @property bool $change_pass
 * @property bool $actif
 * 
 * @property \Illuminate\Database\Eloquent\Collection $admin_boards
 *
 * @package App\Models
 */
class Admin extends Eloquent
{
	protected $table = 'admin';
	protected $primaryKey = 'username';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'super' => 'bool',
		'master' => 'bool',
		'take_order' => 'bool',
		'change_pass' => 'bool',
		'actif' => 'bool'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'super',
		'master',
		'take_order',
		'password',
		'last_modif',
		'change_pass',
		'actif'
	];

	public function admin_boards()
	{
		return $this->hasMany(\App\Models\AdminBoard::class, 'username_admin');
	}
}
