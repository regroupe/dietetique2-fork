<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PanierClient
 * 
 * @property int $rowid
 * @property int $rowid_client
 * @property bool $actuel
 * 
 * @property \App\Models\Client $client
 * @property \Illuminate\Database\Eloquent\Collection $panier_articles
 *
 * @package App\Models
 */
class PanierClient extends Eloquent
{
	protected $table = 'panier_client';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $casts = [
		'rowid_client' => 'int',
		'actuel' => 'bool'
	];

	protected $fillable = [
		'rowid_client',
		'actuel'
	];

	public function client()
	{
		return $this->belongsTo(\App\Models\Client::class, 'rowid_client');
	}

	public function panier_articles()
	{
		return $this->hasMany(\App\Models\PanierArticle::class, 'rowid_panier');
	}
}
