<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Faq
 * 
 * @property int $rowid
 * @property int $rowid_theme
 * @property string $question
 * @property string $reponse
 * 
 * @property \App\Models\Personnaliser $personnaliser
 *
 * @package App\Models
 */
class Faq extends Eloquent
{
	protected $table = 'faq';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $casts = [
		'rowid_theme' => 'int'
	];

	protected $fillable = [
		'rowid_theme',
		'question',
		'reponse'
	];

	public function personnaliser()
	{
		return $this->belongsTo(\App\Models\Personnaliser::class, 'rowid_theme');
	}
}
