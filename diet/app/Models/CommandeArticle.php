<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CommandeArticle
 * 
 * @property int $rowid
 * @property int $commande_rowid
 * @property int $article_rowid
 * @property string $image
 * @property string $nom
 * @property float $format_quantite
 * @property string $format
 * @property string $provenance
 * @property string $allergene
 * @property bool $perissable
 * @property bool $bio
 * @property int $quebec
 * @property string $nom_marque
 * @property string $nom_categorie
 * @property string $nom_fournisseur
 * @property int $quantite_commande
 * @property float $prix
 * @property float $frais_fixe
 * @property float $frais_variable
 * @property float $prix_majore
 * @property float $economie
 * 
 * @property \App\Models\Commande $commande
 *
 * @package App\Models
 */
class CommandeArticle extends Eloquent
{
	protected $table = 'commande_article';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $casts = [
		'commande_rowid' => 'int',
		'article_rowid' => 'int',
		'format_quantite' => 'float',
		'perissable' => 'bool',
		'bio' => 'bool',
		'quebec' => 'int',
		'quantite_commande' => 'int',
		'prix' => 'float',
		'frais_fixe' => 'float',
		'frais_variable' => 'float',
		'prix_majore' => 'float',
		'economie' => 'float'
	];

	protected $fillable = [
		'commande_rowid',
		'article_rowid',
		'image',
		'nom',
		'format_quantite',
		'format',
		'provenance',
		'allergene',
		'perissable',
		'bio',
		'quebec',
		'nom_marque',
		'nom_categorie',
		'nom_fournisseur',
		'quantite_commande',
		'prix',
		'frais_fixe',
		'frais_variable',
		'prix_majore',
		'economie'
	];

	public function commande()
	{
		return $this->belongsTo(\App\Models\Commande::class, 'commande_rowid');
	}
}
