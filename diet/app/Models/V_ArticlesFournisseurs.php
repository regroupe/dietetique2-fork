<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V_ArticlesFournisseurs extends Model
{
	protected $table = 'v_stat_articles_fournisseurs';

	protected $casts = [
		'data' => 'int'
	];

	protected $fillable = [
		'label',
		'data'
	];
}
