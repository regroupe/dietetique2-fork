<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PeriodeCommande
 * 
 * @property int $rowid
 * @property \Carbon\Carbon $date_debut
 * @property \Carbon\Carbon $date_fin
 * 
 * @property \Illuminate\Database\Eloquent\Collection $commandes
 * @property \Illuminate\Database\Eloquent\Collection $periode_articles
 * @property \Illuminate\Database\Eloquent\Collection $periode_plages
 *
 * @package App\Models
 */
class PeriodeCommande extends Eloquent
{
	protected $table = 'periode_commande';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $dates = [
		'date_debut',
		'date_fin'
	];

	protected $fillable = [
		'date_debut',
		'date_fin'
	];

	public function commandes()
	{
		return $this->hasMany(\App\Models\Commande::class, 'rowid_periode');
	}

	public function periode_articles()
	{
		return $this->hasMany(\App\Models\PeriodeArticle::class, 'periode_rowid');
	}

	public function periode_plages()
	{
		return $this->hasMany(\App\Models\PeriodePlage::class, 'rowid_periode');
	}
}
