<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Marque
 * 
 * @property string $nom_marque
 * @property bool $actif
 * 
 * @property \Illuminate\Database\Eloquent\Collection $articles
 *
 * @package App\Models
 */
class Marque extends Eloquent
{
	protected $table = 'marque';
	protected $primaryKey = 'nom_marque';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'actif' => 'bool'
	];

	protected $fillable = [
		'actif'
	];

	public function articles()
	{
		return $this->hasMany(\App\Models\Article::class, 'nom_marque');
	}
}
