<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Recette
 * 
 * @property int $rowid
 * @property int $article_rowid
 * @property string $name
 * @property string $link
 *
 * @package App\Models
 */
class Recette extends Eloquent
{
	protected $table = 'recette';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $casts = [
		'article_rowid' => 'int'
	];

	protected $fillable = [
		'article_rowid',
		'name',
		'link'
	];
}
