<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V_TendanceCommandes extends Model
{
	protected $table = 'v_stat_tendance_commandes';

	protected $casts = [
		'data' => 'int',
		'dataset_month' => 'int',
		'dataset_year' => 'int'
	];

	protected $fillable = [
		'data',
		'dataset_month',
		'dataset_month_full',
		'dataset_year'
	];
}
