<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Client
 * 
 * @property int $rowid
 * @property string $email
 * @property string $password
 * @property string $telephone
 * @property string $poste
 * @property string $nom
 * @property string $prenom
 * @property string $adresse
 * @property string $no_app
 * @property string $code_postal
 * @property string $ville
 * @property string $pays
 * @property string $province
 * @property bool $actif
 * 
 * @property \Illuminate\Database\Eloquent\Collection $commandes
 * @property \Illuminate\Database\Eloquent\Collection $panier_clients
 *
 * @package App\Models
 */
class Client extends Eloquent
{
	protected $table = 'client';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $casts = [
		'actif' => 'bool'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'email',
		'password',
		'telephone',
		'poste',
		'nom',
		'prenom',
		'adresse',
		'no_app',
		'code_postal',
		'ville',
		'pays',
		'province',
		'actif'
	];

	public function commandes()
	{
		return $this->hasMany(\App\Models\Commande::class, 'rowid_client');
	}

	public function panier_clients()
	{
		return $this->hasMany(\App\Models\PanierClient::class, 'rowid_client');
	}
}
