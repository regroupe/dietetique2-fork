<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V_Administrateurs extends Model
{
	protected $table = 'v_stat_administrateurs';

	protected $casts = [
		'data' => 'int'
	];

	protected $fillable = [
		'label',
		'data'
	];
}
