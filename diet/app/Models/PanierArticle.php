<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PanierArticle
 * 
 * @property int $rowid
 * @property int $rowid_panier
 * @property int $rowid_article
 * @property int $quantite
 * 
 * @property \App\Models\Article $article
 * @property \App\Models\PanierClient $panier_client
 *
 * @package App\Models
 */
class PanierArticle extends Eloquent
{
	protected $table = 'panier_article';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $casts = [
		'rowid_panier' => 'int',
		'rowid_article' => 'int',
		'quantite' => 'int'
	];

	protected $fillable = [
		'rowid_panier',
		'rowid_article',
		'quantite'
	];

	public function article()
	{
		return $this->belongsTo(\App\Models\Article::class, 'rowid_article');
	}

	public function panier_client()
	{
		return $this->belongsTo(\App\Models\PanierClient::class, 'rowid_panier');
	}
}
