<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Fournisseur
 * 
 * @property string $nom_fournisseur
 * @property bool $actif
 * 
 * @property \Illuminate\Database\Eloquent\Collection $articles
 *
 * @package App\Models
 */
class Fournisseur extends Eloquent
{
	protected $table = 'fournisseur';
	protected $primaryKey = 'nom_fournisseur';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'actif' => 'bool'
	];

	protected $fillable = [
		'actif'
	];

	public function articles()
	{
		return $this->hasMany(\App\Models\Article::class, 'nom_fournisseur');
	}
}
