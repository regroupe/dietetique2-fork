<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Mar 2019 14:51:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Article
 * 
 * @property int $rowid
 * @property string $image
 * @property string $nom
 * @property float $format_quantite
 * @property string $format
 * @property string $provenance
 * @property string $allergene
 * @property bool $perissable
 * @property bool $bio
 * @property int $quebec
 * @property string $nom_marque
 * @property string $nom_categorie
 * @property string $nom_fournisseur
 * @property int $quantite_minimum
 * @property int $quantite_maximum
 * @property float $prix
 * @property float $frais_fixe
 * @property float $frais_variable
 * @property float $frais_emballage
 * @property float $prix_majore
 * @property float $economie
 * @property string $description
 * @property string $remarque
 * @property string $valeur_nutritive
 * @property string $piece_jointe
 * @property string $created_by
 * @property bool $actif
 * @property bool $actif_vente
 * 
 * @property \App\Models\Marque $marque
 * @property \App\Models\Categorie $categorie
 * @property \App\Models\Fournisseur $fournisseur
 * @property \App\Models\UniteMesure $unite_mesure
 * @property \Illuminate\Database\Eloquent\Collection $panier_articles
 * @property \Illuminate\Database\Eloquent\Collection $periode_articles
 *
 * @package App\Models
 */
class Article extends Eloquent
{
	protected $table = 'article';
	protected $primaryKey = 'rowid';
	public $timestamps = false;

	protected $casts = [
		'format_quantite' => 'float',
		'perissable' => 'bool',
		'bio' => 'bool',
		'quebec' => 'int',
		'quantite_minimum' => 'int',
		'quantite_maximum' => 'int',
		'prix' => 'float',
		'frais_fixe' => 'float',
		'frais_variable' => 'float',
		'frais_emballage' => 'float',
		'prix_majore' => 'float',
		'economie' => 'float',
		'actif' => 'bool',
		'actif_vente' => 'bool'
	];

	protected $fillable = [
		'image',
		'nom',
		'format_quantite',
		'format',
		'provenance',
		'allergene',
		'perissable',
		'bio',
		'quebec',
		'nom_marque',
		'nom_categorie',
		'nom_fournisseur',
		'quantite_minimum',
		'quantite_maximum',
		'prix',
		'frais_fixe',
		'frais_variable',
		'frais_emballage',
		'prix_majore',
		'economie',
		'description',
		'remarque',
		'valeur_nutritive',
		'piece_jointe',
		'created_by',
		'actif',
		'actif_vente'
	];

	public function marque()
	{
		return $this->belongsTo(\App\Models\Marque::class, 'nom_marque');
	}

	public function categorie()
	{
		return $this->belongsTo(\App\Models\Categorie::class, 'nom_categorie');
	}

	public function fournisseur()
	{
		return $this->belongsTo(\App\Models\Fournisseur::class, 'nom_fournisseur');
	}

	public function unite_mesure()
	{
		return $this->belongsTo(\App\Models\UniteMesure::class, 'format');
	}

	public function panier_articles()
	{
		return $this->hasMany(\App\Models\PanierArticle::class, 'rowid_article');
	}

	public function periode_articles()
	{
		return $this->hasMany(\App\Models\PeriodeArticle::class, 'article_rowid');
	}
}
