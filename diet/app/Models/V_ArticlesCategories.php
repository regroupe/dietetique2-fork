<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V_ArticlesCategories extends Model
{
	protected $table = 'v_stat_articles_categories';

	protected $casts = [
		'data' => 'int'
	];

	protected $fillable = [
		'label',
		'data'
	];
}
