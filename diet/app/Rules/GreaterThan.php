<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class GreaterThan implements Rule
{
	protected $min;

	/**
	 * Create a new rule instance.
	 *
	 * @param $min
	 */
	public function __construct($min)
	{
		// Here we are passing the min value to use it in the validation.
		$this->min = $min;
	}

	/**
	 * Determine if the validation rule passes.
	 *
	 * @param string $attribute
	 * @param mixed $value
	 * @return bool
	 */
	public function passes($attribute, $value)
	{
		// This is where you define the condition to be checked.
		return $value >= $this->min;         
	}

	/**
	 * Get the validation error message.
	 *
	 * @return string
	 */
	public function message()
	{
		// Customize the error message
		return 'Ce champ doit être supérieur à \''.$this->min.'\'.'; 
	}
}
?>