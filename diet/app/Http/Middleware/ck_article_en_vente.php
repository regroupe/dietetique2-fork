<?php

namespace App\Http\Middleware;
use App\Models\Article;

use Closure;

class ck_article_en_vente
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $article = Article::where('rowid', $request->rowid)->firstOrFail()->actif_vente;
        }
        catch (Exception $E)
        {
            return back();
        }

        if ($article || session()->has('admin')){
             return $next($request);
        }else return back();




       
    }
}
