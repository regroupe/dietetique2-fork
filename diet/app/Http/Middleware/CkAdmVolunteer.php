<?php

namespace App\Http\Middleware;

use Closure;

class CkAdmVolunteer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('admin')->take_order)
        {
            return $next($request);
        }

        return redirect()->action('AdminController@IndexCompte');
    }
}
