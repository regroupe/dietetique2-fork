<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\PeriodeController;

class CkOrderPeriod
{
    /**
     * Check to make sure an order period is active before proceeding.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!PeriodeController::OrderPeriodActive())
        {
            return redirect()->back();
        }
        return $next($request);
    }
}
