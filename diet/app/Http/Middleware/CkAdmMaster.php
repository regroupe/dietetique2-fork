<?php

namespace App\Http\Middleware;

use Closure;

class CkAdmMaster
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('admin')->super == 1 && session('admin')->master == 1)
        {
            return $next($request);
        }
        return redirect()->action('AdminController@IndexCompte');
    }
}
