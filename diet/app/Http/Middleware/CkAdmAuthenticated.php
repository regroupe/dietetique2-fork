<?php

namespace App\Http\Middleware;

use Closure;

class CkAdmAuthenticated
{
    /**
     * Determine si un admin est connecté.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('admin') == null)
        {
            return redirect()->action('ConnexionController@IndexConnexion');
        }
        return $next($request);
    }
}
