<?php

namespace App\Http\Middleware;

use Closure;

class ChangePass
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('admin')->change_pass == true)
        {
            return redirect()->action('AdminController@MustReset');
        }
        return $next($request);
    }
}
