<?php

namespace App\Http\Middleware;

use Closure;

class CkCltAuthenticated
{
    /**
     * Détermine si le client est connecté.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('client') == null)
        {
            return redirect()->action('ConnexionController@IndexConnexion');
        }

        return $next($request);
    }
}
