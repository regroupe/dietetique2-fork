<?php


namespace App\Http\Controllers;
use App\Models\Marque;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use View;
use Response;


class MarqueController extends Controller
{

    public static function Read() {
        
        $marques = Marque::All()->where('actif',1);

        return $marques;
    }

    public function Index()
    {
        $marques = Marque::paginate(15);

        $searchInfo = [2, 15];
        
        return view('marque.index',compact('marques', 'searchInfo'));
    }

   
    public static function RechercheMarque_POST(Request $request){
        $val = $request->search;
        $actif = $request->actif;
        $nbpage = $request->filter_pages;
        
        if ($val == null && $actif == null && $nbpage == null){
            return redirect()->action('MarqueController@Index');
        }
        else if($val == null){
            return redirect()->action('MarqueController@RechercheMarqueActif', ['actif'=>$actif, 'nbpage'=>$nbpage]);
        }else if ($actif == null)
        {
            $actif = 2;
        }
        
        return redirect()->action('MarqueController@RechercheMarque_GET', ['val'=>$val,'actif'=>$actif, 'nbpage'=>$nbpage]);
    }

    
    public static function RechercheMarque_GET(Request $request, $val, $actif,$nbpage){
        if ($actif == 1)
        {
            $marques = Marque::where([['nom_marque', 'like', $val.'%'],['actif', '=', '1']])->paginate($nbpage);
        }else  if ($actif == 0){
            $marques = Marque::where([['nom_marque', 'like', $val.'%'],['actif', '=', '0']])->paginate($nbpage);
        }else{
            $marques = Marque::where('nom_marque', 'like', $val.'%')->paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        return view('marque.index', compact('marques', 'searchInfo'));
    }

    
    public static function RechercheMarqueActif(Request $request, $actif,$nbpage){
        if ($actif == 1)
        {
            $marques = Marque::where('actif', '=', '1')->paginate($nbpage);
        }else  if ($actif == 0){
            $marques = Marque::where('actif', '=', '0')->paginate($nbpage);
        }else{
           $marques = Marque::paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        return view('marque.index', compact('marques', 'searchInfo'));
    }





    public function desactiverMarque(Request $request){
        foreach($request->nom_marque as $id){
            Marque::where('nom_marque','=',$id)->update(['actif'=> 0]);
        }
    }

    public function activerMarque(Request $request){
        foreach($request->nom_marque as $id){
            Marque::where('nom_marque','=',$id)->update(['actif'=>1]);
        }
    }

    public function Edit(Request $request)
    {
        
        foreach($request->nom_marque as $id){
            Marque::where('nom_marque','=',$id)->update(['nom_marque'=>$request->new_name]);
        }
        /*
        $uneMarque = Marque::find($id);
        $uneMarque ->nom_marque = $request->input('nom');
        $uneMarque ->save();

        return redirect('marque');
        */    
    }

    public function RegisterMarque(Request $req)
    {
        $validator = Validator::make($req->all(), [
        'nom_marque'=> 'unique:marque|required|max:255'
        ]);

        if ($validator->fails()) {
            $msg = "Marque invalide.";
            return Response::json(['msg'=>$msg]);
        }

        $new_marque = New Marque();
        $new_marque ->nom_marque = $req->nom_marque;
        $new_marque -> save(); 

        $marques = Marque::All()->where('actif',1);

        $html = View::make('article._marques',compact('marques', 'new_marque'))->render();
        $msg = "";
        return Response::json(['html'=> $html,'msg'=>$msg]);
    }

    public function RegisterMarque_FORM(Request $req)
    {
        $req->validate([
            'nom_marque'=> 'unique:marque|required|max:255'
        ]);
        $new_marque = New Marque();
        $new_marque ->nom_marque = $req->nom_marque;
        $new_marque -> save(); 

        return back()->with('success', 'Marque ajoutée!');
    }
}