<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\PeriodeController;
use App\Http\Controllers\PersonnalisationController;
use App\Models\Article;
use App\Models\PanierClient;
use App\Models\PanierArticle;
use App\Models\PeriodeArticle;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PanierController extends Controller
{
    /**
     * Main view.
     */
    public function Index() {

        return view('panier.index');
    }

    /**
     * Fetch all the articles present in the client's basket.
     */
    public static function Read() {
        
        // Retrieve the client's basket.
        $client = session('client')->rowid;
        $panier = PanierClient::where('rowid_client', $client)->first();

        // Return each articles present in the basket through Eloquent ORM.
        return $panier->panier_articles;
    }

    /**
     * Compute the client's basket amounts. (Subtotal and Taxes)
     */
    public static function ReadSummary() {

    	// Retrieve the client's basket.
    	$articles = PanierController::Read();

    	// Compute the subtotals of the basket.
    	$subtotal = 0;
        $subtotaltaxes = 0;
	    foreach ($articles as $articlePanier) {

	        $quantity = $articlePanier->quantite;
	        $subtotal += ($quantity * $articlePanier->article->prix);            
            $subtotaltaxes += ($quantity * ArticleController::ReadPricePlus($articlePanier->article));
	    }

	    // Return associative array containing the basket's amount values.
	    return compact('subtotal', 'subtotaltaxes');
    }

    /**
     * Update or add a product's quantity into the client basket.
     */
    public function Update_POST(Request $req) {

        $client = session('client')->rowid;
        $article = Article::find($req->rowid);
        $panier = PanierClient::where('rowid_client', $client)->first();
        $quantitySuccess = 0;
        $orderedQuantity = 0;

        // Attempt to get the current ordered quantity of a product.
        if (PeriodeController::OrderPeriodActive()) {

            $periode = PeriodeController::GetActivePeriod();

            try {
                $periodeArticle = PeriodeArticle::where('article_rowid', $article->rowid)
                    ->where('periode_rowid', $periode->rowid)->firstOrFail();
                $orderedQuantity = $periodeArticle->quantite_commande;
            } catch (ModelNotFoundException $e) { 
                $orderedQuantity = 0;
            }
        }

        try {

            // Attempt to find the article in the client's basket in order to update the quantity.
            $articlePanier = PanierArticle::where('rowid_panier', $panier->rowid)->where('rowid_article', $req->rowid)->firstOrFail();

            if ($req->quantity <= 0) {
                $articlePanier->delete();
            } else {
                $articlePanier->quantite = $req->update 
                    ? min($req->quantity, $article->quantite_maximum - $orderedQuantity)
                    : min($articlePanier->quantite + $req->quantity, $article->quantite_maximum - $orderedQuantity);
                $articlePanier->save();
                $quantitySuccess = $req->update ? $articlePanier->quantite : 1;
            }

        } catch (ModelNotFoundException $e) {

            // If the model was not in the basket, add it with the desired quantity.
            if ($req->quantity > 0) {
                $articlePanier = new PanierArticle;
                $articlePanier->rowid_panier = $panier->rowid;
                $articlePanier->rowid_article = $req->rowid;
                $articlePanier->quantite = min($req->quantity, $article->quantite_maximum - $orderedQuantity);
                $quantitySuccess = $articlePanier->quantite;

                // Add the product to the basket if the available quantity is sufficient.
                if ($quantitySuccess > 0) $articlePanier->save();
            }
        }

        // Prepare the basket summary.
        $summary = PanierController::ReadSummary();
        $subtotal = $summary['subtotal'];
        $subtotaltaxes = $summary['subtotaltaxes'];
        $taxes = $subtotaltaxes - $subtotal;

        $formatNumber = PersonnalisationController::GetNumberFormat();
        $subtotal = number_format($subtotal, 2, $formatNumber->decimal, $formatNumber->separator);
        $subtotaltaxes = number_format($subtotaltaxes, 2, $formatNumber->decimal, $formatNumber->separator);
        $taxes = number_format($taxes, 2, $formatNumber->decimal, $formatNumber->separator);

        // Return the updated basket amount after updating the article's quantity in the basket.
        return response()->json(compact('subtotal', 'subtotaltaxes', 'taxes', 'quantitySuccess'));
    }

    /**
     * Remove an article from the basket and precompute the new basket summary.
     */
    public function Remove_POST(Request $req) {

    	// Remove the article from the client's basket.
        $client = session('client')->rowid;
        $panier = PanierClient::where('rowid_client', $client)->first();
    	$articlePanier = PanierArticle::where('rowid_panier', $panier->rowid)->where('rowid_article', $req->rowid)->delete();

    	// Return the updated basket amount after removing the article from the basket.
        return response()->json(PanierController::ReadSummary());
    }
    /**
     * Counting the number of items in shopping cart
     */
    /*
    public static function CountItems(){
        $client = session('client')->rowid;
        $panier = PanierClient::where('rowid_client',$client)->first();
        $count = PanierArticle::where('rowid_panier',$panier->rowid)->count();
    }
    */
}
?>