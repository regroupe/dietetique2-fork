<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\PeriodeController;
use App\Models\Personnaliser;
use App\Models\Faq;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;
use Purifier;

class ParametragesController extends Controller
{
    
    public function index(){
        return view('parametrages.indexParametres');
    }
    
    public function Apropos()
    {
        return view('parametrages.apropos');
    }

    public function FAQ()
    {
        return view('parametrages.faq');
    }
       
    public function TermsOfUse()
    {
        return view('parametrages.termsofuse');
    }



    public function EditAPropos_GET($id){
            $theme = Personnaliser::where('rowid', $id)->first();
         return view('parametrages.createinfo', compact('theme'));
    }

    public function SaveAbout_POST(Request $req){
        $this->validate($req, [
            'summernoteInput' => 'required|max:1431655765',
        ]);

        $theme = Personnaliser::find($req->rowid);
        $theme->about_html = clean($req->summernoteInput);
        $theme->save();

        return redirect()->action('PersonnalisationController@Index');
    }

    public function EditFAQ_GET($id){
         $theme = Personnaliser::where('rowid', $id)->first();
         return view('parametrages.createfaq', compact('theme'));
    }   
    
    public function EditFAQ_POST(Request $req){
        $req->validate([
            'question.*' => 'required|max:255',
            'reponse_question.*' => 'required|max:10000',
        ]);


        Faq::where('rowid_theme', $req->rowid)->delete();

        if ($req->question != null){
            foreach ($req->question as $index => $quest){
            $faq = new Faq();
            $faq->rowid_theme = $req->rowid;
            $faq->question = $quest;
            $faq->reponse = $req->reponse_question[$index];
            $faq->save();
            }
        }

        return redirect()->action('PersonnalisationController@Index');
    }   

    public static function ReadFAQ($id){
        $faq = Faq::where('rowid_theme', $id)->get();
        return $faq;
    }

    //Conditions et politiques d'utilisation
        public function EditTermsofuse_GET($id){
            $theme = Personnaliser::where('rowid', $id)->first();
         return view('parametrages.createtermsofuse', compact('theme'));
    }

    //Save terms of use
    public function SaveTerms_POST(Request $req){
        $this->validate($req, [
            'summernoteInput' => 'required|max:1431655765',
        ]);

        $theme = Personnaliser::find($req->rowid);
        $theme->termsofuse = clean($req->summernoteInput);
        $theme->save();

        return redirect()->action('PersonnalisationController@Index');
    }

    /*Upload the received image and return the corresponding link*/
    public function Heberger_POST(Request $req){
        $req->validate([
            'img' => 'mimes:jpg,png,jpeg|max:50000'
        ]);

        //Store the image
        if ($req->hasFile('img')) {
            $link = $req->file('img')->store('public/hosted_images');
        }

        return url(Storage::url($link));
    }
    
}
?>