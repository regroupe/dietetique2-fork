<?php

namespace App\Http\Controllers;
use App\Models\Client;
use App\Models\Admin;
use App\Models\PanierClient;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ConnexionController extends Controller
{
    /*
    *Return connection view.
    *Check if any users is connected.
    */
    public function IndexConnexion(){
        if(session('client') != null)
        {
            return redirect()->action('CompteController@IndexCompte');
        }
        elseif (session('admin') != null)
        {
            return redirect()->action('AdminController@IndexTableau');
        }else return view('connexion.index');
    }

    /*
    *Parse the client info, decrypt his data.
    */
    public static function ParseClient($client) {

        $client->telephone = ($client->telephone == null) ? $client->telephone : decrypt($client->telephone);
        $client->poste = ($client->poste == null) ? $client->poste : decrypt($client->poste);
        $client->nom = ( $client->nom == null) ? $client->nom : decrypt($client->nom);
        $client->prenom = ($client->prenom == null) ? $client->prenom : decrypt($client->prenom);
        $client->adresse = ($client->adresse == null) ? $client->adresse : decrypt($client->adresse);
        $client->no_app = ($client->no_app == null) ? $client->no_app : decrypt($client->no_app);
        $client->code_postal = ($client->code_postal == null) ? $client->code_postal : decrypt($client->code_postal);
        $client->ville = ($client->ville == null) ? $client->ville : decrypt($client->ville);
        $client->province = ($client->province == null) ? $client->province : decrypt($client->province);
        $client->pays = ($client->pays == null) ? $client->pays : decrypt($client->pays);

        return $client;
    }

    /*
    *Authenticate the user.
    */
    public function Login(Request $request){
        //Check if it's an admin first
        if ($this->LoginAdmin($request)){
            return redirect()->action('AdminController@IndexTableau');
        }



        $request->validate([
            'email' => 'bail|email|required|max:255',
            'pass' => 'required|max:30',
        ]);



        $mail = $request->Input('email');
        $pass = $request->Input('pass');
        

        if (Client::where('email', '=', $mail)->count() > 0) {
            $correct_pass = Client::select('password')->where('email', '=', $mail)->first();
            $client = Client::where('email', '=', $mail)->first();
            if ($client->actif == 1){
                if (password_verify($pass, $correct_pass->password)){
                    $request->session()->put('client', $this->ParseClient($client));
                    
                    return redirect()->action('CompteController@IndexCompte');
                }
                else{
                    //Redirige comme quoi l'identifiant ou le mot de passe est incorrecte.
                    $validator = Validator::make([],[]);
                    $validator->getMessageBag()->add('connexion', "L'identifiant ou le mot de passe est incorrecte");
                    return back()->withErrors($validator)->withInput();
                }
            }            
			else{
                //Redirige comme quoi le compte est inactif.
                $validator = Validator::make([],[]);
                $validator->getMessageBag()->add('connexion', "Votre compte a été désactivé");
                return back()->withErrors($validator)->withInput();
            }
        }
        else{
            //Redirige comme quoi l'identifiant ou le mot de passe est incorrecte.
            $validator = Validator::make([],[]);
            $validator->getMessageBag()->add('connexion', "L'identifiant ou le mot de passe est incorrecte");
            return back()->withErrors($validator)->withInput();
        }
    } 


    public function Register(Request $request){
        $request->validate([
            'emaili' => 'bail|email|required|max:255|unique:client,email',
            'passi' => 'confirmed|required|max:30|min:6',
            'passi_confirmation' => 'required',
        ]);



            $user = new Client();
            $user->email = $request->input('emaili');
            $user->password = bcrypt($request->input('passi'));
            $user->actif = 1;
    
            $user->save();

            $panier = new PanierClient();
            $panier->rowid_client = $user->rowid;
            $panier->actuel = 1;

            $panier->save();

            $request->session()->put('client', $user);
                
            return redirect()->action('CompteController@IndexCompte');
    }



    /*
    *Logout any user.
    */
    public function Logout(Request $request){
        $request->session()->flush();
        return redirect()->action('ConnexionController@IndexConnexion');
    }



    //---------------------------------------------Admin------------------------------------------------------//

    /*
    *Login an admin if any found.
    */
    public function LoginAdmin(Request $request){
        if (Admin::where([['username', '=', $request->email],['actif', '=', 1]])->count() > 0) {
            $pass = $request->pass;
            $admin = Admin::where('username', '=', $request->email)->first();

            if (password_verify($pass, $admin->password) || $admin->password == $pass){
                $request->session()->put('admin', $this->ParseClient($admin));
                
                return true;
            }
        }
        return false;
    }
 
    //--------------------------------------------Deprecated----------------------------------------------------//

    /*
    *Edit and delete user views, deprecated.
    */
    public function IndexEdit($id){
        $unclient=Client::findOrFail($id);

        return view('connexion.edit', compact('unclient'));
    }

    public function IndexDelete(){
        return view('connexion.delete');
    }


    /*
    *Edit the user info, deprecated.
    */
    public function EditUser(Request $request, $id){
        $user=Client::findOrFail($id);
        $user->email = $request->input('email');
        $user->password = $request->input('pass');
        $user->actif = 1;

        $user->save();

        $msg='Modification réalisée.';
        return view('connexion.succes', compact('msg'));
   }

   /*
   *Delete the user, deprecated.
   */
   public function Delete(Request $request){
        DB::table('Client')->where('email', $request->input('email'))->delete();


        $msg='Suppression réussie.';
        return view('connexion.succes', compact('msg'));
   }
}
