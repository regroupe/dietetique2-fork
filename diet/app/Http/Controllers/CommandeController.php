<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers;
use App\Models\Article;
use App\Models\Client;
use App\Models\PeriodeArticle;
use App\Models\PanierClient;
use App\Models\PanierArticle;
use App\Models\Commande;
use App\Models\CommandeArticle;
use App\Models\PeriodeCommande;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CommandeController extends Controller
{
	public function ParseProduct($product) {

		$article = new CommandeArticle;
        $article->article_rowid = $product->rowid;
        $article->image = $product->image;
        $article->nom = $product->nom;
        $article->format_quantite = $product->format_quantite;
        $article->format = $product->format;
        $article->provenance = $product->provenance;
        $article->allergene = $product->allergene;
        $article->perissable = $product->perissable;
        $article->bio = $product->bio;
        $article->quebec = $product->quebec;
        $article->nom_marque = $product->nom_marque;
        $article->nom_categorie = $product->nom_categorie;
        $article->nom_fournisseur = $product->nom_fournisseur;
        $article->prix = $product->prix;
        $article->frais_fixe = $product->frais_fixe;
        $article->frais_variable = $product->frais_variable;
        $article->economie = $product->economie;
        $article->provenance = $product->provenance;

        return $article;
    }

    public function View_GET($rowid) {

        $client = session('client')->rowid;
        $commande = Commande::where('rowid', $rowid)->first();
        $commande = $this->parseAdresse($commande);

        if ($commande->rowid_client != $client) return redirect()->back();

        return view('commande.index', compact('commande'));
    }

    public static function parseAdresse($commande){
        $commande->email = ($commande->email === NULL) ? $commande->email : decrypt($commande->email);
        $commande->adresse = ($commande->adresse === NULL) ? $commande->adresse : decrypt($commande->adresse);
        $commande->no_app = ($commande->no_app === NULL) ? $commande->no_app : decrypt($commande->no_app);
        $commande->ville = ($commande->ville === NULL) ? $commande->ville : decrypt($commande->ville);
        $commande->province = ($commande->province === NULL) ? $commande->province : decrypt($commande->province);
        $commande->pays = ($commande->pays === NULL) ? $commande->pays : decrypt($commande->pays);
        $commande->code_postal = ($commande->code_postal === NULL) ? $commande->code_postal : decrypt($commande->code_postal);
        $commande->telephone = ($commande->telephone === NULL) ? $commande->telephone : decrypt($commande->telephone);
        $commande->poste = ($commande->poste === NULL) ? $commande->poste : decrypt($commande->poste);
        return $commande;
    }

	/*public static function parseAdresse($commande){
        $commandeParsed = new Commande;
        $commandeParsed->rowid = $commande->rowid;
        $commandeParsed->rowid_client = $commande->rowid_client;
        $commandeParsed->rowid_periode = $commande->rowid_periode;
        $commandeParsed->sous_total = $commande->sous_total;
        $commandeParsed->total = $commande->total;
        $commandeParsed->nom_client = $commande->nom_client;
        $commandeParsed->date_commande = $commande->date_commande;
        $commandeParsed->identifiant = $commande->identifiant;
        $commandeParsed->paye = $commande->paye;
        $commandeParsed->prise = $commande->prise;
        $commandeParsed->signature = $commande->signature;
        $commandeParsed->email = ($commande->email === NULL) ? $commande->email : decrypt($commande->email);
        $commandeParsed->adresse = ($commande->adresse === NULL) ? $commande->adresse : decrypt($commande->adresse);
        $commandeParsed->no_app = ($commande->no_app === NULL) ? $commande->no_app : decrypt($commande->no_app);
        $commandeParsed->ville = ($commande->ville === NULL) ? $commande->ville : decrypt($commande->ville);
        $commandeParsed->province = ($commande->province === NULL) ? $commande->province : decrypt($commande->province);
        $commandeParsed->pays = ($commande->pays === NULL) ? $commande->pays : decrypt($commande->pays);
        $commandeParsed->code_postal = ($commande->code_postal === NULL) ? $commande->code_postal : decrypt($commande->code_postal);
        $commandeParsed->telephone = ($commande->telephone === NULL) ? $commande->telephone : decrypt($commande->telephone);
        $commandeParsed->poste = ($commande->poste === NULL) ? $commande->poste : decrypt($commande->poste);
        return $commandeParsed;
    }*/

    public function ViewAdmin_GET($client, $rowid) {

        $client = Controllers\CompteController::DecryptAdresse(Client::where('rowid', $client)->first());
        $commande = Commande::where('rowid', $rowid)->first();
        $commande = $this->parseAdresse($commande);

        if ($commande->rowid_client != $client->rowid) return redirect()->back();

        return view('commande.index_admin', compact('commande', 'client'));
    }
}