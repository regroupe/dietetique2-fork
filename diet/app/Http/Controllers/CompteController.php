<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Client;
use App\Models\Admin;

class CompteController extends Controller
{
    /*
    *Return the client account view with its users info.
    */
    public function IndexCompte(){

            $usr = Client::where('rowid','=', session('client')->rowid)->first();

            $usr = $this->DecryptAdresse($usr);
            

            return view('compte.index', compact('usr'));
 
    }

    /*
    *Decrypt the client address.
    */
    public static function DecryptAdresse($usr){
        $usr->nom = ($usr->nom === NULL) ? $usr->nom : decrypt($usr->nom); 
        $usr->prenom = ($usr->prenom === NULL) ? $usr->prenom : decrypt($usr->prenom);
        $usr->adresse = ($usr->adresse === NULL) ? $usr->adresse : decrypt($usr->adresse);
        $usr->no_app = ($usr->no_app === NULL) ? $usr->no_app : decrypt($usr->no_app);
        $usr->ville = ($usr->ville === NULL) ? $usr->ville : decrypt($usr->ville);
        $usr->province = ($usr->province === NULL) ? $usr->province : decrypt($usr->province);
        $usr->code_postal = ($usr->code_postal === NULL) ? $usr->code_postal : decrypt($usr->code_postal);
        $usr->telephone = ($usr->telephone === NULL) ? $usr->telephone : decrypt($usr->telephone);
        $usr->poste = ($usr->poste === NULL) ? $usr->poste : decrypt($usr->poste);

        return $usr;
    }

    /*
    *Return the commands history of the client.
    */
    public function IndexHistorique(){

            $usr = Client::where('rowid','=', session('client')->rowid)->first();


            return view('compte.historique', compact('usr'));
    }

    /*
    *Return the commands history of a client requested by an admin.
    */
    public function IndexHistoriqueAdmin($id){
            $usr = Client::where('rowid','=', $id)->first();


            return view('compte.historique_admin', compact('usr'));
    }

    /*
    *Modifie le mot de passe du client, si celui-ci confirme son mot de passe actuel et que le nouveau est valide.
    */
    public function ResetPassword(Request $request){
            $actuelpass = $request->input('actuelpass');
            $bdpass = Client::select('password')->where('rowid', '=', session('client')->rowid)->first();
            
            //Effectue la validation sur le mot de passe actuel
            if (password_verify($actuelpass, $bdpass->password))
            {  
                //Autres validations
                 $validator = Validator::make($request->all(), [
                'actuelpass' => 'required|max:30',
                'nvpass' => 'required|min:6|max:30|confirmed',
                'nvpass_confirmation' => 'required',
                ]);

                if ($validator->fails()) {
                    return back()
                                ->withErrors($validator)
                                ->withInput();
                }


                $nvpass = bcrypt($request->input('nvpass'));
                $user = Client::where('rowid','=', session('client')->rowid)->first()->update([
                    'password' => $nvpass
                ]);
                
                session()->put('client', Client::where('email','=', session('client')->email)->first());

                return back()->with('success', 'Mot de passe modifié');
            }
            else{
                $validator = Validator::make([],[]);
                //Renvoie l'erreur du mot de passe actuel.
                $validator->getMessageBag()->add('Motdepasse', 'Votre mot de passe actuel ne correspond pas');
                return back()
                            ->withErrors($validator)
                            ->withInput();
            }

           return back()->withErrors($validator)->withInput();
    }

    /*
    *Sauvegarde l'addresse de facturation du client.
    */
    public function SaveAddress(Request $request){
        
        $validator = Validator::make($request->all(), [
                'nm' => 'required|max:50',
                'pnm' => 'required|max:50',
                'address' => 'required|max:255',
                'appart' => 'numeric|digits_between:0,6|nullable',
                'ville' => 'required|max:255',/*
                'prov' => 'required|max:255',
                'pays' => 'required|max:255',*/
                'code_p' => array(
                                    'required',
                                    'max:7',
                                    'regex:/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/i',
                                ),
                'tlphne' => 'required',
                'poste' => 'numeric|digits_between:0,6|nullable',
                ]);

        //Validation telephone
        $justNums = preg_replace("/[^0-9.]+/", '', $request->tlphne );

        if ((strlen($justNums) != 10) && strlen($justNums) != 11){
             $validator->getMessageBag()->add('tlphne', "Numéro de téléphone invalide");
            return back()->withErrors($validator)->withInput();
        }

         if ($validator->fails()) 
         {
            return back()->withErrors($validator)->withInput();
         }
         

         $user = Client::where('rowid','=', session('client')->rowid)->first()->update([
                        'nom' => encrypt($request->input('nm')),
                        'prenom' => encrypt($request->input('pnm')),
                        'adresse' => encrypt($request->input('address')),
                        'no_app' => encrypt($request->input('appart')),
                        'code_postal' => encrypt(strtoupper($request->input('code_p'))),
                        'ville' => encrypt($request->input('ville')),
                        'pays' => encrypt($request->input('pays')),
                        'province' => encrypt($request->input('prov')),
                        'telephone' => encrypt($justNums),
                        'poste' => encrypt($request->input('poste')),
         ]);
         
         $user = Client::where('rowid','=', session('client')->rowid)->first();

         $user = ConnexionController::ParseClient($user);

         session()->put('client', $user);

        return back()->with('successA', 'Adresse modifiée');;
    }


    /*
    *Check if the connected user can place a command.
    */
    public static function CheckIfAddressExist(){
        if (session('client')->nom && session('client')->nom)
        {
            if (session('client')->adresse)
            {
                if (session('client')->telephone)
                {
                     if (session('client')->code_postal && session('client')->ville)
                     {
                        return true;
                     }
                }
            }
        }
        return false;
    }
}

