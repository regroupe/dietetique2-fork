<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Personnaliser;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers;

class PersonnalisationController extends Controller
{
    /*
    *Return the view with the theme list.
    */
    public function Index(){
        $persos = Personnaliser::orderBy('rowid', 'asc')->paginate(15);
        return view('parametrages.index', compact('persos'));
    }

    /*
    *Return the active theme.
    */
    public static function ReadActif(){
        $mainTheme = Personnaliser::where('actif', 1)->first();
        if (!$mainTheme) $mainTheme = new Personnaliser();

        return $mainTheme;
    }

    /*
    *Return the view to create a theme.
    */
    public function CreateTheme_GET(){
        $theme = new Personnaliser();
        return view('parametrages.ajoutertheme', compact('theme'));
    }

    /*
    *Return the view to edit a theme.
    */
    public function EditTheme_GET($id){
        $theme = Personnaliser::where('rowid', $id)->first();
        return view('parametrages.modifiertheme', compact('theme'));
    }

    /*
    *Validate, save and activate the new or already existing theme.
    */
    public function SaveTheme_POST(Request $req){

        $validator = Validator::make($req->all(), [
            'nom_theme' => 'required|max:255',
            'nom_article' => 'max:255',
            'nom_article_plur' => 'max:255',
            'affichage_decimal' => 'max:1',
            'affichage_separator' => 'max:1',
            'titre_onglet' => 'max:255',
            'icon' => 'mimes:ico|max:1024',
            'logo' => 'mimes:jpeg,jpg,png|max:10000',
            'banniere' => 'mimes:jpeg,jpg,png|max:50000',
            'footer_desc' => 'max:1028|nullable',
            'footer_entreprise' => 'max:255|nullable',
            'footer_copyright' => 'max:512|nullable',
            'footer_address' => 'max:255|nullable',
            'footer_ville' => 'max:255|nullable',
            'footer_code_postal' => array(
                'max:7',
                'nullable',
                'regex:/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/i',
            ),
            'car1' => 'mimes:jpeg,jpg,png|max:50000',
            'car1_msg' => 'max:50',
            'car1_link' => 'max:255',
            'car2' => 'mimes:jpeg,jpg,png|max:50000',
            'car2_msg' => 'max:50',
            'car2_link' => 'max:255',
            'car3' => 'mimes:jpeg,jpg,png|max:50000',
            'car3_msg' => 'max:50',
            'car3_link' => 'max:255'
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        //Validate the phone.
        $justNums = preg_replace("/[^0-9.]+/", '', $req->footer_telephone );




        if ($justNums != null){
             if ((strlen($justNums)  != 10) && (strlen($justNums) != 11)){
                $validator->getMessageBag()->add('footer_telephone', "Numéro de téléphone invalide");
                return back()->withErrors($validator)->withInput();
            }
        }

        
       
        
        //Save the theme.
        $theme = $this->parseTheme($req,  $justNums);

        //Generate the success message
        if ($theme->rowid != null) {
            $msg = 'Thème '.$theme->nom_theme.' ('.$theme->rowid.') modifié avec succès!' ;
            $theme->save();
        }
        else{
            $theme->save();
            $msg = 'Thème '.$theme->nom_theme.' ('.$theme->rowid.') ajouté avec succès!';
        }


        return redirect()->action('PersonnalisationController@Index')->with('success', $msg);
    }

    /*
    *Create the theme and save it.
    */
    public function parseTheme($req, $telephone){
        

        $theme = ($req->rowid == 0 ? new Personnaliser : Personnaliser::find($req->rowid));

        //Générals
        $theme->nom_theme = $req->nom_theme;

        $theme->nom_article = strtolower($req->nom_article);
        $theme->nom_article_plur = strtolower($req->nom_article_plur);
        
        $theme->affichage_decimal = $req->affichage_decimal;
        $theme->affichage_separator = $req->affichage_separator;

        $theme->titre_onglet = $req->titre_onglet;

        //icon
        if ($req->hasFile('icon')) {
            $theme->icon = $req->file('icon')->store('public/themes/icons');
        } else {
            if ($req->icon_dirty) $theme->icon = NULL;
        }

        //Logo
        if ($req->hasFile('logo')) {
            $theme->logo = $req->file('logo')->store('public/themes/logos');
        } else {
            if ($req->logo_dirty) $theme->logo = NULL;
        }

        //Bannières
        if ($req->hasFile('banniere')) {
            $theme->banniere = $req->file('banniere')->store('public/themes/bannieres');
        } else {
            if ($req->banniere_dirty) $theme->banniere = NULL;
        }

        //Footer
        $theme->footer_desc = $req->footer_desc;
        $theme->footer_entreprise = $req->footer_entreprise;
        $theme->footer_address = $req->footer_address;
        $theme->footer_city = $req->footer_ville;
        $theme->footer_telephone = $telephone;
        $theme->footer_code_postal = strtoupper($req->footer_code_postal);
        $theme->footer_copyright = $req->footer_copyright;

        //Carrousel
        if ($req->hasFile('car1')) {
            $theme->car1 = $req->file('car1')->store('public/themes/carrousel');
        } else {
            if ($req->car1_dirty) $theme->car1 = NULL;
        }
        $theme->car1_msg = $req->car1_msg;
        $theme->car1_link = $req->car1_link;

        if ($req->hasFile('car2')) {
            $theme->car2 = $req->file('car2')->store('public/themes/carrousel');
        } else {
            if ($req->car2_dirty) $theme->car2 = NULL;
        }
        $theme->car2_msg = $req->car2_msg;
        $theme->car2_link = $req->car2_link;

        if ($req->hasFile('car3')) {
            $theme->car3 = $req->file('car3')->store('public/themes/carrousel');
        } else {
            if ($req->car3_dirty) $theme->car3 = NULL;
        }
        $theme->car3_msg = $req->car3_msg;
        $theme->car3_link = $req->car3_link;



        return $theme;
    }

    /*
    *Activate the selected theme and disable the current main theme (function).
    */
    public function ActivateTheme($id){
        Personnaliser::where('actif', 1)->update(['actif' => 0]);
        Personnaliser::where('rowid', $id)->update(['actif' => 1]);
    }

    /*
    *Activate the selected theme and disable the current main theme (function and return).
    */
    public function ActivateTheme_GET($id){
        Personnaliser::where('actif', 1)->update(['actif' => 0]);
        Personnaliser::where('rowid', $id)->update(['actif' => 1]);

        return redirect()->action('PersonnalisationController@Index');
    }

    /*
    *Delete a theme.
    */
    public function DeleteTheme_GET($id){
        Personnaliser::where('rowid', $id)->delete();

        return redirect()->action('PersonnalisationController@Index');
    }

    /*
    * Create the link associated to the given name.
    */
    public static function linkNameToLink($name){
        switch($name)
        {
            case 'boutique': return action("BoutiqueController@Index");
            case 'calendrier': return action("PeriodeController@IndexCalendar");
            case 'connexion': return action("ConnexionController@IndexConnexion");
            case 'panier': return action("PanierController@Index");
            case 'propos': return action("ParametragesController@Apropos");
            case 'faq': return action("ParametragesController@FAQ");
            default: return '';
        }
    }

    public static function listsLinkName(){
        $value = array('#', 'propos', 'boutique', 'calendrier', 'connexion', 'faq', 'panier');
        $name = array('Aucun', 'À propos', 'Boutique', 'Calendrier', 'Connexion','FAQ', 'Panier');
        return json_encode([$value, $name]);
    }

    /*
    * Get the name that defines the product sold.
    */
    public static function GetSellerName($plurial, $initcap){
        $theme = Personnaliser::where('actif', 1)->first();

        if ($theme != null){
            if ($theme->nom_article != null && $theme->nom_article_plur != null)
            if ($plurial){
                if ($initcap) return ucfirst($theme->nom_article_plur); else return $theme->nom_article_plur;
            } else{
                if ($initcap) return ucfirst($theme->nom_article); else return $theme->nom_article;
            }
        }
        
        //Default
        if ($plurial){
            if ($initcap) return 'Produit(s)'; else return 'produit(s)';
        } else{
            if ($initcap) return 'Produit'; else return 'produit';
        }
    }

    public static function GetNumberFormat() {

        $theme = PersonnalisationController::ReadActif();
        if ($theme->rowid == null) (object)['decimal' => '.', 'separator' => ''];

        $decimal = $theme->affichage_decimal == '_' ? ' ' : $theme->affichage_decimal;
        $separator = $theme->affichage_separator == '_' ? ' ' : $theme->affichage_separator;

        return (object)['decimal' => $decimal, 'separator' => $separator];
    }
}