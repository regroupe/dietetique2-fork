<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers;
use App\Models\Article;
use App\Models\Client;
use App\Models\PeriodeArticle;
use App\Models\PanierClient;
use App\Models\PanierArticle;
use App\Models\Commande;
use App\Models\CommandeArticle;
use App\Models\PeriodeCommande;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\PayPalOrder;
use App\PayPal;
use Illuminate\Http\Request;

class PayPalController extends Controller
{
    public function form()
    {
        return view('panier.index');
    }

    public function checkout(Request $request)
    {
        $order = $this->CreateOrder();

        if ($order == null ) {
            return redirect()->action('PanierController@Index');
        }

        $paypal = new PayPal;

        $response = $paypal->purchase([
            'amount' => $paypal->formatAmount($order->total),
            'transactionId' => $order->identifiant,
            'currency' => 'CAD',
            'cancelUrl' => $paypal->getCancelUrl($order->identifiant),
            'returnUrl' => $paypal->getReturnUrl($order->identifiant),
        ]);

        if ($response->isRedirect()) {
			$request->session()->flash("reference", $response->getTransactionReference());
            return redirect($response->getRedirectUrl() . '&useraction=commit');
        }

        return redirect()->back()->with([
            'message' => $response->getMessage(),
        ]);
    }

    public function completed($order, Request $request)
    {
        $order = Commande::where('identifiant', $order)->first();

        $paypal = new PayPal;

        $response = $paypal->complete([
            'amount' => $paypal->formatAmount($order->total),
            'transactionId' => $order->identifiant,
            'currency' => 'CAD',
            'cancelUrl' => $paypal->getCancelUrl($order->identifiant),
            'returnUrl' => $paypal->getReturnUrl($order->identifiant),
			
			'transactionReference' => $request->session()->get("reference"),
			'payerId' => $request->get("PayerID")
        ]);

        if ($response->isSuccessful()) {
            
            // Clear the client's cart after successful payment.
            $panier = PanierClient::where('rowid_client', $order->rowid_client)->first();

            foreach ($panier->panier_articles as $articlePanier) {

                // Update the total ordered quantity of the product.
                $article = $articlePanier->article;
                try {

                    // If the product exists, update the quantity for the given period.
                    $periodeArticle = PeriodeArticle::where('article_rowid', $article->rowid)->where('periode_rowid', $order->rowid_periode)->firstOrFail();
                    $periodeArticle->quantite_commande = $periodeArticle->quantite_commande + $articlePanier->quantite;
                    $periodeArticle->save();

                } catch (ModelNotFoundException $e) {

                    // Else, create the product and set the ordered quantity.
                    $periodeArticle = new PeriodeArticle;
                    $periodeArticle->article_rowid = $article->rowid;
                    $periodeArticle->periode_rowid = $order->rowid_periode;
                    $periodeArticle->quantite_commande = $articlePanier->quantite;
                    $periodeArticle->save();
                }
            }

            PanierArticle::where('rowid_panier', $panier->rowid)->delete();
            
            $order->paye = true;
            $order->save();

            return redirect()->action('CommandeController@View_GET', $order->rowid);
        }

        return redirect()->action('PanierController@Index')->with([
            'error-paypal' => "Une erreur s'est produite.",
        ]);
    }

    public function cancelled()
    {
        return redirect()->action('PanierController@Index')->with([
            'error-paypal' => "La commande n'a pas été effectuée.",
        ]);
    }

    public function CreateOrder() {

        // Retrieve the client's basket.
        $client = session('client');
        $panier = PanierClient::where('rowid_client', $client->rowid)->first();
        $articlesPanier = PanierController::Read();
        $period = PeriodeController::GetActivePeriod();

        // If the basket is empty redirect back to where we came from.
        if (count($articlesPanier) <= 0) return null;

        // Create the order.
        $commande = new Commande;
        $commande->rowid_client = $client->rowid;
        $commande->rowid_periode = $period->rowid;
        $commande->date_commande = date('Y-m-d H:i:s');
        $commande->nom_client = $client->prenom . ' ' . $client->nom;
        $commande->email = encrypt($client->email);
        $commande->adresse = encrypt($client->adresse);
        $commande->no_app = encrypt($client->no_app);
        $commande->ville = encrypt($client->ville);
        $commande->province = encrypt($client->province);
        $commande->pays = encrypt($client->pays);
        $commande->code_postal = encrypt($client->code_postal);
        $commande->telephone = encrypt($client->telephone);
        $commande->poste = encrypt($client->poste);
        $commande->notify = 1;
        $commande->save();

        $subtotal = 0;
        $total = 0;
        foreach ($articlesPanier as $articlePanier) {

        	// Update the total ordered quantity of the product.
        	$article = $articlePanier->article;

        	// Add product to the client's order.
        	$articleCommande = $this->ParseProduct($article);
        	$articleCommande->commande_rowid = $commande->rowid;
        	$articleCommande->quantite_commande = $articlePanier->quantite;
        	$articleCommande->save();

        	// Compute totals.
        	$subtotal += $articleCommande->prix * $articlePanier->quantite;
        	$total += Controllers\ArticleController::ReadPricePlus($articleCommande) * $articlePanier->quantite;
        }

        // Remove all products from the basket and update order totals.
        $commande->sous_total = $subtotal;
        $commande->total = $total;
        $commande->identifiant = GenerateGuid();
        $commande->paye = false;
        $commande->save();

        return $commande;
    }

    public function ParseProduct($product) {

		$article = new CommandeArticle;
        $article->article_rowid = $product->rowid;
        $article->image = $product->image;
        $article->nom = $product->nom;
        $article->format_quantite = $product->format_quantite;
        $article->format = $product->format;
        $article->provenance = $product->provenance;
        $article->allergene = $product->allergene;
        $article->perissable = $product->perissable;
        $article->bio = $product->bio;
        $article->quebec = $product->quebec;
        $article->nom_marque = $product->nom_marque;
        $article->nom_categorie = $product->nom_categorie;
        $article->nom_fournisseur = $product->nom_fournisseur;
        $article->prix = $product->prix;
        $article->frais_fixe = $product->frais_fixe;
        $article->frais_variable = $product->frais_variable;
        $article->economie = $product->economie;
        $article->provenance = $product->provenance;

        return $article;
    }
}

?>