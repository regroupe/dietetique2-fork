<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Client;
use View;
use Response;
use DB;

class GestionCompteController extends Controller
{

    //Index////////////


    //Retourne la vue avec la liste des admins
    public static function ReadAdmin(){
        $compte = Admin::orderBy('username', 'asc')->paginate(15);

        $compte->withPath('admin');
        $searchInfo = [2,''];
        $html = View::make('administrateur.gestioncompte._vueadmin', compact('compte', 'searchInfo'))->render();
        

        return view('administrateur.gestioncompte.index', compact('compte', 'html'));
    }

    //Retourne la vue avec la liste des clients
    public static function ReadClient(){
        $compte = Client::orderBy('email', 'asc')->paginate(15);
        $compte->withPath('client');
        $searchInfo = [2,''];

        $html = View::make('administrateur.gestioncompte._vueclient', compact('compte', 'searchInfo'))->render();
        return view('administrateur.gestioncompte.index', compact('compte', 'html'));
    }
    
    //Retourne la vue d'ajout de comptes
    public static function ReadAjouter(){
        return view('administrateur.gestioncompte.ajouter');
    }


    //////////////////
    //RECHERCHE

    //Récupère la value de la recherche (client) et call la route Get (Pour afficher la value dans l'url)
    public static function RechercheClientPOST(Request $request){
        $val = $request->search;
        $actif = $request->actif;
        $nbpage = $request->filter_pages;
        
        if ($val == null && $actif == null && $nbpage == null){
            return redirect()->action('GestionCompteController@ReadClient');
        }
        else if($val == null){
            return redirect()->action('GestionCompteController@RechercheClientActif', ['actif'=>$actif, 'nbpage'=>$nbpage]);
        }else if ($actif == null)
        {
            $actif = 2;
        }
        
        return redirect()->action('GestionCompteController@RechercheClient', ['val'=>$val,'actif'=>$actif, 'nbpage'=>$nbpage]);
    }

    //Récupère la value de la recherche (admin) et call la route Get (Pour afficher la value dans l'url)
    public static function RechercheAdminPOST(Request $request){
        $val = $request->search;
        $actif = $request->actif;
        $nbpage = $request->filter_pages;


        if ($val == null && $actif == null && $nbpage == null){
            return redirect()->action('GestionCompteController@ReadAdmin');
        }
        else if($val == null){
            return redirect()->action('GestionCompteController@RechercheAdminActif', ['actif'=>$actif, 'nbpage'=>$nbpage]);
        }else if ($actif == null)
        {
            $actif = 2;
        }

        return redirect()->action('GestionCompteController@RechercheAdmin', ['val'=>$val,'actif'=>$actif, 'nbpage'=>$nbpage]);
    }

    //Recherche dans les emails des clients avec la value du POST précédent
    public static function RechercheClient(Request $request, $val, $actif,$nbpage){
        if ($actif == 1)
        {
            $compte = Client::orderBy('email', 'asc')->where([['email', 'like', $val.'%'],['actif', '=', '1']])->paginate($nbpage);
        }else  if ($actif == 0){
            $compte = Client::orderBy('email', 'asc')->where([['email', 'like', $val.'%'],['actif', '=', '0']])->paginate($nbpage);
        }else{
            $compte = Client::orderBy('email', 'asc')->where('email', 'like', $val.'%')->paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        $html = View::make('administrateur.gestioncompte._vueclient', compact('compte', 'searchInfo'))->render();
        $compte->withPath('client/recherche/');
        return view('administrateur.gestioncompte.index', compact('compte', 'html'));
    }

    //Recherche dans les emails des clients avec la value d'actif
    public static function RechercheClientActif(Request $request, $actif,$nbpage){
        if ($actif == 1)
        {
            $compte = Client::orderBy('email', 'asc')->where('actif', '=', '1')->paginate($nbpage);
        }else  if ($actif == 0){
            $compte = Client::orderBy('email', 'asc')->where('actif', '=', '0')->paginate($nbpage);
        }else{
           $compte = Client::paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        $html = View::make('administrateur.gestioncompte._vueclient', compact('compte', 'searchInfo'))->render();
        $compte->withPath('client/recherche/');
        return view('administrateur.gestioncompte.index', compact('compte', 'html'));
    }

    //Recherche dans les usernames des admins avec la value du POST précédent
    public static function RechercheAdmin(Request $request, $val,$actif,$nbpage){
        if ($actif == 1)
        {
            $compte = Admin::orderBy('username', 'asc')->where([['username', 'like', $val.'%'],['actif', '=', '1']])->paginate($nbpage);
        }else  if ($actif == 0){
            $compte = Admin::orderBy('username', 'asc')->where([['username', 'like', $val.'%'],['actif', '=', '0']])->paginate($nbpage);
        }else{
            $compte = Admin::orderBy('username', 'asc')->where('username', 'like', $val.'%')->paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        $html = View::make('administrateur.gestioncompte._vueadmin', compact('compte', 'searchInfo'))->render();
        $compte->withPath('admin/recherche/');

        return view('administrateur.gestioncompte.index', compact('compte', 'html'));
    }

    //Recherche dans les emails des clients avec la value d'actif
    public static function RechercheAdminActif(Request $request, $actif, $nbpage){
        if ($actif == 1)
        {
            $compte = Admin::orderBy('username', 'asc')->where('actif', '=', '1')->paginate($nbpage);
        }else  if ($actif == 0){
            $compte = Admin::orderBy('username', 'asc')->where('actif', '=', '0')->paginate($nbpage);
        }else{
           $compte = Admin::paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        $html = View::make('administrateur.gestioncompte._vueadmin', compact('compte', 'searchInfo'))->render();
        $compte->withPath('client/recherche/');
        return view('administrateur.gestioncompte.index', compact('compte', 'html'));
    }

    ///////////////////
    //ACTIVER/DÉSACTIVER

    public function ActiverCompte(Request $request){
        foreach ($request->rowids as $id){
            if ($request->tpe == 'client')
            {
                Client::where('rowid', '=', $id)->update(['actif' => 1]);
            }else if($request->tpe == 'admin'){
                try{
                    if ($this->checkUserRights(session('admin'),0) > $this->checkUserRights(Admin::findOrFail($id),1))
                    {
                        Admin::where('username', '=', $id)->update(['actif' => 1, 'last_modif' => session('admin')->username]);
                    }
                }catch (Exception $e) {}
            }else if ($request->tpe == 'categorie'){
                Categorie::where('nom_categorie','=',$id)->update(['actif'=> 1]);
            }
        }
    }
   

    public function DesactiverCompte(Request $request){
        foreach ($request->rowids as $id){
            if ($request->tpe == 'client')
            {
                Client::where('rowid', '=', $id)->update(['actif' => 0]);
            }else if($request->tpe == 'admin'){
                if (session('admin')->username != $id)
                {
                    try{
                        if ($this->checkUserRights(session('admin'),0) > $this->checkUserRights(Admin::findOrFail($id),1))
                        {
                            Admin::where('username', '=', $id)->update(['actif' => 0, 'last_modif' => session('admin')->username]);
                        }
                    }catch (Exception $e) {}
                }        
            }else if ($request->tpe == 'categorie'){
                Categorie::where('nom_categorie','=',$id)->update(['actif'=> 0]);
            }
        }
    }

    //////////////////
    //Génère des comptes

    /*
    *Check if the super havn't try to cheat on the user info.
    */
    public function GenererPOSTSuper(Request $request){

        if ($request->typeUsr == 1 || $request->typeUsr == 2)  
            $request->typeUsr = 0;

        return $this->GenererCompte($request);
    }

    /*
    *Master has all rights to generate.
    */
    public function GenererPOSTMaster(Request $request){
        return $this->GenererCompte($request);
    }

    /*
    *Generate the number of users asked.
    */
    public function GenererCompte(Request $request){
        
        $request->validate([
            'nomUsr' => 'alpha|required|max:15',
            'typeUsr' => 'required',
            'nbUsr' => 'integer|max:30|required',
        ]);

        //Détermine si le nom existe déjà dans la bd, si oui récupère son incrément.
        $increment = Admin::where('username', 'RLIKE', '^('.$request->nomUsr.')[0-9]+$')->max('username');
        $increment = (int) filter_var($increment, FILTER_SANITIZE_NUMBER_INT) +1;

        //Défini le type de compte.
        if ($request->typeUsr == 1)
        {
            $super = 1;
            $take_order = 1;
        }
        else{
            $super = 0;
        }
        $passList = [];
        $account = [];

        //Crée autant d'utilisateurs que demandé.
        for ($i = 0; $i < $request->nbUsr; $i++){
            $newUsr = new Admin();

            $newUsr->super = $super;
            $newUsr->master = 0;
            $newUsr->take_order = ($request->typeUsr != 0);

            $inc = $i+$increment;
            $inc = (string) str_pad($inc, 5, "0", STR_PAD_LEFT);

            $newUsr->username = $request->nomUsr.$inc;
            $account[] = $request->nomUsr.$inc;

            $passList[] = $this->GenererPass();
            $newUsr->password = bcrypt($passList[sizeof($passList)-1]);
            
            $newUsr->last_modif = session('admin')->username;
            $newUsr->change_pass = 1;

            $newUsr->actif = 1;

            $newUsr->save();
        }

        $msg = 'Comptes générés';

        return back()->with(compact('msg', 'passList', 'account'));

    }

     //Crée un seul compte.
     public function CreerCompte(Request $request){
        
        $request->validate([
            'nomUsr2' => 'alpha_num|required|max:30|unique:admin,username',
            'typeUsr2' => 'required',
        ]);
            $newUsr = new Admin();
            $super = $master = 0;

            switch($request->typeUsr2){
                case 1:$super=1; break;
                case 2:{
                        $master=1;
                        $super=1;
                    }break;
                default;
            }

            $newUsr->super = $super;
            $newUsr->master = $master;
            $newUsr->take_order = ($request->typeUsr2 != 0);

            $newUsr->username = $request->nomUsr2;
            $account[] = $request->nomUsr2;

            $passList[] = $this->GenererPass();
            $newUsr->password = bcrypt($passList[sizeof($passList)-1]);
            
            $newUsr->last_modif = session('admin')->username;
            $newUsr->change_pass = 1;

            $newUsr->actif = 1;

            $newUsr->save();

            $msg = 'Compte créé';

            return back()->with(compact('msg', 'passList', 'account'));
    }

    //Regenerate passwords of the users id list received
    public function Regenerer(Request $request)
    {
            $nb = 0;
            $passList= [];
            $accountList = [];
            foreach ($request->rowids as $id){
                if (session('admin')->username != $id)
                {
                    try{
                        if ($this->checkUserRights(session('admin'),0) > $this->checkUserRights(Admin::findOrFail($id),1))
                        {
                            $passList[] = $this->GenererPass();
                            $accountList[] = $id;
                            Admin::where('username', '=', $id)->update(['password' => $passList[sizeof($passList)-1],'change_pass' =>1, 'last_modif' => session('admin')->username]);
                            $nb++;
                        }
                    }catch (Exception $e) {}
                }        
            }
            $data['account'] = $accountList;
            $data['pass'] = $passList;
            $data['nb'] = $nb;
        
        return Response::json($data);
    }

    //Génère un mot de passe
    public function GenererPass(){
        $bytes = openssl_random_pseudo_bytes(4);

        return bin2hex($bytes);
    }

    //Détermine le niveau de droit d'un utilisateur
    public function checkUserRights($user, $minus){
        if ($user->super==1 && $user->master==1)
        {
            return 2 - $minus;
        }elseif ($user->super==1 && $user->master==0)
        {
            return 1;
        }else{
            return 0;
        }
    }
}
