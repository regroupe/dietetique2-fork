<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\AdminBoard;
use View;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    /*
    *Return the board view (home page of admin).
    */
    public function IndexTableau(){
            return view('administrateur.tableaubord');
    }

    /*
    * Return the board edit view.
    */
    public function EditBoard_GET(){
            return view('administrateur.editboard');
    }

    /*
    * Read board item from session username.
    */
    public static function ReadBoardItem(){
        $items = AdminBoard::where('username_admin', session('admin')->username)->get();
        return $items;
    }

    /*
    * Create an item view for the editable board.
    */
    public function AddRow1_POST(Request $req){
            $id = $req->id;
            $partials_list = json_decode(ReadPartials());
            return View::make('administrateur._boarditem_js', compact('partials_list', 'id'))->render();
    }

    /*
    * Validate and save the partials selected to be the new board.
    */
    public function SaveBoard_POST(Request $req){
        $req->validate([
            'board_item.*' => 'required|max:255',
            'board_size.*' => 'required|max:50',
            'board_color.*' => 'required|max:50'
        ]);

        $this->CreateBoardItem($req);

        return redirect()->action('AdminController@IndexTableau');
    }

    /*
    * Create an object foreach views and save it.
    */
    public function CreateBoardItem($req){
        AdminBoard::where('username_admin', session('admin')->username)->delete();

        if ($req->board_item != null)
        {
            foreach($req->board_item as $i => $item)
            {
                $boardItem = new AdminBoard;
                $boardItem->username_admin = session('admin')->username;
                $boardItem->vue = $item;
                $boardItem->vue_size = $req->board_size[$i];
                $boardItem->vue_color = $req->board_color[$i];
                $boardItem->save();
            }
        }
    }













    /*********USER INFO********************************************************************************/

    /*
    *Return the account view.
    */
    public function IndexCompte(){
            return view('administrateur.compte');
    }

    /*
    *Return the must reset view (Appear when the user must reset its password).
    */
    public function MustReset(){
        if (session('admin')->change_pass == 1){
            return view('administrateur.mustchangepass');
        }
        else  return redirect()->action('AdminController@IndexCompte');
    }


    /*
    *Modifie le mot de passe de l'admin, si celui-ci confirme son mot de passe actuel et que le nouveau est valide.
    */
    public function ResetPassword(Request $request){
            $actuelpass = $request->input('actuelpass');
            $bdpass = Admin::select('password')->where('username', '=', session('admin')->username)->first();
            
            //Effectue la validation sur le mot de passe actuel
            if (password_verify($actuelpass, $bdpass->password) || $bdpass->password == $actuelpass)
            {  
                //Autres validations
                 $validator = Validator::make($request->all(), [
                'actuelpass' => 'required|max:30',
                'nvpass' => 'required|min:6|max:30|confirmed',
                'nvpass_confirmation' => 'required',
                ]);

                if ($validator->fails()) {
                    return back()
                                ->withErrors($validator)
                                ->withInput();
                }


                $nvpass = bcrypt($request->input('nvpass'));
                $user = Admin::where('username','=', session('admin')->username)->first()->update([
                    'password' => $nvpass,
                    'change_pass' => 0,
                ]);
                
                session()->put('admin', Admin::where('username','=', session('admin')->username)->first());

                return back()->with('success', 'Mot de passe modifié');;
            }
            else{
                $validator = Validator::make([],[]);
                //Renvoie l'erreur du mot de passe actuel.
                $validator->getMessageBag()->add('Motdepasse', 'Votre mot de passe actuel ne correspond pas');
                return back()
                            ->withErrors($validator)
                            ->withInput();
            }

           return back()->withErrors($validator)->withInput();
        }

}
