<?php

namespace App\Http\Controllers;

use DB;
use View;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Models\Commande;
use App\Models\PeriodeCommande;
use App\Models\PeriodePlage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\http\Controllers\ArticleController;
use App\Models\PeriodeArticle;  

class PeriodeController extends Controller
{
    public function IndexCalendar() {
        return view('periodecommande.calendrier');
    }

    public function GetCalendar(Request $req) {

        $periods = PeriodeCommande::where([['date_debut', '<=', $req->end], ['date_fin', '>=', $req->start]])
            ->orWhere([['date_fin', '>=', $req->start], ['date_debut', '<=', $req->end]])
            ->get()->toArray();

        $pickups = PeriodePlage::where([['date_debut', '<=', $req->end], ['date_fin', '>=', $req->start]])
            ->orWhere([['date_fin', '>=', $req->start], ['date_debut', '<=', $req->end]])
            ->get()->toArray();

        return compact('periods', 'pickups');
    }

    public static function GetAllPeriods(){

        $periods = PeriodeCommande::all();

        return $periods;
    }

    public static function GetAllPickups(){
        
        $periods = PeriodePlage::all();

        return $periods;
    }

    /*
    *Return the view of periods management.
    */
    public function IndexPeriode(){
        return view('periodecommande.index');
    }

    public function IndexProgression($id){
        try{
            $period = PeriodeCommande::findOrFail($id);
            $articles = $this->ArticlePeriod($period->rowid);
            return view('periodecommande.detailperiode', compact('period','articles'));
        }
        catch (Exception $e)
        {
            return redirect()->action('PeriodeController@IndexPeriode');
        }
    }

     /*
    *Return all periods after current date.
    */
    public static function GetAllPeriodsAfterToday(){
        $date = Carbon::now('America/Toronto')->toDateTimeString();
        $periods = PeriodeCommande::all()->where('date_debut', '>=', $date)->sortBy('date_debut');

        return $periods;
    }

     /*
    *Return archive view.
    */
    public function archive_GET(){
        $date = Carbon::now('America/Toronto')->toDateTimeString();
        $periods = PeriodeCommande::where('date_fin', '<', $date)->orderBy('date_debut', 'desc')->paginate(15);
        $annee = null;

        return view('periodecommande.archive', compact('periods', 'annee'));
    }

    /*
    *Get the input for the archive search.
    */
    public function RechercheArchive_POST(Request $request){
        $annee = $request->annee;

        if ($annee == "Toutes les années")
        {
            return redirect()->action('PeriodeController@archive_GET');
        }else return redirect()->action('PeriodeController@RechercheArchive_GET', $annee);
    }

    /*
    *Return the archive search view .
    */
    public function RechercheArchive_GET($annee){
         $date = Carbon::now('America/Toronto')->toDateTimeString();

        $periods = PeriodeCommande::where(DB::raw('YEAR(date_debut)'), '=', $annee)
            ->where('date_fin', '<', $date)
            ->orderBy('date_debut', 'desc')->paginate(15);

        return view('periodecommande.archive', compact('periods', 'annee'));
    }

    /*
    *Return each year from the existing periods.
    */
    public static function YearOfPeriods(){
        $years = PeriodeCommande::select(DB::raw('distinct YEAR(date_debut) as year'))->orderBy('year', 'desc')->paginate(15);

        return $years;
    }

     /*
    *Return add view.
    */
    public function AjouterPeriode_GET(){
        $period = null;
        $editdate = null;
        return view('periodecommande.formperiode', compact('period', 'editdate'));
    }

    /*
    *Return edit view.
    */
    public function EditPeriode_GET($id){
        $period = PeriodeCommande::where('rowid', $id)->first();



        if ($period->date_debut < Carbon::now('America/Toronto')->toDateTimeString())
        {
            $editdate = false;
            return view('periodecommande.formperiode', compact('period', 'editdate'));
        }
        else{
            $editdate = true;
            return view('periodecommande.formperiode', compact('period', 'editdate'));
        }
    }

    public function IndexPartial(Request $req, $id) {

        $commandes = Commande::where(function ($query) use(&$req, &$id) {

            $query->where('paye', 1);

            // Apply order period filter.
            $query->where('rowid_periode', $id);

            // Apply search filter.
            $query->where('nom_client', 'like', '%'.$req->filter_search.'%');

            // Apply pickup filter.
            if ($req->filter_active) $query->where('prise', $req->filter_active == 1 ? '1' : '0');

        })->when($req->sort_name, function ($query, $sort) {

            // Apply sort filter.
            if ($sort == 1) $query->orderBy('nom_client', 'asc');
            if ($sort == 2) $query->orderBy('nom_client', 'desc');

        })->paginate($req->filter_pages ? $req->filter_pages : 10);
        $commandes->appends(Input::except('page'));

        return View::make('periodecommande._prisecommande', compact('commandes'))->render();
    }

    public function OrderPickup_GET(Request $req, $id){

        $period = PeriodeCommande::where('rowid', $id)->first();
        $partial = $this->IndexPartial($req, $id);

        if ($req->ajax()) {
            return $partial;
        }

        return view('periodecommande.prisecommande', compact('partial', 'req', 'period'));
    }

    public function OrderPickup_POST(Request $req, $id){

        // Laravel is bad at saving blobs, set the signature to null before saving.
        $order = Commande::find($req->rowid);
        $order->signature = null;
        $order->save();

        // Update the order status to picked up or not.
        $order->signature = $req->signature;
        $order->prise = $req->status;
        $order->save();

        // Parse the search form's serialized data to an array of values.
        parse_str($req->state, $state);
        $partial = $this->IndexPartial(new Request($state), $id);

        return $partial;
    }

    /*
    *Add a new period.
    */
    public function AjouterPeriode_POST(Request $request){
        $request->validate([
            'date_debut' => 'required|date',
            'time_debut' => 'date_format:H:i',
            'date_fin' => 'required|date',
            'time_fin' => 'date_format:H:i'
        ]);

        if (!$request->date_debut >= Carbon::now('America/Toronto'))
        {
             $validator = Validator::make([],[]);
             $validator->getMessageBag()->add('error_msg', "La date de début doit être après la date d'aujourd'hui.");
             return back()->withErrors($validator)->withInput();
        }
        if ($request->date_debut >= $request->date_fin && $request->time_debut >= $request->time_fin || $request->date_debut >= $request->date_fin)
        {
             $validator = Validator::make([],[]);
             $validator->getMessageBag()->add('error_msg', "La date de fin doit être après la date de début.");
             return back()->withErrors($validator)->withInput();
        }

        $derniere_periode  = PeriodeCommande::orderBy('date_fin', 'desc')->first();

        if ($derniere_periode != null){
            if ($request->date_debut <= $derniere_periode->date_fin)
            {
                $validator = Validator::make([],[]);
                $validator->getMessageBag()->add('error_msg', "La date de début doit être après la date de fin de la dernière période saisite (".$derniere_periode->date_fin.").");
                return back()->withErrors($validator)->withInput();
            }
        }

        $date_debut = Carbon::createFromFormat('Y-m-d H:i', $request->date_debut.' '.$request->time_debut);
        $date_fin = Carbon::createFromFormat('Y-m-d H:i', $request->date_fin.' '.$request->time_fin);

        $periode = new PeriodeCommande();
        $periode->date_debut = $date_debut;
        $periode->date_fin = $date_fin;
        $periode->save();
        
        if ($request->c_emp != null)
        {
             if (count($request->c_emp) >= 1)
                {
                    foreach ($request->c_emp as $index => $emplacement)
                    {
                        if($this->validateCueillette($emplacement, $request->c_debut[$index], $request->c_fin[$index]))
                            {

                            $plage = new PeriodePlage();
                            $plage->rowid_periode =  $periode->rowid;
                            $plage->emplacement = $emplacement;
                            $plage->date_debut = Carbon::createFromFormat('Y-m-d H:i', $request->c_debut[$index]);
                            $plage->date_fin = Carbon::createFromFormat('Y-m-d H:i', $request->c_fin[$index]);
                            $plage->save();
                            }
                            else{
                                PeriodeCommande::where('rowid',$periode->rowid)->delete();
                                $validator = Validator::make([],[]);
                                $validator->getMessageBag()->add('error_msg', "Certaine(s) période(s) de cueillette sont invalide(s).");
                                return back()->withErrors($validator)->withInput();
                            } 
                    }
                }
        }

        return redirect()->action('PeriodeController@IndexPeriode')->with('success', 'Période créée!');
    }

    /*
    *Edit the period with the folowing id.
    */
    public function EditPeriode_POST(Request $request, $id){
        $request->validate([
            'date_debut' => 'required|date',
            'time_debut' => 'date_format:H:i',
            'date_fin' => 'required|date',
            'time_fin' => 'date_format:H:i'
        ]);


        if (!$request->date_debut >= Carbon::now('America/Toronto'))
        {
             $validator = Validator::make([],[]);
             $validator->getMessageBag()->add('error_msg', "La date de début doit être après la date d'aujourd'hui.");
             return back()->withErrors($validator)->withInput();
        }

        if ($request->date_debut >= $request->date_fin && $request->time_debut >= $request->time_fin || $request->date_debut >= $request->date_fin)
        {
             $validator = Validator::make([],[]);
             $validator->getMessageBag()->add('error_msg', "La date de fin doit être après la date de début.");
             return back()->withErrors($validator)->withInput();
        }

        


        try{
            $periode = PeriodeCommande::findOrFail($id);

            $derniere_periode  = PeriodeCommande::orderBy('date_fin', 'desc')->first();
            echo $derniere_periode;

            if ($derniere_periode->rowid != $periode->rowid){
                
                if ($request->date_debut <= $derniere_periode->date_fin)
                {
                    $validator = Validator::make([],[]);
                    $validator->getMessageBag()->add('error_msg', "La date de début doit être après la date de fin de la dernière période saisite (".$derniere_periode->date_fin.").");
                    return back()->withErrors($validator)->withInput();
                }

            }


            $periode->date_debut = Carbon::createFromFormat('Y-m-d H:i', $request->date_debut.' '.$request->time_debut);
            $periode->date_fin = Carbon::createFromFormat('Y-m-d H:i', $request->date_fin.' '.$request->time_fin);

            $periode->save();

            PeriodePlage::where('rowid_periode', $id)->delete();

            if ($request->c_emp != null)
            {   
                if (count($request->c_emp) >= 1)
                {
                    foreach ($request->c_emp as $index => $emplacement)
                    {
                        if($this->validateCueillette($emplacement, $request->c_debut[$index], $request->c_fin[$index]))
                        {

                        $plage = new PeriodePlage();
                        $plage->rowid_periode = $id;
                        $plage->emplacement = $emplacement;
                        $plage->date_debut = Carbon::createFromFormat('Y-m-d H:i', $request->c_debut[$index]);
                        $plage->date_fin = Carbon::createFromFormat('Y-m-d H:i', $request->c_fin[$index]);
                        $plage->save();
                        }
                        else{
                            PeriodePlage::where('rowid_periode', $id)->delete();
                            $validator = Validator::make([],[]);
                            $validator->getMessageBag()->add('error_msg', "Certaine(s) période(s) de cueillette sont invalide(s).");
                            return back()->withErrors($validator)->withInput();
                        } 
                    }
                }
            }

            return redirect()->action('PeriodeController@IndexPeriode')->with('success', 'Période modifiée!');
        }
        catch (Exception $e){
            return redirect()->action('PeriodeController@IndexPeriode')->with('error', 'Une erreur s\'est produite');
        }
    }

    /*
    *Edit the period with the folowing id (Only pickup points).
    */
    public function EditPicksOnly_POST(Request $request, $id){

        try{
            $periode = PeriodeCommande::findOrFail($id);
            PeriodePlage::where('rowid_periode', $id)->delete();
            if ($request->c_emp != null)
            {
                if (count($request->c_emp) >= 1)
                {
                    foreach ($request->c_emp as $index => $emplacement)
                    {
                        if($this->validateCueillette($emplacement, $request->c_debut[$index], $request->c_fin[$index]))
                        {

                        $plage = new PeriodePlage();
                        $plage->rowid_periode = $id;
                        $plage->emplacement = $emplacement;
                        $plage->date_debut = Carbon::createFromFormat('Y-m-d H:i', $request->c_debut[$index]);
                        $plage->date_fin = Carbon::createFromFormat('Y-m-d H:i', $request->c_fin[$index]);
                        $plage->save();
                        }
                        else{
                            PeriodePlage::where('rowid_periode', $id)->delete();
                            $validator = Validator::make([],[]);
                            $validator->getMessageBag()->add('error_msg', "Certaine(s) période(s) de cueillette sont invalide(s).");
                            return back()->withErrors($validator)->withInput();
                        } 
                    }
                }
            }
            
            return redirect()->action('PeriodeController@IndexPeriode')->with('success', 'Période modifiée!');
        }
        catch (Exception $e){
            return redirect()->action('PeriodeController@IndexPeriode')->with('error', 'Une erreur s\'est produite');
        }
    }

    /*
    *Delete a specific period.
    */
    public function DeletePeriod($id){
        PeriodeCommande::where('rowid', $id)->delete();
        return redirect()->action('PeriodeController@IndexPeriode')->with('success', 'Période supprimée.');
    }


    /*
    *Validate a pick.
    */
    public function validateCueillette($emp, $deb, $fin){
        if ($emp == null || $emp == "")
        {
            return false;
        }

        if (strlen($emp) > 255)
        {
            return false;
        }

        if ($deb == null || $deb == "")
        {
            return false;
        }

        if ($fin == null || $fin == "")
        {
            return false;
        }

        try{
            Carbon::createFromFormat('Y-m-d H:i', $deb);
            Carbon::createFromFormat('Y-m-d H:i', $fin);
        } catch (\Exception $e)
        {
            return false;
        }
        return true;
    }


    /*
    *Return the current period.
    */
    public static function GetActivePeriod() {
        $date = Carbon::now('America/Toronto')->toDateTimeString();

        $period = PeriodeCommande::all()->where('date_debut', '<=', $date)->where('date_fin', '>=', $date);

        return $period->first();
    }

    /*
    *Return the next period scheduled.
    */
	public static function GetNextPeriod() {
        $date = Carbon::now('America/Toronto')->toDateTimeString();

		$period = PeriodeCommande::orderBy('date_debut', 'asc')->get()->where('date_debut', '>', $date)->first();

		return $period;
    }
    
    /*
    *Return the last finished period.
    */
	public static function GetLastFinishPeriod() {
        $date = Carbon::now('America/Toronto')->toDateTimeString();

        $period = PeriodeCommande::all()->where('date_debut', '<', $date)->where('date_fin', '<', $date)->sortByDesc('date_fin')->first();
		return $period;
	}

    /*
    *Return true or false if any periods is active.
    */
    public static function OrderPeriodActive() {
        $date = Carbon::now('America/Toronto')->toDateTimeString();

        $periods = PeriodeCommande::all()->where('date_debut', '<=', $date)->where('date_fin', '>=', $date);

        return count($periods) >= 1;
    }

    /*
    *Return the end date of the current period
    */
    public static function OrderPeriodEndDate() {

        if (!PeriodeController::OrderPeriodActive()) return NULL;
        $date = Carbon::now('America/Toronto')->toDateTimeString();

        $period = PeriodeCommande::all()->where('date_debut', '<=', $date)->where('date_fin', '>=', $date)->first();
        
        return $period->date_fin;
    }


     /*
     *Return the numbers of days between the date and the current date.
     */
     public static function DaysLeft($time, $abs) {
        $newTime = Carbon::parse($time);
        $newTime = Carbon::now('America/Toronto')->diffInDays($newTime, $abs);
        return $newTime;
     }

     /*
     *Return the numbers of days between two dates.
     */
    public static function DaysLeft_2var($time, $time2, $abs) {
        $newTime = Carbon::parse($time);
        $newTime2 = Carbon::parse($time2);
        $newTime = $newTime2->diffInDays($newTime, $abs);
        return $newTime;
    }

    /*
     *Return picking periods from an id.  
     */
    public static function PeriodeCueillette($id) {
        $picks = PeriodePlage::all()->where('rowid_periode', $id);
        return $picks;
    }

     /**
     * Get all article attached to this period id.
     */
    public static function ArticlePeriod($id){
        $articles = PeriodeArticle::where('periode_rowid', $id)
            ->join('article', 'article.rowid', '=', 'article_rowid')
            ->orderBy('nom_fournisseur', 'asc')
            ->orderBy('nom', 'asc')
            ->get();

        return $articles;
    }

     /**
     * Get all article attached to this period id (order by most completed first).
     */
    public static function ArticlePeriodMostCompleted($id){
        $articles = PeriodeArticle::join('article', 'article.rowid', '=', 'article_rowid')
            ->where('periode_rowid', $id)
            ->orderBy(DB::raw('quantite_commande/quantite_minimum'), 'desc')
            ->orderBy('nom', 'asc')
            ->get();

        return $articles;
    }
    
    public static function ParseDateLocale($date) {

        setlocale(LC_ALL, "fr_FR.UTF8");
        return strftime("%d %B %Y", strtotime(Carbon::parse($date)));
    }

    public static function ParseDateTimeLocale($date) {

        setlocale(LC_ALL, "fr_FR.UTF8");
        return strftime("%d %B %Y %H:%M", strtotime(Carbon::parse($date)));
        }
}
?>