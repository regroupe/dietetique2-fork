<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use View;

class ProduitOffertController extends Controller
{
    /*
    *Return the view of the products on sale.
    */
    public function IndexProduit(){
        $article = Article::orderBy('rowid', 'desc')->where([['actif','=','1'],['actif_vente','=','1']])->paginate(15);
        $dispo = 1;
        $searchInfo = [1,''];
        $html = View::make('prodoffert._vueproduit', compact('article','dispo', 'searchInfo'))->render();
  
        return view('prodoffert.index', compact('html'));
    }

    //Récupère la value de la recherche et call la route Get (Pour afficher la value dans l'url)
    public static function RecherchePOST(Request $request){
        $val = $request->search;
        $actif = $request->actif;
        $nbpage = $request->filter_pages;

        if ($val == null && $actif == null &&  $nbpage = null){
            return redirect()->action('ProduitOffertController@IndexProduit');
        }
        else if($val == null){
            return redirect()->action('ProduitOffertController@RechercheActifVente', ['dispo'=>$actif, 'nbpage'=> $nbpage]);
        }else if ($actif == null)
        {
            $actif = 1;
        }

        if ($request->search_type == 'crea')
        {
            $type_rech = 'created_by';
        }else{
            $type_rech = 'nom';
        }

        return redirect()->action('ProduitOffertController@Recherche', [ 'type_rech'=>$type_rech, 'val'=>$val,'dispo'=>$actif, 'nbpage'=> $nbpage]);
    }

    //Recherche dans les articles s'il est disponible ou non
    public static function RechercheActifVente(Request $request, $dispo, $nbpage){
        if ($dispo == 1)
        {
            $article = Article::orderBy('rowid', 'desc')->where([['actif_vente', '=', '1'],['actif','=','1']])->paginate($nbpage);
        }else  if ($dispo == 0){
            $article = Article::orderBy('rowid', 'desc')->where([['actif_vente', '=', '0'],['actif','=','1']])->paginate($nbpage);
        }else{
           return $article = Article::orderBy('rowid', 'desc')->where([['actif_vente', '=', '1'],['actif','=','1']])->paginate($nbpage);
        }

        $searchInfo = [$dispo,$nbpage];

        $html = View::make('prodoffert._vueproduit', compact('article','dispo','searchInfo'))->render();
        $article->withPath('recherche/');
        return view('prodoffert.index', compact('html'));
    }

    //Recherche dans les nom d'articles
    public static function Recherche(Request $request, $type_rech, $val,$dispo,  $nbpage){
        if ($dispo == 1)
        {
            $article = Article::orderBy('rowid', 'desc')->where([[$type_rech, 'like', $val.'%'],['actif_vente', '=', '1'],['actif','=','1']])->paginate($nbpage);
        }else  if ($dispo == 0){
            $article = Article::orderBy('rowid', 'desc')->where([[$type_rech, 'like', $val.'%'],['actif_vente', '=', '0'],['actif','=','1']])->paginate($nbpage);
        }else{
            $article = Article::orderBy('rowid', 'desc')->where([[$type_rech, 'like', $val.'%'],['actif_vente', '=', '1'],['actif','=','1']])->paginate($nbpage);
        }
        $searchInfo = [$dispo,$nbpage];
        $html = View::make('prodoffert._vueproduit', compact('article','dispo','searchInfo'))->render();
        $article->withPath('recherche/');
        return view('prodoffert.index', compact('html'));
    }

    /*
    *Put on sale every checked products.
    */
    public function Offrir(Request $request){
        foreach ($request->rowids as $id){
            Article::where('rowid', '=', $id)->update(['actif_vente' => 1]);
        }
    }

    /*
    *Retrieve from sale every checked products.
    */
    public function Enlever(Request $request){
        foreach ($request->rowids as $id){
            Article::where('rowid', '=', $id)->update(['actif_vente' => 0]);
        }
    }
}
