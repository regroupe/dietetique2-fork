<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UniteMesure;
use Illuminate\Support\Facades\Validator;
use View;
use Response;

class UniteMesureController extends Controller
{
     /**
     * Return the unit of measurement view. 
     */
    public function Index()
    {
        $unites = UniteMesure::paginate(15);

        $searchInfo =  [2, 15];
        
        return view('unitemesure.index',compact('unites', 'searchInfo'));
    }

    /**
     * Get an array with all the units of measurement.
     */
    public static function Read() {
        
        $unites = UniteMesure::All()->where('actif',1);

        return $unites;
    }    


     /**
     * Receive the research post data and redirect to the data corresponding function (Index, search active or search all).
     */
    public static function RechercheUnite_POST(Request $request){
        $val = $request->search;
        $actif = $request->actif;
        $nbpage = $request->filter_pages;
        
        if ($val == null && $actif == null && $nbpage == null){
            return redirect()->action('UniteMesureController@Index');
        }
        else if($val == null){
            return redirect()->action('UniteMesureController@RechercheUniteActif', ['actif'=>$actif, 'nbpage'=>$nbpage]);
        }else if ($actif == null)
        {
            $actif = 2;
        }
        
        return redirect()->action('UniteMesureController@RechercheUnite_GET', ['val'=>$val,'actif'=>$actif, 'nbpage'=>$nbpage]);
    }

    
    /**
     * Search unitemesure with the value requested from the post (see RechercheUnite_POST).
     */
    public static function RechercheUnite_GET(Request $request, $val, $actif,$nbpage){
        if ($actif == 1)
        {
            $unites = UniteMesure::where([['nom_unite', 'like', $val.'%'],['actif', '=', '1']])->paginate($nbpage);
        }else  if ($actif == 0){
            $unites = UniteMesure::where([['nom_unite', 'like', $val.'%'],['actif', '=', '0']])->paginate($nbpage);
        }else{
            $unites = UniteMesure::where('nom_unite', 'like', $val.'%')->paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        return view('unitemesure.index', compact('unites', 'searchInfo'));
    }

    /**
     * Search unitemesure with only the actif field from the post (see RechercheUnite_POST).
     */
    public static function RechercheUniteActif(Request $request, $actif,$nbpage){
        if ($actif == 1)
        {
            $unites = UniteMesure::where('actif', '=', '1')->paginate($nbpage);
        }else  if ($actif == 0){
            $unites = UniteMesure::where('actif', '=', '0')->paginate($nbpage);
        }else{
           $unites = UniteMesure::paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        return view('unitemesure.index', compact('unites', 'searchInfo'));
    }

    /**
     * Create a new unit, the call is from the form on the unite de mesure page.
     */
    public function RegisterUnite_FORM(Request $req)
    {
        $req->validate([
            'nom_unite'=> 'unique:unite_mesure|required|max:255'
        ]);
        $unite = New UniteMesure();
        $unite ->nom_unite = $req->nom_unite;
        $unite -> save();

        return back()->with('success', 'Unité de mesure ajoutée!');
    }

    /**
     * Create a new unit, from an ajax call. Return the list of units.
     */
    public function RegisterUnit(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'nom_unite'=> 'unique:unite_mesure|required|max:255'
            ]);

            if ($validator->fails()) {
                $msg = "Unité de mesure invalide.";
                return Response::json(['msg'=>$msg]);
            }

        $unite = New UniteMesure();
        $unite ->nom_unite = $req->nom_unite;
        $unite -> save();

        $unites = UniteMesure::All()->where('actif',1);
        $msg = "";
        $html = View::make('article._unites',compact('unites','unite'))->render();
        return Response::json(['html'=>$html, 'msg'=>$msg]);
    }



    /**
     * Edit an existing unit with the new name requested.
     */
    public function EditUnite(Request $request)
    {
        UniteMesure::where('nom_unite','=',$request->nom_unite)->update(['nom_unite'=>$request->new_name]);
    }

    /**
     * Desactivate one or many units.
     */
    public function DesactiverUnite(Request $request){
        foreach($request->nom_unite as $id){
            UniteMesure::where('nom_unite','=',$id)->update(['actif'=> 0]);
        }
    }

    /**
     * Activate one or many units.
     */
    public function ActiverUnite(Request $request){
        foreach($request->nom_unite as $id){
            UniteMesure::where('nom_unite','=',$id)->update(['actif'=>1]);
        }
    }
    

}
