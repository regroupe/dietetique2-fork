<?php


namespace App\Http\Controllers;
use App\Models\Fournisseur;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use View;
use Response;


class FournisseurController extends Controller
{
    public static function Read() {
        
        $fournisseurs = Fournisseur::All()->where('actif',1);

        return $fournisseurs;
    }    

public function ReadFournisseur()
{
    $fournisseurs = Fournisseur::paginate(15);

    $searchInfo =  [2, 15];
    
    return view('fournisseur.index',compact('fournisseurs', 'searchInfo'));
}



    public static function RechercheFournisseur_POST(Request $request){
        $val = $request->search;
        $actif = $request->actif;
        $nbpage = $request->filter_pages;
        
        if ($val == null && $actif == null && $nbpage == null){
            return redirect()->action('FournisseurController@ReadFournisseur');
        }
        else if($val == null){
            return redirect()->action('FournisseurController@RechercheFournisseurActif', ['actif'=>$actif, 'nbpage'=>$nbpage]);
        }else if ($actif == null)
        {
            $actif = 2;
        }
        
        return redirect()->action('FournisseurController@RechercheFournisseur_GET', ['val'=>$val,'actif'=>$actif, 'nbpage'=>$nbpage]);
    }

    
    public static function RechercheFournisseur_GET(Request $request, $val, $actif,$nbpage){
        if ($actif == 1)
        {
            $fournisseurs = Fournisseur::where([['nom_fournisseur', 'like', $val.'%'],['actif', '=', '1']])->paginate($nbpage);
        }else  if ($actif == 0){
            $fournisseurs = Fournisseur::where([['nom_fournisseur', 'like', $val.'%'],['actif', '=', '0']])->paginate($nbpage);
        }else{
            $fournisseurs = Fournisseur::where('nom_fournisseur', 'like', $val.'%')->paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        return view('fournisseur.index', compact('fournisseurs', 'searchInfo'));
    }

    
    public static function RechercheFournisseurActif(Request $request, $actif,$nbpage){
        if ($actif == 1)
        {
            $fournisseurs = Fournisseur::where('actif', '=', '1')->paginate($nbpage);
        }else  if ($actif == 0){
            $fournisseurs = Fournisseur::where('actif', '=', '0')->paginate($nbpage);
        }else{
           $fournisseurs = Fournisseur::paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        return view('fournisseur.index', compact('fournisseurs', 'searchInfo'));
    }





public function EditFourn(Request $request)
{
    foreach($request->nom_fournisseur as $id){
        Fournisseur::where('nom_fournisseur','=',$id)->update(['nom_fournisseur'=>$request->new_name]);
    }
    /*
    $unFournisseur = Fournisseur::find($request -> old_name);
    $unFournisseur ->nom_fournisseur = $request->nom_fournisseur;
    $unFournisseur ->save();

    return response() ->json();
    */
}

public function desactiverFournisseur(Request $request){
    foreach($request->nom_fournisseur as $id){
        Fournisseur::where('nom_fournisseur','=',$id)->update(['actif'=> 0]);
    }
}

public function activerFournisseur(Request $request){
    foreach($request->nom_categorie as $id){
        Fournisseur::where('nom_fournisseur','=',$id)->update(['actif'=>1]);
    }
}


public function ViewListFournisseur() {

    $unFournisseur = Fournisseur::All()->where('actif',1);
    //MODIFIER PLUS TARD
    $html = View::make('fournisseur.listeFournisseur', compact('unFournisseur'))->render();

    return Response::json(['html' => $html]);
}

public function RegisterFournisseur(Request $req)
{
    $validator = Validator::make($req->all(), [
        'nom_fournisseur'=> 'unique:fournisseur|required|max:255'
        ]);

        if ($validator->fails()) {
            $msg = "Fournisseur invalide.";
            return Response::json(['msg'=>$msg]);
        }

    $unFournisseur = New Fournisseur();
    $unFournisseur ->nom_fournisseur = $req->nom_fournisseur;
    $unFournisseur -> save();

    $fournisseurs = Fournisseur::All()->where('actif',1);

    $html = View::make('article._fournisseurs',compact('fournisseurs','unFournisseur'))->render();
    $msg = "";
    return Response::json(['html'=>$html, 'msg'=>$msg]);
}

public function RegisterFournisseur_FORM(Request $req)
    {
        $req->validate([
            'nom_fournisseur'=> 'unique:fournisseur|required|max:255'
        ]);
        $unFournisseur = New Fournisseur();
        $unFournisseur ->nom_fournisseur = $req->nom_fournisseur;
        $unFournisseur -> save();

        return back()->with('success', 'Fournisseur ajouté!');
    }

public function deleteFournisseur(Request $request)
{
    $unFournisseur = Fournisseur::find($request -> nom_fournisseur);
    $unFournisseur->actif = 0;
    $unFournisseur->save();

    return response()->json();
}

public function CreerF()
{
    return view('fournisseur.creerFournisseur');
}

public function ModificationFournisseur($id)
{
    $unFournisseur = Fournisseur::find($id);
     return view('fournisseur.editFournisseur',compact('unFournisseur'));

}

}


