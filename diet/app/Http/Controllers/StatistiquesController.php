<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use App\Models\V_ArticlesPopulaires;
use App\Models\V_TendanceCommandes;
use App\Models\V_ArticlesFournisseurs;
use App\Models\V_ArticlesMarques;
use App\Models\V_ArticlesCategories;
use App\Models\V_Utilisateurs;
use App\Models\V_Administrateurs;
use App\Models\V_NbClientActif;
use App\Models\Client;
use App\Http\Controllers;

class StatistiquesController extends Controller
{
	public static $palette = [
		'#328bb6',
		'#f4b747',
		'#c93874',
		'#e8554a',
		'#545ea6',
		'#ef926c',
		'#92c260',
		'#0ca032',
		'#b9ce1b'
	];

	public static function PrepareColors($data) {

		$colors = [];
        for ($i = 0; $i < count($data); $i++) {
        	array_push($colors, StatistiquesController::$palette[$i % count(StatistiquesController::$palette)]);
        }

        return $colors;
	}

	public static function CreateBarChart($name, $labels, $data, $showLabels) {
        $chartjs = app()->chartjs
            ->name($name)
            ->type('bar')
            ->size(['width' => 400, 'height' => 400])
            ->labels($labels)
            ->datasets([
                [
                    "label" => "Nombre de ventes",
                    'backgroundColor' => StatistiquesController::PrepareColors($data),
                    'data' => $data
                ]
            ])
            ->optionsRaw("{
				responsive:true,
				scaleShowValues: true,
				scales: {yAxes: [{ticks: {beginAtZero: true}}],
				xAxes: [{ticks: {autoSkip: false, display: " . ($showLabels ? "true" : "false") . "}}]
			}}");

        return $chartjs;
	}

	public static function CreateLineChart($name, $labels, $data) {

		$chartjs = app()->chartjs
	        ->name($name)
	        ->type('line')
	        ->size(['width' => 400, 'height' => 400])
	        ->labels($labels)
	        ->datasets([
	            [
	                "label" => "Tendance des ventes",
	                'backgroundColor' => StatistiquesController::$palette[6],
	                'data' => $data
	            ]
	        ])
	        ->options([]);

	    return $chartjs;
	}

	public static function CreatePieChart($name, $labels, $data, $doughnut, $legend) {

		$chartjs = app()->chartjs
	        ->name($name)
	        ->type($doughnut ? 'doughnut' : 'pie')
	        ->size(['width' => 400, 'height' => 400])
	        ->labels($labels)
	        ->datasets([
	            [
	                'backgroundColor' => StatistiquesController::PrepareColors($data),
	                'data' => $data
	            ]
	        ])
	        ->optionsRaw("{legend: {display:" . $legend . "}}");

	    return $chartjs;
	}

    public static function ArticlesPopulairesPartial($month, $year, $show_title) {
		($show_title) ? $title = "Top 10 des ".Controllers\PersonnalisationController::GetSellerName(true, false)." populaires par ventes pour " . $month . "/" . $year
					  : $title = "";
        
        $data = V_ArticlesPopulaires::All()->where('dataset_year', '=', $year)->where('dataset_month', '=', $month);
        $chartjs = StatistiquesController::CreateBarChart(
        	'popularProducts',
        	$data->pluck('label')->toArray(),
			$data->pluck('data')->toArray(),
			false
        );

        return View::make('statistiques._chart', compact('title', 'chartjs'))->render();
    }

	public static function TendanceCommandesPartial($year, $show_title) {
		($show_title) ? $title = "Tendance de commandes par mois en " . $year
					  : $title = "";

        $labels = array_reduce(range(1, 12), function($month, $m) { 
			setlocale(LC_ALL, 'fr_FR.UTF8');
        	$month[$m - 1] = strftime('%B', mktime(0, 0, 0, $m, 10)); 
        	return $month; 
        });
        $data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach (V_TendanceCommandes::All()->where('dataset_year', '=', $year) as &$commande) {
        	$data[$commande->dataset_month - 1] = $commande->data;
        }
        $chartjs = StatistiquesController::CreateLineChart(
        	'orderTendency',
        	$labels, 
        	$data
        );

        return View::make('statistiques._chart', compact('title', 'chartjs'))->render();
    }

    public static function ArticlesFournisseursPartial() {

        $title = Controllers\PersonnalisationController::GetSellerName(true, true)." par fournisseurs";
        $data = V_ArticlesFournisseurs::All();
        $chartjs = StatistiquesController::CreatePieChart(
        	'productsPerSeller',
  	      	$data->pluck('label')->toArray(),
  	      	$data->pluck('data')->toArray(),
  	      	1, 1
        );

        return View::make('statistiques._chart', compact('title', 'chartjs'))->render();
    }

    public static function ArticlesMarquesPartial() {

        $title = Controllers\PersonnalisationController::GetSellerName(true, true)." par marques";
        $data = V_ArticlesMarques::All();
        $chartjs = StatistiquesController::CreatePieChart(
        	'productsPerBrand',
  	      	$data->pluck('label')->toArray(),
  	      	$data->pluck('data')->toArray(),
  	      	1, 1
        );

        return View::make('statistiques._chart', compact('title', 'chartjs'))->render();
    }

    public static function ArticlesCategoriesPartial() {

        $title = Controllers\PersonnalisationController::GetSellerName(true, true)." par catégories";
        $data = V_ArticlesCategories::All();
        $chartjs = StatistiquesController::CreatePieChart(
        	'productsPerCategories',
  	      	$data->pluck('label')->toArray(),
  	      	$data->pluck('data')->toArray(),
  	      	1, 1
        );

        return View::make('statistiques._chart', compact('title', 'chartjs'))->render();
    }

    public static function UtilisateursPartial() {

        $title = "Utilisateurs";
        $data = V_Utilisateurs::All();
        $chartjs = StatistiquesController::CreatePieChart(
        	'users',
  	      	$data->pluck('label')->toArray(),
  	      	$data->pluck('data')->toArray(),
  	      	0, 1
        );

        return View::make('statistiques._chart', compact('title', 'chartjs'))->render();
    }

	public static function AdministrateursPartial() {

        $title = "Administrateurs";
        $data = V_Administrateurs::All();
        $chartjs = StatistiquesController::CreatePieChart(
        	'administrators',
  	      	$data->pluck('label')->toArray(),
  	      	$data->pluck('data')->toArray(),
  	      	0, 1
        );

        return View::make('statistiques._chart', compact('title', 'chartjs'))->render();
    }

	public function Index(Request $req) {

        $month = (int)($req->filtre == null ? date("m") : explode("/", $req->filtre)[0]);
        $year = (int)($req->filtre == null ? date("Y") : explode("/", $req->filtre)[1]);

        return view('statistiques.index', compact('month', 'year'));
	}
	

	public static function ClientActifMois($year, $month){
		$nb = V_NbClientActif::where('dataset_year', $year)->where('dataset_month', $month)->get();

		if (count($nb) > 0){
			return $nb[0]->nb; 
		}else return 0;
	}

	public static function NbClients(){
		$nb = Client::count();
		return $nb; 
	}
}