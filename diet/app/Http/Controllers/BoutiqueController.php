<?php

namespace App\Http\Controllers;

use DB;
use View;
use Illuminate\Support\Facades\Input;
use App\Models\Article;
use Illuminate\Http\Request;

class BoutiqueController extends Controller
{
    public function Index(Request $req) {
        
        $partial = $this->IndexPartial($req);

        if ($req->ajax()) {
            return $partial;
        }

        return view('boutique.index', compact('partial', 'req'));
    }

    /**
     * Partial view of the main view (List of products containing different filters).
     */
    public function IndexPartial(Request $req) {

        $articles = Article::where(function ($query) use(&$req, &$admin) {

            // Apply search filter.
            $query->where('nom', 'like', '%'.$req->filter_search.'%')->where('actif', 1)->where('actif_vente', 1);

            // Apply category filter.
            if ($req->filter_category) $query->where('nom_categorie', $req->filter_category);

        })->when($req->filter_price, function ($query, $sort) {

            // Apply sort filter.
            if ($sort == 0) $query->orderBy('nom', 'asc');
            if ($sort == 1) $query->orderBy('prix', 'asc');
            if ($sort == 2) $query->orderBy('prix', 'desc');

        });
        $articlesCount = $articles->count();
        $articles = $articles->paginate(12);
        $articles->appends(Input::except('page'));

        return View::make('boutique._boutique', compact('articles', 'articlesCount'))->render();
    }

    public static function Read($category) {

        $articles = Article::orderBy('nom', 'asc')->get()->where('actif', 1)->where('actif_vente', 1);

        return ($category != 'Tous les articles' && $category != 'Toutes les catégories') ? $articles->where('nom_categorie', $category) : $articles;
    }

    public static function ReadPriceAsc($category) {

        $articles = Article::orderBy('prix', 'asc')->orderby('nom', 'asc')->get()->where('actif', 1)->where('actif_vente', 1);

        return $category != 'Tous les articles' ? $articles->where('nom_categorie', $category) : $articles;
    }

    public static function ReadPriceDesc($category) {

        $articles = Article::orderBy('prix', 'desc')->orderby('nom', 'asc')->get()->where('actif', 1)->where('actif_vente', 1);

        return $category != 'Tous les articles' ? $articles->where('nom_categorie', $category) : $articles;
    }

    public static function ReadRandom($category) {
        
        $articles = Article::orderByRaw('RAND()')->get()->where('actif', 1)->where('actif_vente', 1)->where('nom_categorie', $category)->take(3);

        return $articles;
    }

    public static function ReadRandomAccueil() {
        
        $articles = Article::orderByRaw('RAND()')->get()->where('actif', 1)->where('actif_vente', 1)->take(3);

        return $articles;
    }

    public function View_GET($rowid) {

        $article = Article::find($rowid);

        return view('boutique.article', compact('article'));
    }
}
?>