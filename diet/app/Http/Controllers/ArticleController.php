<?php

namespace App\Http\Controllers;

use DB;
use View;
use Validator;
use App\Models\PeriodeArticle;
use App\Rules\GreaterThan;
use App\Models\Article;
use App\Models\Recette;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use finfo;


class ArticleController extends Controller
{
    /**
     * Read method returns a json array containing every article in the database.
     * Can be used for ViewModels or for foreign key lookup.
     */
    public static function Read() {
        
        $articles = Article::all()->where('actif', 1);

        return $articles;
    }

    /**
     * ReadLast method returns a json array containing the last number requested article in the database.
     * Can be used for ViewModels or for foreign key lookup.
     */
    public static function ReadLast($number) {
        
        $articles = Article::orderBy('rowid', 'desc')->where('actif', 1)->take($number)->get();

        return $articles;
    }

    /**
     * Renvois toutes les recettes d'un article en json.
     * 
     */
    public static function ReadRecette($rowid) {
        
        $recettes = Recette::where('article_rowid', $rowid)->get();

        return $recettes;
    }

    public static function ReadPriceFee($article, $qte) {
        
        return truncate_number(((($article->prix * (($article->frais_fixe / 100.0))) + $article->frais_variable + $article->frais_emballage) *$qte), 2);
    }
    
    public static function ReadPricePlus($article) {

        return truncate_number((($article->prix * (($article->frais_fixe / 100.0) + 1.0)) + $article->frais_variable + $article->frais_emballage), 2);
    }

    /**
     * Get the current progress of a product.
     */
    public static function ReadProgress($article) {

        $periode = Controllers\PeriodeController::GetActivePeriod();
        $articleQuantite = 0;
        $available = $article->quantite_maximum;

        if ($periode != NULL){

            $periodeArticle = $article->periode_articles->where('article_rowid', $article->rowid)->where('periode_rowid', $periode->rowid)->first();

            if ($periodeArticle != NULL) {
                $articleQuantite = $periodeArticle->quantite_commande;
                $available = $available - $articleQuantite;
            }
        }
        $percentage = floor($articleQuantite / $article->quantite_minimum * 100);

        return compact('percentage', 'available');
    }

    /**
     * Main view.
     */
    public function Index(Request $req) {
        
        $partial = $this->IndexPartial($req);

        if ($req->ajax()) {
            return $partial;
        }

        return view('article.index', compact('partial', 'req'));
    }

    /**
     * Partial view of the main view (List of products containing different filters).
     */
    public function IndexPartial(Request $req) {

        $admin = session('admin');
        $articles = Article::where(function ($query) use(&$req, &$admin) {

            // Apply admin filter based on the session variable.
            if (!$admin->super && !$admin->master) $query->where('created_by', $admin->username);

            // Apply search filter.
            $query->where('nom', 'like', '%'.$req->filter_search.'%');

            // Apply category filter.
            if ($req->filter_category) $query->where('nom_categorie', $req->filter_category);

            // Apply brand filter.
            if ($req->filter_brand) $query->where('nom_marque', $req->filter_brand);

            // Apply supplier filter.
            if ($req->filter_supplier) $query->where('nom_fournisseur', $req->filter_supplier);

            // Apply active filter.
            if ($req->filter_active) $query->where('actif', $req->filter_active == 1 ? '1' : '0');

        })->when($req->sort_name, function ($query, $sort) {

            // Apply sort filter.
            if ($sort == 1) $query->orderBy('nom', 'asc');
            if ($sort == 2) $query->orderBy('nom', 'desc');

        })->when($req->sort_price, function ($query, $sort) {

            // Apply sort filter.
            if ($sort == 1) $query->orderBy('prix', 'asc');
            if ($sort == 2) $query->orderBy('prix', 'desc');

        })->paginate($req->filter_pages ? $req->filter_pages : 10);
        $articles->appends(Input::except('page'));

        return View::make('article._list', compact('articles'))->render();
    }


    /**
     * Parse a request object in order to convert it into an article instance to be used with
     * the database.
     */
    public function ParseRequest(Request $req, $rowid = 0) {

        $article = ($rowid == 0 ? new Article : Article::find($req->rowid));
        $article->nom = $req->nom;
        $article->format_quantite = $req->format_quantite;
        $article->format = $req->format;
        $article->provenance = $req->provenance;
        $article->allergene = $req->allergene;
        $article->perissable = $req->perissable;
        $article->bio = $req->bio;
        $article->quebec = $req->quebec;
        $article->nom_marque = $req->nom_marque;
        $article->nom_categorie = $req->nom_categorie;
        $article->nom_fournisseur = $req->nom_fournisseur;
        $article->quantite_minimum = $req->quantite_minimum;
        $article->quantite_maximum = $req->quantite_maximum;
        $article->prix = $req->prix;
        $article->frais_fixe = $req->frais_fixe;
        $article->frais_variable = $req->frais_variable;
        $article->frais_emballage = $req->frais_emballage;
        $article->description = $req->description;
        $article->remarque = $req->remarque;
        $article->provenance = $req->provenance;
        $article->created_by = session('admin')->username;

        if ($req->hasFile('image')) {
            $article->image = $req->file('image')->store('public/products/images');
        } else {
            if ($req->image_dirty) $article->image = NULL;
        }

        if ($req->hasFile('valeur_nutritive')) {
            $article->valeur_nutritive = $req->file('valeur_nutritive')->store('public/products/nutritional');
        } else {
            if ($req->valeur_nutritive_dirty) $article->valeur_nutritive = NULL;
        }

        if ($req->hasFile('piece_jointe')) {
            $article->piece_jointe = $req->file('piece_jointe')->store('public/products/attachments');
        } else {
            if ($req->piece_jointe_dirty) $article->piece_jointe = NULL;
        }

        return $article;
    }

    /**
     * Parse a request object in order to convert it into multiple instance directly inserted 
     * into the database.
     */
    public function ParseRequestRecette(Request $req, $article_rowid) {
        Recette::where('article_rowid', $article_rowid)->delete();
        if ($req->nom_recette != null){
            foreach($req->nom_recette as $key => $val)
            {
                $recette = new recette();
                $recette->article_rowid = $article_rowid;
                $recette->name = $val;
                $recette->link = $req->url_recette[$key];
                $recette->save();
            }
        }
    }

    /**
     * Validator for the article object.
     */
    public function ValidateRequest(Request $req) {

        $validator = Validator::make($req->all(), [
            'nom' => 'required|max:255',
            'format_quantite' => 'required',
            'provenance' => 'max:255',
            'allergene' => 'max:255',
            'nom_marque' => 'required',
            'nom_categorie' => 'required',
            'nom_fournisseur' => 'required',
            'quantite_minimum' => 'required|max:3',
            'quantite_maximum' => ['required', new GreaterThan($req->quantite_minimum), 'max:3'],
            'prix' => 'required',
            'frais_fixe' => ['required', new GreaterThan(0), 'max:4'],
            'frais_variable' => ['required', new GreaterThan(0), 'max:4'],
            'frais_emballage' => ['required', new GreaterThan(0), 'max:4'],
            'description' => 'max:2048',
            'remarque' => 'max:2048',
            'image' => 'mimes:jpeg,jpg,png,gif|max:2048',
            'valeur_nutritive' => 'mimes:jpeg,jpg,png,gif|max:2048',
            'piece_jointe' => 'mimes:pdf|max:2048',
            'nom_recette.*' => 'required|max:255',
            'url_recette.*' => 'required|url|max:1024',
        ]);

        return $validator;
    }


    /**
     * Redirect to the create page for an article.
     */
    public function Create_GET() {

        $article = new Article;

        return view('article.create', compact('article'));
    }

    /**
     * Add a new article in the database.
     */
    public function Create_POST(Request $req) {

        $validator = $this->ValidateRequest($req);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $article = $this->ParseRequest($req);
        $article->actif = 1;
        $article->save();

        $this->ParseRequestRecette($req, $article->rowid);

        return redirect()->action('ArticleController@Index');
    }

    /**
     * Edit an article by supplying its primary key. Redirects to the edit view
     * after retrieving the model from the database.
     */
    public function Edit_GET($rowid) {

        $article = Article::find($rowid);

        return view('article.edit', compact('article'));
    }

    /**
     * Save the edit of an article. Used for form submission.
     */
    public function Edit_POST(Request $req) {

        $validator = $this->ValidateRequest($req);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $article = $this->ParseRequest($req, $req->rowid);
        $article->save();

        $this->ParseRequestRecette($req, $article->rowid);

        return redirect()->action('ArticleController@Index');
    }

    /**
     * Deactivate a specific article in the database. Foreign key relations will remain but
     * the article won't show up again until reactivation.
     * This method should be called in AJAX.
     */
    public function Delete_POST(Request $req) {

        $article = Article::find($req->rowid);
        $article->actif_vente = 0;
        $article->actif = 0;
        $article->save();

        return response()->json();
    }

    /**
     * Reactivate an article.
     * This method should be called in AJAX.
     */
    public function Activate_POST(Request $req) {

        $article = Article::find($req->rowid);
        $article->actif = 1;
        $article->save();

        return response()->json();
    }

    /**
     * Approve an article in order to include it on the website.
     */
    public function Approve_POST(Request $req) {
        
        $article = Article::find($req->rowid);
        $article->actif_vente = 1;
        $article->save();

        return response()->json();
    }

    public function DownloadPieceJointe($rowid){
        $file = Article::where('rowid', $rowid)->first();

        return Storage::download($file->piece_jointe);
    }
}
?>