<?php


namespace App\Http\Controllers;
use App\Models\Categorie;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use View;
use Response;


class CategorieController extends Controller
{
    public static function Read() {
        
        $categories = Categorie::All()->where('actif',1);

        return $categories;
    }

    public function Index()
    {
        $categories = Categorie::paginate(15);

        $searchInfo = [2, 15];

        return view('categorie.index',compact('categories', 'searchInfo'));
    }

     public static function RechercheCategorie_POST(Request $request){
        $val = $request->search;
        $actif = $request->actif;
        $nbpage = $request->filter_pages;
        
        if ($val == null && $actif == null && $nbpage == null){
            return redirect()->action('CategorieController@Index');
        }
        else if($val == null){
            return redirect()->action('CategorieController@RechercheCategorieActif', ['actif'=>$actif, 'nbpage'=>$nbpage]);
        }else if ($actif == null)
        {
            $actif = 2;
        }
        
        return redirect()->action('CategorieController@RechercheCategorie_GET', ['val'=>$val,'actif'=>$actif, 'nbpage'=>$nbpage]);
    }

    
    public static function RechercheCategorie_GET(Request $request, $val, $actif,$nbpage){
        if ($actif == 1)
        {
            $categories = Categorie::where([['nom_categorie', 'like', $val.'%'],['actif', '=', '1']])->paginate($nbpage);
        }else  if ($actif == 0){
            $categories = Categorie::where([['nom_categorie', 'like', $val.'%'],['actif', '=', '0']])->paginate($nbpage);
        }else{
            $categories = Categorie::where('nom_categorie', 'like', $val.'%')->paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        return view('categorie.index', compact('categories', 'searchInfo'));
    }

    
    public static function RechercheCategorieActif(Request $request, $actif,$nbpage){
        if ($actif == 1)
        {
            $categories = Categorie::where('actif', '=', '1')->paginate($nbpage);
        }else  if ($actif == 0){
            $categories = Categorie::where('actif', '=', '0')->paginate($nbpage);
        }else{
           $categories = Categorie::paginate($nbpage);
        }

        $searchInfo = [$actif, $nbpage];

        return view('categorie.index', compact('categories', 'searchInfo'));
    }



    public function EditCategorie(Request $request)
    {
        foreach($request->nom_categorie as $id){
            Categorie::where('nom_categorie','=',$id)->update(['nom_categorie'=>$request->new_name]);
        }
        /*
        $uneCategorie = Categorie::find($request -> old_name);
        $uneCategorie ->nom_categorie = $request->nom_categorie;
        $uneCategorie ->save();

        return response() ->json();
    */
    }

    public function desactiverCategorie(Request $request){
        foreach($request->nom_categorie as $id){
            Categorie::where('nom_categorie','=',$id)->update(['actif'=> 0]);
        }
    }

    public function activerCategorie(Request $request){
        foreach($request->nom_categorie as $id){
            Categorie::where('nom_categorie','=',$id)->update(['actif'=>1]);
        }
    }


    public function ViewListCategorie() {

        $uneCategorie = Categorie::All()->where('actif',1);
        //MODIFIER PLUS TARD
        $html = View::make('categorie.listeCategorie', compact('uneCategorie'))->render();

        return Response::json(['html' => $html]);
    }

    public function RegisterCategorie(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'nom_categorie'=> 'unique:categorie|required|max:255'
            ]);

            if ($validator->fails()) {
                $msg = "Catégorie invalide.";
                return Response::json(['msg'=>$msg]);
            }

        $uneCategorie = New Categorie();
        $uneCategorie ->nom_categorie = $req->nom_categorie;
        $uneCategorie -> save();

        $categories = Categorie::All()->where('actif',1);
        $msg = "";
        $html = View::make('article._categories',compact('categories','uneCategorie'))->render();

        return Response::json(['html'=>$html, 'msg'=>$msg]);
    }

     public function RegisterCategorie_FORM(Request $req)
    {
        $req->validate([
            'nom_categorie'=> 'unique:categorie|required|max:255'
        ]);

        $uneCategorie = New Categorie();
        $uneCategorie ->nom_categorie = $req->nom_categorie;
        $uneCategorie -> save();

        return back()->with('success', 'Catégorie ajoutée!');
    }

    public function deleteCategorie(Request $request)
    {
        $uneCategorie = Categorie::find($request -> nom_categorie);
        $uneCategorie->actif = 0;
        $uneCategorie->save();

        return response()->json();
    }

    public function CreerC()
    {
        return view('categorie.creerCategorie');
    }

    public function ModificationCategorie($id)
    {
        $uneCategorie = Categorie::find($id);
        return view('categorie.editCategorie',compact('uneCategorie'));

    }

}


