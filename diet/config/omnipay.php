<?php

return [
    'credentials' => [
        'clientId' => env('PAYPAL_CLIENT_ID'),
        'secret' => env('PAYPAL_SECRET'),
        'testMode' => env('PAYPAL_SANDBOX', false)
    ],
];

?>