
create or replace view v_stat_nb_client_actif as
select distinct count(client.rowid) as nb, year(date_commande) as dataset_year, month(date_commande) as dataset_month from client, commande where client.rowid=commande.rowid_client and commande.paye =1;