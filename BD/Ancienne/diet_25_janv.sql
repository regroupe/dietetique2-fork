-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 25, 2019 at 01:48 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diet`
--
CREATE DATABASE IF NOT EXISTS `diet` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `diet`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `super` tinyint(1) NOT NULL,
  `master` tinyint(1) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_modif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `change_pass` tinyint(1) NOT NULL DEFAULT '0',
  `actif` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`super`, `master`, `username`, `password`, `last_modif`, `change_pass`, `actif`) VALUES
(0, 0, 'admin', '$2y$10$qejIbdueBlAOk9Fxger.su33bTctWlR6CKcqZ1iOnz6/GCZvdFQfO', 'master', 0, 1),
(1, 1, 'master', '$2y$10$al.OCHpRFjQKFrkosV4vXuYv3uQnwi.Tyl8pYoKFtdcBUlt.gPxW.', '', 0, 1),
(1, 0, 'super', '$2y$10$GuKNlmDXvss6HWnDS0PCxut.TaD33w7Xzi8BO71KbCB9Q794HzI/m', 'master', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite_minimum` int(11) DEFAULT NULL,
  `quantite_maximum` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL,
  `description` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarque` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valeur_nutritive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `piece_jointe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '0',
  `actif_vente` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rowid`),
  KEY `article_fk0` (`nom_marque`),
  KEY `article_fk1` (`nom_categorie`),
  KEY `article_fk2` (`nom_fournisseur`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`rowid`, `image`, `nom`, `format_quantite`, `format`, `provenance`, `allergene`, `perissable`, `bio`, `quebec`, `nom_marque`, `nom_categorie`, `nom_fournisseur`, `quantite_minimum`, `quantite_maximum`, `prix`, `frais_fixe`, `frais_variable`, `prix_majore`, `economie`, `description`, `remarque`, `valeur_nutritive`, `piece_jointe`, `created_by`, `actif`, `actif_vente`) VALUES
(1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.95', '5.00', '0.00', NULL, NULL, 'Excellente pseudo-céréale naturellement sans gluten!', NULL, 'public/products/nutritional/GSSLKaSc5PSg4gmaRIdSnK3FSTehvSWdeJs5MkIE.png', NULL, 'master', 1, 1),
(2, 'public/products/images/aKxliPxIIy0PREv07R4UHxQsM6vQ5wgNI8O4V26t.jpeg', 'Couscous de blé entier tamisé', '1.00', 'kg', 'Canada', NULL, 0, 1, 0, 'Gerble', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.91', '5.00', '0.00', NULL, NULL, 'Pour faire d’excellents taboulés… Yé!', NULL, 'public/products/nutritional/MthROLiRbthvhEaQKDR4s1qPV5CDrQ7Itf2rALBG.png', NULL, 'master', 1, 1),
(3, 'public/products/images/N0AvyopsFGqYKEbFV5t1Pso4YSV20M190CNfcO40.jpeg', 'Flocons d’avoine', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Aliments céréaliers', 'Noix Quebec', 80, 100, '3.43', '5.00', '0.50', NULL, NULL, 'Cette avoine est roulée et torréfiée à sec avant d’être chauffée à la vapeur à 200°F pour être roulée. C’est ce qui distingue ces flocons de ceux des autres rares entreprises qui en font au Québec, et c’est ce qui leur confère ce goût de gras naturel de l’avoine.C’est ce qui leur permet également de hausser sa durée de conservation à un an sans problème.', NULL, 'public/products/nutritional/8I7DFLv4nW6zmVwVkWAUkyr5gtejjhzzY6SgMYlH.png', NULL, 'master', 1, 1),
(4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 50, 50, '19.92', '6.00', '1.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes, dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température. Pour les desserts cuits comme les gâteaux ou les brownies, nous recommandons plutôt le cacao en poudre; le goût du cacao cru serait trop prononcé pour de tels desserts.', NULL, 'public/products/nutritional/p7PKD2k1iGJ1CLL5igoP8Sv2GyxvFHIJTdZPFBKP.png', NULL, 'master', 1, 1),
(5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 40, 100, '14.88', '5.00', '0.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao en poudre pour les desserts cuits comme les gâteaux ou les brownies, pour lesquels le goût du cacao cru serait trop prononcé. Dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température, nous recommandons plutôt le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes.', NULL, 'public/products/nutritional/SIljBYRTECmTu0cARK3YJQGdaiwOMKzmGB3HEH78.png', NULL, 'master', 1, 1),
(6, 'public/products/images/fxtpyETXUMXk2OswDJ3T1SparCXp9c4caKKNOa8J.jpeg', 'Capuchons de chocolat 70%', '500.00', 'g', 'Fèves du Pérou, chocolat produit en Europe', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 150, 200, '23.93', '7.00', '0.00', NULL, NULL, 'Pépites de chocolat géantes, pour cuisiner ou manger telles quelles comme délice du moment. Environ 5 fois la grosseur des pépites de chocolat, elles font d’excellents desserts avec de gros morceaux de chocolat qui en mettent plein la dent!', NULL, 'public/products/nutritional/Xx8RvLJaxYXeNYale45l5G4WCbvOKrv54PdIUmiG.png', NULL, 'master', 1, 1),
(7, 'public/products/images/TmxisxJI9ocq2Ip8TQriNaer4WypYgn9Enuy9HBb.jpeg', 'Café La Montréalaise Santropol', '500.00', 'g', 'Amérique centrale et du sud', NULL, 0, 1, 1, 'Materne', 'Café', 'Grossistes en café Quebec', 65, 125, '14.37', '5.00', '2.00', NULL, NULL, 'Café gourmet biologique, équitable, en grain et de torréfaction française. Un riche mélange corsé aux reflets de marron et d’ébène.', NULL, 'public/products/nutritional/stjSP5cjaLxUFLYOdMhmaB2M6t1cy6gsZGsdzdJh.png', NULL, 'master', 1, 1),
(8, 'public/products/images/yepAc3CrDZsNuFSxHG4h1hFbgYuqwsq7lI72nlSV.jpeg', 'Levure alimentaire en flocons', '500.00', 'g', 'États-Unis', NULL, 0, 0, 0, 'Ayam', 'Condiments et autres', 'Farine Quebec', 80, 120, '13.89', '5.00', '1.00', NULL, NULL, 'Levure produite par fermentation naturelle, aucune source animale ni synthétique.', NULL, 'public/products/nutritional/oMVZlewVITN6Jx7GfMeiFzOUOr9hQe4YXg5vt0o4.png', NULL, 'master', 1, 1),
(9, 'public/products/images/7AzHZCs5UGaVzjEJETAw9z1XquORt6uMNrRyF99U.jpeg', 'Miso soya et riz sans gluten', '500.00', 'g', 'Québec', NULL, 1, 1, 1, 'Bledina', 'Condiments et autres', 'Confitures et gelées Quebec', 100, 100, '13.83', '5.00', '0.00', NULL, NULL, 'Le miso biologique Massawipi non pasteurisé de soya et de riz est un aliment sans gluten très polyvalent; son goût est doux, rond en bouche et légèrement salé. Il est le résultat d’une fermentation naturelle (non forcée) d’au moins 2 ans. Pour sa fabrication, nous n’utilisons que des ingrédients certifiés biologiques garantis sans OGM.', NULL, 'public/products/nutritional/Ty12FzodDT1kbWEeKsEnqlXZwim1KggiKytU5P9l.png', NULL, 'master', 1, 1),
(10, 'public/products/images/I6XjF0Hwgg9sIX0tqhw7rrjiacSVk3oHcqy0DeXV.jpeg', 'Moutarde de Dijon', '1.00', 'kg', 'Québec', NULL, 1, 1, 1, 'Herta', 'Condiments et autres', 'Produits alimentaires Quebec', 20, 20, '8.98', '5.00', '0.00', NULL, NULL, 'Moutarde crue, sans sucre ajouté, sans agents de conservation, sans gluten. Une moutarde de Dijon avec une belle texture crémeuse et une touche de piquant qui la rend irremplaçable! Pour les amateurs de moutarde forte!', NULL, NULL, NULL, 'master', 1, 1),
(11, 'public/products/images/uwhju93QU1tzQNJO5xXD9iu1JDSzw7w00tRcRFEe.jpeg', 'Farine blanche tout-usage non blanchie', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Sacla', 'Farines', 'Farine Quebec', 60, 70, '2.88', '5.00', '0.00', NULL, NULL, 'La farine blanche tout usage non blanchie est tamisée pour en retirer tout le son et ne subit aucun traitement de blanchiment. Elle est produite à partir de blé dur de printemps biologique, et les grains sont moulus sur cylindres. Idéal pour la boulangerie et la pâtisserie.', NULL, 'public/products/nutritional/6cCibCA8qr7SRrQYZCsZimR5Ly3GKJUWB1d9bm9o.png', NULL, 'master', 1, 1),
(12, 'public/products/images/n5nqMXBrO1Lx3XhZ1l8ZCpQA5D7g08p9WHQq9tuB.jpeg', 'Farine d’épeautre', '200.00', 'g', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Farines', 'Farine Quebec', 25, 30, '4.60', '5.00', '0.00', NULL, NULL, 'Depuis mai 2018, cette farine est tamisée de sorte à en extraire 10 % du son, ce qui lui procure un meilleur rendement en pâtisserie et en boulangerie qu’une farine intégrale.', NULL, NULL, NULL, 'master', 1, 1),
(13, 'public/products/images/1TpWugpuoiLsRiiFu3ToTpzfZcBhz8dRDzXDL5UL.jpeg', 'Farine de blé intégrale', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Fleury-michon', 'Farines', 'Farine Quebec', 80, 80, '2.88', '3.50', '0.00', NULL, NULL, 'Cette farine de blé intégrale contient toutes la parties du blé. Le son et les rémoulures donnent plus de texture à vos pâtisseries et à vos pains.', NULL, 'public/products/nutritional/PSd1hLFB885sjyTw5jfEZAOjZTPm1zGah4LvzJ8x.jpeg', NULL, 'master', 1, 1),
(14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 20, 40, '13.94', '5.00', '1.00', NULL, NULL, NULL, 'Il est possible de retrouver des noyaux dans le produit.', 'public/products/nutritional/0K5UCVCywUYqmbWHv79g8rJiwUQzyEKXh5BX6e09.png', NULL, 'master', 1, 1),
(15, 'public/products/images/17YmeaiQPt8j48kzvCWmxRqXAzBKtDD3r7r0TetF.jpeg', 'Canneberges séchées au jus de pommes', '1.00', 'kg', 'Québec', NULL, 1, 1, 0, 'Citadelle', 'Fruits séchés', 'Produits biologiques Quebec', 175, 200, '18.66', '5.00', '0.00', NULL, NULL, 'Ces canneberges séchées sont préparées à partir de canneberges (Vaccinium macrocarpon) matures de première qualité et infusées dans une solution de jus de pomme concentré. Elles sont tendres et juteuses et présentent le goût acidulé et légèrement sucré caractéristique de la canneberge. En faisant sécher les fruits plus longtemps et à plus basse température, Citadelle parvient à préserver la saveur des canneberges ainsi qu’une quantité maximale de nutriments, pour le bonheur de nos palais et la santé de notre corps.', 'Le tout est 100% naturel, tant le procédé de conservation que les canneberges elles-mêmes auxquelles aucun agent stabilisant, ni colorant, ni glycérine n’est ajouté.', NULL, NULL, 'master', 1, 1),
(16, 'public/products/images/kPZuCChkJhrxRQ0YOhKAqSt6MaWVfGeLjc5eqwFD.jpeg', 'Dattes Deglet dénoyautées', '1.00', 'kg', 'Tunisie', NULL, 1, 1, 0, 'Prana', 'Fruits séchés', 'Produits biologiques Quebec', 80, 80, '12.77', '5.00', '0.00', NULL, NULL, 'Youpi! Savoureuse et sans noyau, bien que, étant donné que le processus de dénoyautage est automatisé, il est possible de retrouver quelques noyaux parmi ces dattes.', NULL, 'public/products/nutritional/qQDAsd2DoU6n1C0ABNUWrrR14Qz0yj6xvi2IWj9W.png', NULL, 'master', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_categorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`nom_categorie`, `actif`) VALUES
('Aliments céréaliers', 1),
('Cacao et ses acolytes', 1),
('Café', 1),
('Condiments et autres', 1),
('Farines', 1),
('Fruits séchés', 1),
('Graines à germer', 1),
('Huiles et vinaigres', 1),
('Légumineuses', 1),
('Livres', 1),
('Noix et graines', 1),
('Pâtes', 1),
('Produits québécois', 1),
('Sacs écolo', 1),
('Superaliments', 1);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`rowid`, `email`, `password`, `telephone`, `poste`, `nom`, `prenom`, `adresse`, `no_app`, `code_postal`, `ville`, `pays`, `province`, `actif`) VALUES
(1, 'jessy@cegep.com', '$2y$10$BkAGbBom8y9bQz8LeD9L3eRIAKDhQsOsifdr114hbWIJyLEVVF8VC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(2, 'zach@cegep.com', '$2y$10$C3Kao9./HOM/HDABXssW0OS9d0cPDXOGdTb6M5TIl9czDfULJsmNW', 'eyJpdiI6Ik9WRWd1a1drQk1EYVBXclJoUXVLMUE9PSIsInZhbHVlIjoia280MTJBN2lcLzZvQjVod2tsRG96QUtndW1HY2poTFBLb09Lb3RJMytQTGM9IiwibWFjIjoiNDU1M2JjMzllNTU2NzNhYzc4N2M0YmUyOTUzNjdkYzY1OTEzZTdkMTQ1OTI2MzhmZGE0NjgyOTZjMzY2NDhhNCJ9', 'eyJpdiI6Im1oOUJhR29HcW9Sa2x1K01ETzYrUnc9PSIsInZhbHVlIjoiTzFBU3MxdUM1anAwWllHbjdoNmZqdz09IiwibWFjIjoiOTgyOTM3NWY4YjY2YTNkNGUxYzJjY2U2OGYzMmU0MjQ3MzQ0MDNmYWQ1YzEyMzEwMjIwYTYyZjI4ZTQyMzI1NiJ9', 'eyJpdiI6IjZ6aWloTkc4VGRXMWJ0XC90bGZYc1pnPT0iLCJ2YWx1ZSI6IkpKXC84dStiSHdtYkJhTjZaQ3NGdzBRPT0iLCJtYWMiOiI5NTIwMDYwM2RhNjFjMzc2MjgzMTE3ZGVkOWY5MDY3OWVlYjcxNTFiMGEzYzAwM2NhZDg3YjlmOGY1YmIxOGE0In0=', 'eyJpdiI6ImtHUE5DWktZUzRiNnBrSjdnb3FTXC9nPT0iLCJ2YWx1ZSI6Img5TjlGZ1Iyd09KQ21cLzJadU5vNHp3PT0iLCJtYWMiOiI2NDFmYjFlYmU1MzU0NWYwMmM1MDMxZGQzYWFiODFiMGU5NDYxOGRjNGRiMzgzOGQ0NGQwOTM5ZGNkMWI5NmNmIn0=', 'eyJpdiI6InN3RkozSW1PQngzZzg3UXlsQXB1MXc9PSIsInZhbHVlIjoiS0hhUDlKenJOYVwvd1YySFYyMGlydGc9PSIsIm1hYyI6IjQwYWY3OTgzZTlhODI3NzE1M2Y5ZGU5ZWY4NWQ4ZTkwOWYyNWI2MGY3NGNiOTEyZjdlOWE4ZDE5ZmU3NTA3OGUifQ==', 'eyJpdiI6IjdSMzlFMmxac3ROYUFNUHlEdXQ1SEE9PSIsInZhbHVlIjoiWmQxc1Q4QXdKVGIreG0yaGV3c1Y1QT09IiwibWFjIjoiMDgxZmEzYjI4YjFjYjI3MjVkMzUxYjg2OGYzNzc2M2M2ZmJlMjM5NzQ4YzZjOTI2YTliMWVmZDhhYmY5NzM0OSJ9', 'eyJpdiI6IkNXSExJOVZmcUFYNUdFOHhpUzI4Tmc9PSIsInZhbHVlIjoiNGJqcmdVOVdIeWhuTzI4YVdyUHgzdz09IiwibWFjIjoiNDQ0ZTY5OTI2MjVkZDIyODA5Y2ViODU3Zjg5NDQzM2JkNGU0MzY1ZjAxMmIxMWVhNzljOTAyZjkzYzdmYWU3ZiJ9', 'eyJpdiI6IllDVWhHYlZtUHNtSzFYa0ZvdFBFYUE9PSIsInZhbHVlIjoiREYrd1pcL00wK3l6XC9WYlN5T1lQeDBBPT0iLCJtYWMiOiIxMmM2ODU5MWIxNDdhZDQzNTg5ZWY5MzU0MjNhMTBiMTEwZTI0NWFiNWRlMTgwMzljMDI3YTQ5MGY5ZjBhYTcwIn0=', 'eyJpdiI6InlVeGYrbGxIWk5Ra2lub0srNG02dWc9PSIsInZhbHVlIjoiTVl0cXhCT0Yrd0tVOStLV2tWNmZQQT09IiwibWFjIjoiZTdiZDA1NDE0YTlhNDZiNjg0ZjM4ODdhZTFjZDVhNmQwN2FiNjY1ZjE5NjkzZDYwZmM2MzkxOWQ2M2NhMTIyOCJ9', 'eyJpdiI6IjRaWUhMZ0RvZkhXZ1J6blFWWVloZ3c9PSIsInZhbHVlIjoiVm94UmptbmtPcUF3Vm1qTVwvTkNSckE9PSIsIm1hYyI6IjViM2MzMDYzNmRiZmRlZTUzM2Q3ODQ3NWQ4MDIzZjdkNTkyMWI3ZmZiYmQyOWM0ZmUwYzBiMGZiNzBmMzZjNjYifQ==', 1),
(3, 'charles@cegep.com', '$2y$10$wQ5TKSBn3Ev.7abcv/jld..Akftaar1kXWzLRK2qbDvOOrBAJ.9U6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_client` int(11) NOT NULL,
  `rowid_periode` int(11) NOT NULL,
  `sous_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `date_commande` datetime NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `facture` blob,
  PRIMARY KEY (`rowid`),
  KEY `fk_periode_commande` (`rowid_periode`),
  KEY `fk_client_commande` (`rowid_client`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `commande_article`
--

DROP TABLE IF EXISTS `commande_article`;
CREATE TABLE IF NOT EXISTS `commande_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `commande_rowid` int(11) NOT NULL,
  `article_rowid` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantite_commande` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`rowid`),
  KEY `fk_commande_article` (`commande_rowid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
CREATE TABLE IF NOT EXISTS `fournisseur` (
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_fournisseur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fournisseur`
--

INSERT INTO `fournisseur` (`nom_fournisseur`, `actif`) VALUES
('Aliments en vrac Quebec', 1),
('Chocolat Quebec', 1),
('Confitures et gelées Quebec', 1),
('Croustilles Quebec', 1),
('Détaillants de boissons Quebec', 1),
('Distributeurs et fabricants de mets chinois Quebec', 1),
('Eau embouteillée et en vrac Quebec', 1),
('Farine Quebec', 1),
('Fruits secs Quebec', 1),
('Glace Quebec', 1),
('Grossistes en aliments congelés Quebec', 1),
('Grossistes en café Quebec', 1),
('Grossistes et fabricants de produits naturels Quebec', 1),
('Huile d\'olive Quebec', 1),
('Huiles végétales Quebec', 1),
('Levure Quebec', 1),
('Maïs soufflé Quebec', 1),
('Malt et houblon Quebec', 1),
('Miel Quebec', 1),
('Noix Quebec', 1),
('Oeufs Quebec', 1),
('Produits alimentaires Quebec', 1),
('Produits biologiques Quebec', 1);

-- --------------------------------------------------------

--
-- Table structure for table `marque`
--

DROP TABLE IF EXISTS `marque`;
CREATE TABLE IF NOT EXISTS `marque` (
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_marque`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marque`
--

INSERT INTO `marque` (`nom_marque`, `actif`) VALUES
('Ayam', 1),
('Babybio', 1),
('Bledina', 1),
('Bret-s', 1),
('Carrefour', 1),
('Citadelle', 1),
('Danone', 1),
('Fleury-michon', 1),
('Gerble', 1),
('Herta', 1),
('Jardin-bio', 1),
('L-angelys', 1),
('Le-bon-paris', 1),
('Lea-nature', 1),
('Marque-repere', 1),
('Materne', 1),
('Monique-ranou', 1),
('Nestle', 1),
('Prana', 1),
('Propiedad-de', 1),
('Sacla', 1),
('Schar', 1),
('Sojasun', 1),
('U', 1),
('Yoplait', 1);

-- --------------------------------------------------------

--
-- Table structure for table `panier_article`
--

DROP TABLE IF EXISTS `panier_article`;
CREATE TABLE IF NOT EXISTS `panier_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_panier` int(11) NOT NULL,
  `rowid_article` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `panier_article_fk0` (`rowid_panier`),
  KEY `panier_article_fk1` (`rowid_article`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `panier_client`
--

DROP TABLE IF EXISTS `panier_client`;
CREATE TABLE IF NOT EXISTS `panier_client` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_client` int(11) NOT NULL,
  `actuel` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rowid`),
  KEY `panier_client_fk1` (`rowid_client`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `panier_client`
--

INSERT INTO `panier_client` (`rowid`, `rowid_client`, `actuel`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `periode_article`
--

DROP TABLE IF EXISTS `periode_article`;
CREATE TABLE IF NOT EXISTS `periode_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `article_rowid` int(11) NOT NULL,
  `periode_rowid` int(11) NOT NULL,
  `quantite_commande` int(11) NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `article_rowid` (`article_rowid`),
  KEY `periode_rowid` (`periode_rowid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `periode_commande`
--

DROP TABLE IF EXISTS `periode_commande`;
CREATE TABLE IF NOT EXISTS `periode_commande` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_commande`
--

INSERT INTO `periode_commande` (`rowid`, `date_debut`, `date_fin`) VALUES
(1, '2018-12-17 01:30:00', '2018-12-21 23:30:00'),
(2, '2018-12-10 00:00:00', '2018-12-14 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `periode_plage`
--

DROP TABLE IF EXISTS `periode_plage`;
CREATE TABLE IF NOT EXISTS `periode_plage` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_periode` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `emplacement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `periode_plage_fk0` (`rowid_periode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_plage`
--

INSERT INTO `periode_plage` (`rowid`, `rowid_periode`, `date_debut`, `date_fin`, `emplacement`) VALUES
(1, 1, '2018-12-24 08:00:00', '2018-12-24 23:00:00', 'Sciences'),
(2, 1, '2018-12-24 08:00:00', '2018-12-24 23:00:00', 'Hums');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_fk0` FOREIGN KEY (`nom_marque`) REFERENCES `marque` (`nom_marque`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk1` FOREIGN KEY (`nom_categorie`) REFERENCES `categorie` (`nom_categorie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk2` FOREIGN KEY (`nom_fournisseur`) REFERENCES `fournisseur` (`nom_fournisseur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_client_commande` FOREIGN KEY (`rowid_client`) REFERENCES `client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_commande` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande_article`
--
ALTER TABLE `commande_article`
  ADD CONSTRAINT `fk_commande_article` FOREIGN KEY (`commande_rowid`) REFERENCES `commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `panier_article`
--
ALTER TABLE `panier_article`
  ADD CONSTRAINT `fk_article_panier` FOREIGN KEY (`rowid_article`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_panier_client` FOREIGN KEY (`rowid_panier`) REFERENCES `panier_client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_article`
--
ALTER TABLE `periode_article`
  ADD CONSTRAINT `fk_article_periode` FOREIGN KEY (`article_rowid`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_article` FOREIGN KEY (`periode_rowid`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_plage`
--
ALTER TABLE `periode_plage`
  ADD CONSTRAINT `fk_periode` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
