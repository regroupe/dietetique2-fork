SELECT
	COUNT(commande.rowid) as data,
    MONTH(commande.date_commande) as dataset_month,
    MONTHNAME(commande.date_commande) as dataset_month_full,
    YEAR(commande.date_commande) as dataset_year
FROM
	commande
GROUP BY
	YEAR(commande.date_commande),
	MONTH(commande.date_commande)
ORDER BY
	YEAR(commande.date_commande) DESC,
	MONTH(commande.date_commande) ASC