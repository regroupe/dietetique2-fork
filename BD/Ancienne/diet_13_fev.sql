-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 13, 2019 at 07:26 PM
-- Server version: 5.7.19
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diet`
--
CREATE DATABASE IF NOT EXISTS `diet` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `diet`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `super` tinyint(1) NOT NULL,
  `master` tinyint(1) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_modif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `change_pass` tinyint(1) NOT NULL DEFAULT '0',
  `actif` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`super`, `master`, `username`, `password`, `last_modif`, `change_pass`, `actif`) VALUES
(0, 0, 'admin', '$2y$10$qejIbdueBlAOk9Fxger.su33bTctWlR6CKcqZ1iOnz6/GCZvdFQfO', 'master', 0, 1),
(0, 0, 'etu00001', '$2y$10$s6uKfDlLWHplRf/zR5MXyOnP4qvhzKumY4p2pNlAj.1YkhBkHWmvy', 'master', 1, 1),
(0, 0, 'etu00002', '$2y$10$KFaVjzP2cX5fXPTGof2K9eFKUaYd5nPOMAEVD3NpxUlqEtD3qBkv6', 'master', 1, 1),
(0, 0, 'etu00003', '$2y$10$bmBMMqJuHKCURyF.yK/wqurX4M0KdHzPe0pBoMTD/SEKYoDbbyYmi', 'master', 1, 1),
(0, 0, 'etu00004', '$2y$10$u0MHk8VA6NqFcqE8xr1gBeome2gcMNx9xE18gW.0T7HUwl/jYmQ1y', 'master', 1, 1),
(0, 0, 'etu00005', '$2y$10$V6J5IRwe0BApgg08sPBSjOhUv.7GPxa7anNoozYIRw3FWfingdar2', 'master', 1, 1),
(0, 0, 'etu00006', '$2y$10$oolOgUi3uQFGiH.BlHeHKeFgz5r0oPf1Ua6biwTp1dEaEgvot4QNW', 'master', 1, 1),
(0, 0, 'etu00007', '$2y$10$6ArOTea4HdjR6Jyu9Y4nzegYOIXaQ7na/WupR2vjGUHerMHLBg1CC', 'master', 1, 1),
(0, 0, 'etu00008', '$2y$10$7xjY1QTsDuz2s2iav1qj/utVHaz4Gy00b2FnU2BGQJOAHtQ5HzWYG', 'master', 1, 1),
(0, 0, 'etu00009', '$2y$10$xba7xr8XbZBcb7KzubPTe.MZC9WqR6Dxdj5Hk90uxS6YdeNuZnTii', 'master', 1, 1),
(0, 0, 'etu00010', '$2y$10$DC5.a7WgLram2ERh0cvA8uATTkY/VcmHzxxggDBGan7vCQi7oHIhy', 'master', 1, 1),
(0, 0, 'etu00011', '$2y$10$blwxBjRA8bafs/EQKvnpAuPOESVgXOv18lqxKEPae4TfsLvpjbEfW', 'master', 1, 1),
(0, 0, 'etu00012', '$2y$10$zmkKWGKsdLow8zV7nwKJY.1PMFl6ZhLCP9Q.Y7CRNxZ3nk197ZhAu', 'master', 1, 1),
(0, 0, 'etu00013', '$2y$10$YwJi93Gf8t6CiXhMIUTg0eStrMZyiN9EyiWprT.1dl4lSdYTd1DjC', 'master', 1, 1),
(0, 0, 'etu00014', '$2y$10$UUTITvIgGBdI8/jsErLXEeYJ1v8dDzdw.0bLCahjTxuobaTj08Mz6', 'master', 1, 1),
(0, 0, 'etu00015', '$2y$10$e.Nxk6qs59csjwhXQRefgOeIGAk5eAunHhcR3eEpIpoIJX45Duaha', 'master', 1, 1),
(1, 1, 'master', '$2y$10$al.OCHpRFjQKFrkosV4vXuYv3uQnwi.Tyl8pYoKFtdcBUlt.gPxW.', '', 0, 1),
(1, 0, 'super', '$2y$10$GuKNlmDXvss6HWnDS0PCxut.TaD33w7Xzi8BO71KbCB9Q794HzI/m', 'master', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite_minimum` int(11) DEFAULT NULL,
  `quantite_maximum` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `frais_emballage` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL,
  `description` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarque` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valeur_nutritive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `piece_jointe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '0',
  `actif_vente` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rowid`),
  KEY `article_fk0` (`nom_marque`),
  KEY `article_fk1` (`nom_categorie`),
  KEY `article_fk2` (`nom_fournisseur`),
  KEY `article_unite` (`format`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`rowid`, `image`, `nom`, `format_quantite`, `format`, `provenance`, `allergene`, `perissable`, `bio`, `quebec`, `nom_marque`, `nom_categorie`, `nom_fournisseur`, `quantite_minimum`, `quantite_maximum`, `prix`, `frais_fixe`, `frais_variable`, `frais_emballage`, `prix_majore`, `economie`, `description`, `remarque`, `valeur_nutritive`, `piece_jointe`, `created_by`, `actif`, `actif_vente`) VALUES
(1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.95', '5.00', '0.00', '0.00', NULL, NULL, 'Excellente pseudo-céréale naturellement sans gluten!', NULL, 'public/products/nutritional/GSSLKaSc5PSg4gmaRIdSnK3FSTehvSWdeJs5MkIE.png', NULL, 'master', 1, 1),
(2, 'public/products/images/aKxliPxIIy0PREv07R4UHxQsM6vQ5wgNI8O4V26t.jpeg', 'Couscous de blé entier tamisé', '1.00', 'kg', 'Canada', NULL, 0, 1, 0, 'Gerble', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.91', '5.00', '0.00', '0.00', NULL, NULL, 'Pour faire d’excellents taboulés… Yé!', NULL, 'public/products/nutritional/MthROLiRbthvhEaQKDR4s1qPV5CDrQ7Itf2rALBG.png', NULL, 'master', 1, 1),
(3, 'public/products/images/N0AvyopsFGqYKEbFV5t1Pso4YSV20M190CNfcO40.jpeg', 'Flocons d’avoine', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Aliments céréaliers', 'Noix Quebec', 80, 100, '3.43', '5.00', '0.50', '0.00', NULL, NULL, 'Cette avoine est roulée et torréfiée à sec avant d’être chauffée à la vapeur à 200°F pour être roulée. C’est ce qui distingue ces flocons de ceux des autres rares entreprises qui en font au Québec, et c’est ce qui leur confère ce goût de gras naturel de l’avoine.C’est ce qui leur permet également de hausser sa durée de conservation à un an sans problème.', NULL, 'public/products/nutritional/8I7DFLv4nW6zmVwVkWAUkyr5gtejjhzzY6SgMYlH.png', NULL, 'master', 1, 1),
(4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 50, 50, '19.92', '6.00', '1.00', '0.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes, dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température. Pour les desserts cuits comme les gâteaux ou les brownies, nous recommandons plutôt le cacao en poudre; le goût du cacao cru serait trop prononcé pour de tels desserts.', NULL, 'public/products/nutritional/p7PKD2k1iGJ1CLL5igoP8Sv2GyxvFHIJTdZPFBKP.png', NULL, 'master', 1, 1),
(5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 40, 100, '14.88', '5.00', '0.00', '0.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao en poudre pour les desserts cuits comme les gâteaux ou les brownies, pour lesquels le goût du cacao cru serait trop prononcé. Dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température, nous recommandons plutôt le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes.', NULL, 'public/products/nutritional/SIljBYRTECmTu0cARK3YJQGdaiwOMKzmGB3HEH78.png', NULL, 'master', 1, 1),
(6, 'public/products/images/fxtpyETXUMXk2OswDJ3T1SparCXp9c4caKKNOa8J.jpeg', 'Capuchons de chocolat 70%', '500.00', 'g', 'Fèves du Pérou, chocolat produit en Europe', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 150, 200, '23.93', '7.00', '0.00', '0.00', NULL, NULL, 'Pépites de chocolat géantes, pour cuisiner ou manger telles quelles comme délice du moment. Environ 5 fois la grosseur des pépites de chocolat, elles font d’excellents desserts avec de gros morceaux de chocolat qui en mettent plein la dent!', NULL, 'public/products/nutritional/Xx8RvLJaxYXeNYale45l5G4WCbvOKrv54PdIUmiG.png', NULL, 'master', 1, 1),
(7, 'public/products/images/TmxisxJI9ocq2Ip8TQriNaer4WypYgn9Enuy9HBb.jpeg', 'Café La Montréalaise Santropol', '500.00', 'g', 'Amérique centrale et du sud', NULL, 0, 1, 1, 'Materne', 'Café', 'Grossistes en café Quebec', 65, 125, '14.37', '5.00', '2.00', '0.00', NULL, NULL, 'Café gourmet biologique, équitable, en grain et de torréfaction française. Un riche mélange corsé aux reflets de marron et d’ébène.', NULL, 'public/products/nutritional/stjSP5cjaLxUFLYOdMhmaB2M6t1cy6gsZGsdzdJh.png', NULL, 'master', 1, 1),
(8, 'public/products/images/yepAc3CrDZsNuFSxHG4h1hFbgYuqwsq7lI72nlSV.jpeg', 'Levure alimentaire en flocons', '500.00', 'g', 'États-Unis', NULL, 0, 0, 0, 'Ayam', 'Condiments et autres', 'Farine Quebec', 80, 120, '13.89', '5.00', '1.00', '0.00', NULL, NULL, 'Levure produite par fermentation naturelle, aucune source animale ni synthétique.', NULL, 'public/products/nutritional/oMVZlewVITN6Jx7GfMeiFzOUOr9hQe4YXg5vt0o4.png', NULL, 'master', 1, 1),
(9, 'public/products/images/7AzHZCs5UGaVzjEJETAw9z1XquORt6uMNrRyF99U.jpeg', 'Miso soya et riz sans gluten', '500.00', 'g', 'Québec', NULL, 1, 1, 1, 'Bledina', 'Condiments et autres', 'Confitures et gelées Quebec', 100, 100, '13.83', '5.00', '0.00', '0.00', NULL, NULL, 'Le miso biologique Massawipi non pasteurisé de soya et de riz est un aliment sans gluten très polyvalent; son goût est doux, rond en bouche et légèrement salé. Il est le résultat d’une fermentation naturelle (non forcée) d’au moins 2 ans. Pour sa fabrication, nous n’utilisons que des ingrédients certifiés biologiques garantis sans OGM.', NULL, 'public/products/nutritional/Ty12FzodDT1kbWEeKsEnqlXZwim1KggiKytU5P9l.png', NULL, 'master', 1, 1),
(10, 'public/products/images/I6XjF0Hwgg9sIX0tqhw7rrjiacSVk3oHcqy0DeXV.jpeg', 'Moutarde de Dijon', '1.00', 'kg', 'Québec', NULL, 1, 1, 1, 'Herta', 'Condiments et autres', 'Produits alimentaires Quebec', 20, 20, '8.98', '5.00', '0.00', '0.00', NULL, NULL, 'Moutarde crue, sans sucre ajouté, sans agents de conservation, sans gluten. Une moutarde de Dijon avec une belle texture crémeuse et une touche de piquant qui la rend irremplaçable! Pour les amateurs de moutarde forte!', NULL, NULL, NULL, 'master', 1, 1),
(11, 'public/products/images/uwhju93QU1tzQNJO5xXD9iu1JDSzw7w00tRcRFEe.jpeg', 'Farine blanche tout-usage non blanchie', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Sacla', 'Farines', 'Farine Quebec', 60, 70, '2.88', '5.00', '0.00', '0.00', NULL, NULL, 'La farine blanche tout usage non blanchie est tamisée pour en retirer tout le son et ne subit aucun traitement de blanchiment. Elle est produite à partir de blé dur de printemps biologique, et les grains sont moulus sur cylindres. Idéal pour la boulangerie et la pâtisserie.', NULL, 'public/products/nutritional/6cCibCA8qr7SRrQYZCsZimR5Ly3GKJUWB1d9bm9o.png', NULL, 'master', 1, 1),
(12, 'public/products/images/n5nqMXBrO1Lx3XhZ1l8ZCpQA5D7g08p9WHQq9tuB.jpeg', 'Farine d’épeautre', '200.00', 'g', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Farines', 'Farine Quebec', 25, 30, '4.60', '5.00', '0.00', '0.00', NULL, NULL, 'Depuis mai 2018, cette farine est tamisée de sorte à en extraire 10 % du son, ce qui lui procure un meilleur rendement en pâtisserie et en boulangerie qu’une farine intégrale.', NULL, NULL, NULL, 'master', 1, 1),
(13, 'public/products/images/1TpWugpuoiLsRiiFu3ToTpzfZcBhz8dRDzXDL5UL.jpeg', 'Farine de blé intégrale', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Fleury-michon', 'Farines', 'Farine Quebec', 80, 80, '2.88', '3.50', '0.00', '0.00', NULL, NULL, 'Cette farine de blé intégrale contient toutes la parties du blé. Le son et les rémoulures donnent plus de texture à vos pâtisseries et à vos pains.', NULL, 'public/products/nutritional/PSd1hLFB885sjyTw5jfEZAOjZTPm1zGah4LvzJ8x.jpeg', NULL, 'master', 1, 1),
(14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 20, 40, '13.94', '5.00', '1.00', '0.00', NULL, NULL, NULL, 'Il est possible de retrouver des noyaux dans le produit.', 'public/products/nutritional/0K5UCVCywUYqmbWHv79g8rJiwUQzyEKXh5BX6e09.png', NULL, 'master', 1, 1),
(15, 'public/products/images/17YmeaiQPt8j48kzvCWmxRqXAzBKtDD3r7r0TetF.jpeg', 'Canneberges séchées au jus de pommes', '1.00', 'kg', 'Québec', NULL, 1, 1, 0, 'Citadelle', 'Fruits séchés', 'Produits biologiques Quebec', 175, 200, '18.66', '5.00', '0.00', '0.00', NULL, NULL, 'Ces canneberges séchées sont préparées à partir de canneberges (Vaccinium macrocarpon) matures de première qualité et infusées dans une solution de jus de pomme concentré. Elles sont tendres et juteuses et présentent le goût acidulé et légèrement sucré caractéristique de la canneberge. En faisant sécher les fruits plus longtemps et à plus basse température, Citadelle parvient à préserver la saveur des canneberges ainsi qu’une quantité maximale de nutriments, pour le bonheur de nos palais et la santé de notre corps.', 'Le tout est 100% naturel, tant le procédé de conservation que les canneberges elles-mêmes auxquelles aucun agent stabilisant, ni colorant, ni glycérine n’est ajouté.', NULL, NULL, 'master', 1, 1),
(16, 'public/products/images/kPZuCChkJhrxRQ0YOhKAqSt6MaWVfGeLjc5eqwFD.jpeg', 'Dattes Deglet dénoyautées', '1.00', 'kg', 'Tunisie', NULL, 1, 1, 0, 'Prana', 'Fruits séchés', 'Produits biologiques Quebec', 80, 80, '12.77', '5.00', '0.00', '0.00', NULL, NULL, 'Youpi! Savoureuse et sans noyau, bien que, étant donné que le processus de dénoyautage est automatisé, il est possible de retrouver quelques noyaux parmi ces dattes.', NULL, 'public/products/nutritional/qQDAsd2DoU6n1C0ABNUWrrR14Qz0yj6xvi2IWj9W.png', NULL, 'master', 1, 1),
(22, NULL, 'test', '1.00', 'unité(s)', 'test', NULL, 0, 0, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 1, 1, '1.00', '1.00', '1.00', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, 'master', 1, 0),
(23, NULL, 'aaaa', '1.00', 'unité(s)', '1', NULL, 0, 0, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 1, 1, '1.00', '1.00', '1.00', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, 'master', 1, 0),
(24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 39, 40, '663.47', '0.00', '0.00', '23.00', NULL, NULL, 'Zolie péteu de Ava fraichement cueilli dans les prés de Catherine.', 'Très zolie et muscululé', 'public/products/nutritional/qtZoE6aMSXqLxGNivqh9AWMwaVbpa6UBF67bhLHd.jpeg', NULL, 'master', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_categorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`nom_categorie`, `actif`) VALUES
('Aliments céréaliers', 1),
('Amour', 1),
('asdfas', 1),
('Cacao et ses acolytes', 1),
('Café', 1),
('Condiments et autres', 1),
('Farines', 1),
('Fruits séchés', 1),
('Graines à germer', 1),
('Huiles et vinaigres', 1),
('Légumineuses', 1),
('Livres', 1),
('Noix et graines', 1),
('Pâtes', 1),
('Produits québécois', 1),
('Sacs écolo', 1),
('Superaliments', 1);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`rowid`, `email`, `password`, `telephone`, `poste`, `nom`, `prenom`, `adresse`, `no_app`, `code_postal`, `ville`, `pays`, `province`, `actif`) VALUES
(1, 'jessy@cegep.com', '$2y$10$BkAGbBom8y9bQz8LeD9L3eRIAKDhQsOsifdr114hbWIJyLEVVF8VC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(2, 'zach@cegep.com', '$2y$10$C3Kao9./HOM/HDABXssW0OS9d0cPDXOGdTb6M5TIl9czDfULJsmNW', 'eyJpdiI6IkRBc1wvZGJuTm5nMDU2ekE5WDV4SHZRPT0iLCJ2YWx1ZSI6Im5vUm11bUpDRlJGVU5sdVhLMGJXVm9OSmU1Z1MyWjEySzFaV1pEWTI0Tzg9IiwibWFjIjoiZmI2NDg2NWRhY2RkMmI5MmY2ZmY5ZWE3ZWQwMWZkOTk1MDQ1NjE4MTllN2RkY2ExNGIyMDZiY2UxNGRlNjZiMyJ9', 'eyJpdiI6IjJPOW1IbGxvNCt0SlNtRThzQmNOanc9PSIsInZhbHVlIjoiZ2dFMGpxTVo2Z2p1Q0p4R1JPUDc0UT09IiwibWFjIjoiZDNiZDQ4MWZmNTNlMzA3ZTRiYTMyMTA5NGU1ZjU2NjA0ZjZlNTRhNmVlZDdhMDUzZjRlMzIzMTI0ODUxN2ExMyJ9', 'eyJpdiI6IkpTaUk3NDBiWVNcL0dpRUlweTBWeTNnPT0iLCJ2YWx1ZSI6InBNVzFxOGxGb0F5S2JzWFF2bEZXRmc9PSIsIm1hYyI6IjA4YjU4NDhjODRhNTY4M2Q1Y2UzYjM4OGQ3YmU1MzY3NzdhNDJiNzkyNmQxNmJkYmEzYzhhZGFjNDlkOGUwZjMifQ==', 'eyJpdiI6ImI5eGl2REUwREY2ZzVkZUJnbUhnQ2c9PSIsInZhbHVlIjoieVdRZk80M1RWTWUxM3FuXC9qNlpaXC9BPT0iLCJtYWMiOiIwZDcyN2NiZjU1MWZjYzc0ZGE1MWNkOWNlZDQ0Mzk0YzJlYzk3NDcwZTVhZmYwMzFkYmViYzlkZDQ2YjZhOGNhIn0=', 'eyJpdiI6IlgyRnpJRStHaWIxTDk0NGlhYVwvSlBBPT0iLCJ2YWx1ZSI6ImpuYVd5Y1hjeDE2N2ZPRTVhUDFqNXc9PSIsIm1hYyI6IjM5NjdlYTAzNmI3MDA2NTI4MjU4YjVjNWJlNmZmNTlmY2FmNTEyOTYyYzM0YjhlMTczNTgzMzhjYWI4OGM3N2UifQ==', 'eyJpdiI6InptR0lCK01NNnBLZUdLXC92TE15YlJRPT0iLCJ2YWx1ZSI6InZWVVdpazlzajBzZTN4S3ZqOE1VNVE9PSIsIm1hYyI6IjdhMjY1ZWIxY2Y4MzFjZDc0ZmJhZWE2NzgyMWQ5M2JmZTBlNjgyOTFjODI1ODE0NWJjODEwZWFjNTk4Njc1ODQifQ==', 'eyJpdiI6Ik5XczhPZkg2S0RvRWk2YW5pVW5LK1E9PSIsInZhbHVlIjoiNjZieHBzU3Jtbk54RjNLXC9QMjVRdWc9PSIsIm1hYyI6Ijg4ZTExMTUxZWI2MGVmNzFhOGU4ZWIyMjQ1YjYyNDU3MzI4NGQ2MGJkODNmYTRjNDA3ZmUxNmI0NTVkYjQ2MmYifQ==', 'eyJpdiI6ImpOKzBxTlNiK3dHVGtJZG92ckdrY1E9PSIsInZhbHVlIjoiaDBFZVZobUo4M00wUjdrUDZRYUR1Zz09IiwibWFjIjoiNGZiNjEzMzlhNGMzNWMxMzQ0YzRjOTQ3ODU2NzJmMTU1ZDIxOGZhZjFlYWYzZmZhZGIxNWNkYmJiMzg2ODU3OCJ9', 'eyJpdiI6IlRCK0NcLzN3OWtjbGY2aXpRVkNRaDN3PT0iLCJ2YWx1ZSI6ImdNbmhwNzZBTTFsXC8zbUY2R0ltNlhBPT0iLCJtYWMiOiI1NDQ5NGRlOGY2MzIyZjk1NGRlMjMzYmFlMTIwMGIwYTFlNmUwMTljNjNlMzhiODlhYmQ2MGEzZjZmZmQzNmY2In0=', 'eyJpdiI6IlNXaGZoUHoyakc5NHdPMlNjZ2ZtUUE9PSIsInZhbHVlIjoiUlNia0E4YVR2cm42T0NXeWRXbEZQZz09IiwibWFjIjoiNjI5YTgwNDRmZTBlMTM3YzEwYjEzYzUwNTljMjUxYWM1YTNjOTRjZjQzZjkxNTQ1Mzk0ZWNjN2NjNjllMTNkZiJ9', 1),
(3, 'charles@cegep.com', '$2y$10$wQ5TKSBn3Ev.7abcv/jld..Akftaar1kXWzLRK2qbDvOOrBAJ.9U6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(4, 'jean@jean.com', '$2y$10$aXV75HlGGp8iu5qReeWQpuiYs9NJyzeEtRHl.hV3fmzHJM.Bi1xvi', 'eyJpdiI6ImJjODZJYXZmV2lhREloVUdEclVJQVE9PSIsInZhbHVlIjoiZjZSNU5WeHhRRjNtT1RsY0NvTEVYc1lJc3hmOVJDelJkaWZCN3Ntaklidz0iLCJtYWMiOiI2NzRiZGVkM2NjNTJjNjk1NmRhMmRjYzE1ZTYzYTQ3ZDViOWI2YzRiYmE2ZjgzNDZjOTZkYjYyMDRlNTkyYjA4In0=', 'eyJpdiI6Inh2YmwxXC9DcE5hRW1CY05uT0RLQkhRPT0iLCJ2YWx1ZSI6ImdhUFNpZHROaEphZk9Lelh3WmV1WWc9PSIsIm1hYyI6ImIwZTA4Yzk3NWZjY2M4NzVmNzMxMDNlYjU1Yzg2MjYxNDc3YzU2MWRlYzQzODgwN2Y4MTFhODY1NmZiYzQwZjQifQ==', 'eyJpdiI6IlpjanRzY1Q2enp6NVdpbUVOaG9NUlE9PSIsInZhbHVlIjoid3EyUGZVUjdONXRQRlgraTVLdzVsZz09IiwibWFjIjoiNzE1MGQ5MjE1ZjVkODI5NTdhOTgxMzFiYzc3MDNiMjRkNTQyYzZlOGU3ZDkxNmE3ZjIwYjY1YjhlMjA4ODNiNSJ9', 'eyJpdiI6Im1Sb0tQcXFDWGdUeUR2K2J4RmJHOWc9PSIsInZhbHVlIjoiNW1JT1pTajRaRWpLbXNvVTZLcjBWUT09IiwibWFjIjoiYmUwY2RjOTI2MDYzODM4Nzg4MGFlZjU2MDExYjNiY2NiMWU0ZDliYWRhYWEyMDhhNGFiMGIxOTJiYWE0YTc2NiJ9', 'eyJpdiI6InRYMkNRZlhNWno4OFQ0ZG1ERTM2Y0E9PSIsInZhbHVlIjoiUkdZZlNvQ3BCV3BVWTJRK1NtTFFKZGRaMTJ1cmhNVXRBVEJqSjZ5UEJpVENIYlU5VXc3SmUxSWhxd2dTMVorUiIsIm1hYyI6IjYwZWNhMDUyZjVhMTAwNjRkY2U1NTNiZTdiOGJjMmE5ODA0MDlhNWE0N2M4MWM1ODQwYjQ4OTcwYTM5NzVmNjMifQ==', 'eyJpdiI6InZHcnBmVFUyVVE0d2U4XC9LRDRaMUJ3PT0iLCJ2YWx1ZSI6Ik9qVmhlZlNBUm9RS0pJWGg5U0NpSmc9PSIsIm1hYyI6ImMwZDc4NmYzNjkxN2RhZDk1ZGUzYjY1ZGUyMWFjZWZmZjg5OWVjZTI2ZWQ0NDBiODc0MmQ4M2YxYmY1Nzk1ODQifQ==', 'eyJpdiI6ImdnUTYydksrVE1RSFVDSndRNmxLRVE9PSIsInZhbHVlIjoiY1VKMVF2R29uVVwvSG5lbTlreUpIS1E9PSIsIm1hYyI6IjkwODQ1YTMzYmMyMTIwNzBkMjZkZTJmMzMyMGZkYjMzZGMwMjBkYzIwMDM2Mzc5ZDg3YWExNjYxOGQ3YTFkZWIifQ==', 'eyJpdiI6IkpGOFJ2SUo0dGhCcHlENU9XXC93TVVBPT0iLCJ2YWx1ZSI6IlY3RzJEcU5cLzNQZmtFdml2NTliR1FGcDFGdXpKVTdVZFRJT2tHb2wzK1ZzPSIsIm1hYyI6IjQ5NzgxN2M1NDE3OWUxYzRmM2JkYTE5M2YxZjQyMzU3MTJkNmU3MmJkMTE5M2NhYzJhZDBmMDBlZmY2ZjY2MzcifQ==', 'eyJpdiI6IlUrM01ZN2FNWlFhdE9BclREK3p4aFE9PSIsInZhbHVlIjoiZERYY3NcL2x0MEZrUjZ5ZDhJY2pONVE9PSIsIm1hYyI6IjZiZjVhNjI5OTY4ZDU5Y2EzNjI1NTk1NTBiYzYwNTIxMmNhNGQ3YzYxNmZjMDc0Njk4ZmJiZWM0NGQwM2MwNGIifQ==', 'eyJpdiI6ImRIRkh2WkxXaXhVc2VSZFp1aXJtNmc9PSIsInZhbHVlIjoiXC8xQ0VPUFdYZUNZS0VqWU5IeTNcL2FRPT0iLCJtYWMiOiI1YjU2OTc5NDhmNjM5NmVlYmM2NWU0Nzg0N2ZkMGQ4ZGE2Mjk2MGFhMTY3Njc4ZGIyZmY4OTY1ZjcxNjQ2NzQxIn0=', 1),
(5, 'cath@cath.com', '$2y$10$5M567W3PrDjPEcAViVSWd.Cdir7ck3PQELPvnMNdoXiIkuw.kKgJ2', 'eyJpdiI6Im40SmNheDdFY2dkWmZDdUVEYzBlcHc9PSIsInZhbHVlIjoiVmpvQXN0ejY0Q0FEeXJjc2E5VFR4bXFwOUxRSVdxUml6UjR5WDk4Y2FwST0iLCJtYWMiOiJlM2E0N2M3NTdlYjBmNWE3YWMxMGU2YzViZjI3YzZlMTMzMDRmODcwODQ2ZWNjODQ2ZTQwMWQ0YWI3OWM2NGJiIn0=', 'eyJpdiI6ImRnVmozN0dFaGtES2U0SHNUeHFMRVE9PSIsInZhbHVlIjoidEtlNEE0cDUwcW5IT2VqZkQ3ZWlKQT09IiwibWFjIjoiMTY3ZGJkYjhiMWJiNWY2MDk0ZDhkMzFiMWJlZGZhMzliNzJkMDY4MjkxOGFhZWQ0NTk2YjRhZjNhMWNmZDhjNyJ9', 'eyJpdiI6Inl1RkZPVkdGZTFvd1k5Y2toVUtNNlE9PSIsInZhbHVlIjoiUmNmb1BYQzBGVjcxM2p1aUN6SFZOSXRYbUNnTW9GMFZJTmlUTDZaalA5dz0iLCJtYWMiOiI1ODE2OGFmZTM1ZWRiNzUzMmM0Y2U3MTUzMDM2M2UxNTYwN2FkY2JiNmUzMTVjMzgxMjY1ZmRhNTQ2MTEyMGFiIn0=', 'eyJpdiI6IkZSemVNWmVHN0h2NnVFb2NENXR5RkE9PSIsInZhbHVlIjoicmJpekdRTktLbXpLeE53RkpvY2toT3JaZVZRVGl4T3FhOTVpVWpXVlJLRT0iLCJtYWMiOiJiNGFkMTUyNzA1MzNjODYzNzVkMjBiMWM4MWFiNzY3YWZiZWQ2NGJjMzJlZjg0N2QwMmY0NjdlNjkyNGM0ZGE5In0=', 'eyJpdiI6ImRSZEc1ZjREMXl2TVwvdFpRZlpqYkNnPT0iLCJ2YWx1ZSI6Im5LXC9Ic25VdklXbUFcL2VGR2NCc3JQNElQTzVEbXFGaTdRXC9XUnBrc0h2TWs9IiwibWFjIjoiNmQ2YWYzNTMyNDdkMDY5ZmU5ZjVlY2M5YzdkMjgwYjE2YWFiYTc3NDRhZWZiNWYxMDJhNDExZTNkMWU2Y2ZjZSJ9', 'eyJpdiI6ImJKSjVERW4wbnQ3blgxXC9TMktyRDRnPT0iLCJ2YWx1ZSI6IlE0WW1BNU8zSHVmSHZSNGZ4Y09zZmc9PSIsIm1hYyI6IjVlNjc4YmZjZDYyZTJlMDc1ZWFhN2QxZGQyNTU4YTgyM2U0MDhmNTE1NWUyYmZkMzQ1ODQ1OWE3OGZiZDQ0MmYifQ==', 'eyJpdiI6IkZFeHNwSHB4MEFmbUN5SWtwNWRmNGc9PSIsInZhbHVlIjoiK1g4ZVU5emVlbzlabFR6S1YyUlMwQT09IiwibWFjIjoiMGE4ZDNlMGY3MDU1ZmJjZmU1NzA5ODg3MGMyMTYzYjdlMjY5MGU0OWRlNzkxODJiOTc3NTM1ZjM1MjgyYjY4MiJ9', 'eyJpdiI6Ijg3UWpuVkUwbVpxck9TQ1NYc0lySnc9PSIsInZhbHVlIjoiR0NNYlpvMDV3OTJxWHU4NlYyeDRKRHZ1OGpmZlwvWGpaenlJQUhlOUlTeUU9IiwibWFjIjoiZWJjNTEyYjY1MDE2Y2U1NzRiMzk4Y2RlYWFlODdkNzRhZTI0ZTYzMjYyOTZjYmFiNGU0M2Y2MTg2NWMwYTdiMCJ9', 'eyJpdiI6Im5kRWFmYXRib1QxajNneDhcL2R3SGJRPT0iLCJ2YWx1ZSI6IlFVQ0pjSzlzUGRxTk1PQWhPZUlkNlE9PSIsIm1hYyI6IjNjNjM4MjU2MzNiOTE4ZmMwYTY1NmM2NjdmYTA1YTNkZGJlYzBiM2U5NzI2OGE1YjFkN2Y3NThjMjkyZTg5ZGYifQ==', 'eyJpdiI6ImtDRGJLVmp2WU9wQXpoR2dwRStNOHc9PSIsInZhbHVlIjoiOVZIK0tPdDZha05qK081RE9qSkZHUT09IiwibWFjIjoiNTc2MTI5ZWZkMmY3ODkzYzVhODE5NTg2OTVmZmVkODBjZTFhNDViNDY2NjRhYzViNDIzM2E0ZTc1Y2MwYjhhMCJ9', 1);

-- --------------------------------------------------------

--
-- Table structure for table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_client` int(11) NOT NULL,
  `rowid_periode` int(11) NOT NULL,
  `sous_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `date_commande` datetime NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify` tinyint(1) NOT NULL,
  `facture` blob,
  `identifiant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paye` tinyint(1) NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `fk_periode_commande` (`rowid_periode`),
  KEY `fk_client_commande` (`rowid_client`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commande`
--

INSERT INTO `commande` (`rowid`, `rowid_client`, `rowid_periode`, `sous_total`, `total`, `date_commande`, `email`, `adresse`, `no_app`, `ville`, `province`, `pays`, `code_postal`, `telephone`, `poste`, `notify`, `facture`, `identifiant`, `paye`) VALUES
(1, 2, 1, '32.83', '36.45', '2019-01-30 13:08:03', 'zach@cegep.com', 'ghfhf', NULL, 'fghfgh', NULL, NULL, 'G6B0W1', '8888888888', NULL, 1, NULL, '', 0),
(2, 2, 1, '79.68', '88.44', '2019-01-30 13:09:27', 'zach@cegep.com', 'testing', '12', 'Hello', NULL, NULL, 'G6B0W1', '8888889999', NULL, 1, NULL, '', 0),
(3, 2, 1, '44.64', '46.86', '2019-01-30 15:49:24', 'zach@cegep.com', 'testing', '12', 'Hello', NULL, NULL, 'G6B0W1', '8888889999', NULL, 1, NULL, '', 0),
(4, 2, 1, '80.82', '84.78', '2019-01-30 15:51:34', 'zach@cegep.com', 'testing', '12', 'Hello', NULL, NULL, 'G6B0W1', '8888889999', NULL, 1, NULL, '', 0),
(5, 1, 3, '330.51', '392.84', '2019-02-03 23:07:10', 'jessy@cegep.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '', 0),
(6, 1, 3, '64.35', '67.47', '2019-02-03 23:08:25', 'jessy@cegep.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '', 0),
(7, 4, 3, '163.32', '182.22', '2019-02-06 14:10:21', 'eyJpdiI6IldZZ21iRjJ4eEtIaW5hcG1pTjg1XC9BPT0iLCJ2YWx1ZSI6IlU5MU1RUVN4NGRqQUtBbjdVRHZxVHBwYkpkeDNSU3gzQ3NvVE5JU3ZsYlU9IiwibWFjIjoiYjk5NTA3ZDE1MmM3ZGNlNGM3NGVkNzY4NThmZTZhZjE4MzA5OWNjOWVhNGM0YjM1NTU3MDNkYmI1Yjc5Y2VjMSJ9', 'eyJpdiI6IjcxS2RzTHRBbmhMS0pBQkl5R2c5YlE9PSIsInZhbHVlIjoiT29jVEt3VHN3UVlrN3czNVdxXC9mSGc9PSIsIm1hYyI6ImUxYTZjMzViMTFkNTcyYzdlMjFjOTVkMTAwYmU3YzU3NjNhZGYzOWQ5ZGFkNjI3YTgyOGFhNTliZTYxNDQ0NTMifQ==', 'eyJpdiI6Ijd1S3NiOUhWM3BoRjF1dzhaXC8zSEdBPT0iLCJ2YWx1ZSI6IjBldkFoVjRjTnBzdU9WOVZcLzA5QVZ3PT0iLCJtYWMiOiJjMjIxNTkyZGNiZTZhOTNmNjlkY2E5NzZmOTJjNWE4MmFhMGRkNDkwMzNiOTRmNWMyOWQ5YWMzODI4NDAzYzBkIn0=', 'eyJpdiI6InZNdTVKWHI1VkpsVWtrOWxmb2pmWWc9PSIsInZhbHVlIjoia2ljbldaWG1zZ25pZGRPeVRzeE5IZz09IiwibWFjIjoiZDQxMTYyNGY2Mjc4YTViYzI1ZmM5MjMxOTJlMzdlMDcyMzQ0OWRiOTQzMGFiNjg1NjYzMmNlNWZkMjQ3OTBjNiJ9', 'eyJpdiI6IjFhKzJcL3plOHZVSzladmZYRUpiZnh3PT0iLCJ2YWx1ZSI6Iis1dHhaUE1KUFdIMDBPSmN5Z3FVOXc9PSIsIm1hYyI6IjAyMmJiZjMyMmNlMjA0MDAxYzU0YWExZDY1YjE2ZGEzNGYyOGViZTE1NzU3NTNiNWUyNDZmOTRlNTcyNDc0NGUifQ==', 'eyJpdiI6Ijd0M2tlQitXR01SMythVmNIODh2YWc9PSIsInZhbHVlIjoieTVcL05LUHFtdUFZOFBaQ3MyMzhHY2c9PSIsIm1hYyI6IjQxN2QyMjQ5Mzk2OTVkMzVlOWY3ZmExYzRhNDUwOTlhODk0NTVjNmI1OGVlYjBmZjgzNTE0OTAyY2MxYTkzZDUifQ==', 'eyJpdiI6ImpINTd0aXVZNlVvQW92RSt3ZCsxSGc9PSIsInZhbHVlIjoiZUcwbmVBbDN2M2dIQU10NHhoRFY0UT09IiwibWFjIjoiNzRkZTM5YTYwYjU3ZTc0MjYwM2Q2MTg0MWQyOWFiMzAxZTFiMmZhOTFmNzc4MGVjMjlkMzcwMjJmMmZhZWU0YyJ9', 'eyJpdiI6IjIwQ0xUV1pydmlEMUkzeXh3QkIwT0E9PSIsInZhbHVlIjoibXR4cVVsQXRTR3lQYWdvajd2aFQzZz09IiwibWFjIjoiZDE2YmYyOGVhNWY4Yzg3ZTAwMGRjMDBmYWM1ZTEyNzA3OGEwYmQxZjYzNmFmY2FkODViZGM1OTlkMGRmZWYzOSJ9', 'eyJpdiI6InkwZ0dtakNCZUJWcEZMVWFIWWU3Q2c9PSIsInZhbHVlIjoicGpaeDZhQzFCVHc4amhPc1p2UDNFdz09IiwibWFjIjoiOTM5MDExY2Y2YjIxNGE5MWU5MDc5YjE5NTcxZTUxMjVjYjJjNGY3ZmU1YWRlZGQzZDE1MDEzNmU1NWJiNzczNyJ9', 1, NULL, '', 0),
(8, 4, 3, '143.58', '153.60', '2019-02-06 14:13:47', 'eyJpdiI6Im1FY0JLbXY1RGJpZzdXNlRPYVVSQ1E9PSIsInZhbHVlIjoiVzJVd1hpVFA2M3UzTlQwUFRkZDE3YmhpSDRnbWwzc1BsV0FqMjV5U0ltTT0iLCJtYWMiOiIxOTMxZTJlYzFhYWU3M2RiY2M0OWVmZGJjYTczNDE0MmJiMDNkNTI4ZWQwMDk3NTdiZmE1NTBiNjhlMzNkNjVkIn0=', 'eyJpdiI6Imt2a3FrbDdGM3Z4aDdkYmRTSDBGSEE9PSIsInZhbHVlIjoicVwvS09za0ZoeGU2UGpOSFlaZW5RN1E9PSIsIm1hYyI6IjE3Yzk0ZTk3MDc5NWJjMTBkZjQ5NTE4MTNiN2JhNTYwZjZmYTE2NmY2NGJlODYyNzAzZWFlMzkwM2MyYzlmMjEifQ==', 'eyJpdiI6IjJEXC84emo4cHcxXC9meTZISmpvd2xNdz09IiwidmFsdWUiOiJ1YnFUeUVqZDF2ZkU5V2lUZldPU0h3PT0iLCJtYWMiOiIwNWJlZmQ0ZWY2MDU3YTRhYjU2MmFiNzU0NGRiZmE2ODdlYTcyOTU5ZmY5ZTJjYWU4N2JjZmI4MjM3ZGNhNTA0In0=', 'eyJpdiI6InpKYU40VjRLcmMwM1lhb3hRSU9XRlE9PSIsInZhbHVlIjoidncyQnlBdythRldSaWtPMUxrQTVUZz09IiwibWFjIjoiNWVlY2JjNmM5ZDUxNzdiMGVjNmYwYWI4NmY3MTQ3Njc1ODQ4ZWZmNzAwMmY1NTIzNDM3ZTRjNTk1ZTc1M2RkMiJ9', 'eyJpdiI6ImhiRFJrdjRQOHQyakZOOHdlQUN4R3c9PSIsInZhbHVlIjoiQ0ZkUjcxWjlXR2hEWUNNM1hoN2FxQT09IiwibWFjIjoiMTQ5MmRjMjFkMzcyNDdhODM1MDhlYmFmM2EzZGM2ZTIwZDYzM2U1ZDRmNjVkMzFlOTQ2OGM1OTZhNjE3YjI3MyJ9', 'eyJpdiI6ImY1OTU3dmFvRmNnd2RkWEppYU53bXc9PSIsInZhbHVlIjoiNXE0S212YlZobDAwSnRmVlwvVkJPRUE9PSIsIm1hYyI6IjNlNmRiNmU1ODllOWYyZDk5MDE2NzA2ZmZmOTQ2ZjY3NWU5YjUxMzA3MzBjOTMyMWFkYTcyYWVmMWJiYjgzMDkifQ==', 'eyJpdiI6IlQ5XC9oaTJpY1RiVTgxZVFoRmNWTitRPT0iLCJ2YWx1ZSI6InJTbm12emdJRjdKcFZwM1wvbXZLXC9oUT09IiwibWFjIjoiZmQyOGUxZjBiMzFkMjZjMTZkZTZhODU0NjliNTJmNmZmZTY2MDRiYzQ5MjMzMTJhMGQxNzlmNTg0NmViM2I5MCJ9', 'eyJpdiI6InpuamJJMFwvK1wvUm5KN1JLOEdtWmlzUT09IiwidmFsdWUiOiJ5VUlXQjVpNisxV1Bnd0NkNlJQSndRPT0iLCJtYWMiOiJlMmQ3ODAxNjhlOGZiZjdhZjg0NDlmYzAyYmJkODk5ODkyNGYyMWI1Y2M3Yjk1M2UyYTAxNDlkNjM1NDVjZDlkIn0=', 'eyJpdiI6ImN5eEROSjZzWktpRlh6UU9nWHBGb0E9PSIsInZhbHVlIjoiTUFRRXlrcTNxUmxVNHJ0TnhyODl3Zz09IiwibWFjIjoiZDNlMDU2OWEyY2RmNTE5YzJkZWMyYTE4ZjU0MTkyOGY5MjQ1YWRkNzc1MWZlYWIzNzRkNWYyMWQxYzc3OGMyYyJ9', 1, NULL, '', 0),
(9, 4, 3, '13.94', '15.63', '2019-02-08 13:09:49', 'eyJpdiI6IkVCZ05zMjM3Ynczc1AxV2w5cnh1ZUE9PSIsInZhbHVlIjoiamJwbEI2VG9Wenp6RHdTdjU1T0RENVozTVBXc2ZGaFJGM3psUXRoXC85SVE9IiwibWFjIjoiMTY5N2ZkOWUyNzE0OTczMTk3NjA1MWUzY2YwNDJiOTViYjc5MmQ3MDZhNjhhYWQzZDkzMzY2Y2QzZDE1MTk1OCJ9', 'eyJpdiI6Ik9Ha0NyVGplZXhJZ2RqemZuY3VaQXc9PSIsInZhbHVlIjoiYkVrTStXSHJ0ZEpiUUFqT09reW45Zz09IiwibWFjIjoiZjkwMmRhNzc0MGNlNGUzNDljM2E4NDQ2ZjU3MGI5ZGM0NzIyMjYxMjhkNmZhZjkzNWQxZDg3YTYwOTY1OGE2YSJ9', 'eyJpdiI6Im5XVmRydlJsWEp5VXRsM2xFeEgzNHc9PSIsInZhbHVlIjoieFVBU1RieEJKSFZEMGVUUXdibktNUT09IiwibWFjIjoiMmY4ZTk0YWMwYWQyYzg4OWNiZWEzNjM2OWZhODQwMWE5NjI4ZTUyNjI4MDY4MTc2ODIwNmUyZTU4ZjA1ZTZmOSJ9', 'eyJpdiI6IkF4RkpCSTQ1bnAyRkp6dlJyRGtxMEE9PSIsInZhbHVlIjoiMmJrQnhNOUpyM1U1cmRPRmhOanQ2UT09IiwibWFjIjoiN2E1YWExMDExOWZlNDg0ZGQzZTcxMmYzNDcxNmU3NTM0ZGNjNmI1Y2E5NTc3MzA3MWEwM2NkYjZhNGI0ZGQ0OCJ9', 'eyJpdiI6IkVqYWxZMG90REx5aWRIZkMwNWRYK2c9PSIsInZhbHVlIjoiWGExR0JGMjRPYWtXQUNwRnlcL2o5WUE9PSIsIm1hYyI6IjVhNzFlNDIzNDhhOTFhMzYzOTk4Mzc0MmRjYjc5OTJjZGM4NWE2NDk3OTQ4ZjUwNGEzYWRhYmNjNmZmYjFhYzkifQ==', 'eyJpdiI6InBmdnVGcW4rcDZ4MkpvK0dHY2VvMXc9PSIsInZhbHVlIjoibzRCQ1VDV0phcU1BMnhSKzdOc01LUT09IiwibWFjIjoiNDE2NTM4YjkwMGRhYjcwNjU1M2IxODlmMzUyMjRjZmRlMjBhYTczNDA0MTlkNzVlZDIyZTA1NWY2NGU0YzM2YiJ9', 'eyJpdiI6IlRjR0U2S2lidzZtR3BMd0xvRXk4T1E9PSIsInZhbHVlIjoiMTRkTnNzR1BGbVlLOVBmWUxUYXpwZz09IiwibWFjIjoiYmY0OTdhNTJkM2JhMjM5YTNlNGQ3Y2FkZTNiZjM2NDkwOWZkMWE1MDdiNjhlYmYzZWI5NmQ5MzQxOWRjZjY3NiJ9', 'eyJpdiI6ImNFOFcyVGZ6WndYYjR4VFlaM3h0Vmc9PSIsInZhbHVlIjoiYWF6YVl2dlRCXC9BVWx2U3JPbkp6aUE9PSIsIm1hYyI6IjNkNzJlMmFjNGFkNzBiMDk5N2E3ZmE4NTdiYmVmNTA2YjE0NGYxZGZhZjRlOGZhZjA4ODczNjcxZWJlZmYyNWEifQ==', 'eyJpdiI6IklNUEdvZXZ5QTJBMjBEWU1XZitYNmc9PSIsInZhbHVlIjoiRnptbFZmbStWeHY2QnFDQ0pES3NFZz09IiwibWFjIjoiZGVhMmUzZDYyMjNhOTYwNTEzYmNmMDljZDQ5MzEzY2FhNjNkNGEzYTc0MTg3MDdiZjZjN2VkNTlhMmE1MzQ2OSJ9', 1, NULL, 'EB616F46-E1D8-4434-9052-4AF1B41B263F', 0),
(10, 4, 3, '13.94', '15.63', '2019-02-08 13:10:21', 'eyJpdiI6IjdOOU9NN2NibHE5MnMxMWMyeVdIUmc9PSIsInZhbHVlIjoiYktaWDBMMmNKWW9RT0tNUkhhS3habGI0ajhBODRod1RMako4eEMxUkRDTT0iLCJtYWMiOiIxZjBmZjlmZDBjNzRjYWRmOTU4MGY4NmVlZWZmMDFjOWNmMmRjOTljZDg0ZWEyYzQzMDdjYTkxZWVjYWJiOTBiIn0=', 'eyJpdiI6IllcLzJpUjBVZjE1UlNlZElGM3ltbEhRPT0iLCJ2YWx1ZSI6IjByUkRFWkh3NVwvbW81S1ArUVZKc3V3PT0iLCJtYWMiOiIxOTRlODY0YWRhZjA1NDZiMWUxOTQ3YTY4MTUxNDY3YzY4MzA5YzZlZTE1NmQyMDM0NDU0MGE1NzRkYWVjYzViIn0=', 'eyJpdiI6ImJJY1Z5aE9xSHpodEduZ1JZQXpnT3c9PSIsInZhbHVlIjoiQWc5ZmNtZUlyTFZnMEJacWh5ZDNydz09IiwibWFjIjoiZDNhMzY4ZWJmOGU0ZTQxZWE3YWI5MGM4MjJmYjJjNWE2Y2MyNjY5YzQwZjI5YTE1NmNjZDU2YTRhMjFkYTk5ZSJ9', 'eyJpdiI6Ikc5bHNkbEdVVU5lYVA3YXMxbzA1VUE9PSIsInZhbHVlIjoiTmZ0T1R1UVByNzFERmJhWHVtQUs1QT09IiwibWFjIjoiNmQ1MTJiOGJhMzRjYWE5NTc4NTk3YzhhMTkxYjdlZjFjM2ZiYjMxYTFjYTE4MzFiYWYzMjJmOWNkZjJhOTFkNSJ9', 'eyJpdiI6Im1YVXRHc2FHUU1KMWZzaEFyNnExenc9PSIsInZhbHVlIjoiUCtTdFRcL0xMMm05WHlUUENFWWNocnc9PSIsIm1hYyI6ImMyNmQzZDU5YmMyMjBjZTVlZWE5ZGFlZjdmNmJlYzg0NTM4MDlkMjZkZmZkYmJmYTNmNjVmYmQzNDYxYmUzNTUifQ==', 'eyJpdiI6Indqb1QzREZwK05Gb0E0VUVzTXNTbUE9PSIsInZhbHVlIjoibUZteDZqaytHTStmWFBlTWwxd0tcL3c9PSIsIm1hYyI6ImM1MzljYWFkOWY4MjA5MmFkMzM5YzBlN2E2NzkzODk5NWFjMzc0OWJlN2JlMTM2NTVmNTIzZGZjNWUwZGJmMDQifQ==', 'eyJpdiI6ImVlbFNENlFJd3FiTEpOd01tVzk3cFE9PSIsInZhbHVlIjoiUGNJWHR0YXpxQWpmTVdPcTAwWmg3Zz09IiwibWFjIjoiZjVmMmY3NDE1YzQwYjVkOWY5YWYxMmJlOGY0YTZkZTAzMTViMjU5NGJjMWEwNDBmNTc4ODBiZmI5YjcyOTUwZSJ9', 'eyJpdiI6InhHWlU0cWFtOVhqazhGamxmSjE5UUE9PSIsInZhbHVlIjoiRFdQUGJRQmlITCtKTVFSbmRuSTFNQT09IiwibWFjIjoiZDhkZTQ0NmNkNDUwOTA1OWFlNjg2MTQ2N2E1NWQ3Yzg5YjUxZWJjMzVjOGYzNjhiOWNjZTJhYmY5OGM2MWJhNiJ9', 'eyJpdiI6Im50bUNBVWVRRUlOaXlNVVFpTnN0Q3c9PSIsInZhbHVlIjoibUVEMkRlQkV3RkFveThsbW5sK2lZdz09IiwibWFjIjoiYjczMDM5MTZmMjFmNTE2MWJiNjdlMDczMjQxNWE4YWFhNzE0Y2FiMzBjNjU2NTQ5OGIxNmMyNGRjYTc4ZjMyNCJ9', 1, NULL, '', 1),
(11, 4, 3, '14.85', '15.57', '2019-02-08 13:11:37', 'eyJpdiI6IlM0R3FPTTN0MWFIRUU4blwvMW9CU0JnPT0iLCJ2YWx1ZSI6ImQ1ZDhrZzJYMDlJVVZhXC82bjZQV0E5bDE3SU1KRlhHd3lLcVdIVDRpMjNRPSIsIm1hYyI6IjI5MTA0Y2Q4NjlkOGVkYmYyMDIzZWQ0NmViNzU5MmM4ZjQ2NzBlMjlmMzg0MzEwOGIwMWFlZmExZjUzYjE3MmMifQ==', 'eyJpdiI6Ikd5UVR1QnRDQTZPNlAzMit3REYwZHc9PSIsInZhbHVlIjoiS01SQzZ1NXZ5SmdOWTVXWDJIRkZmbmVQMFJ3V2tKZk53TFZZVjZFYjJGXC9mUkZKN1pucFplbXl0VFFTU2FWcnciLCJtYWMiOiI0ZDI1NzZmNGFiNDc4NWUwZTQ4MjIwYjc0NzFhMDNkMzdlZTYzMDE1ZGIyOTkwMWNhZGRlYWY3ZmFjZjVlZjc5In0=', 'eyJpdiI6IlFSa0pGaHRHZkJvK3M2UkRXMEZBZmc9PSIsInZhbHVlIjoiNTJXMGo3K09jaTFUUmRDQlF3YXNJQT09IiwibWFjIjoiNWNmNzExNjcwNjRjYmJjNzk4Y2NkYmVlMGFhYzE4NWZlNmQ2N2UwYTAzYWIxNTFkMmE0NDg0N2Q4Mjg1OTljMyJ9', 'eyJpdiI6IktMQSsrQmE1Zkt3Q3RWdlpcL0lCTTJRPT0iLCJ2YWx1ZSI6ImIydXZkYkE3YUVwVTc1YXNNNG1DcWxEWDhKazVSY0FWKzdzVENoalFGWnc9IiwibWFjIjoiZjY0ZTAyZDA1OWI4MTUwMzhmMWIwODUwZWJiODEyYWM2Y2JhMDBkMWU3MWRmYThhOWU3NmRlZjA2MWEwNjYyNSJ9', 'eyJpdiI6Ik1yVEZtNTBUVEJWbExaTTdReWxMN0E9PSIsInZhbHVlIjoiT0Q3b3FjVE83MExMNVpqMUUyeFdiQT09IiwibWFjIjoiYzJjNTk4OTFlZGJmNTA2ZTg4MGZlMDdjMTgxODlkZDkyNWI2NjVjYjMzNWMzNmYwYzhiZjg3NmExOGE1ZWIwZiJ9', 'eyJpdiI6IjZ4WUxuelc0TDFtT3lBR3hnT2ZpS0E9PSIsInZhbHVlIjoiRjM4Qnh0VEZqeEczbTZjTlZFeWF5dz09IiwibWFjIjoiMmMwNjgxMDc4MWQyMWY3NmRhMDY5MmFlMmU0Yzk1YWY4YTUyZWZmMzFjMTYyZDA5ZTQ0ODI1MWMyZjI5YzZjOSJ9', 'eyJpdiI6IkZZWDdGc28yVERTMFVHRVhpa1wvS1wvQT09IiwidmFsdWUiOiIxSjNrd2FVM05FVFdzZktWZEpDXC9cL1E9PSIsIm1hYyI6ImNiZWEzNWY4ZjliMWVmMjFkYjRhMTQxMDU5YTg1MThmODM2ZDY5ZmMzNjQyOWVkY2Y3MDBjYWM3M2I2NmJjMWQifQ==', 'eyJpdiI6IllnQk1ycWV3S3dcL2FROTQyZ2FcL1c0Zz09IiwidmFsdWUiOiIzZjM0V052QW9RQzNSNTVYTFNWMzhFNWo1ZjJFTkVqcVRJZWh6OERPZGdNPSIsIm1hYyI6ImUzMjJmZTBjNmY4MjEzYjU4NWJkMTkwMWM5MjM3OWI1OWE5NWE4N2U1YzAzOWY0OWEzMzQ5ZmRiYWQwZmY4ZjcifQ==', 'eyJpdiI6IjVzak5EakcyaTZwNnl0cWVGNmdLMEE9PSIsInZhbHVlIjoiTFwvMW02UkVrVmxPUG1mdlZiVVwvdUhnPT0iLCJtYWMiOiI1ZTAyZWVhMGVlZmNhOGVjOTczMGU2NjBiZDdlMWZiZDY2YTk5ZTVlZTlhNDM2N2ZlNjQ0NGVhZTM0ODIxMGZiIn0=', 1, NULL, '', 1),
(12, 4, 3, '55.98', '58.77', '2019-02-08 13:18:18', 'eyJpdiI6Ikxqd2o4U1VRc2g5SmsxSzVEc0J6NGc9PSIsInZhbHVlIjoiNXlIRkRPMUhhMVwvVDZkQU84NnRZNDNBOU1cL0dJVm1kK1RWOTNYVHV6NkIwPSIsIm1hYyI6ImM1ZTk1NjI2OTU5OWY3YTUwZWY1YWU0MWU1OTNiNWEwMTk0NTBkNzJmMTlmYmNhOWFhNzQ4OGVjNTc5NzQzNmYifQ==', 'eyJpdiI6Im9BVjhKaFhJMTd1eEgyXC9cLzhNOHZtUT09IiwidmFsdWUiOiJoNXpwdnp2WnVOYjdJR3pQVm5WcFlWVzRCSktMR21FMWd0dkEwMVplVlplMStpSTlsSU9XM0V1M3lHVHhrdTVHIiwibWFjIjoiZmNiNWY3NjFiZTYyMzFjMGQzMmIzODVlZjRlYTY1MGY1N2Y1NTMzOTExNTEzZmFiNWI5ZGY5NzJlZGZhYzVkOSJ9', 'eyJpdiI6InJ3MlBtY3Rtdjl6VEpDV2FobENnSkE9PSIsInZhbHVlIjoidE1nV3RMVEJudzZnT2dMZ0RmZ0hDUT09IiwibWFjIjoiZjFhNjIzYmFlZjA3YjEzZjVjNmMwMmY0ZWYxOGY2NDU4NjI4MDA4YmU5YjkyOTM5ZDJhZGNjYzU5N2I0NzM1NiJ9', 'eyJpdiI6IkZpMzBpejkxSjV5OUZ4dk53Z1Q1cFE9PSIsInZhbHVlIjoiR3BtdytrcmlJS0hMU1NZNkdob1dpTU9mQno4dDZaWTBjQzMxb2pnVUpVaz0iLCJtYWMiOiJkMTk5NWY3NzFlYTVjYjJhOTg0MjNmMmY4MWY1ZThiOTEyZDZmMjcwZTk3OWZiY2VhMjMzYzlhYTE0NjEzOWIwIn0=', 'eyJpdiI6IlNDdlJ3MFJFSlZYMFEzaVArUTJwb0E9PSIsInZhbHVlIjoiekR0MTR4WFphbmNHckNJK0NZNnp3QT09IiwibWFjIjoiMTVkZTQ0ZmFjZmUxZmQyOGIwNDIzY2E0NDQzMzE5NjQxNDdkMzY3MzEwMzc1YjA0MWUwZTRjZWQxMGYwNmRhNyJ9', 'eyJpdiI6ImhYSVhGSVBVcEtJQ284cExERyt6enc9PSIsInZhbHVlIjoiR0JvRWJGY1N5a1JyaTZjalpyYVF4Zz09IiwibWFjIjoiOTE3MWQ0Mzg4ZDhjNTViMzZmNDZmZmVlMTJmMzg5NDJjM2EzNWExMTZhZDU2OGU5MjMxNmM3MGQ5ODg0OTFjNiJ9', 'eyJpdiI6IlhIcTF2dnRCSlUxNzFBXC9La3pUcjlnPT0iLCJ2YWx1ZSI6IkRVdnJteUJXZjNKcVNEY3VSZVZIMXc9PSIsIm1hYyI6ImQ0ZTM4ZDgyN2I0YWJlMWI2NDVlMDA0ZDE0M2M4NWI4ZjEwNzA3MWEzOTU3Njg3NDk0YTc0OTRiMmM4MDg4ODYifQ==', 'eyJpdiI6IlpEYk1CN1JqM1ZFOWxmVmRoQ0FcL0pBPT0iLCJ2YWx1ZSI6ImdYaDNHaFByVEd0ZmlWZGhsSVwveUNTd3JzcXZ0Q1RGckhiT09nZ3VVT1V3PSIsIm1hYyI6IjIxYTcwODIxYjE2NjAyMWIxYTBhYzY1MDQ1ZjVhY2IyODU0Njc3OTVjNjNlYzYyN2E3OWFhYmI0YzI0MzY2MzIifQ==', 'eyJpdiI6IjFhR3BRR1RWVjUrdjJmTld1WHBnQ1E9PSIsInZhbHVlIjoidWl1VUFkSG4wWDBXcEc0SHZoc2tSZz09IiwibWFjIjoiMTkzMzRiZjA0OGIxM2MxMzExZWFiZTEzOTRjNzc5ZDBjODNjYzBlZTZjY2E2YmI0MTA3OWNjYmFjYjZlYjlhNyJ9', 1, NULL, '', 1),
(13, 5, 3, '1326.94', '1326.94', '2019-02-10 02:18:27', 'eyJpdiI6Ik5BY0ZqZ2JwZDE5dDFXVGlxZlJYOVE9PSIsInZhbHVlIjoiVTVcL1k2MkFcL2d6VlJLNVBWRWk4WHZpajRpdzIrbGZDc0g0cm10VUdwR0cwPSIsIm1hYyI6IjFmZmNhNzdmMTkyY2UxMGUxNjlmNTk5ZDA0NDVmMTFiOTc0YzdlOWU1ZTRiNDIwYzEzZDIwNTNiYWRkMDBkYzEifQ==', 'eyJpdiI6IlwvU3hLUjQxeWIwbHhkRld4VU1pMHpnPT0iLCJ2YWx1ZSI6Ik9kWkdjOFN4SmowRVNjajRMK2V1VExpbVFIQkpvYXU1UzVTQVlRN1BYWEU9IiwibWFjIjoiMmQyZTZkZTJlZThmMmMyMWE1MWQwOTk2NWNhNjE3OWE4MDg2MjExZmEyOTY3NmU3Nzg1MTBhYWFjZWRkMDJhMyJ9', 'eyJpdiI6Ilh6K3d0clpqV3kzc3VydmtpNVRMV2c9PSIsInZhbHVlIjoiZmZWTUU1RmV1eGtOZE5WVlJ2eTF6Zz09IiwibWFjIjoiNmY1MWEwZmNlZGFkNTBkYWY4NTA5OTQ5MTFjMjBkNDMzYTdiMDc0M2U3NWJhN2YxYTZkOTVmMmYwMGNhMGE5YiJ9', 'eyJpdiI6Iis5VUg0cVZ0Yk81MEtZUmdjRU83RlE9PSIsInZhbHVlIjoiN0J2RmJRNnprY0JqQWlhcUNsUFRWRlN6NGF1em9WK2R4TjBqTmd2OGVGND0iLCJtYWMiOiI1NDVlZTdmZTViYjM1N2M2MGVjNDRhZmVkMDllMjM2NzJjOWVlN2VhODdiYTcwOGM1MDVjNjVkNmIxMDhkOTllIn0=', 'eyJpdiI6InhhdGF4UDhJRVhGTllNTmZzXC9rS2V3PT0iLCJ2YWx1ZSI6InlqM2ZXWUxhdVl3a09zNHQ3enEwRFE9PSIsIm1hYyI6ImMwODk0ZjcwMGE5YWVjNzU1MGI0NWMwZTM4NTY2ZmRiNzg2NTgyMjRkMGYzNzExZTY0MzVjZjMwMmNmZTRjYTQifQ==', 'eyJpdiI6IkdIT2lZU0VCSUVQNlNpYmN4XC8xWUpnPT0iLCJ2YWx1ZSI6InA4eWQza1FxOVdCMzEzcEhISkNhcEE9PSIsIm1hYyI6ImMzOWFiMjMzYTE3YzAyYzhhNmNlZTEwZDdhMjVmNDhlMWE5ZDQ0NDQ2NmRmMWJhYmY2Mzg1MGUwYTdhMTRkODAifQ==', 'eyJpdiI6IlhEMURiZ2FXRHhlSjN4UWhNTTFxblE9PSIsInZhbHVlIjoic2J5VVVTVGkyVERMZjhQSm1Cb05FZz09IiwibWFjIjoiNzRhZDdmNDJlN2ViN2Q0YmE5MTRlMjllYmJkMmY4ODhjOGVhODAyMmFkZTgxMGY1ZTQxMmVjMWU4ZmI2YTA5NyJ9', 'eyJpdiI6InM1a2NONzRtRzJ3c3duY1JaWFwvc01RPT0iLCJ2YWx1ZSI6InRsdjR2ZHM4bjY0V1o0dXBtXC9LV053MmdxRWhCSCtQWTczS3AxT1lGRUdjPSIsIm1hYyI6ImJkYmRhNGJhNzNiYTVkMDNjYjE0ZWZkMTUxNDM3NmYxMTMyOThmMWY4ZWU3YWVhYTA1ZDM1NzVmNjQxOTgzYjIifQ==', 'eyJpdiI6IkUxTVllU2dYK3I3MnVTUzY3WG5LVlE9PSIsInZhbHVlIjoiVkhFQWNEU3Z5RUpUSDhleVR3OTRoQT09IiwibWFjIjoiNzNhNjM0MDZiOWVjZTVhOTU4ODAxNDdmMDE2NTM5ZGQyOGI5NmE0NTkzN2QxZDg4MDFlYTE4ZjdlYTgyMjIwNCJ9', 1, NULL, 'D5591E70-07B4-4355-AB2D-92EDD1416449', 0),
(14, 5, 3, '1326.94', '1326.94', '2019-02-10 02:26:29', 'eyJpdiI6IkxlUnZYXC9iRzQxQnUyYkZCWWVzd1dnPT0iLCJ2YWx1ZSI6IlRJVlZyUG51ZThOcG00UDI1bklGZ21VZGJyOU51OWVSSkhwYkp1KzdRRDQ9IiwibWFjIjoiZDA2ZWQ5ZmExZmI5NWEwNzVjMmY4NzJlMWM3ZDYzODg1MTdmMTk0Zjc1N2U3ODc5ZWZlZjI4MWUyYmUyZjMxMCJ9', 'eyJpdiI6IlBvbjhzUVJ4S0ZPUG16R3BmNE5vTEE9PSIsInZhbHVlIjoiTXVOTGFUbVZDMlI3NEprZDY4SnZVbTlJZU1qRVRHWFNuT1BtZ2tUSVwvS2c9IiwibWFjIjoiNDljNDI0Y2Y1YjU3NTgwZWVlNDM3Yzg0NTBmMmUwYTExNTY5NWNiNzNkYWQxZGVhZGQzOWVhMTA1ZmE4M2QyMiJ9', 'eyJpdiI6Ilh2UmVMM3JzWHhNcVlcL2kwN2RVNGdBPT0iLCJ2YWx1ZSI6IlNpN2U2SENlejlCVHlVTUYwWjJyNFE9PSIsIm1hYyI6ImQ3Nzc5NDM4YWZjNzIxODY0N2NjMDQ5ZTQxM2MwYWM5MjMxYzk1YWIzMzIzOGY5ZDhmNWJkNTg1ZmY0OWVlMDMifQ==', 'eyJpdiI6ImR0UnhDaVA5UXFYS0xqNWdBOVwvT3JnPT0iLCJ2YWx1ZSI6IjhvSndNU29hT1VSMXhjTmhLN3V6TDhKenlqeFh2YUVVTTBRQ0tVWDdsVDQ9IiwibWFjIjoiODIwZjFlYmVmYzhjYTcwZWUzMmVmMTZmNTU3NjU3NWUzMzIxOGQ2ODUwZmQ5Zjc1MzRhYmRhMjdiYjY0Y2U0MSJ9', 'eyJpdiI6InRVWW9BdFFxUm9ZOVBuaG9MckZLc2c9PSIsInZhbHVlIjoidkJ5U0tsMG0waWUxQ3Y0eEhjKzFEQT09IiwibWFjIjoiZWUyNDQxMGJkNjllODBlNWQxY2UxNzI1OThmODYxZDdmZjU1MGUzZDIxNmIzNzBkYmI0NTkyZmQ3MWI4YTI2MCJ9', 'eyJpdiI6InJWUlNmek9uWHh4Mk5xeURVaDNYZ1E9PSIsInZhbHVlIjoib1wvZWNubllVV25pRStNQ3RxSGo3bGc9PSIsIm1hYyI6IjIxYzU0N2Q5YmM2ZmE0ZDhiOTM1N2E3NDYzY2RlYjQ0ZmY2NjhjNjBjOTM4NWI0NzI4YjhmZDkzNmMyZWUxZWMifQ==', 'eyJpdiI6IkR0c2tWbk9BNzdMWVA1cUhjUUFLNEE9PSIsInZhbHVlIjoiNHhXbmhtVUR0Q05WalhST2Z2UlRRZz09IiwibWFjIjoiZjczMWQ3OTA1NmVmMjllZjViYmU3NmEwNTE2OTMzMWFiYjBmZTgxYjMyNjJiOTVkMzE3YWYwNmQ1NThmZjBkMSJ9', 'eyJpdiI6ImZlSXFpXC9GVGVyaDJKU2RPUjFsXC9Odz09IiwidmFsdWUiOiJINlVLam5EczlpKytMMUZuOFpuVjJTTFoxaUc3dFNBTWtcL0tRYVVTbUp6cz0iLCJtYWMiOiJhYzJhYTcyNjljOWRiZWY4MDRmNGYwNDY4OTM0N2U0NWZiMTc2NTkwMzRkZTA5YWI4MDY2Y2ZiNjI4NzRlMDU0In0=', 'eyJpdiI6IkMyR2xzSmlJNDlNcWF0WHNPc3Y1Tnc9PSIsInZhbHVlIjoiRVQwWUxtQnM2OWJqOTBjN3JyajlSZz09IiwibWFjIjoiNTM1YTk4MGI1NTUwODMyNmQ2YzIzNjU4MDdhNjFlZWJjNTBkMmUwZThjZTViOWYyNGNmN2Y1MTcwYjEwNGMxNiJ9', 1, NULL, 'C851785C-4CDA-4615-A933-EA2A3605AFF2', 0),
(15, 5, 3, '1326.94', '1326.94', '2019-02-10 02:28:41', 'eyJpdiI6InphaG01cCtpSmFLTlBJZDJ3Q2NYK0E9PSIsInZhbHVlIjoia3NuQXlBbU5WaWxjMkFSXC96VnNIZ2QxNkx5anorK0N2QlpzdUpyN1k1Q2s9IiwibWFjIjoiNjczYzcwZWY0YzJkYjYzMWZiYmFmMDg1ZWYxN2IzZjM5ZjU5NzIxN2U2OTE4NmZjNzQ3ZDlmNWUwM2ExYmM0ZSJ9', 'eyJpdiI6InFtTnZ1cWtRVnhMaFhUK3dhY1lGZlE9PSIsInZhbHVlIjoiUGRcLzdtN1wvVEJma3VHbTNhWE85Q3ZYK2RDTkVFQ1NhcFpGRFliOFwvR0l4QT0iLCJtYWMiOiJhOWFlNzYxODY1ZWI3ZjNmZTY3ODFmNzEwZTNkOWVkZjJjNzIyYjkxMDE0M2RkZjkyMTJlOTZlNThjODQ4MWFkIn0=', 'eyJpdiI6Im1VUHk2bGpqckEyUlF5Q2J4T05VaEE9PSIsInZhbHVlIjoiU2dvYlQyb016SVo2dlhJbnlibkI3QT09IiwibWFjIjoiZmU0OWZjODEyYTFiNzRiOTkwMjY3OWU0YWE3NGExYTgxOWI0MDE4ZDg1OWM1NDA5OWU3MzJjMTRlODA2MzE4ZCJ9', 'eyJpdiI6IlNhM25VWHlaWDJtXC8ySHg5NkR4ZjRRPT0iLCJ2YWx1ZSI6InFBNlJsZUxTXC8zb0JjMUxxam1uVTNQZHJnUWN4QkQ2bjZzUnZUVnpCZFFVPSIsIm1hYyI6IjZlM2ZlYTBhOGVjMjdiYzE0MjQzYzIwODg2MTI3ZjMyYzYzYjcxOGE3N2U4NDg4Y2RjOTZhOWQwZDAyN2VlNzcifQ==', 'eyJpdiI6Ikw1ZGFcL2MrT2pPZzN2MVJwRHRwV3hBPT0iLCJ2YWx1ZSI6IjYrYXV1QkExYXJRRDMxUmR2MDVTT0E9PSIsIm1hYyI6IjgwYzQwZDdjM2IzNmI0YzlhMGZiMzYwOTljNDkzMzU4ZmIyNDA2Y2UwZjJjMDgxYWMzMzRlMjk5MTA2NTY2YTAifQ==', 'eyJpdiI6Ik5nRkluTjVIcXVIeWY5TDNqMlpvR3c9PSIsInZhbHVlIjoiQUxmYzlHUDEweXg5VUdWc2xOYVNjUT09IiwibWFjIjoiMmQxYmY0YTlhY2Q3N2NiYjhhMTU3NTcxMzU2ODgxNzAxZWZkZmU1N2E5YjQ2MDI0YjczZjg5YmE2YzE4NWRkYSJ9', 'eyJpdiI6ImhreFl4OHRqWDJLais5UExPMk9rZEE9PSIsInZhbHVlIjoiV0NKWGFwR0w1dmtzWlFyMUZBNklhQT09IiwibWFjIjoiMTYxNmNlMmM0ZjI2ZDc1MjRmNTAwYjNiOTY0NjQwMmE2MTg3NmUwMjRlYWExYjRkMjBkNTUwZDNlMTRiZmQzYSJ9', 'eyJpdiI6ImNsajRpcm1XM09lR3FWVjc4UERod2c9PSIsInZhbHVlIjoiTzI5MitnOE9mZWo1NG42bFNkSlc5bEFaY2J6ejBNT3ZNakg5bWpvYXdFWT0iLCJtYWMiOiJlNjFmZTUyYWIzNmNlZGFhNjkyNzIyODlkOWRmOWU1ZTA4NTkxZjNmNGMxMjZhNWNjMTVmZDcxN2U5MmQ3Y2U5In0=', 'eyJpdiI6IlZaRlZITVNubU5UdldtWEVRcjJnZHc9PSIsInZhbHVlIjoicDI1bmxNMktHOExmbThUbkR5Nk9uQT09IiwibWFjIjoiMGJiNzVkYjA1YTRhMmRlYzNlOGQzMmE1ZTQyMzdkYmIzYmZhZDU3N2Y1NDNjYzJiMGE0MjcyNzA0NTYyMDJlNCJ9', 1, NULL, '5CB79A6A-7A65-49AA-B29F-1D658833A9C1', 0),
(16, 5, 3, '1326.94', '1326.94', '2019-02-10 02:30:33', 'eyJpdiI6ImxOSERJUnlWdk1vM2cwa0t4MUc1M2c9PSIsInZhbHVlIjoiNnNaa3JRdXFycHZKbCtcL0FtZVZHN2VzN2FTTmhXSVcxSWVraU5KSUQ3U2M9IiwibWFjIjoiMTEzNzljN2U5OWY0OGUzODdmNDk3NjNmZTMyZThhOGU4MzAzMmRkNjM5ZjU1NTBhYmU2M2QyMWJhMzM5NjIwMCJ9', 'eyJpdiI6IkFldlRRejlydVhxVEU5VEhoa2hYdlE9PSIsInZhbHVlIjoiNVdXODRrQWxXZVBzVjlmaG51dHY1cnNFM3RUYzdpVUNkSlhoVzhiTklwVT0iLCJtYWMiOiJkMjFkZDc3OTVkYjhkZDIyMWM2OGI5MTE3NTFmOGY1OWNkZWU3NmExZjQxNzNlY2ZkZDU3MWZkMzk4MDIwNTllIn0=', 'eyJpdiI6InVNMXBCR1ZkVnpCbk1nSThGVHdvbHc9PSIsInZhbHVlIjoiN1JucEhJQ3QzNDFHUjRVOXRmckdUUT09IiwibWFjIjoiZDM1YmE2NjJlODRiOGM5Y2RjZmU1NDM3ODkxMGY4ZDdlNjNhNGQ2ODdiYjA4YTVmNDkzNzAxYzU0ZTU5MjljNiJ9', 'eyJpdiI6IlJhQkFzcTZxclZRcVdLN1BFUXFsZGc9PSIsInZhbHVlIjoiOUJpVnRXSkhiTXB4QnFveEl6cWUrZ2tYaG5MQ3p6TjJLVHY1Y2N2R1FjRT0iLCJtYWMiOiIzMjQxZWU5NjU2YWRmYjk4MDM0NTkzOGFkNGViZmMwYmEyZmExNzM4MDc0ZTUwMmRjMzU3YTQ1Yjg2OTVjNmYzIn0=', 'eyJpdiI6Im8yQ0dmWm1GNDhld3hpVnAyckRzd1E9PSIsInZhbHVlIjoiTVdxV1dhemg4SkNIaWNTcHl0ZW1sUT09IiwibWFjIjoiOGY5ODEzMDM3NTRhYTgwNWU5ZDg2ZmY1YTc1YzljYWUwNzllZDBkMzIxOGNhYWM0N2ZiMDY2NGVjYTFjYTYzYyJ9', 'eyJpdiI6IlVUTEJtckJrc2JRSGh1UStXVWY0bXc9PSIsInZhbHVlIjoiRnQ1S1ZYd1NmMTRaNm9tQjBraTdqUT09IiwibWFjIjoiZjI2NzdmMmExYzRjYWEwN2FhZWVhNWM5YzZlYTAyZDliMWEwNDdlNmJiNDZiYWI2MDM2MmM3ZmVmMzQ1MDhhZiJ9', 'eyJpdiI6IlUyNXhYVFR0NnBXY0FYb2tGc1NMZlE9PSIsInZhbHVlIjoiUGYrNURDWU9wbTBmaW1jcVptaitrUT09IiwibWFjIjoiYTA2YzJmZDY5ZGY5Y2VlOTg2MTc2NTEwMzc0NWQ1MGUyYjI2OGFkYmMwZDdhMjEwYjY3NDgxYWQwNGFiYzMxMCJ9', 'eyJpdiI6Imt0OE5oYWxPUndKc3RyQUlUMzdjOFE9PSIsInZhbHVlIjoiRGY4bElkOTFUUFF5OTlBbjB5N0dhcTRJTHdKSUpvbDczRTZYTVFpN2pHdz0iLCJtYWMiOiI4MmM3MWE0MjkwOWMyMmEwMGQ1ZDRiNjA3YzFjZWVlZmE5ZTJkZThkNDU4OTYxZjBmMjA3NDhmYzdhNDllODllIn0=', 'eyJpdiI6IkFIbitUUUc0aHdNSXBcL21aSDcyaThRPT0iLCJ2YWx1ZSI6InIyVHlSQVZFb3c0VjB4SlVjYUc0d2c9PSIsIm1hYyI6ImU2MDRjYTk3ODYzNjZmNDhmZjU2ZjI5Zjg5ZDEzMjI4Mzc2ZmM4OWNlYzZhMjBlY2M2MjEyOGE4MWRlNzk1MTUifQ==', 1, NULL, '387C1EA8-F34A-48AE-8AEC-CAF32AD50D1D', 0),
(17, 5, 3, '1326.94', '1326.94', '2019-02-10 02:31:48', 'eyJpdiI6IldPRlNCakFLaFF0MzNiY2VSYXZGWVE9PSIsInZhbHVlIjoienNYQ1AwUTVxTUx5VVwvWE9SaVVWbklEVWdQdzB1a04zQzZjaWd1RjVBNk09IiwibWFjIjoiNzk5NTUwMGQxMWVlZjQ2OWZlMDg1ODJjMGY0YzlhMWYwNmIxMGI4NjA4NTM1OGVlNGRjZTgyNjBhOGRiOWY2MyJ9', 'eyJpdiI6InlMQ1J1ZEJnSGtIQWw1NU4zTEtsQnc9PSIsInZhbHVlIjoiM1FieGZTbUFTVGFHQTY3ZWFOVHJTbGtXQ3ljV0krNXplTVR0VGhyRVdscz0iLCJtYWMiOiI5MWQ3YmQxNzEyMTFhZmM4NDYxZWU3M2UyMmNmNjFjMmE2MTBmM2Y0MDNlN2ZlYWNhODdmMjU2YWZjZjk2YTk1In0=', 'eyJpdiI6IkVlcmxoemhGSzUyY05MamsrYjRhUHc9PSIsInZhbHVlIjoiY3NtY2YxYXhwQ0dYTSt1bG9zZmtiUT09IiwibWFjIjoiYjgxMzM4ZWYzZGM3YmYyNTFiYThjMDYyZmY5YjVlY2UwYmQwMGM2NWE0MGJjYThiMDIyNjcxZjljY2FiN2Y5YyJ9', 'eyJpdiI6IjJISTA1RHU3R2V4THJTbU1teElMZUE9PSIsInZhbHVlIjoibmpzTFZ2dkRZODROdEVINkJLRGJPYklVSTllQ3VGamhCM3ZPVVkrS2dvbz0iLCJtYWMiOiI0YmMwNjAzNmZmZmRmNDQ2NTQzN2ZhMmY5ZGY4OGNkYWY2MWExNTIxODJmNTI0NGY3MjZmYWFkYTE1ZDRjZDg2In0=', 'eyJpdiI6Imh2T1c4THpXMHVCQVZ3c09iU3RIRFE9PSIsInZhbHVlIjoiOXdmTFk0Vm1aSHBJRktXdGJNclVadz09IiwibWFjIjoiZDFkZDc3NzhhNDQ5OWZiYzQ1MTE5MTU5NzA4ZTk2NmZhZmM2NzM3OTViN2U2NDU5Y2UxNDMxMzNkM2UzNjRkNiJ9', 'eyJpdiI6IkNzSXRKbFdDSk9nbFY2YUUxY0F4akE9PSIsInZhbHVlIjoiNFlHMG84clVyMWc4WWRKTlpjUlBjQT09IiwibWFjIjoiOGU4NjBlZDdhZWY2MGYwMDMyMTU4ZDc4ZGRkYTA3MTEyN2I5YzgzNTI3NzA4ZjUzYWQxOTJiMGI2YjFjYzg2NyJ9', 'eyJpdiI6IkFFQTZ0WjBBczRoRDBXcW1LOEE5MFE9PSIsInZhbHVlIjoibjFrRkhqNGp3WGsxZHl2YTgyMkg5QT09IiwibWFjIjoiODM1MGNmYmNmMTU5ZTYxMTY2Y2QwZDg0MTJmMmViOGM1ZTZlYTQ4ZmQ0ODkxNTNhOWQ3NThlMGYyNGQ4OGQ1MyJ9', 'eyJpdiI6Im41elpPU2NxU1E2ajcrY1FDMmtqeGc9PSIsInZhbHVlIjoiVEMwVWthMWhMZGdOemZUTlpsaEoyQ1Rvc3pMcDhTUUlyZEs0dWVSKzZNYz0iLCJtYWMiOiI0MDFmMWNiM2FhZDZkMDk3MWVkZjgzZTcyZWE1YjBmOTViODNmMDFiNzdhZTYzNmM1NmQ1ZTlhYTkwNjA5M2NjIn0=', 'eyJpdiI6IjQ4ZitXckpJXC9aOU83VHJJckpUUVwvQT09IiwidmFsdWUiOiI1dXZXTlJ0bFJqVWthVHhobnNKd0NBPT0iLCJtYWMiOiJlMmY4YzFmMTQ1Y2QwMjc3ZThlOTQ4Nzg2MDZlZmQ5MzcyMGRhM2E5ZGVkNGI2MWJjZGNhMWU2ZGE3YTZhZmRjIn0=', 1, NULL, 'B2798CB7-18F6-41C3-AE6B-4943C74AA634', 0),
(18, 5, 3, '1326.94', '1326.94', '2019-02-10 02:31:53', 'eyJpdiI6IllqcUJmdlluTnJUakZuU1U3akZHZHc9PSIsInZhbHVlIjoiZ2M1dWhhZHdNdks0Zkxjd3U3bHVraVhOY01aTGxCbzlzXC9iVjN4VXFrdXc9IiwibWFjIjoiZmMzM2ZjMGI4ZGVlMGM2YTgzMWQyOWYwOGQ4Yzc3NzJiM2E0NjY4MmNiMzU2MWViYzA4ZDJkNjJkMzkwNWE1ZSJ9', 'eyJpdiI6InJCb3BlUzRtMkxReEpSNjVxcU9GdGc9PSIsInZhbHVlIjoiZHlXR0FMSjdoNDI1bCsyU1k2dTVcL3FMUjdOcjEySlRSNlh4dVpOOHlpekU9IiwibWFjIjoiZTI4MmQwN2RkYzg1MTEzOGZkYmFlMDBjMDAwZWYwYzI5NTg2NDQxZjg3Y2Q0ZDBkZmY2YTdjZWI1OTZjZWZkZCJ9', 'eyJpdiI6InRRcnlpXC9kQ2ZzU3Q0VlhLMzJJY2JRPT0iLCJ2YWx1ZSI6IlwvTmdqVHloS3d1cFFtXC9HdXI5eXhaUT09IiwibWFjIjoiZjEyOTIyYTA4MjBhNzE1NWRhM2JlZmY4OTVlZGVhZjgwODkwMmFkZGEwZDFjMGEyMjkzYmE5OTM4NWUzYTJhYSJ9', 'eyJpdiI6IkRUbnpMZTVhWDJ1MFh6UHQwZTZualE9PSIsInZhbHVlIjoidlk5U29jVmFaRzF5d1lSYk9VTG13ZCtLYklXVVVjdzgxSnlpc1dcLzNvVmM9IiwibWFjIjoiMDAwNGZmMDg1M2FkZTRjN2ViMzAxNzYzOGY1NzhjMTg2Mjc0NDgzYTcwZGExOTQzYmNiNWNiMmQ0ZDU3YWQ1MiJ9', 'eyJpdiI6ImR6M0thMWp4OUFhZGliTWhVUzk3bWc9PSIsInZhbHVlIjoidlMrQkZEakFJMEZjeWsreGZTd3BIdz09IiwibWFjIjoiZjMzZTAzYjg0ZTRhOWRkZmE2M2IwMGU1ZjZiYTlhZDYzZjQ0ODY2YzcxNTU5MzMzZDBjYjZlNmMxOWU0NWQzZSJ9', 'eyJpdiI6ImlZcnk2T0VUZmRxaWtFbldsbkp5OXc9PSIsInZhbHVlIjoiYUZRMmVXcDVDVVZZNTNoZHJmanp5Zz09IiwibWFjIjoiNjRmMWQwN2EwOThlOWI2Mjk1NjJmOWJkYmQ2YzQ3NGRiM2E1ZDY5N2QwZjE3NGUwNzhhMDM4M2QzYzk1ZGU0NCJ9', 'eyJpdiI6ImphdFVJRFwvZEFGUE8xSk43Z2w1WDd3PT0iLCJ2YWx1ZSI6Ik9CcmtFTDdITkJFdTMyXC9kNWdzVFhRPT0iLCJtYWMiOiJlNGI5N2RjOTliNjk0ZDcwYjJlNDU3N2JjNjU3NWNjMWZkYzEzYWQ3NzRjNmRlZThjNmJkMWQ5OTFiYjM2MDcwIn0=', 'eyJpdiI6ImhcL3NSSWtWSnQxZUpEV1lCYmc0M1NBPT0iLCJ2YWx1ZSI6IlREQVwvdGVta29Kb0xPXC8xZVdyTStsR05oSGZUcSt3QkVkQXE5ZkZ2UTFSUT0iLCJtYWMiOiI3ZTdiMDdkMDJlYzU1YzhjY2U1NDMyOTE4NTA0NWU5OWIzZDg5OWE3ZWRjYjhmNzM0ODc5MGIyZDUyOTJiZDQ2In0=', 'eyJpdiI6IkJQSU1DenFGeHRVM05MNTkyYnBxenc9PSIsInZhbHVlIjoiU2VmYVpIUWxlWkk3YzRLbGVwcjRtUT09IiwibWFjIjoiZjEzZGIyMzIwOTE1ZGJmYzJiNDM1YzU0MDAyNWE0YzRmMWZjOTFiZmU2NmZmZjQyOGM4YjY3N2MwM2FlMWUwOSJ9', 1, NULL, '0529D722-4AFF-44CD-8D51-63047FCA374E', 0),
(19, 5, 3, '1326.94', '1326.94', '2019-02-10 02:31:58', 'eyJpdiI6ImZiWHhZTGlpanY5TUlWV1RkQStSYmc9PSIsInZhbHVlIjoicmp6Mk5PUGFveHBsMDlYWDNyZlpkWU1hcExsR0VrS2tkR2lONk9aaG1waz0iLCJtYWMiOiI3MmQ1YzVlZmM0ZmQ1NGE4MTg3ZGFiZGU4YmZmNDQ0YTExYWYxN2VjNTE0NjhlOTAxYTY4ZWM1NjBkMDJmNWI1In0=', 'eyJpdiI6Im9QT2VyNkM5K1MzV2RkZG0xKzM4ZUE9PSIsInZhbHVlIjoiUElnQVczOGNvQmR0bVZ1ZVg0c1wvK3laaXUxTHVrS093b2RsT1JFS08rQm89IiwibWFjIjoiODRlOWViYmQ1NDBjNTAwMDRiYWFjYmQ5M2YwYTQ4MTI0Y2E0NTk3MzNjY2FiYjdjNjUwMDExMmMwNGNiZDVmYSJ9', 'eyJpdiI6IkRaaHpobHBscjNkWE1pTmdlQ29SQkE9PSIsInZhbHVlIjoickxEXC9sNWtTTWNsdVlvZGZCbGRCVGc9PSIsIm1hYyI6ImU3Yjc1YTRjMjA2ODg2MTQ4MDBmMjFiOTM5MWZhMjI0Y2Y1Njk2NDMzZTIzYmM3NzJmM2U2OGIwNDExMmU2MTYifQ==', 'eyJpdiI6IjlrWllMVXdqT1NycW1venhwWVk4Ync9PSIsInZhbHVlIjoiZlMrU0NoaTBib3NmYkRxTjVVUFZPV3ZVTVVuVk5OWmo1WjJQbGNuWVJrYz0iLCJtYWMiOiI5NTE0Y2JkZTBhYWViZWE1ZjgyYTVmZWFiYjBiY2JlMzBlNTRmNGZlMTZmZTk0OWI1NzdlMTQ3OGZkZDQ0N2UwIn0=', 'eyJpdiI6IjJkdGdQZHo3XC9Tc0t1czhjZlZrMTlRPT0iLCJ2YWx1ZSI6IjZxbUpwZldnQjY3MjNHZWpEanJUUWc9PSIsIm1hYyI6IjExM2ZhNTA1NTg4NTc2MjQ0YmVkMjQzMGU0YmE2NDUxNDlkZTkyMDY3ODg3YWIyMDEwNmM0N2M2ZjUyNWJjNWIifQ==', 'eyJpdiI6IjBIXC9GNHVhXC9Fd3FvbVhwWVI3YkVnZz09IiwidmFsdWUiOiJNNVNCVHRvQmIrQ2NOYzZBeVB1WFwvZz09IiwibWFjIjoiNmZiMjI3ZmNkYjcwNDllYzkyYzQ0MzRhYmY2NmNiOTM3NjM4MTAxYjhjMGUyYzMzOWIzOWMxYjZhZWM3M2JlZCJ9', 'eyJpdiI6IjViajZ2ZEplQ0J3TkNPWFwvbXM0VzlRPT0iLCJ2YWx1ZSI6IjMwc2tJeUdVSzVrbTZ6K1pSZlZhK1E9PSIsIm1hYyI6ImVjYjMxYzdiMjYwMTc1ODAyMjFlMjEyYTc1ZWU3OTg1ZmQ0ZjQxOWIzMGZkMWQxYWExMjBhYzYyNGRhNTRlYTMifQ==', 'eyJpdiI6Ikw2bmRsVVBwWTVhOURCK1hKQmdVWXc9PSIsInZhbHVlIjoidzM3Z01xVU9FcEtsSWtCVUxJZlpTRE9aaFwvTmw3ZlV3cTVnVTF4QVA2alU9IiwibWFjIjoiYTBhZDdiMGNjMWU4YmFmMTdjOTliYjdkM2QxZWFhODhhNWJjNTcxODhhYzcyZTAwODkzZDgxZDMwNGIyYmM3ZiJ9', 'eyJpdiI6ImxCcG5Pb0o0S3lvN0xwS3Q4RXpiNHc9PSIsInZhbHVlIjoiaEIrVCtSNDZKSG5DdXNXNzRHQkV6Zz09IiwibWFjIjoiYzYyNjY3N2IzNmUzNDc0NmNlOTZjNjk5OTc5MjEzOTc0MTM3ZDM4M2NlY2FkOGI1YzgwOWE5Y2E1MDg1OTMwNSJ9', 1, NULL, 'B6DAD16C-E64B-489D-BF31-E5265481BAB7', 0),
(20, 5, 3, '1326.94', '1326.94', '2019-02-10 02:32:05', 'eyJpdiI6Im4xTUNJamlCVFBDWGV4QVZvWWpBUkE9PSIsInZhbHVlIjoiU0tKSENhMk5mWW82Y09SWXJQQ3psNDVXS1NxWXAxTFd5UGFFXC9hclZLZnc9IiwibWFjIjoiNmVhZjU0NWVmNjFjOWNiZDJjNmNkMjEyOTY4NmYwZjlkYWQ1MmQ4NzY0OGRjMDA4OGRmYjc1NDQ0MzZlYWUwNCJ9', 'eyJpdiI6IjRFZVwvaGFWekI4WThVZVdhVXF4QkR3PT0iLCJ2YWx1ZSI6IjV3NmE5VVwvaHhjbWg4d0szek44Qjk5ZWxGRzNKeFhQeGpOelNORlpnK080PSIsIm1hYyI6IjQyN2JjYTRmNTczYTZlYTg3NzFmNzEzMDdmZTdlZjY4NzI0ZjUyMzA1MDAyMzI3ZjE3NDQ0OGU5ODkwZmE2ZTYifQ==', 'eyJpdiI6IkQxZUU2ZW51UjBuXC9yaFhnZ0ZuS0dRPT0iLCJ2YWx1ZSI6Ilwvb0JhZ2xqMUQyM3I5b1BReHdvRzB3PT0iLCJtYWMiOiIxODE2Zjk3MTA5YjI5YzcxNGM3OGU0MTY5ZDQwN2ViMTk1ZTllODVlMzY4M2JjNmFhMmMxY2Q3MjBlOWUzZDI1In0=', 'eyJpdiI6IjlwTDBjVjFVK1ZPMWJvcnl3TjNiTFE9PSIsInZhbHVlIjoiOHBXcUZqNE53YTFSUVVUa2lNTEdnYURXVTFPdGxjbzNhRnRFeVwvMWhkR009IiwibWFjIjoiMzg5MWQ3ZmJmZTkwZWMyNGEzMzNkODEyMTJhY2YxM2VkMGUxYjZjNjcwYjQ4ZjU4ZTE1MTA2MWI3ZTUzNWIyYSJ9', 'eyJpdiI6IkhyOW12R1RORzVTWjJQclVkMTVNZFE9PSIsInZhbHVlIjoidlVHME9wXC9ndVptNVlRbXlBS2lrMnc9PSIsIm1hYyI6IjgwNmVmOWRlNWFmYWRlODU2NjkxNzdjMDVhYmE0ZDc0ZTQ1NDJkNDI0MTc4YzA5M2M5N2IxMDg2ODQ4YThkOTkifQ==', 'eyJpdiI6IkFPTjhNTkR5QThMMEdVd0prbXZzR2c9PSIsInZhbHVlIjoidk5NdFwvMHo5UjI3a0lLK0VnbXg4MEE9PSIsIm1hYyI6IjUwMDJjYWRmYWJjM2I4ZjRlOTRjNmFmNGY1MDBiNDQ5YTcyNDkyZDNhZjhiNTgzZDBhYzg3OWMxMTIyZjMxOTAifQ==', 'eyJpdiI6IkNRbFE2djdocVZPQW9qbVhqVnRrSUE9PSIsInZhbHVlIjoiRGE5dUNlaklFQjgzVGdcL0YzYnhJR0E9PSIsIm1hYyI6ImRkMzlhZDIyNWVlM2Y1ZWRkN2NiNjY4ZTBkMjY4MTZiMjJiMDNjZWVmNzE4MTYwMjJmZGI1N2VjM2RkODY4NzEifQ==', 'eyJpdiI6IkJwVU1RTnY4XC9KS2VSc0xoOVlaQnlBPT0iLCJ2YWx1ZSI6IlR0bHlBZERFbTBEVFJXOUJ5ZEQ2YVlUMUZkelJqMk80MEZnV1p2NmI5Q3M9IiwibWFjIjoiNjkyYzg1OWNlY2NhNzRmN2ZlZWIxNjdkZjU5MjVjY2RjZGJmOTRkYmQwOTFjZmRjZmNkODA1MTBjYjE3NzE4NyJ9', 'eyJpdiI6ImF3dUJcL3ZvNytPaFwvTm03MzNoZmVGQT09IiwidmFsdWUiOiJyamxOVThiNEI5UmdXemlKM1BXZU1nPT0iLCJtYWMiOiI1ZTEyYzIzNTY5MDcyNmJkOWQwMDY1M2U0NGU2MjEyODc2NjA5YTE2NDYxZmM5MTIyZjg4YjQ5YWZmZjQzYjQyIn0=', 1, NULL, 'EF8E5A03-F9BA-4506-9FFE-DA6A308DB8B5', 0),
(21, 5, 3, '1326.94', '1326.94', '2019-02-10 02:32:45', 'eyJpdiI6IlV2Z2NnNEY3NEtzT0NjMmhSanJqSlE9PSIsInZhbHVlIjoicFV1eDhuRTBleVVPTis4WGZjaHRcLzNUXC9Jck83OXRyNlZZRUhwZXBPc0Y4PSIsIm1hYyI6ImRjY2Q1NWJmNjJhYmNkMTkwNjE1MjBkNDFkMDA0OWNkM2U4MzUxMTM3YWUzZmVmMmY1Y2JkZDY3MDkyM2MzOWIifQ==', 'eyJpdiI6IjA5R01VbWF6VU9sd04yYjJYdHVXUFE9PSIsInZhbHVlIjoiN1Y3MDB1aUdKMXNnaGNhZjZYeTZWOWlaamlodW1WcFJ6REl1Tmtmb2NLTT0iLCJtYWMiOiI3MGI0ZTc2YTY0YWQxYzNkZDRjN2NkOTM2NWFkZTc4MjE5MDJiZTM0NGQwYzQ3N2QxYzEzNzZhZjFkYmIzMjI5In0=', 'eyJpdiI6IkdEMjdTUHdmUTRNV1VENkRZdHRzTVE9PSIsInZhbHVlIjoicDQycU11UXlpVTNCODhrM0ZZV1YrUT09IiwibWFjIjoiZGMzZWEzOWFmZmYwZmM2NWNjNWIyZWRjOGMyY2RlY2UwNjk2M2Q0MjkwOTE4NjU5MzY1Y2QyN2JkMGMzYzRiOCJ9', 'eyJpdiI6ImczT2RJbUpFY3ZkTEZnbjQxQTdKR0E9PSIsInZhbHVlIjoiS0Fzem5WeDRPMThWM3ZsY2NobzVBdEpOcVlGM3dKQm5uTVFLdkw4cDZrUT0iLCJtYWMiOiIxYTFiY2YwOGZjNjAxYzhkOWM1MGNiNjI0MDEzZjJjNTBkODNjNGFjZDY0ZGRmMDdhNjQ3MTFmZjc4YTY3ZmQxIn0=', 'eyJpdiI6ImFrN3NWbllleFE3SzQwSEU5R2d5SVE9PSIsInZhbHVlIjoiTGFoXC80TVNLc2E1QzlmQ1E4b3lcL1pBPT0iLCJtYWMiOiJmOGIwMDBmYTg3YjczOGYyODY3YjQ4YmFhMGExMzFiZTJjZmM0MDVkYmQxNDJhNDhhZjIzNWEyZmM5NDNiNGVkIn0=', 'eyJpdiI6IlI0Mis4N0dIN3A0bWpLeTBkMUs1UkE9PSIsInZhbHVlIjoiVzVsc3VubUpUNkUzRFVLSmE5VTArZz09IiwibWFjIjoiNmZhYjZlNjI2NWIxOWZlYTU1Yjc5ODNjMWZkMmQzZDQyYTU2NWQ1NmE5OTM4NDA1MzI3MGE5ZDFjMGM1ZGJmNiJ9', 'eyJpdiI6IlZGbUFmazMwMXZJSEFhM1VldzBaT0E9PSIsInZhbHVlIjoiUTk5WmVvWHhmbWFBUytOdDhOVFViQT09IiwibWFjIjoiODUwZDU4NjE0NTZhMTkyNTFkMmJkN2FkMDE1NzdlY2Y5ZmMzNmU5YmNlZWY3YTA3MWM2YWIxZGNiNTg3YTQxZSJ9', 'eyJpdiI6Im5cL3Bzc3NsQ3VIMEJwZnUyaW9kYWRRPT0iLCJ2YWx1ZSI6InE5a0VON0JMdjhVY2I2aDNpTXBnbGsyT0JcL2VBdzZmT3plRXBYQ3VWc3U0PSIsIm1hYyI6ImE0YmMwN2NmMzkyMWZmYzliODViYjdjYjM1MWEyY2RlMDczN2FmNmY3Y2NjNTI5NThlMDhhN2RhNmViYTVlY2YifQ==', 'eyJpdiI6ImRGaHljTUxER3JWOUN6SHpUSk04cWc9PSIsInZhbHVlIjoidGE0VXI4ZVdyeExENDlSSGtsaTVyZz09IiwibWFjIjoiNDEyZGNlMDFhZTM2YzA2ZGVmYWZlZDI2ODM2ZDk2ODU3ZGEyNTk1NmI2ZTc2NDlkYjM2ODRkNTcwZGU2MDQxOCJ9', 1, NULL, 'F5F4884A-9E4B-4CF2-8146-427E75BD71A7', 0),
(22, 5, 3, '1326.94', '1326.94', '2019-02-10 02:33:01', 'eyJpdiI6IlwvTmY1cEhxblp3UklHa29MbHFUV3NnPT0iLCJ2YWx1ZSI6IjdBd2VxOFRVSGphTDZLTGt6RlZQS21sWHc4VE1GSDVRa0taTm1ad29Eajg9IiwibWFjIjoiNGFiN2RjN2RmYmY4ZmVlMDYwYTQzOTIyNzIwYzEwZDhhMDAwN2JhYzBhNDcxYmFmNzgzYzE2YzE0ODgxY2U4ZSJ9', 'eyJpdiI6Ikw3T0NhWHZ0a205TUErZ2FUUzdRYXc9PSIsInZhbHVlIjoiaHhxZmpySjM3bVI1UHFJVEdzZkFDM3JBcGdkRTJWWWl2clVzTlJjaW9lVT0iLCJtYWMiOiIzOGQ3MTZmYTRhMDQ4ZjYyNTIwZjgzNzJlYjg2NTg2MTllZTA4ODYwZWEwZGI0NGQxZmFmZmNkMWEwODUzOGYzIn0=', 'eyJpdiI6ImRuWERlS2VYWGdCQ1hJMm1EV2thQnc9PSIsInZhbHVlIjoiYnJyS1l5aFwvT0FFR1NsMzBVS01MZnc9PSIsIm1hYyI6IjNkNmZjY2I4MzdiYjgwOGVhZjkzMWJmOGVhYWRmNmIxNTViZDQyMDgyM2M1NDdlYTExYTkxODk0ZjFlOTRiNGIifQ==', 'eyJpdiI6Ilg1VlNnXC9KajU4THRDbDgza0owR3lnPT0iLCJ2YWx1ZSI6Ill0dURwXC9Ma3hvUnZ5TW5GWE0rVWtQdFJTMXd0ZG1tQ0VFdzZhTHlCblNzPSIsIm1hYyI6IjhmOGQwOTdhZWQyNjk0Zjg3MWQ1MTBiN2FiNWQ0MTYyYWQ3YzE4ODM4MzY3NTAwMGQzMmQxYmY5MDBlY2NjZmQifQ==', 'eyJpdiI6IkFBV1h2MUJjS0NRYm5aZWRcL1lQcWJRPT0iLCJ2YWx1ZSI6ImZidGVoK0tkVVRJc3RkZHVtRmlmZ1E9PSIsIm1hYyI6IjNhZjU2MmY3MTIwYzA4NmRkYTYzYzdjOTQ2MjgxOTI5M2MxODRiYjkxODQyZTg2MDI5OWI3NDhjOTQ1MTdhNjgifQ==', 'eyJpdiI6IjZ1b29cL0ZidXArSk9hcFwvQndTK2dVQT09IiwidmFsdWUiOiJkSW9xblZuUTdnT1BRRGZVUFNzQnBBPT0iLCJtYWMiOiI0ZDgyOWJlZTI2MGJjZjZkMDY5MzJjNmQyYTNhMmI2ZmQ2OWVjMjZhOWVmZmIzNzRjYWQwYTc3YjYwMGUzMzE5In0=', 'eyJpdiI6Imo2VThVZWIwcDVpeW9VTWhHWTFuZnc9PSIsInZhbHVlIjoiTUEzRjFQYzZKbGtyK3VuTW16ZHlJQT09IiwibWFjIjoiNDRlZDQxNDY5ODk2ZGMxY2U3MTViM2M2YjhlOWQ3ODA3YTE4YTkzNjNjYjg3YjRmYmFkNWExOTA4MjY5MjgwOCJ9', 'eyJpdiI6IlVHaGtlSTBFdUhFaGZnQnR0TVwveCtBPT0iLCJ2YWx1ZSI6ImMxTnJKbFc5aWtKQWZ6SDAxTVN6ZEtXOXB5aXRXM090MVwvbGFCNFI2SVFNPSIsIm1hYyI6IjJiMTMxOTZlMTdlNGIzOTk2NTMzNDk4ZTcxNjdhM2RkMzQyYmM2MDFhYzYzMDVjOGI5NTc2ZmIxOWI0YWJmODMifQ==', 'eyJpdiI6Ik1rZ1hlQzJIZ1QwOTIwc3djRVpxRHc9PSIsInZhbHVlIjoiTzc5SFdqcUhrSjdjQXlZTlg5RlRNQT09IiwibWFjIjoiODBhZjMzZTlmM2RiNTg5YjBhMDFhZGFkMmIzMWJhNzk3ZjAzOWQxNGUzYTE3MWI5MmJjNjI2NDlmZjkwMDA5MSJ9', 1, NULL, '070D684B-4D38-4D6C-99EE-853956E65A1A', 0),
(23, 5, 3, '1326.94', '1326.94', '2019-02-10 02:33:09', 'eyJpdiI6IlJLVlRVSGRoVDd0Q3NuSnZqZHZxdWc9PSIsInZhbHVlIjoib3hKTjBoSCtCdzN1Q2dJcGpPOXkxU2RIR1puUWJMeDNwZ0hlSG84SGJCUT0iLCJtYWMiOiI4MjlkNTliN2QxNGJiNmM3M2VmZDMwOGMxZmNjNTM5ZmE1NjAyZGYwYmEyZDM5ZDg2YWRkMDg1NGExMDRkYjczIn0=', 'eyJpdiI6InVrZzZjd1B0QzNpUzVuZlpUbk1LdWc9PSIsInZhbHVlIjoiVUtPb0VoYUlTcEhtOVQrWHBBT3VGWk8zdGh1dEJ5aTRseHVYZFNOU3NkOD0iLCJtYWMiOiIyMjAxZTk1N2Y4OTY0NTcwNGRhZTRmZWExMzc2OTYzOTc2NjEzZTM2OWQ1M2QxYjllNWY1NzhiZGI3ZTZkN2UzIn0=', 'eyJpdiI6IkVLUFwvYnNoTHdTakJlSVwvQVJENXdNdz09IiwidmFsdWUiOiJnd1wvSkVuaXhvRGMrXC9QaUlubURWK3c9PSIsIm1hYyI6IjA1MDg5ZWRlNTgwYTRhOGVjNGE0ZjcwMjdkYTY5MmI2MGMxY2Q4YTVhMzA0OWI3NWM4YjgxNjMyMjBiMTIzYjMifQ==', 'eyJpdiI6InVGaWpIaVo1KzFQdTJjWTVYN2h4NlE9PSIsInZhbHVlIjoib2o5Uk1cL0NzbXJZbVFoZ0hzenByOFl4TkNtcDJicWRXejJyaEpjcWJzbWs9IiwibWFjIjoiZjZiZjY0M2E5NWJkMDg0ZWE1MTk5YzE0Yjk1N2MxMWI2YTFkNjIzM2YzZWY5MDQwYmYyMmMyNjNmZGY0Mzg0OCJ9', 'eyJpdiI6Ik9TWkdFWlk0VVhPb29ocE9tSW8xU3c9PSIsInZhbHVlIjoiRVdjdGxTYVUrZGlqTkRWY1gyUTQ2UT09IiwibWFjIjoiMjNiODExZTIxN2RiOWM5YmQ0NDAwMDE0NGVhMzZkOWYzMjNiYjFjYzhkMTRkYjBhYzZmODUwN2Q1MDZjZDJkYyJ9', 'eyJpdiI6ImRCNDVEMGpKVUx0YVArWkExdHU3eWc9PSIsInZhbHVlIjoiY2ZPWVNFSnlROGl4c1d5SytKMmNndz09IiwibWFjIjoiMDY4OTg2NDQ5NzhhMTRkNjAxMTI1MTQ3YTAxMWE3MzI0ZGMyZjcxZGNjNTFmYzg2OThlMDIzNWY3MTMwZGYzOSJ9', 'eyJpdiI6IkEzeXZzNmxLMUpaVUw2SVZnM2wxOGc9PSIsInZhbHVlIjoiTklpUUZaQmhDMjZLS1BtN1A1TjJUdz09IiwibWFjIjoiYzk4ZTUwMmQzNmIxZmIyOGVlYTYwYjU1NjlhZjdlMTQ1ZDRiNzI0OTM2MzJlZmQ3MjdlNWI3OTVkODgxMGM2OSJ9', 'eyJpdiI6InhpM1FOQ1Z3Z3ZcL2RmaW1cLzFIbFpuZz09IiwidmFsdWUiOiJIWXVhY00yNEpIdysrU05rcHQ5TFNyZDlVWjArREZNSDJGc09zVG5ENXJvPSIsIm1hYyI6IjE3MDIzM2E3YjYxNTA3ZmExY2I0OTI3NGY5MmExY2FjMWVkNzliMjNkYzA4OTkzNGM4NzI1M2VlZTkwZDkwZGMifQ==', 'eyJpdiI6IjVFanBQbmJQcmRiUFwvWkNIaXQ0UmVBPT0iLCJ2YWx1ZSI6InhcLzlIVXJrcFRUczM4eXdhS0JEdzB3PT0iLCJtYWMiOiIzNzBiMmM0NmNmMjAwNDZhYWVmYTc5MzVlMDgxZGRhNTE1YTgzMTJhMjQyZjFjNGE0NzYwZDUzNTkyZWViZTQ4In0=', 1, NULL, '460B2D9C-6241-4734-B857-DB8865CF6B2C', 0),
(24, 5, 3, '1326.94', '1326.94', '2019-02-10 02:33:19', 'eyJpdiI6IkJIWDdBbmpjdGw5OFBJWVdpejdBRGc9PSIsInZhbHVlIjoibVpYUWcyR2V6c21cL3kzaEMxSElmeE10UGZVXC9RNlpTaG1QXC9LNThvNHVYTT0iLCJtYWMiOiJkNTdiOGE4OTc1NGY5YzhlNTI3OWY2N2RjYzdjNjRlODhkN2NhMDc2ZjFlZTk5NjQxZWMwM2ZmNTRjYjZhMDA0In0=', 'eyJpdiI6Imo2VFRxNGdUWjAxTFZhbFZzdE1XeHc9PSIsInZhbHVlIjoibGdGbWxBNU5zemtUTFhKVXV1bGRsb2RTckNRaURRVGJKVVl4MXBaZmVOaz0iLCJtYWMiOiJkMDZiYmExOTFhMjZjYTA3ZDZiMWU2YzZlNzk3OWRhN2ZjZDZlYzkwMzY4MmNmZjg1NDYyNzgwYzQ2MWNlZWM1In0=', 'eyJpdiI6InhTXC9meHVqelcxaXAxNzNxVzhBTGdBPT0iLCJ2YWx1ZSI6Ijdrenlsd3lkTGZUeCtobU9pV1VkQ0E9PSIsIm1hYyI6IjVmOWJiM2MwM2ZjOGQ4MGRlZTJmZDZlNzQ3ZDJjYjhhNzlhOTc5ZjcxOTA0ZjU2YzQwZGI2ZWMzZDA5MDUxYTkifQ==', 'eyJpdiI6IjZcL2JMcGY0emJseE1BNlVLTUtYSmNRPT0iLCJ2YWx1ZSI6InZQQ0NFTGlTTUhTNEpSYkx6bTZuMzRBVkt5VTdPZkJwMWV1UFg4K2w2akU9IiwibWFjIjoiMDUxYzcwNWRlOWYyYmRiYjI0ZWMzNDgwYzc3MGJjODljOWI5YzcxZmFmNzgwYjQ4Njk1ODRjN2NkMTUxZDA1YyJ9', 'eyJpdiI6Im52d3Izc2krTEZWMEdacDR3K2htSnc9PSIsInZhbHVlIjoicWtyWWVXc1FxZ3UxK1RRazlVZStYQT09IiwibWFjIjoiZGE1MDc0NDExMTlhYWIyMmUwOWFiYTNmODcwNzhhNTVlYTQwMTQ4OTE2YzdlNWQzZGNkZjExZWI1MDJiNDg4ZCJ9', 'eyJpdiI6Ino5WU5DNW9cLzRcL3VIZkR4SW1pZXBKQT09IiwidmFsdWUiOiJQckhYcm90YmdDbXpHNU1uWnpvQWZRPT0iLCJtYWMiOiJlMTZlMjM1M2U4NDVjNjVhOGRiMWMwNGIyYWJkYjA2YzY4NDUxYTIxMGJiZDU5YzJlNjYxZTgzMjVmZDA5YzRkIn0=', 'eyJpdiI6ImplbXdBMCtlUld5NmRCYmgwbFhxckE9PSIsInZhbHVlIjoiaFBRdUF0cDVOZUQyN2ZIU3Y5ZHVYUT09IiwibWFjIjoiZTE1NGE3YzQ0OWE1OGU0NmRiZTNhODIzNTNjYWQwMzBiYTEzMTZjOTdhZDVhODVlMDhkMGI4ZjYzOWZkNjFhMyJ9', 'eyJpdiI6IkRDamdmckZuQXRuY1VmZEw5SmRhZnc9PSIsInZhbHVlIjoiemNpRzJFQ0tCWjNlT2Yxdm1MbTgwTnhxRzRjV3BHdURsSGhsRW1la3BQOD0iLCJtYWMiOiI3NzQ4YjBlNmU1ZjMwMWFkZThmYmE3NzE4ZDc5MzZkODVkMmY1YzFhYTU4NTM5NjVlZWIwNTA0ZjRmYzBiNWNhIn0=', 'eyJpdiI6IlZoVHg4QmttS2U0dkh3NmVqVFZmQUE9PSIsInZhbHVlIjoid2RYKzNTKzNEUGpEY3VHdEQ3a05odz09IiwibWFjIjoiZDk5OTEwOTFmZDIxMjFiY2RhZGEzYzMzMGEwYWFlMGY0YzY2ZjZlYmViOTUzMmI1YTdiOWU0ZTA3ZjU5NDJiMCJ9', 1, NULL, '987E2F87-C7A9-4146-AD05-032DA729AAC9', 0),
(25, 5, 3, '1326.94', '1326.94', '2019-02-10 02:33:50', 'eyJpdiI6Imp1T2llNWxCZzFPZ2I1ek5QWW91eVE9PSIsInZhbHVlIjoiSjRVUjFIWUQ3U2xxRUpVZW5MNEZlcGVKTk1XMVN2VkpieEptSW5EdjNDWT0iLCJtYWMiOiIwYzUwMDkwYjUyZjllOWVhMGMwODM2MWExZjk5YWFkNzY4OGIxZDdiZWNiZjFhZDM4NGVlY2UzZWQwMzQ0ZmQ4In0=', 'eyJpdiI6ImNvenRrSjdCY21WcW1wZVU4YXNpRmc9PSIsInZhbHVlIjoidjJIM2lmTUo3ZUQ0NGlSUHJQRHNlKzlcL0s4TEc5XC9EeGtmcWtpMjk0amx3PSIsIm1hYyI6ImYxMDY3ZmNiY2E3Njc1ZTljOGEyZWYzMjQ0ODM5N2I2MDk0YWRjN2FhMGYxNTY1N2Y3NzI0NjkyMTBmYTk3Y2IifQ==', 'eyJpdiI6IituczE2Y2RCcXkzOGxpNVJSR0U2aUE9PSIsInZhbHVlIjoiSkk5YWhSeGtkMHArYmhVelp1NVA2QT09IiwibWFjIjoiYjdiZjQxYzgzM2M1NTY2NzUzZDgxM2ZhMDA1MzI3NTY5MzE2YzRhZGQ1NjE2OWJiMmVmMTQ0OTM3MjJiODI0YiJ9', 'eyJpdiI6IkVqcTFFQ2k1N2JtNHZsNDF5XC9DTExnPT0iLCJ2YWx1ZSI6IitZeldGeTBUcmhpUHRzRFRuaXArcGYyZHJYNlVyUUVrcEdHV0ZqMHJTcnM9IiwibWFjIjoiZTQwNTRkZTJiZGQyM2Q2ZDFmMjY4YWVjMGZlMWQ2MGQxYzZhZDg3YmZmMzFhOWZhOTJkZjFiYTIyMjUyMWJhMiJ9', 'eyJpdiI6ImwzbzVwTHUyeHhDN1JlZW9lRTEzVXc9PSIsInZhbHVlIjoic2lYdFIwZ3drTjNnajhpZGJQTzl3dz09IiwibWFjIjoiNzc3ZGMzMThkYTgxZTdlOGI2YzkzZjI0ODUxZjUzNjBmYzUyZDE3NDg0YjY5NGEzZDg2ODNjMWJkMGUwY2JjYSJ9', 'eyJpdiI6IndjSTBSdTc3T3kzWnBSTU1JMDRoMXc9PSIsInZhbHVlIjoiUHVBSDAxS240Z2tBaXZSM3U0NExIQT09IiwibWFjIjoiNzc4MjNjMmI1N2ZiOWNkMjQ5ODI5MTNhM2U3Mjc0OTIzOTBiYzczMDhhMmMyMjczYTBkMzgxOTliNzA2NTVmYiJ9', 'eyJpdiI6IjhDNVJPNW8zc09LRDJjQnB3XC9jcnNnPT0iLCJ2YWx1ZSI6Inh6ZFNDbGdmcjBQR1d3NHpZdFNXc2c9PSIsIm1hYyI6ImFmYjU0YzIxYTkxYzQ0NzE3M2EyMGFjMWQ4MmQ3YzJlNzRiMjA2MmU5Y2Q3ZTJkMjhmYjg2ODc4ZThiNzQ0NzkifQ==', 'eyJpdiI6ImJ3a2pXXC9KNzd6UXNPeXp5XC9jYjI1dz09IiwidmFsdWUiOiJxcTdRSjVQbHhCb1VZQnlqazlBNk5tYkh2ZFM2WFJGM1lcL3RTQXBtQTYwOD0iLCJtYWMiOiJhNmMxMWZlMzRjZGYwZjhmOTcwNWE0MGNmM2Q1ZGM2Zjc1MWI3MmMzNmI5MTIwNmFiYzVlZGViM2VhYWJmMzY3In0=', 'eyJpdiI6ImExclp3eXFDREJRbERKT3FRZ0MxZ3c9PSIsInZhbHVlIjoiTHlDdTlxcm5iRWxrNDQrUXhCcllmUT09IiwibWFjIjoiODZmMzcyYzJmMzEzYzU4MGE5NTkwNzkxNDM2NDkxNDIzZDdiM2I4ODY2MGQ2YjgwNDI1YTAwMDY2N2FkYjE3MiJ9', 1, NULL, '239CE5AC-A4E2-49D3-85F0-95679C6C2648', 0),
(26, 5, 3, '1326.94', '1326.94', '2019-02-10 02:34:17', 'eyJpdiI6Im1EXC9GNVljK2lRS3dmQTdFVHJ5elV3PT0iLCJ2YWx1ZSI6IlZyZzRoOTAzOVNIMTFUTU5JaU40aUdKemw3dVFqSEI0cTZubTZGT2dqZEU9IiwibWFjIjoiMGI0ZmRjNTQwNzU3NzQwYTM4ZTkxMWNhNzYzZTU3ZWY2MTU3ZTFkZDY1NjZjNzQ3OGIxODdhMzA4NWRhZGNjZCJ9', 'eyJpdiI6InBcL005ZUkxbzd5U3MyWWo4SDB4eGxBPT0iLCJ2YWx1ZSI6IkhHMVdydFhxOEdRT29FWTNPNjdDVVRXQ0ZlYVdxNWFpeEROcFJEUk5vSGM9IiwibWFjIjoiMDE5OGFkMTYxZGY1NjMzZjRmMTQ0YjI2NzkyMjE5NDAzZThiOWMyZTFkM2YwN2RiMWNiMTRlOTUwODc0MzU5MSJ9', 'eyJpdiI6InNDN2MrbmV5QzZGM2drMVBERTF5UlE9PSIsInZhbHVlIjoiR0p5cUNVbzZmcGpcL3A5NjdyS3c5dlE9PSIsIm1hYyI6IjEyZjMxNzg4Y2E2NjFhNjAxNTJmYzNjNTY1MDhmZjg5MzNkYzc1ODE4ZWRmYTYwZjE5MGM2NDI5MWUwYzJlYzkifQ==', 'eyJpdiI6IjlPSHhWTVk1RmNDdUVmenk4XC9yZUVBPT0iLCJ2YWx1ZSI6IjJNN0syYXZubFwvNVFnbWt5OWdxN29Tb1phNmk1NXNkV3NFZEhaV3kxUURVPSIsIm1hYyI6IjY3NmM3OWQ5NzI3NGQ1YjI3ZjFiYThmMDhmMWJkMWJmMTA4ZTQ3YTZkM2Q5YTAzZWRmNDNjODhhZGViZTJkYWUifQ==', 'eyJpdiI6IjhjRHJXTGZGVjNKZ0JWTCtTUFdBY0E9PSIsInZhbHVlIjoiZmQ5aXNLT20rR2pFYm93UUE1RjUzdz09IiwibWFjIjoiMTkwNTUyYzRmZDc1OWRhZTdiOGZiNTFjMmIyY2VkNzA5OTA2MDFiYThjNzY2MzZiMGUwMmM1YjAwMjkxZWQ2MyJ9', 'eyJpdiI6IkRVRTRETVwvQWVRWjU0alNIUTU0RVpBPT0iLCJ2YWx1ZSI6ImNtbDJqWDZvSER4ZElZMDNwWTJWQ0E9PSIsIm1hYyI6IjU3ZmNlNmZhMDZhMjY1MzRjMWFjNmFiZjUxZmE1YzYxMmUyOGYzMTUwM2FmZTFiYTc4MDM1YzQwMzZjNDUyYzYifQ==', 'eyJpdiI6IjI0aVVGTnBWeG1JU2ROZ1pQZDk5Rmc9PSIsInZhbHVlIjoiQ1JBQjhEdk5xcTUrTFZRdzRXaHhyZz09IiwibWFjIjoiOTFjZTY1OTQwZTBiZWE5OWIwNDM3NzczOWI4YThhMDdhYzVmYjQxOWFhNmQ3YzkzYTIxYWM1NWI0MTI5ZWFmNSJ9', 'eyJpdiI6IlpUdU9uSVhYSDdXUUhIcjB2d2x3Nnc9PSIsInZhbHVlIjoiRTA3d1crdWx0a3FPUllCeDlxOGNDeFUwWWphenRRQUk1RnJWQ1cxRzBQcz0iLCJtYWMiOiIwNzJjOTQzYTM1ODExZmQ3NzYzY2JjOTY2MGFlNGIxNDZkM2MwMjNjYTJiN2FiZTI2ZTdmMjVhNGIxMWQ5MDMzIn0=', 'eyJpdiI6InJMV0VPc01MMVM0VlRnMGxMU2pXaHc9PSIsInZhbHVlIjoiQmptZk13NG9GVnhMNXUwOHo0UjZ2UT09IiwibWFjIjoiZWIyNTYxNTFjYjY2ZDJiNzA2NmEzNDQyYzg2MTg2ZDZlNGM2OGUwYzAxNzRiYWUwODI3ODFjNzAwYzdlZjRjYSJ9', 1, NULL, '9E05C102-4293-4985-B796-E286F9A103F5', 0),
(27, 5, 3, '1326.94', '1326.94', '2019-02-10 02:34:32', 'eyJpdiI6IlRMc2JncnVrS0RJU2JqcjYwWjdsRlE9PSIsInZhbHVlIjoiM051SzVqUWNtZ1dhcUl3eGRSM2wwbVhaYTNHYzdaelpcL0c2cWk1SUhEK2c9IiwibWFjIjoiNjc0YTE2ZTE4ZDU2ZTY3NTJiZGRjOTQzZTliOGYyYTVkMTc0ZWZiOTAyY2I5OTUzYTJiY2NmMWYxNGQ0ZGZkMyJ9', 'eyJpdiI6IlI4Y3NHNXRBWVVkcmIxYVprcW1WN1E9PSIsInZhbHVlIjoiVUxveHVjUCtxbTY0MkNZc1hrbmlZRjVTako5NlpzVW9SYW9xalc3T2k4Yz0iLCJtYWMiOiI1YTY1MmMzOWIxY2NkZmEwYzI1MTM0YzM3NTg0ZTBhMzhmNTViYzZlZGI1OTg1MTRkOTllZWRjOTE0MTE2NWJlIn0=', 'eyJpdiI6IlArXC8xNHFWTERaSWpVYnp5OUl2dlZnPT0iLCJ2YWx1ZSI6IllzOXgyZlNFRzVVQ1M4cnFNWXpoSVE9PSIsIm1hYyI6IjA5YTBhMjFhZTg2ODU4MjAyMTMwNzg2Y2VhMTY4MmIxODQ2ZTQxYWIwYWRiMTkxZmUwYzc3MGMwODYxMzU1YjkifQ==', 'eyJpdiI6IkFTV092ZWFvaHBFYk1YTzVkdUZlcGc9PSIsInZhbHVlIjoieHh4bjMxakR2NmVYXC9kOVpjNzN4ZGhwS204QkRiS1FuNGptTFwvNGtqS013PSIsIm1hYyI6IjY5NjA4ZDQ0YTZmNDdkMzEzN2M5ZjFlZTQ2Y2Y5ZmM2ZDE4ZGNlOTgwZjA3NDM5ZmI5YmYwZmJkMDhhOGQxNDEifQ==', 'eyJpdiI6IjUxUjZQTHFIN3FSRnRuOWJaSmhqZ2c9PSIsInZhbHVlIjoiXC9rcW53NGpMakRHbXBXdjRRdTJRbVE9PSIsIm1hYyI6Ijc1NDRhNDRlMDgzYTVlMjQwZTQ5MmEyNzllMGFhYzhjZjM1MmJjMjhiN2FkMWFjN2U2OTBhODg4M2Y2OGU5ZDIifQ==', 'eyJpdiI6IkFYUDB4NjhzSTFRNURaYjM3OXJWVkE9PSIsInZhbHVlIjoiM1IydFFDTnh5eDlBSHJDWlpaVmFUUT09IiwibWFjIjoiNGQwZmUwNzgxOGRkZDBjZDE0ODA2ZGEyNjFiODNhYzIxMTEwOTA5Yzk4NzI2ZTMxNTk2MzUxZWU1ODBkZGRhZSJ9', 'eyJpdiI6IkFrMlF5bmJjWlwvQ2lIZVZIbHRvWnpnPT0iLCJ2YWx1ZSI6InYybENXdlRxMHg3SEhGQndna09EdHc9PSIsIm1hYyI6IjUzZGNmNmYxY2FmZjc5NWM1NzJjODZmYzI1NTU1NWNmMzAwYjYxYzg1NGM3YjY2NzVmZGQ5MGZmMTJjNmFkNzQifQ==', 'eyJpdiI6ImhkZkVFb3BaMDZhMEFteGMyTzZub2c9PSIsInZhbHVlIjoicmJTd3o5Q3Y4N0pHekN0ZmhNZW9Eb3ZqekNBZXhPbUdHdndPVmxtODNcL1k9IiwibWFjIjoiN2I4ZmQ3ZTdjYzhkNTY5MTI2ZWZiNjBjYTY2Y2RkOWI2NDY5YzgwYjUzMjk2OTI3MzNjYmY1Zjg1OGMwYmViMiJ9', 'eyJpdiI6IldpcXhCSHVkZDFLNXM3MTF1MGRlWVE9PSIsInZhbHVlIjoiaU56NFwvVWxHT2NkYjVtS0Z6M2M3NFE9PSIsIm1hYyI6IjEzNzlkMTU0N2NjZjljMTcxY2Q1NTljMDFkNjUxZmFkNTIwN2NjZWIxOTVmYzc4YWU4ZTM5ZWJkZjNkMzg5MDcifQ==', 1, NULL, 'AC75433D-B3AE-42BD-9561-4260529B8F26', 0),
(28, 5, 3, '1326.94', '1326.94', '2019-02-10 02:34:37', 'eyJpdiI6ImxUbkRtTzlaYXNIZ1NuN2E5SUdDV3c9PSIsInZhbHVlIjoiVERGcHhnc1wvQWQwaVVnZDM5dWJPbTgwTDk4V0o5NEJBK2RkdFNaQjdVVGs9IiwibWFjIjoiNDMyNDdiMjMyODFlYjU5YzliYmVkN2FhYWEyMzRjZjI3MjI2ZmI3ODU0YTI5Njc1MmI5MDQzNWJkYjE3YjY3YiJ9', 'eyJpdiI6Im4yUE5lTlhVd251VXRFYitveUxPalE9PSIsInZhbHVlIjoieHNRTVZaVVNtYVJmNVU5XC9zTXNwN1Urbm5vZ1lHY2szVUNHMmNFS1FWdmc9IiwibWFjIjoiOWY5NDdjMmE4MmYyOGNkNjVhZmNjOWRmODk1YjdlMmZjYTUyMmQwZjUwNDhlNzczMDcxNGVlODA0YWMwYzdkZiJ9', 'eyJpdiI6InE4b1wvWndBSHJHbngzYmJQcTlDeXBBPT0iLCJ2YWx1ZSI6InpicUdjcWRRWEJSZWVNOGJGd0xhWVE9PSIsIm1hYyI6Ijk0Yzc5OGMyZWEzZjRkODlkOTcyY2I0YjRhYzhhYWYyNGI4ZGE1ZDY5YjRkM2JkYWE2ODBlMDhhNmM3ZWM3YjgifQ==', 'eyJpdiI6InRkMHNWb08yUzNlYkNiUWpYUFBBUUE9PSIsInZhbHVlIjoiWEJVVnNjdENYalB2ZEFpTVR2cXk0K0ZjZzF6MjlYTDZxMkhEcDJDUkVYZz0iLCJtYWMiOiIwZTIzNDI2MWIwODk0YzZmY2E1NjU0MDY3OGZkNTk4NGFiNWUyMzZhNDUyZTI3ZWNkZDU0ODEzNzQ3Y2UwYjdiIn0=', 'eyJpdiI6Ilk4eGZMVTd0WTZQM0E3REQzTUl2bXc9PSIsInZhbHVlIjoibWFtQ1lmdDhmaEVRT3VsSnR1UEFQQT09IiwibWFjIjoiM2NkYzI4NTA4Mzc0MGQxMWJiY2ZiNjE0N2UwYzM0YmE0YTQwZGE4ZjI3ZmY4NmYyMWQ4ZmNkZjhkNWNjNmE3YiJ9', 'eyJpdiI6IlY3OXJUKzhNcTN5OFBZK25HUjhENHc9PSIsInZhbHVlIjoiVkl1V0srcmtxQmN0ZSsrWVZSSFdMQT09IiwibWFjIjoiMjZjZmJlYTYyMTFiYWJkN2YwZDQxZTdjNTdiYjRiMWYzOTkzODFhYjViNTNkNTEyNzg5NGFkZjE5NDZlMjM5NCJ9', 'eyJpdiI6IktsUVJrMlwvY1lIQmZ0WkFKUE9IZkJ3PT0iLCJ2YWx1ZSI6IlJUbTRBM0pBYUZ5TkpuUTY5NUtSOHc9PSIsIm1hYyI6IjM2YzI5OTllMjI0NmJmN2RkOGNiMDA5NGJlYzRiMzgxYmJkYjljZjU0NjVlMmY2YzRkMmNhNDUzNjAwYzRhMzQifQ==', 'eyJpdiI6InZIS1p4bHAycWlIaDNnT2ZCMVJPOUE9PSIsInZhbHVlIjoiNGorSVRGV2dPWWF1cmltWnlIWHh2SGwxdEpHWnhGSmZSR0ptQTJLcE9aND0iLCJtYWMiOiJmY2JkNzAyZjMzMGU1OTFlNzAzOWMyNTc0NTczZjY1MzI0NzI3OGVlMGRjMTA1NWE4YzE3YTZhNDg5M2JiYmE1In0=', 'eyJpdiI6InEzdVpmdkRsOE5RVkJqd2pVS2FBQkE9PSIsInZhbHVlIjoiektCWVl6RGdaMlpiTXB5YjU3cXJOQT09IiwibWFjIjoiNGViOTdjOTVkYWFiNjJlNWJkMWUwYjNlMTc4OTRmODNkMzFiNjI3YmE4ZWJhYTU3OWQzYTZkZjNmNzRjODBhOCJ9', 1, NULL, '0A5A4887-814B-4275-925E-217B50D6ED31', 0),
(29, 5, 3, '1326.94', '1326.94', '2019-02-10 02:37:31', 'eyJpdiI6ImJudlYrNUdPNDhHclczWjllZCs0Znc9PSIsInZhbHVlIjoiSFczYmZmSVhqeTJcL2xUM3puQzhEVmVFc3VkRERnTlNYZUJqZHBRMndqb0k9IiwibWFjIjoiMWJmNTAwM2NiZDhlN2JlZDZmNzQyMGJmOTA3Y2YwNTZjZjFiZTk2NDYwNzUyOTk5NDRjYmE2NDlmN2I1NjYzMSJ9', 'eyJpdiI6InNpTGx0YllQTWxqWm5seGVvTlNXaHc9PSIsInZhbHVlIjoiRGdocldYTkVDdUZpOWVsc3dpRGZKbDJ6ZjhzOTNNNlMwcE9wRFZhTjY5ND0iLCJtYWMiOiJiNjg1MGUyNjZkN2NkYzMwODM2MDQxNTk3ZTE4N2VlNjU5ZTlhZmQxMjBhZjgxODEzYzRkYjZjMzg2MWYwYzllIn0=', 'eyJpdiI6InV1emd3aU5qVjlqQlZrNVdoSE8ySVE9PSIsInZhbHVlIjoiM0JvMER5SFduUDcrTjk1MWpHcHJQdz09IiwibWFjIjoiMTRlYjdjNjgzN2EyMWViNDNlZDZiY2Y1MTUwMjJlNzJlZWFhNWM0NDczMDQwNDUwNTNlNTZlMWMxYWVjN2IxOSJ9', 'eyJpdiI6IkhtdzBDQkVuSlwvTzVcL01OZEt0Ympldz09IiwidmFsdWUiOiJrNkZsQ01HMHdCb3VEbzNGTGFseEJmVVN1MDE5NlwvaGIzQ2lCeEdZdGg4dz0iLCJtYWMiOiIzZDBmMTdlMTFiZWQ3NGE3MGY3YzM2Mjk2NTg4MWUxOWJlMDBhYjY0ZDYyYTEwMDA2OTIzOTI3YjZlODQ5NDZhIn0=', 'eyJpdiI6IlBIcTJ1MGNqOFpXc0Eyc05cL1hQbFVBPT0iLCJ2YWx1ZSI6Ik5YeGRVVHZxWGpMYVU3QmQ4MllWa3c9PSIsIm1hYyI6IjVmODZjZmIxMzkxODY0ZGFiODc3ZjgxM2JkMjAyZTY5NTI3MzI4MGViMjcwMzBmYjM2YjU5MjNhZDk5NDlmMzEifQ==', 'eyJpdiI6IlwvcVNwekNISUJvWFJycE0wWFkreU1nPT0iLCJ2YWx1ZSI6Ik1jTjBrTEg0eXFCN2F1ejZlc2J4alE9PSIsIm1hYyI6IjIwZDkwNGY2Zjc5ZWQzZTk4MjUyYzg5Zjg5ZTcxNGM0NjFiNjNkZjU4NGM4ZDAwMmZiMWZkOWQyMTJlNDU0ZWEifQ==', 'eyJpdiI6IlFSbjNzSWpkYlMwQXoyOExkSEgzVHc9PSIsInZhbHVlIjoiczZBQmZQSlNiZG94SlBHS2FhOUN5dz09IiwibWFjIjoiNzEzYjVlMGQ2YWQwMGM2M2EzOWM3YjlhZjEzYmY2MWJhNjA2OTkxNTE2NDNiNTc1NjBlMjhmNmYzODc5ODk0NSJ9', 'eyJpdiI6InR6NDhYeE9VclwvOXNlc3ZoWHJaaUdnPT0iLCJ2YWx1ZSI6IkY1VkhCak5XR0RNNVM0OTA5WUY0V0hucEl6RGVrOVVYWW51b2ZWRjhNWk09IiwibWFjIjoiMGJhNDRlYjQxN2U3NmE1YTYzMzVhNDdlMDI2NmE2NWQ1YTM1MzY3NzYyODM4ZGE0ZGYzODA3ZGY0NTEzODcwOSJ9', 'eyJpdiI6IjRyM1pPOER4bHYxOUhud0hFYWU4cFE9PSIsInZhbHVlIjoicjBGNmhZdTgydGVxM01ZOEczM0g0dz09IiwibWFjIjoiYmE2NmE1OGNjZWM4ZDFlNjQwNWZkZmYyNzFkYjliNGFhMDA2ZmEzNTc0YjgwMDZmMzc0NjhkYTgzNTk0YjBkZiJ9', 1, NULL, '06754CA4-EBB7-4663-BFDC-786DE9D824C5', 0),
(30, 5, 3, '1326.94', '1326.94', '2019-02-10 02:37:37', 'eyJpdiI6ImVtc29BRWp3YzhsVmF4Z2VsMVM1cFE9PSIsInZhbHVlIjoiQW4wZFd4ZVJJYjZvWU5ENmRKTzR1ZGFocUxvTE9aTklqSTNTZWhiTzd6bz0iLCJtYWMiOiIzMmExMDc0NDVjNjQxNzQ1ZDNhNWViM2RiYjBiMmZiMTg3NzRiMzc5N2Y4ZTgxNzY0MzhkNjU5NjUxZGQ1NGQ4In0=', 'eyJpdiI6ImgwU0NFblZGbGx5bDBFampoRG5uVVE9PSIsInZhbHVlIjoicUJ2dUdHcG4rR2hORnhvZkxNTmdsVEMzYkxmY3RPSnVoN1wveGpTd1p6VTQ9IiwibWFjIjoiNTY2OTBlMWUwZjNmYTVhMzdlN2RmNTc4ZTNhMmZhNDgzYzk4OWY1OTk1ZTY5NjQxZjQ0MmJiYmRiZjc1ZDYyMSJ9', 'eyJpdiI6InExYUtYVUdic3hlNFlmSE45V01QdGc9PSIsInZhbHVlIjoiVDZiTFcxZmttQXdzN21jTDNqRTlmZz09IiwibWFjIjoiYmU4MDRlYzA4ZTE1ZjIwODg3MzdmZjc3YzBhYjExZTBlMGY1MWM2MjM5YWI2OTc2YjM5NTQ5Y2EzODg5MDJjNiJ9', 'eyJpdiI6ImtJRGo5b1wvTnAyYUxEWm5VMldyQWRnPT0iLCJ2YWx1ZSI6IktVbVBHdEUwMWlpcWIxVEZNdVdJcTl1anhnYTJKcVlyRkY5cmFJajN1QTQ9IiwibWFjIjoiYzYwNDNiOGQxMzMwZDRmYWQ0MzllYTFmZWUwYzMwMmQ3ODc4MTY4ZjdjM2EyOWM0NTVjNjc1YzExYzFkZDg3ZSJ9', 'eyJpdiI6IlwvQjFuUVI0V0Q2OFlKSW9JeDNRa0N3PT0iLCJ2YWx1ZSI6InRGcitpdExTVG5nRmRFWHdJb25Ta3c9PSIsIm1hYyI6IjAwNDZjNGQzYjVhYTk1NjM4MjhiMzgxZmJiMGU3NGVlNDJkN2JiOTlkNGRmYjI2YWQxZGJlOTU5MDlmN2E0Y2IifQ==', 'eyJpdiI6InNRcGd0eDNtXC8xSjhSRDlSZFkxRWlBPT0iLCJ2YWx1ZSI6ImFUOWs3RFkyRTVETDd5NUttS3VWblE9PSIsIm1hYyI6ImJlNGM5ODU0MzA0OWI2NDk0MjdkNzI2OTY1ODcyODVhNTk4MjhkMjhmYTg5MTVjMzQ3NTU2MGJhZTgwZWIwOWUifQ==', 'eyJpdiI6Ik5adTJDWHdYSHQ2WUJ4NHczQTRaNmc9PSIsInZhbHVlIjoiMjI4YU43UVJvcFQ0ZXJid3hERm8xdz09IiwibWFjIjoiN2YyY2ViMGYyZDkyNjFmZDk2MzdiYmYyMjI4ZGY2MDEzYzQ1YjllMjVkMzQ1ODc0Zjk0Yzk1YTBkY2E3YzgwZSJ9', 'eyJpdiI6IktzRllDZnIrcjh2VTFcL0xIVXlUbFpBPT0iLCJ2YWx1ZSI6IkRUWDNFMjdlczM4bjRLNjZ5Mm9xUlZRaDEraDRIQ3FCXC9aK1lSRzFLNWM0PSIsIm1hYyI6ImFiZGM1NzY0ZDdkZjUwYTA5MjU2ODJkYmRkMmM2NjkxNTVjOGQ4NzJlNDRhODI2MzdiNTBjMDgwZjZmMTIzYjgifQ==', 'eyJpdiI6IjROSDlMM3cxbHZmRVFOUnlGQkYxdVE9PSIsInZhbHVlIjoiendGWE9tYXVvSHN2UjAxdjZHZWx3dz09IiwibWFjIjoiNGMyNTllNWMxNmViNjUzOTVmMmE1YThmNWY0YzQ1MThkNmU4Y2Q1N2UzYzFhYTk1Mjc2YTAzYmU2ODExOTYwMiJ9', 1, NULL, 'FA66D78E-4AEB-4292-A25B-ADB4A1AC096D', 0),
(31, 5, 3, '1326.94', '1326.94', '2019-02-10 02:38:57', 'eyJpdiI6IkFhdHR6emFaZW1LditZQW9YYVo3XC9nPT0iLCJ2YWx1ZSI6Imk4NU1BZDhyRVdKSDU5WEpNdGhmVFR3SzJHQm50SlczSTNUZUFGbTBRc3M9IiwibWFjIjoiNTI4MzJhNTE4MWY0N2QzNjU4YmY2OGQyZjQyZGI3ZDI5OTNjYzU1MTljMTNkMzhmZjRiZmMyODBhZWVjZDBjYSJ9', 'eyJpdiI6InhVREVuRkZXcTlQWGdTVHFXajNcL3N3PT0iLCJ2YWx1ZSI6IndrQXdQaWtqNEJKTEJDNk9CbjkrcW5HWEpWT1BURHEyV2pZSmlveUdMQlU9IiwibWFjIjoiMWEwMGE2NDVhYzllYWZlYTBkOTRiMjQ0NGJjNjIwZjk1NWVjZjhlYjkyYTkxMTYzZGMzZGMxNGFjYTZkODYyMyJ9', 'eyJpdiI6Imk1dGU3SkI0a1Q5OWkwZmdNdGY5VXc9PSIsInZhbHVlIjoiYTVLblUrMWtSYlBES2VOOXAwaEphUT09IiwibWFjIjoiNTVlYzg4ZDNmN2I4MTUyNDM5ZDA3N2YwYzUyNDg4ZDQ4ZGU1YTJhODQ4NDNjMTllZDY4MWVlOWM1NTNmMTUwNCJ9', 'eyJpdiI6ImlNR0JMVldxZkJMMkwwWXVGYk1HOGc9PSIsInZhbHVlIjoiNGREd0tjTGsrMTZmcUpoeE8yMzNYNVlWQXVuZTlNOFRISWhySk5DQUNEdz0iLCJtYWMiOiI3YmRiMjJlNTdhOWJkZDk4MTg4ZmMwYTJlMmUxZmI1ZTI0MmQyNmQzYmE0MmM2MWY4ZDY3ZTdhNzI2NzkxNjdlIn0=', 'eyJpdiI6IkVnZlNiNlwvaW9IXC93MjhIWDNZM3AwZz09IiwidmFsdWUiOiJhU1RZdU9PVXpPUWcxeGNnelY4WVlnPT0iLCJtYWMiOiI0MmJiOGQ1NmE4NjY4MjRiZDQ4ODIyYmU5Yzk2ZTY0Yjc0NmY4OTNjNDI4OWY2MzRjZmVmMGVhOGU2OTFiMDM2In0=', 'eyJpdiI6InFacjhNbURzeEg5SjJSMkVVYktoUVE9PSIsInZhbHVlIjoiekFkeUlVOHdwNFJpZVB4VjM4aXJuUT09IiwibWFjIjoiMWZmNDFiNWE4MTI0MGZhM2RkZDk2MDM3NjQxZDc4ZDhlMjdmZDAxMDUzZjJmZGRhNjRlMjc5M2VkZGU0ZGQxYSJ9', 'eyJpdiI6ImpLNEY4R1ljTVppYmVGZ1dFWm9ZUkE9PSIsInZhbHVlIjoiSGFvVXFxakYyVEFHTFVib1BCa0dCUT09IiwibWFjIjoiNmMzMWEyZDMyMDJiOTIwZjJjNzY4MjhlZGU3NDBjMDIwMDZmMzk5MGYyMGM1ZWEzZGEzZTg5NmY1MzcyY2Y3MSJ9', 'eyJpdiI6InZWOEpVbVlQZnI4ckEzTldOTksxZlE9PSIsInZhbHVlIjoiNzhtV0hOcEU1MmZtYnFiaXJkQTdUSGl0Z3h3Sm5mTllRVVpHREFEZDVtWT0iLCJtYWMiOiJlYmY0NGIzMWNhZWI4NGEzNzhmNjgwZmU2NjdkNmRiZDQ0YzgxM2I4NmMwNjQxYjQ5NWE0M2FiMGUyMjM4MWViIn0=', 'eyJpdiI6InZWN0g4MmRzVU81SEc1bm1hVDFcL1pRPT0iLCJ2YWx1ZSI6Ijl1cWtGYTFHQzNkYnpOZXcwd1Z4UHc9PSIsIm1hYyI6ImY4OWZhM2Y3OGEwNTZmOWM0ZmEwYTc5MDQ4OTQxZGYyODUyNzVlMmY3MzM5NTg2ZDA2YTVmOTQ4Njg0MDk1ODMifQ==', 1, NULL, '0522F372-5E21-4EC5-95D2-6D9BD7F6EA26', 0);
INSERT INTO `commande` (`rowid`, `rowid_client`, `rowid_periode`, `sous_total`, `total`, `date_commande`, `email`, `adresse`, `no_app`, `ville`, `province`, `pays`, `code_postal`, `telephone`, `poste`, `notify`, `facture`, `identifiant`, `paye`) VALUES
(32, 5, 3, '1326.94', '1326.94', '2019-02-10 02:39:18', 'eyJpdiI6IkJ6Qk9VbnB6KzlHQ0Q1UUVHSWhmQ2c9PSIsInZhbHVlIjoiN1pxTUxCc2lqZlc4XC9oQTE0b2gwNGhRWU9yVlhjYUp1eW9veVp6OU1hVFk9IiwibWFjIjoiZmNkYWMxODg2N2M1YzY0YjIwNDY5MzAwMWMwYWQ5MTA5ZGIzY2JiN2RjNDMxZjIzNGE0M2MwOTZiZmZiZGU1ZCJ9', 'eyJpdiI6Im5CVmo2TGhZMmZLWlVtQWNoM3BQWXc9PSIsInZhbHVlIjoiRVdEakh2WnZCZ2ZRRjhJajdjR0N4azFNWmJBWUx6VWEwaXd6NkRlVlBzND0iLCJtYWMiOiJmMjZkNDY0ZTQzZTg2MTZiZTU1NmNkMjQyODliY2QwZTM3NjQ0Yzc0YTIxZTQ4NTZjZjMwODcwYWI1YWJkMWZhIn0=', 'eyJpdiI6ImhrSlJcL2NHR1lWakdDV09uQlVyNUt3PT0iLCJ2YWx1ZSI6ImVJR01zaFJOUmlDWE1SbkxsUnQ3Ymc9PSIsIm1hYyI6ImZkNmNiMmQ4NDRkYWQ5Mjc5YmE1NDdlNDQ5MjUxN2FlNDVlN2Y5MmFlZmJlNTVlZGFhZmE1NWZmMjljZjBiY2YifQ==', 'eyJpdiI6ImdTWTZSeGowSlRFak5QbndoaWNoc3c9PSIsInZhbHVlIjoiNEsybVVQQTNPUUFyK2NJXC80RDgrQmxlbHpYK2pZTUJwMDhoRXlSSUEwYjg9IiwibWFjIjoiZTFhNjFjZDU0ZDdjYjEzNjI4ZjRlNWVlMDk3OGE2MTc5YTQ2NjljMjk5MjhjNTJlMjg5MGNhZjQwNWZmMTAwMCJ9', 'eyJpdiI6Im5HaUFKS3RDSXQzM0F5c0NSSnZCVXc9PSIsInZhbHVlIjoiblZZUzBVRXZmSlZuWk1IaU51Uk1kdz09IiwibWFjIjoiZWY3MGM0OThlZjI0Yzk5ZWVhZmMwZTcyODk1MzA0ZmQzMjg1Y2I3Yzk0NmZjM2Q2NjBmMDRkMWUwYjk0NDhlMyJ9', 'eyJpdiI6InhYdTd5UkFmQU44WU1MdGQxM25VaGc9PSIsInZhbHVlIjoiZ21EOTdLM2JkS1wvYlwvNlh2cEhSTE5RPT0iLCJtYWMiOiIyNzA1MmU1MmQ4NmQ2NjkzYzk2ZWMwOTNiY2RlODE1NGM5NWE3YjgyY2JiNjY5Y2IwNjdhY2I4MWM2YTkyZWU3In0=', 'eyJpdiI6InpiQlwvZ0grMFZqdDc0bjlVeXhQaFh3PT0iLCJ2YWx1ZSI6IkVGWHhkVGVGS3IzXC81Vk8zaDNqUDV3PT0iLCJtYWMiOiIzYjE4MmNlZTA2Y2QzMjQ0ZDNjZGRiMmRhYjViY2JjMTMyMDgzYmY1YTE2NWFjNTEwYjlhYzQ5N2FhYjRiYmEwIn0=', 'eyJpdiI6IkJadE8zd0RPT3RsY0xDMHRNNUV2ZUE9PSIsInZhbHVlIjoiaDFleEJWaVdwSXNqUGFIbXlOOTVlTnFDancrSEVXdHhKS05lWWk5bFlMZz0iLCJtYWMiOiIwZTU2MGU0YTQyMzVjZjIwMTg3ZjJlN2UzMzg5NGNjYmJlZTMzZDBhNDllMjIwN2U0M2NiMzRmMzI1YjJhMzJmIn0=', 'eyJpdiI6InVHNlV0QzJYbEc2bzdvamtGb2pwNVE9PSIsInZhbHVlIjoiOTBQNEhic0NZQVpYV1ZkcWo4UmNJdz09IiwibWFjIjoiZGI5NGJhNjIyNzNhMzRmNzRmNjcyY2EwMWZmMjM4NzI5MmJlNTNmMTViMTYxYjA1ODk4YTVhNjA1OTBmN2QxMyJ9', 1, NULL, '27F2BAFF-60F6-4E2B-A56B-1B60CE2155D4', 0),
(33, 5, 3, '1326.94', '1326.94', '2019-02-10 02:39:40', 'eyJpdiI6IkkxcXl1bHhVeUhFTEppNkl6Rjd0N3c9PSIsInZhbHVlIjoidjVSOUZWYVVoZ1BISVM5eUFrbThlZXd5NjZIc01rSW5MdGlvbUowK2tKWT0iLCJtYWMiOiI4ODhmYWE2NjBhM2EwOTIwOGEwMjU0ODIzZGQ3MDNmZWFhOWE5YTNjYWY1ZDgyM2RlN2E5MTA0OWIxOTM4NTAzIn0=', 'eyJpdiI6IklrVUlNY0tSQnlvYm1KMEN5M3hlMXc9PSIsInZhbHVlIjoiRGc2aEd0WFwvRmwrenlWTjRjSjdROUhkdG9tbDAxZTlVQjJyRllvaGtZMGM9IiwibWFjIjoiNDNjMmRhZjcyYjFlNTQxODA5NzRlY2U3ZGRjOGYwOGNhOWJmNzM0ZmEyNWY1ZjcyYmI3ODczYjg2NGY4Y2EyYSJ9', 'eyJpdiI6IlNvRDJEczJpb2RBbU1na0p4Q2V0MGc9PSIsInZhbHVlIjoiaEVkaUJLYjlFOGVKVnFjTXJ3SHBvQT09IiwibWFjIjoiZDZhYzIxNzA2YjM4YjY3NjZjZmQwMTQwOTUxODQzMmI4ZGY2Zjc2OTdkZjEwMDcxMmFlNWYzZjFmMjYzYWI1YyJ9', 'eyJpdiI6IjVDNTc4SWNiSXViNmdSV2NucXloMGc9PSIsInZhbHVlIjoiMnlXc3E3TEhqbVRLQnZhYk16Qit4bkM4dDBXbEI4bkFGZVo1QjEySHA5Yz0iLCJtYWMiOiJkNzIzZGJkZDEyOTY3YWEyN2U5MWI5Y2I3MjMyN2I2Y2JjYmFiMzZjZjM2ZTI3ZTdiZjcwN2ZjODFiNDQwNjVkIn0=', 'eyJpdiI6Ik53NkxSWTdTd0tNTEtCN1Y5eDBHanc9PSIsInZhbHVlIjoiTTZyZytJRTlHcHJkSzBPZkFEeDBPUT09IiwibWFjIjoiMWY0NGM4ZDZlYWE1ZmFmMGYwZWI5NTNjMGRmODdhM2Q1NDFiZTg1ZjI4MWE2MjgyMThhNjM4MGE5YTQxMzU0MSJ9', 'eyJpdiI6IjFudVVwRndDdG9PMGJ2cnd4WVI5WkE9PSIsInZhbHVlIjoiT1ZLSXFGZWRSVXI0cVhnZWtnU2ZIdz09IiwibWFjIjoiOTZiMGJmNTk0YTEyMGUzZWE2ZjZjZmE5NzI0ZDBjMzhjNWY3NjQzMDQzZWM2YTUxZjZmYWYwYjVkYjEwYmZhYSJ9', 'eyJpdiI6IjVtWUQyeG5OcVNWbGQ2dkpwOFBiXC93PT0iLCJ2YWx1ZSI6Iit4VjkxNTkxWEdSUW9mVGNyRkp6Unc9PSIsIm1hYyI6ImYyYWM2MjQwNzlhNWRiOWJlZTcwNjg5NzYyOGUzODNkMTY1OTQyZWEyZjA3NWNiNzhhODNlZDI5NmRjMjFlY2QifQ==', 'eyJpdiI6IldGVUV6VEozSVRrdHF6bzgrWDduS2c9PSIsInZhbHVlIjoiVkMwUEpmWDMrd3EwS0RmTTdpN3NsUE0rSVN5WWx0eU5OWXM3Q3RJR1Y5bz0iLCJtYWMiOiJkODYyNzY1NDA5MmY2YzA1MjA4MmNmODNjYzU2YWU3YWIyOGQ0MjY0MjcwYjcwMGNkOTY1MTE3NTFkNGFkNThmIn0=', 'eyJpdiI6ImpmTTc2SFNsa1VoYnNHOVo4NW4yc1E9PSIsInZhbHVlIjoibEFTOURPWXVoeFJRXC93Zzh0eU95RVE9PSIsIm1hYyI6Ijc4NGI0MTRkM2EzNDk2NDFkZTM3MzM0MjY1NjI2ZDNkMDNjYjllMTg0YmJiODgwYjM3YmEzMzMwYzI2YTJkMDYifQ==', 1, NULL, 'F0835A96-5B0F-4248-811C-04025EB15001', 0),
(34, 5, 3, '1326.94', '1326.94', '2019-02-10 02:40:11', 'eyJpdiI6Imh2eHJtT3BHZVZHSFF0dEZXbmtOcVE9PSIsInZhbHVlIjoiNFdVc2tIMUVSOU8wVXRwZHM5VGZcL0hCZ1FTc09DNmJuaVB2ZWZGXC9zXC9kbz0iLCJtYWMiOiI5YTc4MDFjMTY3NzViZjg1NjM3ZjNkNjllMTg3Njk1MzIwODdhMDA0YzBiODBkM2I4ZGY4YTdlOGFjYmE4NmUxIn0=', 'eyJpdiI6IlN4OEErbmdOY1BvZHpjWFYxUmZoZkE9PSIsInZhbHVlIjoiWG1uVFc4YUZiTktuWHNVMktcL2R5SmQ1YmVPSmlWZmtWa2VaRTJ5UWlSSVU9IiwibWFjIjoiZmY3NDBmNjI1NzNhOWViMzBhZjM4ZDBiYzg4OTljMDE2MTY4Yzc0ZWMxN2ExYzdhOWFiYWRiODI3MTNiNmExOSJ9', 'eyJpdiI6IjhOWUlSYUExcnZzTHpTYWE3V3YzNXc9PSIsInZhbHVlIjoiZmFUV09PU2hNek5LaWJRcXdNbDBjdz09IiwibWFjIjoiMDY1NWMxMDdjZGU4ZTg4Y2RjYjk4NzMwNzI2MDRiYjFjMWY2YTNmNDYzOGYwYmU3M2U2N2EyZjkyYTU4MGQ1YSJ9', 'eyJpdiI6IkRmVTlHb1hsWDBBZTJmWGx6T2s3YXc9PSIsInZhbHVlIjoiVmFEVkY4K1hxUmJKT2ZnMFdWcXBRWjdYRWhjNnpENFF4OWdNMHJlMXpyQT0iLCJtYWMiOiI3NjE2N2Y4NTQ1MzY5NGZiZjUyNjc5YjNjZWMxZTFkMDAxN2JkYWFiODFkYjc2YjJmYzdlNWIzZjI5MDgxNjM2In0=', 'eyJpdiI6ImYrbTJcL1Rra1ZLUlhkdmNZWEI1S053PT0iLCJ2YWx1ZSI6IktjOXlHcmdBNzlyNUVjZkR3UEFhU3c9PSIsIm1hYyI6ImNjZjIzNzcwNmMyZjM5OTIzODU5NzYxYWE2NGEwZGVjZjRjZjVjMWFkMzUyZTNiZDdmMjQ5MDg3ODhkODY4Y2YifQ==', 'eyJpdiI6ImdZTnIwdnBQTEZXSXRkVUxHK3FWZmc9PSIsInZhbHVlIjoiRThCdHZTMnU3Rm5KblhjV3VXVGMwZz09IiwibWFjIjoiMmMwOTU4NTQ5N2Y3OWFmYjM3OTNhMjhlMzJkNTg3ODFlYzIzMTYxMzdkODI4YThlZmFiZjE1ZGFmMjgwNmUxMiJ9', 'eyJpdiI6InNaQ3YrRUtMNXZJSXMxcU5OaFBwaFE9PSIsInZhbHVlIjoiYytFNWhCU3lORG5TNnJlSVluWlAwQT09IiwibWFjIjoiODY2MjA0NzE0YTU5YmE1NzhjNmYyYzRiNjk2YmM0NmZhZjM5MTVkZDRmZDlhOTYxZTEyMzljMTc3MDk0YWZjNiJ9', 'eyJpdiI6InAyNWlcL1JqV3lnZGJsWWk2bnJmVW1RPT0iLCJ2YWx1ZSI6IlB3SllBeGFIUVFlUnVcL0lKVjZNRlR5SUU5OXVLeTRJbUZHTEJweWFJdWZjPSIsIm1hYyI6ImZiMzY1OGIxOTNlNzZhY2I3MGZiYTA2N2EzNzRmMmQyN2JlYzIwYTRlOWQ1MGI1MTEwY2E0YjY0NjMwZTZhYmEifQ==', 'eyJpdiI6IlhCdlhPUm1vTERtb2ZYZ2VzMmlIY1E9PSIsInZhbHVlIjoiSkNCdVdJaURxeXVENTVXYmNLeXlEZz09IiwibWFjIjoiMDBjNzNiYmMyZDE5NWY1ODEwY2JiZGMzYjNiOTZkYTk4MTczNTc3NTkxMmZjNWFiMWE4ZDYzNzYzZDEyODE0YyJ9', 1, NULL, '4DABDE22-DB16-4B16-89D1-2CBCEF78815F', 0),
(35, 4, 3, '58.64', '65.50', '2019-02-13 14:09:25', 'eyJpdiI6IlR6RFlTcmdrb3o2MEIxZElwb2t1cWc9PSIsInZhbHVlIjoiU256U2RzMEtUbDNwcWFwTTZJUVFkdTVQWkVLbWFJXC85Kzl6UzlvMVBTK0k9IiwibWFjIjoiYjhlMjYzMzUzZDRhNDM0NDhhNDQzZDg3YjUwMWE0OGU0M2Q5ZGNjNjhkYjQ1Y2IwOTFmYTNjMWY1YjZhZjk3ZSJ9', 'eyJpdiI6IlwvMHZWTjJJQWxOVTB1bjdxZkJDcG93PT0iLCJ2YWx1ZSI6IktIV3FVQ3ZUY1RnXC93ZXVXWkM0Q0tZQkxYWVwvZ2o5TVV1MHJla2UrNDJTOUMyZVduMzNVM1FVMUJ3M1kwdncwViIsIm1hYyI6IjI0NDE4MTdjNWE2ODEzNmI4ZDMxZTQ1MWE4YmFkMzA5NGU2NzBmNjJhNWY4MzBiYzYzNzc2Y2QzZDlmOGRiY2MifQ==', 'eyJpdiI6InRvWE5DMGgwN0tOYkdhVEhOS2JZMVE9PSIsInZhbHVlIjoibWdET0lHb2lpV0JaQmlFZEZERjJvQT09IiwibWFjIjoiZDhjMDA3YTNkYTE2MGIzYjNhMDVhMjUyNDM5NDM5MTIyOWU0NWNkMjk3ZGZjYmRjYzMxNzQwMmM3YmFmMWU0MyJ9', 'eyJpdiI6IlRFT2dGanlEMnJOb3dFdytWZnpoYlE9PSIsInZhbHVlIjoielJoR1k5elRaNDVkcTcrdVRYSVM0U21ySlk0Y2dzSnRIQWpUVXBycE16Zz0iLCJtYWMiOiI0MzNjODEwNGUzYmE3YTEzYTUxZmYzZTczZjhlMDhhMDE5ZTA3ZmExNjkzMTczNWQ1Y2QyNjUyOTE1ZWJkN2QyIn0=', 'eyJpdiI6ImRpNXV3b3dtVXhoQzBBVUNLd2liNWc9PSIsInZhbHVlIjoiWFBvaVhVc2J6XC9cL0d2dlVYamZPbm1RPT0iLCJtYWMiOiI1OGY1MDJiYWY1NDRiZDQ5MGQ3NTMyZmI1ZDY4ODVmZjNlZmIxOTZjYmQ3ZGE3MDUwNzhlM2Y2MWI1YWJjY2M1In0=', 'eyJpdiI6Im8xaVpCa2c3S0MyU0ZrR1ZnXC9maWxnPT0iLCJ2YWx1ZSI6IkYyNzdHOWJCM3pENk05aU91cVNxYnc9PSIsIm1hYyI6IjBkMzEzNzNlMDIzMTMwMzY4YTQ2YmViNTZhNzZlYzA3NTQ1ZDg3Y2EyOGYwMTU5NDQ1OTc4NDNkMTUwM2E5NzAifQ==', 'eyJpdiI6IlU2ajFBVTJwMDVIUldiN3hsWXpqSmc9PSIsInZhbHVlIjoiSU90bFBrUGM3VE1TWms1MVBicHhFUT09IiwibWFjIjoiYjY4OGY1Y2FjZGE1OWI2ZTIzOWI2MzQ2OGRmMGE1YmJiMmMzNzRmNzY0MDJlMGEwZTkwNmRmZTdjZWJhZDE2YyJ9', 'eyJpdiI6InN4MzltSHRxdTc4UTdYeDVQYlZYeXc9PSIsInZhbHVlIjoiN1J6T3YwYjVJSk9XaUFlZDN1ZlwvVGJsNFJqUE9xRmlNUWFqNGp6STJjVE09IiwibWFjIjoiNDNjYzc0NzZjMTJhZTFkNWQ1N2YyY2MyMDM5ODc3MDU5NmU1ZjlmZmRjMDM4OGRjZDFmZGMzZTE1YjViMDI0OCJ9', 'eyJpdiI6ImduUWxaV210d21cL1hXMDdzVVwvZm80QT09IiwidmFsdWUiOiJlc2thMU5pQ0t2RnBlYWdxNmtKQzBRPT0iLCJtYWMiOiJjNTc2MzBjOGU0YTg0NTdiYTI1M2E3YzQyMDU4MTNhNzM5NWE5ODM2YjExYjVlZmVhMmI5NjNiYjRhNDFkZDA4In0=', 1, NULL, '97E8358F-1913-4AFC-B44A-097357A50512', 0),
(36, 4, 3, '58.64', '65.50', '2019-02-13 14:12:35', 'eyJpdiI6IldIN1VDS3BPcXByT1BWNHppcEVUWUE9PSIsInZhbHVlIjoiNmJ4NmNoTzhreFNMQWxTZEEwM0xuTXNhV2RyN0tDVlwvWFdheHZ2a0o1bHM9IiwibWFjIjoiZDQ5MmVlZjc4OWNlMTNjMDMzMzUxYTI0NmM3Mzk3OWQ2ODI2ZDUxMTJkYTY3MWI5YmEwMDJmODI0YzQ4ODQ2YyJ9', 'eyJpdiI6ImY3OFBSWUdpMzlKTDJGM2h3S3BLYVE9PSIsInZhbHVlIjoiUUFwaHhUOG5sa0RoTUFLS0ZWenM1M2ZRTk01RUZGVERMM3NrWVBRT0VsK1JIV1l4NVk5RTdQY3JqSHlPdHJVMyIsIm1hYyI6ImM0ZTBlMTEwN2FiMzkxY2Y3YzcyNDVhMDUzMzZlMWEwZTEyYzNhMDFkZDNhNDRiMTU1YWEyMTM3MDc1NTkyMjMifQ==', 'eyJpdiI6IkRJNE1VeExEclJTbVlJMHBaQ0RmbkE9PSIsInZhbHVlIjoiNUdGWW80QkRJejl2Wm4zd3RRZ0p2dz09IiwibWFjIjoiN2UxN2U1MjYzODA5MDljMWRkNjU5ZmU0M2I4MTk3YTU3MTUzNTIzMDEwNjdhN2EzNzBlOTY0Y2E0YWY3ZjJjMSJ9', 'eyJpdiI6Ik8wZm5DcjM1RnJFbURNMmVxaHJwaGc9PSIsInZhbHVlIjoiaFpFMTZjdTY2b2l4Wm5qN1ljRHp2d2lVU3VsemJqdVBzdFJjRHNnQjJjUT0iLCJtYWMiOiIwNGQxODU5MDQ3N2M1M2U2NmZjZjIwYThjNjNhYzg2Mjg4YmYzNzU3ZjkzMjYyNGJmYjM2N2RmMzY0NzBlZDcxIn0=', 'eyJpdiI6IjBFcitHK2F4NHFBWjZDNDlSNjJ2UFE9PSIsInZhbHVlIjoiMHBOYTludStid2g2eVFsZGJmTzV4Zz09IiwibWFjIjoiYzFmZDExMWE5MmJjMjVhY2JlMGI1YzA4OGZlNjlhM2Q4NmMxNDgxNWFlMzMxZjk1MTFkNTY4ZmE5NDg5ZmRkZSJ9', 'eyJpdiI6IjZWSERkUWlQUHJkano1V2RIMW5PcFE9PSIsInZhbHVlIjoiVXdDY1lYSzN1MU01ZG5ySld6ZXF1UT09IiwibWFjIjoiOTBjNTI3YzA0ZGRjMzM4Yzc0YTcxMDQzZDg2MWI3YzRkZDU3ZjA3ZjVhOTI3YmVmZDA5MGE5ZGVjNzYyYjkwOCJ9', 'eyJpdiI6IktXd2pGWjJHZmYwUStOZndhRzVjVFE9PSIsInZhbHVlIjoialdHcHpTN0J3OE5xT0FzTXZqb1ZMdz09IiwibWFjIjoiYTgwZjJjMzdmNmI3ODk5ZTM4Y2M4NzM5OTdlMjMzMWQ3NzA4N2M0NzU2YWVlMzIzMTQ0YTk1ZmYyNGE0OGNiNCJ9', 'eyJpdiI6IlFcL1NiencyK2lyQkFaWExJdWRlVERnPT0iLCJ2YWx1ZSI6ImpGczV6Y1F1VjlpR0FGSFwvZUZOb0ZzaDkxNFN5aEZNc0taSkFHM3VtcWhvPSIsIm1hYyI6Ijc5MjBhM2NiYzhmOGVmYjdiZWExNjNjNjk3OGY0ZmFjMmJiNmFjMDhkYzA1MjgyNWE5M2I3N2YwMGJhNWExZWIifQ==', 'eyJpdiI6IlFLMWRUalwvRWNnOUkyd0h6Mko0MWNRPT0iLCJ2YWx1ZSI6Ikk5NThaYkZQRDFcL2Y5ejRHeE9WRHpRPT0iLCJtYWMiOiIyMzJjMDZjODZhZTJjM2MzNzBkOTZlODRkYjc4MGM0OGY5ZTQ0M2M4YmVlNDg4MGMyNjJiZDIzNDhhYjdhOTlmIn0=', 1, NULL, '', 1),
(37, 4, 3, '84.63', '93.63', '2019-02-13 15:27:58', 'eyJpdiI6IkVKXC81RmxHYmxQOXMwR1R3aFpiMzB3PT0iLCJ2YWx1ZSI6IjhKOUdST0dWcElMTTVSOCsxbVQ2UzBuWFwvU3BOK2JaRnpoYStteGRnZGI4PSIsIm1hYyI6IjI3Zjc5YjNmMDcwYTE3ODA3ZGZmYmE5MjRmZjRiYjlhNjFhNmM0MDE0YjMzZTEwYjZhZDg5NjZjZmI3M2QwZDcifQ==', 'eyJpdiI6Im1sM3ljcmdMMXdxQ2c3TndLVjBjQmc9PSIsInZhbHVlIjoiMUtBM2JndmcrMmc0bm9sV0xIc1dramxDYWVMQXBVTEpXUzlzM3hRQVVDeEJTNzJBdnpYVGlxZ1wvRk9YMTlZNVMiLCJtYWMiOiJjYWM1MGEyMzA4NTU3ZTRkNzQ2YWUwNzQzMmE5NGI4YzMyYzNmYjdkYzI0YjljYzVkMzQ1ZjYwNmNkZjgzNTk5In0=', 'eyJpdiI6Imd1am11S1EzN01kVHc4ekFZaldHVmc9PSIsInZhbHVlIjoiS0loK2hlNjZOK3pNQ2dqRWQ2WFl6Zz09IiwibWFjIjoiNmY1YjQ2ZmFkMzE4NWZjOTM1NWU5NTdlZjJlNGQwZWI4NmZhZmUzZmExMTdkMjA5Y2IxMmEwNzM1ZDQ5YjViZSJ9', 'eyJpdiI6IjhQc0FmekVDU0dSZTVzQlhXaXRMMWc9PSIsInZhbHVlIjoiSWZRZERBZFZ0Y3FhbVYzN0piZzlZK0NlYVVYSEU0UDIwczA2cTJKVDVFND0iLCJtYWMiOiJkODI5Mzg4Yjg0ZTVjZDJlNGM0OTE1NzY2MWI3OTgyNDc0M2ExOThjZGIwOTQ5Njg2NjE0MDZiOTg5YmZlMjNiIn0=', 'eyJpdiI6Ilo1dm1uSGl3QkdHZGQreU1GT3NWYUE9PSIsInZhbHVlIjoiNGNOTnBaSEk2ZFFTaEZodTB4bHhIQT09IiwibWFjIjoiOWI3MThmYzU4MjZiYWYwMjM0YmMzZTQyOTRjZTgyYTMxYzNjZGEwMDA1NWMyYmQxODE4MzM0ODBkZDZlOTAwYyJ9', 'eyJpdiI6IlV4OXlDVFI1OUNZcVwvaWdoZDNmMWpBPT0iLCJ2YWx1ZSI6ImtvYkw2Z1JLMHp2QldZVCtJZ2pYbGc9PSIsIm1hYyI6ImZlODI0YmFhMzYwNWVjZDE0MjkyNDBjOTA4MmVlYWMyMGM4NTlkNTUxZWU1ODIyZTg3YTI3NDYxNjg1OGNkODkifQ==', 'eyJpdiI6InQyNUhqWHVJOTBSeThobm1HUG9UeFE9PSIsInZhbHVlIjoiS2o1YTFzRHhBdE5OTGlsS0pzQUp5dz09IiwibWFjIjoiNTAzZmE0YmI0OTE5OGFmNzA2M2RmZTViYTE2OWJkY2RjNTE3MWE1Y2M2NmJlMjJkMTNjNDRiMTUzZmI5Nzk4NCJ9', 'eyJpdiI6IkwrM2VZTk5SZDJEQVZiSllvc0NoM1E9PSIsInZhbHVlIjoiRjQ0Syt6dnh2enpYU2JhdjlmRnFlM2tVOTZXdFZUQmpDWlp3bkZOeExZND0iLCJtYWMiOiJjMjQ0MGE5Y2JlZjc5OThiZjI0YWMyMjc5MTk3Mzg2M2M0MjUzMDIyZDczMDA1NWRlMzIxMzY5YTY1M2E3Y2EzIn0=', 'eyJpdiI6IjVBTG40OEpVYjU0NWx0cUdXaTJOWEE9PSIsInZhbHVlIjoiVnJQcFJDbk56UlZZS01INEo1V1wveXc9PSIsIm1hYyI6ImRiNjRhZGFhYzIxODVhYzRjNDliY2RiZDkyNTQxNDIzMWEyY2FjZTFiYjg0YzVmYmQyZTNmNDNjNTgzMzIxZmUifQ==', 1, NULL, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `commande_article`
--

DROP TABLE IF EXISTS `commande_article`;
CREATE TABLE IF NOT EXISTS `commande_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `commande_rowid` int(11) NOT NULL,
  `article_rowid` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantite_commande` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`rowid`),
  KEY `fk_commande_article` (`commande_rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commande_article`
--

INSERT INTO `commande_article` (`rowid`, `commande_rowid`, `article_rowid`, `image`, `nom`, `format_quantite`, `format`, `provenance`, `allergene`, `perissable`, `bio`, `quebec`, `nom_marque`, `nom_categorie`, `nom_fournisseur`, `quantite_commande`, `prix`, `frais_fixe`, `frais_variable`, `prix_majore`, `economie`) VALUES
(1, 1, 14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 2, '13.94', '5.00', '1.00', NULL, NULL),
(2, 1, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 1, '4.95', '5.00', '0.00', NULL, NULL),
(3, 2, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 4, '19.92', '6.00', '1.00', NULL, NULL),
(4, 3, 5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 3, '14.88', '5.00', '0.00', NULL, NULL),
(5, 4, 10, 'public/products/images/I6XjF0Hwgg9sIX0tqhw7rrjiacSVk3oHcqy0DeXV.jpeg', 'Moutarde de Dijon', '1.00', 'kg', 'Québec', NULL, 1, 1, 1, 'Herta', 'Condiments et autres', 'Produits alimentaires Quebec', 9, '8.98', '5.00', '0.00', NULL, NULL),
(6, 5, 7, 'public/products/images/TmxisxJI9ocq2Ip8TQriNaer4WypYgn9Enuy9HBb.jpeg', 'Café La Montréalaise Santropol', '500.00', 'g', 'Amérique centrale et du sud', NULL, 0, 1, 1, 'Materne', 'Café', 'Grossistes en café Quebec', 23, '14.37', '5.00', '2.00', NULL, NULL),
(7, 6, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 13, '4.95', '5.00', '0.00', NULL, NULL),
(8, 7, 14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 6, '13.94', '5.00', '1.00', NULL, NULL),
(9, 7, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 4, '19.92', '6.00', '1.00', NULL, NULL),
(10, 8, 6, 'public/products/images/fxtpyETXUMXk2OswDJ3T1SparCXp9c4caKKNOa8J.jpeg', 'Capuchons de chocolat 70%', '500.00', 'g', 'Fèves du Pérou, chocolat produit en Europe', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 6, '23.93', '7.00', '0.00', NULL, NULL),
(11, 9, 14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 1, '13.94', '5.00', '1.00', NULL, NULL),
(12, 10, 14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 1, '13.94', '5.00', '1.00', NULL, NULL),
(13, 11, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 3, '4.95', '5.00', '0.00', NULL, NULL),
(14, 12, 15, 'public/products/images/17YmeaiQPt8j48kzvCWmxRqXAzBKtDD3r7r0TetF.jpeg', 'Canneberges séchées au jus de pommes', '1.00', 'kg', 'Québec', NULL, 1, 1, 0, 'Citadelle', 'Fruits séchés', 'Produits biologiques Quebec', 3, '18.66', '5.00', '0.00', NULL, NULL),
(15, 13, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(16, 14, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(17, 15, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(18, 16, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(19, 17, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(20, 18, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(21, 19, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(22, 20, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(23, 21, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(24, 22, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(25, 23, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(26, 24, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(27, 25, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(28, 26, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(29, 27, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(30, 28, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(31, 29, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(32, 30, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(33, 31, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(34, 32, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(35, 33, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(36, 34, 24, 'public/products/images/AuZNwBd9OtKqRZk05XuYGHbiZsuEJZcRv75L3QKN.jpeg', 'Péteu de ava', '1.00', 'unité(s)', 'Ava frais', 'Poil, foin', 1, 1, 1, 'Ava de Catherine', 'Amour', 'Catherine', 2, '663.47', '0.00', '0.00', NULL, NULL),
(37, 35, 13, 'public/products/images/1TpWugpuoiLsRiiFu3ToTpzfZcBhz8dRDzXDL5UL.jpeg', 'Farine de blé intégrale', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Fleury-michon', 'Farines', 'Farine Quebec', 1, '2.88', '3.50', '0.00', NULL, NULL),
(38, 35, 14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 4, '13.94', '5.00', '1.00', NULL, NULL),
(39, 36, 13, 'public/products/images/1TpWugpuoiLsRiiFu3ToTpzfZcBhz8dRDzXDL5UL.jpeg', 'Farine de blé intégrale', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Fleury-michon', 'Farines', 'Farine Quebec', 1, '2.88', '3.50', '0.00', NULL, NULL),
(40, 36, 14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 4, '13.94', '5.00', '1.00', NULL, NULL),
(41, 37, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 1, '4.95', '5.00', '0.00', NULL, NULL),
(42, 37, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 4, '19.92', '6.00', '1.00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
CREATE TABLE IF NOT EXISTS `fournisseur` (
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_fournisseur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fournisseur`
--

INSERT INTO `fournisseur` (`nom_fournisseur`, `actif`) VALUES
('Aliment en vrac Quevec', 1),
('Aliments en vrac Quebec', 1),
('Catherine', 1),
('Chocolat Quebec', 1),
('Confitures et gelées Quebec', 1),
('Croustilles Quebec', 1),
('Détaillants de boissons Quebec', 1),
('Distributeurs et fabricants de mets chinois Quebec', 1),
('Eau embouteillée et en vrac Quebec', 1),
('Farine Quebec', 1),
('Fruits secs Quebec', 1),
('Glace Quebec', 1),
('Grossistes en aliments congelés Quebec', 1),
('Grossistes en café Quebec', 1),
('Grossistes et fabricants de produits naturels Quebec', 1),
('Huile d\'olive Quebec', 1),
('Huiles végétales Quebec', 1),
('Levure Quebec', 1),
('Maïs soufflé Quebec', 1),
('Malt et houblon Quebec', 1),
('Miel Quebec', 1),
('Noix Quebec', 1),
('Oeufs Quebec', 1),
('Produits alimentaires Quebec', 1),
('Produits biologiques Quebec', 1);

-- --------------------------------------------------------

--
-- Table structure for table `marque`
--

DROP TABLE IF EXISTS `marque`;
CREATE TABLE IF NOT EXISTS `marque` (
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_marque`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marque`
--

INSERT INTO `marque` (`nom_marque`, `actif`) VALUES
('Ava de Catherine', 1),
('Ayam', 1),
('Babybio', 1),
('Bledina', 1),
('Bret-s', 1),
('Carrefour', 1),
('Citadelle', 1),
('Danone', 1),
('Fleury-michon', 1),
('Gerble', 1),
('Herta', 1),
('Jardin-bio', 1),
('L-angelys', 1),
('Le-bon-paris', 1),
('Lea-nature', 1),
('Marque-repere', 1),
('Materne', 1),
('Monique-ranou', 1),
('Nestle', 1),
('Nouvel', 1),
('Prana', 1),
('Propiedad-de', 1),
('Sacla', 1),
('Schar', 1),
('Sojasun', 1),
('U', 1),
('vgvv', 1),
('Yoplait', 1);

-- --------------------------------------------------------

--
-- Table structure for table `panier_article`
--

DROP TABLE IF EXISTS `panier_article`;
CREATE TABLE IF NOT EXISTS `panier_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_panier` int(11) NOT NULL,
  `rowid_article` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `panier_article_fk0` (`rowid_panier`),
  KEY `panier_article_fk1` (`rowid_article`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `panier_article`
--

INSERT INTO `panier_article` (`rowid`, `rowid_panier`, `rowid_article`, `quantite`) VALUES
(6, 5, 24, 2);

-- --------------------------------------------------------

--
-- Table structure for table `panier_client`
--

DROP TABLE IF EXISTS `panier_client`;
CREATE TABLE IF NOT EXISTS `panier_client` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_client` int(11) NOT NULL,
  `actuel` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rowid`),
  KEY `panier_client_fk1` (`rowid_client`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `panier_client`
--

INSERT INTO `panier_client` (`rowid`, `rowid_client`, `actuel`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `periode_article`
--

DROP TABLE IF EXISTS `periode_article`;
CREATE TABLE IF NOT EXISTS `periode_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `article_rowid` int(11) NOT NULL,
  `periode_rowid` int(11) NOT NULL,
  `quantite_commande` int(11) NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `article_rowid` (`article_rowid`),
  KEY `periode_rowid` (`periode_rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_article`
--

INSERT INTO `periode_article` (`rowid`, `article_rowid`, `periode_rowid`, `quantite_commande`) VALUES
(1, 14, 3, 13),
(2, 1, 3, 18),
(3, 4, 3, 12),
(4, 5, 3, 3),
(5, 10, 3, 9),
(6, 7, 3, 23),
(7, 6, 3, 6),
(8, 15, 3, 3),
(9, 13, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `periode_commande`
--

DROP TABLE IF EXISTS `periode_commande`;
CREATE TABLE IF NOT EXISTS `periode_commande` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_commande`
--

INSERT INTO `periode_commande` (`rowid`, `date_debut`, `date_fin`) VALUES
(1, '2018-12-17 01:30:00', '2018-12-21 23:30:00'),
(2, '2018-12-10 00:00:00', '2018-12-14 00:00:00'),
(3, '2019-01-30 08:00:00', '2019-02-20 12:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `periode_plage`
--

DROP TABLE IF EXISTS `periode_plage`;
CREATE TABLE IF NOT EXISTS `periode_plage` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_periode` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `emplacement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `periode_plage_fk0` (`rowid_periode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_plage`
--

INSERT INTO `periode_plage` (`rowid`, `rowid_periode`, `date_debut`, `date_fin`, `emplacement`) VALUES
(1, 1, '2018-12-24 08:00:00', '2018-12-24 23:00:00', 'Sciences'),
(2, 1, '2018-12-24 08:00:00', '2018-12-24 23:00:00', 'Hums');

-- --------------------------------------------------------

--
-- Table structure for table `personnaliser`
--

DROP TABLE IF EXISTS `personnaliser`;
CREATE TABLE IF NOT EXISTS `personnaliser` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banniere` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_desc` varchar(1028) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_entreprise` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_code_postal` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_telephone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_copyright` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1_msg` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2_msg` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3_msg` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_article` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_article_plur` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personnaliser`
--

INSERT INTO `personnaliser` (`rowid`, `logo`, `banniere`, `footer_desc`, `footer_entreprise`, `footer_address`, `footer_city`, `footer_code_postal`, `footer_telephone`, `footer_copyright`, `car1`, `car2`, `car3`, `car1_msg`, `car2_msg`, `car3_msg`, `car1_link`, `car2_link`, `car3_link`, `nom_article`, `nom_article_plur`, `actif`) VALUES
(1, 'public/themes/logos/oi1ctvvQkW7fhBQXCBoR4g1yBddTIPEkCqKNMmUW.png', 'public/themes/bannieres/2JT4LWWGYmMQXCfHI7tSvwoiACLzu9xsK5i2qumH.jpeg', 'Regroupement d\'achat offert par le Cégep de Trois-Rivières!', 'Cégep de Trois-Rivières', '3500, rue de Courval', 'Trois-Rivières, QC', 'G9A 5E6', '8193761721', 'Charles-William Morency - Jessy Walker-Mailly - Zachary Veillette', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, 'public/themes/logos/Hp2lenDbyOulWipZMZc04Lf8AYtjZzDxRdQSSdMx.png', 'public/themes/bannieres/g4X4QIKLy3JLNq5m1ZOb4QC1z3yoRSMF067XmDgf.jpeg', 'Ceci est la description de l\'application.', 'Le nom de l\'organisation qui utilise le site', 'L\'adresse de l\'organisation.', 'La ville, la province, le pays', 'G8Z 1E9', '8193761721', 'La ligne de bas de page.', 'public/themes/carrousel/3cHAlaR1wIa9ISzwdYWGnIMCubZanSYa3pMl82e0.jpeg', 'public/themes/carrousel/c6xxjkZZJe8jfOWPkEDDlJKyNYWYLdV2mmF2J1ib.jpeg', NULL, NULL, 'Panier', NULL, '#', 'panier', '#', NULL, NULL, 0),
(5, 'public/themes/logos/UrA6cyWvaH75N6gQD1mgO1Syohy5t3cimNTPQnML.png', 'public/themes/bannieres/0gjLjSpgNrCnzcRwxJ33rsJ9F5gCxnBkfUNBmAvs.jpeg', 'Zolies avas à vendre.', 'Les avas à vendre', '69 rue des avas', 'Ball\'s falls, Ontario', 'H3L1O5', '18193761721', '©Copyright Les avas à vendre', 'public/themes/carrousel/p1NIq7xPiwMqk1zJNwtiQLkBmAo2CT3Yrwt9fktr.jpeg', 'public/themes/carrousel/6Er7cLes5Qv95ILlFFGpWgJ2nWqnDCSZVChzEnP0.jpeg', NULL, 'Péteu de Ava à vendre', 'Bô body de Ava', NULL, 'boutique', '#', '#', 'ava', 'ava(s)', 1),
(6, 'public/themes/logos/4VOCXqVQdUjfG7dpFEK0Bj0kxcfZBXM4v4jeQ3n6.png', 'public/themes/bannieres/XWnvwH7sCeBSrOzGCgkPF3j69DKsFiq6irxH7xfc.png', '✌✌✌✌✌✌✌✌✌✌✌', '😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂', '😜😜😜😜😜😜😜😜', '😆😆😆😆😆😆😆😆😆😆', 'G6B0W1', '8788888888', '👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌👌', 'public/themes/carrousel/amqKFNM2OF1mzjmsFwMcH7LyYWreRYakRUn8gYoK.jpeg', NULL, NULL, '💯💯💯💯💯💯💯💯💯', NULL, NULL, '#', '#', '#', 'emoji', 'emoji(s)', 0);

-- --------------------------------------------------------

--
-- Table structure for table `recette`
--

DROP TABLE IF EXISTS `recette`;
CREATE TABLE IF NOT EXISTS `recette` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `article_rowid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `article_rowid` (`article_rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recette`
--

INSERT INTO `recette` (`rowid`, `article_rowid`, `name`, `link`) VALUES
(1, 19, 'test2', 'test2'),
(6, 22, 'testtrhfghfghfg', 'test'),
(17, 1, 'Amarante en taboulé', 'http://chefsimon.com/gourmets/cuisinealcaline/recettes/amarante-en-taboule'),
(18, 1, 'Marrant petit déjeuner d\'amarante!', 'http://brutalimentation.ca/2014/03/12/marrant-petit-dejeuner-damarante/'),
(21, 24, 'Equideow', 'https://www.equideow.com/');

-- --------------------------------------------------------

--
-- Table structure for table `unite_mesure`
--

DROP TABLE IF EXISTS `unite_mesure`;
CREATE TABLE IF NOT EXISTS `unite_mesure` (
  `nom_unite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_unite`),
  KEY `nom_unite` (`nom_unite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `unite_mesure`
--

INSERT INTO `unite_mesure` (`nom_unite`, `actif`) VALUES
('cuillère(s) à table', 1),
('cuillère(s) à thé', 1),
('g', 1),
('galon(s)', 1),
('Kg', 1),
('L', 1),
('lb', 1),
('mg', 1),
('ml', 1),
('oz', 1),
('pinte(s)', 1),
('tasse(s)', 1),
('Unité(s)', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_administrateurs`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_administrateurs`;
CREATE TABLE IF NOT EXISTS `v_stat_administrateurs` (
`data` bigint(21)
,`label` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_categories`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_categories`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_categories` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_fournisseurs`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_fournisseurs`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_fournisseurs` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_marques`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_marques`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_marques` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_populaires`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_populaires`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_populaires` (
`label` varchar(255)
,`data` decimal(32,0)
,`dataset_date` datetime
,`dataset_month` int(2)
,`dataset_month_full` varchar(9)
,`dataset_year` int(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_tendance_commandes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_tendance_commandes`;
CREATE TABLE IF NOT EXISTS `v_stat_tendance_commandes` (
`data` bigint(21)
,`dataset_month` int(2)
,`dataset_month_full` varchar(9)
,`dataset_year` int(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_utilisateurs`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_utilisateurs`;
CREATE TABLE IF NOT EXISTS `v_stat_utilisateurs` (
`data` bigint(21)
,`label` varchar(7)
);

-- --------------------------------------------------------

--
-- Structure for view `v_stat_administrateurs`
--
DROP TABLE IF EXISTS `v_stat_administrateurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_administrateurs`  AS  select count(0) AS `data`,'Webmestre' AS `label` from `admin` where (`admin`.`master` = 1) union select count(0) AS `data`,'Enseignant' AS `label` from `admin` where (`admin`.`super` = 1) union select count(0) AS `data`,'Étudiant' AS `label` from `admin` where (`admin`.`super` = 0) ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_categories`
--
DROP TABLE IF EXISTS `v_stat_articles_categories`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_categories`  AS  select count(0) AS `data`,`article`.`nom_categorie` AS `label` from `article` group by `article`.`nom_categorie` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_fournisseurs`
--
DROP TABLE IF EXISTS `v_stat_articles_fournisseurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_fournisseurs`  AS  select count(0) AS `data`,`article`.`nom_fournisseur` AS `label` from `article` group by `article`.`nom_fournisseur` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_marques`
--
DROP TABLE IF EXISTS `v_stat_articles_marques`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_marques`  AS  select count(0) AS `data`,`article`.`nom_marque` AS `label` from `article` group by `article`.`nom_marque` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_populaires`
--
DROP TABLE IF EXISTS `v_stat_articles_populaires`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_populaires`  AS  select `commande_article`.`nom` AS `label`,sum(`commande_article`.`quantite_commande`) AS `data`,`commande`.`date_commande` AS `dataset_date`,month(`commande`.`date_commande`) AS `dataset_month`,monthname(`commande`.`date_commande`) AS `dataset_month_full`,year(`commande`.`date_commande`) AS `dataset_year` from (`commande_article` left join `commande` on((`commande`.`rowid` = `commande_article`.`commande_rowid`))) group by `commande_article`.`nom`,cast(`commande`.`date_commande` as date) order by month(`commande`.`date_commande`) desc,sum(`commande_article`.`quantite_commande`) desc ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_tendance_commandes`
--
DROP TABLE IF EXISTS `v_stat_tendance_commandes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_tendance_commandes`  AS  select count(`commande`.`rowid`) AS `data`,month(`commande`.`date_commande`) AS `dataset_month`,monthname(`commande`.`date_commande`) AS `dataset_month_full`,year(`commande`.`date_commande`) AS `dataset_year` from `commande` group by year(`commande`.`date_commande`),month(`commande`.`date_commande`) order by year(`commande`.`date_commande`) desc,month(`commande`.`date_commande`) ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_utilisateurs`
--
DROP TABLE IF EXISTS `v_stat_utilisateurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_utilisateurs`  AS  select count(0) AS `data`,'Actif' AS `label` from `client` where (`client`.`actif` = 1) union select count(0) AS `data`,'Inactif' AS `label` from `client` where (`client`.`actif` = 0) ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_fk0` FOREIGN KEY (`nom_marque`) REFERENCES `marque` (`nom_marque`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk1` FOREIGN KEY (`nom_categorie`) REFERENCES `categorie` (`nom_categorie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk2` FOREIGN KEY (`nom_fournisseur`) REFERENCES `fournisseur` (`nom_fournisseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_unite` FOREIGN KEY (`format`) REFERENCES `unite_mesure` (`nom_unite`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_client_commande` FOREIGN KEY (`rowid_client`) REFERENCES `client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_commande` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande_article`
--
ALTER TABLE `commande_article`
  ADD CONSTRAINT `fk_commande_article` FOREIGN KEY (`commande_rowid`) REFERENCES `commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `panier_article`
--
ALTER TABLE `panier_article`
  ADD CONSTRAINT `fk_article_panier` FOREIGN KEY (`rowid_article`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_panier_client` FOREIGN KEY (`rowid_panier`) REFERENCES `panier_client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_article`
--
ALTER TABLE `periode_article`
  ADD CONSTRAINT `fk_article_periode` FOREIGN KEY (`article_rowid`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_article` FOREIGN KEY (`periode_rowid`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_plage`
--
ALTER TABLE `periode_plage`
  ADD CONSTRAINT `fk_periode` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
