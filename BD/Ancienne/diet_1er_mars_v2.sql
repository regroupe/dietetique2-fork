-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 01, 2019 at 08:04 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diet`
--
CREATE DATABASE IF NOT EXISTS `diet` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `diet`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `super` tinyint(1) NOT NULL,
  `master` tinyint(1) NOT NULL,
  `take_order` tinyint(1) NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_modif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `change_pass` tinyint(1) NOT NULL DEFAULT '0',
  `actif` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`super`, `master`, `take_order`, `username`, `password`, `last_modif`, `change_pass`, `actif`) VALUES
(0, 0, 0, 'admin', '$2y$10$qejIbdueBlAOk9Fxger.su33bTctWlR6CKcqZ1iOnz6/GCZvdFQfO', 'master', 0, 1),
(1, 1, 0, 'master', '$2y$10$al.OCHpRFjQKFrkosV4vXuYv3uQnwi.Tyl8pYoKFtdcBUlt.gPxW.', '', 0, 1),
(1, 0, 0, 'super', '$2y$10$GuKNlmDXvss6HWnDS0PCxut.TaD33w7Xzi8BO71KbCB9Q794HzI/m', 'master', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_board`
--

DROP TABLE IF EXISTS `admin_board`;
CREATE TABLE IF NOT EXISTS `admin_board` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `username_admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vue_size` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'medium',
  `vue_color` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `rowid_admin` (`username_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_board`
--

INSERT INTO `admin_board` (`rowid`, `username_admin`, `vue`, `vue_size`, `vue_color`) VALUES
(102, 'super', 'active_period', 'small', 'success'),
(103, 'super', 'active_period', 'medium', 'primary'),
(104, 'super', 'ecoule_period', 'large', 'secondary'),
(105, 'super', 'test_view', 'small', 'primary'),
(106, 'super', 'test_view', 'medium', 'info'),
(107, 'super', 'test_view', 'large', 'warning'),
(227, 'master', 'active_period', 'small', 'success'),
(228, 'master', 'ecoule_period', 'small', 'secondary'),
(229, 'master', 'stats_client', 'small', 'danger'),
(230, 'master', 'tendance_article', 'medium', 'info'),
(231, 'master', 'top_article', 'medium', 'dark'),
(232, 'master', 'last_added', 'large', 'secondary');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite_minimum` int(11) DEFAULT NULL,
  `quantite_maximum` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `frais_emballage` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL,
  `description` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarque` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valeur_nutritive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `piece_jointe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '0',
  `actif_vente` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rowid`),
  KEY `article_fk0` (`nom_marque`),
  KEY `article_fk1` (`nom_categorie`),
  KEY `article_fk2` (`nom_fournisseur`),
  KEY `article_unite` (`format`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`rowid`, `image`, `nom`, `format_quantite`, `format`, `provenance`, `allergene`, `perissable`, `bio`, `quebec`, `nom_marque`, `nom_categorie`, `nom_fournisseur`, `quantite_minimum`, `quantite_maximum`, `prix`, `frais_fixe`, `frais_variable`, `frais_emballage`, `prix_majore`, `economie`, `description`, `remarque`, `valeur_nutritive`, `piece_jointe`, `created_by`, `actif`, `actif_vente`) VALUES
(1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.95', '5.00', '0.00', '0.00', NULL, NULL, 'Excellente pseudo-céréale naturellement sans gluten!', NULL, 'public/products/nutritional/GSSLKaSc5PSg4gmaRIdSnK3FSTehvSWdeJs5MkIE.png', NULL, 'master', 1, 1),
(2, 'public/products/images/aKxliPxIIy0PREv07R4UHxQsM6vQ5wgNI8O4V26t.jpeg', 'Couscous de blé entier tamisé', '1.00', 'kg', 'Canada', NULL, 0, 1, 0, 'Gerble', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.91', '5.00', '0.00', '0.00', NULL, NULL, 'Pour faire d’excellents taboulés… Yé!', NULL, 'public/products/nutritional/MthROLiRbthvhEaQKDR4s1qPV5CDrQ7Itf2rALBG.png', NULL, 'master', 1, 1),
(3, 'public/products/images/N0AvyopsFGqYKEbFV5t1Pso4YSV20M190CNfcO40.jpeg', 'Flocons d’avoine', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Aliments céréaliers', 'Noix Quebec', 80, 100, '3.43', '5.00', '0.50', '0.00', NULL, NULL, 'Cette avoine est roulée et torréfiée à sec avant d’être chauffée à la vapeur à 200°F pour être roulée. C’est ce qui distingue ces flocons de ceux des autres rares entreprises qui en font au Québec, et c’est ce qui leur confère ce goût de gras naturel de l’avoine.C’est ce qui leur permet également de hausser sa durée de conservation à un an sans problème.', NULL, 'public/products/nutritional/8I7DFLv4nW6zmVwVkWAUkyr5gtejjhzzY6SgMYlH.png', NULL, 'master', 1, 1),
(4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 50, 50, '19.92', '6.00', '1.00', '0.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes, dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température. Pour les desserts cuits comme les gâteaux ou les brownies, nous recommandons plutôt le cacao en poudre; le goût du cacao cru serait trop prononcé pour de tels desserts.', NULL, 'public/products/nutritional/p7PKD2k1iGJ1CLL5igoP8Sv2GyxvFHIJTdZPFBKP.png', NULL, 'master', 1, 1),
(5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 40, 100, '14.88', '5.00', '0.00', '0.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao en poudre pour les desserts cuits comme les gâteaux ou les brownies, pour lesquels le goût du cacao cru serait trop prononcé. Dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température, nous recommandons plutôt le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes.', NULL, 'public/products/nutritional/SIljBYRTECmTu0cARK3YJQGdaiwOMKzmGB3HEH78.png', NULL, 'master', 1, 1),
(6, 'public/products/images/fxtpyETXUMXk2OswDJ3T1SparCXp9c4caKKNOa8J.jpeg', 'Capuchons de chocolat 70%', '500.00', 'g', 'Fèves du Pérou, chocolat produit en Europe', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 150, 200, '23.93', '7.00', '0.00', '0.00', NULL, NULL, 'Pépites de chocolat géantes, pour cuisiner ou manger telles quelles comme délice du moment. Environ 5 fois la grosseur des pépites de chocolat, elles font d’excellents desserts avec de gros morceaux de chocolat qui en mettent plein la dent!', NULL, 'public/products/nutritional/Xx8RvLJaxYXeNYale45l5G4WCbvOKrv54PdIUmiG.png', NULL, 'master', 1, 1),
(7, 'public/products/images/TmxisxJI9ocq2Ip8TQriNaer4WypYgn9Enuy9HBb.jpeg', 'Café La Montréalaise Santropol', '500.00', 'g', 'Amérique centrale et du sud', NULL, 0, 1, 1, 'Materne', 'Café', 'Grossistes en café Quebec', 65, 125, '14.37', '5.00', '2.00', '0.00', NULL, NULL, 'Café gourmet biologique, équitable, en grain et de torréfaction française. Un riche mélange corsé aux reflets de marron et d’ébène.', NULL, 'public/products/nutritional/stjSP5cjaLxUFLYOdMhmaB2M6t1cy6gsZGsdzdJh.png', NULL, 'master', 1, 1),
(8, 'public/products/images/yepAc3CrDZsNuFSxHG4h1hFbgYuqwsq7lI72nlSV.jpeg', 'Levure alimentaire en flocons', '500.00', 'g', 'États-Unis', NULL, 0, 0, 0, 'Ayam', 'Condiments et autres', 'Farine Quebec', 80, 120, '13.89', '5.00', '1.00', '0.00', NULL, NULL, 'Levure produite par fermentation naturelle, aucune source animale ni synthétique.', NULL, 'public/products/nutritional/oMVZlewVITN6Jx7GfMeiFzOUOr9hQe4YXg5vt0o4.png', NULL, 'master', 1, 1),
(9, 'public/products/images/7AzHZCs5UGaVzjEJETAw9z1XquORt6uMNrRyF99U.jpeg', 'Miso soya et riz sans gluten', '500.00', 'g', 'Québec', NULL, 1, 1, 1, 'Bledina', 'Condiments et autres', 'Confitures et gelées Quebec', 100, 100, '13.83', '5.00', '0.00', '0.00', NULL, NULL, 'Le miso biologique Massawipi non pasteurisé de soya et de riz est un aliment sans gluten très polyvalent; son goût est doux, rond en bouche et légèrement salé. Il est le résultat d’une fermentation naturelle (non forcée) d’au moins 2 ans. Pour sa fabrication, nous n’utilisons que des ingrédients certifiés biologiques garantis sans OGM.', NULL, 'public/products/nutritional/Ty12FzodDT1kbWEeKsEnqlXZwim1KggiKytU5P9l.png', NULL, 'master', 1, 1),
(10, 'public/products/images/I6XjF0Hwgg9sIX0tqhw7rrjiacSVk3oHcqy0DeXV.jpeg', 'Moutarde de Dijon', '1.00', 'kg', 'Québec', NULL, 1, 1, 1, 'Herta', 'Condiments et autres', 'Produits alimentaires Quebec', 20, 20, '8.98', '5.00', '0.00', '0.00', NULL, NULL, 'Moutarde crue, sans sucre ajouté, sans agents de conservation, sans gluten. Une moutarde de Dijon avec une belle texture crémeuse et une touche de piquant qui la rend irremplaçable! Pour les amateurs de moutarde forte!', NULL, NULL, NULL, 'master', 1, 1),
(11, 'public/products/images/uwhju93QU1tzQNJO5xXD9iu1JDSzw7w00tRcRFEe.jpeg', 'Farine blanche tout-usage non blanchie', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Sacla', 'Farines', 'Farine Quebec', 60, 70, '2.88', '5.00', '0.00', '0.00', NULL, NULL, 'La farine blanche tout usage non blanchie est tamisée pour en retirer tout le son et ne subit aucun traitement de blanchiment. Elle est produite à partir de blé dur de printemps biologique, et les grains sont moulus sur cylindres. Idéal pour la boulangerie et la pâtisserie.', NULL, 'public/products/nutritional/6cCibCA8qr7SRrQYZCsZimR5Ly3GKJUWB1d9bm9o.png', NULL, 'master', 1, 1),
(12, 'public/products/images/n5nqMXBrO1Lx3XhZ1l8ZCpQA5D7g08p9WHQq9tuB.jpeg', 'Farine d’épeautre', '200.00', 'g', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Farines', 'Farine Quebec', 25, 30, '4.60', '5.00', '0.00', '0.00', NULL, NULL, 'Depuis mai 2018, cette farine est tamisée de sorte à en extraire 10 % du son, ce qui lui procure un meilleur rendement en pâtisserie et en boulangerie qu’une farine intégrale.', NULL, NULL, NULL, 'master', 1, 1),
(13, 'public/products/images/1TpWugpuoiLsRiiFu3ToTpzfZcBhz8dRDzXDL5UL.jpeg', 'Farine de blé intégrale', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Fleury-michon', 'Farines', 'Farine Quebec', 80, 80, '2.88', '3.50', '0.00', '0.00', NULL, NULL, 'Cette farine de blé intégrale contient toutes la parties du blé. Le son et les rémoulures donnent plus de texture à vos pâtisseries et à vos pains.', NULL, 'public/products/nutritional/PSd1hLFB885sjyTw5jfEZAOjZTPm1zGah4LvzJ8x.jpeg', NULL, 'master', 1, 1),
(14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 20, 40, '13.94', '5.00', '1.00', '0.00', NULL, NULL, NULL, 'Il est possible de retrouver des noyaux dans le produit.', 'public/products/nutritional/0K5UCVCywUYqmbWHv79g8rJiwUQzyEKXh5BX6e09.png', NULL, 'master', 1, 1),
(15, 'public/products/images/17YmeaiQPt8j48kzvCWmxRqXAzBKtDD3r7r0TetF.jpeg', 'Canneberges séchées au jus de pommes', '1.00', 'kg', 'Québec', NULL, 1, 1, 0, 'Citadelle', 'Fruits séchés', 'Produits biologiques Quebec', 175, 200, '18.66', '5.00', '0.00', '0.00', NULL, NULL, 'Ces canneberges séchées sont préparées à partir de canneberges (Vaccinium macrocarpon) matures de première qualité et infusées dans une solution de jus de pomme concentré. Elles sont tendres et juteuses et présentent le goût acidulé et légèrement sucré caractéristique de la canneberge. En faisant sécher les fruits plus longtemps et à plus basse température, Citadelle parvient à préserver la saveur des canneberges ainsi qu’une quantité maximale de nutriments, pour le bonheur de nos palais et la santé de notre corps.', 'Le tout est 100% naturel, tant le procédé de conservation que les canneberges elles-mêmes auxquelles aucun agent stabilisant, ni colorant, ni glycérine n’est ajouté.', NULL, NULL, 'master', 1, 1),
(16, 'public/products/images/kPZuCChkJhrxRQ0YOhKAqSt6MaWVfGeLjc5eqwFD.jpeg', 'Dattes Deglet dénoyautées', '1.00', 'kg', 'Tunisie', NULL, 1, 1, 0, 'Prana', 'Fruits séchés', 'Produits biologiques Quebec', 80, 80, '12.77', '5.00', '0.00', '0.00', NULL, NULL, 'Youpi! Savoureuse et sans noyau, bien que, étant donné que le processus de dénoyautage est automatisé, il est possible de retrouver quelques noyaux parmi ces dattes.', NULL, 'public/products/nutritional/qQDAsd2DoU6n1C0ABNUWrrR14Qz0yj6xvi2IWj9W.png', NULL, 'master', 1, 1),
(22, NULL, 'test', '1.00', 'unité(s)', 'test', NULL, 0, 0, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 1, 1, '1.00', '1.00', '1.00', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, 'master', 0, 0),
(23, NULL, 'aaaa', '1.00', 'unité(s)', '1', NULL, 0, 0, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 1, 1, '1.00', '1.00', '1.00', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, 'master', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_categorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`nom_categorie`, `actif`) VALUES
('Aliments céréaliers', 1),
('Cacao et ses acolytes', 1),
('Café', 1),
('Condiments et autres', 1),
('Farines', 1),
('Fruits séchés', 1),
('Graines à germer', 1),
('Huiles et vinaigres', 1),
('Légumineuses', 1),
('Livres', 1),
('Noix et graines', 1),
('Pâtes', 1),
('Produits québécois', 1),
('Sacs écolo', 1),
('Superaliments', 1);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`rowid`, `email`, `password`, `telephone`, `poste`, `nom`, `prenom`, `adresse`, `no_app`, `code_postal`, `ville`, `pays`, `province`, `actif`) VALUES
(1, 'jessy@cegep.com', '$2y$10$BkAGbBom8y9bQz8LeD9L3eRIAKDhQsOsifdr114hbWIJyLEVVF8VC', 'eyJpdiI6IktheU5FbFRRcTVaQ2VqTExmWU9tM3c9PSIsInZhbHVlIjoiekxoTnBXTzE3QUxKenhXNEl5a0o4dUhlTUhcL3dYcXJ0cThLMktcL3F1TFBZPSIsIm1hYyI6IjA3YzEyMDVkMDViNjA3MTQ0MjJmYTBjYTkxYTBkM2NhMGU1NTYzZDY4ZTc5ZDM1MjQwYzM4NzRmNTJmZTM2NGUifQ==', 'eyJpdiI6Ik9KcWszNUFOVGJ4RDBqcWMxM1Z1Ymc9PSIsInZhbHVlIjoiY2paXC9tNTZjcnNyeWZcLzh2YVROTlwvZz09IiwibWFjIjoiZDE2YWJlODg1MjkyOTAxNTlkOGIwMWZlZmUwOTVhMzJmZjU0MzA1MGRmZWVjMDA4ZDEzNGY2MDc2YmIxZDk5NCJ9', 'eyJpdiI6ImE3dTJwTDVQdTkxbHJVb09xXC9yeVwvQT09IiwidmFsdWUiOiJjUHlBejRla2dTUUtZc2xFSm51VXJBPT0iLCJtYWMiOiI0NzM0ZDQwNTY4MzdmNTIxOTgyYzNhNzk4NzcxMWZjNzJiMDdlMmJiNDc2NjRiNTUzMzYwM2M0ODZhNzE3MGM2In0=', 'eyJpdiI6InJqMnZuTXdjRlppWHJ4MzVhZWkzckE9PSIsInZhbHVlIjoiNUFFMGlUZ1ZlUGtIM2ZYRXI5SFJ2UT09IiwibWFjIjoiZWExNTdkNWFlNmRkNjNmYjQ0ODU2OGMzMDljOTg5OGQ1OThjMWMzMDM4YTI5MjQ3YzY1MmI0MGIwZTg0YmJiMiJ9', 'eyJpdiI6IkNcL1p5QVFzNUtOOHJoZXFYTkdoRVhBPT0iLCJ2YWx1ZSI6IisxQTVxTlNybm1EYis5b1NcL0pNNlhNczdla2czckNGcjJVVkR0OEY1OFBGWUZMOTczU1hpVjlkZUdjNzg1UXNIIiwibWFjIjoiMDljMGJhOGU1YzlhNjhiNWM0ZGYwZjc4YTI2OTZhOThjMWZlNTBhYmViMzM1MzgyNzgyNDkzZDU2ZDFjZDc1NSJ9', 'eyJpdiI6IkhnYzZtTFB3YkhFUFpRVTBNNVRnS2c9PSIsInZhbHVlIjoidE9lZlpsWlFxR24zczlUYWdWMUozZz09IiwibWFjIjoiOTIwNzg1MzM3N2Q4NWZlNzAwZGFlMjgzZWMyMDM3MzZiNmYzNTM4MWU4NzYyZTg5YTU3YmQ3OGY5Y2I4ZDk1OCJ9', 'eyJpdiI6IjBxd3E3OHljREY4cFUyWDBVWXBBc0E9PSIsInZhbHVlIjoiallXUWlkRGc4SVJiaCtrNEdwbUV6QT09IiwibWFjIjoiOTUyZGU0NmViN2FmMTdmMGI5ZTk0MDUxMTdjYWNmOTY3NTVmZjg2Y2UyMTkzZWJhMmJiM2MzOGVhY2ZkNmM0OCJ9', 'eyJpdiI6IlVrWDdCQ0lMSWdSejRcL3JkSTV4a2hRPT0iLCJ2YWx1ZSI6Ilc5cTYwU0lhelNOTnlUckRFSVN2UDQrOHZjZmdhNDJIR3lDZGY4Z3hnRFU9IiwibWFjIjoiMGNmNzkxYjMxZWRlZDBlNmE4NTM0NWUwYzYxYjQ0NjI4OTVjN2ExYmE4MWQ3NjU2NWU0NWQ5ZTc2MGUyZGUwNyJ9', 'eyJpdiI6ImFkYUh4dElaRnI0T011ZkdnMnZMM2c9PSIsInZhbHVlIjoiQ3Rwb3ZzK0VLT091a1M5clwvSWNXeGc9PSIsIm1hYyI6Ijg4MDFmZjgzZDAxM2Q2YzAzMDg0ZTlkMDM5ZjUyZjQ1ZWYxNzQ2MGJhNGIzYTNjZjQzYTc4ZWRkNDI0M2Q0MjgifQ==', 'eyJpdiI6InhteGR4QXdON3g5WEZcLzVaVU10eEdBPT0iLCJ2YWx1ZSI6Im5UQkw1alwvWElFbHpzV0kwN0pvRWx3PT0iLCJtYWMiOiJiODExZTdiYjg1ZWFkNDE0YmNlYTkwYmQwMjkzMDA2ZDdjZmYxMmFiZDIxZTFkYWU3ZjMzNTQ2MjcyY2QzOTlmIn0=', 1),
(2, 'zach@cegep.com', '$2y$10$C3Kao9./HOM/HDABXssW0OS9d0cPDXOGdTb6M5TIl9czDfULJsmNW', 'eyJpdiI6IkRBc1wvZGJuTm5nMDU2ekE5WDV4SHZRPT0iLCJ2YWx1ZSI6Im5vUm11bUpDRlJGVU5sdVhLMGJXVm9OSmU1Z1MyWjEySzFaV1pEWTI0Tzg9IiwibWFjIjoiZmI2NDg2NWRhY2RkMmI5MmY2ZmY5ZWE3ZWQwMWZkOTk1MDQ1NjE4MTllN2RkY2ExNGIyMDZiY2UxNGRlNjZiMyJ9', 'eyJpdiI6IjJPOW1IbGxvNCt0SlNtRThzQmNOanc9PSIsInZhbHVlIjoiZ2dFMGpxTVo2Z2p1Q0p4R1JPUDc0UT09IiwibWFjIjoiZDNiZDQ4MWZmNTNlMzA3ZTRiYTMyMTA5NGU1ZjU2NjA0ZjZlNTRhNmVlZDdhMDUzZjRlMzIzMTI0ODUxN2ExMyJ9', 'eyJpdiI6IkpTaUk3NDBiWVNcL0dpRUlweTBWeTNnPT0iLCJ2YWx1ZSI6InBNVzFxOGxGb0F5S2JzWFF2bEZXRmc9PSIsIm1hYyI6IjA4YjU4NDhjODRhNTY4M2Q1Y2UzYjM4OGQ3YmU1MzY3NzdhNDJiNzkyNmQxNmJkYmEzYzhhZGFjNDlkOGUwZjMifQ==', 'eyJpdiI6ImI5eGl2REUwREY2ZzVkZUJnbUhnQ2c9PSIsInZhbHVlIjoieVdRZk80M1RWTWUxM3FuXC9qNlpaXC9BPT0iLCJtYWMiOiIwZDcyN2NiZjU1MWZjYzc0ZGE1MWNkOWNlZDQ0Mzk0YzJlYzk3NDcwZTVhZmYwMzFkYmViYzlkZDQ2YjZhOGNhIn0=', 'eyJpdiI6IlgyRnpJRStHaWIxTDk0NGlhYVwvSlBBPT0iLCJ2YWx1ZSI6ImpuYVd5Y1hjeDE2N2ZPRTVhUDFqNXc9PSIsIm1hYyI6IjM5NjdlYTAzNmI3MDA2NTI4MjU4YjVjNWJlNmZmNTlmY2FmNTEyOTYyYzM0YjhlMTczNTgzMzhjYWI4OGM3N2UifQ==', 'eyJpdiI6InptR0lCK01NNnBLZUdLXC92TE15YlJRPT0iLCJ2YWx1ZSI6InZWVVdpazlzajBzZTN4S3ZqOE1VNVE9PSIsIm1hYyI6IjdhMjY1ZWIxY2Y4MzFjZDc0ZmJhZWE2NzgyMWQ5M2JmZTBlNjgyOTFjODI1ODE0NWJjODEwZWFjNTk4Njc1ODQifQ==', 'eyJpdiI6Ik5XczhPZkg2S0RvRWk2YW5pVW5LK1E9PSIsInZhbHVlIjoiNjZieHBzU3Jtbk54RjNLXC9QMjVRdWc9PSIsIm1hYyI6Ijg4ZTExMTUxZWI2MGVmNzFhOGU4ZWIyMjQ1YjYyNDU3MzI4NGQ2MGJkODNmYTRjNDA3ZmUxNmI0NTVkYjQ2MmYifQ==', 'eyJpdiI6ImpOKzBxTlNiK3dHVGtJZG92ckdrY1E9PSIsInZhbHVlIjoiaDBFZVZobUo4M00wUjdrUDZRYUR1Zz09IiwibWFjIjoiNGZiNjEzMzlhNGMzNWMxMzQ0YzRjOTQ3ODU2NzJmMTU1ZDIxOGZhZjFlYWYzZmZhZGIxNWNkYmJiMzg2ODU3OCJ9', 'eyJpdiI6IlRCK0NcLzN3OWtjbGY2aXpRVkNRaDN3PT0iLCJ2YWx1ZSI6ImdNbmhwNzZBTTFsXC8zbUY2R0ltNlhBPT0iLCJtYWMiOiI1NDQ5NGRlOGY2MzIyZjk1NGRlMjMzYmFlMTIwMGIwYTFlNmUwMTljNjNlMzhiODlhYmQ2MGEzZjZmZmQzNmY2In0=', 'eyJpdiI6IlNXaGZoUHoyakc5NHdPMlNjZ2ZtUUE9PSIsInZhbHVlIjoiUlNia0E4YVR2cm42T0NXeWRXbEZQZz09IiwibWFjIjoiNjI5YTgwNDRmZTBlMTM3YzEwYjEzYzUwNTljMjUxYWM1YTNjOTRjZjQzZjkxNTQ1Mzk0ZWNjN2NjNjllMTNkZiJ9', 1),
(3, 'charles@cegep.com', '$2y$10$wQ5TKSBn3Ev.7abcv/jld..Akftaar1kXWzLRK2qbDvOOrBAJ.9U6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(4, 'jean@jean.com', '$2y$10$aXV75HlGGp8iu5qReeWQpuiYs9NJyzeEtRHl.hV3fmzHJM.Bi1xvi', 'eyJpdiI6ImJjODZJYXZmV2lhREloVUdEclVJQVE9PSIsInZhbHVlIjoiZjZSNU5WeHhRRjNtT1RsY0NvTEVYc1lJc3hmOVJDelJkaWZCN3Ntaklidz0iLCJtYWMiOiI2NzRiZGVkM2NjNTJjNjk1NmRhMmRjYzE1ZTYzYTQ3ZDViOWI2YzRiYmE2ZjgzNDZjOTZkYjYyMDRlNTkyYjA4In0=', 'eyJpdiI6Inh2YmwxXC9DcE5hRW1CY05uT0RLQkhRPT0iLCJ2YWx1ZSI6ImdhUFNpZHROaEphZk9Lelh3WmV1WWc9PSIsIm1hYyI6ImIwZTA4Yzk3NWZjY2M4NzVmNzMxMDNlYjU1Yzg2MjYxNDc3YzU2MWRlYzQzODgwN2Y4MTFhODY1NmZiYzQwZjQifQ==', 'eyJpdiI6IlpjanRzY1Q2enp6NVdpbUVOaG9NUlE9PSIsInZhbHVlIjoid3EyUGZVUjdONXRQRlgraTVLdzVsZz09IiwibWFjIjoiNzE1MGQ5MjE1ZjVkODI5NTdhOTgxMzFiYzc3MDNiMjRkNTQyYzZlOGU3ZDkxNmE3ZjIwYjY1YjhlMjA4ODNiNSJ9', 'eyJpdiI6Im1Sb0tQcXFDWGdUeUR2K2J4RmJHOWc9PSIsInZhbHVlIjoiNW1JT1pTajRaRWpLbXNvVTZLcjBWUT09IiwibWFjIjoiYmUwY2RjOTI2MDYzODM4Nzg4MGFlZjU2MDExYjNiY2NiMWU0ZDliYWRhYWEyMDhhNGFiMGIxOTJiYWE0YTc2NiJ9', 'eyJpdiI6InRYMkNRZlhNWno4OFQ0ZG1ERTM2Y0E9PSIsInZhbHVlIjoiUkdZZlNvQ3BCV3BVWTJRK1NtTFFKZGRaMTJ1cmhNVXRBVEJqSjZ5UEJpVENIYlU5VXc3SmUxSWhxd2dTMVorUiIsIm1hYyI6IjYwZWNhMDUyZjVhMTAwNjRkY2U1NTNiZTdiOGJjMmE5ODA0MDlhNWE0N2M4MWM1ODQwYjQ4OTcwYTM5NzVmNjMifQ==', 'eyJpdiI6InZHcnBmVFUyVVE0d2U4XC9LRDRaMUJ3PT0iLCJ2YWx1ZSI6Ik9qVmhlZlNBUm9RS0pJWGg5U0NpSmc9PSIsIm1hYyI6ImMwZDc4NmYzNjkxN2RhZDk1ZGUzYjY1ZGUyMWFjZWZmZjg5OWVjZTI2ZWQ0NDBiODc0MmQ4M2YxYmY1Nzk1ODQifQ==', 'eyJpdiI6ImdnUTYydksrVE1RSFVDSndRNmxLRVE9PSIsInZhbHVlIjoiY1VKMVF2R29uVVwvSG5lbTlreUpIS1E9PSIsIm1hYyI6IjkwODQ1YTMzYmMyMTIwNzBkMjZkZTJmMzMyMGZkYjMzZGMwMjBkYzIwMDM2Mzc5ZDg3YWExNjYxOGQ3YTFkZWIifQ==', 'eyJpdiI6IkpGOFJ2SUo0dGhCcHlENU9XXC93TVVBPT0iLCJ2YWx1ZSI6IlY3RzJEcU5cLzNQZmtFdml2NTliR1FGcDFGdXpKVTdVZFRJT2tHb2wzK1ZzPSIsIm1hYyI6IjQ5NzgxN2M1NDE3OWUxYzRmM2JkYTE5M2YxZjQyMzU3MTJkNmU3MmJkMTE5M2NhYzJhZDBmMDBlZmY2ZjY2MzcifQ==', 'eyJpdiI6IlUrM01ZN2FNWlFhdE9BclREK3p4aFE9PSIsInZhbHVlIjoiZERYY3NcL2x0MEZrUjZ5ZDhJY2pONVE9PSIsIm1hYyI6IjZiZjVhNjI5OTY4ZDU5Y2EzNjI1NTk1NTBiYzYwNTIxMmNhNGQ3YzYxNmZjMDc0Njk4ZmJiZWM0NGQwM2MwNGIifQ==', 'eyJpdiI6ImRIRkh2WkxXaXhVc2VSZFp1aXJtNmc9PSIsInZhbHVlIjoiXC8xQ0VPUFdYZUNZS0VqWU5IeTNcL2FRPT0iLCJtYWMiOiI1YjU2OTc5NDhmNjM5NmVlYmM2NWU0Nzg0N2ZkMGQ4ZGE2Mjk2MGFhMTY3Njc4ZGIyZmY4OTY1ZjcxNjQ2NzQxIn0=', 1),
(5, 'test@cegep.com', '$2y$10$ENoT8B4hOhPQjcn4meIMyePAzcjNG4x9DiZOcw87fdWGdK1rLRgjO', 'eyJpdiI6IlpkSFZsU3ppR3ArSldPS0tYYlFZQkE9PSIsInZhbHVlIjoiSW5WNjJSWkVMdWl2XC9Cc20xaGFNclNUTTFQMUJXOVFcL0Q5VlU3dmM5bTFrPSIsIm1hYyI6ImJjMGZjYzQ5NGYxOTA4ZWIzZWU0MTk0MWQ1ZDc4YWJkMGNmOWU4ODRhODc2M2I1YjgwNWYyZTg1NDVjYmVjNGQifQ==', 'eyJpdiI6IlN4eG9PazdVQjZXSVNHK2pvTkUxNWc9PSIsInZhbHVlIjoiMnFTem5td1wvZkZTQTlOWHE3ejRYY3c9PSIsIm1hYyI6IjlmOWVjMjE1ODI1NGI3NDY1MThkMGIxMmE1MTk5ZmEzZjY0NTdlMmM2YTdjOGQ0YWVkNGM3YzZiMmE0N2RmY2UifQ==', 'eyJpdiI6IkJRTzY2dHJ3QjV0ZTcrdHJaTWJSZHc9PSIsInZhbHVlIjoiY2pCSFB3VkN5M3B2ckppSDdcL1R3XC9RPT0iLCJtYWMiOiJiMjRiNmViZGNkY2Y2ZDg5NDA4OTA5MzEzMTllNzcxYTUyZGRkYmM1NTdmZTBlMjEwNjkzZmYxN2NiMGM2YmNmIn0=', 'eyJpdiI6IkNueTZpNFp5XC9WNTdnNnlBOUxiMlhRPT0iLCJ2YWx1ZSI6InJ3MTNHS2U0azJzSE5PTkVkS1wvMmpnPT0iLCJtYWMiOiJmZDIzNDE1NjBlYTA2YmI5ZmZhNmJiMTE2ZmM0ZTA5MmZiODEyN2I2MGYyZjQ1OWNhOWFjNjA1ZDViZmJkYzlmIn0=', 'eyJpdiI6IkVLd1wvVVwvSEpPdDVyZ0J2dTFVQTk5dz09IiwidmFsdWUiOiJuU24yS0xHbVJnVlA5cU05VWZOa1RBZTBhdXU5WElEYlZCR0RlUk5JY3BSbU51K0tMNk45c2o4K1hYZVlRYU92IiwibWFjIjoiMTE0NzllNGQzNWE1ODhlOTYyMzZlYjFlN2Y2OTE3NGNjZjE3NWJjYzUzNmQxNGYxMjVhOGRlNDYyYTAwNGJhNiJ9', 'eyJpdiI6InN4OUpUOGtmVU9GXC9jMnFNT3VnVXVBPT0iLCJ2YWx1ZSI6ImtkcW1UZzZ4dXY2YUJSKzRsNlQ3SEE9PSIsIm1hYyI6IjIxMGIyNWIwZjMzMjFlMmNkMjZlMmU1ZWYwYjNlY2ViNDlkNzg0YTRhMmZkOTBlNTM2NGVmNGYzYzBlY2QyMzUifQ==', 'eyJpdiI6IkZRUGExNFRNTEF0UUpMVnZvOVdQWmc9PSIsInZhbHVlIjoiaXJLeHE3MkxaTmRmQ0pXMml4YTlBUT09IiwibWFjIjoiMjAwMTE1MTc1NWUwYzViNDE0OThiMDgyODljMjliYWFhOWUyMzI4NmI4MDYzNmRjOGEzMWMwZWU2OGIwMjAzYyJ9', 'eyJpdiI6Iko2RDBsT2lsNjVoNVZWdFJ6QTRiOHc9PSIsInZhbHVlIjoidWFkU29RSVJUbHBvYWozUjdmQVFXS0tqRnBcL3preG1SN1VBamNFdjc2TDQ9IiwibWFjIjoiMWNhMzdjYmYxYjA4YzYwMWJjYTI4YzEyYWNhZDM3YTEzYWMyOGUxNTNlMmQxZmU1MmVlYzMyMzY0ZWMxOWYzMiJ9', 'eyJpdiI6ImZybnM4anM0T3JDUW9QdVZXUW9xcWc9PSIsInZhbHVlIjoiczkxVlBBcEpcL2ZxWlwvdGE2U3N4dHp3PT0iLCJtYWMiOiJjMDk4NzQyYTcxYjk2ZWJjZDMyMTUzMDNhZGU1YTE2YzJkMmVhM2YzZTE0NDAwNzY5Y2M3NmRmYjU2NzVmMzUxIn0=', 'eyJpdiI6IlFPQWF3U2xHb3BuSWZ6N0ZFajdBTVE9PSIsInZhbHVlIjoiaDJCc3lzNHNyNjdBTTlNYjNZMW4ydz09IiwibWFjIjoiMzcwNWJiMDZjMzdiOGY1NWNiYWFmNjI3MTk2NTk5OWQ5MzYxZDQyYTc5MmMzZmRlMjM5OGZlYjg3MmZjYWM2YyJ9', 1),
(6, 'yo@yo.com', '$2y$10$V0roLMkUG5KnwKt3juh2x.erTWw7qCei7JLieYwxd8qQ8aRuNxum.', 'eyJpdiI6InBZTjFVXC9FS2NySUFUQnhVRlVyOVN3PT0iLCJ2YWx1ZSI6Ild0bmJmYU0xZk1xcXVyVXRQbE83T1wvUlRvZXBYQjNyY0NOVWUzMzV5RWs4PSIsIm1hYyI6IjY0MmQ5NzIzYTRmNmNkMjYxMTQ5ODA4MDNmZjg2NWRiZWUxNWY3NmRlODcyYTdiMTliNDI4YjBhMTIyNDk1ZmMifQ==', 'eyJpdiI6IlVYdWhWeWx2ME5wSFNWV1p0UDE4Vnc9PSIsInZhbHVlIjoielZTRXl3RTdIUXFFMkxJMUhPQ3BaUT09IiwibWFjIjoiMmMyNGU5Y2NlODZmZmU4NDcxYmZiODc4ZDgxNmU3ZjZlOWJjZWYxMzgwOTc2YmE1NDc4NDk1ZTk2YTE5OGM1ZSJ9', 'eyJpdiI6IkdNUjljR0hYeEVEaXFrTGFTRkNrN2c9PSIsInZhbHVlIjoiaHZRanB4VEVQN245ZGI2VGdCWUd4Zz09IiwibWFjIjoiNjFhMjRiZTdlZjJlODllOWVhMzA5ZTkxNDAxNTgzOTJhZjVkMmVjOGE0NTE5NTk1MDg3M2UyNzM0YWM0ZjllOSJ9', 'eyJpdiI6Im92b0NtSkYyVU14cjVlckZKYnlJakE9PSIsInZhbHVlIjoiYm0wdGE3aytcL3VcL0Rqa2xEVE9rY1N3PT0iLCJtYWMiOiJjY2E3MjFiMjM5NTk0MjE1YjcyYzc1OGMxZGUxYmNiNDlhZWM1ZTk2ZjhhNGJjYmFmNjUxNzFlNjM3N2I4MjcyIn0=', 'eyJpdiI6ImRGUEhoKzlMRlwva08xcjlDdUU0aVVBPT0iLCJ2YWx1ZSI6IjFpTjQ3Vzd4SE9LZ3VsMHNRa3Q4NUE9PSIsIm1hYyI6IjQyMjljNzJjZGYxZTM5ZTBlMTdlYmY0ZjVjNzY5YzgxMDQzNmM4MGI1YjZkOGViNzNmNGNjNWRhMjhjODdiNTAifQ==', 'eyJpdiI6Im1NNzZGTjMwa1JrclIyc3duSHd6RFE9PSIsInZhbHVlIjoiSFY0MjhMb0VvaDVjQVhvc2VsZXczZz09IiwibWFjIjoiZTFhM2E0ZjliNDZlZjcwOWJkMjZhM2VmYTNiNTVlMDAyMjg0NDAxYWVmMjZmMTg4ZmUwZDQ1MmYyYTM4NzM5YyJ9', 'eyJpdiI6IkRKYnRnbzE5OVZUSjFaN1pOVnlLM0E9PSIsInZhbHVlIjoiQWdaVTBMVndJVmxtdVgyY1hCcG9Wdz09IiwibWFjIjoiYzc1M2I5NDllNmEyMzA4YTllZjcxOGE2ZGVkMDZiZWViNTYzYTk3ZWZmZDk5ZWRhNzkxNmQ3YThlNDhmODQxZCJ9', 'eyJpdiI6IjFpczdmQUZ5ZDdqekJQbTdxS0xMNHc9PSIsInZhbHVlIjoiQmdqM0U1YlBTWFYrdkZETUhZN3ladz09IiwibWFjIjoiOWQ0NTQ1ZGVjMTgxZjdlNTFlZmEzMDBhMzJhNzRiNzk4M2JlYWM5NWFhZDMyZTQxMmI0NTI0M2Y0MjRiZjVkMyJ9', 'eyJpdiI6InJvN3A2VjBpMzF6aGh1UUJOdjZIN1E9PSIsInZhbHVlIjoieWE0REdwMXM1Y2pmQ01wbGt5STR3UT09IiwibWFjIjoiMGNlOWY0ZWFiYzM4MmE1ZmUxNDRmM2JlMjc2YzJlZGM1ZmRkNzQxMTliMDJkYzlhMWFjNjQ0ZTExNWZiNzhlYSJ9', 'eyJpdiI6ImZRODZOQ2dQM2w2VmtPazN3WnQ0MkE9PSIsInZhbHVlIjoibW91OHJ6UG9QREt1VG5WcWdWRCtsZz09IiwibWFjIjoiNmExMTZkZTkwMzdmZjBhODQyYjU1OTY1OWYzMTAzNTE1NDQ3ODY0MDQ5YjExYzk2ZTE3ZTY2NDBhYzlmMDI1MiJ9', 1);

-- --------------------------------------------------------

--
-- Table structure for table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_client` int(11) NOT NULL,
  `rowid_periode` int(11) NOT NULL,
  `nom_client` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sous_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `date_commande` datetime NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify` tinyint(1) NOT NULL,
  `facture` blob,
  `identifiant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paye` tinyint(1) NOT NULL,
  `prise` tinyint(1) NOT NULL DEFAULT '0',
  `signature` blob,
  PRIMARY KEY (`rowid`),
  KEY `fk_periode_commande` (`rowid_periode`),
  KEY `fk_client_commande` (`rowid_client`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commande`
--

INSERT INTO `commande` (`rowid`, `rowid_client`, `rowid_periode`, `nom_client`, `sous_total`, `total`, `date_commande`, `email`, `adresse`, `no_app`, `ville`, `province`, `pays`, `code_postal`, `telephone`, `poste`, `notify`, `facture`, `identifiant`, `paye`, `prise`, `signature`) VALUES
(1, 2, 1, '', '32.83', '36.45', '2019-01-30 13:08:03', 'zach@cegep.com', 'ghfhf', NULL, 'fghfgh', NULL, NULL, 'G6B0W1', '8888888888', NULL, 1, NULL, '', 0, 0, NULL),
(2, 2, 1, '', '79.68', '88.44', '2019-01-30 13:09:27', 'zach@cegep.com', 'testing', '12', 'Hello', NULL, NULL, 'G6B0W1', '8888889999', NULL, 1, NULL, '', 0, 0, NULL),
(3, 2, 1, '', '44.64', '46.86', '2019-01-30 15:49:24', 'zach@cegep.com', 'testing', '12', 'Hello', NULL, NULL, 'G6B0W1', '8888889999', NULL, 1, NULL, '', 0, 0, NULL),
(4, 2, 1, '', '80.82', '84.78', '2019-01-30 15:51:34', 'zach@cegep.com', 'testing', '12', 'Hello', NULL, NULL, 'G6B0W1', '8888889999', NULL, 1, NULL, '', 0, 0, NULL),
(17, 1, 3, 'Jessy Walker', '4.95', '5.19', '2019-02-13 17:54:00', 'eyJpdiI6Im0zN1IxajRzV3RNaWVCWlNUSlk0UEE9PSIsInZhbHVlIjoibXRXYmhjXC9Pd1Q2bUVPS2I1ZWlLN1hnTGdiMjc1amx3dUg1aGFKK1QxRWc9IiwibWFjIjoiZDczZDhiODY2NmZiMzljNmZlZDliMjQ3ZjYxNDMxNjJiZjY3NjU5ZGFkNjM3NTVhNDU3NDZkZGE4ZGJjM2E5NCJ9', 'eyJpdiI6Ikc5QUJGdzJKVWVEZmxrOWVqREFTNlE9PSIsInZhbHVlIjoiYW1kejVhbEorUlJLYW55d2h1c3VBbjE5bmlcLzlwNjFsNXd2UFZkUCtLS3N3ZlROSEV2dEF5K2hWS21TR0pYZUwiLCJtYWMiOiI1MmM2M2I1MzRjMWUxNTc1OTc2ODE4YjNkYjY4OWZhMDA2OWQxN2Y3MzgzMjMxY2IxMzNhNDZjNTEzNWVlMTg2In0=', 'eyJpdiI6Ikg3NFNEYVZ1WWpYOE1nTXlmOE1GTFE9PSIsInZhbHVlIjoiYzg0OGdUMUtDQVloS2Jid2JvdDR3QT09IiwibWFjIjoiMjgzMGY3NDBjN2FjNmUwZTZlYmEzZGExYTYyZTBlZDQxNmVjM2Q1M2M2ZjNjZDQ2YWM5ODBmMzBlYjYzZGFmNyJ9', 'eyJpdiI6IlJDVTlJVFllSEt5TkNjemdIUGM3QUE9PSIsInZhbHVlIjoiVXVcL3lYOEtcL0l4akdDYlwvZWhyclpLOVdBQzg0a2owaWVlYUh4WnYyM1Qrdz0iLCJtYWMiOiIxYmJmM2ZkZWYxOTg5ZjAzOTA2NmE1NzMzOTkyMWJhYjI4NzEzMmRmYjkwN2FiMGNkNTA0YmM4YmRlNTkwYjkwIn0=', 'eyJpdiI6ImZrVnBmMHhcLzh3QzBQUWt5TFhyWEt3PT0iLCJ2YWx1ZSI6IkVvR2JONTg5TGJJZWg2bFdnSFhBUUE9PSIsIm1hYyI6IjYyMzE3NzRhNzZiMDIwMzk0ZmUzYjIzMjAyZTIyYTY2NjNhNTYzZGFkODZiYmQyZDU0Zjc5OWUyZGFlZWQ0ODgifQ==', 'eyJpdiI6ImRRM1N4enZEekxDSE1hRU9QSEJGT2c9PSIsInZhbHVlIjoiVEE3cTJONzRSWk0xSTQ2dUpSalJCdz09IiwibWFjIjoiNjYyZDk3MjkwZTQ3NjhlOWY0M2NmYzcxMTM4MmQyYTE2MGJlNTgxZTE1ODU1YzE0OTYyNzBmMjFjM2E2ZjAwMyJ9', 'eyJpdiI6IlI1UEpPcFNhb0NwVklUZFB2RTJKN2c9PSIsInZhbHVlIjoidnFaR1NNWlZ1NEhpMjZhOHZWK0Z4UT09IiwibWFjIjoiMmYzZmQ3MDhiNGEyMzFjY2RlYzYzY2Y2NDk1YzA0OTM5YmE0NTU4OGJhMTM0YmNmYjk5NTQ5OTk4ZjY3ZDQzNCJ9', 'eyJpdiI6IkZEVGtsSEJYZHpqZlVcL1luY1hjcWxBPT0iLCJ2YWx1ZSI6IkJSaWpkakRJNFh2Y0dXc2VsSDc0U2REOHhIdkFmdTBNRDJSdlVOT2tIYWM9IiwibWFjIjoiNmE4MzU4ZmY2NDliOTBjMmI0MWIyYjlhYTI4ZDBkODc3YTEyZGZiNjc5NWI5YmE0ZTQ0ODIyMTNiN2NkNTg3MyJ9', 'eyJpdiI6ImxMNFArVDJYMEpvZEpremlOZG9xOHc9PSIsInZhbHVlIjoiQnpZWVpiK2hPWWhCb01Fd2ZyVjFtQT09IiwibWFjIjoiNmJhNmJiMDFjNzA5YzhkNWFhMTllZDMwN2RiMmE3ZGQ0MTAxOGYxNWQ3MmFmZGU0MTViMzllZDdhZjkwMDlhNCJ9', 1, NULL, '', 1, 1, 0x646174613a696d6167652f706e673b6261736536342c6956424f5277304b47676f414141414e535568455567414141763441414144494341594141414351326555674141416741456c45515652345875326442375131525a57326e3145786a686b6a686e46557a434b676d46424848524e6d52527a446d4450475838633035697a6d4c475a465252484469446e6e6943676d7841786d564644456a4d792f487130616d734f39337a33336e6a366e30377658597648423131323136366b2b336275716476676e49694551416b4d6e634548676238434f7749574153774f5842303448374c724f345034416e42373449764172344f666c6e7a384335774c4f552b363337544d412f727370587744654242774d48416b634d585349305438455169414551694145786b37676e38592b7749777642446f69634a4669574a2b69474f556e6c482f2f623948486632757358777734482f445838742f2b39576d4133774a4841326347626c594d644131356a66707a4176384d6e42733464626d2f4f63796a674f38413377554f413335612f717942372f396652427a586c59454c414c73424677664f44396a6e667343766764384172316d6b6b397762416945514169455141694851506f45592f75307a5459766a4a2f4376774a6d41477745374139637542767169492f387a34442f486c5557416876755067464d4337724337494c6773384662676438425a79323639752b30754848355a376c39556a363363663564794d7542435143377134364c4445345850412b2f6653714f354a77524349415243494152436f4430434d667a6259356d57786b6e4148666e646754324148594172726a484d5977462f533538744f2b742f415437594d4e72643258664833333855642f7639353265413934355262674e636f69794b726c49574d78384333673538504b354259357a796a436b45516941455171447642474c34393332476f742b714365774a364d3579666541614d35332f4248674c6345427861666e327170556261482b6e4b6a796658324951484d5a37676463433777534d4b3469455141694551416945514167736d55414d2f79554454764f394a71414c7a56314c454b752b387a6341334b332f46764231344e446971684944763731706c506e56674763427535526d58774538476668686539326b70524149675241496752414967566b434d667a7a5445794a67466c76644e58524c2f39535a576666674e6c586c55426141327239637733416e524b624c735a7139714148412f39564f76635551426368357945534169455141694551416948514d6f45592f69304454584f39497241393846446778695837544658756f38585678417733372b695678744e555a6a76677563423979764366416a793645524d7854536f5a645169455141694551416930544343476638744130317a6e4243344d6d474647492f497352527639794e384e664179493230376e5537537541693755336c5a6367517949396b5441394b435245416942454169424541694246676a45384738425970726f6e49425a642b724f666c5847514e7839536d3735597a72584d417073686f415a6c46796f4b52594a302f306e456749684541496845414968734343424750344c41737a746e5245774d4653586b4f73314e44424e354e4f42447a534b5958576d594470656d4d414c6750755775545454556f4a2f463061614275596b38432f416d307373304e6541397747506e2f506558425943495241437653555177372b33557850463169466773617944536959654c2f6c714351375632492b4d6a344146793879757046675077466f4a6b5242594a6747725a567439656c62324b756c386c396c33326736424541694270524b493462395576476d384a51494766393669754833554a683846504455426f433052376e637a545550736b5758652b36317874427379675738416c31786e41474e64664a344375417a7768314968334869626f38702f58363455497a546578704d5178586970534169457741414a78504166344b524e544756332b4b3334717677437542337734596b78794844422f503850424a344a7642713456366d354544596830436142577749487a6a52345563414d594d6f58676433613748434662656b652b5a4135596d5a4d5a3779526266444c6376706851634d73416c593469656b714242596c734e47506539483263333849624a58416a59704c6a2f653738335256344c746262537a336a5962414e5947506c4e466348766a53614561576758524e774a4e46432f673135596e415934422f413077447246786f5150456d357977472b745533416263612f687233766e5050552b4b6d5467336361593132586c4e34474a4e7a39436236796155684541496445496a6833774830644c6c4e416e356758776134792f5970344c2b425436616f56703661426f487a41383841626733634537447937776b684641494c456e684171536652624b5a2b497730757237762b5a68483739494a394c66763273774f364c4a6b53647a3378424e55713559634437774c2b424277424841763866414d4664775675583035677a314775745439504142494576657a5a54667368734143424750344c774d7574725249344c2f4269344b62413534433741313976745963304e6a5943397744324c626e2f7a66377a7337454e4d4f4e5a4b59485a697430584b4c37744b7146762b772b4b4e6e33663862392b4b5641344338386465624f656d65713454626c66716231784b2b436e3552392f6c79374949794551416a306a454d4f2f5a784d7955585665576e5a753355577a384a615a656949684d4138424337615a30636e647976736e2f6d4d655a4c6c6d44514947376337753473392b487a5838585142634376686d44796c3645765a32774e33344b67626b3772676946787a644d78384d374153634654674d634848757957306b42454b674a7752692b50646b4969616f686b66514868582f502b4237774c4e4c726d774e75456749624a614178735750415773357647537a4e2b66367952503450584436426755446541336b726149686530684a49337a756e744853393934436873596a56446b65304b2b2f692f53332f776d387271484c6b57577864467a5075455764454a676b67526a2b6b357a327a676574582f594e797336514b546e33412f375975565a52594f67455043313655484531634648356f3645504b5071766a4d43736d382f737437466d2b7a47446a51486d66524839362f585264324653786578584c7749302f72735341365876434c79386f63424e47676b62757449722f59624135416e45384a2f38493741794147634333416b7957506474785a3348494e3549434c524a34424c41713441726c646f50756a3545516d42624248535263566536796d76587946367a502f41664a616a633366552b694b6b3544584b76387532794b4e485076692f69417343673661716e7037735735624e655143514551714144416a48384f34412b735337505741777750355a6d786e68537964706a526f6c494343794477476d4178774b504141786f744e69626d556f6949624157676365563536582b6e596b475a6750465457317035707337397743687872537052553178584f585235643361412f5857564f45734a642b2f2f762b4b6979316438794968454149724a684444663858414a395364755a384e37504b6a7172775365446a7771776b78794643374a62426e53532f344e38416759464d56526b4a676c6f43352b7a576d7138782b46773061663135356c33576471724b5a5672547136774c674d774f5a316d5939424b73467a3770594457515955544d45686b736768763977353636766d687634356d36726c5655566a6136626c64327976756f6376635a4c5948764133567246516b7a4e414d6a786a6a6f6a32777942707646704773723637724b4e53355a382b46614c3975536f537a63616933453154307064734f6a62507a53336d62735633332f64662f72694e72575a357958586873436743635477482f54303955703566666966444a6850766372654a54642f7278534e4d704d6a634e7153716e4558345075416351437a46566f6e427955442f6a734254796162787679736d3438754e6162444e4236707939312b64386664524b6c79444f42436f4d7341336b55654955386f72677a6f6c7066663469496b63323849624a4a414450394e4173766c4a794e7753734167733663312f7362556e5072792b33474b68454266434a6739796d772f706d6d385974774d2b6a49746e657268526f567849465761333052504b713078596e5662585653366c4f617068494849462b78536d526236726758524c43696d4332676b42454a67525152692b4b3849394569374d51662f7378706a30346661516a6864486f65504648574731524942383475625865704c774f5662616a504e444a6541377970332f52574c54376c706f5869432b64767935374f767141445765685476424f6871564d584e6c684f47692f7a2f4e4839547962376c4969415341694777496749782f466345656d54643741473865325a4d35726b325457636b4250704f344344414b71507539427134475a6b7567655a4f2b686b612f764a76414735625469374e6d4e4f566d41326e65584c61314c45726e647271742b373644796b3475613278703530513649784144502f4f30412b7934347556592b2b6d3868375647737a373530474f4b4570506b5944767662706a366a4e742f76504939416a7343687863686d3332736572446277616f37355a4677413741627a704555393354564f45754d7a762f486172565374656e4172344d75504e766646676b42454a67425152692b4b384138676936734354386d3074326e6a6f634138304d4c6a7436424f504c454b5a48344e4c41313871776b315a7765765076694e39544b6f6a375a7863426877422b457a58306466585a765153466430586e7a444f4c6a6a462b72783847584b6134337957315a3164505776716446494578766b676d4e594572474f7861752f79334139363467723754525167736b384448674773556f2b5031792b776f6266654f515050557830304d64352b5647757a376675443648577664334f31336b36576d706531597256613776796277455743746f6d6d7464705447516941452f6b4567686e2b6568505549474544325765414b6a5174306958416845416d424d524377356b5374304a70333452686d64503478314f4a753376456377455146467972705876312f6e6e4c2b6466376d57722f79484d4252706457586c324b49725866536b7762643662664b646a4d7a5845395569786f684d4434432b64694e62303762474a462b7262506c31424f413151625a744e453341693845724464784b2b444176696b58665a5a47344e6641325572726c77494f622b544576787877364e4a366e7139683079482f64376d3036305849664270762f616f504177597458326e725465544f4541694265516e45384a2b583148537532367634387a64486241476b424f394f35786d59306b68723168534c43506d63783839342f4c5066544e58706150304f2f676f77626163476435645a664e546e394d5733582f656a4f774b362f49785a7a4a356b46715778756a4f4e6565347974674553694f452f77456c626b7372754b6e3042324b6e522f73324264797970767a5162416e306859485657437a546473415238396b5776364c4563416d62774d524f5a5974372b343444486c49786c566e587557683446504c456f73643241712f504f79374736574356326246356975533445466941517733384265434f3639624a72484732374b2f61374559307851776d4239516863484e44342f795a77375741615059486d7159346e6e41655545627644627142766c334a47344e696967426c7639756c536d525832375a783845726a364376744d56794577535149782f4363353753635a394e3041673865712b4e4878795076346f416d42435248513648653339354c4159524d6139395347656b48676832734d326b4476582f51417872364e514e347037505a58354d6258574151794e6b6b5048734b6f4d4734432b5a474e6533343347743372536972446570334833302f59364b62386651694d6b494275625661654e71336e6634357766426e5350776938424c6a5844417758413066324146437a5371384c67466b3965364469306c53774f4e6b72693676705635665753786f4f67524449366e71697a3443465954344e6d4d3269796b564c7463714a49736d774a3036676d646464647776397669506a497a41627647304773352f325a4a677642753564644a6d617132583138333871384d69657a456655434946524573694f2f79696e645a7544326733342f4d77564d58536d3978786b7843636e38505a536e646f73492f7348304f67496e48396d5a2f3836774964364d6b726a4332726441445063334c346e657131534452646c5077484f7438704f3031634954493141445039707a6269703456375447504c4c6748744f4330464747774c72457444672b424751516e586a66456a7177733752396332344e6d652f71555356375148724445784e7a43706e77636a594a564f622b5978337051547941317370376b343775797677696f594737696a353859754551416963534b4336676c673531647a756b664551614c72356e416177646b4e66704f7232472b43736656467178587255596e6f75774e33356a3452414343794251417a2f4a554474595a4f7641753763304f746667522f30554d2b6f46414a6445336752634a39534e665570585375542f6c736a384744676d5933572b765474633566623357356c796c6d6c376753384f6c57305733766d3031414972456d6754792b2f54464837424e773573527a366a715870547741336275534a62722f487442674377795a775a65417a5a516835507735374c7176323977504d476e4f35386a2f4d374f5069726939794d4c42726e6a6b75566f716f76617438702f6f795039456a42455a4649422b325555336e53515a6a7870354467564f572f36732f2f344f415034783379426c5a4343784d3442534e496b35355079364d732f4d47544d33714b59344a444b71636f5766767765726d59777a434c546f6e317130436c55562b6539334f51336f664d594838754d593375527236426f6b397644473079774e66477439514d36495157417142616e7a307a5139384b594d6463614e37414f384733677a63756a484f506e3333394f632f757569576c4d6f5177332f455038674d725238452b765143374165525957756830572f565554386769756e687a492b63514b6c687a32753058793242616e7963437a68717456326e74355949754e6e785265437a7745324158355a3272564a2b6a356236614b4d5a58532f665752724b397a694766787650564e6f49675730537949746d50413949307a665a55566d4a306f71556b524149676330527149612f6564392f764c6c6263335550434a797a46436738417268754b59706c7868696c54775737314f664a6a594a562b523748384f2f427a7963716a4a3141586a546a6d47467a38622b304d5a5376415a636478394179696842594f5946712b4538316e2f724b676266596f5256766631766171326b68447746324c762b7662392b384e7a58636b50716d573476544d6e64546366575a47315575444947744563694c5a6d76632b6e52587379694e65706d43384c4841385831534d72714577494149564f50446171702f47354465553166313749336143376f342f724141716650354475446d50594e304948444c6e69354b756b426c37517a6e556266564537705149483247774e674a7850416639677a764e3150613351775772782f326b4b4a394348524b344e54416e324f4964546f48572b6e636e58376a4d517a496267624a6e68373466576e776873423774744c34457538356f4f5374743474386a2b4534774b784c43617866346b4f587071644e49432b613463372f733074367a6a71433359746636334248464d31446f4873434e77586347663435634a377531596b476378413458534d393531564b51472b3937644b41726f394b483365525061463952466c736e6e614f735937396b6d4f41732f52307273624f50754f62434945592f734f63364f73423732756f627358443177357a4b4e45364248704677494a333177496542757a544b3832697a466f454e4f6172572b4e744148336d6d2f4a34344448417834462f3679464343346c5a5a30444a3978682b5768626359644844687a55716a594e41666c7a446d30666e724f6e372b4b695347574a3449346e474964412f417455662f4a496c4e57372f4e4978476c5544544c63767350523963413831626744314c5a70396d416f532b554c7842772f306f332b4e6b39656e4c63786b39526b77674c357268545737396b4b6d356759634749455a43494151574a324465666c3138737675364f4d746c7439437373487776594e39314f76776d5948616d697744484c6c75704c62522f4f65444c6565622b6a3179792b6d7a6849636f744962415a416a48384e304f722b32756267577071632b6165667379364a78554e516d447a4242355a54733965426478313837666e6a685552634c506a2b344231467659433341785a532f345a2b463135522f7175374b4d59784f706938772b4136556572346474485856656855777a2f5656424f48354d6d454d4e2f574e4e2f4548436a6f724942595538626c7672524e67523654634371312b373647796a76546e476b6677536165666f7466715772343371697a2f386267566344642b6e665550367555544e47776544656d6c4771702b6f75586130592f6b74486e41366d546943472f334365674e6e642f755159483837635264502b4539674a2b4570524d2b2f46667335583036662f7473442b4736685a33534c763277696737655049717246376565424c6656527768547246384638683748513154514c357741316e3376634761746e35507539674459646f4e41324245776d38453767786f4c765055774f6d6477536147782b6d5848572b4e704a71524e594b76687464333958662f366e6b7262385a384439644b6447546670307a6737514e316f3645514167736755414d2f79564158564b542b71727173367034334f312f523049674242596e304851664f53502f4b43495536512b426677462b554e5335447643684f56527256764531454c6a50767650474b3168702b4847413655656e4b6e567839784c414e4b6552454169424a524349346238457145746f306f775533796e747667363434784c36534a4d684d4655437a77556530504d67304b6e4f6a5558557a4f3275374e4c49674c4d526a2f73447a774f2b425678696f3473372f767433415659562f6742676a5a6170796f37413463434441517455526b4967424a5a414949622f457141756f636d584158637637667079724975414a5853564a6b4e6755675332412f3553526d7a672f4c736e4e66702b44335948344d636c6262462f2f73556d314c58416f5562306f34456e62654b2b4c69343155594d566644325638485269716d4b4274593843647744326d7971456a4473456c6b306768762b7943626654766a3739567566564b4b6c5a666470704f613245774c514a654872326d6f4b6737793468553571704b774b664b79364e46322f732b732f4c6f4c723258427634794c773364585364516231664c4831502b5a74382b324c77587858345445647a6b57354459505145707679534763726b366e39384b4b43667177472b4c78364b3474457a4248704f6f466b4665776737777a3348325a70364e776665566e7a3544586a392f535a62506774777a494157633031334a6a4d582f5857543478334c35635934504c62555a2f436b4a78494349624145416a48386c774331355359397276625957726b6d384c47573230397a49544256416b386f7269434f2f77796c694e4a555766526c334e586f66792b777878615673704b7641614c4b554c3578395954436541546a4571596f2b77442f42535441666f717a6e7a47766a4d4251586f6f7241394c446a7477426353666b6879587a517739566a456f684d4467437a56336865774d7648647749787166777259474841652f5a6f44445852694e2f66306b48716176516c5465367543642f5877332f70774d5037346c4f71315a446c377672412b6465646366704c77536d52434347662f396e7578722b76685476334839316f32454944494a414d7a3175336f506454356c46747659714f647966754b41363159696570386a58676c32316472732b375857524d74586e3059426558567176316872564e42514349584179416c4e3977517a705562674873432f776157443349536b6558554f67707751304d6c3951644c7336384d6d65366a6b56745979764d492f3946316f3465546b6c634877424e3652364a396343506c7a306e7171662f30484130556c58505a57666663625a465945592f6c32526e372f66617667664156776d6862766d423563725132414e416a565875482f315657436e554f7155674b6b323965562f476e4241433572556c4a41324e625476577a3270304d2f396d53327747466f5476796b566d55336e47516d424546675367614739474a65456f64664e4e6a396b4365377439565246755a345461465a7a566456546c527a7850566437744f715a6e6c6966376c7343373268706c4d32614a3050377668304d37466f3465484a78516b744d6874434d776656577a4e624e367a4644554467366873425143517a74785468557a6f76716262446244564c536656474d75582f43424d344d754b4e597852534b503538776a793648627459574458336457323446484e69694d6e58582f464d443942585833656e376863585564763172536c4d4c566236697865636854595641434d7751694f452f6a45656942766a712f2b69755a535145516d422b4176704d2f376c782b515742492b652f50566532534544326877476e41363441754d7664706c54442f79364168512b484a6c38474c6c65556e6c4b4b326432417a7750584b665562686a5a7630546345426b4d6768763877706b7266667632524656312f506a344d74614e6c4348524f514b502f574f413052524f7a6868677673777a7874366d6b317362616447582f672f4a58706d7a387852496d6f52722b5a35303534566c435630747055723364344b6b796c577253745771764d5466315737635577476b30424b5a4f4949622f634a36416a78616a50326b39687a4e6e30625262417163462f74685134664c416c3561676b696479376c3766734c5339724836576f50724b6d747742734272726e307153677538756f65656d4f3965517632323650395641353373437869324d586178663846446739444f2f326247504f2b4d4c675a555447504c4c636557774f753777476f42477637746d6d62654f4a7950643935374139734176473170654376686d53317037656d424662662f5253447648544c73782f45384b35436f6c4862482f64356d78465259416531507065756a7679426343653565786d4e2f66596d526a6c712b55444674446e3763787a3148474e6849432b5a454e61794c724d5861792b77787233714c7461676b30712f4c61633174754837727937417a6f6c72444c476b50794e4f4535774274574f3978653933597a344f3146512b666c74307655396c6e412f79734c504264365178612f7a6438414c6c45476352486765304d653041613631323962624a495254334b47316738432b5a48315978376d31634a677454735648324b4e2f306749684d444a4357694156385038584d42524c55437141665a724e61565076356c49597643666c49346e6c4863732f2b7473774445747a4d4f326d716a5a7a78344650486e4a666132692b646d673944473777576a346d3837546a452b52454169424a524b49346239457545746f757562302f333070622b2b484c6849434958416967653841376f372b724653446257627a3253716e7a77433657387a4b573441584a35683354617a36612b75334c58396a4c565968706d76567a3938306f635a456a554530397433704e786a363238444667626f37506f62784f5959613950313634442f484d71694d49775436536943476631396e5a6d3239544f2f6d7272392b78593844486a387339614e7443437956774965416177503643787354597a61665263534674723833445a4e5a6354666233393850462b6c6768506636546245776c3356485848795a76764f764b78706e4e5967312f6865642b78577050466333377677626e334c653476356a49506d59354d37417134433741613863303841796c68446f493445592f6e32636c573372314d7a346b506b6233767846342b55512b474278372f6c697151613761432f727566626f72764a613445474c646a44432b7a5651396546336831394775695775557362734a2b3637336d66627972363673686c415068623551736d4b6451486752324d5a564d62524b594650417863746d784236526e673647796b45596a674f3831476f7676342b7a48734e63776a524f675261492f425a344571414c2f6961556e505278746479703468727a2f7055645556786831393549504338525364676b2f656261636c556f63715976327531774a657055532b376772694a545537446c69346638344a745330427930304945716b7430737846505a6c38437648776b76356d464149333542626b516d4a3766664b5079454a2b764750355a7a665a3877714c6530676a552b685a764132375a5169392b4e45796c4f4a73564a6f767339654665483368762b5776665462723672466f75426e797270413364666457647237432f55355a6463644f69486c387956686b554f3153704163792f4138343031454645373934524d4a4f594662426e3354522f417669743845527947545664656764694c5956692b4139696d745a557372723875504e6a51465371685135334c7150353167676357497a396477493333566f544a376c4c48326f4c4a3131317071334530367750397a2b412f637466757848686837554c4d61624145782f54715a725363387a69643975717837562b6843344e797969497467714774774465436a796a46504261525a2f70597a6f45644e6b385a366d356375475a595673597a774a356b354d592f734f6538757279347968302b636e4f2f37446e4d39725052384264776f4f4136774948462f2f672b6535632f36713659397938347166466253572f7137573557537a4c6f6c6c6d6d376e30436f4e3431394b6d4c6b414d367637456f672f44414f3533643978346b314d55585858372b646f41394a35563858426778354b424b34487941357a4141616e736165513967427333644e5a4e31414b446b3549592f734f663775727145502f6a3463396c52724178416650422f787a59727357734d666346586a4454645537533170384c6a633544797a4736787639744e70363270562f7878714b4847573963444535427a46356b43744d7151367a77472f2f2b4b5479702f526d6a6d524864704868777959366f5a6e704c544b6f7555677a2f2f6a79516932696965344b7550315a36394f6a55486268494349794e67446e4e725746525a6447306a66704c3635742b6e526c51707038304457554e5668306278305847343348353130766d6e7073443731696b73526276726163505a6852716f335a4469366f7474616c6d554c4d64755876704c755951784f6448662b74396758734e5165486f4f436f437a51794a6b334c376965452f6e7565343776796277337a6e3851777249776d42767850516f50746a6738585a67614d5859445062586d327172526f4143366a57793174314b586b4138477a672b365767575273566b6473613749644c31646664326d7077514f3234692b6b697456613976514f77337744304e77755443356364414e33714969477761674957594c7833366453454474624c474c334538422f584646666a66354a2b612b4f61796f796d51654438774a486c762f3953736e387373717672727168356e6d666c306343545176356b4246786b7561502b3734445a4d7659455475675a4a3131472f476a505a6d50716d5a704c553866544b303969724f797261506937414f6972315053762b765666714b394b52712f52453742327842466c6c4761463877527139424c4466337854724c2b61785631302b376e692b4961584555324d7743584c73317948625744764970566758772f6362673247426770624243787955674b6d6a62537171696b373959743178373976557431646e46737a6e453156504a5852344c39744165413377494a66697979536c3858792f5355342f3372414235625653646f4e67513049564863665479394e384e434d6d526b7476426a2b343574616433344d6674792b2b507276314368734d3737525a6b526a4a6e4331526f59574d35695976764276577879773737723164716c645842793278586248664e73657749744b35686a6a49506f614f325257474c504450424a343670676e5a4d36784f576633615678725849627557583252715252623677767636484679416c5a654e794f5a6d364d6d636a41463847537974385877482b6450776e7a617a644c6e35724839355469486d6c474e6c49435a59737a556f6e775375506f433433516e644c30467739534351656646614b596a6779392f50594471344c565370346b4e6445574b6742732b787174554d586a57494e6f2b694335315477446957746548325a694f4468627a4d75317633527a514f304b3736485841753661445964796c7a6163306a32754e39544c415678742f34556c41332f7879707a354847662f61424a3450334b2f3831644f4268793841366e544148396134332f536439312b6733544866616f79514d525847444c313041414f312b4e4e44674632414c77394133315770614e705643333235754b3269503332582b664b62692f4273504b3771535a683250786342586c4738494977423075446670385235485474464e506e686a5876575a344d596466397842793853416e306b634b7069714a6972583346333573304c4b4c726554762b514b35307567475044572f306f766738347672694b6d4f7030434f4b48334d4a6469325a3647734a5974364a6a637948742f5a364b474164675670315679375742443558306e58303567566731672f533366414b366b3132725a43497a6a7552627851563637366c6b37746b5734686a2b79333841752b37424b6e587662494a616d6d34414142386d5355524256436a6844384c4d4b4a455136424d4258586b2b336c426f30643162632f363773326c63514a586a67484f7463774c514a786172317358545150336a64622f77794e76716c6b4f71595a41695542732f4d624d6e774e35782b354b74616174784d787633657649726a4e553553306e6a6d652f5156676a6d6e6d3052384954336a73414c4164397268355464665265376564344b75526a2b302f67527a6537386d30724e492b4249435053426744742f4770764b547743444552664e525049653441614e775a6d6d3833454c4241663367644d79644843426446445a48587457636174797833394945734e2f2f746b794d394d7a5a7935666c66392f4464625864553858766b6749744556416d2b624a7746314b6735357976625752484b4b74666b625254677a2f55557a6a58494d777256757a6c4c303772415a4e526b4b674b774c75787273417265386858395957695670557a45627a376b596a48766e71727834354b5148546d706f43557a46487630577768695a6e4c595863444e497a6955466b59774c2b336a542b7a575453464539384e4a36577454507141764e473265336665494a7978567745334c51774359542b2b74553931472b4969386f55684e734777686a2b637a31666f376d6f66695472674235565876536a475741474d686743466870717074413043306b7a4748327241356c64344c724c2b4b6d744e6a62532b7a774f392b4e6f414c58426261597a3961526c69484a4e34434e6c64382f435970483543656a32615a47766c38336338686e41596b616d6857354c36752f646a4371366c555643594b7345644e66302f61564c54355855673967457a526a2b6d3441316b6b734e6f4e522f31304266785a307966306a317548776b77387777656b7a41462f5a72696e356641457a482b4d6357394c55416930466356533448484e704375324e716f726e676371644d393663323248664636486b6c4f39505467456430706351492b7432357848693449312f466234494c412f4f624c2b70365a384575613045596342384a67633053304a586e3775555a7266653669487775595047747943594978504466424b795258576f71517a2b6156585944766a69794d5759342f534c6779397664325573557461347734333632694c594744426f34574f5771674475586b524d4a574e58576e4e584b6e52754c7279457a306a314a5679367a2b6e7869794150706965363654376941307558756a413264486c6757414674786f54426d3537766c5a4f476550526c6e314f673367653241797864586e7273435070654b715875664d37446b4137306a48634f2f64314f79556f557361504744526f2f7578466244594b574b704c5052457a4256354e664c4b443848584c6e4645663872384c3147652f703976716e46396f66656c4363667a667a325a6e6970637a483073645754536f3155737a5a46326950676239615575727145566e6c444d6235386675624e42765259774b42697136536d516e5a37387a4f326c6d34475749586265434d33626e345065464c3074704a3776376d784d376178723351384d667858697275336e666b794e362b7a38704b5a63752b395654714b44594b41377869506147764f377275566e655a356a59614e426e6e2b556d7971586d6651346e3974644e4f452f72365a786358435847623447704d6b6f3839715a744d545962502f654c707951654141514465392f796d372b65747034536e66666d57682b614456714a7065426b4c416a6366644154647550494530707351557a4435584877432b4d5a42784445374e475036446d374b6c4b657750734762352b587735596d756542697974347a513857674c6e4156344e47486a317a654b4f38617357523376714764396a4451785072524b7641724d4c497574356a4c457366517a2f466e3951637a536c6a37345a34667865334c7855537a5a626a3861616d62526d4b7965625276652f6933475837386b6367456438695174474b306466716352372b47657a384a686751466577754f717461504a6a2b4b384939454336636556747455353361537a3333715950396b4151524d325743426a493930722b595944657444785866323270625a76524237535a647442716f5059356462466f7a554f42707a52416a4c6d6962517a2f377035346e7a586a64637a4c62327059356675417872365a7450797a7631472f4b6332673465343054732b724a4f447a34556d52385345612b7959527362364b2f3769375038614e6946587933584a664d6679336a4737554e37704c6536637951674e70644a316f797a566a314f4179754c38544d45326b75344a6d326445316f4d31642f6f7034646c66663366383246785a446e457050574a72426c2b5a70392f633756744777714d58473869337264705a393973774d354f352b6453657a526f66473371314c5948424f347271646f325833376d2f51697575654c6c6f38305454427676734e7750383038467267684755726b6659334a7043583563614d706e714661647a386f565978364f5937553457526363394677424f6a41307332454e4f7376576975757a5a2f6b5837464e326e634e6e576a332f4837577a5551552f463365756b6c466d4c612f4977743577364e7970707250742b7935544465537175654d486b4b384a435a6d34322f325238345a43754e357035654572426f6e713663626736614e45417868624b314964344a2f4c6958576b3963716277734a2f34416244423830326b315533792b45444241712b36796856344956414b2b3943334170532b2f575858614b4d6131466c31506f6a795271754b4878316f5555785750306f334a71614a4c52624e71385a6935584141346f686758757052462b6b5041484f73612f7759456578707737356e4b7972346e4c4d4c306c664c4f7944656c50334f334c55332b476242516e76552f394e6d76596d56305478654e39346a306e45414d2f35355055412f5530356661664f67754171726f777648744875675746626f6e5948356c665457746f50714f6b68484b416e484c6b42316d64704430473230617663766f73363974476d5435724f492f7134352b634131736e6c4c4b75787241624543704c676152666841344d2f43626f6b725478764450317766632b64634e5a4662634a546162693573474f56337578317836636d67565a344f3554624e5a3555386c526176463361613838644b505764716b466a48384e776c7377706637556a59745978554c674c3167776a777939483845626c5844323352737570737330342b333266596a41586356707967474d5773675654452f756c6c567069613144736e424a524842314d6266312f48575444346231644d3458366b4d624c4577542b3757457864326e6a7872594c363978504b59397446734d4a4632434d6a66754379447459334a734d4b334d545256664c395966385573504d32614b6533306e6c5a57536943472f3070784437347a4451782f2f45317846335972315277484432506941394474612b39534c7431435038734934473069317358733265562f544e58514f31566859504330596f56566d54537a473033707364545651414f7737594a7755324c59396c6a4e4276666230716a50363261535170774e38487469634f674e4e31467a776757414759542b5842594656676658426379544133334d5062552b7a5162314274726d304c663258435162574f744a697963792f747633396d57426938346f6132724e46356571366d3773545058393072633562453266475036746f5a784d513736634e6652396b566252414e6b6e45667554654159382b6e58333762546c7946342f33733138334c63433651777a56566e396b452f4e4a396a735347624855493471726c58365355395a716f392f584833363878543462744174314669635a6e4b4952545530594e673030396f7378685035486a4b4c6c3776535679764737422f4c6538477134433436584944346e7172664b67316146775161736d3555754744555a5555354576685265632f34666e464232586478346574375548644c54302f716631764632744e592f36327837392f3533373476484c634c70547065782b6d693666425576753737644c656e58777a2f396c684f72535750636438344d3268334658797852735a4877412b70706450646a564e575765504268615942676f6f375646386248393531522b524332364135733277706e6e7a6f5972667378645951454a2b6a4c494c6332643170434171505845656631562b584d5770733932476e32506557377737313854544368594147736639502b38642f583763597652724a4c6a424d522f7174596c52377253634a5a7172784e2f6478775067613733576838624879332f342f585a4732476d506a6f71596136726f302f6c745a76496a7a4771552f656272593954723733375559387935596a4b6b777935565a6b343472392f726574476a613778765a7230622b43475a3438784349345438507056797a486f48546c564c73706e4773596870484377684e506166366d4a34616a34532f5867626b68382b503071716b5a67757950342b6664532b616968684d5a2f456a6a5a66336c374848762f62453261394633443637436265517154773758597a546b796758593662784e50423871474a4657593173673866644c646639794e33306977426d746646373138786f73367878656b4c683731334433542b374f322f694241313930396961786a66566b4a6446663854747876416638655375634768584c5a55616d3133654633687064695a584f4176746436574c6a5548647479314e6d362b3547565461666f386e622f4633355750723337697274737a673456574d5a39342b58447762764f79594c595a6a756a783348694d6e45716746764135624a30744d574b324f774d314b344b3039547332753844544278594b2f31664d436e6e79374d2b392f2b336679384c66724174342f3631376b36594878423536512f3653634c766875302b306d526135573939784f737165702f55416e4f636b72484c533566522f62364d2f4b6a5236622b6a4b4c444975415a645a72316463766c586c6339644739626b55576756476d346b616d6637535a532f544231562f6165596973546141612f6838437a48515536593541585a423747756970594351455171436e42474c34393352694271795778342f6d63336558736f7235785638333444464e5358575073663177653853744f492f7636776941753248756e476e383337516a48566256726637527a774e75587a6f3033376e75505a4674453944674e4e5767476363693352423454616b6859652b784b62715a672f5161416e4d5479493930626c5335634a4d457a417638715561714d41742b475a543438726773624a4c6b61693733474e706737567556376c3556664d7072316f7656614846694c383364666f507964506b5a6f2f674f74747939565579565435513832767277526a596d594c70426e31454449434f724a37417a63456a70317344596f316576516e6f4d67524459444945592f7075686c577533517341734c4c6f756d4846416e33452f30685a374f69432b6a467642755a5237626a5254617432354d6c4376537a466738384b6c4576434258537179784c357248767261786232426653635578394147577250496d4a66636857746b74515138336131784a385a30765769313361653345416942725243493462385661726c6e4b77513039743146726d4b4741764d745a32647a4b7a546275576637736c746e3967706c7a354b79732b734157754e4537674634536e544e646f626175315965447a796d6f5a563579622f524f7933377239436e79366e697469712f396e3855773952516c3043727652723730367a724d737a52524f73516d416942475034546d656965444e4e3061507072577861384b547343332b6d4a6a6c4e517732775439797a704d5232767166644d776463484d52326738534436754f76753836342b4b4e5769446863724f634a726b7762416d342f627a4236527a524f6f426150794c647338753058757543767769744b416979357a3245644349415147514341767977464d30676856314e433355754373754876307952474f74303944387054466f6a4d65302f395071624270385a652b5348553755736578376661626561615a4476574777487636416e366765727762324b4e5563453061784e564d59744e466252664179736d52454169426752434934542b51695271706d72507550335759476e392b304350744566433035646c6c46393034433366552b35683254783933335877386b624347774268456c36725a4864486441643155496f73524d4b4f5056615264794b5a6f3447497335376e625749724b2b5a476c31735138392b5761454169426e68434934642b54695a6977476a36442b6a732f65673047476e2b767a6764393461666a626957626b67337442526773323755662f3171444f6d733543644c6478344934786f454d5858546a4f626778694a2b576d6751785574755a5755395150456c4a52706c326547367246642f566e745265744c7850584b424851694145426b596768762f414a6d7a6b3674345065503461593953663141564148343356506b2b4a465a5550416a536f39793437364d6633574f46714a4673777a454a5751785a336f48384c6e4c5978694963422b77783555443355585865316d35533645346d54574f34452b65796165765959514c2f2b5072394c6c6b73697259664167416e4538422f773549315939566c663644725568355a7173766e67624876797a64496a4b31507350626b596d38634f34486d783675644869353544666a666441586a74444739335362383767446b596d6f72576e72684e575369365949777368384464473635335a76425a64525876355977717259624142416b4d2b654d3677656d61334a444e2b2f3832344c6f7a49396533314d7776326545372b534e786e35496c3569706c4a335249325a4b4762766a3776483456735070786c563842756935466c6b4f677576705932646d4d595a483243526a4157786456316b775977695a432b7854535967694d6845414d2f35464d354d694834636447467942335571753434365472684c554238694543303053612b764a487750344e6e2f3468505270444e667933412b36316870746163764d762f2b6c3744764241774469575679362f75386e31594f7945693166463332636645774a4d626c4979344242596845414d2f30586f35643556452f42354e52505153307648476c7a4b5634426254645356346c2b414e7746584c4735517a77514d49423269584c4a52784f7053774463484d496862462f367a717034582b4e6b4139422b36696b3842486c4532414977466972524c77424e444d344c7066766d686470744f61794551416c3051694f486642665830755369424d35564d514f37304e635667537250574e484f6c4c397058582b3833754d37647a747557564a45474f4836757238724f71646475774f664c745830332f4b3248384a6b317871574c3157666e48473875573579417a3730427668716c47716552396769387637685a336835345133764e70715551434945754363547737354a2b2b6c365567414754466b427952327057786e777366664f5335556758714c755550792f4b73672f33473552385a46484572443539444e59307346473373396c5568675a525078623457783941546b6948387a524f7550493961322f69337737634448686563615671722b57304641496830436d4276436737785a2f4f57794c67417544623637536c4b3859424c665854645450364d65766d644d70536666632f674639307256524c2f6575713941704176336a6c35577359317931317461566d64696746304478526173702b674148567832327031647a55426f4761356a6666737a5a6f2f694e6d776d6636304f4a4332553672615355455171415842504b69374d553052496d57434f774d484c4a4f572f634858744253503674755a6f3953644f743070574f72376e6f4d50795a78742f78784d774e79782f396f51442f756a335530574538683767513859615a2f46796e6d4e50394e5233716c32784d4a564d502f584d425241624d5141643136584d782b45726a365169336c3568414967563453694f486679326d4a556773534d4e42337656312b4462676e447154347a466c4b39567064657052626c4d773959367a36366f372f697746544236346c62796e784777732b476e50664c6e747a6c7a384e4f45586a4c76334a44535939624f36576375477943587743754270676d742b6e4c72757a4562642f4c6544445a54473766647a57526a7a544764716b436354776e2f54306a337277476d7675314b3658346b2f5849484e2f66367548465052622f67476750376c69796c4a3979736675502b353454556e36373258637a74474f6a666b78614e62673257574a56585a64584c6b77624f62697437394841792f4d44762b7930432f55376857414c355157386b33624773706d595030464737453257327374643456414350535751463653765a32614b4e59694151333864367a543368644c5a70772b564658565665484e7744556142722b31436d6f6537526152394c597046327576626d6a336d426b336d7873413732745a65324d6d3343326564656578472f2b2f7267382f62726e504e4e63654162396a4a35546d504b6b787531646b666749576d4b7375556a6375692b2f35373836564952414367794951773339513078566c467953675965324f73686c6a5a75576a70546a4e347866735979753375394f7473587562637250754a427241552f55666479374d796c54466e5837545a796f5763644d515831526b7273754f735156726959622b39515a5353324252466d4f345839632b586679654154783044414e6130526a4f336167333457396872635876696c524a4e79455141717367454d4e2f465a5454523938496150525a61645864354c504e4b506644346c652f377a71466d646f6379366d4c38566d445767384864467634585a7564444c53745770473171663466415050374f3065624564322b6467494d696a5a516546767964574250774c6d4944496541686578306a31503858593078447162743266436b362f6a537142572f4c39423242326b7642454b67667752692b50647654714c5261676d59436c526a55474e76566e344a764c356b7544433133666462564f31477745476c76642b584e4a61624e576862564b6433545a32785a4d3352743335577a50447a32684a303637776f316a62514a5570665a593341656b497737384173484759427143793635695857762b7638485a322b4247576264536d795067474c49445a646f767a7650507435596b4a67416752692b4539676b6a504575516c34314732752f504f7463386333537244777077426a41375969376c686242644d64614e314a4447544e37764a4a5365706963365647657339664132666643757735376e6c774b564930397344704f56414d2f684c64773351542b7a6c676748786b62514c4e6e583676534678456e7051516d424342475034546d75774d6457344346736136353479662b566f3376777a59762f7a4650486e6d3731657176707033334f7778367755637a36336f794336384a504353346e4c677272314d546646704b6b2f39742b732f577833326e307067384a4e3657685634712b504b66536353304f67336c756536774163445a6b30435a73767970464d3562385048503768434941516d514343472f77516d4f554e6369494242746e63456444335a6452737461575234496d424f38574d61426166386a626b5461634375625479725a49723579304a616a65666d37636f695348637255326836436d4b4770623358436179394d4743524963584667584b4777763555674b637833752f6953702f7657747870504d51796b6d3052384c663231754b575a37784d354551432f6a364f42576f68514e3369746e707947613468454149444a52444466364154463755374961446872322b2b786b557a36387861797569762f786e673471556f6c62765837764b374b496941526f696e4b75624772324a4b52744e3166694341516d4142416934656479692f30593876304d36596270333136642b726e4b534e6159775a537769457742774559766a5041536d58684d4161424b726866352f69676a49764a426341566f5039632b4e55594e353778334b646762687661777a47496d6f75416a365a486671785448476e343768684978653943387970783238304d7834354d573567484e4c70444b587a454169427a676a45384f384d66546f6547514558416e356748313563566e5268325569734a2f436c6a53346179643972674f6b794e5a7474355772465057636b77387777656b4c416b7a57445675384c764b676e4f6e57687871316e30684b62737450556e5a45514349474a456f6a685039474a7a3742624a36412f38664e4b476b6e392b52395938733358416c45473064316a70746370475036584155793532587a586d4862786c73576c4a7a373472542b4b6162436b64545646717a4c46724458574d76414554542f2b4b735959485a656e49775243594e6f4559766850652f347a2b73554a57506e537a4437752b4f756a62736e373935552f723956364d7a5a676e6b7841693275342b685a4d70576861314e6b716f4a38447a4a6830784f7056536f38544a4744424e774e5a6e316c71516b77467756566e54744665437478374b6f50504f454d67424c5a4e49495a2f6e70415132426f42672b55305a433952626e63332f3558624d5069333173747737724c327752554255357a4f566b4f657572764663475a78584a70654444422b524e6b462b504b34686e65793065686561474737327a542b786853356834313833426c65434954414a676a45384e3845724677614173566c5266656436734c7a4e63416474696c57765478464b55526d45617a627a5477644479305a652f3659707959454f6952676457777a63536e6e424b7a4750555a7830653147524a576e6c72544259787872786851434962414167526a2b4338444c725a4d6a634762674e34315258336e6d597a734649506f4f36385a6a3273317155446c75673554334251354d797449705041614447654e736c5670506f38615555746536466c5943312f4376736a76773663484d5542514e675242594b5945592f6976466e633447544f414f35526a6449566767797142564b38464f5158546a32516d34563850592f777277363149735352656e4643536277704d777a444871376d4a787653716e47636e7a326e776e4f6259484143394953747868507154524f675257525343472f36704970353868452f445933445364796f4f41357735354d48507162685664383646624a62655a4765537a7051364278636c2b4e5764627553774575695a77775a4a6c712b6f78354b713146775665332f6864376c4d433663325746516d424541694262524b49345a38484a4153325465415a7745504b4a566343616f72414d58497a594e6c632b38396659334436374f7453384e4d78446a786a6d67534279356255736e57776e6c5464487a44377a7844456c4d447642693558334a574f425059734a35424430443836686b4149394942414450386554454a553643324239785266646856306c3030586e7a484a6151467243656a434d787563367a6866434477622b4d475942703278544a7141626a3576622f79756866477773746a74712b75656451677366476674697971362b657733365a6e4d34454d67424c5a454949622f6c72446c70676b512b436a6744714546623359476a68374a6d486346724a62376e48584738797a674b534d613730696d4c634e6f6d59437550724f6e643363425874317950347330643648794f3731706f78477a694e3169684a73516933444b7653455141707367454d4e2f453742793657514961505262614d7343573963632b4b6a6434627831715372737a75466159737943376a335a32522f345a45663954524f344c76442b637063755036634872447678386f34436750306d7537502f6f704a2b7441376f463655716548366a6d353769334241434964416b454d4d2f7a304d496e4a534151584f36765a6965556a655949596c4769366e3864415059417a6a724e7053336d756e6a67415145446d6d476f2b7379434a774b7548734a334e656c7238716279794a67325548736e69783641754669597933786c4f36515a51773862595a414345795051417a2f366331355272772b41665051573446334b45612f4f636f76425268302f4268676832314d72706b2f644f2f35655236414541694264516c34777665524e6637322b6f325467633367733572757863734e4776656d4672315743644464566a75655072694948337531346332777a4c55684541497445496a6833774c454e4445614176396252754c48587a6566506f6c477662454731775973326d4f365459332b575448647073474c467643786d72412b775a4551434948354356696b7a673041632b4b764a3063413379345a6759774475674b7759366e74596256715439742b73734669664c627445307078764e6342663574663356775a4169455141764d54694f452f503674634f573443377752755844373270766a72536b7a5a5a78564f585857736b4c75652f416734422f4265344844676a5448797535717939447469416d3443364261337978624771497651397343664152634b467967372b4638764e54417367766574387676316d6b6749684541494c4a314144502b6c49303448417947674957324632716333696e557453335633426a332b31376633336f44472f6a79696d383562536758685134486a35376b7031345241434c524377434a6778732f34753731593265452f706c51462f6948777a664c506a344654414963426632326c357a51534169455141693052694f4866457367304d33674342774d4730536d362b54782b546e63666658625043666978312f584749332b7a353977454f48384c5644344d57447a72717a48305736435a4a6b49674245496742454a67776752692b453934386a50306b7844517039666733716134692f6342514a2f664b6762556d676466663178332b2b70696f5332634c6a704d72666b4a344e64744e5a7032516941455169414551694145516943476635364245446952774b314b2f6d783935356370527748664b33325a5155672f333067496845414968454149684541494c4a564144502b6c346b336a41795867416b41586e7575736b5a586a5a79565972316239504c494532527063612b6f2b43325a354571447254337a77422f6f41524f3051434945514349455147434f424750356a6e4e574d4b5152434941524349415243494152434941526d434d54777a794d524169455141694551416945514169455141684d67454d4e2f41704f6349595a4143495241434952414349524143495241445038384179455141694551416945514169455141694577415149782f43637779526c6943495241434952414349524143495241434d54777a7a4d514169455141694551416945514169455141684d67454d4e2f41704f6349595a4143495241434952414349524143495241445038384179455141694551416945514169455141694577415149782f43637779526c6943495241434952414349524143495241434d54777a7a4d514169455141694551416945514169455141684d67454d4e2f41704f6349595a4143495241434952414349524143495241445038384179455141694551416945514169455141694577415149782f43637779526c6943495241434952414349524143495241434d54777a7a4d514169455141694551416945514169455141684d67454d4e2f41704f6349595a4143495241434952414349524143495241445038384179455141694551416945514169455141694577415149782f43637779526c6943495241434952414349524143495241434d54777a7a4d514169455141694551416945514169455141684d67454d4e2f41704f6349595a4143495241434952414349524143495241445038384179455141694551416945514169455141694577415149782f43637779526c6943495241434952414349524143495241434d54777a7a4d514169455141694551416945514169455141684d67454d4e2f41704f6349595a4143495241434952414349524143495241445038384179455141694551416945514169455141694577415149782f43637779526c6943495241434952414349524143495241434d54777a7a4d514169455141694551416945514169455141684d67454d4e2f41704f6349595a4143495241434952414349524143495241445038384179455141694551416945514169455141694577415149782f43637779526c6943495241434952414349524143495241434d54777a7a4d514169455141694551416945514169455141684d67454d4e2f41704f6349595a4143495241434952414349524143495241445038384179455141694551416945514169455141694577415149782f43637779526c6943495241434952414349524143495241434d54777a7a4d514169455141694551416945514169455141684d67454d4e2f41704f6349595a4143495241434952414349524143495241445038384179455141694551416945514169455141694577415149782f43637779526c69434952414349524143495241434952414350782f51335147554f4b3551324941414141415355564f524b35435949493d);
INSERT INTO `commande` (`rowid`, `rowid_client`, `rowid_periode`, `nom_client`, `sous_total`, `total`, `date_commande`, `email`, `adresse`, `no_app`, `ville`, `province`, `pays`, `code_postal`, `telephone`, `poste`, `notify`, `facture`, `identifiant`, `paye`, `prise`, `signature`) VALUES
(18, 5, 3, 'Test Test', '59.80', '62.79', '2019-02-13 17:54:47', 'eyJpdiI6IjNiQkhWRmJZNWpLQVhpR0RkaUlhZVE9PSIsInZhbHVlIjoiMTRwU0R1YnExbjhOeVRFaGQrUjR6MG83MWUzNmNncldmMU9SS2hjSUhWQT0iLCJtYWMiOiJjMzNhMTYwMGFmZTZlMjdkYTI5OTRlMjQ2Y2VlNDg5ZDI4ZDBmNjE4YTkyNjgxNjVmYWZlMjExZDVlZjNjYTA0In0=', 'eyJpdiI6Ik9cL3FiREVHUVRwMnkxRHB4STFTdjB3PT0iLCJ2YWx1ZSI6IkZoNk1xelVUWmJYM3JHN3FBM2ppc2J4Z0dKOG1QU2ZySUs0ZzZ4enR2U3hvamVrUGVBaU5QV05hdFpJTjl6R1wvIiwibWFjIjoiYWI4MmE1YTg3ZTk5YWYxNmIyNmQ0ZTY0YWY3MDA1ODg2OTI1NTQzZDc1NjM3Mzk1NmEyMjI0ODZkNzg4ZmNlYiJ9', 'eyJpdiI6IlJJOUU5QmxJZjkzeVArd0xMemEzRlE9PSIsInZhbHVlIjoicDFmNG1ETmZ6VVk5Y2hUTEo2Q1N3dz09IiwibWFjIjoiMzVkNTNmMzBmMmU4NmJiZGRjODI0Yzc1MDQ1YTcwYWM5NzdiZWNiMGRkNGRjM2U2ZDNjNDI5MDc1ZjQwMzQ5YyJ9', 'eyJpdiI6IjJoK21FQ3pLdjBKTWVpVEJ0bVF2aGc9PSIsInZhbHVlIjoiQmN5N2NZSHUxRTN6WlhWZG5mbGtwZ2Z4RmRuQ1BsQU1wZExnWENzQkVKQT0iLCJtYWMiOiJkZWY5OGY0NzI1YzZkNDY2N2FlNmViNDQ3YzRjNTM2MDU5MDBiMzJjOTMyYTM1NzQwZGJhODRiZTQyNTZmZjY2In0=', 'eyJpdiI6IldITFA1UVFQNGFwQnUyQUZDbHVWTmc9PSIsInZhbHVlIjoia1BCMUVvK0l1alhjckRuZ01nN2xrQT09IiwibWFjIjoiY2ZiN2NlZjQ3MzAzM2ZhNDkzYjdkMDBkMmRkMWM2MmUzYTA3NTA0YWMzNWI5ZmFiOWNmOWY2YzM2YTM1MzUyMiJ9', 'eyJpdiI6ImZLdkR0SW5XUVAwbmpBblkwQ01FM2c9PSIsInZhbHVlIjoiN3dnbXFFengyXC9VbnI4XC92bGJlZ2V3PT0iLCJtYWMiOiI3NDJiMjU5OGE2MTFiYmMzZGJkZGJjMGU2NjkwNjc5ZDEyMWRmMTE5Zjg0MmNmODA4ODAwZTc0NzkxMzQ4ZTQyIn0=', 'eyJpdiI6IlJcL3cyUnpoOEFBQXJvc29tVG9LY3JBPT0iLCJ2YWx1ZSI6IkloYUVXdHV3ZEdybVwvTnBcL0Jrc0h2QT09IiwibWFjIjoiNjEyZGExMzYxZmU5NDJmZjIzNTU5YTY2NTdiMjVkOTU5MjkwNDcxZTgxMTQ3ZDQ5NDA5NTE5Y2RhNjAxNzc2MCJ9', 'eyJpdiI6IjU4NEc4QTZqRjJBd2hEaDN4bThPbmc9PSIsInZhbHVlIjoieFwvK0Y1Y3hGa1dSWEdyQUF0dVkrR01CVEk3R0xYRWc5S2VGeTArbXdXUlE9IiwibWFjIjoiMDdlMTRlNGJlOGRkODlmNGVlYzkwZGRkMTcyOWUwMDVjMmUxMzIxY2IyZThhZGQ5YTliOTJiYTVhN2MwMGNmMiJ9', 'eyJpdiI6ImhUTithUjZTdUFQbUhqd3dYazk0Mnc9PSIsInZhbHVlIjoiNjRsaW1CcUphTlwvNlExc29GXC9PbDZBPT0iLCJtYWMiOiJiYzY5NTk2OWQ5NzFmN2ZkOGQ3OWZiYmIzNTgzZTA2NTBiZmU5Mzc2ODAwODcyYzZiZWM0NDZlYzM4MjUzYWNiIn0=', 1, NULL, '', 1, 1, 0x646174613a696d6167652f706e673b6261736536342c6956424f5277304b47676f414141414e535568455567414141763441414144494341594141414351326555674141416741456c4551565234587532644364683931646a2f762b3972546b686d6c5967536b6d53715a45673049574d704459674945596b476b6b516f6f73786a687054496b49696f7a49574d49564e4b706b775a792b7a392f363950377358714f4f633530783757337674375839647a2f59626e37445638316a3537332b7465392f412f73706941435a6941435a6941435a6941435a69414366536577502f30666f61656f416d5967416d5967416d5967416d5967416d59674b7a342b79597741524d7741524d7741524d7741524d7767514551734f492f674558324646736e7347754d344c6a57522b49426d49414a6d49414a6d49414a444a614146662f424c72306e3368434270306c366566543164456d76614b686664324d434a6d41434a6d41434a6d4143567942677864383368416e55532b416f53536a38795036536a7169334f37647541695a6741695a6741695a6741754d4a575048336e57454339524c34684b5237527865666c4c52467664323564524d7741524f346e41436e6a596449756b545371337a61364c764342457741416c623866522b59514c30453369587045644846695a4a3271726337743234434a6d41436c78503476615272423475765362716a755a6941435a694146582f6641795a514c344533535870736450455453577656323531624e7745544d49484c4366785230716f5a6933306b48574d324a6d414377795a6778582f59362b2f5a31302b416a4437766947374f6c3752752f56323642784d7741524d5157635232795469634a6d6b62637a4542457867324153762b7731352f7a37352b4170744c2b6f77562f2f7042757763544d49457245466850306e657a2f2f6b2f535a74494f736563544d4145686b7641697639773139347a62345a416e7336544876326461346137657a47426f524d5966666241673954432b7734646a4f6476416b4d6d5943566b794b76767554644634422b537269547054354b753256536e3773634554474451424e6158394f30524175644b75734f6771586a794a6a4277416c6238423334446550714e454c684d306972523037615350747049722b374542457867794154474b66372f54394c2f44686d4b353234435179646778582f6f6434446e3377514258725a4a584c323343654c757777524d594a7972443152326b7934502f4c575967416b4d6b4941562f774575757166634f4145722f6f306a6434636d4d4867434435543051556b38662f4a332f56636c625478344f675a6741674d6c594d562f6f4176766154644b4946663837794870733433323773354d774153475343426c464350476949772b56773049763557302b684342654d346d59414c4f4d4f4a37774154714a70437362716b664b2f3531453366374a6d41434545685677306b716349305271372b4e667235485447436742507a6c482b6a436539714e45526a31733757506632506f335a454a444a72414a79546457394c764a4a46675949324d68742f396737343150506b68452f43586638697237376b3351654272492b6e7a2f4a3172677272374d4145546f4749346c634f704750354c535a746c534f3771516c362b515578676d4153736841787a3354337235676a6b71547a787462314b633132374a784d77675145547942582f517958783779512b65527a776a65477044357541466639687237396e587a2b42504c43583376796471352b35657a414245354353717738572f36306b585a42424f556353566e2b4c435a6a417741685943526e59676e75366a524a5956394c33736834766c585374526b66677a6b7a4142495a4b674f773971306e36704b51744a66307a413047574836714a57307a4142415a477749722f77426263303232554145567964736c362f4c796b545273646754737a41524d594b6f48765337715670424d6c375254352f484d5766763850396337777641644e77462f3851532b2f4a31387a675752785339317349756b4c4e666670356b334142457741416b6e786637476b413633342b36597741524f416742562f3377636d55422b4255662f2b71306e3657333364755755544d4145542b44654276307536737154744a5a305352627a79643737662f37355a5447434142507a46482b43696538714e4558426762324f6f335a454a6d4d41496766543853526c384c705230382b777a4730763671716d5a67416b4d693441562f32477474326662484947314a6631777044742f3335726a3735354d594f67452f68444a424e4a7a3530684a2b3256516e692f706b4b46443876784e594767457249674d626355393336594966466a53646c6c6e574e76576161707a39324d434a6a426f41763872435663664d766c634e5568734b4f6e72475a574c4a643130304a513865524d59494145722f674e6364452b3545514b6a626a353753587039497a323745784d776761455449472f2f615145687665665a444f517050666d316459436833796d652f2b41492b45732f754358336842736959502f2b686b433747784d7767663869734a756b7434386f2f767a547a7958664c43597763414a572f41642b41336a36745243775a613057724737554245786752674b6b384e7a6669762b4d74507778457867514153762b413170735437557841692b5364454457473159324e674d57457a4142453269437748736b5064794b66784f6f3359634a6449754146663975725a6448327730436f3866705730763657446547376c476167416e30674d426e4a473065726a323530634775506a315958452f42424a5968594d562f4758712b31675447452f444c31586547435a68416d77522b4c476e4e4d52622f58307536586a59773677427472704c374e6f45574350684c33774a3064396c724171744c756d526b687636653958724a50546b544b493741587952524b527a4a6e7a38452f424c346d38545070754b577a674d7967586f4a2b4574664c312b33506a7743564d4c634b4a733242584e654e6a774d6e72454a6d45434c425034764648372b764649326a69306c6e573746763857566364636d304449424b2f34744c34433737783042752f6e30626b6b394952506f484948304842704e4c48423153582b3234742b353966534154614179416c62384b305070686b7841563435716d516e46705a4b755a53346d5941496d3044434270506a6a646e6a396b62357a3477536e415a774b57457a4142415a437749722f5142626130327945774347536e7066316442314a6632696b5a33646941695a67417638686b4a543762306a613049712f627730544d4946457749712f3777555471493541626b6b6239613274726865335a41496d5941497245306a506f754e47676e6d354b6e394f455144384e384d3041524d59446745722f734e5a61382b3058674a586c665458724975644a4a3159623564753351524d77415447456b6a4b2f513653546c7242346d2f4676397762364e715337697670455a494f6c765339636f66716b58574a6742582f4c71325778316f79676564494f697747535044634b695550316d4d7a4152506f4c51454b6476307a5a72654f704174485a73727a69534266354371532f744662457432613248715339704b306f365131526f622b616b6c3764327336486d3270424b7a346c376f79486c66584350784f456a37397946615350743631435869384a6d41437653427743306b5878457a4776654f2f4a656d32385873483937617a354b744b4f6c7a534e704c5748544d45366a437759614d433834636c6e656f4e576a734c3163646572666a3363565539707a5949704b4e312b2f6133516439396d6f414a4a414b506b7654574652542f6a306d363377712f4e386e714361776c3666475339704230307a484e4534543961556b766b5554565a59734a31456241696e3974614e33776741673853524a4873516a4675696a615a544542457a43424e67676b786637766b6f673947705558534870322f4b63742f765774304a47534e704630353879316974352b494f6e396b74346c3663763164652b57545741384153762b766a4e4d59486b435a3863446e7059634c4c633854376467416961774f49474c4a4e314d3069636b3357644d4d7a744c4f6a372b337a7241347078487237786e5a46444356332b7a714f6c434f75667a776c586e765750694c6172723353325a774977452f4b5766455a512f5a6749544342424952374163515849485344724370457a414245796752514b2f6c6e5139536674494f6d624d4f50417054786c6972414d73766c4259387a6350502f324e4a563158306b636c2f556a534f384e315a2f48576661554a31455441582f7161774c725a775242346771545878577839624436595a66644554614249416e6c476e35554b434f494752467a534f46656749696457774b42677536326b58535539584c7138557676504a5a3065376a743538635943687573686d4d4234416c62386657655977484945654948794176696770416374313553764e6745544d49476c434742352f6c4b6b383854746b4751443479516c4937414f73444a756771434a3262713770477347547a496d66565953526838585031767164765846625244776c37344e3675367a4c7754576c7654446d4d78744a48326e4c78507a50457a414244704a34425868346f4e7665556f7650473469624168342f7a75502f785870594e58485265707045536552666b7351376d736c76626d54643455486251495a4153762b7668314d594845435a3054774841567765494661544d4145544b424e4172696533456a534c2b505053574e4a46763862532f70466d774d756f472b5566654b7a396f335943495a4546665a545967507730774c473643475951475545725068586874494e44597841376b763766456d4844477a2b6e71344a6d454235424a4a43543261666d363877504977567843514e4e51735a32593565486f584d634e56454f43553554644c756b6969675a54474258684b7734742f4c5a66576b4769437767365233527a384f366d3041754c737741524e596b63424e4a503073506b46646b6231582b48527939626d6a704b384e6843747054506558644964737668644c657647453745634477654a70446f324146662b6872626a6e5778554267727077372f6d714a414c714c435a67416962514a6f47335278353578764177536539625954422f6c4c5471444a3972637a35563945334b7a654d6b3354496134376c4e66594e6e535471336967376368676c306a5941562f3636746d4d6462416f48314a583037426f4c536a2f4a764d5145544d4945324356776d615a55597745717050506b49726979342b547865306876624848544666654f3277326e4876535774452b354d3335643063766a785438707956504577334a774a6c457641696e2b35612b4f526c55766738354c75466e6d77386657336d49414a6d454462424a4a2f502b4f59396d35504a35623475525055326d5635546d5467325451732b32787154704a30516c6a33757a77336a3930454b6963773765465165596475304152365143433959456d64392f51657a4d64544d41455436445942436e47526953624a744866376e79526451394c374a5432305931504866516472506a384852785869723076366a4b53765344716e592f50786345326755514c544867364e447361646d554148434c776f6a6f775a716f4e364f3742674871494a44494141436a445a78575a562f456e3365514e4a6e4635694b53395a4e704f30755352536a39354f306c62687a76506a555062504b6e6e774870734a6c456241696e39704b2b4c786c4534675766742f4b326e3130676672385a6d4143517943514d7253773251766b5854394b624f6d324f43744a5a307661643343434a476469457135323062696849306b5555434c6f4e786653547169735046364f4362514b514a572f44753158423573797751654c756b394d515a656d74397265547a753367524d774151676b50763363797035304251736e354f454a66303357644771746b68754b4f6c426b753466466e30436c416c552f6d496b546e686d57774e7a7679625152774a572f507534717035545851542b4c4f6e713062692f4f3356526472736d59414c7a454d694c43584c644c4557354d474267794341754944335435756c7a306338795674783237694a706a7a6831774757536a6373466b6b3656644b777a7053324b3139655a77485143566c366d4d2f496e544141436130763659614234766153396a4d5545544d414543694277704b5439736e484d386c342f4b6849546f4844586d5a6d4d7470387336624753626958706d6a464f4d753977596f71696637796b6278544130554d77675545516d4f55424d5167516e71514a54434741306f2f796a2f6837343976464245796746414b356d382b737a79645359423557772f4e7341306d48534b4969634371615254636f2b6d54652b61696b353555437a754d77675345537341497a784658336e4263686b463675424a6e64655a45476649304a6d49414a31454167562f792f46473430303772427a65624e5379722b75416a64513949444a5431524573577a6b7678553073636c45572f67574b6870712b48666d304344424b7a344e776a62585857574143586664346e524f34566e5a356652417a6542336847346b7953552f5354544b76616d7a3247524a2b63394d6f386577485737533371637046586a656a596576356230716969615261566369776d59514b454535766e43467a6f4644387345616965514c4772666c6e5462326e747a42795a67416959774734464c4d372f35655a5434315353526b6e6957613361543943524a62444b7545746638584e4c686b6c3435327a44394b524d7767564949575045765a5355386a6c494a76467653446a473447306d693849334642457a4142456f676b4c763534464a446d75465a684d772f2b4e3250552f7a586c4c527a704e66634f4c4945596448486465656c6b7234355377662b6a416d59514a6b45725069587553346556546b45306f74316e7064714f61503353457a414250704b5941744a5a3261546f36727465584e4d4e6a336230415049766b4e65662f7a2b6279364a314d5545343149522b4e4e7a744f6d506d6f414a46453741696e2f68432b54687455726747456c506952466353784c48366859544d4145544b4948414879547858456f79372f73384b66366e533770394b5073666b6253334a436f4257307a4142487049594e34485251385265456f6d4d4a62414e5354394b58377a666b6b504e5363544d4145544b49684137755a7a737151487a7a43325453546451744b366b67364e7a7a39623068636b6e54484439663649435a684178776c5938652f34416e7234745246346256616b79392b54326a433759524d77675155494843547068646c314b32587a5755665370704c326c485450634f63683838354c346e71665a693677414c3745424c704b7741704e5631664f343636547746576a6c4431396b4965616c367a4642457a4142456f686743744f656e2f7a6439494d35344c505073594c4d764863514e4a76496969584444314a306f6b4270357370304c65552b586b634a6d41434e524777346c38545744666261514c6b6f36624d504f4b382f5a316553672f65424870484944644d4d4c6d4853507141704e5844503538632b796a374b505048682f48694a324d6f4a4d562f31747a2f76515070435a6e4145416c5938522f69716e764f4b7847672b75546634774f506a4949304a6d59434a6d4143705241344b317833306e6a326c665230536154684a433770374d6a47382f6b7041376246763551563954684b4934423733495753386a696130736134384869732b432b4d7a6866326c4d43786b68347436624b734d6d5650702b70706d59414a644a444150305a636531424f76696a70395a4a346673307153616d35657562614f4f75312f70774a394a45417157774a647566556a4b4a31754d763154717a343932354a5061456c434b77684b52324a7078332f4573333555684d7741524f6f6a4d44646f34675750766c4a55505433574c434850492f2f676b33344d68506f425947394a44314e3071306b2f5533537470492b30396530746c6238653348506568495645546852306f365350695a703634726164444d6d5941496d73436742556e516545656b3352397641534c48576f67316e62677a5741356141364573375334444164797055503148534b70496f30766e477145376432556e4e4d6e422f34576568354d384d675142564c314d70656f4c6e6b702f2f454f62754f5a7141435a5246674578697a38396365733656744a346b33484b53385074446c6869324c66354c77504f6c6e5358776745686c65397577364a38673653684a582b6e736a4f59637542582f4f5948353437306c3846564a47306b3650487a38656a745254387745544b4249416c676655554275484b50376654795073506a2f4f494a33303843785474353679566c59385638536f432f764649456a4a653058492f36724a4c4c3348527756717a73316b5755486138562f57594b2b76673845626962706f724479587a6343652f73774c382f424245796766414a37537a6f6d7938762f5a556b375246595252732b2f4e78365a78766153546c6c79616c62386c77546f7934736e734c736b46503462786b68357a7a3942306d6e466a377a4741567278727847756d2b344d67564d6c33565853415a4c65314a6c526536416d59414a644a624371704539476753336d514245755848634f485a6e51655a4a754d2f4a2f584c644642524f33346c3842524464524841466364546b6c653271326d5562525a785077792b4a4732384b4172506933414e3164466b574154426c45373339746a465774714946364d435a674170306e73466b5532364c41466f49377a336153794d302f4b702b51644f2b522f3851506d6144455a59584b76762b4d41462f2b626a47424c684f67304f622b6b70346c69594a30434e62395a3070365435636e5673665972666a5851645674646f6e414479535275704e556e6a2f723073413956684d77676334517746586e73354a534b6b3779376a38735378383854756d2f7830692b2f71715566767269784f4750595147395557636f6571416d63455543704e3945345366765073497031736d53646e4a7469736d336968562f663432475449433876533850617875576634734a6d49414a564557414b7541385977374c737647384958794d562b706a38334144776f715a5a4e6e556e61503958562f537279523957784c5a5453776d30425543664b38654a656d466b744b6d6c553078676275636b6c6d6d454c44693731746b7941542b4843396b736d4f514a634e6941695a6741737353774d6559664f443446434d586877764363544d307a456268735a49327944364c5a6634686b7336593466705a50304b4d774a6e52356e316e766369664d344557435a43453436315a664d763545524d7a792f657178574758313755562f2f4c5778434e71687341724a4f306a366154496f4e464d722b374642457967727754776c662b77704731696771514935686c44444e477351714942716f626d556c55776239346d75637a4a437654634f4a475964587a2b6e416b30535742645362744a326a4f732b37384a35542b6c355778794c4c33707934702f6235625345356d444142593538766769564f7a44386d387841524d77675555495846505375794e496c2b7452394b6e385065397a5a545258503847337446564642702f526565307236575753486933706259744d32746559514930454e705830797178694e5a7466584f612b56474f6667326e61697639676c746f547a516838544e4c394a4c306c6a74554e7877524d7741546d4a5844374b414a307a776955665a2b6b4a306e363037774e53667174704e56477271764430702b36654545554b72536234774b4c35557471493443796632644a6d3054324b30376d5031566262774e743249722f5142642b774e4d6d43384433592f362b2f77643849336a714a7241674164774d396f6a4b7562384f502b50584c4e67576c34327a394a386a4361746e58664c6979495a43414445314243776d3042614257307036636d796150797270416b6d6353466c71496d44467079617762725a5941696a394b503855797a6d6b3246463659435a6741695552514548477373397049536b4476784d6e686c676b6c3547665a356c4a556a746b4771746238546c653073355a67614e6c3575427254574265416c6a3048792b4a44466134322f3543456857737a3536334958392b66674a572f4f646e35697536532b442b6b6a346b36572b537274626461586a6b4a6d414344524b676d7665576b746157394c72774e535a547a374a437532547753554a4d414f335872665454483737537050464536624b5951464d4571453378357644644a3530736d317a695936696e59326d496742582f686b43376d79494970505364464d37424839646941695a6741754d4933447838344d6b586a685753665064375659794b344e32386175354c4a42315163522b546d714f614b556f2f7868434c4364524a34463652597a2f6461376978555647587748564f7a79774e45374469337a427764396361415877495879587048354b7530746f6f3350453842436a47516b70453343465176504146586461315970372b2f646e684563422f2f346d5355507a78665363314a2f64683158377752306c36656f6133436665656644552f48565637487a36384a66614d47794a4141446e7550446549392b34486f317031513932376d306b4572506a37336867434165377a394f4c654f4a544a4963793779334d636c2b57452b57416c7855723051306c55487956764f7062595337733857592b3964514b34335a4461456c2f2b69384c6c68674459756f5367344f744634397a507565572f726a377a64692b543948564a6d7a58526d66735944414679376a3948306e6f78342f4d6b48535470354d4551364d424572666833594a4538784b554a764576534979525658665a2b3659473567596b454a696e2b4e6d4c3470716d4b414b344854346d632b37524a39573563657a35665651637274504f586b54676a556f4279736e5743704a644b2b6c334e59384151776d594869367a46424a596863505749652b4737524f77634e5849343065492b70754357705441435676774c5778415070334943713075364a46724677755948556557496132735146347550534c7178704a7547516b5a6739724d6945776f766e43522b6c745732444c31722b45424a423075365273794d54463855334c7177775a6c79596b5777384467354d36727131756e57786f6b4431564466332b43633356572f434f776663544458696d6d5268684e7250357458533845456d6e685a596c5567754f4f4d476f4b6a436b62726f52564367477742363053416e6f2b3143316d554b634e594e6462737156454d695438526773474f6c55544f64484b666b2f4f5a3965586e6c47354d7a614e736951414741437255506944722f31755348745253526847552f72644b757675456d4b4d2f784e676f346c57314a474d4936524e665858586a62712f58424b68365431616558574f576e427a6878724e6a2b50483365764a396d567a64696a3833787a7379574b6b454f636441666c483335533471647835336c665346474a344c315a53355468674648684a4878477a4d62684b4b454437505a4233426573527a43742f524a412f303836504d785378775646546f506c485364624f785552325537446d4c564e6974593470506b33536e2b4235634d2b766747354932724b464455704f654875365070464b306d4d424b424867576b324b576b7a49326a5169476c326449496a755570574d45366c62386a3561557248586a30484463754a57444c5474323133526e75436c5632467447386d56335a776239474f6e644a4a472f6d564c734246666a637256614246797a52766a7a38344d466e3152764b447a6e396d50716e6b564c424d6a4f632b52493338654e624342624774724562746b41734347355566614a3530566c344372482b6a684a6235523065306e66724c4a6874395572416d75454e5a394e4b554943426535506e784a31664a6e725676793559623434593861433138596d6758534c46684e596c7343686b7034626a56773573734573323661766e3078676f314471385a564730622b4f4a48772f4f576c422b46367a305365416b6252752b446a6a746d4d7867536f4a734d6c2f7a4569444b4e52592b61744f79566e6c75464e624f30524638647646663953526b4942364163544a58467653482b7559684e76734e4145327a57533053733975584377355a6631397032666c77662b62514e324b50783378304356484d664a4c5354656377702f30542f673931703356774c644266776e7777456f62534a52514e702b5735516d67334a4d6443626155576b657876335734357677395843642b464f35565034753443674a304c535a514a774663455434753654346a6e5844616a4d4c6652636b4c4739326834684f777779494973346e336678665a4433484d7548385241344f56482f6c75755059342b4c754864304e54582f79556d672b4c4330472b2b46334f497476626c3363575450374d4349477653654a6c5364446e7255786e4c674c34464b385a562b426d5146774f575a48777653634c43693845636f43546559514d4f79644a496a5768785153614a73424a3375736c375a46316a4d4b387579546365726f732b46436e377948314256444b71684943686f6d74616572395839573433553631424d694b78676b5a4d5662386e594279456965516d59666e7671576e424a7236346c4e786b34634e447a4b4f4c6a6b32516a456a71384573346733414c4a543847516a77516b755a4d4b67596948754a35596f453769674a367a30506537364c64776d4c4b6347302f5032512b446a356d5050672f4b6f34336a74636666415a5465757a515669624f4b333568615376754a78375662683732553775797363454d5372684a764f2b6e73773250796b6e5163456d4663364c4b746a72572f47766b4768336d6b4958493761442b6732345a614c736b7a495a397a6965393559424547684b3851646c2f69416a6f49696244376c464250536c4b6f61547347504a77553349697477416273776c7070694f794c6e66434334666f6d414a4a5a4432595a4574683563383379387339766a657777694c4b473435704d356b4d3437435649666c6e6f30594b5252764b326d374a52626a314367533030527870535747365574724a7644304b41365564344f467634344e617331546d646f384a32705871614877494336335a4766684f57455a426745326a712b53684f4b50336f66374b7965365a77316a2b70356c5471424a785a392b38666646326f654d3973324443482b79653836775244654c6446497a664e51664752434244306d694769667849586e367672346977477150785a344356796a574241536933484e4d69334a50756b4c636e7241596b725038707a56584a53565938416d534f4648594e6a4c33314d57654f623351726f42313453327558574a315069324a504f4a4a534266397a754a4757743241506843352f416e41356274566c66785a456a453556625a5a31646a63546e5545694a586357524a70725445456359714b386f2b466e314e567930414a4e4b333467356b71696668646e79397033516e6338544e3734677872516d41685a6459744a6f42566d794e73684c4c68574d763649415451346e35446a767564497455666d787063596c447573644b54625948763035636b6f5378776f70617146546642344f46527741565869326d43307348594f56596d396f6354423551513467596f744c61496b4855434a5a41544445752f4348437663312f6e39776276683664304a4576504d71744237414c466b67696d782f4a666c6541576852466772616f61644476464547424e74776c58487052396774354a6d454b2b2f6338564d306f507046554362536a2b5750327836704f4a5956722f783851446668716b4230652b32576d66382b2f37537744724e745a755470512b316346706b6c574234316a2b4a4d4157685966765343362f4361734e38384f432f2b45434e6a6a354b64346f646b3462634d31415a7131416d6b344575515933496553694f4e4841594d424a4172454a6b345450766b6a537535782b726f50666769734f6d61773875627365645235592f3645492b644b6646436433655747765a6565507759433075675473572f704267446f7070476e6c3363464745555051506c62322b374734566339696d754a6464582b707665534866564338704b6631773075634e494c54424f73495a636a72384657653172642f337834422f4e563369614930424332564b766a55346f71457a7a734b2f746f54616c78772f334a366358787359696871565a7167684c39643071596a41384e766c5048696b312b5863474b415659735847305945546e6a4743527766476b66626459334637565a50674b7876784a7877476f5467756f64435136616249556c3654374b42786e32754b7146647670383869797a644a63443767335331664466344f3745624b50396b577650705a33665874666152743658344a3365666559755437422b464a5759426330705947306e70614f6b7641617a6b5a34646665796e42617667686f3979544a6730466679584c47713571574d4d2f4a756d3948566b6d43764e78367043736b436a5942466932566236646c7836565346644b45307773424445676c72494a6e43794a4c47354a4f5058427433396f677274654b754a56355875617a52517841366646356e6c6f584c732b583477635a4c51696551562f4a35344c4130796530726272632f543461795a5135514e6c6e71476d44443959486836305949416552316c45714d38694b436234755647746b446f436c6e3451514d464f4b6368776a576d7936427376554a523672476134703241426e2f5239346a366e5569332b6c752f752b4430346d6c55464b797a423969584962614b2b414d484f6b34544e436d35686c72494937436e70794d67367863676f4d70557162356331307670486331546d496b64527369714c34484679514a416e4d585376713338713771456941745279494d33797a582b6b552f34414143414153555242564b4d3939422b5347387961457232695962695a5068426f532f4748585370516772567a697956675975336a57486a6a476470414157504f2b446553317044716f67534f3453744e52694879696c753651344469492b51664a6e4d42376d425643316b7679435a436c56714361366b39515732415359496c3764797733484d4b306266556b364e4b507938642b4a636d78487038554e4a394a777973616d577174506d585042344d4d49646e42625949797565304b7757615868427565333337377379364a736b6f5670636c64372f59594a46616c2b77756c6e494a724266664535493749426935734f362f774b3438355335614630625770754b6667674c7243444a435563506e6d3830416859453445734d766d4b4358616534675a426a684338616d414f73737838776f64454e39455a563648364e776f76682f56684b425459734b6157525250766768634241466850746d5568594e37694755452f7875535358343951672b5862542f726c784868645138666f4c4d4b6b3875665044554c706855392b4e5238524974664171394752344b3759757a654177326b61746c7865497779684448315a613757416d677959704668563432727373617843624e42336443346c36753333446d72784c34646d454d6e4d675171356a48644641744857572f3639576f7538422f45474e73552f486e495a2b737450677959676c71517367496776383146672f3877796b6778735a675668616b51727334307146684d546e4372674e4e4c4e742f395945436a6d7350526431596b35556b5a66744a41654c6b4e6359315a3578796a36574e49436b43564d6b5151367749363433503756416c4c373448673634707a615162486164515570716534334a4c76515334663534526c647454543768667075784d4b44527342495a636e4a4838366d6b6a2f584a4a2b3961304a447a4c6349576239583158307a446362456141553254654d35777535304a5770775044384768674a6c415a6762612f2f447a38556270352b664953626c4e77475749734247515355495a62423962676c56494854686f765a624378424c4f78515a48456a51686c306c494e4164786f324c5352702f6a4c6b526d4855783769526368666a314b50332f326130563153384e6b67634d2b7a507669354576694e395a3541565535365373796555773278355670686734567248454c5769414f576136365671366d48774c7150796c36534f4d327731454d41355158476f366c706553372b4b747a6f68707941676563554a3463334451575075495a58314c4d556c37664b63343554373762662f54564f73524e4e593478433579477a5953346e52475a43556a6462544b4157416d312f2b584758514d484778335072576d5a5954614f3874464130743470784573784970705a6c2b6645515a7650444335416a574e794c2b4438737a63354138702b31343053492b4175732b77536b345971447773356d446157554452764274724345495a2f46502f2f303850556d63773742763362586d762f373848784a4238646c62476170467478563466744b4c6e692b78376d514a494437795649744155355a6343735a566672706866754b594d55684378744f69764c78724b49754278736b5842667246467971654d666b465a4472374d39745835454166767473394442614a6548306d67326743327a35626d6d45774c4b4b36374b445448372b565a636b58335a6338313550384131575a7637456865684734623836627a754c66703659424b786d634d535352745661487537386959734b4c6b6e385033454f72446b5748314b71456d41397a55316d3054484e657833486e4752637759324550336b774d75615536353735384d50767150684b4f6a726d643161345857484e48574c6176336b357a2f74354642464b76795070684758654e6b72375043635746506e4b78646c2b716c326c43374d4d4a486e4c624d35355467375a6459344e44346f65503153793574396b4e4b70626550627a764d635151745637537a4d454d4677524530584e6b56796f736b363248697176573079674d514a744b2f376b336362616a3449334c6569324d53673164595331476c396a356b6c574556352b424c646867616d794b754d38773666766c413654653445664c4f61556338644b68795839432f482f4b4e316e52754d4530314967354263784239496a6b73306d576662535a6f4c3261494f58444b34344847657a3170795945457a4c337a6e797a4f3944786f4f764c79386e556d435364596d2f3435724432464436536476595a4f724f655a6a3237624f706942447a617674355553566241756a79334e636f7048775053396b49567a6e584a7475695867495a6c53594a7a346a386e6d70796243583078595949617a7650516c78414f524670536f687477356563656a6a45706c6e714a55432b2f644755744c784453557a68416c7631736e66724b78416f3455574f57775976334e30637454373376637236345372463853454b4d7939556c4844383350467278792b62593053556d6653793552702b6836574a7a374968345358456151422f5868624b4e554850764b43495456676c346835346f584d3972684a59324648717363687a564531664b453973624c694f332f464476377a732b5745637445652f78453567726565496d3563685657725a434579536238524a4370756d4956734c3537354a6c72674131716e75426245552b6648304573305763796d7848626b5637683152694b795941585a7349436c6a7a4b52685939576d6c737251684763694375437a34396d48595950364830306c7445693863585044705a5a73562f5968722b63754a4a4364444479354d592f4e46686e527947426f4d594857435a53672b4f50506a6757347276526c72555032414a596d384b596f53303477464364456c6d5949354e6c384f506b687a7156506b6a61324b58435a75544648356d715a6a77424b4c4364786b775144414d6b53686e5a536839736e73544838695a426d47694e4847344a4c43556161457437376263792f726a34336c665347534232652b76684f75424f5364393969416b55524b4f45426b507a386553455167476b78675a7a414e75456669634a50384a756c4f514b6378747732756950645970335a527071623152563757694e4f6e5a4c796a38736170326957325169774f635248486266465848444c347751786c784c654e37504e61766c506361704b546e364b437949454f6c4d746c395059746f544e4679657561525053316a6a3630432b6e4f476c393033314e3342792b2f472b4d6b2f4d2b7a4e4e7a3643474255683745754967676936544f374f477965457042594d4e774e654655714739754a6c3159354a52756c374653425465352f58526837504f4d6366643469584d4e6353766b4f62664d526741587739454d4d65536778323376364b794a726964776d493347767a3646337a35755477677853547937327334616856556131307257684d32615a583443754c2f754b656d706d5373504353614f6b6651384b2f767a412f55563752416f526648483578742f634c4b484f4f56694f2f6443696233793869536567427a736c6d594a374a646c476948644c4c4567665a5955412b4f5478396c572b6168516749674e796f58764b6e4662353073695457715349626879596b6b6e2f6f697175416a426e6151744c53466750465865356c5239614f355773393352347a2f462f5533324a51717372527578634d526f45432f424a69415a4c5a667077396561514b4d45536c48384b556c4e674f717849356b32476f58687a6f6f69674f4c35346e694a2b6b58562f4e4a514132484c364c6276536c736579324446662f71396c74777a38302b4f786f434d5a7534687a5446706866736f6e486a736b32584b2b5859456a5a6455695a6754474249736c504c4f4c2f302b774f4430324644364d5871512b686f2f2f6e64486c726e53782b2f786d63424541715538424e34534b6137494e454d47476375774357776769654a7550486a786f37513054774158686553333356662f2f6b51315632543776736c5a39453669594f457a4a6532515663524f625a47743576437334547446477436384c3035306359766f6d2b776f366352735572694e6b5232714a4b47344961355778305832764a4c4756744a59634e466950636c6d646f5049756b51324f55354c65455a59544b4158424570522f4648302b49496851382f7a3349736261346c4a6b4f2b66516c79342b61426b574e6f68304e66382f654e6f446d6d5473386a64684e4a44676349317831784d5459397831575a484c66366c764773576d662b346130672f6e5764732b637959416b3156396256734f386c74372b5a524658375a39767079505734386a3554306b50684a387949776e66536275504f736c474b364c7877386a3445524b4f6c686e463455464c6c4b675645445734374254356663787851506f2f5a4138704d64504a5357414c414779582b626c2b4d4a4c59326a6957377877655a5a614465664b39496d312f77374a574578356c37676e73674c4c65494f38664578432f5334794779532f36716b643830793978524b34767579426a696c4a764d5678515a4c4663613256765a394c6e57635459324c4a43497648354d6c6a7253724230655273366247346e354d6f4845434a54324d5534376848345231715845593772423141696b676b487a6735454732744563416e395a6b34535546494b34654a515170566b32456242796b6f30536f356f76534f6d52424b534b3235736d683547507878453045463667485a32416d4b663138684f75704470744c53652b6165646558653538417a7a7a2f50746d664868315734586e62612f4c7a754d35536c4a484e79734f61374c69777676416b7747566e3950764e4d34304162467a56714535764d59486545796a7059557a2b572f49634979574e712f633351534554704b4c6e53304c6834463677744538676439666770556a786e37356c7353434c324e30436464396a475661366f7768517858586c45534d664f6a41324149646c2f7a39745935347979485264386165774a4275675647325945772b436d4d6e77387137327635347a6a574350324e4179467a627a51785069546234594c735435334c386e3656484f496a69303238487a4c553342586b6353316e344553794d7548355a68454c6866564f516c5347366e5955793545374d6b397a69357135503038545475793549326a744f4d306453556e56696b4a51644a35696155324e79314469732f41627448686f74493773617961376a2f724e5474427951394b507341703363725666566463677156586f356c6548314a62485149396f5146397a314a42766a70326e754a394b495545377431705a544b626f7930716c385a5530434f55564d462f676b395062307365315538756d49496c475a5a54785a4758687a34556c72365477424c46415736714f5641345354634369786c454568726b342b47314c736f526e3052556935656232442b2f66654b5444536a4656787836634b366e544a706351724143553936547a78483067746e57506a5053646f732b397772492b662f444a6532396847436c4c655752467a44527049756a43717375506351374e6c463462372b6562697935436332585a7a4c4c47506d2f7352745a3578656733552f443861657054312f7867523653614130785a394171565339743753783966494761486c532b4a385332304841494147452b4b4a61796949774c6a556a42623377652b3544786f746b624f6837716b4d55576c7855794f77794b6c2b53744555553373702f687839374b747a3249556b506e504857484658384f63584c5531374f3245797448794e4c45616351564c516c646f472f6e7859467545677563586174765466544f4b367a72356130646f2f646646446f7961382f576a3061776a2b4b65454837376a647a76376d586a68416f54626b6d542f5152776136307358566b535473317a4a52474556654c72335a71354d4d614c446d7473516150537463336131536678613866755750556a756a54796a3433436b7574506d5a53354e516e73506c4645795a4d7470373778752f65452f6e4e5a325744416b33776235493753447033316f74722b68776248696f4a337a354f6b2b385a47316332666d644b326a757144592b6d496131704f493030693476575833766f35734e3339594d543073734339706934377875423745354d6f477345536c4f757366796d33586b6658385264757a2f7148432f70496245456473454e6f45344f58576b62397834716b6f344b62695055586569616f415469746f526650326e38634f2f6f756d44313544754657776475577150433652714b506b48304b306c65796667385362656245777a335365344f52767a414a584f3273657a484f546e6d744972344951774c6f3663567343447a304d6e6844724e73663656647a2f767a6e4168634a34366c36384c704d416f3952523348435335704f3076435464686941696177416f485346482b476d697775374f6a7a414445765a483849554a694c30756566696a52352f5a6c5a763265797951515843464c69725a634635336542416d356c4b424d2f4361575153744664464a5271584869777a7550546e51747a773232467a58564b6e4442746a73545a584a78396949335276476c633658654e724931724e4a414e436e63576c487871547543324e456c4f6c33524142482f3279626f2f4f6c2f694e49697a324641534c72526446444a4d635770466e59524a6774382b376a3457457a434247516d5571506754354a6c6546507870365263425374727a55734c4e5a35774c5172396d32372f5a59456c64535a484156592b4d4d4b512b4c46464933556b4b5434524163724a38374676695146635945776f64516261637471526e4f425a5059693777786163344561357a7550504d4b316a6d302f635346792b436e2b6356556957756d313155527a5632736b325259516a333047744e4753425634516e387848327071307277504774414955513262327a36486a50506853312f6c767545394b4f6b5546337042493537652f7659744c63385a4864764174306a554b4c696e7866554b5846383356766c636b5a4d53726c556d47745270614b63325178374a436a4d6b34376449594f466d5853496266743235367630756b6a6c6c2f35764e306b4539585a4238456c2f526967382b58684a4c2f6d7971445a4b4162786c4a506672782b6564774e424642426371546f43534c5073635235486c586d4f7a51317a4a724d4939696c7654736c786d37612b557a7847732f5035516e6e466a4b316d3454796945396c524a72504d6b34585359594f5678376f596c7a38396a4d34486943437a37514b356a517051574a786f6649663977715a62444f756265357a5a7a48334663456e375435386b4f5a473559364d374b436d424e6d6a62426e6d7a6f6b3657396154775548694e7a54624a436f776a6d46756d6d787a4e4c6638513776534343546b63566f6a394570564838303675716349322f4f7856336b586b792b497962533736423450667a76476577336e4f53385a5352414f465a6d48474b535045774e697934477731564f48486875306e6d6f744c6b72704a496e63704a784571784936776c477a644f45426335645370743368365043525244594a344863704f44547236583935483069535937646c2b31454342664f506d6b6b576c565032735a674275746e5144754a36644b7573344b50524734542f56514e674b346f33792f396c464a75534742376b6f75516f616867364a5a2b347a6867674545785a7a666f2f68584b53694a4b54557266374c705745614f44537475616d505365346234415252414e6f5635544d437366624f5762444a49464e444641504e5a357a6e5035776a714a7073504c6a4e766e656643696a394c594458506768316a6a63656c323879377843324e4947546955646a5134764a724d514554714946417159702f537650494d533242574a6275457344615367594e684144454d376f37465939385267496f32346449326d5a4768593654674e644739656130515a797871346b6651336c466b5343344d516b5a625135617475454b7279646567754246664a72356e6f79544a7036426e49435135516942467a3778793867584a47485a545a4c654d772b4c2b655a724d6b382f564b4839694b52335644444765667274306d665a48424b7a6769746c65753479666a493149666a48347a615443786d41694b65377161547253714b614d315a3567742f5a6a504c444a6f304e596c55364138483157504c336b3352536c7742377243625164514a5666596d723570434f697674574a625271547157336c3174624b5a54546c717448365a7a36506a367941523034786a39396c6e6c6a36655945454b5544717943575150783855514478622b6633574b6b4a314f554842587176714d43617438385955456a62454f37394f3464435254353563736b7a6e33484348484878616172533670705a6353663477476c5a495735696c795562496373534d526d666a4c5372537a59336d4d7452396a6c5277394b656843426f4e6b744a306e6547307a6332664533714152674575443875474d794b654b496d554269424a722f7738307964514a2b6a4a5645684e46574f6e4f643666375a394169686757485377466a3141306f666248354a485541694237634956424176774a41573479714769364b425134323641705246584344594c5634742f6e784b7561466731536663344c66556c7a3033386b314f6341426c31734a4c69306b623879675a684c535537547471306a4a735076385064435a634d667649306d6c584f6636573238677738384a676e45784362426c49746b6e595231354a6c35475068746b4d6d6d70394634616c6c32687669746151796661656b72655030624a4c69337a51625841445a43503675365937646e776d59774838544b4658787a3931445368326a3736664a42446769527346434b4b72794c734d7967525549345072786b4d693463354f4b53424650774b617a4c65482b35396c46454474566a33386836584e783673446d6f68524a3856536b756552374f306d326a564d55334a4b71454459354748656f31304b416370397a366c6642613559324c6f3137626c78326e4f5471382f69497335716c7655552f772b5a787a7a686c384c6f75537448586d55424e4245705671686b58566a5438644c48344e31333173536263673267574379432b754b7768466967433779776d554255424c4f706b62634771755672574b416f47566e506366456174316d534a51536e43416f3831486e63574d7037675938394741372f6d5259512b55574252376e464a775657476a4752644354516c6730344b464359464a4547565a4e2f696d6375704b79637956516e7557572b494c464445587152673471726148336f377242572b3871513966637355474b54514a48614f676d65633269776a764b66706c77336365355a70794e656167416b3051364255785a2f5a347962435378344c78527562776546656c695441526f3058415334545646336d5a5741786757554a554a3257584e3855426b764b50685a394b73712b4c39784d754f3873387847414b526c343668514b624c32307a673763397555454f46466977375a49436d7779384f4279522f586a447a69467475386f452b6733675a495666344a2f714d36494e574748666939444c32614865395a466f5a6a7849766c4b4c32626c5362524641437338315659664e354c7842697337745150493830364745737473424d6a61517456695844413469617444794c742b614669546b3076514b7951397659374f334f612f4358444b2b69314a623566304b484d7841524d77675a55496c4b7a34703871674b4a4e6b777243555334434d4a65654569775670344c3559376c4139736f494a6b4f36566244626a4d7374516b4f6b316b6b6a4a61566d5a774630694e534f626f793171444b416d31536a5a6d6b62397550456e70303444516e5965306b566136694e4166415a756179572f7a2b75627656733241524f596930444a44347264776f4c4268456f6535317a41652f6a68464d684c4d434e75474b3630334d4e46726d6c4b473458664e3472714f4d46582f3258684b754a4b7a31636b644f2b7738704c356a4b72456e4a437355394d366b5459566e3341793778436f5049756b7a51415a6c446935746452443447424a7a773933574e78694c535a6741696177496f475346576f4338564a5162386e6a485049747868457a75666e4a485931567a36585668337733724478334d6f31516952734c4d586e744a776d352b46466b687577586a7438395758524944356f4c6d326f55622f36664f4a6f71686377366277766c6e75443865644a366a68744866677267353365564b2f5766746b692f536d7a4c53696c6a362b6e5a725a714143585357514f6b5035505479494d6a5846722b79626a503868436b4b5179774742596b6358466e572b72513947674b3937792b4a344d3637727a4159546f7077345746443445777630756153506c50443471484d30793578455a2b4e496d6a3753364c534b314c31753444364247524f51724434592f6d33564575414b74647341746b6b6672546170743261435a684158776c552f6243766d6c4e532f4645674b414a694b59504151794d6279486c54724c646c6a4e616a614949414a33515573654c65494530676d2f56525356564669642b684f72666c76776d6b346f58704e316a352f79714a4c45594931565a4a783469316c304a6a634d62464c6c5575357639493530686c597a4b3972435262525a5658506c5031753441546846744835315733376674476571476b6779525266473537417a4542457a434257516d552f6b424f696a38466231416d4c4f30546f4f54365075455373465037772f4549576952414c6e3279754a41744a686543445846464f5474796531504c77536432737938554162463347506b344a3274666a343356374332742f456b32444c2b4e6a374265704565745374697355436d5a445174573664525056653050755a316434375356326854555972435967416d59774d7745536c663838544d6c4c7a464b777a674c347377543951637249584234704666455a61444b346a3656444d364e31457141644b32346446486f36526b544c4d516f706d77455070525a71477364564938624a3430704c4738365a6f36634170446c434a2f38347956396230454f4b4f5a73306e675030412b7549315549707a37766a595a7735567131696b626478755545386c4d616c4836556634734a6d49414a7a457967644d5766463971364d5a7653787a6f7a3949352b384857533769727075354a32377567635a6830326674616b6b4b58614b454b6c3061454a5369485a646e413932584843354c486d6b71663953456c6b6c3746555434434e4674383371672b504276756d336a43512f437732416d7a4b7a35686a47466a697366796a5546626866705679797163686b4a5670767a6e473434394f4a73417a36634c344e526d7832476862544d4145544741754171557230362b507972314d717653787a67572b59782b6d46447675484668796e395378735938624c6e6e47627861466f6643564a6d2f384c5665347831427743566164395237454c655069384c766d3566794a794869454f7755624a337a6453784a4f3156416b4e67676c66357356426f634c42355a6f387251376f4c765a5653516e2f3136534e7334435a36654e674e4d424e674a554f4d622f663352546b42522f696d7978695674472b463668354b38526a667775306f7775303661762f5263423176374d674d48336c494a6446684d7741524f596d38437369737a634456643041525637337831746c54375769715a6358444d6e68394b5078663935785931752f67456c2f396a3572367a2f436c7774794c7279786e446c594d5042786d4730514e496949324854675455575a5a335571342b4f7747797376644f452f4f307671436e627a4c532b2f66764a424843706f64344a6969435a63355a4e38636d6d6a75773733494d336a6d37354e2f636774514a5349635872533370514241395454344441627549443245546e7767617844382b4d74753942444250457979444531664239744a6941435a6a415167524b56365935326b352b70365750646145464b50776972495334474f444b6b644c2b4654376b71634d72576647664f7667704830695a583744676b786f545a59792f59336d645263476e656134354b77706e66624469674d396c352b667256796141697872334e345763326e356537707456372f57364c553641352b386e592f4f2f5a7268304c643661727a51424578673867625a6644744d5734482b7a334e366c6a335861584c72326536782b363066467a714f374e766770343533583151636c656c6c7261707349436543386568544577336366662f7a7a34303963503468682b4561624133546674524441622f2b314e566230485230304a314f63444f4179524e43785a546b43504b64777165503577326e646f6b486379343343563575414366534b514265553665546d344177477a6478367134524c4230722f6f79536431457933786654437648466c3445583768766a37704972454e386b2b797a5845516477705575775263486e6463482b41615650664e587936635157676f412f78424a795955572b422f37655941415334747a6b6457437555797530696b4a746141506a38633639654a324a677068476a6b6a43665079634377596b6a734378505941394a314c74676b303439424537744c435a67416961774e49476d6c4a466c42706f55662f4a616e37744d513735324b674743386e69426f3669537763635770716e494b76384166744c6336316a35755066356b324267567a3674484c55627a416877776b65414e32346c424a4c6d6775555a5033376943424263546f6a392b594476793172756f6265473059564d54536a3954746c5a4332593361674c444a4e414678522b66593861357436525844334f5a47706b31466b4253416634684c4e565646764e705a414c757841524d59474543424f6d53665171704973505077674d5a2b4958555038446f6769766536415a733447673866524d7767536f4964454878353667544e776f4344636b6b59616d657750306a5665656634766a66565661725a2b775754614230416751464538535069396f3479332f70342b2f362b4b685176366d6b557957523063356941695a674170555436494c696635796b585352395068364b6c554d5965494f38344d6b63515644656c6d4878487a675354393845426b306735666248722f776f5359634e6d6b62396b3739645a4e4b694f6a5a7852552b6f763076335941496d4d465143585644384b5168444f736c4c7776492f314c577159393445674a4958476c392b66456b744a6d41434a67414246483563666842622f2b75374a7a4332594f6c48374d3561483265336241496d454153366f506a6652644958493973454157615735516d51302f30586b6b6a742b45524a464f65796d49414a6d45424f67494a6478305a613378744b347651566479414b66566d574a3342495675427364306e76574c354a743241434a6d41434b78506f67754c50444d6875776739352f53334c4555674659576a6c595a496f306d557841524d7767556b453768484741644b413476365443766f3541634469393077366253584e3775306c66576678706e796c435a69414363784f6f43754b2f32565251496b69524a6246435643526c654178684a5238503132384b563970416959774d414c62683957666d696f4979525a49756d435a6e63417451386d2f636d7969434b53326d49414a6d45426a424c716b2b4a4e62666d4e4a354a75327a4565416f444779492b48613835556f4d6a5666432f3630435a69414366794c514d6f7a7a393876446d50435259597a6c6344624a4f485367357767365a4654722f4148544d4145544b4269416c31522f4b6d436568564a6a35624577394d794f3447556e353872447044306b746b763953644e7741524d59437742546c2b2f484657412b514450614249464541687375534b42423067364a66757672535239334a424d7741524d6f413043585648384b53724638664a426b6c37554271674f396b6b41372f475374705830397a67742b575948352b45686d34414a6c4574674d306d666a534b4c6a4a4a61494f53674a7866393047556453642f5059744e4f6b3753644a4d64474450334f3850784e6f455543585648385531377056325170356c7245566e7a5847307236656f7a794c5a4965572f7949505541544d4945754533696d70434e474a6e4366724270776c2b633237396978376d506c542f4c6e4d4d4238617436472f486b544d414554714a70413178542f6b317a52634f6f746b4f6f652f464d53316a68536f56704d7741524d6f416b436559724b314e2f356b68366547534f61474565546658416176572f6b3461664b664a4c665337707231456c70636a7a757977524d7741516d4575694b346e2b707047744b656f2b6b4862326559776c734545667531776c4f424936524b73356941695a67416b305432485643586e704f49766555644537544136713476357445634f367a4a65575a656168783845704a7a3557457064396941695a67416b5552364972696a30386b597a315a306f4f4c496c6a47594a3452782b7777676f3954374a57784c68364643517964414736485a4c43684273436f6b4247496d43305535644b466a476a336b2f51635352535676464932594e4a4e767a364b6e546d4f717653563950684d594f414575714c3434375a43386135447330714841312b3679366650793467734775546d4a33427347304d7841524d776755494a50437469744734385958776f3041514655794734424f50463153533953644a744a464735654b3173334c2b4d6f6d61344e6c6c4d7741524d6f444d4575714c345537555834666a30734d37517258656754346758442b343876464266586d393362743045544d41454b694e41316a4779744f306d61644a4741495050755a4a4f6a336f424744656f63467458567077375251775a68636e5747366b5554372b6b4c333263704c39554270567854674141424f5a4a524546555273454e6d59414a6d454444424c716d2b44762f38622b735469646d566e3643356f6942734a6941435a684156776c77656b6d717931306b6253514a483372697573624a4a5a49344863437435674f537a7054306777556d7a724d5531783369787469496a4d71504a623157306e73646f4c73415856396941695a514a4945754b5036554e696350505849375365635653624b5a5164314e306c47524b654b426b6a3761544c66757851524d774152614963416d3449365348684962676876462b324453706f44544149714a59614848447838585554374c6e357767724431697963386e786539786e547836704f42574b784e3370795a6741695a51423445754b503445557157556c4753736f5a6a58454956382f4c44674a555a684749734a6d49414a444a454153767a366b75347261667377434c4568574f52393969314a42317252482b4a7435446d627744414a4c504b67624a72553879536c414b6f756a4c634f506d644c347356325271544371364d5074326b434a6d41436653427762556c62786f6267316c48316e633343425a4c654d504254347a3673722b646741696177424945754b4e4c3462323452632b7a43654a64596a762b36394636535068362b7248744c4f71764b78743257435a6941435a6941435a6941435a6a4163416830515a456d62646f4e42716a343439727a6d4167716f2f7a373934647a5733716d4a6d41434a6d41434a6d41434a6d41435652506f67754a504a5551795069426447473856612f5474384745394a764a653135572b726f7178756730544d4145544d4145544d4145544d49454f454f6943497031792b41394238563958306c636a674a63734671537173356941435a6941435a6941435a6941435a6a41306753732b432b4e734c49476e685a46754c44753338502b2f4a567864554d6d5941496d5941496d5941496d59414964635a305a677357667245566b4c794c727844306c2f64523370776d5967416d5967416d5967416d5967416c555361414c466e2b4b7170434b44656e43654f645a482b5a4631694b7939317a6f2f507a7a6f504e6e54634145544d4145544d4145544d4145356948514255583648456c336a6b6c6453394b6c38307977384d2f2b4d43704a766b33536f7773667134646e41695a6741695a6741695a674169625159514a64555079336b2f546859487971705074336d4863614f735734634f656870507962584a5372427976714b5a6941435a6941435a6941435a6841345153366f5069447345392b2f68516a773730486562616b7777752f527a7738457a4142457a4142457a4142457a43424868446f69754a507864704e672f65356b753751556661766b6654454750736d6b72375130586c3432435a6741695a6741695a6741695a674168306a304258466631564a66387a5937686b754d6c334254616165543856672f79594a56352f66645758774871634a6d49414a6d49414a6d49414a6d4544334358524638596630677957395030502b5a456c59304575576d306a36764b536278534170794556684c6f734a6d49414a6d49414a6d49414a6d49414a4e45716753346f2f59413653394d4b4d30484d6c4864596f73646b3632314453415a4a326a6f2b547665644f6b6e347a322b582b6c416d5967416d5967416d5967416d5967416c5553364272696a2b7a50315153436e2b5356306c3653725659466d707441306b506c6253377046744743796a3662414a636b477368704c374942457a4142457a4142457a41424579674b674a645650795a2b396153507049563950714c7050556c585651566d426e626f51445857794c77654c337347744b506b70662f317a4f3234342b5a67416d5967416d5967416d5967416d59514b304575717234412b57476b7234703651595a49574941734c725849536a35564e6a644e537a3631346e735171516176537732414b546d2f4755646e62744e457a4142457a4142457a4142457a414245316947514a63562f7a5476725353644e674c685a65466a2f34396c34456a61584e4b576f66435466764d6155564d41355a366758537a366a3175794431397541695a6741695a6741695a6741695a6741725554364950696e79413956644c5259346964494f6e414b57354171306a614a616f437279627039704a577a397236733654765366706175504455766a44757741524d7741524d7741524d7741524d774153714a4e416e78543978496444336d436d51634d395a6165362f6b6e534f70474d6c6e537270543156436431736d5941496d5941496d5941496d5941496d304453422f773973494364666b612b6d654141414141424a52553545726b4a6767673d3d);
INSERT INTO `commande` (`rowid`, `rowid_client`, `rowid_periode`, `nom_client`, `sous_total`, `total`, `date_commande`, `email`, `adresse`, `no_app`, `ville`, `province`, `pays`, `code_postal`, `telephone`, `poste`, `notify`, `facture`, `identifiant`, `paye`, `prise`, `signature`) VALUES
(19, 6, 4, 'a a', '131.67', '146.99', '2019-02-23 15:24:48', 'eyJpdiI6IllSOVJzZkVQbGZCYjVzSUwzTTBBU2c9PSIsInZhbHVlIjoiYkVicDhxXC9Lc0tsOThKRFNwejI1ZGYyRFkxTGY5b3NcL1kwUGErWEt0UXpFPSIsIm1hYyI6ImUzMzY4MDM2ODAzOTdiNmNjNjgyNTk0NGFiNGJhYzExMTdiN2FmMjkxYjhhZTAzYTU0NDY5MjkwMDFiYWJjOWYifQ==', 'eyJpdiI6IktkTU5raHRvZ0FUMWUzd3A1Q3N1YWc9PSIsInZhbHVlIjoiUHNvbjdvSTFxRHYySTZyU2FSWHNiZz09IiwibWFjIjoiNGU2YmY3NmZhOGQxZTM4OGFiYjAzNDFkMmE3YmE5OTBiYjQwM2VmMmZmOTk5ZmEyODFmZjRmYjJiZjUxNjk1NSJ9', 'eyJpdiI6ImVlMWpPTVNlTm16MVN1Z3l5MXJ5Nnc9PSIsInZhbHVlIjoiWXI3SWtIdWdVM1pxVDg3SndPdHcrZz09IiwibWFjIjoiZjYwZmZhNzVjMmM2OWU5MTdiMTMwYzQ4ODYzODA0YTRmM2JiYjcwMDkxZWRhNjNjMDRlZGZhYWVhMmZhNzM0OSJ9', 'eyJpdiI6IjJvVDN0bkpvMnh3T3dmUkhhbjVoWVE9PSIsInZhbHVlIjoiNVQ1SGhkSzNhNGNZZ0FYXC9qV2huZ2c9PSIsIm1hYyI6ImEzMTQ0M2ZmOWZiZjkwOThmY2M4OTg3NDYwNzIwYjQ4MzY0NjY0NDg5ODYwZjk2NTgwY2JkYjc0MDNjMTJhZjYifQ==', 'eyJpdiI6IlUxbjUxOHAxSzdFdzZVYXJzUUVzd3c9PSIsInZhbHVlIjoiUFwvRmlQRWxEZFF4UDFRQUlEXC9tVmtnPT0iLCJtYWMiOiJiYzg5Y2Q4ZTFkZDljNDE2MjQwNDNiNDBmM2IwNjBlMTY1ZWI4ZWMwZGM3ZTgyODQzNTMyNDFlNjk0NzhhN2NkIn0=', 'eyJpdiI6Im1lUXlsQSsrTlwvNGEwSGRMbTIwZWtBPT0iLCJ2YWx1ZSI6InRkNlU0RHBUaVFIcU9lRkVGMkVJa0E9PSIsIm1hYyI6IjEwYzMwMDk5OWQ3NjRiZTY5NjNlZGJjNzFkNWRkNmJiZGViZGY0YzZkMDVjMDg4NWM0YzNkODQzNGU0ZmY5MTEifQ==', 'eyJpdiI6IlwvdmlxNHFcLzJmZDQ5T0I4TURSc2o2dz09IiwidmFsdWUiOiJGRGpkcmE0Y3N5aWFyeEN4UFhZQnFRPT0iLCJtYWMiOiJlMjM1N2NkYTNhMWZiMzcwNmM5M2M4NDBkNDg3MDFjOTExMTY5MmIyZjMxYzk1M2NjN2JjMDliZGViZWZlNjIwIn0=', 'eyJpdiI6IjBJT2ViZ2RVcGtcL3FxTWMyMHV3b09BPT0iLCJ2YWx1ZSI6ImFzT3E5TlwvZTdubTVHNGEreTdkdDlKTkJhY3JVSWFBeE8xZjBPR2N3MkNzPSIsIm1hYyI6ImFjMDFhNzg2N2M3NTU2NWI0ZDJkNDhmOWFlM2MyZTRkNDczZmUwMDQ4MzE3ZjNlMDBhYjMxYzY2MjZiYWYxOTcifQ==', 'eyJpdiI6InpsMWFYcmc4Nmhpb1dmUlV1WnRqR0E9PSIsInZhbHVlIjoieXB2Z3lqUGVpandZNzZwMmMzT21JZz09IiwibWFjIjoiN2E0OTQ0OGNmZmM1OTEzY2QxNmNhNjAzMDIzZTZjMzE1YjdmNmY4YmEyMmRiMzBkZTU5NDI0Yzk2ZGNlNTc0NCJ9', 1, NULL, '', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `commande_article`
--

DROP TABLE IF EXISTS `commande_article`;
CREATE TABLE IF NOT EXISTS `commande_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `commande_rowid` int(11) NOT NULL,
  `article_rowid` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantite_commande` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`rowid`),
  KEY `fk_commande_article` (`commande_rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commande_article`
--

INSERT INTO `commande_article` (`rowid`, `commande_rowid`, `article_rowid`, `image`, `nom`, `format_quantite`, `format`, `provenance`, `allergene`, `perissable`, `bio`, `quebec`, `nom_marque`, `nom_categorie`, `nom_fournisseur`, `quantite_commande`, `prix`, `frais_fixe`, `frais_variable`, `prix_majore`, `economie`) VALUES
(1, 1, 14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 2, '13.94', '5.00', '1.00', NULL, NULL),
(2, 1, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 1, '4.95', '5.00', '0.00', NULL, NULL),
(3, 2, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 4, '19.92', '6.00', '1.00', NULL, NULL),
(4, 3, 5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 3, '14.88', '5.00', '0.00', NULL, NULL),
(5, 4, 10, 'public/products/images/I6XjF0Hwgg9sIX0tqhw7rrjiacSVk3oHcqy0DeXV.jpeg', 'Moutarde de Dijon', '1.00', 'kg', 'Québec', NULL, 1, 1, 1, 'Herta', 'Condiments et autres', 'Produits alimentaires Quebec', 9, '8.98', '5.00', '0.00', NULL, NULL),
(19, 17, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 1, '4.95', '5.00', '0.00', NULL, NULL),
(20, 18, 12, 'public/products/images/n5nqMXBrO1Lx3XhZ1l8ZCpQA5D7g08p9WHQq9tuB.jpeg', 'Farine d’épeautre', '200.00', 'g', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Farines', 'Farine Quebec', 13, '4.60', '5.00', '0.00', NULL, NULL),
(21, 19, 2, 'public/products/images/aKxliPxIIy0PREv07R4UHxQsM6vQ5wgNI8O4V26t.jpeg', 'Couscous de blé entier tamisé', '1.00', 'kg', 'Canada', NULL, 0, 1, 0, 'Gerble', 'Aliments céréaliers', 'Aliments en vrac Quebec', 5, '4.91', '5.00', '0.00', NULL, NULL),
(22, 19, 3, 'public/products/images/N0AvyopsFGqYKEbFV5t1Pso4YSV20M190CNfcO40.jpeg', 'Flocons d’avoine', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Aliments céréaliers', 'Noix Quebec', 8, '3.43', '5.00', '0.50', NULL, NULL),
(23, 19, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 4, '19.92', '6.00', '1.00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE IF NOT EXISTS `faq` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_theme` int(11) NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reponse` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `fk_theme` (`rowid_theme`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
CREATE TABLE IF NOT EXISTS `fournisseur` (
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_fournisseur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fournisseur`
--

INSERT INTO `fournisseur` (`nom_fournisseur`, `actif`) VALUES
('Aliments en vrac Quebec', 1),
('Chocolat Quebec', 1),
('Confitures et gelées Quebec', 1),
('Croustilles Quebec', 1),
('Détaillants de boissons Quebec', 1),
('Distributeurs et fabricants de mets chinois Quebec', 1),
('Eau embouteillée et en vrac Quebec', 1),
('Farine Quebec', 1),
('Fruits secs Quebec', 1),
('Glace Quebec', 1),
('Grossistes en aliments congelés Quebec', 1),
('Grossistes en café Quebec', 1),
('Grossistes et fabricants de produits naturels Quebec', 1),
('Huile d\'olive Quebec', 1),
('Huiles végétales Quebec', 1),
('Levure Quebec', 1),
('Maïs soufflé Quebec', 1),
('Malt et houblon Quebec', 1),
('Miel Quebec', 1),
('Noix Quebec', 1),
('Oeufs Quebec', 1),
('Produits alimentaires Quebec', 1),
('Produits biologiques Quebec', 1);

-- --------------------------------------------------------

--
-- Table structure for table `marque`
--

DROP TABLE IF EXISTS `marque`;
CREATE TABLE IF NOT EXISTS `marque` (
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_marque`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marque`
--

INSERT INTO `marque` (`nom_marque`, `actif`) VALUES
('Ayam', 1),
('Babybio', 1),
('Bledina', 1),
('Bret-s', 1),
('Carrefour', 1),
('Citadelle', 1),
('Danone', 1),
('Fleury-michon', 1),
('Gerble', 1),
('Herta', 1),
('Jardin-bio', 1),
('L-angelys', 1),
('Le-bon-paris', 1),
('Lea-nature', 1),
('Marque-repere', 1),
('Materne', 1),
('Monique-ranou', 1),
('Nestle', 1),
('Prana', 1),
('Propiedad-de', 1),
('Sacla', 1),
('Schar', 1),
('Sojasun', 1),
('U', 1),
('Yoplait', 1);

-- --------------------------------------------------------

--
-- Table structure for table `panier_article`
--

DROP TABLE IF EXISTS `panier_article`;
CREATE TABLE IF NOT EXISTS `panier_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_panier` int(11) NOT NULL,
  `rowid_article` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `panier_article_fk0` (`rowid_panier`),
  KEY `panier_article_fk1` (`rowid_article`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `panier_article`
--

INSERT INTO `panier_article` (`rowid`, `rowid_panier`, `rowid_article`, `quantite`) VALUES
(4, 4, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `panier_client`
--

DROP TABLE IF EXISTS `panier_client`;
CREATE TABLE IF NOT EXISTS `panier_client` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_client` int(11) NOT NULL,
  `actuel` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rowid`),
  KEY `panier_client_fk1` (`rowid_client`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `panier_client`
--

INSERT INTO `panier_client` (`rowid`, `rowid_client`, `actuel`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `periode_article`
--

DROP TABLE IF EXISTS `periode_article`;
CREATE TABLE IF NOT EXISTS `periode_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `article_rowid` int(11) NOT NULL,
  `periode_rowid` int(11) NOT NULL,
  `quantite_commande` int(11) NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `article_rowid` (`article_rowid`),
  KEY `periode_rowid` (`periode_rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_article`
--

INSERT INTO `periode_article` (`rowid`, `article_rowid`, `periode_rowid`, `quantite_commande`) VALUES
(1, 14, 3, 9),
(2, 1, 3, 19),
(3, 4, 3, 8),
(4, 5, 3, 5),
(5, 10, 3, 9),
(6, 7, 3, 24),
(7, 6, 3, 6),
(8, 15, 3, 3),
(9, 12, 3, 13),
(10, 6, 3, 5),
(11, 6, 4, 5),
(12, 2, 4, 5),
(13, 3, 4, 8),
(14, 4, 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `periode_commande`
--

DROP TABLE IF EXISTS `periode_commande`;
CREATE TABLE IF NOT EXISTS `periode_commande` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_commande`
--

INSERT INTO `periode_commande` (`rowid`, `date_debut`, `date_fin`) VALUES
(1, '2018-12-17 01:30:00', '2018-12-21 23:30:00'),
(2, '2018-12-10 00:00:00', '2018-12-14 00:00:00'),
(3, '2019-01-30 08:00:00', '2019-02-20 12:00:00'),
(4, '2019-02-22 00:00:00', '2019-03-23 20:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `periode_plage`
--

DROP TABLE IF EXISTS `periode_plage`;
CREATE TABLE IF NOT EXISTS `periode_plage` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_periode` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `emplacement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `periode_plage_fk0` (`rowid_periode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_plage`
--

INSERT INTO `periode_plage` (`rowid`, `rowid_periode`, `date_debut`, `date_fin`, `emplacement`) VALUES
(1, 1, '2018-12-24 08:00:00', '2018-12-24 23:00:00', 'Sciences'),
(2, 1, '2018-12-24 08:00:00', '2018-12-24 23:00:00', 'Hums');

-- --------------------------------------------------------

--
-- Table structure for table `personnaliser`
--

DROP TABLE IF EXISTS `personnaliser`;
CREATE TABLE IF NOT EXISTS `personnaliser` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banniere` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_desc` varchar(1028) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_entreprise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_copyright` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1_msg` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2_msg` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3_msg` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_theme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Nouveau Thème',
  `nom_article` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_article_plur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titre_onglet` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'RegroupÉcole',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_html` longtext COLLATE utf8mb4_unicode_ci,
  `termsofuse` longtext COLLATE utf8mb4_unicode_ci,
  `affichage_decimal` char(1) COLLATE utf8mb4_unicode_ci DEFAULT ',',
  `affichage_separator` char(1) COLLATE utf8mb4_unicode_ci DEFAULT '_',
  `actif` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personnaliser`
--

INSERT INTO `personnaliser` (`rowid`, `logo`, `banniere`, `footer_desc`, `footer_entreprise`, `footer_address`, `footer_city`, `footer_code_postal`, `footer_telephone`, `footer_copyright`, `car1`, `car2`, `car3`, `car1_msg`, `car2_msg`, `car3_msg`, `car1_link`, `car2_link`, `car3_link`, `nom_theme`, `nom_article`, `nom_article_plur`, `titre_onglet`, `icon`, `about_html`, `termsofuse`, `affichage_decimal`, `affichage_separator`, `actif`) VALUES
(1, 'public/themes/logos/oi1ctvvQkW7fhBQXCBoR4g1yBddTIPEkCqKNMmUW.png', 'public/themes/bannieres/iaBcDoXy7Tyz4LV9SMascATAAB1a0psO3sjyWndP.jpeg', 'Regroupement d\'achat offert par le département de Diététique du Cégep de Trois-Rivières.', 'Cégep de Trois-Rivières', '3500, rue de Courval', 'Trois-Rivières, QC', 'G9A 5E6', '8193761721', 'Charles-William Morency - Jessy Walker-Mailly - Zachary Veillette', 'public/themes/carrousel/J6orjMRDNBn0x1hFEjXYl8EXmUvlLewZtrtXDsJI.jpeg', NULL, NULL, 'Aller voir nos produit(s)!', NULL, NULL, 'boutique', '#', '#', 'Principal', 'produit', 'produit(s)', 'RegroupÉcole', 'public/themes/icons/VFL9fx4VgqBZNZrPuYcWbCMU4kaTguSdWFAGRyr9.ico', '<h1>Test</h1>\r\n\r\n<p class=\"MsoNormal\"><strong><span style=\"font-size:10.5pt;font-family:Arial, sans-serif;\">Lorem Ipsum</span></strong><span style=\"font-size:10.5pt;font-family:Arial, sans-serif;\"> is simply dummy text of the printing and typesetting\r\nindustry. Lorem Ipsum has been the industry\'s standard dummy text ever since\r\nthe 1500s, when an unknown printer took a galley of type and scrambled it to\r\nmake a type specimen book. It has survived not only five centuries, but also\r\nthe leap into electronic typesetting, remaining essentially unchanged. It was\r\npopularised in the 1960s with the release of Letraset sheets containing Lorem\r\nIpsum passages, and more recently with desktop publishing software like Aldus\r\nPageMaker including versions of Lorem Ipsum.</span></p>\r\n\r\n\r\n\r\n<h1>Test</h1>\r\n\r\n<p class=\"MsoNormal\"><strong><span style=\"font-size:10.5pt;font-family:Arial, sans-serif;\">Lorem Ipsum</span></strong><span style=\"font-size:10.5pt;font-family:Arial, sans-serif;\"> is simply dummy text of the printing and typesetting\r\nindustry. Lorem Ipsum has been the industry\'s standard dummy text ever since\r\nthe 1500s, when an unknown printer took a galley of type and scrambled it to\r\nmake a type specimen book. It has survived not only five centuries, but also\r\nthe leap into electronic typesetting, remaining essentially unchanged. It was\r\npopularised in the 1960s with the release of Letraset sheets containing Lorem\r\nIpsum passages, and more recently with desktop publishing software like Aldus\r\nPageMaker including versions of Lorem Ipsum</span></p>\r\n\r\n\r\n\r\n<p class=\"MsoNormal\"></p>\r\n\r\n<p> </p>', '<p>test</p>', ',', '_', 1);

-- --------------------------------------------------------

--
-- Table structure for table `recette`
--

DROP TABLE IF EXISTS `recette`;
CREATE TABLE IF NOT EXISTS `recette` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `article_rowid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `article_rowid` (`article_rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recette`
--

INSERT INTO `recette` (`rowid`, `article_rowid`, `name`, `link`) VALUES
(1, 19, 'test2', 'test2'),
(6, 22, 'testtrhfghfghfg', 'test'),
(17, 1, 'Amarante en taboulé', 'http://chefsimon.com/gourmets/cuisinealcaline/recettes/amarante-en-taboule'),
(18, 1, 'Marrant petit déjeuner d\'amarante!', 'http://brutalimentation.ca/2014/03/12/marrant-petit-dejeuner-damarante/');

-- --------------------------------------------------------

--
-- Table structure for table `unite_mesure`
--

DROP TABLE IF EXISTS `unite_mesure`;
CREATE TABLE IF NOT EXISTS `unite_mesure` (
  `nom_unite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_unite`),
  KEY `nom_unite` (`nom_unite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `unite_mesure`
--

INSERT INTO `unite_mesure` (`nom_unite`, `actif`) VALUES
('cuillère(s) à table', 1),
('cuillère(s) à thé', 1),
('g', 1),
('galon(s)', 1),
('Kg', 1),
('L', 1),
('lb', 1),
('mg', 1),
('ml', 1),
('oz', 1),
('pinte(s)', 1),
('tasse(s)', 1),
('test', 0),
('Unité(s)', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_administrateurs`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_administrateurs`;
CREATE TABLE IF NOT EXISTS `v_stat_administrateurs` (
`data` bigint(21)
,`label` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_categories`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_categories`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_categories` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_fournisseurs`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_fournisseurs`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_fournisseurs` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_marques`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_marques`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_marques` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_populaires`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_populaires`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_populaires` (
`label` varchar(255)
,`data` decimal(32,0)
,`dataset_date` datetime
,`dataset_month` int(2)
,`dataset_month_full` varchar(9)
,`dataset_year` int(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_nb_client_actif`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_nb_client_actif`;
CREATE TABLE IF NOT EXISTS `v_stat_nb_client_actif` (
`nb` bigint(21)
,`dataset_year` int(4)
,`dataset_month` int(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_tendance_commandes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_tendance_commandes`;
CREATE TABLE IF NOT EXISTS `v_stat_tendance_commandes` (
`data` bigint(21)
,`dataset_month` int(2)
,`dataset_month_full` varchar(9)
,`dataset_year` int(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_utilisateurs`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_utilisateurs`;
CREATE TABLE IF NOT EXISTS `v_stat_utilisateurs` (
`data` bigint(21)
,`label` varchar(7)
);

-- --------------------------------------------------------

--
-- Structure for view `v_stat_administrateurs`
--
DROP TABLE IF EXISTS `v_stat_administrateurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_administrateurs`  AS  select count(0) AS `data`,'Webmestre' AS `label` from `admin` where (`admin`.`master` = 1) union select count(0) AS `data`,'Enseignant' AS `label` from `admin` where (`admin`.`super` = 1) union select count(0) AS `data`,'Étudiant' AS `label` from `admin` where (`admin`.`super` = 0) ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_categories`
--
DROP TABLE IF EXISTS `v_stat_articles_categories`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_categories`  AS  select count(0) AS `data`,`article`.`nom_categorie` AS `label` from `article` group by `article`.`nom_categorie` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_fournisseurs`
--
DROP TABLE IF EXISTS `v_stat_articles_fournisseurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_fournisseurs`  AS  select count(0) AS `data`,`article`.`nom_fournisseur` AS `label` from `article` group by `article`.`nom_fournisseur` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_marques`
--
DROP TABLE IF EXISTS `v_stat_articles_marques`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_marques`  AS  select count(0) AS `data`,`article`.`nom_marque` AS `label` from `article` group by `article`.`nom_marque` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_populaires`
--
DROP TABLE IF EXISTS `v_stat_articles_populaires`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_populaires`  AS  select `commande_article`.`nom` AS `label`,sum(`commande_article`.`quantite_commande`) AS `data`,`commande`.`date_commande` AS `dataset_date`,month(`commande`.`date_commande`) AS `dataset_month`,monthname(`commande`.`date_commande`) AS `dataset_month_full`,year(`commande`.`date_commande`) AS `dataset_year` from (`commande_article` left join `commande` on((`commande`.`rowid` = `commande_article`.`commande_rowid`))) group by `commande_article`.`nom`,cast(`commande`.`date_commande` as date) order by month(`commande`.`date_commande`) desc,sum(`commande_article`.`quantite_commande`) desc ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_nb_client_actif`
--
DROP TABLE IF EXISTS `v_stat_nb_client_actif`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_nb_client_actif`  AS  select count(`client`.`rowid`) AS `nb`,year(`commande`.`date_commande`) AS `dataset_year`,month(`commande`.`date_commande`) AS `dataset_month` from (`client` join `commande`) where ((`client`.`rowid` = `commande`.`rowid_client`) and (`commande`.`paye` = 1)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_tendance_commandes`
--
DROP TABLE IF EXISTS `v_stat_tendance_commandes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_tendance_commandes`  AS  select count(`commande`.`rowid`) AS `data`,month(`commande`.`date_commande`) AS `dataset_month`,monthname(`commande`.`date_commande`) AS `dataset_month_full`,year(`commande`.`date_commande`) AS `dataset_year` from `commande` where (`commande`.`paye` = 1) group by year(`commande`.`date_commande`),month(`commande`.`date_commande`) order by year(`commande`.`date_commande`) desc,month(`commande`.`date_commande`) ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_utilisateurs`
--
DROP TABLE IF EXISTS `v_stat_utilisateurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_utilisateurs`  AS  select count(0) AS `data`,'Actif' AS `label` from `client` where (`client`.`actif` = 1) union select count(0) AS `data`,'Inactif' AS `label` from `client` where (`client`.`actif` = 0) ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_board`
--
ALTER TABLE `admin_board`
  ADD CONSTRAINT `fk_admin` FOREIGN KEY (`username_admin`) REFERENCES `admin` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_fk0` FOREIGN KEY (`nom_marque`) REFERENCES `marque` (`nom_marque`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk1` FOREIGN KEY (`nom_categorie`) REFERENCES `categorie` (`nom_categorie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk2` FOREIGN KEY (`nom_fournisseur`) REFERENCES `fournisseur` (`nom_fournisseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_unite` FOREIGN KEY (`format`) REFERENCES `unite_mesure` (`nom_unite`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_client_commande` FOREIGN KEY (`rowid_client`) REFERENCES `client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_commande` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande_article`
--
ALTER TABLE `commande_article`
  ADD CONSTRAINT `fk_commande_article` FOREIGN KEY (`commande_rowid`) REFERENCES `commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `faq`
--
ALTER TABLE `faq`
  ADD CONSTRAINT `fk_theme` FOREIGN KEY (`rowid_theme`) REFERENCES `personnaliser` (`rowid`);

--
-- Constraints for table `panier_article`
--
ALTER TABLE `panier_article`
  ADD CONSTRAINT `fk_article_panier` FOREIGN KEY (`rowid_article`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_panier_client` FOREIGN KEY (`rowid_panier`) REFERENCES `panier_client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `panier_client`
--
ALTER TABLE `panier_client`
  ADD CONSTRAINT `fk_client` FOREIGN KEY (`rowid_client`) REFERENCES `client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_article`
--
ALTER TABLE `periode_article`
  ADD CONSTRAINT `fk_article_periode` FOREIGN KEY (`article_rowid`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_article` FOREIGN KEY (`periode_rowid`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_plage`
--
ALTER TABLE `periode_plage`
  ADD CONSTRAINT `fk_periode` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
