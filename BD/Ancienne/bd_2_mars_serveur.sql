-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 02, 2019 at 08:02 AM
-- Server version: 5.7.25
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `concep5t_diet`
--
CREATE DATABASE IF NOT EXISTS `concep5t_diet` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `concep5t_diet`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `super` tinyint(1) NOT NULL,
  `master` tinyint(1) NOT NULL,
  `take_order` tinyint(1) NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_modif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `change_pass` tinyint(1) NOT NULL DEFAULT '0',
  `actif` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`super`, `master`, `take_order`, `username`, `password`, `last_modif`, `change_pass`, `actif`) VALUES
(0, 0, 0, 'admin', '$2y$10$qejIbdueBlAOk9Fxger.su33bTctWlR6CKcqZ1iOnz6/GCZvdFQfO', 'master', 0, 1),
(1, 1, 0, 'master', '$2y$10$al.OCHpRFjQKFrkosV4vXuYv3uQnwi.Tyl8pYoKFtdcBUlt.gPxW.', '', 0, 1),
(1, 0, 0, 'super', '$2y$10$GuKNlmDXvss6HWnDS0PCxut.TaD33w7Xzi8BO71KbCB9Q794HzI/m', 'master', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_board`
--

CREATE TABLE `admin_board` (
  `rowid` int(11) NOT NULL,
  `username_admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vue_size` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'medium',
  `vue_color` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_board`
--

INSERT INTO `admin_board` (`rowid`, `username_admin`, `vue`, `vue_size`, `vue_color`) VALUES
(102, 'super', 'active_period', 'small', 'success'),
(103, 'super', 'active_period', 'medium', 'primary'),
(104, 'super', 'ecoule_period', 'large', 'secondary'),
(105, 'super', 'test_view', 'small', 'primary'),
(106, 'super', 'test_view', 'medium', 'info'),
(107, 'super', 'test_view', 'large', 'warning'),
(247, 'master', 'active_period', 'small', 'success'),
(248, 'master', 'ecoule_period', 'small', 'secondary'),
(249, 'master', 'stats_client', 'small', 'danger'),
(250, 'master', 'tendance_article', 'medium', 'info'),
(251, 'master', 'top_article', 'medium', 'dark'),
(252, 'master', 'sales_progress', 'large', 'dark'),
(253, 'master', 'last_added', 'large', 'warning');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `rowid` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite_minimum` int(11) DEFAULT NULL,
  `quantite_maximum` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `frais_emballage` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL,
  `description` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarque` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valeur_nutritive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `piece_jointe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '0',
  `actif_vente` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`rowid`, `image`, `nom`, `format_quantite`, `format`, `provenance`, `allergene`, `perissable`, `bio`, `quebec`, `nom_marque`, `nom_categorie`, `nom_fournisseur`, `quantite_minimum`, `quantite_maximum`, `prix`, `frais_fixe`, `frais_variable`, `frais_emballage`, `prix_majore`, `economie`, `description`, `remarque`, `valeur_nutritive`, `piece_jointe`, `created_by`, `actif`, `actif_vente`) VALUES
(1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.95', '5.00', '0.00', '0.00', NULL, NULL, 'Excellente pseudo-céréale naturellement sans gluten!', NULL, 'public/products/nutritional/GSSLKaSc5PSg4gmaRIdSnK3FSTehvSWdeJs5MkIE.png', NULL, 'master', 1, 1),
(2, 'public/products/images/aKxliPxIIy0PREv07R4UHxQsM6vQ5wgNI8O4V26t.jpeg', 'Couscous de blé entier tamisé', '1.00', 'Cuillère(s) à table', 'Canada', NULL, 0, 1, 0, 'Gerble', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.91', '5.00', '0.00', '0.00', NULL, NULL, 'Pour faire d’excellents taboulés… Yé!', NULL, 'public/products/nutritional/MthROLiRbthvhEaQKDR4s1qPV5CDrQ7Itf2rALBG.png', NULL, 'master', 1, 1),
(3, 'public/products/images/N0AvyopsFGqYKEbFV5t1Pso4YSV20M190CNfcO40.jpeg', 'Flocons d’avoine', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Aliments céréaliers', 'Noix Quebec', 80, 100, '3.43', '5.00', '0.50', '0.00', NULL, NULL, 'Cette avoine est roulée et torréfiée à sec avant d’être chauffée à la vapeur à 200°F pour être roulée. C’est ce qui distingue ces flocons de ceux des autres rares entreprises qui en font au Québec, et c’est ce qui leur confère ce goût de gras naturel de l’avoine.C’est ce qui leur permet également de hausser sa durée de conservation à un an sans problème.', NULL, 'public/products/nutritional/8I7DFLv4nW6zmVwVkWAUkyr5gtejjhzzY6SgMYlH.png', NULL, 'master', 1, 1),
(4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 50, 50, '19.92', '6.00', '1.00', '0.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes, dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température. Pour les desserts cuits comme les gâteaux ou les brownies, nous recommandons plutôt le cacao en poudre; le goût du cacao cru serait trop prononcé pour de tels desserts.', NULL, 'public/products/nutritional/p7PKD2k1iGJ1CLL5igoP8Sv2GyxvFHIJTdZPFBKP.png', NULL, 'master', 1, 1),
(5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 40, 100, '14.88', '5.00', '0.00', '0.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao en poudre pour les desserts cuits comme les gâteaux ou les brownies, pour lesquels le goût du cacao cru serait trop prononcé. Dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température, nous recommandons plutôt le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes.', NULL, 'public/products/nutritional/SIljBYRTECmTu0cARK3YJQGdaiwOMKzmGB3HEH78.png', NULL, 'master', 1, 1),
(6, 'public/products/images/fxtpyETXUMXk2OswDJ3T1SparCXp9c4caKKNOa8J.jpeg', 'Capuchons de chocolat 70%', '500.00', 'g', 'Fèves du Pérou, chocolat produit en Europe', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 150, 200, '23.93', '7.00', '0.00', '0.00', NULL, NULL, 'Pépites de chocolat géantes, pour cuisiner ou manger telles quelles comme délice du moment. Environ 5 fois la grosseur des pépites de chocolat, elles font d’excellents desserts avec de gros morceaux de chocolat qui en mettent plein la dent!', NULL, 'public/products/nutritional/Xx8RvLJaxYXeNYale45l5G4WCbvOKrv54PdIUmiG.png', NULL, 'master', 1, 1),
(7, 'public/products/images/TmxisxJI9ocq2Ip8TQriNaer4WypYgn9Enuy9HBb.jpeg', 'Café La Montréalaise Santropol', '500.00', 'g', 'Amérique centrale et du sud', NULL, 0, 1, 1, 'Materne', 'Café', 'Grossistes en café Quebec', 65, 125, '14.37', '5.00', '2.00', '0.00', NULL, NULL, 'Café gourmet biologique, équitable, en grain et de torréfaction française. Un riche mélange corsé aux reflets de marron et d’ébène.', NULL, 'public/products/nutritional/stjSP5cjaLxUFLYOdMhmaB2M6t1cy6gsZGsdzdJh.png', NULL, 'master', 1, 1),
(8, 'public/products/images/yepAc3CrDZsNuFSxHG4h1hFbgYuqwsq7lI72nlSV.jpeg', 'Levure alimentaire en flocons', '500.00', 'g', 'États-Unis', NULL, 0, 0, 0, 'Ayam', 'Condiments et autres', 'Farine Quebec', 80, 120, '13.89', '5.00', '1.00', '0.00', NULL, NULL, 'Levure produite par fermentation naturelle, aucune source animale ni synthétique.', NULL, 'public/products/nutritional/oMVZlewVITN6Jx7GfMeiFzOUOr9hQe4YXg5vt0o4.png', NULL, 'master', 1, 1),
(9, 'public/products/images/7AzHZCs5UGaVzjEJETAw9z1XquORt6uMNrRyF99U.jpeg', 'Miso soya et riz sans gluten', '500.00', 'g', 'Québec', NULL, 1, 1, 1, 'Bledina', 'Condiments et autres', 'Confitures et gelées Quebec', 100, 100, '13.83', '5.00', '0.00', '0.00', NULL, NULL, 'Le miso biologique Massawipi non pasteurisé de soya et de riz est un aliment sans gluten très polyvalent; son goût est doux, rond en bouche et légèrement salé. Il est le résultat d’une fermentation naturelle (non forcée) d’au moins 2 ans. Pour sa fabrication, nous n’utilisons que des ingrédients certifiés biologiques garantis sans OGM.', NULL, 'public/products/nutritional/Ty12FzodDT1kbWEeKsEnqlXZwim1KggiKytU5P9l.png', NULL, 'master', 1, 1),
(10, 'public/products/images/I6XjF0Hwgg9sIX0tqhw7rrjiacSVk3oHcqy0DeXV.jpeg', 'Moutarde de Dijon', '1.00', 'kg', 'Québec', NULL, 1, 1, 1, 'Herta', 'Condiments et autres', 'Produits alimentaires Quebec', 20, 20, '8.98', '5.00', '0.00', '0.00', NULL, NULL, 'Moutarde crue, sans sucre ajouté, sans agents de conservation, sans gluten. Une moutarde de Dijon avec une belle texture crémeuse et une touche de piquant qui la rend irremplaçable! Pour les amateurs de moutarde forte!', NULL, NULL, NULL, 'master', 1, 1),
(11, 'public/products/images/uwhju93QU1tzQNJO5xXD9iu1JDSzw7w00tRcRFEe.jpeg', 'Farine blanche tout-usage non blanchie', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Sacla', 'Farines', 'Farine Quebec', 60, 70, '2.88', '5.00', '0.00', '0.00', NULL, NULL, 'La farine blanche tout usage non blanchie est tamisée pour en retirer tout le son et ne subit aucun traitement de blanchiment. Elle est produite à partir de blé dur de printemps biologique, et les grains sont moulus sur cylindres. Idéal pour la boulangerie et la pâtisserie.', NULL, 'public/products/nutritional/6cCibCA8qr7SRrQYZCsZimR5Ly3GKJUWB1d9bm9o.png', NULL, 'master', 1, 1),
(12, 'public/products/images/n5nqMXBrO1Lx3XhZ1l8ZCpQA5D7g08p9WHQq9tuB.jpeg', 'Farine d’épeautre', '200.00', 'g', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Farines', 'Farine Quebec', 25, 30, '4.60', '5.00', '0.00', '0.00', NULL, NULL, 'Depuis mai 2018, cette farine est tamisée de sorte à en extraire 10 % du son, ce qui lui procure un meilleur rendement en pâtisserie et en boulangerie qu’une farine intégrale.', NULL, NULL, NULL, 'master', 1, 1),
(13, 'public/products/images/1TpWugpuoiLsRiiFu3ToTpzfZcBhz8dRDzXDL5UL.jpeg', 'Farine de blé intégrale', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Fleury-michon', 'Farines', 'Farine Quebec', 80, 80, '2.88', '3.50', '0.00', '0.00', NULL, NULL, 'Cette farine de blé intégrale contient toutes la parties du blé. Le son et les rémoulures donnent plus de texture à vos pâtisseries et à vos pains.', NULL, 'public/products/nutritional/PSd1hLFB885sjyTw5jfEZAOjZTPm1zGah4LvzJ8x.jpeg', NULL, 'master', 1, 1),
(14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 20, 40, '13.94', '5.00', '1.00', '0.00', NULL, NULL, NULL, 'Il est possible de retrouver des noyaux dans le produit.', 'public/products/nutritional/0K5UCVCywUYqmbWHv79g8rJiwUQzyEKXh5BX6e09.png', NULL, 'master', 1, 1),
(15, 'public/products/images/17YmeaiQPt8j48kzvCWmxRqXAzBKtDD3r7r0TetF.jpeg', 'Canneberges séchées au jus de pommes', '1.00', 'kg', 'Québec', NULL, 1, 1, 0, 'Citadelle', 'Fruits séchés', 'Produits biologiques Quebec', 175, 200, '18.66', '5.00', '0.00', '0.00', NULL, NULL, 'Ces canneberges séchées sont préparées à partir de canneberges (Vaccinium macrocarpon) matures de première qualité et infusées dans une solution de jus de pomme concentré. Elles sont tendres et juteuses et présentent le goût acidulé et légèrement sucré caractéristique de la canneberge. En faisant sécher les fruits plus longtemps et à plus basse température, Citadelle parvient à préserver la saveur des canneberges ainsi qu’une quantité maximale de nutriments, pour le bonheur de nos palais et la santé de notre corps.', 'Le tout est 100% naturel, tant le procédé de conservation que les canneberges elles-mêmes auxquelles aucun agent stabilisant, ni colorant, ni glycérine n’est ajouté.', NULL, NULL, 'master', 1, 1),
(16, 'public/products/images/kPZuCChkJhrxRQ0YOhKAqSt6MaWVfGeLjc5eqwFD.jpeg', 'Dattes Deglet dénoyautées', '1.00', 'kg', 'Tunisie', NULL, 1, 1, 0, 'Prana', 'Fruits séchés', 'Produits biologiques Quebec', 80, 80, '12.77', '5.00', '0.00', '0.00', NULL, NULL, 'Youpi! Savoureuse et sans noyau, bien que, étant donné que le processus de dénoyautage est automatisé, il est possible de retrouver quelques noyaux parmi ces dattes.', NULL, 'public/products/nutritional/qQDAsd2DoU6n1C0ABNUWrrR14Qz0yj6xvi2IWj9W.png', NULL, 'master', 1, 1),
(24, 'public/products/images/fTxg5UHAz8BBGPsf5BpBRQGQTxPYOIXC4SudEjHj.jpeg', 'Ananas en gros morceaux', '1.00', 'lb', 'Argentine', NULL, 1, 0, 0, 'Dole', 'Fruits', 'Dole Canada', 10, 15, '3.23', '3.00', '0.00', '0.20', NULL, NULL, 'Ananas fraîchement coupés.', NULL, 'public/products/nutritional/IpcjqnKHeIaVqSkv4v3hDxKq9Pox9z7Na18ibPvz.png', NULL, 'admin', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`nom_categorie`, `actif`) VALUES
('Aliments céréaliers', 1),
('Cacao et ses acolytes', 1),
('Café', 1),
('Condiments et autres', 1),
('Farines', 1),
('Fruits', 1),
('Fruits séchés', 1),
('Graines à germer', 1),
('Huiles et vinaigres', 1),
('Légumes', 1),
('Légumineuses', 1),
('Livres', 1),
('Noix et graines', 1),
('Pâtes', 1),
('Produits québécois', 1),
('Sacs écolo', 1),
('Superaliments', 1);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `rowid` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`rowid`, `email`, `password`, `telephone`, `poste`, `nom`, `prenom`, `adresse`, `no_app`, `code_postal`, `ville`, `pays`, `province`, `actif`) VALUES
(7, 'zach@cegeptr.qc.ca', '$2y$10$d0EqxtI.yw4x07z1zfpxjuFYRDHtSGXHydrfiD3QA4MxM2xeJna7O', 'eyJpdiI6IlZ2Y2lGNXNKS2NnXC9UYmNLcHVlMTd3PT0iLCJ2YWx1ZSI6InRJc3N5Vk1PcDJ2RW9NMm9FbDNINjBuN2xiQmdjR0VIWHdPRnJPUit3aG89IiwibWFjIjoiNGZjZTI5NDUyNDhkNDBlYjM2YzI0OWEyZTYxYTg2M2E4ODY0NTIzYWVhOWI4YzY2ZWE4NDlmYWE4NDI3NjdlOCJ9', 'eyJpdiI6IkZ6c3E2RkZEcktrV0M4amlYRTJTS0E9PSIsInZhbHVlIjoid0JHT2JSQjRIbFl5M1A4YXlLWDNtQT09IiwibWFjIjoiYjM1ODkxN2ZmMzUxMjRiYzAxNDNkNDY4NDhjNWQ4YjgyYmY5MWE0NjY3N2MzNmIzNTEyN2FlOWMzZmJkNTRlNyJ9', 'eyJpdiI6ImNlMmRYOThPZVk1YXlPMlk2SDVidVE9PSIsInZhbHVlIjoiQzhFVEU0Z0JRSFViVW10UzVwMjRTZUVvdStWcTUzRmxlczBzdmNZMTV6MD0iLCJtYWMiOiI1NjVjNTFjY2IyMGM1YzY5ODEzZTdkZTVhZjc3ZWJmYTM2NWIzYjAxNTVhMTAyZDAyZmZiNmEzZDQzZDM5ZTRhIn0=', 'eyJpdiI6IlRydlpJbmpIYVVQZUVsTmJiYjZvdWc9PSIsInZhbHVlIjoibXNpQjRVSDhlZkVJcUpkd0V2NlBIQT09IiwibWFjIjoiNTc4MDE4YjYxMjk2MWM4NTg0MDI5NjIxZGI2MTJmOTM3MzNlZDk4OGU5Y2JiYWFmZjFlMGU0MTljMzQ4MDc4NCJ9', 'eyJpdiI6IlhGVjlcL042MmxzUmFJTllRZG5kMTFRPT0iLCJ2YWx1ZSI6IlFJelpsa3o0eTZYaGVlQ2h6bHZIa2wyTkFoT204cHZuSVpDY2ZqQkhWbEk9IiwibWFjIjoiNjJjYmUzMWM1ZDQ2YzNhMWQzMzNhZDk3YzZlMzFlNjQ2NGEwMTY5MzJlYTkxNzVjMDczYjMzODhlNzdmYzhlNiJ9', 'eyJpdiI6InB2UXNmbHI1QVUwTGt0a05oT0VHN0E9PSIsInZhbHVlIjoiRE5JbFwvNjlYZlJJOExNMlR4NVg3UGc9PSIsIm1hYyI6ImU3ZjZmNjdjMzZjYWFlMWE0ZDU4M2U2YTY3MzQwYWE0YmY3Y2U5NzU5MmU4NzNkNDMwMDAzNWE5NWRiY2I3YzkifQ==', 'eyJpdiI6IlVhTzRjcjZnbHFVTXc0ZmkyZjJ1Mnc9PSIsInZhbHVlIjoiWTZodWh4ZWExNHJaUm1zeUxPV2lnZz09IiwibWFjIjoiYWM5YTY1ZTY4MGQxYTg2ZWIyMDdjNDA4MTQ0NDZmOGFlZDAxNmEyZjgwOGJhYzZiYzIzYzAxOGZiNDk0ZDVhZCJ9', 'eyJpdiI6ImNNVE5TaVNJb2E4NUJKbk91ZVZwS2c9PSIsInZhbHVlIjoiMlFwU1NSc3BzNVI2MFwvNTM3eGducFNSR0tWeURsQzhWNjZ2c3c4OTllcDA9IiwibWFjIjoiZDgyZTIzY2JhNTMzYTEyNTJiMWQwZTNlZTI2ZTkwZDBlNmYxNTQ4ZDllMzhhZmEzZmVlMjg2MDI4YzQzOTI4MCJ9', 'eyJpdiI6IkNoRjBCQ05uTzRkR0VKV0k0UDI2VUE9PSIsInZhbHVlIjoiREQzaTREWUJ0R2RIKzNleHhpallrZz09IiwibWFjIjoiYTVkZmQ4OTA2MTZjZTcxMzc5YzllMmNlMjM5Y2VmMzlmZTIwMDhiNzhlNzI5Y2I1ZmY3MjhkYTdhNmJjZGMxMSJ9', 'eyJpdiI6IndNMFwvRXJHY1QzOVFWUzFiMkozRDZBPT0iLCJ2YWx1ZSI6ImhGNmJkM3FpVVwvN3BMTmhEbFRHOVRnPT0iLCJtYWMiOiI3NGUxZDg2OGMxN2I2MTAxMmNlZTJkMjRjOTg1ZDljNmRhYjFlMTkwNjg0MGNlODMyY2UwZGM4MzJlYjc5OGI1In0=', 1),
(8, 'jessy@cegeptr.qc.ca', '$2y$10$lkPIKheb7Sv.07CHAd4B0e6uJTIjvBmCVQ5TudANmAg8P92UbCVOS', 'eyJpdiI6IlFRN3VFbENXeFdQNm9RSzBCVjgyMnc9PSIsInZhbHVlIjoiMjY1WktjNFwvak5ZbkNtT2k5NmtuWFdKaloyMTBOaUZLdXo3YTJmWDlid1U9IiwibWFjIjoiMTE3ZmVmNmRlMGY5YmRmNGM0YWFjZWI0YWRiY2MzMzcwYTQwMTQ0NjRhOWY4NjFhNzQ5ZjhhMWIyNjAyODgwNCJ9', 'eyJpdiI6IkFwaEFRSFZXSkFvZVFVelRYTCthbHc9PSIsInZhbHVlIjoiRkI4N0pLOFoyVllQakxBVkFcL2JkV1E9PSIsIm1hYyI6IjBiMWRlZmY5MWJmM2MxNzRiMzMwYmE0ZGZiOWQ3ZGQzMGIyYjY4OTdhMjBhMjc1ZGQxOGE2NTAyMjZkYTE5MmQifQ==', 'eyJpdiI6InJNN3plTE1Ia0d1Z0ZCbkhvckhDWkE9PSIsInZhbHVlIjoiQzlPQ0NrS2c1STZ5VEF4XC9VWEFMYlE9PSIsIm1hYyI6IjE0NGRiYjhmMjY3OTRmNjQ1ZDAyNWM1Y2I2NWYzODkzYjg0MzAzMjgzNTYxYzE1MjgxMTdhMTEyZDI0ZTU4NGYifQ==', 'eyJpdiI6Imgrb3hSRXBWNCtIKzRvc2YzYXJKdnc9PSIsInZhbHVlIjoiWjk2SzdrM2Q4VExKTTJBck5RMEx5UT09IiwibWFjIjoiMzgyNDBkZTM5Y2YxNGE4ZWQ2ZjhmN2FmNjQ1ODJiNWY1YWQ0MjAyMTZmOTQ1MzFmNzdiODBlMjU5ZWM0YzZiOCJ9', 'eyJpdiI6Imp1YXl5QVNqXC9QYjVab2ZsV01hSUZRPT0iLCJ2YWx1ZSI6InhtZUVEV0RSNlwvZXdsT1puNlM5dUk5YThwWENzWmFtam1aXC9hMW9seVkrdG8rRTYzQlVHNXcxXC9EQnBreWZVUWkiLCJtYWMiOiI4ZjhmYTc4ZDNkOWNiODlhMzg1YWQxN2U0ZjBhOTJiZjc2NzVmYmE2MGJkNzg4NDRmMzNlODU3ZTBiZDA2NDk1In0=', 'eyJpdiI6IkNMTHpKbVFqeDFsZ1h1YjZMS002VUE9PSIsInZhbHVlIjoiQ0QxOW1kXC9QcnJ4V2RFVXZUMGhXYmc9PSIsIm1hYyI6IjE0OTI0YzQyM2JhYzQ1MGQ2ZWMyMmE1NjZkNTdjMjI4ZmM2YmIyNjFkNjg1MDg0M2QyOTBiMWMxMDNjYTQwNWYifQ==', 'eyJpdiI6IlhKSUMrSHF1N1YrZUJ6cXhzOUNwQWc9PSIsInZhbHVlIjoidEhhRmQ5cW1xTlpiQlg4bndPN2hwdz09IiwibWFjIjoiZWE1YWNmZjZmMzRmMzg0NTNlNjcxNzNiZWE4ODYwOTQ4NTcxNGU0NTJmOGU3MGNhODU4YTk1YjRjY2RhMmFmMiJ9', 'eyJpdiI6Ijd3WW1GUnowcE1FZURESWs5Skp6N2c9PSIsInZhbHVlIjoiSkQ2VWxOajkxa0FORU96NHN3eFBabzJpU2NDaGd1MVlHbmxRUTVHNVl0TT0iLCJtYWMiOiI4ZjJjODBiMDNjZjMxNTRmMThlODBjNTRjYTZjYzJiMDFlODQ1ZTk0NDk5YzJmNDU4ZjY2MGZjNWI3MjU1Y2RkIn0=', 'eyJpdiI6IkpnVTRqNXBOcGFXSUpPVGxpZWdka2c9PSIsInZhbHVlIjoiUFwvY05CWko4NHBNcE5ERmNqR09PNGc9PSIsIm1hYyI6IjA2MTllMzAxZGYyZDQ5NWZiNjFlYjI2MWJjY2RiYTVjOWI2M2I0NTAyZDU1ZGY4MjUzMThhNzdhNWJjYjAyYWMifQ==', 'eyJpdiI6ImZuZzhOaWxDNnNxblV5YTd1VXZmTXc9PSIsInZhbHVlIjoicURzNnU5VDlhZ3hGSEdjYjZoNkt5QT09IiwibWFjIjoiNzExM2JlNzNiMzIwNTY5NGZiNTM1NmNkMDk0NGY5Yjg2ZDU1MTM2NzAwNzE0ZDBkNjA0ZDEzOTcyNzhmZDI0NCJ9', 1),
(9, 'gerard@hotmail.com', '$2y$10$0POJSJlbXx3hBM6VNKASvOKZiPgaK4oJnUQdgoydwr5B4h.HCbpCG', 'eyJpdiI6InMwNDVcL1JXUGtHcVZaUFZhUnI2T3Z3PT0iLCJ2YWx1ZSI6ImpoRlJ4dnhMeHZWbmx2cis1NFpQaHNIblhkNk0xMVZSWFFUMFZyRlk1QVU9IiwibWFjIjoiZjg2NTllNjQxMDRiODdiZTU4ZGJlN2ZiODU3MTViNTBmMTU3Y2YxNWMzMTZjZGU0YTdkOWEyNjUwMWJkNzY1NyJ9', 'eyJpdiI6IkRCSEowXC9RNGpQZERWK2ZGRTBoUVd3PT0iLCJ2YWx1ZSI6IlwvNWRvbUtCQ2Nic3Q0TEJ2NEpTVUxBPT0iLCJtYWMiOiI3ZDUzNjhlODBmNjczZTM2OTMxZTNlYTBkMWVhNDc3NTMxODA3NTRhZWFkYzJjOTAxYmI0MGE2NGIzZWVmZDE3In0=', 'eyJpdiI6IlowMDdiQjN0a0pMUGRCUG0xelc5c2c9PSIsInZhbHVlIjoibzVuYWxjMW16SUFvNTNTWWNnbHZMQT09IiwibWFjIjoiNmUyNGY3YTYzZDFmNGRhYzIyYWQ0Y2ZlOTQxZjFiMThhNDllYmNlNTQ1NDU4ZWM3ZjE2MzZlNzZmNjI2M2QwNyJ9', 'eyJpdiI6IlZsNnZ6ZjFMaWxKR1RsQkxicFwvYUJnPT0iLCJ2YWx1ZSI6Ik82NTlHdjB5NHQ4SlpCUlBxU05GUWc9PSIsIm1hYyI6ImYwZjE4ZTg2ZWY3YjQxYzU0ZTgyMzM2NjU5MWY2NTI0NjBjMTZiMTcwMDYzZWNkOWI2MGNkYTc0NThlZjk3ZGEifQ==', 'eyJpdiI6ImRwaktZUWNyeXJFUHFmYlRTMHVkYWc9PSIsInZhbHVlIjoiMTJSdEZGM0RPOHBMdVd5Z1dnNXRqNWJtQmloTFh6SHZSWE5QeExxYll2QT0iLCJtYWMiOiI3NTU4MTAzMDAxOGFjY2M0ZDI1NjczNjgxMzA5MGI3NDcxNzdkOTVmYmI5M2Q1OWVjZTFkNjU3YWQ3MTQxMWYzIn0=', 'eyJpdiI6IlZQU2huVitQcTB1NGFoVmdrVnJBUkE9PSIsInZhbHVlIjoiMHR5bGxJSVwvUHFCbDhZcWpLbU5jTkE9PSIsIm1hYyI6ImQ0MGEyZTRhZWNhMzYwNzhhMmNkYTkxNGE2OGMxNjg2NTA4MDU0OGVmM2Y3MDg0YjQ1Zjc5YjhlYzEyNzc5NGUifQ==', 'eyJpdiI6IjNRazZEejNCVlpxUWpFNmJYOUxPZkE9PSIsInZhbHVlIjoiajJNZ0g2YWhuUElESU1vRE9MZ3V6Zz09IiwibWFjIjoiNWE2OTk4ZjcxNGY5ZmM0ODc3Yzk0OGY2MDIyNzZlZWYwOTVhMzgxMDc2MjQyM2Q1MGIxODMyNDRiNzY0MDM4NCJ9', 'eyJpdiI6ImJ6REZQSjB1MlwvQ0VuQ05OQlZhREVnPT0iLCJ2YWx1ZSI6InZrQnV5Q3Jkd0lOd0xRY1RacmxCd0x4NjlielwvanVZR2V0Q2lZa2x1STJVPSIsIm1hYyI6IjVjNzBjNzhjNDc2MDk3MjdmNzhhNjE2MDBlNzk4MWNkZjQzMDQ5NWM0YTkyNTllZWRmMGNmZDk3YmE2ZjllOWUifQ==', 'eyJpdiI6ImZ5UVwvZ1dcLzR5YXIwbGllY3dBMElHQT09IiwidmFsdWUiOiJGMk1kckh6ZWg2Z3ViS1Vra2I5Z0ZnPT0iLCJtYWMiOiJhODRlYmQ3ODc2MzllMGEzN2U5NDY4OGVmYTc4ZTI4YzVjOWU0MDNlYjEyOGNiZjI4YzMzZTcxODViNmIyNjdhIn0=', 'eyJpdiI6ImJEaTh3YzJXeHFiVkRiY2NcL1FcL1N4QT09IiwidmFsdWUiOiJvMmtUSjJHRGg0NmRyUDZhRFNNdnR3PT0iLCJtYWMiOiJkMDExZjU4OTkwODQ0ZjQzZjIwNmJmNzY1MjZjZmUzYWQwMWFkM2QxZWFhM2RjZTZhNjg0M2Q2NmRkMGFhNTk0In0=', 1),
(10, 'tarantule@hotmail.com', '$2y$10$V1JC3U38QM4XHdTgBKgwPOT3YM69jQXki61Am7Ht2LO/NBlVq8EUK', 'eyJpdiI6IkZ3MWxKRG9ZdGd4Mno5OURZdEhBUUE9PSIsInZhbHVlIjoiNlBaeTVWMEg2c25cL3JhYnYxbCswbVMwaStqcEpNcnVqck9MTjcyRXNXcTQ9IiwibWFjIjoiNzQyMTA0Yjc0MTRhOTk4NTE0MTY2MTFmZjFkZmMzYzg1ZDA5YzJhOTNlNjFiNjA3YThjODFiMjQwYzVlNjc3YSJ9', 'eyJpdiI6ImwrT1k5VXRTZ0NhVzFrNVh3XC9CbkF3PT0iLCJ2YWx1ZSI6ImpPU1lxcTJxWmgrY1dwRTZqUGZJbGc9PSIsIm1hYyI6IjM2MWFmMTAxODM4YjA1NWZkYTk3Y2ZmNDJmNTA4NWNmN2Q5MDQxZDRkZmU5Yzg5OTBkYTQ5YWUwZjVmY2Q1NmEifQ==', 'eyJpdiI6IlZTMnR3WUdBOUZxeGgzQkNtN2E0NFE9PSIsInZhbHVlIjoiOW4wSm5cL0hSaWFtUFRtM3hDdHJ1N1E9PSIsIm1hYyI6IjIyMDNhNjAxMmM2YTk2YmI0YTkzMjgyZTE0NjM3ZTdkYTIwNGVjNDQxMWUwNTdlOGJjZmY1MGMwN2I5ZjFkNGIifQ==', 'eyJpdiI6IjA3NVwvNHNtcFN5aWFzNHBtbGlXdDZBPT0iLCJ2YWx1ZSI6Ilp1endkQkVjSUF5Q0J2dzc3cjNGcWc9PSIsIm1hYyI6ImM0ODU3NjMzYWQwNDYxYTRiZGZmMWU3ZmRkZGM0MzE5NmY0MDJkNzUzYWNjMmE4NTc5MGNiYzFhNDYwMTRiYzQifQ==', 'eyJpdiI6IldlY29jUSswMHBsYWZaQXN1Zm8xWFE9PSIsInZhbHVlIjoid1pGZndHaVV4bHNGWTFwbGZIQjRoR2wxam5nY3hISkNsRVA1VWI2MUprNjFHejRqMDJWNk8wVnNTbDlMR3ppdCIsIm1hYyI6ImUzOTk3MzY1NTdkNjJjNjM4NzhhMDM3ZmU1ZDA5YTUyM2I0MmZmZGQ2YWI3Mzg1NWIyZmUwZjZmZGE5OGQ3NTIifQ==', 'eyJpdiI6IkZPaEV2ODBlb1NpblhrbjZGamRxcGc9PSIsInZhbHVlIjoibEpQc1hDTXpFMEYweFVQSk9oNGFvUT09IiwibWFjIjoiOGYxNmMzZDhkZjg1N2E4MjJmMjgzZjlmMGFlZmFiNjJiNjhlODQ0NWRiNjQ1NGExNTNiNWJlMGVjYzA4MDY2OCJ9', 'eyJpdiI6IlJXUnlySUhSc04rT1ZrV2NEQ0JSOGc9PSIsInZhbHVlIjoiOGQ2VG16dTJBT2ludlwvR2xxSnlHTnc9PSIsIm1hYyI6IjExYTQ0ZGJjZTY3OTQ0NmFmYThlY2U5ZmMzOTNhM2RiNWUzOTYyZjFhNjA2NmYyNTk0ZjhiYTVjOWZmNWE2M2QifQ==', 'eyJpdiI6IkV2dCs2bUpabGZGOEU4WmNXNVozRFE9PSIsInZhbHVlIjoiVEVtMDJ0T0NKUldubytZaFBBcEJ2cytZTERsV0pIcGF6a3FDMU1lcnJQUT0iLCJtYWMiOiJlNWZhNTg2YWRjODVhM2VlM2Y4YzBlMjViOTlkYzA0MDVjOTQyYTY5ZWVkZTc1Mjc0NzNjNGYxMTVkYTFjNjgyIn0=', 'eyJpdiI6IkdDSVNyT0xuNHZsREE5a2cwM2o2dlE9PSIsInZhbHVlIjoienF5TTJaYnlIdXlIZXk1S09JUU1RUT09IiwibWFjIjoiYzQ3ZWEzMjkyNmMxYWZmYWUwODUzYTllMGFiNTdhYjExZDQ5NWQxYzEyYmU3NWE4ZDRkNzUwNjA5MDExZDNjYSJ9', 'eyJpdiI6IkZRN0ppdjZGXC9OS3pNejZcL3hzbkh5dz09IiwidmFsdWUiOiJPak5RRE9TZGIwcStrd0JyQ3VCejN3PT0iLCJtYWMiOiI1MTk1MDZhMjc0NTIwNzQzMTA4NzBkODkyZjBhM2IwNjlkYjM2NDY4MGYxNjE4YzQ1ZTkxMjYzYzYwNjBlN2ZjIn0=', 1),
(11, 'hugo@hotmail.com', '$2y$10$05VGySnT8QUM/nyCwRqe..sWgZM2gpQexxtyBKO1rKFOz5k8CdS2m', 'eyJpdiI6Ik5NcG5yVzRFQkNKa3FKYkY0eFZrY1E9PSIsInZhbHVlIjoiNUF3clViWTlORjFvdVBTdzlNcWtLSWltVXA1cGVMV3c1amRkU0EzT3E5Zz0iLCJtYWMiOiJiNzk1NWEzNjM2YmMzN2Y1NzYyZmIyNzQ2YmI1OWU4MzBjNzc3NTg3MTc2ZDY4ZDlhNzgzNmZmMjZkZGZkNDUxIn0=', 'eyJpdiI6InQwMmM3OEo5NnMzSDZDUXFwQW85d3c9PSIsInZhbHVlIjoieHgwRjNZU0hyOWpGYlV1VXloVzJydz09IiwibWFjIjoiZmU0YjI4MzE0MGQ5ZDdlNjIyY2M2YWZlZTU1MzBmMWFjZTczMDAzN2RiN2VlZmQ2ZWI0ZmEwMzBiZWQyN2IzZCJ9', 'eyJpdiI6IlVWcUliaWNTTmxGUWErRm1HSHkzcVE9PSIsInZhbHVlIjoia2hqR1B6d2w0Y3ZoWkcwZGxjd01QUT09IiwibWFjIjoiY2JkOTk1YTI0MmY3NTBlMTNiYzJjN2JiYTM5OTA4MjFmNWRiOTNlYzQ2ZmViYWUzNmNkMGJmYzhmM2IyNDA3YSJ9', 'eyJpdiI6ImVuTzljaEpjVVNNQ0oxaUxVRnZyOFE9PSIsInZhbHVlIjoiV1BFMjM2Sm56U2Q5MDliUG9vejNKQT09IiwibWFjIjoiZGY3N2E4MzAyMjM3ODhiYzc5NWNkYWUwNWI3MTczMDI0ODI3MjVjNGJjODM5MTlhY2FmODZlNzhlZTY4NDU3MCJ9', 'eyJpdiI6InpEenF4VDdYaHN5YlNOQktQdlI0V1E9PSIsInZhbHVlIjoiSXNZTVBNZmFKU0taVzFhSFJxY3g2NmNGUnp3SFFtd0lRb0RNc1dhZ3RLVDRSTjBDXC8rNkNLYTVVR0twMkdGMXYiLCJtYWMiOiJkOThlNzdkNDZkNWM0Zjc4MjRjMzFiMzdmNDNlN2VhZDI5OWYzMjkyMjgxYjEyM2YyOGJlMDMxZDU4OWZmMzQzIn0=', 'eyJpdiI6InNnV2V5Q25taFJjdmVtZG43RXhlSlE9PSIsInZhbHVlIjoiWHJyMmlRakZyUUpIMFlWRzhqUm8wUT09IiwibWFjIjoiYTg4MGVlOWU1ZGNkMzAzN2JlOWM0ZjFhOWM0ZDBlZWQzZDgyMmRjZjJkM2E2OWQxNjQyMDBmNjA0NDhjZTI2YSJ9', 'eyJpdiI6IkR4bnVJYjJGMWttTjFnelwvaW5zdzFBPT0iLCJ2YWx1ZSI6ImZ3V2t2aTJtT0dOb0F3Q1wvTEpjMjRRPT0iLCJtYWMiOiJmY2I1YjQzZTQxOTI2ZGFkMzIzNTJhNGY3ZTBhMGY1YTRlMDRmZTRiMzMxYzQyNmMwY2Y3NzgwYzg5NWRlMzFkIn0=', 'eyJpdiI6InVKWWxmdjQzeHlWQnhwQVhRY1c4TlE9PSIsInZhbHVlIjoiRzNuSm5oTzhwNEl0eTNXdlBVc1drdFZaZDhPcG9meTdpTU8zekJlMEpOYz0iLCJtYWMiOiIyMTc4Y2Y2ZmI2YzA0NWFiOTkzMWY4NTIyN2RkYzY5ZmRhNzk2YzAwMTEyMTU0N2UwMDM4ZWEwM2NiMTQyNzM5In0=', 'eyJpdiI6IjFEUXhETXVyRHB6TVhucUpXN1NFY3c9PSIsInZhbHVlIjoiWWY4aEZyaU9uYUErM0pFZ3Npa1FCZz09IiwibWFjIjoiZmJhNGNmYzc4ZDUyZmJiM2M2ZjMzMDJiMGViNmNiOThiYmVmMWE0MmNlZGViMWUyMmQ5Mjc0YjA2YjJiMzU2NSJ9', 'eyJpdiI6IkluR240Sm5cL0pCWW5QNEpyM1wvMXZXZz09IiwidmFsdWUiOiJsNlEzYVJXRGtXbWNsQWdSczM5RTZnPT0iLCJtYWMiOiI1NDkyOWE4YzhiMWZkYWZlNTY5Y2FiNDY5ZmNhMzYwMzc5ZmE5YWE1MjVhMjE3OTA2ODlmYTFmMjM4NjlmMjcxIn0=', 1);

-- --------------------------------------------------------

--
-- Table structure for table `commande`
--

CREATE TABLE `commande` (
  `rowid` int(11) NOT NULL,
  `rowid_client` int(11) NOT NULL,
  `rowid_periode` int(11) NOT NULL,
  `nom_client` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sous_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `date_commande` datetime NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify` tinyint(1) NOT NULL,
  `facture` blob,
  `identifiant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paye` tinyint(1) NOT NULL,
  `prise` tinyint(1) NOT NULL DEFAULT '0',
  `signature` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commande`
--

INSERT INTO `commande` (`rowid`, `rowid_client`, `rowid_periode`, `nom_client`, `sous_total`, `total`, `date_commande`, `email`, `adresse`, `no_app`, `ville`, `province`, `pays`, `code_postal`, `telephone`, `poste`, `notify`, `facture`, `identifiant`, `paye`, `prise`, `signature`) VALUES
(20, 7, 1, 'Zachary Veillette', '98.37', '106.66', '2019-02-12 20:11:05', 'eyJpdiI6ImdTWWVJQnVxRHBDVmVzWkpUSmFYZHc9PSIsInZhbHVlIjoiQXhIY0lCZXdDeWNJdjhWVmpSMHFMMFwvODVBMVlHSkFDSXljTjd5RitIcUk9IiwibWFjIjoiMDMwYmQxOGI4MDEwOTQzYzg3ZjYxZjI1NzQ3ZjM5NDljN2FmMTM5MGUwYjJlOTdhMWEzNTFlMWMyODM3MDNiZiJ9', 'eyJpdiI6InAyYWhJMGs2ZW4rb2owc1wvSDVMZU1nPT0iLCJ2YWx1ZSI6IlhqZWxndVJnc0hSR1B4eVwvR281eUJVUDk4NktSWnduajd4R0NSZ2dDRVFrPSIsIm1hYyI6ImM3OGRiZmU5ZmNiNjZiZDJmNWI3Mjc3YzFhNTNjMzIyZTExOGZmMjM0ZGJjMzkxYTJmOTRhM2ViNDNhNjIwMzgifQ==', 'eyJpdiI6IlpoTGdmQmJxdXZTTUg2TWp2dlBvRVE9PSIsInZhbHVlIjoiK3piREhNUUhmZDBRTlwvdkJuZFNBZVE9PSIsIm1hYyI6Ijk1NDY4MzRhOTA5MWM3YjViYTJhMmJkNTE4ZTc4NWM4OGViYmU1ZGRhYTAwZmM2MWU5YzIyMjUyMzhhOTA5ZTkifQ==', 'eyJpdiI6ImtsY3dzaGNoMkVBYno5VE91NzJpYVE9PSIsInZhbHVlIjoiM2R0NmpcL251S1JQV2hzVk5KdGhqMnFOUVZrVndET1hBdzZMd1o1NldOY2s9IiwibWFjIjoiMDg2ZjM5NDRiMDg1OWE0MzFkYjU1NTZkMjMzYjAyNmVjOGY0ODM4M2RjOGUzNWFkZTA1MzkxMDNmOGJiMzVkMyJ9', 'eyJpdiI6IkxmVXZQRjFnclh2aUREOVJrczVOZUE9PSIsInZhbHVlIjoiUmY1dXdyMEExbGVrSENYa3V4TytkUT09IiwibWFjIjoiMzFlMDVlZDMxMmViN2UyYTkyOGM0MjY5MjY0NmJjMTE1NWEzZjRjZTg0MmM3YmU3M2Y0MjBlZWUwNWViYmIyNyJ9', 'eyJpdiI6InBsTWVadkJxTkF3amdxcUg0SEZVd2c9PSIsInZhbHVlIjoiM0lDYmVsYjNKMTg4UExlZ29sSFFOUT09IiwibWFjIjoiOWQ2OTA1ZTYwYTkzYWVmNDcwNmU0YWZhMjAwYjc4YzM5MDJlOWQwNjVlZTM4NTM5Nzg4M2MzOTFkMTU2YzRkNyJ9', 'eyJpdiI6InZKbkFOZGwxTlRPRFJVV04wNEczamc9PSIsInZhbHVlIjoiNGQyVjZTTGhiaEpSdjZcLzRmN1pGOHc9PSIsIm1hYyI6IjIxMWYzNzJkMDBlYTY0YTE4OWVmZDM4M2NhODUzZTU0ZGIwNWNhZjU4MmY3YzVjN2Y5ZDQyMDY5NzkyYTIxNjAifQ==', 'eyJpdiI6InVBVlwvUk1sTGJESHZqZTlpdmtoRytBPT0iLCJ2YWx1ZSI6IjYyR0dcL1RaVTNjaEdmRFFwdjB1Skl6QWNVZTVadE1cL3FmdG9PdlNYOU91UT0iLCJtYWMiOiIxNGY0ZmQ5YWEzYTgzYjk5MjAxNzFhMTYxYzc4YjIzYWZiYWFhM2YzMzNmODFjMmMyNTM5MTNjZmJkOThkOTMyIn0=', 'eyJpdiI6InJTdzVCME5WZDFlUFBCbGp3RFF5ZEE9PSIsInZhbHVlIjoiN2d0NWRMcEFYUEZJeEJabHdKcCtIUT09IiwibWFjIjoiMTQ3MDJiMGY3ZjA3OWZjMGNkZGIzZDViMDYyNDY1YTJkN2ZiN2IxMDZkOTE4ZGFkMzQxNWZhZWVmZDc5NDY3YSJ9', 1, NULL, '', 1, 1, 0x646174613a696d6167652f706e673b6261736536342c6956424f5277304b47676f414141414e535568455567414141763441414144494341594141414351326555674141416741456c45515652345875326443527739627a6e6f4831755861366562684571326b715753736c53754a4b6c4c6846755836752b6978653266756b6b552f74706b546252636c576968473556576c5736524a5a4c304c39464b534d717555704557664c365a393370367a5a777a6332626d6e4a6b35332b667a2b58312b763938354d2b2f796665664d50504f387a2f4a656f55684141684b516741516b4941454a534541436d796677587075666f524f5567415457524f444c4975495745664854456648554e5133637355704141684b5167415357546b4446662b6b723550676b634434454c6f714965365470336a41696e6e452b3033656d457043414243516767586b4a71506a50793966574a534342666751754652462f32584b6f3936682b2f44784b41684b516741516b734a6541443957396944784141684934416f4758524d536e712f676667625264534541434570444132524a5138542f6270586669456c674d67537447784d7562305677634556644c492f4d65745a686c636941536b4941454a4c4232416a3555313736436a6c384336796677757848784763303072686b527a316678582f2b694f674d4a53454143456c67654152582f3561324a49354c414f524734616b52673555652b4c69497545524550562f452f703076417555704141684b51774c45497150676669375439534541434e5948336a596a585251534276532b4d694b74487842736934734f61412f3875496a355362424b516741516b4941454a54454e41785838616a72596941516b4d4a334366694c687263397156477a2f2f747a5657667a352b546b52636433697a6e6945424355684141684b515142734246582b7643776c4934425145506a63696e685552487867524630624567794c696573316e5a547a66484245505073586737464d434570434142435377525149712f6c74635665636b6757555477492f2f6e356f682f6e7045584b6635397a4d6a34767070364e654f694f63756579714f546749536b4941454a4c41654169722b36316b7252797142725243674775384e6d736c386545533873666e3376315154664f2b4971442f624367506e4951454a53454143456a67364152582f6f794f3351776d634e5946625238524447674c664768452f30767a3743684878366f714d3936657a766c53637641516b4941454a54453341422b76555247315041684c6f496b4375666e4c32492f386345652b54446e784d524e77732f662f2b455845485555704141684b516741516b4d42304246662f70574e715342435451545941673372656b727a386f497436612f6c2b373948784d524c78656f424b516741516b4941454a54456441785838366c72596b41516c304538694b2f54556934675870554e31387648496b4941454a5345414352794367346e38457948596867544d6e38496949754b426863464645334b76693866794934475767794a3947784f584f6e4a6e546c3441454a4341424355784f514d562f637151324b41454a4a414c586967685364685a70752b6655626a36586a346a5853464543457043414243516767576b4a71506850793950574a43434239315479436549743876347066332f35724834783448507653313546457043414243516767526b492b4943644161704e536b414337796277774969345863506952684878394259757462582f69524678452f6c4a514149536b4941454a4441394152582f365a6e616f67516b454548576e6a6333494d6a4f51356165577467422b4d667177375a6441586c4b514149536b4941454a4441424152582f435344616841516b384238492f467045584c763539434d69346730746a423456456264496e37386f4971346d53776c496f4a504166346d49473066456b795069722b556b41516c495943674246662b687844786541684c59522b44446b714a2f393469345238634a745a76502b306245752f5931377663534f474d434c3436497a32774b3456336c6a446b3464516c49344541434b76344867764d30435569676b3043757776756649754c744c5564654a694a656c7a352f53615051694655434575676d6b462b57665835377059776c7745766b6e534c69697950694b5248784c523333363748396550364343486a6a574e42694f42514a624944412b3655487879306a34716337356f53727770656e3779345245652f59775079646767546d4976412b4566484f314c6a5037376c4962373964377266506a496a2f326a4a567236754e7237384c76504546646e6f534f44494233486f6f306f5877457041566c547955624c6e45313738552b447279634f314f41717368774f375a3231543856374e655378336f7830554552524b37354873693470354c48627a6a476b394178583838513175516741542b6a51434b796139477844556a346a59523864414f4d4e654e6946394b333331497967416b53776c496f4a3141336b336a434a2f6658696c44435878345250786479306c55552f2f7539446d3753376b477939422b50483742424c78784c486878484a6f45566b626771794c693863325964393162666a38697274776368387650563678736e6735584171636751486173763030642b2f772b7853717374382f615659795a33436f6948745a4d4362664d6d7a662f2f74714949465a4c32534142627877625846536e4a4945544543416a542f485266306845334c5a6a444e787a396c587a5063487737564943697964414b732b2f557646662f446f74645943766949685053594f37656b53384d50302f76316865484247667464534a4f4b357842465438782f487a62416c49344e384959446b71726a326646684576375142446e763779734e6e316769425843556a6750516c634e694a656f2b4c765a5845416765733062706a6c5641777a3348397249627661707a6366766e644531436d58442b6a6155355a47514d562f615376696543537754674a393077772b4943497562487a364c392f686237704f416f356141764d5375474a4576467a46663137494732303933352f6646424855576d6d544c3471495a7a6466334367696e723552486d63394c52582f7331352b4a792b425351686b4b2f3444492b4c324861316d48395037527353644a2b6e6452695277486751496d76387446662f7a574f774a5a346d2f2f714e546577543476724848505a705972474c396e3341344e6e56714169722b703134422b3566412b676e38596b523853544f4e5331594269486c324250452b73666e6759794c6939657566756a4f51774e4549664746452f4c4b4b2f3946346236576a624f322f643557397032324f6a3475497232362b324a5753655374387a6d34654b76356e742b524f57414b54452b6a72356b4e5162376e6e654f2b5a66426c73634f4d454b486848467177692f6f593276754154544f397a492b4933557a7439306e522b61584c786f6341584b5a7156445248777872476878585171456a674267632b49694e39742b7355616959396f6d3244682f37506d69322b4c6942382b77566a7455674a724a6e436e694d42465473562f7a617434334c482f51305238514e506c45794b436c4d7637684f4d35447a4864386a35614b2f786578582b46692b61514a62416741732b4e694d3976786b4f71754664316a4f312b45584848356a734b6662313951584e774b424a59417747556670522f46663831724e6270782f694a456645486152693766507672305a4c442f32595251534477523066455035352b4f6f35674b6749712f6c4f527442304a6e436542506d342b32594c30687848785365654a796c6c4c594253426e347949623144784838535148556c32463239584b634744476c6e7077632b4a434678316b46644742466d682b67724748497736794155523861692b4a33726338676d6f2b43392f6a527968424a5a4b6f4337473158552f2b64626b32734d444a6675634c6e56756a6b7343537950776f6f6934696f702f37325568442f32374673594c6e337663626169535731776b6530396f7749454535655a64315538652b4f4c543939342b59456765756851434b76354c57596e35786b473152794c302f334e45344764397559693452464d393963386a676d702b35655a4939645550696f6a33622f35777a676333506f49664768456632427a37746f69676d417942516c5437497a555979747a5449754c5654647476614b5a4547323974436f463853504d5a4e32543634767a794e2f2f6d4439646b73534c2f7a70487976474d4a655766544c334e6b66703864455264464244664d496e3853456154557939557a35317535356264386d5968345854504d523062453133634d75632b7577504a6e3677676c63466f43667863527547735538666d39657a32346c37386c48584c6c69486a5a615a6677505170697a626c2b5078415264786c3572667852524878383038616837706b38387a38314971345845666671594d2b7a6c3131676e712f4b45516a4d656545645966683230595041697950694d33736374395a44554478354f536b76466365597836556a34692b50306448432b37684e5244793447534f577944594c4670396a71555165316c543458666930484a3445466b6d6772714c71383376334d7147306f72775777576639353036347374654b6946386671597a33475836756c384c7858784d526a2b397a596e584d4c5a4b4c447a5658636d42355076526a49774b587169396f4447503850565373346a365532496a6a7658474d674c6553552f4874793162726c517937397a42522f4e6d4e594b6469724c417a675a576f6a2f6a622b546672325a5561574630576f617973734559775669516767654545565079484d617544573747412f3943774a6959396d683378473659573533714733433069766a6631633669316e743334647a54742f48566a744b465949332f593855665a6e306f65314652306e366f3932396c42594b344c542b6a4c4959444c4371342b564f446a44322f6e5262426150374e5278746843526f48474459687933767a42696f3431486465664e69453934363830376a693039627a6b31764f336a6373504e773553672f315430796258585045664c502b6d6266726c426f5762445a2f7a2f7a39757a707562356b633157384c306a3274535354755a2b3730774968365150756979634d38393169573176382b46687a566b3359743476316e53366a6d5774524849767a64634b58477a564c6f4a59486e6d2b56546b2b79494370666855636f77584e3537586635386d2b464d52385930394a6f77695479726d37347949542b68782f4a5348344935312b5232464836667379375a534d523168534541432b776e6b472f645449754c472b302f5a374245352b4b7372662f2f33523853334e775375486845763343774e4a7961422b516e6b2b772f5673696d30704851547547564545487455424c6645627a34524d4f4c49586c3731505963683546595238644455443861374e37664d6d62362f4d694a2b2f7367386941584532506762546244786e78363566377454386663616b4d4167417238554564644e5a387878347834306f424d657a4f37525335722b62786f526a32305a7937346467524d4f3336346c73446f432b666445696b6f4b34536e64424844744963683143596f2f757733664d62506958326378756b396a77532f646b756944652f58394a396239534e484d44685442756452787751583030576d7547494475366f573648414c6e724c67735a78556379566f4948474f7264693073626873525039344d39694e6273693939576b543858764d39466967436752554a534f417741767a472f69616465767549654f4268545a334e57632b496942756b3263494c627365574f7273512f662b66707262416c47504a615a4e704631644c6842536946483662596f66362b5246424d556179415849396c7178755a5237302b667570566776757635383335535274617a774246662f78444731682b77514963694c7251433665673157443948446e4b766a4f6c75774e6266635272454446562f5451344c4a7a5a6575384a56415471463146694e3069336248535453416e482b416f72502b31316630592f436969525445745974324949554e777953484c325652532b2f5a6a635366754c6c64363775714c4e4c47342f46436b3635386a3476557074536233634f376c434d59656e6f4e64777479494b574258416146644d754356414f4770356d6f3749776d6f2b493845364f6d624a34416635424e615a6f6b56413276477551705a4869345a45552b506942753151436937492f6a3134392b765345414368784d6751444d72696d52574b576c7944323931323266574f3751596268352b67696d5477704e556e6c6e49686f625666436f686c6f4759686937684a5a4764422b496363466c3961632b4f7963525759675232785568634e534975546d307935792b7343716a31374e4c4435696167346a3833596474664b774779484b47306c67496d65523734534e356872524f62594e776f485356516c2b336c48366e614a447645733576504b42714842556d526741514f4a38427637482b6e30376b765766436f6d79653644646c746370706e38744c2f7a4f464c634e435a4e322b71394f49446e3475763459396676356763314547546553396e38696e74764b5a4a36306b672b4773506254794e45774d59565964722b59714965474c36454264516e6f39612b6b64416e2f4e55466638353664723247676d5175685272525a6556656737667a4c567879746c36766a77696e6c704e344c6562797366632b4975663664726d3648676c73435143464d664c65644d70306f526268744a4f414c39324d73686b7753704e5163746a43596f2b4f3649553043536a44776154496c506f58683851455939723258486c57726c4a566278737a4a7a4c4338706a49754a7271346275486848666b7a376a4a654f2f61656b666733762b633665342b4f59667054314934446745736b4c6231694e626c7a6b7639484647746278657371554b7638352f5445506b2f365649467a454176376138345473694361794f67496b466869305a575754496170506c436b31746d47457448583430536a6b3164486a5a344c3559436d6e7945764370687a6637376a50724b73436c4f667a71435153665573713168337351535273514444726335346c2f4b384a382f34644b2f35546f35326c4c785838657272613648674c636b50396652487a2b6a69465437687a2f79617a67726d6547303439305635724f7372564e72393566706d6476692b644a514d562f324c713375644855526f70684c5134374768664855676953514e3666534b656a48502f73734f622b2f3948733946446c7469744c477347304250564f4b5357525131483832576b6755446b4c465a464a6e36717367494150356855736b6b4f63685144574837492b6b48466d6c3544374f4b66526d3255774b3271554636546e7076485739784143316a366c53666e574a36504569716275554356774d6749712f73505174796e2b78395233536c597a3342317844383078595966755046792f4b583631693851636333785752467976366651794c54466239367a63665961746c4563666e63416346386e524a3247484575684a674541767167623279537663357276657335744e4830624a2b2b39745a6b68413259656d32624c392b302f4e2f377371526d34616a704f5477457745547148346f3641535549773164303056566f6e5461747564505a612b38316b7031537078425858327061486a654c2b6d364262704e4976676230382b66654b70697542575246704f6467564949556f71546e61726366385a493851706446574a7671424a417a716d666338394d6f4768462b435268326433457069455147326c3374556f4a64314a573661304533686a5576625a637234774859616244336d653263596d2f61416941516d4d4a2f42685457585530684b2b31546c627a6667652f6d4d4c424b546d514e695053363472632f51335a5a74314953766135723656732b704d32562f64566e6c4a7739702f756370436a68734f376a68394250393573756a55626b465933662b384a5373512b687a2b2f515433346d705535467369346746394f757734686a53796266667a7a346b49436e6f704b794f6734722b7942584f3476516d51737869586c4b7630504f4e72477574497a385050397242736562786a52507859496f464c46412b65723475492f337532684a7934424b596c6b4e506a306a4a575861793763307139773844766d642f314771544e7a61633255737731443149396c2b72417242464648752b524f714d757a4a4e36644535574f664c7473334e61684a654158464d6d7a2f4f504777742f5636616e62347549482b3752623973684a476934647658465230664558787a596e7165646d49434b2f346b58774f346e4a59432f2f6739474242614f506a496d794b70502b3173384a6a3973506a45695874314d4d67657a34664a6a447563747272357a4f675542464461733245585957614d79363578534b383950695967627a396e68684732334b66366b517632394366746f61347034734c394b583642663157505a6c372f2f49786f462f582b6d646834524562685959755576776c726b46346950616c374d36706f71655a7a45434f4376333164774d654a46672f53635763626533336c706f4b62437a534943563667756f556a6b627a535a3947424a5544483858744b53517272766e447a4f724274654178736863506e4778595162327a35683235534d43473046542f6164652b3766632f504e467157635378792f6678354f5a49416737616b6941516c4d517741663765796d4d7265313959595238625271364c7838594456657575416530325a304f4961526b78654c6b75365347444871775643344b3076584f44363955576a7a7363524c73577677437933513232492b2b685145322f666955627236704968345655752f314166497862723658672b384c48414e6c643251767566744f6f3441616f714839613143504557666d326a6a47442b4754594279456f736b634d586d4155555157706538727247452f4f6f695a37437551574656797475372b663552486a72574f6c6a586d6a726135524d59616a55654f364f53766a473363394f49654f7a59686f3977666c666d6d376c316e627a6a7954547072363632544b44306a375977514a464767633143596178373738694a6e36384a4d6762786776476f716731636a484a784c62372b373033527231314c6b53757a31386639723469674d6d3966345557446f4752637265615332675671726e3432302b37635034624e6748496969794c416469673375347336526b5841365a77336d6b58424f4f4a676349334b7676766c2f6b4547696563303478693744587a453664695642465a42344a675a66626f7934765252474a63416b783164586c4b7950447769766d486d776555316f6d7277623757342b587877524c796c47736672493449646e434c50614b7275766d3348654c3873496e43394b744c6d557352336646355831743058354578734157336e4d643036496837616445615137302f315a496c374659484758554c6d705663327a327263656768697037415a3661445a7565482f38487058524c7970635858693339654a694774576a563470496b676c726651676f4f4c6641354b484c495941312b736a472f2f41656c41555363487631534a62387931586365664a44787a2b6a62574a6d7a53374b7277454b424b51774851456a716e346b2f345243326f7458787352704a42637570514541336d634a4277596d394a7931377a4a635a393935336c4f45662f304239564a746236464a5478627a38734c777a374778424551543443515a70584d51665531516859665974317778337a6e6e6e4755727a382b4976366f4f705a7354696a56464c6c452b67516e6f375454393332727469686f6876456f3134485a4e39657537356b5873512f4575794276373147543539432b4e6e656569762f6d6c6e535445794a3148565963416e657839694e59414e6a4778507277356b334f656e6d542b733249344f4745504b2b70683041656636784943476c544f6559517757324c5147474467672b68357a6c624a5644483161446734543478682b44626a39734a7533626b68373947366753726236342b4f30662f593975454378626857756255632b6f717470654b434b7a58564c626c75794a74645747797374376c427454474a4a2b487759553531382f41584b57347a34736a6c6e7a5364685a423053663446714d4f757a302f313379424e62374e39372b63782f63453333494e46654736495436455a2f62557371754b2f4e5239626161394f5838516d34486b52453547674a73494c6a336b316b665939714d4b49756e73637547536b7733777a44724f4e316d326b5045682f5936492b4c3647777948336b787445424e76625261696d7a425a78327750387a48413758516d3832374b624d3858636134654c3478686374514b4c5153573778364155737175365a476d7a7375502b676356364c754535394e6c4e347969383144394139696e6275646768782f653964395978444a7848515571793332517037524555587539323148335636574a2f4a694c494b6c5232436e4a64684632374a37554c45756c494f58655875382f5964654535775176663730634541644a4b44774a394c37596554586d49424359687744584a31696b5a414641416933786e524e786e6b683573354641432b57474753772b755065557a716c4d5346445a4571415a4a5663673261664f4848644b32783070674377544936764b344e42454337504f4c7746527a4a41504e745a7247714e524c6b536c534c6859687377794b335a4c6c64794b4371726c5a35765439527548504271695335617857705048334c7a756c65577a3179304766784168313571436e4e7370317a7447505252366a47554c2b66664c77467946625530375065614d716178433750746b5177336d38424a62306f7353416c4f72737055305562324c71636d5668586c447632654a6d4e4f58316b32504c754462627368394e3264396d326c4c7833387853626d496962576e6b73444c78344a746a6d334154304934306964726c67427a506c3031352f48485649564372723754356b39626e557278474e36362b52443175697753777675624357584d387337382b496769414c594931476c392f33464f4b45495361713845756b5856624f737335654a5735352f3551396e2b352b6149654238556b636632706853784a4649374d5172706b46476d4d4973514e354a3350726c32434f71413546364d6b64585775524939524455733843767964497749464865466c6756536b6261365754303758517330543139752f72656241793966465237684164504d3545504b6350346f44682b52705a3067413638514c49674972627847436746443464666c597867584251346548463159742f696275416d735446694e6b794c336b597950697454326e4e6154646e6b31366d415257517743466a4a7a715261622b5064517065722b677352412f4f794a515a724e4d336665556938437a6f36374e517144714a307a5a53576f72463156375a6b5467736c686b6e3574504f5934586854343154396a31776232566c4a386c687a34754e4b5659563936746f57304b57524c73696841386e43337863474a38374935677243455a4273702f636464737735566655504931774974434c6f70475a6a654362586c4a6e4676794c6751427941514f4b7a304a4c506d4833484d4b48725a53416c7837334d687943584b6d5170415230667064706364584f743356443574614361564b4c35504a4b65547931764b2b695a495667705367574a794b454d50426d7238346256475837363662556f5875613976764a624131416c6d4a4a45764d4a3038387764772b6c6d4173776d304b592f6e4e54397a395a4d32317066476b756d314f657a6c565a3355735151366b78593047673069527a346d4935376430584f2b6744683162567537664768474d6f556a5736346737794c3776754364685a4376534a79382f2f764e58626c7838796e32375474584a533069754c6a3130506b4f4f7878574e3351726b42356f347379486e6e2f32784b76356e66776b634851415759355437683151396b3045693335434f506a413733456d6744697137544c4c7334412f386c7a33346b5a57707276355a70346337396e5a396a32463779416b4934475a43634f6e334e7a6e52547a43455258535a667739545a3959684e33734f3243554e59396c687661423547533851794e5a463171366c537474396f3876465a757763384a6e48647836704d356e3174665a2f635571524f575938374a786938533543717377794e6a3762566333334a794f43613271666b61334d463563666a4858314c6847704f306b664f72665538517248366e6675655232396652582f6f794d2f36773678336d4a5679704c39456338617a73496e6e314f364d565371536e35584d2b592b397848537764464745544a4e6f4e7a5632536a6133494463796c333478544868384e714b2f7653357669596377714b61796f72626c496f7332576659595374534238466d74784b4f4b586e684677556e44615a57634f664b3873497577704f61666f6d447148333036347136392b3841526f4273546e6c5a44694d394a356e72714a7477714e792b6365556834554a58495441733942686839696e396a4b484d6958733434387275742b7a573568694351386538367a774d4148446b2b692f4369785075614d6f42424d3735686e6f414c6b38356b454439454b475a4f3062456a7833596e7163646e3842646d6d3356756d64757674794564306e656d6933485862565350504c3539554f384b79764738536e59343177455544784a35646f6d352f79636d694f417353377131475935726632335331446f584f732f706c33383545746762576d6e31426b5a303235396268316357366532724632413867354b626f7334696c2f70636131664d694c75476846336d6e49537a55344f3176345839697834576135426a48625562436b46336e437649685a76617348345137746630684a6e516d3242556c4e67366e37507072317a767147657a534b66634b4a744e2b5276696768754f7371364347445649554e454c6273556549374e4b64664b756553574c6b572f326969515670434174537a6571395a31766651644c6463434c6d44634639726b6e46304144383331766f3839715a485a73554f36307644693034335676416a4b324f76324e587969372b7667566f5a426c664779497a6e56734868756c646f4762576b764d575478456f5630756146514c34487177746b6e663939394c7166634a484e6153645535316278794f2b7741594767686f4a6634414b5338564c4644644a5630384254335a494b763264456c4f4a71556f52524179384a7a41754d534c6e2b3871436754454a686934535959686b31736a41435a41776a5776456d61463161434f7042335939506539485449324941536a2f42674947674e32585550715831424f5a34485839663263775a59572f30705a4d514455396b47415a5261646f4b4b6f70526e526556544650342f32635a55443535465470564944524d553972464355476a2b2f655841314e77324b55524a4a56726b386848786d7247647a33522b6d783837376934506e4c412f4d757641413337455272515a516649347575357a5a4a2f70536f744b687033366859426467354a6938333474316e2f71413552713662775538454c432b6e4b39454b63786c7844546865734e3264336d45485146677154524935534a43616a3454777a307a4a766a7a5a31533336514a4b30497063767a7a2b7667536e6a6d2b52552b66584d306f496769575146774279444e4e30472b627345324e3677594b65354568785964495835646648456c6a6c2f2b2f6146674f62696542367a5446332b71447142524b317048736533374f4b4c5037553164326d4b46384870554b632b464b776632365456416363544d703076654666656834786837666c52316e537063514d696d564769556f345369374a56316d475838654235567173325738484a4e334250674d537a344674597238595a57366c632f786f63666e48326d726d6b7457473977776b63633050766a737a4a4238495574786c7951374734555734594f565066764e44316d4c76326a53627839366674305851636e6f43536a376651784451386271735255424658387669536b49554d794a68385139556d4d5568586e6b464933627869494974466e56756c4a7455734346504e465a53436d585851663254596f584279792f5262442b5576524c575465424c70637838714a2f7577614339316a63374f4b42736b6e61786a464364564d4355746c743464374d5062704c66724878735337664c3156585943655a4f62584a4647504f466e6636774e386579337374764a6752563441516f4a377a322f505a3930544533644e4a5a4d72424b4a6264563272465037394d744c305530427770586f6b74514f36774932344f6f383062426c7738764f6a78677341756233454c4b366454594b7a74656644527a5145592b5168532f74436d47426e58472b65674a3554644336376c503359586438434b54486a6f46442b4d4359646a5579736a77412b624942794b684254684a73454e7a734a624b31764d48634f6c677535664e6476636246586a3734753033542f61417466716c4864397966783259774575782f4d512b74572b4a337663596768674653795a4f657042635432684e433356662f795545504635706c425231323974794e69497053435456704763423736746e6235704b59654d5959356a64376e4f6a4e567643494c472b6f7a795836524c3653576d3447354e7a763769426c6e4f77624c2b6a4772793346504a2b55394e6b794b34743554415754346a41555a35796569716a4c34725857647039796561744a3248384b395476724b3767497565736d4943593338594b3536365178394a41495766725574634f56447938616e456d74666e526a53796130382f4d67454b733243525266416e4a6143584f677859724c4c677231316e61754b384f6e642f332b486e31486d6377304d595335537944674b38494f4c4b6741573054583630736142367a316f4961616f41414341415355524256476a6e51373254427a57426a646c61664d6a714579534a6f516270452f673670654b50736b79774d4b352f557871453670655a7a495835387630593462354634486d524d6f65324e6e6d706f722b364946613963386d354b503176626c345557497369396271554e656979396e4e656e39384f626a392f666941497250645a5436534146366c496c5255545550466638654b64594f676f58515154456468456973352f614a5336683531674c485a3550414c6b32762b38706a73732f325265754555562f4a644c714a65527362574d5832716668315058624b5a55514f5969686d555156795955484c62347933623258503074755630556e58733276736d3778766e307873643579584d353964684b6e4d7659516b57316d776d75462f757530536c2b642f77756d414d7638466d776744397a4172696c6f6d786255324e316d7a6f7841656b3379564c584a6a6b49477839365846694b344d7144793079524843526437326a6979332b37356b44635a6939712f74336d4f73525833472f327655694e4d627a515237344f534b4e4a6249437963674a6a667877726e3737444830414146773673764b5153493330615155576b2b53495467624a74416d324b4f38702f386348486a614d745257745848757368744f712b707978694e47516362636569324e7972436f4c6b754a6332442f73634145696848344931635531676533394c41577759424d6a7a2f683370426248774b6f48676d522b575836796e5577715a556f67427755704f4f746a693930776646493537334a536448616b74664d5a784738456c4a41654244756d6548566c65316f76304c5a673468654b2f3634582f305246783879455461546c32562f746a644a73366a53706438317676536c4342736c3479434f562b36364b485a416269526168495058353247464455367a6f4c5858504a726b42744b4b64347563356a3748493347726d4d6e6e3573416d4e2b484d6365712f3264686744707762685a7363574845494358747a39504d797037685141576e303974724449766e796b77736d32726d72374c76594f4841583358517141586d522f4743696b454c35736151626c62537072484f6b7448505664793033667468754571527747634e51754b44566d64554c617a2f476e7a456f4346734d30694f59573741432b5658786f5254326e3651466e614a6273557436577551564736787554517836556b70317a732b387a504368385a62584b6d746a363862746b6a75514d76696869514470453235547933303365656258335863514f344e4f4b573169556c34786b5a6b73695556495466415a6e75454f34564b4f705a617357667172686b35726c7653747435755969676e56707947732b326356475a4678664e4d594a4c3070745341324f596a686d483530354d77495763474f68476d754d68656566477437524d695a7330376877353038704770727536615a44726d5a7a65424a4e6c6d614e38657033576a2f3749534d454f5547325a4b6d4d704437417077504c414a5674466b53586c45782f6a77735238396855796d344c666c47327732346369677473584d54306c765374397341754961304b75534d71783955766170534f43484f43484374632b4b536c7a4547536674716259666572547a315448734c50316c7161785131396176716770666c54474e4d52696d362f74433574596779467a362f7662364171573364635831782f704839754551466f4d566f644944716775352b2f536b334a64424834584a624e504366616c44585a636342327170575a45696d514d4862797337657237706e754d426c4f6c4d733333666e616332486c534e6b424178583844697a6a68464c694a6f65446e514c4948524d523356322f2b45335a7055774d4a3147586836394e763237686b44577932382f433242336978566d4d4a4a4d64316c7539726553455a4d356261736c58373049357065387935634d375a7241357043356366436955745758444a77515742467a3165756842634867686d784b4a50675a30366457755a5438345a7a32646a4c4c773871326a7645506351664e794a4f316954344e644e506e6a6b304f663047486564664f375136736c316b54446d6745382f4c32766c5a61617342627435727a3167595849563366703064684734316f5a4b323731316e364568767969556c35696332704d7864475531713963486c7a6b732f73545049646d64737379465769625a5861696534355275626178567964506674664d776c4c48484c3444416f54655542517a64495578496742734f775551352b7772626c75523972677556544e69745451306b674e554961337574624e664e544a487a6d7a61377470504a30587a4e6a674339716538704b416135597568556759454430622f48345368437a782f5451447033616c3554444f754c6d35694e347161513279516745542f6b6654377a6e78515272306f6e736c4f49496e4f4955432f696c3371635348416b726a2f63752f70616e48733065354a44324f55713769574858435055566346695732526f3565764d62326a784c6e596963375961786c4232584772334f4f356c354b49664b6e39553166556769772f334a61544f724e4f6e376259713437654f43464a6837704c4d695856713277567457372b326c794e3273306959675677634564524479564a58553637484e535a745a3930577a3542394f7739397548724d41676b63636b4e5a344451633067674358786b523541387538756f6d642f71515968386a75766655415151494773576e663538516246617150653437747539444c522b58483144356378363832536430544e2f6c3344706c33374d6a417356306e2b434777734f727a54393233376c64332f4d67526745746152487a63547977735935684159523969596e5a31396353377345553173456e4730577854546e48586566686a5439323331522b74654a396949765042306645332b3842795070533379466e55746e4866413366763669702f74705642586258484c672b5559534c344b4a5a78324873593844396e39387a566c2f5759596a5561312b79457257356667313971574163626463462b655a52314a47684c784e7438514a393478724b584e6d4e7571424a64347a4270416a2f706d70754c5751372b3576715133356a33443851597175494853695364344336316d4a4b643759766a34676e4e7830394e694a774c314932516d414a4435324e6f467a644e5044586f7a7832466e783473355675645a50613849447243704a4d6c574a574b443049365456352b434254334b6a4a30764b7344703634547551717a527a576c584a75374a4c55466a534352584e426e6272396e467150373368346f676a6b346b5639783854396b57313648735a6b4c746f6c504d687a48796a54764b5468466f4e7979677452725242312b66373248642b5934314334634d764b38524e316579696678477538596d4248765067516231494579335664383246586b775277593948644a6139764d69654e695263594f4b326a486c367546567774555a79484349596344447046754262664f615342366c6f646f69653046664244736235327936344e68677a38326f64493233325138386c575648616853703738507533794f796857396e78386e7a6e6e6a456c6b374d4c5138494b71303635326542484f756657706c4d78754a6f4a78676674766b617445424c2f4658594a37305653376b5053543035434f4353377673775965633251436653377549772f4a376d596d514e71374a36614b714e7a30554643794f38584d51374435417769306c61624846594d4d4641692f355a4a75446755555258534d344a714231516c68432f6c577a623935754f446d6b32584b4c655a367a4c56314430572b6a43736669333874572f776f5372554d7a5554462f41686d363875516e5143323576634a44336f652b4556347546353933306b54665138666c486a63644e6f4344584d334649324332567350364a735830656455352f554a34435162436c62626e4d4770725876797a325052336e6f6134614c346b7a732b42307a7657354c6171673558584a2b47534730424836496e314157665743387334626c4362526e4c304263537873454c582f344e6c626277362b656152595a6b6a654a614c636154306c626638306e6c2b31334e53597974667248485261332b4c5a512b366d7870334c764b4c69335864396e7032686654566471624d7330784f674976336c77485a53647a7950586a7351736e4d4f5148766643704f4c7739424c6a4a456f6a4877782f684273324e43617578736e77435044674a71733253663739595a5571513343383038526d487a7571526a6574484f5238725737455938744b5972596c7369564f38616c39426f45504867754b434e617749515a72734f47524255572f6254692f487347566441756261786f476c4471732b507254315338327563544d572f7577726f6c506161504d376e2f73657649394e4752764b4a56625432763167364c72566379536c6156336a41666348726946324858456251316e744937676a2f5853664131642b54506176377175456c696e6a696f58436872444c56507a65687943355476566347484b4e317574507a5959323938524433464b493379447572453359376554365150714f3979347436555348574c667a584e6c7872612f4e5865506f536f4f633678753075514e3172654d68504c766134766c786f2b624c51336163686c7872486e7343416e312f494363596d6c314f5241416661667a7a384e38744a637978774253336b496d36735a6d5a436479764a5139302b6633696c2f3363694343674569472f655661576877774e317862614b6f4c664c466b6b73723970626f38742f487a386b4c3736484d764c44412f6a49726753454f434d516f53696967764a566663306841554c53324552334964343663554670552f4d424f6378662b624a6735424b786f6355344a70623855645277446633336846786d52357755637067794e3954795278427456506b4a4a39716673646f4a372f73446e6c473837756e61464f5274717777666362507a6b765a34534e4e4b793843665758662b704f474f416364393232586135707a757954372b5064683168596f69335764474c652b55755a4b77484c744d6f66727a79345875516448784731614f734a566b66674b7168302f616364413247586c336c76714b2f535a63353935355a316a6a702b71486b7566766a336d5341536d756c694f4e467937475543413758795546423773434248364b456d6b4332767a61527a5139466b64796732636d7a445a544c434b31745562655a6e4348784e58484a5252584b5949724d5079687673482f32387263445555596c76755a6e5a784741395a4d55687a695641704651763849634c346178634b4c456e4d6a64534e62544c6e5061544f5130372f75494c385a7655797347757578423767696f50504f646c34446847796c4b4238396731733765706a6173576661343936472b785738494475492f7a326357326f665a48376e4e766e6d47777437485038726d4f6d54673037646a7a484f702f374e726e7a6b53472f4c78524e34725351722b3952514b747250726e6f4630576e55477a3779713445424e7a4469442f4b776f345036614c72394a75506278526a3342625a72574f6e73557434706e4766346c6c58463946714f36664f464d59785133336b63334572574c4f626e6d58667572464c69417463466e365450474e7964723338506666366c7a51765971775278324949475871643746704c64415475645558327a6150766465467843794c676f69356f4d535961436b6f4f4e396569434e49735072766c51544a524e3574754268394c746d337262575838323346546f46444c495662754d5473746255466f5a4f396862624d4d645133493539614b4b664e486b614d434a4c3768745278715565787a38585146336655356434706a734d61784b3361495a622b722f7935724b45472b70442f45436c696e7a38554b69594b415173664f42525a425875776f306a4e45766e70502f753868626530367473346f633069375a424569397146726c2b6d514e746430446d357275476752464e37584453716e3732535862462b73784334652b546f744f3279376a756533696b7458563045747a6d566e72546141734d61346d714c3864386e564f754a6e634330734d514c63382f69646f767a5867624631753233754d34664551645175694c6d66506f58533975324d745048676854323735764b535134597a444348733145346865567938664258443452527432385a43434b6a344c3251684a686f476c7561636168442f597853326e4939336f713432317777504433794f73586967324d386c5933357a2b7834574f646833365068356f4e52622b6d57735a4d74416361786c7a467a326a592b69554855653633336e5450483930487a6e512f726374333544327470314c4c2f33483270655875764b7556503173617364726776695566425837684a65634b6977796d35614675355858526250593478394358325536365376346f56536a6157394344746c767a786949766b364a52616a792b57452b41462b70352f516f362f366438584c624e2f596d4c7035637544584c386a735a5045435172427458554f676e462f7661474a42702b4456726f4a5958564e6a4235527a6d514f7567316e363342654833417377764c447255764d716262424c752b766c716366797650735167714f2f507832386c474b4a666366766354304a394c6c41657a626c5953636d304659345a4b70435469656532697a647734763061577872376975493158634170426245447855666448772b7363686e2b62306d375758663975726a324f627463754e7073366a313751645849537a4c57584a2b376261484642565564796c32666676754f6f3673466e3379687a4f3251394943356e3552414c674768766a33486a4b2f75716a53495732306e554e32494e7247386e64494a70367078744733485a34372b4937585673713555734c32486464536a69752f7431315a596370596135397350752b5452576e5858505076505165626c6e4e597034656c7a4844483545595137632b30334b754b2b7970756c313346336c44777158786270452b42727136356c5178643745726c3746393961776a55476236362b6b4768373870675664594a4e39516853516e612b6d703745564d2f504f61566663532b584e676a776a35435638587130645956767439596b46424b75566d52727173756e333645495a3630433237517047444c4f63624844416966627836433144376752514a66564253774e6b7631474773385938526968754c666c73707531384e68312f7a716e50666c324c6f363774532b36583259593257737263483550494a313261464157572b72536448564279344350396a3441684f6b65366a5673633863366d4e7754534474344a4363396e55625039356b4d474b485a6f3070654c767973424f54684d7554387539704966734556684b676e674e4c44366c61577a4f6e6a6578437941344339346f374e61364f58577645387764724f3962704f5a34745a49664370616957764f4e42445968366c347472376d6562574b56793772354d58377575773637646969454b4f5064733468617576364d6a6c486e61374a4a79587a366b794676644a6b617058464f4258555069474a514e456c44783339616931726d426838364f725865754366776c79665442445a51415662494d3743756f4d3753765978342f314e4c4b4458585862344f4d436d794c666e626a41396e324d474a2b78414767524239696853556c487a6d384b64357a777a327744746e5a77582b6639486931464c2f2b386e6d62722f305564514c32725438764f6c6a792b54734c31794d4b66383055467a63733337586241646377696a59502b694862362f76474e2b5a37464a474c6d71444750753267354a4f474e32636d366e5065306f35707135444b474f656f2b4c79307566636454335a48676465754e4c6c744c2b3554504e4d506a612b683042524b4b454c674f622b39715a52483341317872576d546e50477362663634723551632f357a50432f506c2b793549793348454a72514678354e56375138487449734269747a393932336971504b702b344b71635863713855646b4e47724c454e52334b47305a342f6231333764746a31736767536c754567756331746b5069653147746d4a4a3933554d77654c397a496a4171733032363475624169543459614b5159534842496f37436739577a2f42757242386f596633433334412f6e384c446a442f386d667a796659356e464373486e6653796439494743586b7167392b484151347467316c31703437434d374d7161513359487248446c4164696e58343542796365666c70634948727873372f4a3348336c646b38717a5431456a67704e7843576d547a32783246664a3357474c2f6f6a71592b624f446441786848516c735a577563463943744362384e5869414a466d546e67743851757832344d78446b6563776469546e5a6f747933725a2f506f50656b6e697536376d4e547638687933655161473250577379696c6664726f63706b707536426b4b64715864596f5863344a5636325146394a38726c4c654e4a37764e314d7a493546565874423153326265747637753331424b6839676e5a665959494f2b2b38764e5643664161374c4c7545657a585057595364474a54335134527a6566476f686544775568666d6b4859395a38454539743159466a78306839615441425a54556c4a694c6547683043652f64382b6d54334959447a7463624c434d54534545524f507967594c4c56756551647248755944586e44395a39586d713459574b703430474876796e62745652567843725033796832513458646c353971586b724961662f736a676271596b6c597452674c66524930695557755458616c77477572474f7839592b674b6e7666786258376f75446c6b662b767a4a76547673386459677a736d4c306c74536d45356b6e7356526f6773597a4a367466486e666f4c7870453265315253763439375a5a79654e6e546a697161677a55557370794865506c752b34722b3479594f5859746f644578473154477a7a33636f304b597243344c2f635a373637726b5a324d4f7476536b4d4a66744a335467645a3939626d2f5975456e43786843356833636549644b57355874306b61664d517a747a2b4d5851734446586368436e476759785a714b55736f7541565945627661485648733831685477485558425a71767a464d4c444178635541752b514f726872716a4868646f555668317a4e3958592f466e66382f6163514b6a546d6f6a39316d33584a654b7a53543575695939733443774c5a4a61464d65457851356461686b51324a4b743237586f7a616d4d4a6c4463397a646e47356632664262512b6a5343335061537a667578523164674e4c6f5379797354327661615274682b6e51574b6836585056344d4d5351566e4f4973447677384a59543243456e7363452b49574d6639512b51512b374a474154726d4270656d73724c784271757058324d2f4c3644674976727062474c414e5a69556e72786f4748726b51497333447878766346436a7557647a43736f6f56684263434842676b37414a656478666246316a5973506265473667694a4a506e49437376366d695231416565596c6841634131693575534d586c70376a36384464626a37672b30456435304a55745a507243723551484341473851322f454e5163733743676f394873732b6250476c78762f3058333534316b487550644a706463312f7236704b322f666a497543536d317841636669597a2f72496f4343683855315a3477696b302b5871396d365a6a6650614c2b786358644271534d6c613575514c352f665a4a61317547625553764f75654b70394d51374d6e324433597555766d636a616c4672693336616f43394757762f38513975792b76696774344e41644d4a354e3748416751312b6b7162394350465375694937686a3642756650764c733357654b3978575430354178662f6b532b41415a69434169773162305963495736676f2f5558686e7a7566504139785374697a6b3343493743746c3339566d583876534957507948416c674c4d42796e643135734d34534436523045794437444161577275424b346b4871544339597565657350544c566576584e33662b45786e326c72704c654e67344d4a4758334633336d6c6930566936654d5353497231313353514d5a6b76376c566b78574f2b4143654d304f456e5672693052434b54544c76506e4b744a70567550726138464647776b485370694c706848356f725063624658656e434f657939424c4443344c614547784e574a6636505879775a61626a5235647a3942464a6937576772644e62326f4e33584f525571326248414a51697250486e7932623546366145613539544372677242746a6d66644673664b474b38324d7952616d2f714f646e6565676c67655555357a656b42325930724f3358726e646e38493865316a32713576444268426337534653444e626d6b664a586e2b30652f75675876777673514d4a446d4151562f5a3536382f64573249756a396946703761643741544870657668543675527579366f396a6a5370596c7039456c43507643356b743177776b5861326c4e7562684c577848487330514370476c7273315353733571627156624d4a613661597a6f466753744742432b2b52596748496246414b624230696a477471632b69574e62754932304647706b5841617a4678333370383778716b783636625a79346831367470333937506e2b58346f394c793139504341584443753670575536705132484177625556646e5542786a7a474c754e565865774d31374937712f6850654d5573744b6c545872514c52654b774a43414243556a6741414a31666e50534d4f49545453795173703841536c7a5a6a6175742b4f53684a78393946757154314a2f74372b5630523351706f4750535562616c485757586c55515655777656796e47724b564a6e555a753676333374355a6565746e67494d676678457436577959385975446f374850456c704146483141333330562f7839793775696866506f55744141684a5943494536765352754b6c6a366a786b63767841554277384439384e58746968652b4f2b544e6a4d4c4757394b5a7247444f7a7a796953696731427a4a73697556634e2f686f65546a6d3436662f474e6e644875716478654957646a6e61745233446f636352316167556a7667646b30784d486263594847334851325378704f58386c70796869523177304e575a43586e754c6772575369484b51454a53474368424f704d4a314d6f6377756436717a44777332695a5049707a2b593276336a5337354c43385a524b3579456732756f356e467035486a4b507a4a7569594a387a354f515a6a75556147427066514842762f524a5a687059724e7173627a72426753326e537856334b536a674f435568414175736a6350326d616e635a4f616b3653646d704443654136305770324d717a6d514a656451724b5179724544682f4a6647666b394a743930776e504e3570684c65397a72526e573276696a752b492b756c72756b386d747a4846736465507873374f463251696f2b4d2b47316f596c4941454a624a704137644e50787171506e39485659744d77492b4a4e54555658356b6c635246326b6b4254467647677070794641425854714b2f446e3050544c55342f38753571364e627661485a4a35714b52487859304d647a4a6c677752552f4465347145354a41684b51774d77454b4d4a4877626b73506b2f47516338572f376f6c4b6e6c2f525a4d6d654677766e723031416c38564566654e694d73314579504c4437566832423269344f5551755367693768455259774b75682f546e7353636734493336424e4474556749536b4d434b436554734d307944644c5a55346836715a4b7759775378447837304371333874564d7a6546617735793242733943774a464e6539507255427a684c514669617434722b465658514f457043414249354441506554743059452f73554978656c794d627a6a6a474b3776564251366655525164417256747676334f35556e646b43436644377874336e4455324d79514b48364a4447456c4478483076513879556741516d6344344648524d51467a5854786337364378626e4f5a2f476436566b516f45675a78636f4d384e336f63717634623352686e5a59454a4343424751675578662f69694c6947376a307a454c5a4a435a795741455868376841524e7a54413937514c4d5666764b76357a6b625664435568414174736a514956512f506c665a7144703968625847556b674971345845575351656b68453346596932794f6734722b394e585647457043414243516741516c49344241434a636a3848524842693736794d5149712f687462554b636a41516c49514149536b49414552684234593052513845736463515445705a37716f6935315a5279584243516741516c49514149534f443642583479494c346d497a3475493578322f653375636b34434b2f35783062567343457043414243516741516d736977432b2f5438654558654e43494a396c513052555048663047493646516c49514149536b4941454a444353774b5569676e5339723469494b34317379394d58526b44466632454c346e416b4941454a534541434570444169516e385339502f2b357132393851724d5848334b763454413755354355684141684b516741516b73484943723432496a3233716462786735584e782b496d4169722b586777516b4941454a53454143457042414a6e4448694c68665250786b52487954614c5a44514d562f4f3276705443516741516c49514149536b4d4155424c4432592f56483142576e494c71514e6c7a4d68537945773543414243516741516c495141494c496c44382f432f64425073756147674f355641434b7636486b764d384355684141684b516741516b734630437a3471493630584567794c697775314f3837786d70754a2f587576746243556741516c49514149536b45416641746e64782b772b66596974344267562f7855736b6b4f556741516b4941454a534541434a7942513348304939763278452f52766c784d5455504766474b6a4e5355414345704341424351676759305175456c452f48777a6c307445784473324d712b7a6e59614b2f396b7576524f586741516b4941454a5345414365776b55717a2f352f4b2b783932675057445142466639464c342b446b3441454a434142435568414169636c384445523857664e43463455455a38564565566c344b5144732f506842465438687a507a44416c49514149536b4941454a48424f424b346145526333452f3774694c6832524c7a396e4142735a6134712f6c745a5365636841516c49514149536b494145356950774b524878697451387762376670742f2f664d446e61466e466677367174696b424355684141684b5167415332522b444449344c382f726a37464c6c6252447735496c363276656c75623059712f7474625532636b41516c49514149536b4941453569527768596a3439596934544f71456c344869446a526e33375939676f434b2f7768346e696f424355684141684b516741544f6d4d4148524d514645584864694c684e524c7a686a466d7359756f712f7174594a67637041516c49514149536b4941454a434342635152552f4d66783832774a53454143457043414243516741516d73676f434b2f7971577955464b514149536b4941454a43414243556867484145562f33483850467343457043414243516741516c4951414b72494b44697634706c637041536b4941454a4341424355684141684959523044466678772f7a3561414243516741516c49514149536b4d4171434b6a3472324b5a484b51454a4341424355684141684b516741544745564478483866507379556741516c49514149536b4941454a4c414b4169722b7131676d42796b424355684141684b516741516b4949467842465438782f487a62416c49514149536b4941454a434142436179436749722f4b70624a5155704141684b516741516b4941454a534741634152582f63667738577749536b4941454a43414243556841417173676f4f4b2f696d56796b424b516741516b4941454a5345414345686848514d562f48442f506c6f41454a4341424355684141684b5177436f497150697659706b637041516b4941454a5345414345704341424d59525550456678382b7a4a5341424355684141684b516741516b73416f434b763672574359484b51454a5345414345704341424351676758454556507a4838664e734355684141684b516741516b4941454a72494b41697638716c736c42536b4143457043414243516741516c4959427742466639782f44786241684b516741516b4941454a534541437179436734722b4b5a584b51457043414243516741516c4951414953474564417858386350382b576741516b4941454a53454143457044414b67696f2b4b39696d52796b4243516741516c49514149536b4941457868465138522f487a374d6c4941454a534541434570434142435377436749712f7174594a67637041516c49514149536b4941454a434342635152552f4d66783832774a53454143457043414243516741516d73676f434b2f7971577955464b514149536b4941454a43414243556867484145562f33483850467343457043414243516741516c4951414b72494b44697634706c637041536b4941454a4341424355684141684959523044466678772f7a3561414243516741516c49514149536b4d4171434b6a3472324b5a484b51454a4341424355684141684b516741544745564478483866507379556741516c49514149536b4941454a4c414b4169722b7131676d42796b424355684141684b516741516b4949467842465438782f487a62416c49514149536b4941454a434142436179436749722f4b70624a5155704141684b516741516b4941454a534741634152582f63667738577749536b4941454a43414243556841417173676f4f4b2f696d56796b424b516741516b4941454a53454143456868484f4a427567774141416f314a52454655514d562f48442f506c6f41454a4341424355684141684b5177436f497150697659706b637041516b4941454a5345414345704341424d59525550456678382b7a4a5341424355684141684b516741516b73416f434b763672574359484b51454a5345414345704341424351676758454556507a4838664e734355684141684b516741516b4941454a72494b41697638716c736c42536b4143457043414243516741516c4959427742466639782f44786241684b516741516b4941454a534541437179436734722b4b5a584b51457043414243516741516c4951414953474564417858386350382b576741516b4941454a53454143457044414b67696f2b4b39696d52796b4243516741516c49514149536b4941457868465138522f487a374d6c4941454a534541434570434142435377436749712f7174594a67637041516c49514149536b4941454a434342635152552f4d66783832774a53454143457043414243516741516d73676f434b2f7971577955464b514149536b4941454a43414243556867484145562f33483850467343457043414243516741516c4951414b72494b44697634706c637041536b4941454a4341424355684141684959523044466678772f7a3561414243516741516c49514149536b4d4171434b6a3472324b5a484b51454a4341424355684141684b516741544745564478483866507379556741516c49514149536b4941454a4c414b4169722b7131676d42796b424355684141684b516741516b4949467842465438782f487a62416c49514149536b4941454a434142436179436749722f4b70624a5155704141684b516741516b4941454a534741634152582f63667738577749536b4941454a43414243556841417173676f4f4b2f696d56796b424b516741516b4941454a5345414345686848514d562f48442f506c6f41454a4341424355684141684b5177436f497150697659706b637041516b4941454a5345414345704341424d59525550456678382b7a4a5341424355684141684b516741516b73416f434b763672574359484b51454a5345414345704341424351676758454556507a4838664e734355684141684b516741516b4941454a72494b41697638716c736c42536b4143457043414243516741516c4959427742466639782f44786241684b516741516b4941454a534541437179447772347765666c4372776846734141414141456c46546b5375516d4343);
INSERT INTO `commande` (`rowid`, `rowid_client`, `rowid_periode`, `nom_client`, `sous_total`, `total`, `date_commande`, `email`, `adresse`, `no_app`, `ville`, `province`, `pays`, `code_postal`, `telephone`, `poste`, `notify`, `facture`, `identifiant`, `paye`, `prise`, `signature`) VALUES
(21, 8, 1, 'Jessy Walker', '59.76', '66.33', '2019-02-05 20:56:02', 'eyJpdiI6InN6b2lzUXlBUGJUWGVSVTF5UDJpU1E9PSIsInZhbHVlIjoiQjNldmlQMmVFM3NkY3JcL1FYeEM3R0h3aDJNQ1FLRGk5UjM0WEJMb1RIS3c9IiwibWFjIjoiNTEyYWFjYzYzZmE3MDA1ZjBkMzFlOTkyN2Q0NTM0MTE5MDE5NmI5Mjk1YjljYWI2OTIxNGNjZTgzM2U4ZDgwZiJ9', 'eyJpdiI6IjJhRDdnM2JMNHVJZitDVlk1UDd6M1E9PSIsInZhbHVlIjoiZGxXWkp0S09WdXlldURuUW9CTnJkOWdVN25oc2VVYVA3T2xkSTBWOXliTkxISTVReDcyck16M1FyQmxqRWVHSyIsIm1hYyI6IjdlMDU2YTE1NGZmMTdmYWFhNDk0NDViNWFjMjk2OWZhY2JhOTBjZWQxY2NiYjU4NGU3YTJjMmFlNzJlM2QwOTYifQ==', 'eyJpdiI6ImJ5bVE0WndCOEpRRHYyUFlqcnpDM3c9PSIsInZhbHVlIjoiY1h3TGk3ZE9mckIzQm1MSlZQQkNzUT09IiwibWFjIjoiY2FjZDQ5MjU4NjZmMWRiZjAzZDMxNTNjOWViY2MyNjgzZWNmNDU2NjhiZDg0NjRhNjM0MjllNTZiODYxMmQ2ZiJ9', 'eyJpdiI6IjN1VTNmeXBSSlRKYWlyQW5qXC9vTkhBPT0iLCJ2YWx1ZSI6IlpEU20rTktMR0lvUGt6eExLRDlsa3NmanV5N2JRXC93QU5xaytjdE9qRVZZPSIsIm1hYyI6IjRiYmI2NWUwZDEyMDQyZTVkN2QwMWQxODVlMzEyNzFkY2Q2M2NjYTc5OWY1M2Q0ZWM3N2I1MDhjNWIyZmUzM2UifQ==', 'eyJpdiI6IkY2YzdKQm5hUlJcL3Ftd21VZ1l6WWx3PT0iLCJ2YWx1ZSI6IlwvU0ljVHpGWFVVazA3Zkl1Z2gyQW1BPT0iLCJtYWMiOiI0OTU0NGFhNTBjMmRkY2FjMzk1ZDIxYzZkNzZiNTQyNmM5MDNiZmEzOGQ4MDlhNTMyYjE2ZmEyZTMyMTIzMDRhIn0=', 'eyJpdiI6ImYzTEVETzd6VTM2YWFNb2t6dWVcL0RRPT0iLCJ2YWx1ZSI6IkxUUjJ0cEFiSWNrMkQ2TWR1c3VyQWc9PSIsIm1hYyI6ImNiMzRmZGVlYTNjZGQwZDJmZjkyYWY2MzMyN2ZiZTJmMjFlZTQ2ZGQzZDlmODU5NGI2NWVlYzU0OTY3ZDcwYzcifQ==', 'eyJpdiI6Ilo5MnJram5UZkNMbHFJNVVOSTlcL2lBPT0iLCJ2YWx1ZSI6ImJUT2ZcL3JWS1llWFVSQjlTVlVBQndRPT0iLCJtYWMiOiJiNzZiODgzMmYxNjFmNzhkYWY5YWI1MDNlOWEwYWM3OGUxYTZkY2FhOTgzYWEwNzI0NGFhMjNlZTY0OGJmMDA2In0=', 'eyJpdiI6ImdZYlRjT2NvUzBaWUJ4cmtJbERpZWc9PSIsInZhbHVlIjoiWURRS1FlWldqTzlBM3NLcUoxeXlMWTc0eU10eEo1b1wvXC9QN2MzdWEwK0VBPSIsIm1hYyI6IjA0NWU1YWI2OWMwYjIyMTE1ZDJjMzFkMGM5YThjZDFmZTk0Mzg5MGE2YzA5NDhkYzljNDY1YjBiOTNjNmIxYmEifQ==', 'eyJpdiI6IjJWcHNlSzVNYXlUdlJYTmkwMDkrbHc9PSIsInZhbHVlIjoiXC9QXC9GQmYra2h5S2VDaEhEMjBUWUxBPT0iLCJtYWMiOiI2MTI4NWE2NDRhMTIzODNmMzRmOWRkYjYzYzNlY2IzN2VjYzI5MDhlMjBmNWQ3MWE0YTk4NjM5YTJlN2Q1YWU2In0=', 1, NULL, '', 1, 1, 0x646174613a696d6167652f706e673b6261736536342c6956424f5277304b47676f414141414e535568455567414141763441414144494341594141414351326555674141416741456c45515652345875326443646832587a33767630704b5255564b455a6f5552656e496b4a536a51686c4b366967616a43654f6f554d70777a486b47464a456d52316a2b4d74553055515a547049684361575142757155516963566d6854584a33733536367a7535336e3276596637337676656e3939317664643776652b7a3131712f39566e3775652f665775733376454d554355684141684b516741516b4941454a534f446b43627a4479632f514355704141684b516741516b4941454a53454143306644334a5a43414243516741516c49514149536b4d414743476a34623243526e6149454a4341424355684141684b51674151302f4830484a4341424355684141684b516741516b73414543477634625747536e4b41454a5345414345704341424351674151312f3377454a53454143457043414243516741516c73674943472f77595732536c4b514149536b4941454a4341424355684177393933514149536b4941454a43414243556841416873676f4f472f6755563269684b516741516b4941454a5345414345744477397832516741516b4941454a53454143457044414267686f2b4739676b5a32694243516741516c49514149536b4941454e5078394279516741516c49514149536b4941454a4c41424168722b4731686b707967424355684141684b516741516b4941454e663938424355684141684b516741516b4941454a624943416876384746746b70536b4143457043414243516741516c49514d506664304143457043414243516741516c4951414962494b446876344646646f6f536b4941454a4341424355684141684c51385063646b4941454a4341424355684141684b517741594961506876594a47646f67516b4941454a5345414345704341424454386651636b4941454a534541434570434142435377415149612f6874595a4b636f41516c49514149536b4941454a43414244582f6641516c49514149536b4941454a434142435779416749622f4268625a4b55704141684b516741516b4941454a534544443333644141684b516741516b4941454a534541434779436734622b425258614b457043414243516741516c495141495330504433485a43414243516741516c49514149536b4d414743476a34623243526e6149454a4341424355684141684b51674151302f4830484a4341424355684141684b516741516b73414543477634625747536e4b41454a5345414345704341424351674151312f3377454a53454143457043414243516741516c73674943472f77595732536c4b514149536b4941454a4341424355684177393933514149536b4941454a43414243556841416873676f4f472f6755563269684b516741516b4941454a5345414345744477397832516741516b4941454a53454143457044414267686f2b4739676b5a32694243516741516c49514149536b4941454e5078394279516741516c49514149536b4941454a4c41424168722b4731686b707967424355684141684b516741516b4941454e663938424355684141684b516741516b4941454a624943416876384746746b70536b4143457043414243516741516c49514d506664304143457043414243516741516c4951414962494b446876344646646f6f536b4941454a4341424355684141684c51385063646b4941454a4341424355684141684b517741594961506876594a47646f67516b4941454a5345414345704341424454386651636b4941454a534541434570434142435377415149612f6874595a4b636f41516c49514149536b4941454a43414244582f6641516c49514149536b4941454a434142435779416749622f76497438307954387558575339307679563931772f357a6b756b6d756b2b54715364346c795252723855644a6e706e6b31355038595a49724a486c656b6a66504f3031376c3441454a4341424355684141684a594f6f45706a4d326c7a2f48512b7430707952636c75653268427834774868754370796435644a4b6e4a506e7a41583359524149536b4941454a43414243556867425151302f4b6462704c736c75552b536a356d75793650327849626754354d384f636d664a4c6d6974776448585138486c3441454a4341424355684141714d496150695077766532786e644e3872416b3739327a7135636e6558575364303243793838314f35656376307a796f695176366635392b653576334941756d2b54334f3050384c643034745033484a482b663546564a3370446b556b6c6f3936596b622b33612f314f6e32343254334b787a50634c3961457035616166444f7965354a4d6b66644f4d2f4b386e6654546d5166556c4141684b516741516b4941454a44434f6734542b4d57326e316d326563384750595879584a2f30377978306e2b4c4d6e7a6b2f787464326f2b627454705737396235357030717953333662712f5954584d6137754e797469524835766b6856304d776e4f3754633759506d307641516c49514149536b4941454a4e434467495a2f44306a6e5046494d66344a3263596e684e502f6e463272636a356e705a5a4c634b4d6c566b3378514635523867795333473945704e784c63554d447546354a634f736e76646a6345624a67554355684141684b516741516b4949454a43576a346a34663539556b654e4c36626b2b754247343933372f376376737463784d3343545a4a38514a495864332f766d6a69626753636b65554753377a6f354d6b354941684b516741516b4941454a48494741687638526f43397779474e75586e417034673862416d3456694a6d6f35624f542f5067436d616d534243516741516c495141495357425542446639564c64637379704b3542344d626e3373436749387431447534655263376359636b317a3632516f347641516c49514149536b494145546f474168763870724f4c774f567935797a4245442f6a562f2b666858646c5341684b516741516b4941454a5347444a424454386c377736382b74326c7934596d5a467770384774527047414243516741516c49514149534f4545434776346e754b6837544b6c4f523470627a5a5032614f756a457043414243516741516c49514149724971446876364c466d6b4856662b3336704f3441475863554355684141684b516741516b494945544a6144686636494c32324e6131326f4b61506b753949446d49784b516741516b4941454a534743744244543231727079342f572b56354b66364c72356f53543347642b6c5055684141684b516741516b4941454a4c4a57416876395356325a2b7657722f2f6f636b65654438517a71434243516741516c49514149536b4d437843476a3448347638636366396d4351592f6b564934306b3654305543457043414243516741516c493445514a61506966364d4a654d4b31765433492f446639744c72367a6c6f41454a434142435568676d7751302f4c6535376d394963746c75366e2b5935454f336963465a53304143457043414243516767653051305044667a6c71586d62616e2f63394f6370507459584447457043414243516741516c49594673454e5079337464374d3970496b64362b6d2f52314a37723839444d355941684b516741516b4941454a6249754168762b32317676715356375254506c36535636344c517a4f5667495347456d41474b45374a336c304567345046416c495141495357414542446638564c4e4b454b6e35396b6d2b6f2b694f5444786c3946416c4951414a394364772b79524f3768392b61354f5a4a6e7457337363394a514149536b4d447843476a3448342f394d555a2b664a4a5072415a6d452f436759796a696d424b5177476f4a2f46795375316261507a544a41315937477857586741516b7343454347763762576578724a586c4a4d3930624a586e656468413455776c495943534236795a35516450486a796635374a4839326c77434570434142413541514d502f414a41584d7353584a486c457063766a6b6e7a4b516e525444516c736b514375643275376366756b4a48783231504b694a47774946416c49514149535744674244662b464c3943453675475469323975456237416e7a42682f33596c41516e304a2f436e5362687865323653472f64766476516e4f547a67454b45575934574f766977714941454a534b41664151332f66707a572f7451746b7a79746d675342654c64493873613154307a394a62425341722b5a35474f53724d6c6f766b61536c3165383335546b6e5a4c38664a4c2f73744a315547304a534541436d794b6734622b4e3557364438653654354965324d58566e4b594646456e684d6b6a74316d70465a69773341306d57586d302f5232652b5370612b652b6b6c4141684a49346f6631366238473739326c326e7550627171637a74303379642b632f7453646f5151575336424f72627557496e70744f75412f36464a352f6c7153327932577449704a514149536b4d422f454e4477502f3258676252376e50675834556f6534312b526741534f52774133483978396b4b636e7752317636664c4d4a502b7055764b31536434317954386b756372536c56632f4355684141684c777848384c373843664a626c684e394576542f4b645735693063355441436769384f4d6e37645872654d636b764c31686e4448344d2f794b34436e3571456d34532f79374a315261737536704a514149536b4542487742502f3033345676697a4a77376f707669774a626a2b4b424353774441492f6c75537a4f6c575748694437583550385949554e66657369586e36584c4f4f645567734a5345414335784c77772f7030583544724a666e564a4e6670706b694248517274624633576d4474393632743271764e76666561582f486c38535a4b3756777542793244745172686b33552f312f584665457043414250596d344966313373674f3067436a6e5a7a37564d68383073415261364f4344434a3348746850322b7a396b3979324f753337695a56734b4d686b644c6356706c4363614e6e735a6f454557734e2f79646c392f72586878336448635658367179545858694266565a4b414243516767523066336b4a5a466747432f71696f6935734f51723739333931547851636d655844565a6b676670546e36594f782f5a4f575773457564357964355a4a636d464a2f66705167624b41797344363855576c5075394b567756492f70436679334a4e396264667446536235762b6d456d36664631536137593959522f507876705976695866303879304d593638515a79597776756443567762414b652b4239374264352b2f44626e2f7234754f753070346a636b656443416157496f73336e41384e39582f6a444a357952353972344e4a337965594d51763372465a3055695a454c4a646a534c7730306b2b6f2b706871576b3943654239535a4c4c646272656f7776302f66507533376751386a6d6c37456467726457623935756c54307441416f73696f4f472f714f563457364166415839467669334a562b3668597475654444356b38746c58326b432b666475583534396c5a4e64426b375875517a6442512b652f71313270316a706c6e2f613154674b63396e50715832514a372b645a3732784a5063725072356e6b426c55363071587176665333596f33566d35664f5650306b4949454c43476a344c2b63566562636b7635486b4a70314b722b373838767457394b54397130596145546674584139774454704c6669664a4d37726333662b59684771653539304b48506f3073453652574f5a4142684a4f55332f2f774d7439382b374734514f545844624a4e6272306a57394b4173645354344554315347334d6765656a734e4e544b4339335674715a702f36494b443438396475536e3650375039696b4748747056327a5133394737712b744c535167675a4d6834416632637061796464485a31392b336e42347849324943766a704a33303044626644684a334e48795374657946436438777537712f367a66506470632b3875494c6e3270533939584c384c564a36544e7134396a30704359485174682f546e703567524844483053787256766e4d2b704a353964664b3565516e5576374f4d744e52336f4e617a36466a6356417a734866614f454d6631537a4d592f73594d4446735057306c674d7751302f4a6578314a7a5759324358314a7650375537532b564c744978542b2b65547577622f70626733324362437471346a573477303569576f334d505433784353663247636949353570733437514654454735515a6c524e63584e6d566a38392b543344724a6a533538657663445134772b762b5148776c3549732f5a335a63673763496970314c39627861324847386b724a2f6e72485963466839427037575055747a3154755551614d37443274304c394a5841414168722b423444635977674b34334364586f51633263554e354b4c6d7466487766354a63363649477a63383572656445727a37703538614172454a4458574f656d7552573154687a4667386a745367705266453772755551526852426a726535494e7652727558346879546631526c4e6c2b2b716e2b376a3673504e416f78787a574b54654f4d3931397a486c3046674459592f626d682f572b486967414558762b4c7a5037562f2f7a736e656630796c6d64574c584433753077337774425972466242636a4f7a564a6578575948617551516b30492b41686e382f546e4d2b6863474b5956786b6e394f66527954356b6f45626874494d39356850722f7241594c356e456a59525177512f64744a36597041586d644d3433585853502b64347a416c33707673316c55743373634b506e30304a54506762492f3971653770673765713344677164653635443367486239434e4138503444716b6548334c443147326e3455376a5150624e717a76754c667a3847507a4a563751467548596b5834756273306c3378775538597276616957333537392f6c526c4a79697a6b6f644d2f435453653631614149714a77454a4849324168762f5230502f48775058702b4e393370373939745071704a4a395a5066696c53623637543850714762356f4d667976305030664c6b5030322f65326f52304f6c36556632474551633376414366665530767049302f2b6368764364756a536c786132716e51396a772b2f334a6a44756437473653684c5775526864504f5076384e52763165483661302f3848354b454768784c6b6c33566865766675796e65507a624744392f787554483162634a537545365663726d655478307a34496e2f556c5a615053537751414a54664767766346717255616e39417543452b4b4e36614e2b6d3278787956587a587272425638556e6e65703269504a7757445a483353764c624f2f78393577722b51332f385a46755a2b70322b61704b5053504b70585745312f74304b7479543354304c39676a6d46344f75375677507334784932703137325059784175334764367652386d446137572b304b3743323362464f35303756705457744e707635396e704c4e304c376157386f5054764b636f5a313137577147517736425267357663776c495943304554764644645333737159623746355779424f4f2b543549335844434264724f413063394a2f54345a6642696942494b563466593149726d61782f385875574e7a2b3142505964392b2b3677666d584d656e345162686c724756436a654e65355a3951446530675573733147623633532f315964695365524f4c7a4b563064574874382f4d513641312f4a663465567937416e497777487458507266324c53363469324c37656362764671342b526136623545587a3444394b722f584a5041713850416d484a6d4f464f497a79655879714e79566a47646c6541684c515465436f3730447235396e6e77356f76694264324f654652766b2b62585a4e7344513675686b6b66326d5943346b755a72423146336a634a7758656b3979544139434c35304a6c4f7764763835324e5974485041375941626c63394c677439734b7a446e644f37524630312b77702f76636d6e6942754b78453435685634636e554a2f384c6e456a64366b6b722b7469557a44497956374661544a2b2b4d6a596a51716649376a346c434258676f6176574333444b354f38352b47585a6459526679624a33616f526349484546584b4d6b4532735076695a793756796a4936326c594145466b4a673741663351716178536a5871516c4f346947416b6e796634647a2b745368665a703832752f746f544e767268692b67467a634f376a4d322b6f50387943546361557774472b564e32704d77637567467139534e6d346d75626b33576565564b5834656858526d513647734b4341476e6d32386f54756b4449495833615a6a6b45586c4e746f446c4e4a37422f53634a425178336b2f3747644b79442f502f5a336a687644787a574750706d742b4877714d6d65387a7245343179667a3644444634516948415056427846636b3457424a6b5941454a504232424454386a2f4e534542794b65303652506c2b6939536b335878355848364136563846386d5a597259626f347978566e714f465055527143594f65514e6f7352592f5268313063586a477863652b7054666c683963784a4f365134745a4454352f6830784536646f4442326137564c472b37394a324e416a784e6f3862796d4b6458715175616f596b4b5367665844335a34714e666631353975596b392b335365504937574f5455676c512f714b737455756233394353336e47444e63666373747a446e66615a504d4a5264534541436179656734582b634661774c627148425261632b62664162365353664d554231307644525635474c6a4f62613165634f3359614266502b632b484539542b4175663968493843584e7454576e6d485049726f30494a2f43336e3241776650574a4779694379774670556b6d76654179424a3448533761324a52763878566d4f654d536e5768397365386f6f75356573384977337674643659554953503935484b32426439626c7730497347736631493952467254722b784f2b2b754d565574306637706f627566397645324e2b74416d6e6576517674745970446e69716f62715a6a734a53474268424454386a374d6774572f7661354e633652773132757731592f77333634446573562b71584e5876473141386844626a734747425179305935562b6468457246512b55444f794f6d3770734e54506b7a744e2b7837585a6c4c4f4a556a78755076745763782b70672b336b4a314335335933385835394b302f707a434c59656279696c30726439764e724e663350564c51626f2f71695a44686a4d3235616369625958307162493474515567722b336e784b6d384d73354441744d54305043666e756c4650625a5a4863347232744f65454831664634523730526937666c36664d504c7a73616432513354597477326e6937675674464a4f43506674723336654c2b48766165494659494b66395a6a4e78426964614d754e79724d714678442b6a7744492b69702f37426932507a36422b675a7269622b4c48456267336c4f45644c3845396c4d72684144666f594a4c457758424c746431384f564a324e51573466655354524775503838654f736843323832567668576566466355306642663641756757684a594167454e2f384f76516e7374653137526e767145666d677762356c6875346e346e4d366e2f6641452b6f2f595a75386836784146736a41572b676747424b354e46455a7242582f6c756c6a534571374872397a46464844445559546242347967593235472b7244326d663049314b667055353338377166422b553854494d706e5579745547682f7a4c6c4b706d493037676973526e307462756357614b3331726e536869717653675537354c39695542435379496749622f3452656a4e667a50796f586456755964617879303138784c4443617356364f393975646e666650304536424c49544c6d544c59693068445730715a5358594c526a33344552542b6d5570534d4b7463362f43767169444d5471472f39324d786562656278686e546646706d696a353974556c454f3662633266736c4538326c444f6c6c706d31636e59584f5034447246657a425732707463596f4d2b656d796e74706541424536586749622f3464653272627137793642766a665170444e4d3273506569674f4c446b2f6e2f523778586b702b6f2f7575525365376455366b36574864583557424f7855674e69767871456a4c6f4c4548614538456c756f417367645061646167332f325063392b626b734d7677782f32486d4b53685174413647636d4b454a6850675035575a49363644626747346737356a6831453470362b645374416e6163454a4c412f4151332f2f5a6d4e6266464a33576c5036576458734737394259482f61312f586c764e30612f50334c39326f62414e632b32352b4c764b6a2f656b6b6e394742776f6935565a4e685a4f7a36446d3366766863455064357570467646554631734e78384243742f686d6c477154742b7343576964622b542b50563868435a6d746169474c474e6e45786b7235624e7461687172324d4765717a392f50616c7732783770696a563166323074414167736e6f4f462f2b41586961767358716d472f49386e397133395472624a632f552f31355544336e4178686342535a496a76486e50543254543361667247695737745a61414f722b32346d3570786e3662754f3539696c2b79463063497a354358444b5457704d5a4b6d2f673776382b386565396a50662b6e64366951584c356c7a394e73614b53756e63396f79562b71426a7157356a592b646f65776c4959454943477634547774796a717a6f4c51333356582b66334878764d753075644e7533626b674e485362584a71574152334850756b7552336430794d32347a506134707637584b68716c3073794e3644386245457159314239466d53626c5079595a326f7a72706c71642f424b546632557a4a743358796576364f61395a447836733374316a4c507449622f324a69747772396571314d726544626b48624f4e424352774151454e2f2b4f38497655704455476f2b4c505852763875762f51704e435634464b4f797274794c6a7931473838756d47474469506e595637634a7772497638744a6c2f43496a39776953503336454c2f73566c37747751504856696659643231775a38587a664a69345a32747342324744316b554d4a3961366d6e334966437876744a4162782f36594c4f6c356a52706a5838536550356870474143476f6c75425835387951664d4c4b2f745456765853326e2b4f3574437a744f745a6c59473176316c594145396941777859665048735035614565672f524c41474d4951526559326a4e717847664f386c4b4c4858445359734445695330387475437864307455304b466b792b506c356d544c614734536c6e446a6936303177587648352f70467549335a4d376c4f4e5457616d4f7a596274626e6637366c306e364d664173713575547245372f6c512f6476382f5654554c656b33682f5a4a75362b72626e764f796d513270762b6c74353344384b664f51696e2b754f5866713657767666704a594645454e5079507378776665553546796b4f7353587453564177527375695135575a4d6e753670696536715974754f3855394a3370546b4e5a582f4e4e66656e4f357a637637434a4151736372704b3967744f57346c3561473835434c79385a524c536656366c71786d41762f4f635567666e6f642b6e4a766e6a4f516338554e3973594b6756556374535856734f684f527468616e4b62645653445458657766657467457a786566542b5358362f5332564a51443262626e4c3462306e716247376e46573373793454504c3177684c39383134455a7456393246767633356e41516b734245435533796f62775456354e4d6b573039626a66565136384758426b5949527563757753442b78695434396d4b67484676594b4432704f74326151702f2f3263554c66466a6e38764d4653664331782f67763874596b377a4f7a47785231426e447451646f71706c504d38354239344d49435637495274626330624d52774b52755444764b5163356c6a724b565836373166742b6b7463353871315333764e556b4d6b4b6e366e474e393575787a3672586e382b72374b34586666594f6271546e587937346c634c4945446d566f6e697a416b52506a69785a6a3649326432387254522f613354334f4d2f36394b776b6c55482b456b6b442b6372474f6f586a584a3837722f2b365075524f3931335733424b2f703032504d5a4e69673336553743657a615a374c477a6267616d4771412b425a776a6d48737150532f7142786357337556376e6c474d697666364868757130486f577237724336744c71614878516b6d6333696e50792f354b4c46722f487a32756a643670734e6a32475864516a4e594d706650486e71674b384b4767544b2f4f4c6e55736c4e31436b506556516931747536723577794b564959424d454e50773373637a6e54684a586d764a6e4c68717653764c594a502f636e51526a43484a4e545a41666d776d4350392b6c47787a3348437050386e383354334b5a48557268696f5378535676363572534c6b2f503337445a526245424b7a4d53594f6332644a595035333642546349304730655753334b5a7a6961706a4c57726d76395464624330786948584d75374676327a6264374a492b65776e653558657a6c536c302f506971534e66576376665850462f6166666139766e4c503266636471702b666f786a5947483257334a59614b642b633549505055484c4c372b5753313033645a69497778516637544b725a3759454a5948426a6e48416178516e374c5134382f6b58445964672f64412f33492b5a44586d7479683250556348714a2f2f35747134476f7038427443396c2b4b4b52574377474e4244624f4a585873776c386b75654663413833514c2b2f4a783361334d44632b6f33393875682b653547646d47482b4e586461472f354c382b792f6278634467706c554c61584f6e2b41796f34787132484f4e5255706c4f73665a7a46514e62342b39564835336247696d37326b787843394e4846352b52774e454a6150676666516b577177425a635069434b55597a56364f34766c7976632f557066756c7a5459417653417833546d4f6d7976334f664f694c6172304938796d6c3775743554424638647847584f72586f6b67714a6e616633666276717258632f3579454d2f63636b34625a452b58384561674e344b566c744d506f666e65514f6e5a70553579586d4257474e487a567941636c55395a7a4f72594b7572742f647a493373646e584e7039373074526d434e46703376784b3877392f534a58496f5435424f393965362f797533776d743273317a644c344d4b48352b4168762f78312b41554e4d416468784e724d7565516c59626754716f503433707a365334754144394b2f495678382f6e484c7131686952756f58583334507a4a5645443877683143303633764f4d50673535535434634b714e786c6e36747857456c2f3537694b48424463574e7a7067515276375475745372704268553370354162617774596150483779782b7a63543649426a6f47456e6c686f626635563375502f7573625a32786173756e2f565066656e7872645273355638325866645a35616339794f465869352b71446e6662336a734d744e726f633943675332417942705273636d316b494a336f51416d7777534e5735792f2b6667475179306c424a65573568383150634b4468782f62533542787a515036355375456d645a2f427a4b384d66697349744b515873674f6e4f337154327954373235793662386c64574d3661514862373439302f7954556d656b41532f364c4643762b56323763376454644459507466596675724133726f434f376569724b6553594d6a7a65565558654378634f50446863307152774f594a4850734c61504d4c494943444543434c41395638322f537037654238695a4a7042324e3272704e7244477271437052675a744b4645766933464f45456d4e6f4666494857465a35722f546a685a344f3068465376532b46326b52374638443932494348754e77544531304a674e6a5577536e3250722b6c4f2f792b6130336b2f352b615054533279785571394e5a75704e3332316d79432f68795147554a4b32346a524d4f4e4468382b796e42435142436677374151312f333452544a3041527165382b49354d474f655872765032464251573079454b45675475317939466e4e6c39436a4538576f6947437763616671544c6d344e503934593150624e474c4d62675366334a582f324349766c7474773033544d37764a48394e51773330484e377461634e3871373367786e4b627747563969544d4d78336a2f53486e4f67674577564f315162754674326f547072633156592f3178582f2b5559362b3659456c67734151332f7853364e696b3141594665465972703965586674697976434a33536e32396335597a784f61456b582b6f61714b6a416e3453554c4573326f484577786e543556666e2b725331644b7536455a506e444234512f5668516d36486e4f4e6a56484b5a7552447a6e4342777243416b36663777312f49715832386832687978523062544736332f6c6658575232414f765a376756736a616e75774b6556336a526f42573676555739616f6a6e4f59597450585a76535a59704d32354831615568732b6f33487a516477494c576c6c31475752424d5a2b774339795569713161514a6b4b43466c4a363471626656594168674a3748316334354e4f48764d7636644a3966764a41656e32712f4c5a427658322f7448454c4b6873557168693338787179676341676f612b32674e75624f782f7653387a4d4d2f424e6550746d552f743437367359726d366b6a4b32462b677033717634442f333753355535784b6c306270317333784b5a652b336f54756454346f4833667a7a4850507a6a4a4137734f2b467a6e4d3161526741544f49614468372b74784b67547758635a3476316553397651656c353366546b4a5148486e377a784f4d6f632f6f4e67373773726e496b4b397649503667537033596a6c4f79545842532b68464a626e704f30522f533031464571322f6c79624d793947335830546341414265365355524256424233384a4d48794769304c394e54654a3461464b532f4a573645573570444370584279366c2b4752642f2f72626732684f543348366945394d36622f6f6e566a646c68357a335573616132722f2f78354b776155653276716d71613648416735765a726434734c65563956343856454e44775838456971654b464244426d4353616b38466774722b37635945704e674173376168376769775868466f4155636267766b4161526a447a58617549442b707936313046355a453535514f64666a61485071537a47474365765a36584e724e586a6570734e54642f556f2f693763684c6242757879776b757872522f594634375039795a51546e305047646a4c5a7a74754e715455626438624e705274494754354e7a644c704c51644b726a35764c6872664d6a35447456337a6e616b4e53596d42756e7a2b644248467a6236706467616d3869356b6844303061562b686c7454346f4e4b345554534e37397033303732654a35627a312b705069752f4d386d583739486552795777575149612f7074642b704f612b4b374b6a425270495833625649576b6976745136786f4453417763306f52656c412b614c305063613271686541782b396e3246504f756c4b75354662573757785231515a62644f2b5565774951594a4e7844554c6c446d49314337765a42393552447059713930686b48492b2f64524f36624b2f33456a686c78375a4c423466534a4e4a71307462796a4a496f5a42696e4349514b58774d634942415a38313554506e724b725a75386234344735444e6a535277466c366379424331664e3764335663796e4d5552305258474d77524839536539746442366d4d59323159434a303941772f2f6b6c3367544579526a536a4765795437445354716e51574f2b354d6f5847763731397a79484968734c4b74723279574f504b394c443938796d7858796f394d704a57742b626978736b2b6134754c7142576e5130447870386e5934663774616839764c6b706d6e756a396158644f39624f6b4f4a63704f6e634a562f567065394574314a66596769682b7253663745456c5a653251766b36684451554c75526e45765242587235654e6e4e54444f694f62626a43322b52322f5344675a4a78626735743247377075542f5042466a6662344f54654a3557623072475a386275452b3265637a73752f5139652f56564c6370666366324f516d736d6f43472f367158542b5537416752305559446f52556b3451523937776b544f5a7a4c646e4364734b6a3533774930435839675938726862374a4a53464175336f6c2f65633457352b762f524a6d69544c76423735662f375a4233616330676650346341363046514c5a73324e6f68554470314c4d4c4a4a513773724f78565674647667336c7150596b534e4e61444931382b6d453848514b3157413535727a6b48363567634546716d394d7a4a4178614950624871364779465175543756373173636c65636f46796d475163367458752f667832586265515562662b624c4a65337850743854534a3539374639324b6e6a552b37707a46725a483534445a5a354641336158335a2b4a7745466b314177332f5279364e79527944517073766270514a6658745147654e5a412f533662424465635976786a494f43374f2f51302b504b6457394f757a516f75505268686264476d67617262624538437856676a563336663249303975332f6234385367374e6f6b45737a2b42543036704941634a385063467642654478466334426876536b4e336942356e74534567466e6555746d6f3376387345326d4e4d396f325836615058586170446762366e382b6631652b7671514f4f695453534a44746a4d346437547968525a6d36375233524356494f4d2b5048694741346837444d6974583177357977614b564d5a7361497267786c68714a6654567865636b73466b434776366258586f6e666736422b68713550495a786a767351426b3562424f6c594d4e38707964636d2b523837464f4130447639617376556f787947414d596c37773657547644374a2b3835676f4f425454796172537a565476475753702f65594e675a2f715278392f53526b494e70586349763779367252324469426663632f37336b4d66564c3439746c306a6233787150576f503050366e4d35664e476343706a6c6c52376735326857375248417437774b473853366854666c7a30586a6e2f62794f34796a5034554c3472556b7730736c45526f596f2f72544a4248425059695055563272337362492b745876526c477657567965666b38437143576a34723372355648346d4170795934543545595337636833436a474f732b4e4c5771664c6d6564555077305657773574546a326c392f4171314c7774694d4f6658496e4d356a354c57423462683073546c395330383170776a73725933636930366a65366f3179574e317a76732b485535705245365a78684d446d6e537252585a39622b38797873767a3343592b7072765236635068764765495533704538774148496c5249623333345359324d586e5871574249755548437772395133536155416d70574c2b394c7a4f516e73494b4468373273686758555259464e796c74764f4746654e645646596a376231366551596f35683072365372355151624e7a45326672586774343652524a72596665516856657a486b4f2b444e72764b52625573397446747a4c506e7565793949736e647138374a6c6a4f6c71303839397067314c7970534f66745733543932706133383953526b37746f6c33506878497a4f4637474c4b2f45696d51424b4349707a5363387642753343335a75444864716d582b2b704474716d53695970336e73307a3951754b4c4f5639367a73666e35504130516b4d2b61412f757449714949454e45734264684e53494647527142566366736e556f797950516e6a716635616252616f34686973484461652b75464c4c31382f6874662f584172436c55384f563269347850375762694970705853454a4162366b6b76653970376b5839442f3035764844766f655a474c65684877504863743366313658766639543576726d7a71384b7448616e6373626e742b6f366b6e30723458424e524f4a5730476e335a547737764f3571434e6f796a6a76364572737467337178437563665747416c392b4e716f6c7475415a5865324171655a6e50784c594241454e2f30307373354e634f5948324337424d682b42692f48724a6d6130736c77436e726d3232485978516a4f626e644837316e4a4b575533314f4e58646c35326c6e53464171376739444d3658513334393062686f4539584a6a74492b306d3571684d514c376a486e6573786a43754a7a73386e452f354d6e776c47342b764164316e45344a63443376526f4e62675a2f744e6e4e54735355646152332f6755464f305551795353473734714c7173646b6b554a7951464b643968646f6f392b73655a76504472556574413448724a6143386235382b4a34484e45394477332f777249494346452f69364d374b4e554a48316c5176585866582b6e5141474d6a6331355752384c426669546834355552616159715469507246505668744f58546e5a4c724a76304f5a59426e56374e6b335530694359765258695950424c4a38337649615432783538695a71446458474834733036637672654355637a4735326b7a544a544e524d323366563971777838645352764b35674333717145334c4e77516b41454e6f5138534672415a514d6751784b48486b474430476644597051545751304444667a317270616262496b434b546b3730533137304d6e7663503869676f61794c514f734c76362f324646306a534c4e764562632b2f564e467572784c66624d413053397a49544e4c71554c4e6153366e7278686a68785a3077546875732f616745796c5561332f77512b6857752f6e7375356e61705639376b763450546242734d5972786853664431787843634335315255705749624a4638623630557566614836744866647050583851786f45634a5a69643730466d31554d614f6258734a6e44514244662b54586c346e7431494346415a722f5741357961745057466336745532727a536b356137764c614b724263474a4b51436575504a7a657a70575346654f64617243637a4249342f4e6f6571304f474b79724233725a36396b4d50654b4a6571336857317034684a2b30666c75535372734c756d45717a624d376571314f53416d3459366d4f6b5475505a39734f6d674a75664d61356566585237564a4a50502f423673785a314144622f7068354a6b536b325658336d376a4d534f446b434776346e7436524f614d5545434f436a47465074742f6f646e572f736971656c366a7349334c513751533270446f6e6a4942306942646671674d5935345a5667546478686274467a6f50626d34686747474959317269666f55677675504e38326f4a6f32426962427747544d4b734c764961346c2b3069646333364b6172336e2b66465055596972373978656c75536133634e734e4369454e72665563524b4d5256423276524534316d5a7a376e6e627677526d4a364468507a746942354441685151772b4d6c4567694654584262776b66312b33586f755a4f634477776d5569723366314e4e4e704d36707a716745466e2f4e424b66612b38364150504c3437646379354a5366397276793076502f517a5930395133456b50597468374e754e48423771573963397557337a2f4e5579433342306851757645305373756e4d4b57535a497474554554624337316a4679507a396a734a67632b706a337849344b5149612f6965316e45356d68515177397373663143645937664e48424d53744549457148344641665a714d47386575594e46614c5679426671744b48546c4666766f683036344e30644c2b6356314b306e333732335853582f706776732f656f304e7543386a51784d6e3433335562655034654937766d536e395462437236364558774c4735643562522f6974536b7538596c494a706141302f7150762f61656639656b32713246504c714d7765666b594145476749612f723453456a674f4151797657316642682f67474538434a516156495947344339576c796e3153586453456c337456727a6133676a76353342556750725746776c795434726c4d666f355568747763317a366b4d30374f7138523671586b4a62664f364c756b334e6c45746642356954577062735632316341356d4279474a57684e734f626a3055435568674141454e2f77485162434b426b5151772b722b6c796f71436a7a55466d49616d765275706a733033534b424f78336e74632b494b4b4354472b3172664344793071395a365347796343442b6c79697a4432506a7a662b55414a5a675057574e4b6870693243344a4938536e66522f3632636a2b5a79762b3839584d762b68444c7742687a5375747563346675524837714d5776334d5678366542634a694c35534e7842784c365634476639566e706c61442f755477475949615068765a716d643645494963444c4979526b4746584b6f612f754654463831466b4b676476576859692b566533644a3632642b4c4265663168642f794b6c386d562b6449764f74535335565452796a6b3277382b306a4e63703941365976474f4d7677703931356d37574c2b75337a383972645a6f704135625047724e65434e615849317a4f7268386e6c66376e7133334f35472f5668346a4d534f416b434776346e7359784f596755454b4554443657544a4b3635727a776f5737595256724e316d37706a6b6c33664d645666673635445438436b775569455776337545333531374472776834356267415a56437232344d665178504d6d6e74492f586d614d79477042365432346869414250447747616b54674d37702b462f39613777567447486a5133705a656351356c687558746855736745745262726138546a744a2b3371324e694a4f655a686e784a594451454e2f3955736c5971756d41416e717077454675454c446a396758587457764b676e6f486f355561614946345868696c43686c633041426c3874787a707476583453416a784c75733268726b5a74656b774d79564b55696e6c53674f77446b75433273342f55626c4e543359693047594934396137646d7559302f48487265554948594b714e7a4336653348725772496b315964376c7658744c45344e787245336e50752b437a307067385151302f42652f5243713463674a31384270542b59736b4e317a356e46542f4e4168516762586b377964676c4544586a7a736a63486571674e5568354b674b5447726249686a726637316e52786954354f71764b2f7a2b5757666f6c36352b4f736b393975793350493742537661622b777873337a61725857412b50676e6631622f53505952426a4e2f37584366663964687a75694b534a6853586f694c454c645275506a57545937352f4579327033556867475151302f4a6578446d7078656751776f6837535a656f7073364d4b362b3253765048307075754d566b674174346d7a6650764c644843786566674271734f65682b395a5354366b653444435857532f326c63656e4f5342565350635a796a53566375516f6c3337367448332b564a6a3466564a4c742f46424e576e343165623066437671772b7a5558706558365833664b374f477351616b2f44674638376f59367141365431563948454a6e42344244662f5457314e6e64487743584d75546b71354f515565714f6b34444f6131544a4c41554176685866316d537a2b775577756564643552695466772f726834457642354c4f49482f795770776269544937724f507449584863504868744a2f383855562b7264755537395076584d2f574c6a444631615a315535724c494b2b7244382f70356f50623171737167477a6d2f716d70576c352b7a4f636d4e314b4b424351774151454e2f776b67326f55454b674c766e595454756c6f34616554305835484155676c67694f486a6a7445356c77764a6b4c6d3357562f493462375035766c726b33786a4e54436e32542f636e666258365479585a467a57473555535639466d562b70546532454937337073616952383970424f6572543537695266334433336d69513337654b6736734d53666e7949314b553931505552435a774f4151332f30316c4c5a334a38416c7a4a6332705679354c63423435505341306b304a3941653871397237383578764a396d367739785a416d69314874366f50422b63722b71733336354a39577351676c695063486b32435549327a4d6350575a5177376833332b6e4a492b706c432f423558783238686c61793549325a485077746b384a484a794168762f426b54766769524c67432f70467a64796f7a5074624a7a70667079574275516d30686a2f784270783039334539496c337077354a774131656b44524239524f66753836557a46616361776766334b765247616e3372744a647a6e7354587451506d6343647169786379547a4a4934574a454c456b7459344b74683743336a515132515544446678504c3743526e4a74446d76576134362b3759434d79736874314c344f5149634c7039315770572b4a31545666664a5366346c7954743250384f672f4f6775312f3337373642416d6b3379392b5066763253704131377267466253415a4d5747486c7464794f4132394b555574634f4f4b39434c76556459493268546d616f766f496247566c38324a51564b656c5032324a6c6a4538326f2b663337647a6e4a434342666751302f50747838696b4a6e45574137443059494c5738652b63764c54554a534741636753636c2b59527858655146536167467341596861784631464a44362b2f6b5a535735655457434f6d67713134553877625a7561744337366869706b5276715550614332516459305a563359764246735863752b626c3137714f476a457467324151332f62612b2f73783948674f7739704e757235623253764878637437615767415136417269416b4f627837674f4a724d6e6f5a346f4557574d4d7679544a3331527a2f6f6f6d5151416e346a644c5168616d716152327261726469596954494a735368623171495541584e366b2b556d396f7976503039397464757444614a6574486b3378756e30353952674953324a2b4168762f2b7a477768415168634a676c42654c5662776274306152416c4a41454a54457541302b5a50532f4c705062766c78507170535337702b667a5348324d4452476177326b304759356f6735616b71674e635a7964683045414f4241583639426736785449394d3871436530416979706c6858485a426362697a61514e2f584a626c626b696632374e76484a43434250516c6f2b4f384a7a4d636c3042483478535233726d6a4d5756424836424b51774c3854494a36474451436e3079574446746c347147704c676173334a57397a37546c46616175414d3064382f346c35774356717a4c797633426e35333334427548304469317633494c71766735625a574a42657451673347786670634970723635776b6344414347763448512b31414a3049415678367574776b55524a3654424e2f594e352f492f4a79474243537758414c33542f4c514d39546a6c6f4f383939644938746456544143336b7151724a5450514653723349487a755337447a4f33656271724e6d5468417566766439712f696941796c49322b7249625a41317477792f32695644344961476746354641684b596b5943472f3478773766726b434844646a744650796a336b32354a776a6633476b35757045354b41424a5a4b6f4d36315036654f56444e2b56424b716a753872625a304532724d707763576e545875386239382b4c77454a6a434367345438436e6b30335251445841713667533756507330357361766d64724151575259444d5033644a386c4554617657794a46644b6373577554394b4659716a6a5472535031436c4a53377439585954324763396e4a5343425051686f2b4f384279306333533443542f6864587339666f332b7972344d516c734367437550365164724d4e774b3256784b4448465a4567344f4c61633830754d51482f52363045676d6e355766745a527a2b2f31503338686b6b2b70497576345053655077685a7a4967524945765072686f4b334278382f6770714b43787159565647416e4d523050436669367a396e684942664539763155316f6a767a5a7038544b755568414173636838466d64582f384856735a3862657a333165714253523763392b454c6e764f515a434b5164694f427151686f2b453946306e354f6d514258336754314976374f6e504a4b4f7a634a53414143464e546968482b4d37436f434e71592f3230704141684d513049695a414b4a646e447942556b372b75556c7566504b7a645949536b4d4457436544793837336444634a3756444277432b4c662f453036565a35726856754772306e794f3175483650776c734551434776354c58425631576871424679613554706347377762646c3937536446516643556841416c4d54774d696e2b4e5a726b6a7835527a70503349716f59554a387744736c6566375543746966424351774c51454e2f326c3532747470457142433734323671656d7a65707072374b776b4941454a534541434a303941772f2f6b6c39674a546b4341585030592f4556756c34524d46596f454a4341424355684141684a594451454e2f3955736c596f656d63424c6b31426c737367744275533350764955484634434570434142435167675330543050446638756f37393330496b43727678366f477076586368353750536b41434570434142435277644149612f6b64664168565945514779584e77317965393141572b6b2b56516b4941454a53454143457044414b67686f2b4b39696d56525341684b516741516b4941454a534541433477686f2b492f6a5a32734a53454143457043414243516741516d73676f43472f7971575353556c4941454a5345414345704341424351776a6f43472f7a682b747061414243516741516c49514149536b4d417143476a3472324b5a56464943457043414243516741516c4951414c6a43476a346a2b4e6e61776c49514149536b4941454a434142436179436749622f4b705a4a4a53556741516c49514149536b4941454a44434f6749622f4f4836326c6f41454a4341424355684141684b5177436f496150697659706c55556749536b4941454a4341424355684141754d4961506950343264724355684141684b516741516b4941454a72494b41687638716c6b6b6c4a5341424355684141684b516741516b4d49364168763834667261576741516b4941454a53454143457044414b67686f2b4b39696d56525341684b516741516b4941454a534541433477686f2b492f6a5a32734a53454143457043414243516741516d73676f43472f7971575353556c4941454a5345414345704341424351776a6f43472f7a682b747061414243516741516c49514149536b4d417143476a3472324b5a56464943457043414243516741516c4951414c6a43476a346a2b4e6e61776c49514149536b4941454a434142436179436749622f4b705a4a4a53556741516c49514149536b4941454a44434f6749622f4f4836326c6f41454a4341424355684141684b5177436f496150697659706c55556749536b4941454a4341424355684141754d4961506950343264724355684141684b516741516b4941454a72494b41687638716c6b6b6c4a5341424355684141684b516741516b4d49364168763834667261576741516b4941454a53454143457044414b67686f2b4b39696d56525341684b516741516b4941454a534541433477686f2b492f6a5a32734a53454143457043414243516741516d73676f43472f7971575353556c4941454a5345414345704341424351776a6f43472f7a682b747061414243516741516c49514149536b4d417143476a3472324b5a56464943457043414243516741516c4951414c6a43476a346a2b4e6e61776c49514149536b4941454a434142436179436749622f4b705a4a4a53556741516c49514149536b4941454a44434f6749622f4f4836326c6f41454a4341424355684141684b5177436f496150697659706c55556749536b4941454a4341424355684141754d4961506950343264724355684141684b516741516b4941454a72494b41687638716c6b6b6c4a5341424355684141684b516741516b4d49374176774546627a42426d785565556741414141424a52553545726b4a6767673d3d),
(22, 9, 5, 'Gérard Turmel', '19.80', '20.76', '2019-03-02 12:20:36', 'eyJpdiI6Imp1QzlcL2kzckNDZ09BWFlwVEhBeG5RPT0iLCJ2YWx1ZSI6InNkS2FQN3BjQUFGSnBTckdRYlFCR2VkTmxzb2ErMWF2ZFBLU0x3OURyZkE9IiwibWFjIjoiYTI0YjFjZWZiZTUyZWRhYTNkNGI2YzYyMDVjNTNiOGQ3MThhY2JhOGE5ZGQxMWZhYzk4MmE3N2NkYzU3YjY5ZSJ9', 'eyJpdiI6IjBFaUV2cUFkRjRibVRXcmdDanlhMVE9PSIsInZhbHVlIjoiSDdvVnp6d2NBTHJ2a0FPN1VJUWkwSldqN1FOTEw5YW9TQ05oMnVMSDVMZz0iLCJtYWMiOiI3NGUzNWFhY2QzOTllNTY2YmNhOTBjMjg4ZjJiNzg4Yjg0OTBmZWYyNjRjZGIzYjViMzhjMjkxYzAyZTQxOTBkIn0=', 'eyJpdiI6InplZmZcL2NPYVJJXC9uZlNvZ3k1SEdaZz09IiwidmFsdWUiOiJIOG1oY2lyc2Y4QUQyMDVJNkFjRW5nPT0iLCJtYWMiOiI3ZTc5MDIxMzdlYjY0ZTNjNWM2N2QzMGQ1OTk2NGNkOWY2NTk1MGUwNzE2M2U2ZDdiZDcxNjlkOTRjMTlkYTQxIn0=', 'eyJpdiI6IklmRVUxMzV3NWRXUlVNV2FaUTZTZ0E9PSIsInZhbHVlIjoiR0laSVVjeDdvZnBmMFVJbEJnclBQeDhZZTh1NVFXR3FrSmxNWld6dW03OD0iLCJtYWMiOiJmOTA2Y2IyMjk0NjExMDY2NTM2Y2UyMjI3Y2ZhYTM3ZWFiZjk5YzFjZTVjZGZlNmRiMTdlNTBmNDdlMzg0MDE0In0=', 'eyJpdiI6ImhwVkwzZTQ5TUtPK3c0NjFiTmNcL3JRPT0iLCJ2YWx1ZSI6IkU2R1V5TzdFUVY5YzlOQUFRZU9NSHc9PSIsIm1hYyI6ImZiZDdhZjFkNGI3ZTQ3NWQyOTA1YTFkYzE5ZjhhNzVkMjhkOTY1ZjBlNTY5NjhhODhjMTE5NzUyMWNiNmQ5MjAifQ==', 'eyJpdiI6IkFqMlJXMG8rc1QwU2VGeitJOHJKRkE9PSIsInZhbHVlIjoienk1Z1JRamlGQjFSb2ZBbld2MDRJUT09IiwibWFjIjoiYjgxYTgwYThlZjExNDg5YWIwNmJiM2ZhYTNmMDRkMGFlMDAwOWIwYmY5MTM4NjVkMmQ1NTZlY2YzMDNlNjljYSJ9', 'eyJpdiI6IkNoZUthSndWekc3amVWVkd4N3IyMlE9PSIsInZhbHVlIjoidUNHRW9hdlhrTUtmb2p3SmpDTlhuQT09IiwibWFjIjoiYzAxZTQ4NjM1ZDQ1MTIwZGYyYTE1MWE1Y2U4M2QzMDcyOWI2M2ViYThhMzdlNmQ1NGM3ZmNiMWY4MjQ0NDRlOSJ9', 'eyJpdiI6InYrMlwvbjZvQzQzcWg4TUlyc2s0WGJBPT0iLCJ2YWx1ZSI6IlwvSlowbW9IajVMeE1MZXVLb1BaVDZvTTBpTGl3Z0NTNnp4a2ZCNm1yejd3PSIsIm1hYyI6ImMxNmNiOTdkNDM5YzU0NTMwODE1ZGYzMTgyODEyODk4OGFkZmJjYTkyMmQxYWRhYzIwYzhmYWUxMDRjOGNmMjMifQ==', 'eyJpdiI6IjBldmVyZk05OERUczFrQXFZTEhEbEE9PSIsInZhbHVlIjoiV1ppUEM4T1FxdUhmRHUzNFVqa0p1UT09IiwibWFjIjoiMDAwNTgzZmVhODVlYzg1NTkyODZjOTVlNjUyYzI1NjAwYWRkM2FkYWQ1MDk4YTE1NGVlNmQyNjU2Yzc5ZDRiZiJ9', 1, NULL, '', 1, 0, NULL),
(23, 9, 5, 'Gérard Turmel', '24.55', '25.75', '2019-03-02 12:25:05', 'eyJpdiI6ImdxOXBpYzl6bDF0Z09qUmFrbXV6dlE9PSIsInZhbHVlIjoiYStuV29TTG9Benc4aW1VN21aUHQyNE9ocTJ0K2lYN2EydWFyajMyOCttVT0iLCJtYWMiOiIzYjNmODY2ODVjMThlN2E0NDEzZDE4MzczNTM1NWRmZjIyMzE2ZjJhNmI3ZTA1ODkxMzRlNzg5OTA5ZTZlOGEwIn0=', 'eyJpdiI6InR2dnN1SU9nQW14T1JYMEJtRkFiU1E9PSIsInZhbHVlIjoidzRpeWRMRXV2dFZLTzZMWkh3WnFJeUVcL2tuQ21URFhEM25aYlc5VURWOU09IiwibWFjIjoiODc4NzIyNDgyYjhmZjk1OTI3YjAzMzBkYTk0NDE5N2NiOGY5OTEzZDc5ZWYyZTExN2IxMDYzNTc4OGYwN2E2MSJ9', 'eyJpdiI6Ik5RQkthMXRSdktRZ1k2V0YzOGU2UkE9PSIsInZhbHVlIjoidXFKcndRQWpWRTZ6R3JMMDBhOUJOQT09IiwibWFjIjoiZDdmNjM2YTZmMzIxODcyZDliYTk0MzAwMDZiZWUyOTU5NGE3Yzc2YWJjZmQxZWM3MmFlY2UyMmQxY2IxMzYzZiJ9', 'eyJpdiI6Iko5emx1QjhGcW42eXkrTWlac1RHdGc9PSIsInZhbHVlIjoiWW0yOU9PeXNUbUtJVk5Eb3dZZ0lvXC8wY096cnpKbTN5Nm5FNGtxdDVFVjg9IiwibWFjIjoiMWI1NTc2ZGM4YWI2NWEyOGY0MmJjZWYwN2FhY2E5MTQ5ZThjNDc0MzczYjU5MTE4MTM2ZDQzY2I3MzgwNjVkMCJ9', 'eyJpdiI6IllJbStLbVJlVXB0UXFIT1R6SlNkaVE9PSIsInZhbHVlIjoiaTlEalhWaE9LeWI5dGsraUdHOStcL1E9PSIsIm1hYyI6IjRhMmU0NmZhY2M0MTQ5MmNiNWM1YjFiM2ZjZjZmMWFjNTBlNDI5ODIwYTY2OWRkNTgyZDMzODBiZWI1OWJmMWQifQ==', 'eyJpdiI6IlQyYmZZSDBIOE1udXV0NW1mTzZFaHc9PSIsInZhbHVlIjoiMzA4TDc1NnFmZnAwS29HbnZ5UURPQT09IiwibWFjIjoiZDU0MjY2NDQ2NWI3MjdhMzNlMWZlYTkwNWQzNzc2YThhOWU5MWJlODdkOTQ2OTZmYTMxZmFmZDFlODQxYjIyOSJ9', 'eyJpdiI6IlVhN20rM1Q5aEhoRHNCQ1B4em04aHc9PSIsInZhbHVlIjoiVGc1VW1EVmdQWVFldEQ4dkFQRVBUQT09IiwibWFjIjoiNzUyMmI4YWY0YWRhMmYwYzhhMzUzMWE3OThiMWYwZDliZjliZWQ4NGE5OTAxY2RhM2M3YTAyNDVmZThlMDAwOSJ9', 'eyJpdiI6IkFrWVYxVXlYc2xDRXVZXC80WU9ZVGtnPT0iLCJ2YWx1ZSI6ImhHdnFMQmg0YXI5ZTdzRnNvVGRPN0hPOHdWcmNjM2MxdmlzVWRnbThSY0E9IiwibWFjIjoiMDkwZjUzMjkyNjAxNTRiMjZlMjAyYzdlMGYxMjY0ZjJiNmFhYjg3Njc1ODhlZWE0MzhiZDQzODExNDU4MzcxZSJ9', 'eyJpdiI6ImxLXC9zRTk0cFF2XC9PZ1hscGRZTEhCUT09IiwidmFsdWUiOiJtdkxFZzJCd2licWlUQUwwVTNKUDlRPT0iLCJtYWMiOiI3YWY3ZjBlNTk5MjEyOWMzZTg0OTdlZjAzYWU2OTNmNzQ5YjdhMmYzNWZmNDNmNGI4YzdhNDkyYWRmNmM2YzljIn0=', 1, NULL, '', 1, 0, NULL),
(24, 10, 5, 'Claude Gagnon', '18.31', '19.72', '2019-03-02 12:30:52', 'eyJpdiI6InNZUnZGRG0wZ0FnbTdtcVJoa1Q2R2c9PSIsInZhbHVlIjoiY0NhQkxmXC9sRVZ0c05DU2YrVGZsdENuaGpHWWRVSzU1TFRlVzhsTkMzc2s9IiwibWFjIjoiM2YxNzAzYjIzZTNmZTA0YTk1MWNkM2YyNGE1YjY3YzhhMzQ4Njc5OGE2OWRiMDQ5ZWE3NWFmMTllMWQ2OWRlMyJ9', 'eyJpdiI6ImtkRERXMnF0dU1yTlpcL3pQZCsyT3dRPT0iLCJ2YWx1ZSI6IlZsNDRUWnFySGx0YW5wQTRqNWJNWFNma2dxZ3NcL1JWUlYwdDhISG9QQ2lWT1J1ZjVxY3dMR1MyZGM0K2RGYjVyIiwibWFjIjoiNGJmMGY1YzQ0NjM1ZjRjMGYyYzNiYmE0NTFiNDdkZmI5ODE5NWViZDhmNjk0ZGQ0MDQwYmIwYTc2ZmM3MDM2MSJ9', 'eyJpdiI6Ik4xRHcxZWtcLzN1MkN0XC9UYU1jSld6QT09IiwidmFsdWUiOiJnOVZcL0FJWkJKKzkxKzFhaU9qRHpqdz09IiwibWFjIjoiNTY3YWU3YmZiOWU1NDBiYTZhY2Q0ZDJmYzU3MzVhMTRjOTQ5YmJjM2Q2OGMxNzg0OTA4NGFhNDk4ODQwYzg5OCJ9', 'eyJpdiI6IkZBYllxWjFSUGdwc21GS2N6eDBja0E9PSIsInZhbHVlIjoieDg0ZUN2MElocWZhNFFLVWpYTFp4VjFwT3c0NVZHVk1hekZ6OUs5UHFwUT0iLCJtYWMiOiJhMmFjYjgxNzE5OWU2ZGVkZWZhMjQ5Mzk1MDM1MDU0MGExYzNmMWEyM2ZhNTQ4NzZjMGU5MWFkMTRlOTFjMzUzIn0=', 'eyJpdiI6IlZrbUdyQkc3ZSsrZTY1R0g2Njg2UVE9PSIsInZhbHVlIjoiWm1YR056MjdyWTFCT0ZoMDA3XC93dUE9PSIsIm1hYyI6IjJkOWVjOGQzNjUyODk3MGE2N2MwMDBjMDNlNDU5OGYxNWJjYjk2YzU1YjU1MDdmNDg2MjRkY2JiNWJkNjQ0OWIifQ==', 'eyJpdiI6IlZxQW8ramNsNkN2VWx0R21hSlJ2Smc9PSIsInZhbHVlIjoiMlE5Qys3MlJZRVFFM1FDdURDbW5QQT09IiwibWFjIjoiNTYwOWNjMDE4ZmVhMWE1NjYyZGNmMTQ0Yjk1ZGJmYmU2YjhhMThjNTYwOTQ4NTllYWE4Zjc2ZDcxNTU0NGIyNCJ9', 'eyJpdiI6Ikl4ZFQyN01YdytNek1GME9LYXQ5S2c9PSIsInZhbHVlIjoiaTF3SjNJenNaTUJtUWZCMVNpMzJvQT09IiwibWFjIjoiNjg4MGQ4MjQ0YzYyMmU4MzdhODc1NjY4YzM4Y2JlMzMxOGExMWNhZjUyMWZjYTMxNmFiNmU2Y2E1M2U2NTA4NCJ9', 'eyJpdiI6IkhTXC9cL1l4aHEraFFuVVNRTnZCYzgxUT09IiwidmFsdWUiOiJVaXlIXC8ySFg4SnlDZUhyYVJDXC9PRHVSdk1HNlJpc0llbnlLdWc2VkNVY2s9IiwibWFjIjoiYzg0NDk1MWZiMzZjYmMxZGVkZTA5ZmNkZDA2Yjg1YjUzZGY3ZGRlYjFmY2U2NGEwMTA5MmI0NjY2ODc2NDFkZSJ9', 'eyJpdiI6Ijl1NnJcL2hHUkNmSVwvcG1TdkQ4OVNadz09IiwidmFsdWUiOiJQRTVTWGtMZk8wZ1JSbkJ3NHYwcEhnPT0iLCJtYWMiOiIyNDhjZTY1NzI5ZGMxZDEwY2Q4YmZiZTM4Njk2MjY3OWUwNTJjZmIzMDc2ZGU1YjNmYzY4ZGE4MzMyN2VlMzNhIn0=', 1, NULL, '', 1, 0, NULL),
(25, 11, 5, 'Hugo Lesage', '106.49', '117.06', '2019-03-02 12:32:30', 'eyJpdiI6ImdTaEFaeHdMeDNmTGlMUGV3S25xUXc9PSIsInZhbHVlIjoiQ2Z4eXRGUVozTXRQYkU1MEVJNlEyU2d0Y1VQaStNRTYwWEMySVMzRFVaND0iLCJtYWMiOiIzN2YzZDBiODVhMWU0MDQzNzRlNTVjZGY4MjY1MWRlNTczNjFjZmJlMmY1YjllNGZkNzg2NjhiOTg0NDUyMGQxIn0=', 'eyJpdiI6IlFUK2VRRmZidmZwa2VcL2RYTFhmc0tnPT0iLCJ2YWx1ZSI6IjM5OGtZQ0xxazhNNWF0WDVpS2pqZlFyMCtpdUFLR1hXMEJQcmJmbGVNWFwvRnZpVHcySFFDNFJRZGlDeCswQ20xIiwibWFjIjoiNWNlYzc3N2ZhMTUzOGJkYmYxMDA1YTFlZTMxY2RjNmVkZmNjMjkxNDMwMTNhZTcyZWI0MmZkNzU1NjA5ZDFiMCJ9', 'eyJpdiI6ImUrWENvY1EwWjJ4NlRmdDQxelBPYUE9PSIsInZhbHVlIjoicThZRmQ3ZU5cL1BjMkJ3b002Sm9sTkE9PSIsIm1hYyI6ImYzZGU5M2Y5Zjc0N2FhNDc0ZjhjODYxYjk2MGM0NzI0MTI4OTBiNDU3MjkzZDRhOTFkM2Y5MWQ5M2MzYWI1MTIifQ==', 'eyJpdiI6Ik1FdWpwdmthVEtmYWIxMTFiOXk3anc9PSIsInZhbHVlIjoiOVpIa1wvQ1wvbXA5WWdsTVhkTjNlVVR2YlNYZE5mY2VHQm9iN0JkQW5sclpBPSIsIm1hYyI6IjJkN2I5Njg1MTJlMmYzOTYzNzZkNzdiNTFjZWQ0YmM0OGYzOTg4NDRmMjRjODNiODdmMGMxYzM3NWQ0NGExN2YifQ==', 'eyJpdiI6IjhUbkdUXC9PMFZKcE1FSHdBMnRwMkhRPT0iLCJ2YWx1ZSI6IllLREpNXC9OT0RVMzRWbTZTXC9pQW5cL2c9PSIsIm1hYyI6ImI4ZjZmYzc2YTEzZjNmNTNjZGZmMmY2MmVkZTkyZTljNTcyNjM5MzBmZjAxNWU0NGVjYTAxYmZlOWJkMDg2YzkifQ==', 'eyJpdiI6Im9BY1wvSTUrQ211SVwvdWp2c2ZVZFVZZz09IiwidmFsdWUiOiJCb1EyRGp1N2lWUXNwU3N4V2ZoZUVnPT0iLCJtYWMiOiI3NzZjYTc0NTNkOTAzMzc5YzkyOTk5YTFiOGIxOWE5ZmQzNjkwMzQ1ZWU1ZTQ5NWI5ZDIzMTkwMDQ1M2M1NDI3In0=', 'eyJpdiI6Ild0S2I5R1JxMG53QzBPT0VaQWJDenc9PSIsInZhbHVlIjoibEFVVTBcLyt2aWdrXC9vRXRpV0c4RitRPT0iLCJtYWMiOiIyMzQzMDMwNDg1ODg2NDZlNDdlMGZmOTdmNDg3NDA2NDYxM2Q1NDlhNmJlNGZlZmJiMzllY2Y3NmE0OTAxOTNkIn0=', 'eyJpdiI6IlVmZ0h5ZVdNXC9wWGVaem41ek9SaTl3PT0iLCJ2YWx1ZSI6IlZ1RWlZdUxaNnVVZHJUV2hHYmVXUzVBdnYxOUdcL1YyMGw0OUdPbTBvbHljPSIsIm1hYyI6Ijg2MDdiMzhmNzQzMDM0MmI3ZWI3OTljMTU0OTA2ZTg3ZTllMTg0OGQ4OGI5ZTJmNjI0NTVmZWI5NGVjOTBmOTYifQ==', 'eyJpdiI6InNxaXdYWUhMK1JRbnBTcnRGVFJ6Ymc9PSIsInZhbHVlIjoibjc2UW1JdFo5dlNWaHZuM1NoRzdDZz09IiwibWFjIjoiMmM1MDM4NTRlM2M4YWVkYjExM2E1ZWViYWYxYjBmNmVmMjhlZjQ0YWNlMjM5ZmVhYWNkMTFhNTVkOTRhOGM2MyJ9', 1, NULL, '', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `commande_article`
--

CREATE TABLE `commande_article` (
  `rowid` int(11) NOT NULL,
  `commande_rowid` int(11) NOT NULL,
  `article_rowid` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantite_commande` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commande_article`
--

INSERT INTO `commande_article` (`rowid`, `commande_rowid`, `article_rowid`, `image`, `nom`, `format_quantite`, `format`, `provenance`, `allergene`, `perissable`, `bio`, `quebec`, `nom_marque`, `nom_categorie`, `nom_fournisseur`, `quantite_commande`, `prix`, `frais_fixe`, `frais_variable`, `prix_majore`, `economie`) VALUES
(24, 20, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 2, '19.92', '6.00', '1.00', NULL, NULL),
(25, 20, 5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 3, '14.88', '5.00', '0.00', NULL, NULL),
(26, 20, 8, 'public/products/images/yepAc3CrDZsNuFSxHG4h1hFbgYuqwsq7lI72nlSV.jpeg', 'Levure alimentaire en flocons', '500.00', 'g', 'États-Unis', NULL, 0, 0, 0, 'Ayam', 'Condiments et autres', 'Farine Quebec', 1, '13.89', '5.00', '1.00', NULL, NULL),
(27, 21, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 3, '19.92', '6.00', '1.00', NULL, NULL),
(28, 22, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 4, '4.95', '5.00', '0.00', NULL, NULL),
(29, 23, 2, 'public/products/images/aKxliPxIIy0PREv07R4UHxQsM6vQ5wgNI8O4V26t.jpeg', 'Couscous de blé entier tamisé', '1.00', 'Cuillère(s) à table', 'Canada', NULL, 0, 1, 0, 'Gerble', 'Aliments céréaliers', 'Aliments en vrac Quebec', 5, '4.91', '5.00', '0.00', NULL, NULL),
(30, 24, 3, 'public/products/images/N0AvyopsFGqYKEbFV5t1Pso4YSV20M190CNfcO40.jpeg', 'Flocons d’avoine', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Aliments céréaliers', 'Noix Quebec', 1, '3.43', '5.00', '0.50', NULL, NULL),
(31, 24, 5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 1, '14.88', '5.00', '0.00', NULL, NULL),
(32, 25, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 4, '19.92', '6.00', '1.00', NULL, NULL),
(33, 25, 6, 'public/products/images/fxtpyETXUMXk2OswDJ3T1SparCXp9c4caKKNOa8J.jpeg', 'Capuchons de chocolat 70%', '500.00', 'g', 'Fèves du Pérou, chocolat produit en Europe', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 1, '23.93', '7.00', '0.00', NULL, NULL),
(34, 25, 11, 'public/products/images/uwhju93QU1tzQNJO5xXD9iu1JDSzw7w00tRcRFEe.jpeg', 'Farine blanche tout-usage non blanchie', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Sacla', 'Farines', 'Farine Quebec', 1, '2.88', '5.00', '0.00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `rowid` int(11) NOT NULL,
  `rowid_theme` int(11) NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reponse` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`rowid`, `rowid_theme`, `question`, `reponse`) VALUES
(4, 1, 'Qu\'est-ce qu\'un regroupement d\'achat?', 'Un groupement d’achat est un regroupement d’entreprise, matérialisé ou non par la création d’un organisme dédié, qui a pour vocation d’optimiser les achats communs réalisés par ses membres.'),
(5, 1, 'Quand pourrais-je récupérer ma commande?', 'Des périodes de cueillette seront émises dès que la période de commande sera terminée. Les cueillettes seront affichés dans l\'onglet calendrier.'),
(6, 1, 'Êtes-vous un organisme à but lucratif?', 'Oui, aucun profit n\'est réalisé.');

-- --------------------------------------------------------

--
-- Table structure for table `fournisseur`
--

CREATE TABLE `fournisseur` (
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fournisseur`
--

INSERT INTO `fournisseur` (`nom_fournisseur`, `actif`) VALUES
('Aliments en vrac Quebec', 1),
('Chocolat Quebec', 1),
('Confitures et gelées Quebec', 1),
('Croustilles Quebec', 1),
('Détaillants de boissons Quebec', 1),
('Distributeurs et fabricants de mets chinois Quebec', 1),
('Dole Canada', 1),
('Eau embouteillée et en vrac Quebec', 1),
('Farine Quebec', 1),
('Fruits secs Quebec', 1),
('Glace Quebec', 1),
('Grossistes en aliments congelés Quebec', 1),
('Grossistes en café Quebec', 1),
('Grossistes et fabricants de produits naturels Quebec', 1),
('Huile d\'olive Quebec', 1),
('Huiles végétales Quebec', 1),
('Levure Quebec', 1),
('Maïs soufflé Quebec', 1),
('Malt et houblon Quebec', 1),
('Miel Quebec', 1),
('Noix Quebec', 1),
('Oeufs Quebec', 1),
('Produits alimentaires Quebec', 1),
('Produits biologiques Quebec', 1);

-- --------------------------------------------------------

--
-- Table structure for table `marque`
--

CREATE TABLE `marque` (
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marque`
--

INSERT INTO `marque` (`nom_marque`, `actif`) VALUES
('Ayam', 1),
('Babybio', 1),
('Bledina', 1),
('Bret-s', 1),
('Carrefour', 1),
('Citadelle', 1),
('Danone', 1),
('Dole', 1),
('Fleury-michon', 1),
('Gerble', 1),
('Herta', 1),
('Jardin-bio', 1),
('L-angelys', 1),
('Le-bon-paris', 1),
('Lea-nature', 1),
('Marque-repere', 1),
('Materne', 1),
('Monique-ranou', 1),
('Nestle', 1),
('Prana', 1),
('Propiedad-de', 1),
('Sacla', 1),
('Schar', 1),
('Sojasun', 1),
('Yoplait', 1);

-- --------------------------------------------------------

--
-- Table structure for table `panier_article`
--

CREATE TABLE `panier_article` (
  `rowid` int(11) NOT NULL,
  `rowid_panier` int(11) NOT NULL,
  `rowid_article` int(11) NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `panier_client`
--

CREATE TABLE `panier_client` (
  `rowid` int(11) NOT NULL,
  `rowid_client` int(11) NOT NULL,
  `actuel` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `panier_client`
--

INSERT INTO `panier_client` (`rowid`, `rowid_client`, `actuel`) VALUES
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `periode_article`
--

CREATE TABLE `periode_article` (
  `rowid` int(11) NOT NULL,
  `article_rowid` int(11) NOT NULL,
  `periode_rowid` int(11) NOT NULL,
  `quantite_commande` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_article`
--

INSERT INTO `periode_article` (`rowid`, `article_rowid`, `periode_rowid`, `quantite_commande`) VALUES
(15, 4, 1, 2),
(16, 5, 1, 3),
(17, 8, 1, 1),
(18, 4, 1, 3),
(19, 1, 5, 4),
(20, 2, 5, 5),
(21, 3, 5, 1),
(22, 5, 5, 1),
(23, 4, 5, 4),
(24, 6, 5, 1),
(25, 11, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `periode_commande`
--

CREATE TABLE `periode_commande` (
  `rowid` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_commande`
--

INSERT INTO `periode_commande` (`rowid`, `date_debut`, `date_fin`) VALUES
(1, '2019-02-01 00:00:00', '2019-02-19 12:00:00'),
(5, '2019-03-01 12:00:00', '2019-03-20 12:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `periode_plage`
--

CREATE TABLE `periode_plage` (
  `rowid` int(11) NOT NULL,
  `rowid_periode` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `emplacement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_plage`
--

INSERT INTO `periode_plage` (`rowid`, `rowid_periode`, `date_debut`, `date_fin`, `emplacement`) VALUES
(6, 1, '2019-02-21 13:00:00', '2019-02-21 15:00:00', 'CafÉcole - Pavillon des sciences, 3500 rue Courval');

-- --------------------------------------------------------

--
-- Table structure for table `personnaliser`
--

CREATE TABLE `personnaliser` (
  `rowid` int(11) NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banniere` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_desc` varchar(1028) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_entreprise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_copyright` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1_msg` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2_msg` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3_msg` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_theme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Nouveau Thème',
  `nom_article` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_article_plur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titre_onglet` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'RegroupÉcole',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_html` longtext COLLATE utf8mb4_unicode_ci,
  `termsofuse` longtext COLLATE utf8mb4_unicode_ci,
  `affichage_decimal` char(1) COLLATE utf8mb4_unicode_ci DEFAULT ',',
  `affichage_separator` char(1) COLLATE utf8mb4_unicode_ci DEFAULT '_',
  `actif` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personnaliser`
--

INSERT INTO `personnaliser` (`rowid`, `logo`, `banniere`, `footer_desc`, `footer_entreprise`, `footer_address`, `footer_city`, `footer_code_postal`, `footer_telephone`, `footer_copyright`, `car1`, `car2`, `car3`, `car1_msg`, `car2_msg`, `car3_msg`, `car1_link`, `car2_link`, `car3_link`, `nom_theme`, `nom_article`, `nom_article_plur`, `titre_onglet`, `icon`, `about_html`, `termsofuse`, `affichage_decimal`, `affichage_separator`, `actif`) VALUES
(1, 'public/themes/logos/oi1ctvvQkW7fhBQXCBoR4g1yBddTIPEkCqKNMmUW.png', 'public/themes/bannieres/iaBcDoXy7Tyz4LV9SMascATAAB1a0psO3sjyWndP.jpeg', 'Regroupement d\'achat offert par le département de Diététique du Cégep de Trois-Rivières.', 'Cégep de Trois-Rivières', '3500, rue de Courval', 'Trois-Rivières, QC', 'G9A 5E6', '8193761721', 'Charles-William Morency - Jessy Walker-Mailly - Zachary Veillette', 'public/themes/carrousel/J6orjMRDNBn0x1hFEjXYl8EXmUvlLewZtrtXDsJI.jpeg', NULL, NULL, 'Aller voir nos produit(s)!', NULL, NULL, 'boutique', '#', '#', 'Principal', 'produit', 'produit(s)', 'RegroupÉcole', 'public/themes/icons/VFL9fx4VgqBZNZrPuYcWbCMU4kaTguSdWFAGRyr9.ico', '<h1><span style=\"color:rgb(115,24,66);\"><span style=\"font-family:Helvetica;\">Regroup</span><span style=\"font-family:Helvetica;\">É</span><span style=\"font-family:Helvetica;\">cole</span></span></h1>\n\n\n\n<p class=\"MsoNormal\"><span style=\"font-family:Tahoma;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>\n\n<ul><li><a href=\"https://guide-alimentaire.canada.ca/fr/\" style=\"color:rgb(74,123,140);\">Guide alimentaire</a></li><li><span style=\"font-size:10.5pt;font-family:Tahoma;\"> dolor sit </span></li><li><span style=\"font-size:10.5pt;font-family:Tahoma;\">exercitation ullamc</span></li></ul>\n\n<p><img src=\"https://images.ecosia.org/1nONYrYMf0bwDh73RN02Z2dZacQ=/0x390/smart/http%3A%2F%2Fwww.lechodetroisrivieres.ca%2Fupload%2F12%2Fevenements%2F2018%2F7%2F342125%2Fle-cegep-de-trois-rivieres-celebre-ses-50-ans-001.jpg\" style=\"width:351.831px;float:left;\" class=\"note-float-left\" alt=\"http%3A%2F%2Fwww.lechodetroisrivieres.ca%2Fupload%2F12%2Fevenements%2F2018%2F7%2F342125%2Fle-cegep-de-trois-rivieres-celebre-ses-50-ans-001.jpg\"></p>\n\n<h2><span style=\"font-family:Tahoma;\">Cégep de Trois-Rivières</span></h2>\n\n<p><span style=\"font-family:Tahoma;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. </span><span style=\"font-family:Tahoma;font-size:1rem;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. </span><span style=\"font-family:Tahoma;font-size:1rem;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</span><br></p>\n\n\n\n\n\n<p><span style=\"font-family:Tahoma;\"> </span></p>\n\n<p><span style=\"font-family:Tahoma;\"><br></span></p>\n\n<p><span style=\"font-family:Tahoma;\"><br></span></p>', '<h2>Politique modèle de confidentialité.</h2>\n\n<h3>Introduction</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\"> Devant le développement des nouveaux outils de communication, il est nécessaire de porter une attention particulière à la protection de la vie privée. C’est pourquoi, nous nous engageons à respecter la confidentialité des renseignements personnels que nous collectons.</p>\n\n<h3>Collecte des renseignements personnels</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous collectons les renseignements suivants (lors du dépôt d’une plainte) :</p>\n\n<ul><li>Nom</li><li>Prénom</li><li>Adresse postale</li><li>Code postal</li><li>Adresse électronique</li><li>Numéro de téléphone / télécopieur</li><li>Genre / Sexe</li></ul>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Les renseignements personnels que nous collectons sont recueillis au travers de formulaires. Nous utilisons également, comme indiqué dans la section suivante, des fichiers témoins et/ou journaux pour réunir des informations vous concernant.</p>\n\n<h3>Formulaires et interactivité:</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Vos renseignements personnels sont collectés par le biais de formulaire, à savoir :</p>\n\n<ul><li>Formulaire de dépôt de plainte</li></ul>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous utilisons les renseignements ainsi collectés pour les finalités suivantes :</p>\n\n<ul><li>Suivi de la plainte</li><li>Informations</li><li>Statistiques</li></ul>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous utilisons les renseignements ainsi collectés pour les finalités suivantes :</p>\n\n<ul><li>Forum ou aire de discussion</li><li>Commentaires</li><li>Correspondance</li><li>Informations</li></ul>\n\n<h3>Fichiers journaux et témoins</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous recueillons certaines informations par le biais de fichiers journaux (log file) et de fichiers témoins (cookies). Il s’agit principalement des informations suivantes :</p>\n\n<ul><li>Adresse IP</li><li>Système d’exploitation</li><li>Pages visitées et requêtes</li><li>Heure et jour de connexion</li><li>Lieu de connexion</li></ul>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Ces fichiers sont utilisés pour :</p>\n\n<ul><li>Amélioration du service</li><li>Statistique</li></ul>\n\n<h3>Droit d’opposition et de retrait</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous nous engageons à vous offrir un droit d’opposition et de retrait quant à vos renseignements personnels.  Le droit d’opposition s’entend comme étant la possiblité offerte aux internautes de refuser que leurs renseignements personnels soient utilisées à certaines fins mentionnées lors de la collecte.</p>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Le droit de retrait s’entend comme étant la possiblité offerte aux internautes de demander à ce que leurs renseignements personnels ne figurent plus, par exemple, dans une liste de diffusion.</p>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Pour pouvoir exercer ces droits, vous pouvez contacter :</p>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Geneviève Fortin<br>1000, rue Fullum (Bureau A.208)<br>Montréal (Québec)<br>H2K 3L7 <br>Courriel : <a href=\"mailto:genevieve.fortin@conseildepresse.qc.ca\" style=\"color:rgb(149,31,43);\">genevieve.fortin@conseildepresse.qc.ca</a><br>Téléphone : 514 529-2818 (poste 3)</p>\n\n<h3>Droit d’accès</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous nous engageons à reconnaître un droit d’accès et de rectification aux personnes concernées désireuses de consulter ou modifier les informations les concernant.  L’exercice de ce droit se fera par téléphone, par courriel ou par la poste auprès de : </p>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Geneviève Fortin<br>1000, rue Fullum (Bureau A.208)<br>Montréal (Québec)<br>H2K 3L7 <br>Courriel : <a href=\"mailto:genevieve.fortin@conseildepresse.qc.ca\" style=\"color:rgb(149,31,43);\">genevieve.fortin@conseildepresse.qc.ca</a><br>Téléphone : 514 529-2818 (poste 3)</p>\n\n<h3>Sécurité</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Les renseignements personnels que nous collectons sont conservés dans un environnement sécurisé. Les personnes travaillant pour nous sont tenues de respecter la confidentialité de vos informations.  Pour assurer la sécurité de vos renseignements personnels, nous avons recours aux mesures suivantes :</p>\n\n<ul><li>Gestion des accès – personne autorisée</li><li>Gestion des accès – personne concernée</li><li>Sauvegarde informatique</li><li>Identifiant / mot de passe</li><li>Pare-feu (Firewalls)</li></ul>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous nous engageons à maintenir un haut degré de confidentialité en intégrant les dernières innovations technologiques permettant d’assurer la confidentialité de vos renseignements. Toutefois, comme aucun mécanisme n’offre une sécurité maximale, une part de risque est toujours présente lorsque l’on utilise Internet pour transmettre des renseignements personnels.</p>\n\n<h3>Législation</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous nous engageons à respecter les dispositions législatives énoncées dans :  L.R.Q., chapitre P-39.1</p>', ',', '_', 1);

-- --------------------------------------------------------

--
-- Table structure for table `recette`
--

CREATE TABLE `recette` (
  `rowid` int(11) NOT NULL,
  `article_rowid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recette`
--

INSERT INTO `recette` (`rowid`, `article_rowid`, `name`, `link`) VALUES
(17, 1, 'Amarante en taboulé', 'http://chefsimon.com/gourmets/cuisinealcaline/recettes/amarante-en-taboule'),
(18, 1, 'Marrant petit déjeuner d\'amarante!', 'http://brutalimentation.ca/2014/03/12/marrant-petit-dejeuner-damarante/'),
(19, 24, 'Gâteau froid aux ananas', 'https://www.recettes.qc.ca/recettes/recette/gateau-froid-aux-ananas-3864');

-- --------------------------------------------------------

--
-- Table structure for table `unite_mesure`
--

CREATE TABLE `unite_mesure` (
  `nom_unite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `unite_mesure`
--

INSERT INTO `unite_mesure` (`nom_unite`, `actif`) VALUES
('Cuillère(s) à table', 1),
('Cuillère(s) à thé', 1),
('g', 1),
('galon(s)', 1),
('Kg', 1),
('L', 1),
('lb', 1),
('mg', 1),
('ml', 1),
('oz', 1),
('pinte(s)', 1),
('tasse(s)', 1),
('test', 0),
('Unité(s)', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_administrateurs`
-- (See below for the actual view)
--
CREATE TABLE `v_stat_administrateurs` (
`data` bigint(21)
,`label` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_categories`
-- (See below for the actual view)
--
CREATE TABLE `v_stat_articles_categories` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_fournisseurs`
-- (See below for the actual view)
--
CREATE TABLE `v_stat_articles_fournisseurs` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_marques`
-- (See below for the actual view)
--
CREATE TABLE `v_stat_articles_marques` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_populaires`
-- (See below for the actual view)
--
CREATE TABLE `v_stat_articles_populaires` (
`label` varchar(255)
,`data` decimal(32,0)
,`dataset_date` datetime
,`dataset_month` int(2)
,`dataset_month_full` varchar(9)
,`dataset_year` int(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_nb_client_actif`
-- (See below for the actual view)
--
CREATE TABLE `v_stat_nb_client_actif` (
`nb` bigint(21)
,`dataset_year` int(4)
,`dataset_month` int(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_tendance_commandes`
-- (See below for the actual view)
--
CREATE TABLE `v_stat_tendance_commandes` (
`data` bigint(21)
,`dataset_month` int(2)
,`dataset_month_full` varchar(9)
,`dataset_year` int(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_utilisateurs`
-- (See below for the actual view)
--
CREATE TABLE `v_stat_utilisateurs` (
`data` bigint(21)
,`label` varchar(7)
);

-- --------------------------------------------------------

--
-- Structure for view `v_stat_administrateurs`
--
DROP TABLE IF EXISTS `v_stat_administrateurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`concep5tionwebex`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_administrateurs`  AS  select count(0) AS `data`,'Webmestre' AS `label` from `admin` where (`admin`.`master` = 1) union select count(0) AS `data`,'Enseignant' AS `label` from `admin` where (`admin`.`super` = 1) union select count(0) AS `data`,'Étudiant' AS `label` from `admin` where (`admin`.`super` = 0) ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_categories`
--
DROP TABLE IF EXISTS `v_stat_articles_categories`;

CREATE ALGORITHM=UNDEFINED DEFINER=`concep5tionwebex`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_categories`  AS  select count(0) AS `data`,`article`.`nom_categorie` AS `label` from `article` group by `article`.`nom_categorie` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_fournisseurs`
--
DROP TABLE IF EXISTS `v_stat_articles_fournisseurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`concep5tionwebex`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_fournisseurs`  AS  select count(0) AS `data`,`article`.`nom_fournisseur` AS `label` from `article` group by `article`.`nom_fournisseur` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_marques`
--
DROP TABLE IF EXISTS `v_stat_articles_marques`;

CREATE ALGORITHM=UNDEFINED DEFINER=`concep5tionwebex`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_marques`  AS  select count(0) AS `data`,`article`.`nom_marque` AS `label` from `article` group by `article`.`nom_marque` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_populaires`
--
DROP TABLE IF EXISTS `v_stat_articles_populaires`;

CREATE ALGORITHM=UNDEFINED DEFINER=`concep5tionwebex`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_populaires`  AS  select `commande_article`.`nom` AS `label`,sum(`commande_article`.`quantite_commande`) AS `data`,`commande`.`date_commande` AS `dataset_date`,month(`commande`.`date_commande`) AS `dataset_month`,monthname(`commande`.`date_commande`) AS `dataset_month_full`,year(`commande`.`date_commande`) AS `dataset_year` from (`commande_article` left join `commande` on((`commande`.`rowid` = `commande_article`.`commande_rowid`))) where commande.paye = 1 group by `commande_article`.`nom`,cast(`commande`.`date_commande` as date) order by month(`commande`.`date_commande`) desc,sum(`commande_article`.`quantite_commande`) desc ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_nb_client_actif`
--
DROP TABLE IF EXISTS `v_stat_nb_client_actif`;

CREATE ALGORITHM=UNDEFINED DEFINER=`concep5tionwebex`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_nb_client_actif`  AS  select count(distinct `client`.`rowid`) AS `nb`,year(`commande`.`date_commande`) AS `dataset_year`,month(`commande`.`date_commande`) AS `dataset_month` from (`client` join `commande`) where ((`client`.`rowid` = `commande`.`rowid_client`) and (`commande`.`paye` = 1)) group by year(`commande`.`date_commande`),month(`commande`.`date_commande`) order by year(`commande`.`date_commande`) desc,month(`commande`.`date_commande`) ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_tendance_commandes`
--
DROP TABLE IF EXISTS `v_stat_tendance_commandes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`concep5tionwebex`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_tendance_commandes`  AS  select count(`commande`.`rowid`) AS `data`,month(`commande`.`date_commande`) AS `dataset_month`,monthname(`commande`.`date_commande`) AS `dataset_month_full`,year(`commande`.`date_commande`) AS `dataset_year` from `commande` where (`commande`.`paye` = 1) group by year(`commande`.`date_commande`),month(`commande`.`date_commande`) order by year(`commande`.`date_commande`) desc,month(`commande`.`date_commande`) ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_utilisateurs`
--
DROP TABLE IF EXISTS `v_stat_utilisateurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`concep5tionwebex`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_utilisateurs`  AS  select count(0) AS `data`,'Actif' AS `label` from `client` where (`client`.`actif` = 1) union select count(0) AS `data`,'Inactif' AS `label` from `client` where (`client`.`actif` = 0) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `admin_board`
--
ALTER TABLE `admin_board`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `rowid_admin` (`username_admin`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `article_fk0` (`nom_marque`),
  ADD KEY `article_fk1` (`nom_categorie`),
  ADD KEY `article_fk2` (`nom_fournisseur`),
  ADD KEY `article_unite` (`format`);

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`nom_categorie`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`rowid`);

--
-- Indexes for table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `fk_periode_commande` (`rowid_periode`),
  ADD KEY `fk_client_commande` (`rowid_client`);

--
-- Indexes for table `commande_article`
--
ALTER TABLE `commande_article`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `fk_commande_article` (`commande_rowid`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `fk_theme` (`rowid_theme`);

--
-- Indexes for table `fournisseur`
--
ALTER TABLE `fournisseur`
  ADD PRIMARY KEY (`nom_fournisseur`);

--
-- Indexes for table `marque`
--
ALTER TABLE `marque`
  ADD PRIMARY KEY (`nom_marque`);

--
-- Indexes for table `panier_article`
--
ALTER TABLE `panier_article`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `panier_article_fk0` (`rowid_panier`),
  ADD KEY `panier_article_fk1` (`rowid_article`);

--
-- Indexes for table `panier_client`
--
ALTER TABLE `panier_client`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `panier_client_fk1` (`rowid_client`);

--
-- Indexes for table `periode_article`
--
ALTER TABLE `periode_article`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `article_rowid` (`article_rowid`),
  ADD KEY `periode_rowid` (`periode_rowid`);

--
-- Indexes for table `periode_commande`
--
ALTER TABLE `periode_commande`
  ADD PRIMARY KEY (`rowid`);

--
-- Indexes for table `periode_plage`
--
ALTER TABLE `periode_plage`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `periode_plage_fk0` (`rowid_periode`);

--
-- Indexes for table `personnaliser`
--
ALTER TABLE `personnaliser`
  ADD PRIMARY KEY (`rowid`);

--
-- Indexes for table `recette`
--
ALTER TABLE `recette`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `article_rowid` (`article_rowid`);

--
-- Indexes for table `unite_mesure`
--
ALTER TABLE `unite_mesure`
  ADD PRIMARY KEY (`nom_unite`),
  ADD KEY `nom_unite` (`nom_unite`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_board`
--
ALTER TABLE `admin_board`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `commande`
--
ALTER TABLE `commande`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `commande_article`
--
ALTER TABLE `commande_article`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `panier_article`
--
ALTER TABLE `panier_article`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `panier_client`
--
ALTER TABLE `panier_client`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `periode_article`
--
ALTER TABLE `periode_article`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `periode_commande`
--
ALTER TABLE `periode_commande`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `periode_plage`
--
ALTER TABLE `periode_plage`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `personnaliser`
--
ALTER TABLE `personnaliser`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `recette`
--
ALTER TABLE `recette`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_board`
--
ALTER TABLE `admin_board`
  ADD CONSTRAINT `fk_admin` FOREIGN KEY (`username_admin`) REFERENCES `admin` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_fk0` FOREIGN KEY (`nom_marque`) REFERENCES `marque` (`nom_marque`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk1` FOREIGN KEY (`nom_categorie`) REFERENCES `categorie` (`nom_categorie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk2` FOREIGN KEY (`nom_fournisseur`) REFERENCES `fournisseur` (`nom_fournisseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_unite` FOREIGN KEY (`format`) REFERENCES `unite_mesure` (`nom_unite`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_client_commande` FOREIGN KEY (`rowid_client`) REFERENCES `client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_commande` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande_article`
--
ALTER TABLE `commande_article`
  ADD CONSTRAINT `fk_commande_article` FOREIGN KEY (`commande_rowid`) REFERENCES `commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `faq`
--
ALTER TABLE `faq`
  ADD CONSTRAINT `fk_theme` FOREIGN KEY (`rowid_theme`) REFERENCES `personnaliser` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `panier_article`
--
ALTER TABLE `panier_article`
  ADD CONSTRAINT `fk_article_panier` FOREIGN KEY (`rowid_article`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_panier_client` FOREIGN KEY (`rowid_panier`) REFERENCES `panier_client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `panier_client`
--
ALTER TABLE `panier_client`
  ADD CONSTRAINT `fk_client` FOREIGN KEY (`rowid_client`) REFERENCES `client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_article`
--
ALTER TABLE `periode_article`
  ADD CONSTRAINT `fk_article_periode` FOREIGN KEY (`article_rowid`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_article` FOREIGN KEY (`periode_rowid`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_plage`
--
ALTER TABLE `periode_plage`
  ADD CONSTRAINT `fk_periode` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
