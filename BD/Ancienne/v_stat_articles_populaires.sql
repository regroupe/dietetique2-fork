SELECT 
	commande_article.nom as label,
    SUM(commande_article.quantite_commande) as data,
    periode_commande.date_debut as dataset_date,
    MONTH(periode_commande.date_debut) as dataset_month,
    MONTHNAME(periode_commande.date_debut) as dataset_month_full,
    YEAR(periode_commande.date_debut) as dataset_year
FROM
	commande_article
    LEFT JOIN commande on commande.rowid = commande_article.commande_rowid
    LEFT JOIN periode_commande on periode_commande.rowid = commande.rowid_periode
GROUP BY
	commande_article.nom,
    DATE(periode_commande.date_debut)
ORDER BY
	periode_commande.date_debut DESC,
	SUM(commande_article.quantite_commande) DESC