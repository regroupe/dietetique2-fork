-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 08, 2019 at 01:32 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diet`
--
CREATE DATABASE IF NOT EXISTS `diet` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `diet`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `super` tinyint(1) NOT NULL,
  `master` tinyint(1) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_modif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `change_pass` tinyint(1) NOT NULL DEFAULT '0',
  `actif` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`super`, `master`, `username`, `password`, `last_modif`, `change_pass`, `actif`) VALUES
(0, 0, 'admin', '$2y$10$qejIbdueBlAOk9Fxger.su33bTctWlR6CKcqZ1iOnz6/GCZvdFQfO', 'master', 0, 1),
(1, 1, 'master', '$2y$10$al.OCHpRFjQKFrkosV4vXuYv3uQnwi.Tyl8pYoKFtdcBUlt.gPxW.', '', 0, 1),
(1, 0, 'super', '$2y$10$GuKNlmDXvss6HWnDS0PCxut.TaD33w7Xzi8BO71KbCB9Q794HzI/m', 'master', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite_minimum` int(11) DEFAULT NULL,
  `quantite_maximum` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `frais_emballage` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL,
  `description` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarque` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valeur_nutritive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `piece_jointe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '0',
  `actif_vente` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rowid`),
  KEY `article_fk0` (`nom_marque`),
  KEY `article_fk1` (`nom_categorie`),
  KEY `article_fk2` (`nom_fournisseur`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`rowid`, `image`, `nom`, `format_quantite`, `format`, `provenance`, `allergene`, `perissable`, `bio`, `quebec`, `nom_marque`, `nom_categorie`, `nom_fournisseur`, `quantite_minimum`, `quantite_maximum`, `prix`, `frais_fixe`, `frais_variable`, `frais_emballage`, `prix_majore`, `economie`, `description`, `remarque`, `valeur_nutritive`, `piece_jointe`, `created_by`, `actif`, `actif_vente`) VALUES
(1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.95', '5.00', '0.00', '0.00', NULL, NULL, 'Excellente pseudo-céréale naturellement sans gluten!', NULL, 'public/products/nutritional/GSSLKaSc5PSg4gmaRIdSnK3FSTehvSWdeJs5MkIE.png', NULL, 'master', 1, 1),
(2, 'public/products/images/aKxliPxIIy0PREv07R4UHxQsM6vQ5wgNI8O4V26t.jpeg', 'Couscous de blé entier tamisé', '1.00', 'kg', 'Canada', NULL, 0, 1, 0, 'Gerble', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.91', '5.00', '0.00', '0.00', NULL, NULL, 'Pour faire d’excellents taboulés… Yé!', NULL, 'public/products/nutritional/MthROLiRbthvhEaQKDR4s1qPV5CDrQ7Itf2rALBG.png', NULL, 'master', 1, 1),
(3, 'public/products/images/N0AvyopsFGqYKEbFV5t1Pso4YSV20M190CNfcO40.jpeg', 'Flocons d’avoine', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Aliments céréaliers', 'Noix Quebec', 80, 100, '3.43', '5.00', '0.50', '0.00', NULL, NULL, 'Cette avoine est roulée et torréfiée à sec avant d’être chauffée à la vapeur à 200°F pour être roulée. C’est ce qui distingue ces flocons de ceux des autres rares entreprises qui en font au Québec, et c’est ce qui leur confère ce goût de gras naturel de l’avoine.C’est ce qui leur permet également de hausser sa durée de conservation à un an sans problème.', NULL, 'public/products/nutritional/8I7DFLv4nW6zmVwVkWAUkyr5gtejjhzzY6SgMYlH.png', NULL, 'master', 1, 1),
(4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 50, 50, '19.92', '6.00', '1.00', '0.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes, dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température. Pour les desserts cuits comme les gâteaux ou les brownies, nous recommandons plutôt le cacao en poudre; le goût du cacao cru serait trop prononcé pour de tels desserts.', NULL, 'public/products/nutritional/p7PKD2k1iGJ1CLL5igoP8Sv2GyxvFHIJTdZPFBKP.png', NULL, 'master', 1, 1),
(5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 40, 100, '14.88', '5.00', '0.00', '0.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao en poudre pour les desserts cuits comme les gâteaux ou les brownies, pour lesquels le goût du cacao cru serait trop prononcé. Dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température, nous recommandons plutôt le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes.', NULL, 'public/products/nutritional/SIljBYRTECmTu0cARK3YJQGdaiwOMKzmGB3HEH78.png', NULL, 'master', 1, 1),
(6, 'public/products/images/fxtpyETXUMXk2OswDJ3T1SparCXp9c4caKKNOa8J.jpeg', 'Capuchons de chocolat 70%', '500.00', 'g', 'Fèves du Pérou, chocolat produit en Europe', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 150, 200, '23.93', '7.00', '0.00', '0.00', NULL, NULL, 'Pépites de chocolat géantes, pour cuisiner ou manger telles quelles comme délice du moment. Environ 5 fois la grosseur des pépites de chocolat, elles font d’excellents desserts avec de gros morceaux de chocolat qui en mettent plein la dent!', NULL, 'public/products/nutritional/Xx8RvLJaxYXeNYale45l5G4WCbvOKrv54PdIUmiG.png', NULL, 'master', 1, 1),
(7, 'public/products/images/TmxisxJI9ocq2Ip8TQriNaer4WypYgn9Enuy9HBb.jpeg', 'Café La Montréalaise Santropol', '500.00', 'g', 'Amérique centrale et du sud', NULL, 0, 1, 1, 'Materne', 'Café', 'Grossistes en café Quebec', 65, 125, '14.37', '5.00', '2.00', '0.00', NULL, NULL, 'Café gourmet biologique, équitable, en grain et de torréfaction française. Un riche mélange corsé aux reflets de marron et d’ébène.', NULL, 'public/products/nutritional/stjSP5cjaLxUFLYOdMhmaB2M6t1cy6gsZGsdzdJh.png', NULL, 'master', 1, 1),
(8, 'public/products/images/yepAc3CrDZsNuFSxHG4h1hFbgYuqwsq7lI72nlSV.jpeg', 'Levure alimentaire en flocons', '500.00', 'g', 'États-Unis', NULL, 0, 0, 0, 'Ayam', 'Condiments et autres', 'Farine Quebec', 80, 120, '13.89', '5.00', '1.00', '0.00', NULL, NULL, 'Levure produite par fermentation naturelle, aucune source animale ni synthétique.', NULL, 'public/products/nutritional/oMVZlewVITN6Jx7GfMeiFzOUOr9hQe4YXg5vt0o4.png', NULL, 'master', 1, 1),
(9, 'public/products/images/7AzHZCs5UGaVzjEJETAw9z1XquORt6uMNrRyF99U.jpeg', 'Miso soya et riz sans gluten', '500.00', 'g', 'Québec', NULL, 1, 1, 1, 'Bledina', 'Condiments et autres', 'Confitures et gelées Quebec', 100, 100, '13.83', '5.00', '0.00', '0.00', NULL, NULL, 'Le miso biologique Massawipi non pasteurisé de soya et de riz est un aliment sans gluten très polyvalent; son goût est doux, rond en bouche et légèrement salé. Il est le résultat d’une fermentation naturelle (non forcée) d’au moins 2 ans. Pour sa fabrication, nous n’utilisons que des ingrédients certifiés biologiques garantis sans OGM.', NULL, 'public/products/nutritional/Ty12FzodDT1kbWEeKsEnqlXZwim1KggiKytU5P9l.png', NULL, 'master', 1, 1),
(10, 'public/products/images/I6XjF0Hwgg9sIX0tqhw7rrjiacSVk3oHcqy0DeXV.jpeg', 'Moutarde de Dijon', '1.00', 'kg', 'Québec', NULL, 1, 1, 1, 'Herta', 'Condiments et autres', 'Produits alimentaires Quebec', 20, 20, '8.98', '5.00', '0.00', '0.00', NULL, NULL, 'Moutarde crue, sans sucre ajouté, sans agents de conservation, sans gluten. Une moutarde de Dijon avec une belle texture crémeuse et une touche de piquant qui la rend irremplaçable! Pour les amateurs de moutarde forte!', NULL, NULL, NULL, 'master', 1, 1),
(11, 'public/products/images/uwhju93QU1tzQNJO5xXD9iu1JDSzw7w00tRcRFEe.jpeg', 'Farine blanche tout-usage non blanchie', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Sacla', 'Farines', 'Farine Quebec', 60, 70, '2.88', '5.00', '0.00', '0.00', NULL, NULL, 'La farine blanche tout usage non blanchie est tamisée pour en retirer tout le son et ne subit aucun traitement de blanchiment. Elle est produite à partir de blé dur de printemps biologique, et les grains sont moulus sur cylindres. Idéal pour la boulangerie et la pâtisserie.', NULL, 'public/products/nutritional/6cCibCA8qr7SRrQYZCsZimR5Ly3GKJUWB1d9bm9o.png', NULL, 'master', 1, 1),
(12, 'public/products/images/n5nqMXBrO1Lx3XhZ1l8ZCpQA5D7g08p9WHQq9tuB.jpeg', 'Farine d’épeautre', '200.00', 'g', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Farines', 'Farine Quebec', 25, 30, '4.60', '5.00', '0.00', '0.00', NULL, NULL, 'Depuis mai 2018, cette farine est tamisée de sorte à en extraire 10 % du son, ce qui lui procure un meilleur rendement en pâtisserie et en boulangerie qu’une farine intégrale.', NULL, NULL, NULL, 'master', 1, 1),
(13, 'public/products/images/1TpWugpuoiLsRiiFu3ToTpzfZcBhz8dRDzXDL5UL.jpeg', 'Farine de blé intégrale', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Fleury-michon', 'Farines', 'Farine Quebec', 80, 80, '2.88', '3.50', '0.00', '0.00', NULL, NULL, 'Cette farine de blé intégrale contient toutes la parties du blé. Le son et les rémoulures donnent plus de texture à vos pâtisseries et à vos pains.', NULL, 'public/products/nutritional/PSd1hLFB885sjyTw5jfEZAOjZTPm1zGah4LvzJ8x.jpeg', NULL, 'master', 1, 1),
(14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 20, 40, '13.94', '5.00', '1.00', '0.00', NULL, NULL, NULL, 'Il est possible de retrouver des noyaux dans le produit.', 'public/products/nutritional/0K5UCVCywUYqmbWHv79g8rJiwUQzyEKXh5BX6e09.png', NULL, 'master', 1, 1),
(15, 'public/products/images/17YmeaiQPt8j48kzvCWmxRqXAzBKtDD3r7r0TetF.jpeg', 'Canneberges séchées au jus de pommes', '1.00', 'kg', 'Québec', NULL, 1, 1, 0, 'Citadelle', 'Fruits séchés', 'Produits biologiques Quebec', 175, 200, '18.66', '5.00', '0.00', '0.00', NULL, NULL, 'Ces canneberges séchées sont préparées à partir de canneberges (Vaccinium macrocarpon) matures de première qualité et infusées dans une solution de jus de pomme concentré. Elles sont tendres et juteuses et présentent le goût acidulé et légèrement sucré caractéristique de la canneberge. En faisant sécher les fruits plus longtemps et à plus basse température, Citadelle parvient à préserver la saveur des canneberges ainsi qu’une quantité maximale de nutriments, pour le bonheur de nos palais et la santé de notre corps.', 'Le tout est 100% naturel, tant le procédé de conservation que les canneberges elles-mêmes auxquelles aucun agent stabilisant, ni colorant, ni glycérine n’est ajouté.', NULL, NULL, 'master', 1, 1),
(16, 'public/products/images/kPZuCChkJhrxRQ0YOhKAqSt6MaWVfGeLjc5eqwFD.jpeg', 'Dattes Deglet dénoyautées', '1.00', 'kg', 'Tunisie', NULL, 1, 1, 0, 'Prana', 'Fruits séchés', 'Produits biologiques Quebec', 80, 80, '12.77', '5.00', '0.00', '0.00', NULL, NULL, 'Youpi! Savoureuse et sans noyau, bien que, étant donné que le processus de dénoyautage est automatisé, il est possible de retrouver quelques noyaux parmi ces dattes.', NULL, 'public/products/nutritional/qQDAsd2DoU6n1C0ABNUWrrR14Qz0yj6xvi2IWj9W.png', NULL, 'master', 1, 1),
(22, NULL, 'test', '1.00', 'unité(s)', 'test', NULL, 0, 0, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 1, 1, '1.00', '1.00', '1.00', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, 'master', 1, 0),
(23, NULL, 'aaaa', '1.00', 'unité(s)', '1', NULL, 0, 0, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 1, 1, '1.00', '1.00', '1.00', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, 'master', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_categorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`nom_categorie`, `actif`) VALUES
('Aliments céréaliers', 1),
('Cacao et ses acolytes', 1),
('Café', 1),
('Condiments et autres', 1),
('Farines', 1),
('Fruits séchés', 1),
('Graines à germer', 1),
('Huiles et vinaigres', 1),
('Légumineuses', 1),
('Livres', 1),
('Noix et graines', 1),
('Pâtes', 1),
('Produits québécois', 1),
('Sacs écolo', 1),
('Superaliments', 1);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`rowid`, `email`, `password`, `telephone`, `poste`, `nom`, `prenom`, `adresse`, `no_app`, `code_postal`, `ville`, `pays`, `province`, `actif`) VALUES
(1, 'jessy@cegep.com', '$2y$10$BkAGbBom8y9bQz8LeD9L3eRIAKDhQsOsifdr114hbWIJyLEVVF8VC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(2, 'zach@cegep.com', '$2y$10$C3Kao9./HOM/HDABXssW0OS9d0cPDXOGdTb6M5TIl9czDfULJsmNW', 'eyJpdiI6IkRBc1wvZGJuTm5nMDU2ekE5WDV4SHZRPT0iLCJ2YWx1ZSI6Im5vUm11bUpDRlJGVU5sdVhLMGJXVm9OSmU1Z1MyWjEySzFaV1pEWTI0Tzg9IiwibWFjIjoiZmI2NDg2NWRhY2RkMmI5MmY2ZmY5ZWE3ZWQwMWZkOTk1MDQ1NjE4MTllN2RkY2ExNGIyMDZiY2UxNGRlNjZiMyJ9', 'eyJpdiI6IjJPOW1IbGxvNCt0SlNtRThzQmNOanc9PSIsInZhbHVlIjoiZ2dFMGpxTVo2Z2p1Q0p4R1JPUDc0UT09IiwibWFjIjoiZDNiZDQ4MWZmNTNlMzA3ZTRiYTMyMTA5NGU1ZjU2NjA0ZjZlNTRhNmVlZDdhMDUzZjRlMzIzMTI0ODUxN2ExMyJ9', 'eyJpdiI6IkpTaUk3NDBiWVNcL0dpRUlweTBWeTNnPT0iLCJ2YWx1ZSI6InBNVzFxOGxGb0F5S2JzWFF2bEZXRmc9PSIsIm1hYyI6IjA4YjU4NDhjODRhNTY4M2Q1Y2UzYjM4OGQ3YmU1MzY3NzdhNDJiNzkyNmQxNmJkYmEzYzhhZGFjNDlkOGUwZjMifQ==', 'eyJpdiI6ImI5eGl2REUwREY2ZzVkZUJnbUhnQ2c9PSIsInZhbHVlIjoieVdRZk80M1RWTWUxM3FuXC9qNlpaXC9BPT0iLCJtYWMiOiIwZDcyN2NiZjU1MWZjYzc0ZGE1MWNkOWNlZDQ0Mzk0YzJlYzk3NDcwZTVhZmYwMzFkYmViYzlkZDQ2YjZhOGNhIn0=', 'eyJpdiI6IlgyRnpJRStHaWIxTDk0NGlhYVwvSlBBPT0iLCJ2YWx1ZSI6ImpuYVd5Y1hjeDE2N2ZPRTVhUDFqNXc9PSIsIm1hYyI6IjM5NjdlYTAzNmI3MDA2NTI4MjU4YjVjNWJlNmZmNTlmY2FmNTEyOTYyYzM0YjhlMTczNTgzMzhjYWI4OGM3N2UifQ==', 'eyJpdiI6InptR0lCK01NNnBLZUdLXC92TE15YlJRPT0iLCJ2YWx1ZSI6InZWVVdpazlzajBzZTN4S3ZqOE1VNVE9PSIsIm1hYyI6IjdhMjY1ZWIxY2Y4MzFjZDc0ZmJhZWE2NzgyMWQ5M2JmZTBlNjgyOTFjODI1ODE0NWJjODEwZWFjNTk4Njc1ODQifQ==', 'eyJpdiI6Ik5XczhPZkg2S0RvRWk2YW5pVW5LK1E9PSIsInZhbHVlIjoiNjZieHBzU3Jtbk54RjNLXC9QMjVRdWc9PSIsIm1hYyI6Ijg4ZTExMTUxZWI2MGVmNzFhOGU4ZWIyMjQ1YjYyNDU3MzI4NGQ2MGJkODNmYTRjNDA3ZmUxNmI0NTVkYjQ2MmYifQ==', 'eyJpdiI6ImpOKzBxTlNiK3dHVGtJZG92ckdrY1E9PSIsInZhbHVlIjoiaDBFZVZobUo4M00wUjdrUDZRYUR1Zz09IiwibWFjIjoiNGZiNjEzMzlhNGMzNWMxMzQ0YzRjOTQ3ODU2NzJmMTU1ZDIxOGZhZjFlYWYzZmZhZGIxNWNkYmJiMzg2ODU3OCJ9', 'eyJpdiI6IlRCK0NcLzN3OWtjbGY2aXpRVkNRaDN3PT0iLCJ2YWx1ZSI6ImdNbmhwNzZBTTFsXC8zbUY2R0ltNlhBPT0iLCJtYWMiOiI1NDQ5NGRlOGY2MzIyZjk1NGRlMjMzYmFlMTIwMGIwYTFlNmUwMTljNjNlMzhiODlhYmQ2MGEzZjZmZmQzNmY2In0=', 'eyJpdiI6IlNXaGZoUHoyakc5NHdPMlNjZ2ZtUUE9PSIsInZhbHVlIjoiUlNia0E4YVR2cm42T0NXeWRXbEZQZz09IiwibWFjIjoiNjI5YTgwNDRmZTBlMTM3YzEwYjEzYzUwNTljMjUxYWM1YTNjOTRjZjQzZjkxNTQ1Mzk0ZWNjN2NjNjllMTNkZiJ9', 1),
(3, 'charles@cegep.com', '$2y$10$wQ5TKSBn3Ev.7abcv/jld..Akftaar1kXWzLRK2qbDvOOrBAJ.9U6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(4, 'jean@jean.com', '$2y$10$aXV75HlGGp8iu5qReeWQpuiYs9NJyzeEtRHl.hV3fmzHJM.Bi1xvi', 'eyJpdiI6ImN2dDR1aTBxWnQ3SWc2a09RZnpWaVE9PSIsInZhbHVlIjoiVFAxNFYwdE0rNDlKVGZ0SFdZZzBoRkszY0F2RlwvM25XMHNnMEVaN3d2bE09IiwibWFjIjoiZmY5MDEzN2UxZmE0ZjRjOTg4MjVkZDIzNGZjNmQzYzY0NWFmYzJmMGUyY2MyMzQxNTkxNDg3ZWQzMjJlN2M2NCJ9', 'eyJpdiI6ImtxdzVjWEdENnY5Z1RZaVlGZVFpcHc9PSIsInZhbHVlIjoiUjFteXZXZTA3OWRwQTdNeUg3R25hQT09IiwibWFjIjoiN2VhNWFiYmFjN2U1MmViNjZiMzkwODUxNjY3NGM5MDMyZGZkMGIxOWE0YjQxYmJlM2E0ZWQ0YTNmMTRlMjBjYyJ9', 'eyJpdiI6IkozeDJleVk3TUY1cVVXVExmenVkZlE9PSIsInZhbHVlIjoibzdPbWptalNzUmJvXC9hZFY5RlF1c2c9PSIsIm1hYyI6ImZjMDYyNTNkMWM2ODA0NGYzYmYxMGNmMWIwYTk4YWRiOGQxZWVjYmVhY2U1NjQ3YWI3ZTNjNzY5ZDAxNGRiMzMifQ==', 'eyJpdiI6IlwvSzFrV2hcL1daVEw1RGRyeDdLWlpkdz09IiwidmFsdWUiOiJmbEpFVjF3N2xQakE2MVJuN0hkMWJnPT0iLCJtYWMiOiIwZmU1YjYzMTc5N2Q2N2EzYTEyNjk4YjFjOTg4MjgxYmU1ZjJlNmQ4NDg5ZjI0ZjRjMDM0OWE3Yzg1YjBlNmYzIn0=', 'eyJpdiI6ImRxcTd6XC9wVEMxWXAwVzVzM25ZMGN3PT0iLCJ2YWx1ZSI6ImpsaVpWYmtveHhDc2NZZU9waG94YVpTWERuQ2N2Z1JsUFNWK3ZuR2daRkc2Y0hnXC80Y2lobTd2OG55cXFxYlpsIiwibWFjIjoiMGMyMDRjNTU4OWM1YWVkNDExNzU0ZDdiZDA1MWVkOTRhODk0N2M5ZWJjYzM4ZDVjZTFmMjQ2MThlOTA2Nzg5ZCJ9', 'eyJpdiI6Ilp3R0FvcVB1WmgxXC9tS29KQ0dxdlFBPT0iLCJ2YWx1ZSI6IlgyVXZORlYyOWdjMWFoTkI5OW12dnc9PSIsIm1hYyI6IjBhN2YzNWUxZTljYWQ3MjZjYjE4MmE5NWEwNzg5ODYzODg2ZGRlMGQ2YWY1OWNiNzc5ZmU5OTk1MzE4NTgxZGIifQ==', 'eyJpdiI6IndQM2R3a0h4eDN4VWFRc25WMG9UaVE9PSIsInZhbHVlIjoiWEYzZm9ZNHNlaUx0VUg4bzlcL0pEMGc9PSIsIm1hYyI6IjgzNDY2Yjg3M2M3MjkwZGNmMzQ2OTJmZjE3ZGMzYzY0YWEyMjM0NjI0YWUxMjc1ZDcwNTY4YWZkYTU1YzJiYjUifQ==', 'eyJpdiI6IkVDK0FEcXJXVGpKYldGbHdTS1lSVFE9PSIsInZhbHVlIjoidVwva2pVWFwvTU9OeWtqVDE3RlBCTmNrYVdQYVZXRG05VDVsY2prbThPbFFrPSIsIm1hYyI6IjIyZWFlNDZkMmRjYTM2MmRmNGExYTQ3MTc5NWM2ZmEyNjhiYTM4ZTYwMGNmM2Q5NTMwNzczYzRkOGJlNTJkYjEifQ==', 'eyJpdiI6ImxwYmQxTkpZZGp5MUk5S2o3VVMwQ0E9PSIsInZhbHVlIjoiRVRhRmZ3T1llNVJVSHNOXC85WmdRbWc9PSIsIm1hYyI6IjVhN2QxNWZmOGQyNzQxZGQwNzQzNGJhNTAyZjk5MjljOTg2Zjk2NmUyOGEwZGJmNzUyMzYxZmM0YzkwODRmMmEifQ==', 'eyJpdiI6ImtLYXhmeTZLamVVaW1PWFZZZWlJdkE9PSIsInZhbHVlIjoic2FBeEdwSWxhKzMwTDcwKys1SE5EZz09IiwibWFjIjoiMTEwMTMwYzBiYjA1YWIyOTJjMjI1YjMxMTBjNDcyNWFiNzBlMmExOGJkMzE4M2M5ZWZmMDJiMWMyYWNlMjA4ZSJ9', 1);

-- --------------------------------------------------------

--
-- Table structure for table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_client` int(11) NOT NULL,
  `rowid_periode` int(11) NOT NULL,
  `sous_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `date_commande` datetime NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify` tinyint(1) NOT NULL,
  `facture` blob,
  `identifiant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paye` tinyint(1) NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `fk_periode_commande` (`rowid_periode`),
  KEY `fk_client_commande` (`rowid_client`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commande`
--

INSERT INTO `commande` (`rowid`, `rowid_client`, `rowid_periode`, `sous_total`, `total`, `date_commande`, `email`, `adresse`, `no_app`, `ville`, `province`, `pays`, `code_postal`, `telephone`, `poste`, `notify`, `facture`, `identifiant`, `paye`) VALUES
(1, 2, 1, '32.83', '36.45', '2019-01-30 13:08:03', 'zach@cegep.com', 'ghfhf', NULL, 'fghfgh', NULL, NULL, 'G6B0W1', '8888888888', NULL, 1, NULL, '', 0),
(2, 2, 1, '79.68', '88.44', '2019-01-30 13:09:27', 'zach@cegep.com', 'testing', '12', 'Hello', NULL, NULL, 'G6B0W1', '8888889999', NULL, 1, NULL, '', 0),
(3, 2, 1, '44.64', '46.86', '2019-01-30 15:49:24', 'zach@cegep.com', 'testing', '12', 'Hello', NULL, NULL, 'G6B0W1', '8888889999', NULL, 1, NULL, '', 0),
(4, 2, 1, '80.82', '84.78', '2019-01-30 15:51:34', 'zach@cegep.com', 'testing', '12', 'Hello', NULL, NULL, 'G6B0W1', '8888889999', NULL, 1, NULL, '', 0),
(5, 1, 3, '330.51', '392.84', '2019-02-03 23:07:10', 'jessy@cegep.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '', 0),
(6, 1, 3, '64.35', '67.47', '2019-02-03 23:08:25', 'jessy@cegep.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '', 0),
(7, 4, 3, '163.32', '182.22', '2019-02-06 14:10:21', 'eyJpdiI6IldZZ21iRjJ4eEtIaW5hcG1pTjg1XC9BPT0iLCJ2YWx1ZSI6IlU5MU1RUVN4NGRqQUtBbjdVRHZxVHBwYkpkeDNSU3gzQ3NvVE5JU3ZsYlU9IiwibWFjIjoiYjk5NTA3ZDE1MmM3ZGNlNGM3NGVkNzY4NThmZTZhZjE4MzA5OWNjOWVhNGM0YjM1NTU3MDNkYmI1Yjc5Y2VjMSJ9', 'eyJpdiI6IjcxS2RzTHRBbmhMS0pBQkl5R2c5YlE9PSIsInZhbHVlIjoiT29jVEt3VHN3UVlrN3czNVdxXC9mSGc9PSIsIm1hYyI6ImUxYTZjMzViMTFkNTcyYzdlMjFjOTVkMTAwYmU3YzU3NjNhZGYzOWQ5ZGFkNjI3YTgyOGFhNTliZTYxNDQ0NTMifQ==', 'eyJpdiI6Ijd1S3NiOUhWM3BoRjF1dzhaXC8zSEdBPT0iLCJ2YWx1ZSI6IjBldkFoVjRjTnBzdU9WOVZcLzA5QVZ3PT0iLCJtYWMiOiJjMjIxNTkyZGNiZTZhOTNmNjlkY2E5NzZmOTJjNWE4MmFhMGRkNDkwMzNiOTRmNWMyOWQ5YWMzODI4NDAzYzBkIn0=', 'eyJpdiI6InZNdTVKWHI1VkpsVWtrOWxmb2pmWWc9PSIsInZhbHVlIjoia2ljbldaWG1zZ25pZGRPeVRzeE5IZz09IiwibWFjIjoiZDQxMTYyNGY2Mjc4YTViYzI1ZmM5MjMxOTJlMzdlMDcyMzQ0OWRiOTQzMGFiNjg1NjYzMmNlNWZkMjQ3OTBjNiJ9', 'eyJpdiI6IjFhKzJcL3plOHZVSzladmZYRUpiZnh3PT0iLCJ2YWx1ZSI6Iis1dHhaUE1KUFdIMDBPSmN5Z3FVOXc9PSIsIm1hYyI6IjAyMmJiZjMyMmNlMjA0MDAxYzU0YWExZDY1YjE2ZGEzNGYyOGViZTE1NzU3NTNiNWUyNDZmOTRlNTcyNDc0NGUifQ==', 'eyJpdiI6Ijd0M2tlQitXR01SMythVmNIODh2YWc9PSIsInZhbHVlIjoieTVcL05LUHFtdUFZOFBaQ3MyMzhHY2c9PSIsIm1hYyI6IjQxN2QyMjQ5Mzk2OTVkMzVlOWY3ZmExYzRhNDUwOTlhODk0NTVjNmI1OGVlYjBmZjgzNTE0OTAyY2MxYTkzZDUifQ==', 'eyJpdiI6ImpINTd0aXVZNlVvQW92RSt3ZCsxSGc9PSIsInZhbHVlIjoiZUcwbmVBbDN2M2dIQU10NHhoRFY0UT09IiwibWFjIjoiNzRkZTM5YTYwYjU3ZTc0MjYwM2Q2MTg0MWQyOWFiMzAxZTFiMmZhOTFmNzc4MGVjMjlkMzcwMjJmMmZhZWU0YyJ9', 'eyJpdiI6IjIwQ0xUV1pydmlEMUkzeXh3QkIwT0E9PSIsInZhbHVlIjoibXR4cVVsQXRTR3lQYWdvajd2aFQzZz09IiwibWFjIjoiZDE2YmYyOGVhNWY4Yzg3ZTAwMGRjMDBmYWM1ZTEyNzA3OGEwYmQxZjYzNmFmY2FkODViZGM1OTlkMGRmZWYzOSJ9', 'eyJpdiI6InkwZ0dtakNCZUJWcEZMVWFIWWU3Q2c9PSIsInZhbHVlIjoicGpaeDZhQzFCVHc4amhPc1p2UDNFdz09IiwibWFjIjoiOTM5MDExY2Y2YjIxNGE5MWU5MDc5YjE5NTcxZTUxMjVjYjJjNGY3ZmU1YWRlZGQzZDE1MDEzNmU1NWJiNzczNyJ9', 1, NULL, '', 0),
(8, 4, 3, '143.58', '153.60', '2019-02-06 14:13:47', 'eyJpdiI6Im1FY0JLbXY1RGJpZzdXNlRPYVVSQ1E9PSIsInZhbHVlIjoiVzJVd1hpVFA2M3UzTlQwUFRkZDE3YmhpSDRnbWwzc1BsV0FqMjV5U0ltTT0iLCJtYWMiOiIxOTMxZTJlYzFhYWU3M2RiY2M0OWVmZGJjYTczNDE0MmJiMDNkNTI4ZWQwMDk3NTdiZmE1NTBiNjhlMzNkNjVkIn0=', 'eyJpdiI6Imt2a3FrbDdGM3Z4aDdkYmRTSDBGSEE9PSIsInZhbHVlIjoicVwvS09za0ZoeGU2UGpOSFlaZW5RN1E9PSIsIm1hYyI6IjE3Yzk0ZTk3MDc5NWJjMTBkZjQ5NTE4MTNiN2JhNTYwZjZmYTE2NmY2NGJlODYyNzAzZWFlMzkwM2MyYzlmMjEifQ==', 'eyJpdiI6IjJEXC84emo4cHcxXC9meTZISmpvd2xNdz09IiwidmFsdWUiOiJ1YnFUeUVqZDF2ZkU5V2lUZldPU0h3PT0iLCJtYWMiOiIwNWJlZmQ0ZWY2MDU3YTRhYjU2MmFiNzU0NGRiZmE2ODdlYTcyOTU5ZmY5ZTJjYWU4N2JjZmI4MjM3ZGNhNTA0In0=', 'eyJpdiI6InpKYU40VjRLcmMwM1lhb3hRSU9XRlE9PSIsInZhbHVlIjoidncyQnlBdythRldSaWtPMUxrQTVUZz09IiwibWFjIjoiNWVlY2JjNmM5ZDUxNzdiMGVjNmYwYWI4NmY3MTQ3Njc1ODQ4ZWZmNzAwMmY1NTIzNDM3ZTRjNTk1ZTc1M2RkMiJ9', 'eyJpdiI6ImhiRFJrdjRQOHQyakZOOHdlQUN4R3c9PSIsInZhbHVlIjoiQ0ZkUjcxWjlXR2hEWUNNM1hoN2FxQT09IiwibWFjIjoiMTQ5MmRjMjFkMzcyNDdhODM1MDhlYmFmM2EzZGM2ZTIwZDYzM2U1ZDRmNjVkMzFlOTQ2OGM1OTZhNjE3YjI3MyJ9', 'eyJpdiI6ImY1OTU3dmFvRmNnd2RkWEppYU53bXc9PSIsInZhbHVlIjoiNXE0S212YlZobDAwSnRmVlwvVkJPRUE9PSIsIm1hYyI6IjNlNmRiNmU1ODllOWYyZDk5MDE2NzA2ZmZmOTQ2ZjY3NWU5YjUxMzA3MzBjOTMyMWFkYTcyYWVmMWJiYjgzMDkifQ==', 'eyJpdiI6IlQ5XC9oaTJpY1RiVTgxZVFoRmNWTitRPT0iLCJ2YWx1ZSI6InJTbm12emdJRjdKcFZwM1wvbXZLXC9oUT09IiwibWFjIjoiZmQyOGUxZjBiMzFkMjZjMTZkZTZhODU0NjliNTJmNmZmZTY2MDRiYzQ5MjMzMTJhMGQxNzlmNTg0NmViM2I5MCJ9', 'eyJpdiI6InpuamJJMFwvK1wvUm5KN1JLOEdtWmlzUT09IiwidmFsdWUiOiJ5VUlXQjVpNisxV1Bnd0NkNlJQSndRPT0iLCJtYWMiOiJlMmQ3ODAxNjhlOGZiZjdhZjg0NDlmYzAyYmJkODk5ODkyNGYyMWI1Y2M3Yjk1M2UyYTAxNDlkNjM1NDVjZDlkIn0=', 'eyJpdiI6ImN5eEROSjZzWktpRlh6UU9nWHBGb0E9PSIsInZhbHVlIjoiTUFRRXlrcTNxUmxVNHJ0TnhyODl3Zz09IiwibWFjIjoiZDNlMDU2OWEyY2RmNTE5YzJkZWMyYTE4ZjU0MTkyOGY5MjQ1YWRkNzc1MWZlYWIzNzRkNWYyMWQxYzc3OGMyYyJ9', 1, NULL, '', 0),
(9, 4, 3, '13.94', '15.63', '2019-02-08 13:09:49', 'eyJpdiI6IkVCZ05zMjM3Ynczc1AxV2w5cnh1ZUE9PSIsInZhbHVlIjoiamJwbEI2VG9Wenp6RHdTdjU1T0RENVozTVBXc2ZGaFJGM3psUXRoXC85SVE9IiwibWFjIjoiMTY5N2ZkOWUyNzE0OTczMTk3NjA1MWUzY2YwNDJiOTViYjc5MmQ3MDZhNjhhYWQzZDkzMzY2Y2QzZDE1MTk1OCJ9', 'eyJpdiI6Ik9Ha0NyVGplZXhJZ2RqemZuY3VaQXc9PSIsInZhbHVlIjoiYkVrTStXSHJ0ZEpiUUFqT09reW45Zz09IiwibWFjIjoiZjkwMmRhNzc0MGNlNGUzNDljM2E4NDQ2ZjU3MGI5ZGM0NzIyMjYxMjhkNmZhZjkzNWQxZDg3YTYwOTY1OGE2YSJ9', 'eyJpdiI6Im5XVmRydlJsWEp5VXRsM2xFeEgzNHc9PSIsInZhbHVlIjoieFVBU1RieEJKSFZEMGVUUXdibktNUT09IiwibWFjIjoiMmY4ZTk0YWMwYWQyYzg4OWNiZWEzNjM2OWZhODQwMWE5NjI4ZTUyNjI4MDY4MTc2ODIwNmUyZTU4ZjA1ZTZmOSJ9', 'eyJpdiI6IkF4RkpCSTQ1bnAyRkp6dlJyRGtxMEE9PSIsInZhbHVlIjoiMmJrQnhNOUpyM1U1cmRPRmhOanQ2UT09IiwibWFjIjoiN2E1YWExMDExOWZlNDg0ZGQzZTcxMmYzNDcxNmU3NTM0ZGNjNmI1Y2E5NTc3MzA3MWEwM2NkYjZhNGI0ZGQ0OCJ9', 'eyJpdiI6IkVqYWxZMG90REx5aWRIZkMwNWRYK2c9PSIsInZhbHVlIjoiWGExR0JGMjRPYWtXQUNwRnlcL2o5WUE9PSIsIm1hYyI6IjVhNzFlNDIzNDhhOTFhMzYzOTk4Mzc0MmRjYjc5OTJjZGM4NWE2NDk3OTQ4ZjUwNGEzYWRhYmNjNmZmYjFhYzkifQ==', 'eyJpdiI6InBmdnVGcW4rcDZ4MkpvK0dHY2VvMXc9PSIsInZhbHVlIjoibzRCQ1VDV0phcU1BMnhSKzdOc01LUT09IiwibWFjIjoiNDE2NTM4YjkwMGRhYjcwNjU1M2IxODlmMzUyMjRjZmRlMjBhYTczNDA0MTlkNzVlZDIyZTA1NWY2NGU0YzM2YiJ9', 'eyJpdiI6IlRjR0U2S2lidzZtR3BMd0xvRXk4T1E9PSIsInZhbHVlIjoiMTRkTnNzR1BGbVlLOVBmWUxUYXpwZz09IiwibWFjIjoiYmY0OTdhNTJkM2JhMjM5YTNlNGQ3Y2FkZTNiZjM2NDkwOWZkMWE1MDdiNjhlYmYzZWI5NmQ5MzQxOWRjZjY3NiJ9', 'eyJpdiI6ImNFOFcyVGZ6WndYYjR4VFlaM3h0Vmc9PSIsInZhbHVlIjoiYWF6YVl2dlRCXC9BVWx2U3JPbkp6aUE9PSIsIm1hYyI6IjNkNzJlMmFjNGFkNzBiMDk5N2E3ZmE4NTdiYmVmNTA2YjE0NGYxZGZhZjRlOGZhZjA4ODczNjcxZWJlZmYyNWEifQ==', 'eyJpdiI6IklNUEdvZXZ5QTJBMjBEWU1XZitYNmc9PSIsInZhbHVlIjoiRnptbFZmbStWeHY2QnFDQ0pES3NFZz09IiwibWFjIjoiZGVhMmUzZDYyMjNhOTYwNTEzYmNmMDljZDQ5MzEzY2FhNjNkNGEzYTc0MTg3MDdiZjZjN2VkNTlhMmE1MzQ2OSJ9', 1, NULL, 'EB616F46-E1D8-4434-9052-4AF1B41B263F', 0),
(10, 4, 3, '13.94', '15.63', '2019-02-08 13:10:21', 'eyJpdiI6IjdOOU9NN2NibHE5MnMxMWMyeVdIUmc9PSIsInZhbHVlIjoiYktaWDBMMmNKWW9RT0tNUkhhS3habGI0ajhBODRod1RMako4eEMxUkRDTT0iLCJtYWMiOiIxZjBmZjlmZDBjNzRjYWRmOTU4MGY4NmVlZWZmMDFjOWNmMmRjOTljZDg0ZWEyYzQzMDdjYTkxZWVjYWJiOTBiIn0=', 'eyJpdiI6IllcLzJpUjBVZjE1UlNlZElGM3ltbEhRPT0iLCJ2YWx1ZSI6IjByUkRFWkh3NVwvbW81S1ArUVZKc3V3PT0iLCJtYWMiOiIxOTRlODY0YWRhZjA1NDZiMWUxOTQ3YTY4MTUxNDY3YzY4MzA5YzZlZTE1NmQyMDM0NDU0MGE1NzRkYWVjYzViIn0=', 'eyJpdiI6ImJJY1Z5aE9xSHpodEduZ1JZQXpnT3c9PSIsInZhbHVlIjoiQWc5ZmNtZUlyTFZnMEJacWh5ZDNydz09IiwibWFjIjoiZDNhMzY4ZWJmOGU0ZTQxZWE3YWI5MGM4MjJmYjJjNWE2Y2MyNjY5YzQwZjI5YTE1NmNjZDU2YTRhMjFkYTk5ZSJ9', 'eyJpdiI6Ikc5bHNkbEdVVU5lYVA3YXMxbzA1VUE9PSIsInZhbHVlIjoiTmZ0T1R1UVByNzFERmJhWHVtQUs1QT09IiwibWFjIjoiNmQ1MTJiOGJhMzRjYWE5NTc4NTk3YzhhMTkxYjdlZjFjM2ZiYjMxYTFjYTE4MzFiYWYzMjJmOWNkZjJhOTFkNSJ9', 'eyJpdiI6Im1YVXRHc2FHUU1KMWZzaEFyNnExenc9PSIsInZhbHVlIjoiUCtTdFRcL0xMMm05WHlUUENFWWNocnc9PSIsIm1hYyI6ImMyNmQzZDU5YmMyMjBjZTVlZWE5ZGFlZjdmNmJlYzg0NTM4MDlkMjZkZmZkYmJmYTNmNjVmYmQzNDYxYmUzNTUifQ==', 'eyJpdiI6Indqb1QzREZwK05Gb0E0VUVzTXNTbUE9PSIsInZhbHVlIjoibUZteDZqaytHTStmWFBlTWwxd0tcL3c9PSIsIm1hYyI6ImM1MzljYWFkOWY4MjA5MmFkMzM5YzBlN2E2NzkzODk5NWFjMzc0OWJlN2JlMTM2NTVmNTIzZGZjNWUwZGJmMDQifQ==', 'eyJpdiI6ImVlbFNENlFJd3FiTEpOd01tVzk3cFE9PSIsInZhbHVlIjoiUGNJWHR0YXpxQWpmTVdPcTAwWmg3Zz09IiwibWFjIjoiZjVmMmY3NDE1YzQwYjVkOWY5YWYxMmJlOGY0YTZkZTAzMTViMjU5NGJjMWEwNDBmNTc4ODBiZmI5YjcyOTUwZSJ9', 'eyJpdiI6InhHWlU0cWFtOVhqazhGamxmSjE5UUE9PSIsInZhbHVlIjoiRFdQUGJRQmlITCtKTVFSbmRuSTFNQT09IiwibWFjIjoiZDhkZTQ0NmNkNDUwOTA1OWFlNjg2MTQ2N2E1NWQ3Yzg5YjUxZWJjMzVjOGYzNjhiOWNjZTJhYmY5OGM2MWJhNiJ9', 'eyJpdiI6Im50bUNBVWVRRUlOaXlNVVFpTnN0Q3c9PSIsInZhbHVlIjoibUVEMkRlQkV3RkFveThsbW5sK2lZdz09IiwibWFjIjoiYjczMDM5MTZmMjFmNTE2MWJiNjdlMDczMjQxNWE4YWFhNzE0Y2FiMzBjNjU2NTQ5OGIxNmMyNGRjYTc4ZjMyNCJ9', 1, NULL, '', 1),
(11, 4, 3, '14.85', '15.57', '2019-02-08 13:11:37', 'eyJpdiI6IlM0R3FPTTN0MWFIRUU4blwvMW9CU0JnPT0iLCJ2YWx1ZSI6ImQ1ZDhrZzJYMDlJVVZhXC82bjZQV0E5bDE3SU1KRlhHd3lLcVdIVDRpMjNRPSIsIm1hYyI6IjI5MTA0Y2Q4NjlkOGVkYmYyMDIzZWQ0NmViNzU5MmM4ZjQ2NzBlMjlmMzg0MzEwOGIwMWFlZmExZjUzYjE3MmMifQ==', 'eyJpdiI6Ikd5UVR1QnRDQTZPNlAzMit3REYwZHc9PSIsInZhbHVlIjoiS01SQzZ1NXZ5SmdOWTVXWDJIRkZmbmVQMFJ3V2tKZk53TFZZVjZFYjJGXC9mUkZKN1pucFplbXl0VFFTU2FWcnciLCJtYWMiOiI0ZDI1NzZmNGFiNDc4NWUwZTQ4MjIwYjc0NzFhMDNkMzdlZTYzMDE1ZGIyOTkwMWNhZGRlYWY3ZmFjZjVlZjc5In0=', 'eyJpdiI6IlFSa0pGaHRHZkJvK3M2UkRXMEZBZmc9PSIsInZhbHVlIjoiNTJXMGo3K09jaTFUUmRDQlF3YXNJQT09IiwibWFjIjoiNWNmNzExNjcwNjRjYmJjNzk4Y2NkYmVlMGFhYzE4NWZlNmQ2N2UwYTAzYWIxNTFkMmE0NDg0N2Q4Mjg1OTljMyJ9', 'eyJpdiI6IktMQSsrQmE1Zkt3Q3RWdlpcL0lCTTJRPT0iLCJ2YWx1ZSI6ImIydXZkYkE3YUVwVTc1YXNNNG1DcWxEWDhKazVSY0FWKzdzVENoalFGWnc9IiwibWFjIjoiZjY0ZTAyZDA1OWI4MTUwMzhmMWIwODUwZWJiODEyYWM2Y2JhMDBkMWU3MWRmYThhOWU3NmRlZjA2MWEwNjYyNSJ9', 'eyJpdiI6Ik1yVEZtNTBUVEJWbExaTTdReWxMN0E9PSIsInZhbHVlIjoiT0Q3b3FjVE83MExMNVpqMUUyeFdiQT09IiwibWFjIjoiYzJjNTk4OTFlZGJmNTA2ZTg4MGZlMDdjMTgxODlkZDkyNWI2NjVjYjMzNWMzNmYwYzhiZjg3NmExOGE1ZWIwZiJ9', 'eyJpdiI6IjZ4WUxuelc0TDFtT3lBR3hnT2ZpS0E9PSIsInZhbHVlIjoiRjM4Qnh0VEZqeEczbTZjTlZFeWF5dz09IiwibWFjIjoiMmMwNjgxMDc4MWQyMWY3NmRhMDY5MmFlMmU0Yzk1YWY4YTUyZWZmMzFjMTYyZDA5ZTQ0ODI1MWMyZjI5YzZjOSJ9', 'eyJpdiI6IkZZWDdGc28yVERTMFVHRVhpa1wvS1wvQT09IiwidmFsdWUiOiIxSjNrd2FVM05FVFdzZktWZEpDXC9cL1E9PSIsIm1hYyI6ImNiZWEzNWY4ZjliMWVmMjFkYjRhMTQxMDU5YTg1MThmODM2ZDY5ZmMzNjQyOWVkY2Y3MDBjYWM3M2I2NmJjMWQifQ==', 'eyJpdiI6IllnQk1ycWV3S3dcL2FROTQyZ2FcL1c0Zz09IiwidmFsdWUiOiIzZjM0V052QW9RQzNSNTVYTFNWMzhFNWo1ZjJFTkVqcVRJZWh6OERPZGdNPSIsIm1hYyI6ImUzMjJmZTBjNmY4MjEzYjU4NWJkMTkwMWM5MjM3OWI1OWE5NWE4N2U1YzAzOWY0OWEzMzQ5ZmRiYWQwZmY4ZjcifQ==', 'eyJpdiI6IjVzak5EakcyaTZwNnl0cWVGNmdLMEE9PSIsInZhbHVlIjoiTFwvMW02UkVrVmxPUG1mdlZiVVwvdUhnPT0iLCJtYWMiOiI1ZTAyZWVhMGVlZmNhOGVjOTczMGU2NjBiZDdlMWZiZDY2YTk5ZTVlZTlhNDM2N2ZlNjQ0NGVhZTM0ODIxMGZiIn0=', 1, NULL, '', 1),
(12, 4, 3, '55.98', '58.77', '2019-02-08 13:18:18', 'eyJpdiI6Ikxqd2o4U1VRc2g5SmsxSzVEc0J6NGc9PSIsInZhbHVlIjoiNXlIRkRPMUhhMVwvVDZkQU84NnRZNDNBOU1cL0dJVm1kK1RWOTNYVHV6NkIwPSIsIm1hYyI6ImM1ZTk1NjI2OTU5OWY3YTUwZWY1YWU0MWU1OTNiNWEwMTk0NTBkNzJmMTlmYmNhOWFhNzQ4OGVjNTc5NzQzNmYifQ==', 'eyJpdiI6Im9BVjhKaFhJMTd1eEgyXC9cLzhNOHZtUT09IiwidmFsdWUiOiJoNXpwdnp2WnVOYjdJR3pQVm5WcFlWVzRCSktMR21FMWd0dkEwMVplVlplMStpSTlsSU9XM0V1M3lHVHhrdTVHIiwibWFjIjoiZmNiNWY3NjFiZTYyMzFjMGQzMmIzODVlZjRlYTY1MGY1N2Y1NTMzOTExNTEzZmFiNWI5ZGY5NzJlZGZhYzVkOSJ9', 'eyJpdiI6InJ3MlBtY3Rtdjl6VEpDV2FobENnSkE9PSIsInZhbHVlIjoidE1nV3RMVEJudzZnT2dMZ0RmZ0hDUT09IiwibWFjIjoiZjFhNjIzYmFlZjA3YjEzZjVjNmMwMmY0ZWYxOGY2NDU4NjI4MDA4YmU5YjkyOTM5ZDJhZGNjYzU5N2I0NzM1NiJ9', 'eyJpdiI6IkZpMzBpejkxSjV5OUZ4dk53Z1Q1cFE9PSIsInZhbHVlIjoiR3BtdytrcmlJS0hMU1NZNkdob1dpTU9mQno4dDZaWTBjQzMxb2pnVUpVaz0iLCJtYWMiOiJkMTk5NWY3NzFlYTVjYjJhOTg0MjNmMmY4MWY1ZThiOTEyZDZmMjcwZTk3OWZiY2VhMjMzYzlhYTE0NjEzOWIwIn0=', 'eyJpdiI6IlNDdlJ3MFJFSlZYMFEzaVArUTJwb0E9PSIsInZhbHVlIjoiekR0MTR4WFphbmNHckNJK0NZNnp3QT09IiwibWFjIjoiMTVkZTQ0ZmFjZmUxZmQyOGIwNDIzY2E0NDQzMzE5NjQxNDdkMzY3MzEwMzc1YjA0MWUwZTRjZWQxMGYwNmRhNyJ9', 'eyJpdiI6ImhYSVhGSVBVcEtJQ284cExERyt6enc9PSIsInZhbHVlIjoiR0JvRWJGY1N5a1JyaTZjalpyYVF4Zz09IiwibWFjIjoiOTE3MWQ0Mzg4ZDhjNTViMzZmNDZmZmVlMTJmMzg5NDJjM2EzNWExMTZhZDU2OGU5MjMxNmM3MGQ5ODg0OTFjNiJ9', 'eyJpdiI6IlhIcTF2dnRCSlUxNzFBXC9La3pUcjlnPT0iLCJ2YWx1ZSI6IkRVdnJteUJXZjNKcVNEY3VSZVZIMXc9PSIsIm1hYyI6ImQ0ZTM4ZDgyN2I0YWJlMWI2NDVlMDA0ZDE0M2M4NWI4ZjEwNzA3MWEzOTU3Njg3NDk0YTc0OTRiMmM4MDg4ODYifQ==', 'eyJpdiI6IlpEYk1CN1JqM1ZFOWxmVmRoQ0FcL0pBPT0iLCJ2YWx1ZSI6ImdYaDNHaFByVEd0ZmlWZGhsSVwveUNTd3JzcXZ0Q1RGckhiT09nZ3VVT1V3PSIsIm1hYyI6IjIxYTcwODIxYjE2NjAyMWIxYTBhYzY1MDQ1ZjVhY2IyODU0Njc3OTVjNjNlYzYyN2E3OWFhYmI0YzI0MzY2MzIifQ==', 'eyJpdiI6IjFhR3BRR1RWVjUrdjJmTld1WHBnQ1E9PSIsInZhbHVlIjoidWl1VUFkSG4wWDBXcEc0SHZoc2tSZz09IiwibWFjIjoiMTkzMzRiZjA0OGIxM2MxMzExZWFiZTEzOTRjNzc5ZDBjODNjYzBlZTZjY2E2YmI0MTA3OWNjYmFjYjZlYjlhNyJ9', 1, NULL, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `commande_article`
--

DROP TABLE IF EXISTS `commande_article`;
CREATE TABLE IF NOT EXISTS `commande_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `commande_rowid` int(11) NOT NULL,
  `article_rowid` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantite_commande` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`rowid`),
  KEY `fk_commande_article` (`commande_rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commande_article`
--

INSERT INTO `commande_article` (`rowid`, `commande_rowid`, `article_rowid`, `image`, `nom`, `format_quantite`, `format`, `provenance`, `allergene`, `perissable`, `bio`, `quebec`, `nom_marque`, `nom_categorie`, `nom_fournisseur`, `quantite_commande`, `prix`, `frais_fixe`, `frais_variable`, `prix_majore`, `economie`) VALUES
(1, 1, 14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 2, '13.94', '5.00', '1.00', NULL, NULL),
(2, 1, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 1, '4.95', '5.00', '0.00', NULL, NULL),
(3, 2, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 4, '19.92', '6.00', '1.00', NULL, NULL),
(4, 3, 5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 3, '14.88', '5.00', '0.00', NULL, NULL),
(5, 4, 10, 'public/products/images/I6XjF0Hwgg9sIX0tqhw7rrjiacSVk3oHcqy0DeXV.jpeg', 'Moutarde de Dijon', '1.00', 'kg', 'Québec', NULL, 1, 1, 1, 'Herta', 'Condiments et autres', 'Produits alimentaires Quebec', 9, '8.98', '5.00', '0.00', NULL, NULL),
(6, 5, 7, 'public/products/images/TmxisxJI9ocq2Ip8TQriNaer4WypYgn9Enuy9HBb.jpeg', 'Café La Montréalaise Santropol', '500.00', 'g', 'Amérique centrale et du sud', NULL, 0, 1, 1, 'Materne', 'Café', 'Grossistes en café Quebec', 23, '14.37', '5.00', '2.00', NULL, NULL),
(7, 6, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 13, '4.95', '5.00', '0.00', NULL, NULL),
(8, 7, 14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 6, '13.94', '5.00', '1.00', NULL, NULL),
(9, 7, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 4, '19.92', '6.00', '1.00', NULL, NULL),
(10, 8, 6, 'public/products/images/fxtpyETXUMXk2OswDJ3T1SparCXp9c4caKKNOa8J.jpeg', 'Capuchons de chocolat 70%', '500.00', 'g', 'Fèves du Pérou, chocolat produit en Europe', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 6, '23.93', '7.00', '0.00', NULL, NULL),
(11, 9, 14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 1, '13.94', '5.00', '1.00', NULL, NULL),
(12, 10, 14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 1, '13.94', '5.00', '1.00', NULL, NULL),
(13, 11, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 3, '4.95', '5.00', '0.00', NULL, NULL),
(14, 12, 15, 'public/products/images/17YmeaiQPt8j48kzvCWmxRqXAzBKtDD3r7r0TetF.jpeg', 'Canneberges séchées au jus de pommes', '1.00', 'kg', 'Québec', NULL, 1, 1, 0, 'Citadelle', 'Fruits séchés', 'Produits biologiques Quebec', 3, '18.66', '5.00', '0.00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
CREATE TABLE IF NOT EXISTS `fournisseur` (
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_fournisseur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fournisseur`
--

INSERT INTO `fournisseur` (`nom_fournisseur`, `actif`) VALUES
('Aliments en vrac Quebec', 1),
('Chocolat Quebec', 1),
('Confitures et gelées Quebec', 1),
('Croustilles Quebec', 1),
('Détaillants de boissons Quebec', 1),
('Distributeurs et fabricants de mets chinois Quebec', 1),
('Eau embouteillée et en vrac Quebec', 1),
('Farine Quebec', 1),
('Fruits secs Quebec', 1),
('Glace Quebec', 1),
('Grossistes en aliments congelés Quebec', 1),
('Grossistes en café Quebec', 1),
('Grossistes et fabricants de produits naturels Quebec', 1),
('Huile d\'olive Quebec', 1),
('Huiles végétales Quebec', 1),
('Levure Quebec', 1),
('Maïs soufflé Quebec', 1),
('Malt et houblon Quebec', 1),
('Miel Quebec', 1),
('Noix Quebec', 1),
('Oeufs Quebec', 1),
('Produits alimentaires Quebec', 1),
('Produits biologiques Quebec', 1);

-- --------------------------------------------------------

--
-- Table structure for table `marque`
--

DROP TABLE IF EXISTS `marque`;
CREATE TABLE IF NOT EXISTS `marque` (
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nom_marque`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marque`
--

INSERT INTO `marque` (`nom_marque`, `actif`) VALUES
('Ayam', 1),
('Babybio', 1),
('Bledina', 1),
('Bret-s', 1),
('Carrefour', 1),
('Citadelle', 1),
('Danone', 1),
('Fleury-michon', 1),
('Gerble', 1),
('Herta', 1),
('Jardin-bio', 1),
('L-angelys', 1),
('Le-bon-paris', 1),
('Lea-nature', 1),
('Marque-repere', 1),
('Materne', 1),
('Monique-ranou', 1),
('Nestle', 1),
('Prana', 1),
('Propiedad-de', 1),
('Sacla', 1),
('Schar', 1),
('Sojasun', 1),
('U', 1),
('Yoplait', 1);

-- --------------------------------------------------------

--
-- Table structure for table `panier_article`
--

DROP TABLE IF EXISTS `panier_article`;
CREATE TABLE IF NOT EXISTS `panier_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_panier` int(11) NOT NULL,
  `rowid_article` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `panier_article_fk0` (`rowid_panier`),
  KEY `panier_article_fk1` (`rowid_article`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `panier_client`
--

DROP TABLE IF EXISTS `panier_client`;
CREATE TABLE IF NOT EXISTS `panier_client` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_client` int(11) NOT NULL,
  `actuel` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rowid`),
  KEY `panier_client_fk1` (`rowid_client`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `panier_client`
--

INSERT INTO `panier_client` (`rowid`, `rowid_client`, `actuel`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `periode_article`
--

DROP TABLE IF EXISTS `periode_article`;
CREATE TABLE IF NOT EXISTS `periode_article` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `article_rowid` int(11) NOT NULL,
  `periode_rowid` int(11) NOT NULL,
  `quantite_commande` int(11) NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `article_rowid` (`article_rowid`),
  KEY `periode_rowid` (`periode_rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_article`
--

INSERT INTO `periode_article` (`rowid`, `article_rowid`, `periode_rowid`, `quantite_commande`) VALUES
(1, 14, 3, 9),
(2, 1, 3, 17),
(3, 4, 3, 8),
(4, 5, 3, 3),
(5, 10, 3, 9),
(6, 7, 3, 23),
(7, 6, 3, 6),
(8, 15, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `periode_commande`
--

DROP TABLE IF EXISTS `periode_commande`;
CREATE TABLE IF NOT EXISTS `periode_commande` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_commande`
--

INSERT INTO `periode_commande` (`rowid`, `date_debut`, `date_fin`) VALUES
(1, '2018-12-17 01:30:00', '2018-12-21 23:30:00'),
(2, '2018-12-10 00:00:00', '2018-12-14 00:00:00'),
(3, '2019-01-30 08:00:00', '2019-02-20 12:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `periode_plage`
--

DROP TABLE IF EXISTS `periode_plage`;
CREATE TABLE IF NOT EXISTS `periode_plage` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `rowid_periode` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `emplacement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `periode_plage_fk0` (`rowid_periode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_plage`
--

INSERT INTO `periode_plage` (`rowid`, `rowid_periode`, `date_debut`, `date_fin`, `emplacement`) VALUES
(1, 1, '2018-12-24 08:00:00', '2018-12-24 23:00:00', 'Sciences'),
(2, 1, '2018-12-24 08:00:00', '2018-12-24 23:00:00', 'Hums');

-- --------------------------------------------------------

--
-- Table structure for table `personnaliser`
--

DROP TABLE IF EXISTS `personnaliser`;
CREATE TABLE IF NOT EXISTS `personnaliser` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banniere` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_desc` varchar(1028) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_entreprise` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_code_postal` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_telephone` varchar(255) NOT NULL,
  `footer_copyright` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1` varchar(255) DEFAULT NULL,
  `car2` varchar(255) DEFAULT NULL,
  `car3` varchar(255) DEFAULT NULL,
  `car1_msg` varchar(50) DEFAULT NULL,
  `car2_msg` varchar(50) DEFAULT NULL,
  `car3_msg` varchar(50) DEFAULT NULL,
  `car1_link` varchar(255) DEFAULT NULL,
  `car2_link` varchar(255) DEFAULT NULL,
  `car3_link` varchar(255) DEFAULT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personnaliser`
--

INSERT INTO `personnaliser` (`rowid`, `logo`, `banniere`, `footer_desc`, `footer_entreprise`, `footer_address`, `footer_city`, `footer_code_postal`, `footer_telephone`, `footer_copyright`, `car1`, `car2`, `car3`, `car1_msg`, `car2_msg`, `car3_msg`, `car1_link`, `car2_link`, `car3_link`, `actif`) VALUES
(1, 'public/themes/logos/oi1ctvvQkW7fhBQXCBoR4g1yBddTIPEkCqKNMmUW.png', 'public/themes/bannieres/2JT4LWWGYmMQXCfHI7tSvwoiACLzu9xsK5i2qumH.jpeg', 'Regroupement d\'achat offert par le Cégep de Trois-Rivières!', 'Cégep de Trois-Rivières', '3500, rue de Courval', 'Trois-Rivières, QC', 'G9A 5E6', '8193761721', 'Charles-William Morency - Jessy Walker-Mailly - Zachary Veillette', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(3, 'public/themes/logos/Hp2lenDbyOulWipZMZc04Lf8AYtjZzDxRdQSSdMx.png', 'public/themes/bannieres/g4X4QIKLy3JLNq5m1ZOb4QC1z3yoRSMF067XmDgf.jpeg', 'Ceci est la description de l\'application.', 'Le nom de l\'organisation qui utilise le site', 'L\'adresse de l\'organisation.', 'La ville, la province, le pays', 'G8Z 1E9', '8193761721', 'La ligne de bas de page.', 'public/themes/carrousel/3cHAlaR1wIa9ISzwdYWGnIMCubZanSYa3pMl82e0.jpeg', 'public/themes/carrousel/c6xxjkZZJe8jfOWPkEDDlJKyNYWYLdV2mmF2J1ib.jpeg', NULL, NULL, 'Panier', NULL, '#', 'panier', '#', 0);

-- --------------------------------------------------------

--
-- Table structure for table `recette`
--

DROP TABLE IF EXISTS `recette`;
CREATE TABLE IF NOT EXISTS `recette` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `article_rowid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `article_rowid` (`article_rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recette`
--

INSERT INTO `recette` (`rowid`, `article_rowid`, `name`, `link`) VALUES
(1, 19, 'test2', 'test2'),
(6, 22, 'testtrhfghfghfg', 'test'),
(17, 1, 'Amarante en taboulé', 'http://chefsimon.com/gourmets/cuisinealcaline/recettes/amarante-en-taboule'),
(18, 1, 'Marrant petit déjeuner d\'amarante!', 'http://brutalimentation.ca/2014/03/12/marrant-petit-dejeuner-damarante/');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_administrateurs`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_administrateurs`;
CREATE TABLE IF NOT EXISTS `v_stat_administrateurs` (
`data` bigint(21)
,`label` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_categories`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_categories`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_categories` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_fournisseurs`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_fournisseurs`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_fournisseurs` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_marques`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_marques`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_marques` (
`data` bigint(21)
,`label` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_articles_populaires`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_articles_populaires`;
CREATE TABLE IF NOT EXISTS `v_stat_articles_populaires` (
`label` varchar(255)
,`data` decimal(32,0)
,`dataset_date` datetime
,`dataset_month` int(2)
,`dataset_month_full` varchar(9)
,`dataset_year` int(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_tendance_commandes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_tendance_commandes`;
CREATE TABLE IF NOT EXISTS `v_stat_tendance_commandes` (
`data` bigint(21)
,`dataset_month` int(2)
,`dataset_month_full` varchar(9)
,`dataset_year` int(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stat_utilisateurs`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_stat_utilisateurs`;
CREATE TABLE IF NOT EXISTS `v_stat_utilisateurs` (
`data` bigint(21)
,`label` varchar(7)
);

-- --------------------------------------------------------

--
-- Structure for view `v_stat_administrateurs`
--
DROP TABLE IF EXISTS `v_stat_administrateurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_administrateurs`  AS  select count(0) AS `data`,'Webmestre' AS `label` from `admin` where (`admin`.`master` = 1) union select count(0) AS `data`,'Enseignant' AS `label` from `admin` where (`admin`.`super` = 1) union select count(0) AS `data`,'Étudiant' AS `label` from `admin` where (`admin`.`super` = 0) ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_categories`
--
DROP TABLE IF EXISTS `v_stat_articles_categories`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_categories`  AS  select count(0) AS `data`,`article`.`nom_categorie` AS `label` from `article` group by `article`.`nom_categorie` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_fournisseurs`
--
DROP TABLE IF EXISTS `v_stat_articles_fournisseurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_fournisseurs`  AS  select count(0) AS `data`,`article`.`nom_fournisseur` AS `label` from `article` group by `article`.`nom_fournisseur` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_marques`
--
DROP TABLE IF EXISTS `v_stat_articles_marques`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_marques`  AS  select count(0) AS `data`,`article`.`nom_marque` AS `label` from `article` group by `article`.`nom_marque` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_articles_populaires`
--
DROP TABLE IF EXISTS `v_stat_articles_populaires`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_articles_populaires`  AS  select `commande_article`.`nom` AS `label`,sum(`commande_article`.`quantite_commande`) AS `data`,`commande`.`date_commande` AS `dataset_date`,month(`commande`.`date_commande`) AS `dataset_month`,monthname(`commande`.`date_commande`) AS `dataset_month_full`,year(`commande`.`date_commande`) AS `dataset_year` from (`commande_article` left join `commande` on((`commande`.`rowid` = `commande_article`.`commande_rowid`))) group by `commande_article`.`nom`,cast(`commande`.`date_commande` as date) order by month(`commande`.`date_commande`) desc,sum(`commande_article`.`quantite_commande`) desc ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_tendance_commandes`
--
DROP TABLE IF EXISTS `v_stat_tendance_commandes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_tendance_commandes`  AS  select count(`commande`.`rowid`) AS `data`,month(`commande`.`date_commande`) AS `dataset_month`,monthname(`commande`.`date_commande`) AS `dataset_month_full`,year(`commande`.`date_commande`) AS `dataset_year` from `commande` group by year(`commande`.`date_commande`),month(`commande`.`date_commande`) order by year(`commande`.`date_commande`) desc,month(`commande`.`date_commande`) ;

-- --------------------------------------------------------

--
-- Structure for view `v_stat_utilisateurs`
--
DROP TABLE IF EXISTS `v_stat_utilisateurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stat_utilisateurs`  AS  select count(0) AS `data`,'Actif' AS `label` from `client` where (`client`.`actif` = 1) union select count(0) AS `data`,'Inactif' AS `label` from `client` where (`client`.`actif` = 0) ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_fk0` FOREIGN KEY (`nom_marque`) REFERENCES `marque` (`nom_marque`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk1` FOREIGN KEY (`nom_categorie`) REFERENCES `categorie` (`nom_categorie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk2` FOREIGN KEY (`nom_fournisseur`) REFERENCES `fournisseur` (`nom_fournisseur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_client_commande` FOREIGN KEY (`rowid_client`) REFERENCES `client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_commande` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande_article`
--
ALTER TABLE `commande_article`
  ADD CONSTRAINT `fk_commande_article` FOREIGN KEY (`commande_rowid`) REFERENCES `commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `panier_article`
--
ALTER TABLE `panier_article`
  ADD CONSTRAINT `fk_article_panier` FOREIGN KEY (`rowid_article`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_panier_client` FOREIGN KEY (`rowid_panier`) REFERENCES `panier_client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_article`
--
ALTER TABLE `periode_article`
  ADD CONSTRAINT `fk_article_periode` FOREIGN KEY (`article_rowid`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_article` FOREIGN KEY (`periode_rowid`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_plage`
--
ALTER TABLE `periode_plage`
  ADD CONSTRAINT `fk_periode` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
