
create or replace  view v_stat_administrateurs as select count(0) AS `data`,'Webmestre' AS `label` from `admin` where (`admin`.`master` = 1) union select count(0) AS `data`,'Enseignant' AS `label` from `admin` where (`admin`.`super` = 1) union select count(0) AS `data`,'�tudiant' AS `label` from `admin` where (`admin`.`super` = 0);


create or replace  view v_stat_articles_categories as select count(0) AS `data`,article.`nom_categorie` AS `label` from `article` group by `article`.`nom_categorie`;


create or replace  view v_stat_articles_fournisseurs as select count(0) AS `data`,`article`.`nom_fournisseur` AS `label` from `article` group by `article`.`nom_fournisseur`;


create or replace  view v_stat_articles_marques as select count(0) AS `data`,`article`.`nom_marque` AS `label` from `article` group by `article`.`nom_marque`;


create or replace  view v_stat_articles_populaires as select `commande_article`.`nom` AS `label`,sum(`commande_article`.`quantite_commande`) AS `data`,`commande`.`date_commande` AS `dataset_date`,month(`commande`.`date_commande`) AS `dataset_month`,monthname(`commande`.`date_commande`) AS `dataset_month_full`,year(`commande`.`date_commande`) AS `dataset_year` from (`commande_article` left join `commande` on((`commande`.`rowid` = `commande_article`.`commande_rowid`))) where commande.paye = 1 group by `commande_article`.`nom`,cast(`commande`.`date_commande` as date) order by month(`commande`.`date_commande`) desc,sum(`commande_article`.`quantite_commande`) desc;


create or replace view v_stat_tendance_commandes as select count(`commande`.`rowid`) AS `data`,month(`commande`.`date_commande`) AS `dataset_month`,monthname(`commande`.`date_commande`) AS `dataset_month_full`,year(`commande`.`date_commande`) AS `dataset_year` from `commande` where (`commande`.`paye` = 1) group by year(`commande`.`date_commande`),month(`commande`.`date_commande`) order by year(`commande`.`date_commande`) desc,month(`commande`.`date_commande`);


create or replace   view v_stat_utilisateurs as select count(0) AS `data`,'Actif' AS `label` from `client` where (`client`.`actif` = 1) union select count(0) AS `data`,'Inactif' AS `label` from `client` where (`client`.`actif` = 0);


create or replace view v_stat_nb_client_actif as
select count(distinct `client`.`rowid`) AS `nb`,year(`commande`.`date_commande`) AS `dataset_year`,month(`commande`.`date_commande`) AS `dataset_month` 
from (`client` join `commande`) where ((`client`.`rowid` = `commande`.`rowid_client`) and (`commande`.`paye` = 1))
group by year(`commande`.`date_commande`),month(`commande`.`date_commande`) order by year(`commande`.`date_commande`) desc,month(`commande`.`date_commande`);