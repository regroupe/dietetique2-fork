-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 06, 2019 at 10:16 AM
-- Server version: 5.7.25
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `concep5t_diet`
--
CREATE DATABASE IF NOT EXISTS `concep5t_diet` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `concep5t_diet`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `super` tinyint(1) NOT NULL,
  `master` tinyint(1) NOT NULL,
  `take_order` tinyint(1) NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_modif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `change_pass` tinyint(1) NOT NULL DEFAULT '0',
  `actif` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`super`, `master`, `take_order`, `username`, `password`, `last_modif`, `change_pass`, `actif`) VALUES
(0, 0, 0, 'admin', '$2y$10$qejIbdueBlAOk9Fxger.su33bTctWlR6CKcqZ1iOnz6/GCZvdFQfO', 'master', 0, 1),
(1, 1, 1, 'master', '$2y$10$al.OCHpRFjQKFrkosV4vXuYv3uQnwi.Tyl8pYoKFtdcBUlt.gPxW.', '', 0, 1),
(0, 0, 0, 'op00001', '$2y$10$s8v5obWvREg3uvchjpWs0.oBfFDztCcYCz9QCq2SDf1rIbTd/lJpq', 'master', 0, 1),
(0, 0, 0, 'op00002', '$2y$10$ETsFZhQhv3Tnc7Y0HRVkxOEkSk8X4z.7gRWQyKVpmg52RJJQvcIwi', 'master', 1, 1),
(0, 0, 0, 'op00003', '96e6c685', 'super', 1, 1),
(0, 0, 0, 'op00004', '1c0fcb97', 'super', 1, 1),
(0, 0, 0, 'op00005', 'b7047748', 'super', 1, 1),
(0, 0, 0, 'op00006', '$2y$10$ETOR/kiHa0g.MLAZqs.bWurEQW.UvH2I4ZxfqKVeiIQfg0WYHpC2i', 'super', 1, 1),
(0, 0, 0, 'op00007', '$2y$10$UIPwsp/l6I2WVvj1LXqe2.BwDB01Zd3Zsy/jlkPbkHLzPpOZW1KEe', 'super', 1, 1),
(0, 0, 0, 'op00008', '$2y$10$W4eGv82hIxiNI/n/4ewd4e4qudRFHa9ZI6p8dBk9y7O446a6kyp3m', 'super', 1, 1),
(0, 0, 0, 'op00009', '$2y$10$aeaJqkE8ZvKu4trbpWx4zuagkTDRbfewMZ1dKhpg8xx0WlbUHjKTy', 'super', 1, 1),
(0, 0, 0, 'op00010', '$2y$10$aGqwqg/9Ye7tC2EkVzbSRe9cHMAc/9/J0U51EQ/dXQaLP9.y8FZZS', 'super', 1, 1),
(0, 0, 1, 'preneur', '$2y$10$8oU6DWM/EnLLL7QG5he6peTd6V0aTV9yAcP7yBTTvLdQITN.b98sW', 'master', 0, 1),
(1, 0, 1, 'super', '$2y$10$GuKNlmDXvss6HWnDS0PCxut.TaD33w7Xzi8BO71KbCB9Q794HzI/m', 'master', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_board`
--

CREATE TABLE `admin_board` (
  `rowid` int(11) NOT NULL,
  `username_admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vue_size` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'medium',
  `vue_color` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_board`
--

INSERT INTO `admin_board` (`rowid`, `username_admin`, `vue`, `vue_size`, `vue_color`) VALUES
(247, 'master', 'active_period', 'small', 'success'),
(248, 'master', 'ecoule_period', 'small', 'secondary'),
(249, 'master', 'stats_client', 'small', 'danger'),
(250, 'master', 'tendance_article', 'medium', 'info'),
(251, 'master', 'top_article', 'medium', 'dark'),
(252, 'master', 'sales_progress', 'large', 'dark'),
(253, 'master', 'last_added', 'large', 'warning'),
(258, 'super', 'active_period', 'small', 'info'),
(259, 'super', 'ecoule_period', 'small', 'danger'),
(260, 'super', 'stats_client', 'small', 'primary'),
(261, 'super', 'sales_progress', 'large', 'secondary');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `rowid` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite_minimum` int(11) DEFAULT NULL,
  `quantite_maximum` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `frais_emballage` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL,
  `description` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarque` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valeur_nutritive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `piece_jointe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '0',
  `actif_vente` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`rowid`, `image`, `nom`, `format_quantite`, `format`, `provenance`, `allergene`, `perissable`, `bio`, `quebec`, `nom_marque`, `nom_categorie`, `nom_fournisseur`, `quantite_minimum`, `quantite_maximum`, `prix`, `frais_fixe`, `frais_variable`, `frais_emballage`, `prix_majore`, `economie`, `description`, `remarque`, `valeur_nutritive`, `piece_jointe`, `created_by`, `actif`, `actif_vente`) VALUES
(1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.95', '5.00', '0.00', '0.00', NULL, NULL, 'Excellente pseudo-céréale naturellement sans gluten!', NULL, 'public/products/nutritional/GSSLKaSc5PSg4gmaRIdSnK3FSTehvSWdeJs5MkIE.png', NULL, 'master', 1, 1),
(2, 'public/products/images/aKxliPxIIy0PREv07R4UHxQsM6vQ5wgNI8O4V26t.jpeg', 'Couscous de blé entier tamisé', '1.00', 'Cuillère(s) à table', 'Canada', NULL, 0, 1, 0, 'Gerble', 'Aliments céréaliers', 'Aliments en vrac Quebec', 100, 120, '4.91', '5.00', '0.00', '0.00', NULL, NULL, 'Pour faire d’excellents taboulés… Yé!', NULL, 'public/products/nutritional/MthROLiRbthvhEaQKDR4s1qPV5CDrQ7Itf2rALBG.png', NULL, 'master', 1, 1),
(3, 'public/products/images/N0AvyopsFGqYKEbFV5t1Pso4YSV20M190CNfcO40.jpeg', 'Flocons d’avoine', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Aliments céréaliers', 'Noix Quebec', 80, 100, '3.43', '5.00', '0.50', '0.00', NULL, NULL, 'Cette avoine est roulée et torréfiée à sec avant d’être chauffée à la vapeur à 200°F pour être roulée. C’est ce qui distingue ces flocons de ceux des autres rares entreprises qui en font au Québec, et c’est ce qui leur confère ce goût de gras naturel de l’avoine.C’est ce qui leur permet également de hausser sa durée de conservation à un an sans problème.', NULL, 'public/products/nutritional/8I7DFLv4nW6zmVwVkWAUkyr5gtejjhzzY6SgMYlH.png', NULL, 'master', 1, 1),
(4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 50, 50, '19.92', '6.00', '1.00', '0.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes, dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température. Pour les desserts cuits comme les gâteaux ou les brownies, nous recommandons plutôt le cacao en poudre; le goût du cacao cru serait trop prononcé pour de tels desserts.', NULL, 'public/products/nutritional/p7PKD2k1iGJ1CLL5igoP8Sv2GyxvFHIJTdZPFBKP.png', NULL, 'master', 1, 1),
(5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 40, 100, '14.88', '5.00', '0.00', '0.00', NULL, NULL, 'Différence entre le cacao cru et le cacao classique : NousRire recommandons d’utiliser le cacao en poudre pour les desserts cuits comme les gâteaux ou les brownies, pour lesquels le goût du cacao cru serait trop prononcé. Dans des recettes crues comme des smoothies, des poudings crus, des granolas, des desserts crus et des chocolats chauds qui n’ont pas chauffé à très haute température, nous recommandons plutôt le cacao cru en poudre, qui a conservé toutes ses propriétés antioxydantes.', NULL, 'public/products/nutritional/SIljBYRTECmTu0cARK3YJQGdaiwOMKzmGB3HEH78.png', NULL, 'master', 1, 1),
(6, 'public/products/images/fxtpyETXUMXk2OswDJ3T1SparCXp9c4caKKNOa8J.jpeg', 'Capuchons de chocolat 70%', '500.00', 'g', 'Fèves du Pérou, chocolat produit en Europe', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 150, 200, '23.93', '7.00', '0.00', '0.00', NULL, NULL, 'Pépites de chocolat géantes, pour cuisiner ou manger telles quelles comme délice du moment. Environ 5 fois la grosseur des pépites de chocolat, elles font d’excellents desserts avec de gros morceaux de chocolat qui en mettent plein la dent!', NULL, 'public/products/nutritional/Xx8RvLJaxYXeNYale45l5G4WCbvOKrv54PdIUmiG.png', NULL, 'master', 1, 1),
(7, 'public/products/images/TmxisxJI9ocq2Ip8TQriNaer4WypYgn9Enuy9HBb.jpeg', 'Café La Montréalaise Santropol', '500.00', 'g', 'Amérique centrale et du sud', NULL, 0, 1, 1, 'Materne', 'Café', 'Grossistes en café Quebec', 65, 125, '14.37', '5.00', '2.00', '0.00', NULL, NULL, 'Café gourmet biologique, équitable, en grain et de torréfaction française. Un riche mélange corsé aux reflets de marron et d’ébène.', NULL, 'public/products/nutritional/stjSP5cjaLxUFLYOdMhmaB2M6t1cy6gsZGsdzdJh.png', NULL, 'master', 1, 1),
(8, 'public/products/images/yepAc3CrDZsNuFSxHG4h1hFbgYuqwsq7lI72nlSV.jpeg', 'Levure alimentaire en flocons', '500.00', 'g', 'États-Unis', NULL, 0, 0, 0, 'Ayam', 'Condiments et autres', 'Farine Quebec', 80, 120, '13.89', '5.00', '1.00', '0.00', NULL, NULL, 'Levure produite par fermentation naturelle, aucune source animale ni synthétique.', NULL, 'public/products/nutritional/oMVZlewVITN6Jx7GfMeiFzOUOr9hQe4YXg5vt0o4.png', NULL, 'master', 1, 1),
(9, 'public/products/images/7AzHZCs5UGaVzjEJETAw9z1XquORt6uMNrRyF99U.jpeg', 'Miso soya et riz sans gluten', '500.00', 'g', 'Québec', NULL, 1, 1, 1, 'Bledina', 'Condiments et autres', 'Confitures et gelées Quebec', 100, 100, '13.83', '5.00', '0.00', '0.00', NULL, NULL, 'Le miso biologique Massawipi non pasteurisé de soya et de riz est un aliment sans gluten très polyvalent; son goût est doux, rond en bouche et légèrement salé. Il est le résultat d’une fermentation naturelle (non forcée) d’au moins 2 ans. Pour sa fabrication, nous n’utilisons que des ingrédients certifiés biologiques garantis sans OGM.', NULL, 'public/products/nutritional/Ty12FzodDT1kbWEeKsEnqlXZwim1KggiKytU5P9l.png', NULL, 'master', 1, 1),
(10, 'public/products/images/I6XjF0Hwgg9sIX0tqhw7rrjiacSVk3oHcqy0DeXV.jpeg', 'Moutarde de Dijon', '1.00', 'kg', 'Québec', NULL, 1, 1, 1, 'Herta', 'Condiments et autres', 'Produits alimentaires Quebec', 20, 20, '8.98', '5.00', '0.00', '0.00', NULL, NULL, 'Moutarde crue, sans sucre ajouté, sans agents de conservation, sans gluten. Une moutarde de Dijon avec une belle texture crémeuse et une touche de piquant qui la rend irremplaçable! Pour les amateurs de moutarde forte!', NULL, NULL, NULL, 'master', 1, 1),
(11, 'public/products/images/uwhju93QU1tzQNJO5xXD9iu1JDSzw7w00tRcRFEe.jpeg', 'Farine blanche tout-usage non blanchie', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Sacla', 'Farines', 'Farine Quebec', 60, 70, '2.88', '5.00', '0.00', '0.00', NULL, NULL, 'La farine blanche tout usage non blanchie est tamisée pour en retirer tout le son et ne subit aucun traitement de blanchiment. Elle est produite à partir de blé dur de printemps biologique, et les grains sont moulus sur cylindres. Idéal pour la boulangerie et la pâtisserie.', NULL, 'public/products/nutritional/6cCibCA8qr7SRrQYZCsZimR5Ly3GKJUWB1d9bm9o.png', NULL, 'master', 1, 1),
(12, 'public/products/images/n5nqMXBrO1Lx3XhZ1l8ZCpQA5D7g08p9WHQq9tuB.jpeg', 'Farine d’épeautre', '200.00', 'g', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Farines', 'Farine Quebec', 25, 30, '4.60', '5.00', '0.00', '0.00', NULL, NULL, 'Depuis mai 2018, cette farine est tamisée de sorte à en extraire 10 % du son, ce qui lui procure un meilleur rendement en pâtisserie et en boulangerie qu’une farine intégrale.', NULL, NULL, NULL, 'master', 1, 1),
(13, 'public/products/images/1TpWugpuoiLsRiiFu3ToTpzfZcBhz8dRDzXDL5UL.jpeg', 'Farine de blé intégrale', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Fleury-michon', 'Farines', 'Farine Quebec', 80, 80, '2.88', '3.50', '0.00', '0.00', NULL, NULL, 'Cette farine de blé intégrale contient toutes la parties du blé. Le son et les rémoulures donnent plus de texture à vos pâtisseries et à vos pains.', NULL, 'public/products/nutritional/PSd1hLFB885sjyTw5jfEZAOjZTPm1zGah4LvzJ8x.jpeg', NULL, 'master', 1, 1),
(14, 'public/products/images/BPy3YmYrIv5JDNeLivnYByLod0geKywOHJRJOqtt.jpeg', 'Abricots séchés', '1.00', 'kg', 'Turquie', NULL, 0, 1, 0, 'Lea-nature', 'Fruits séchés', 'Produits biologiques Quebec', 20, 40, '13.94', '5.00', '1.00', '0.00', NULL, NULL, NULL, 'Il est possible de retrouver des noyaux dans le produit.', 'public/products/nutritional/0K5UCVCywUYqmbWHv79g8rJiwUQzyEKXh5BX6e09.png', NULL, 'master', 1, 1),
(15, 'public/products/images/17YmeaiQPt8j48kzvCWmxRqXAzBKtDD3r7r0TetF.jpeg', 'Canneberges séchées au jus de pommes', '1.00', 'kg', 'Québec', NULL, 1, 1, 0, 'Citadelle', 'Fruits séchés', 'Produits biologiques Quebec', 175, 200, '18.66', '5.00', '0.00', '0.00', NULL, NULL, 'Ces canneberges séchées sont préparées à partir de canneberges (Vaccinium macrocarpon) matures de première qualité et infusées dans une solution de jus de pomme concentré. Elles sont tendres et juteuses et présentent le goût acidulé et légèrement sucré caractéristique de la canneberge. En faisant sécher les fruits plus longtemps et à plus basse température, Citadelle parvient à préserver la saveur des canneberges ainsi qu’une quantité maximale de nutriments, pour le bonheur de nos palais et la santé de notre corps.', 'Le tout est 100% naturel, tant le procédé de conservation que les canneberges elles-mêmes auxquelles aucun agent stabilisant, ni colorant, ni glycérine n’est ajouté.', NULL, NULL, 'master', 1, 1),
(16, 'public/products/images/kPZuCChkJhrxRQ0YOhKAqSt6MaWVfGeLjc5eqwFD.jpeg', 'Dattes Deglet dénoyautées', '1.00', 'kg', 'Tunisie', NULL, 1, 1, 0, 'Prana', 'Fruits séchés', 'Produits biologiques Quebec', 80, 80, '12.77', '5.00', '0.00', '0.00', NULL, NULL, 'Youpi! Savoureuse et sans noyau, bien que, étant donné que le processus de dénoyautage est automatisé, il est possible de retrouver quelques noyaux parmi ces dattes.', NULL, 'public/products/nutritional/qQDAsd2DoU6n1C0ABNUWrrR14Qz0yj6xvi2IWj9W.png', NULL, 'master', 1, 1),
(24, 'public/products/images/fTxg5UHAz8BBGPsf5BpBRQGQTxPYOIXC4SudEjHj.jpeg', 'Ananas en gros morceaux', '1.00', 'lb', 'Argentine', NULL, 1, 0, 0, 'Dole', 'Fruits', 'Dole Canada', 10, 15, '3.23', '3.00', '0.00', '0.20', NULL, NULL, 'Ananas fraîchement coupés.', NULL, 'public/products/nutritional/IpcjqnKHeIaVqSkv4v3hDxKq9Pox9z7Na18ibPvz.png', NULL, 'admin', 1, 1),
(25, 'public/products/images/OOQjZ4U5Odwjl0jSsogURX1DPpwdlddQr3uMXpT4.jpeg', 'Pomme', '300.00', 'g', 'Trois-Rivières', NULL, 1, 1, 1, 'Aucune', 'Fruits', 'Fruits du Québec', 22, 25, '1.26', '0.00', '0.00', '0.32', NULL, NULL, 'Pommes du Québec.', NULL, 'public/products/nutritional/XkxhBIaOGI6hu9vvQMNwjWIHz2rDWT0ad0IIsLLs.jpeg', NULL, 'op00001', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`nom_categorie`, `actif`) VALUES
('Aliments céréaliers', 1),
('Cacao et ses acolytes', 1),
('Café', 1),
('Condiments et autres', 1),
('Farines', 1),
('Fruits', 1),
('Fruits séchés', 1),
('Graines à germer', 1),
('Huiles et vinaigres', 1),
('Légumes', 1),
('Légumineuses', 1),
('Livres', 1),
('Noix et graines', 1),
('Pâtes', 1),
('Produits québécois', 1),
('Sacs écolo', 1),
('Superaliments', 1);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `rowid` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`rowid`, `email`, `password`, `telephone`, `poste`, `nom`, `prenom`, `adresse`, `no_app`, `code_postal`, `ville`, `pays`, `province`, `actif`) VALUES
(7, 'zach@cegeptr.qc.ca', '$2y$10$d0EqxtI.yw4x07z1zfpxjuFYRDHtSGXHydrfiD3QA4MxM2xeJna7O', 'eyJpdiI6IlZ2Y2lGNXNKS2NnXC9UYmNLcHVlMTd3PT0iLCJ2YWx1ZSI6InRJc3N5Vk1PcDJ2RW9NMm9FbDNINjBuN2xiQmdjR0VIWHdPRnJPUit3aG89IiwibWFjIjoiNGZjZTI5NDUyNDhkNDBlYjM2YzI0OWEyZTYxYTg2M2E4ODY0NTIzYWVhOWI4YzY2ZWE4NDlmYWE4NDI3NjdlOCJ9', 'eyJpdiI6IkZ6c3E2RkZEcktrV0M4amlYRTJTS0E9PSIsInZhbHVlIjoid0JHT2JSQjRIbFl5M1A4YXlLWDNtQT09IiwibWFjIjoiYjM1ODkxN2ZmMzUxMjRiYzAxNDNkNDY4NDhjNWQ4YjgyYmY5MWE0NjY3N2MzNmIzNTEyN2FlOWMzZmJkNTRlNyJ9', 'eyJpdiI6ImNlMmRYOThPZVk1YXlPMlk2SDVidVE9PSIsInZhbHVlIjoiQzhFVEU0Z0JRSFViVW10UzVwMjRTZUVvdStWcTUzRmxlczBzdmNZMTV6MD0iLCJtYWMiOiI1NjVjNTFjY2IyMGM1YzY5ODEzZTdkZTVhZjc3ZWJmYTM2NWIzYjAxNTVhMTAyZDAyZmZiNmEzZDQzZDM5ZTRhIn0=', 'eyJpdiI6IlRydlpJbmpIYVVQZUVsTmJiYjZvdWc9PSIsInZhbHVlIjoibXNpQjRVSDhlZkVJcUpkd0V2NlBIQT09IiwibWFjIjoiNTc4MDE4YjYxMjk2MWM4NTg0MDI5NjIxZGI2MTJmOTM3MzNlZDk4OGU5Y2JiYWFmZjFlMGU0MTljMzQ4MDc4NCJ9', 'eyJpdiI6IlhGVjlcL042MmxzUmFJTllRZG5kMTFRPT0iLCJ2YWx1ZSI6IlFJelpsa3o0eTZYaGVlQ2h6bHZIa2wyTkFoT204cHZuSVpDY2ZqQkhWbEk9IiwibWFjIjoiNjJjYmUzMWM1ZDQ2YzNhMWQzMzNhZDk3YzZlMzFlNjQ2NGEwMTY5MzJlYTkxNzVjMDczYjMzODhlNzdmYzhlNiJ9', 'eyJpdiI6InB2UXNmbHI1QVUwTGt0a05oT0VHN0E9PSIsInZhbHVlIjoiRE5JbFwvNjlYZlJJOExNMlR4NVg3UGc9PSIsIm1hYyI6ImU3ZjZmNjdjMzZjYWFlMWE0ZDU4M2U2YTY3MzQwYWE0YmY3Y2U5NzU5MmU4NzNkNDMwMDAzNWE5NWRiY2I3YzkifQ==', 'eyJpdiI6IlVhTzRjcjZnbHFVTXc0ZmkyZjJ1Mnc9PSIsInZhbHVlIjoiWTZodWh4ZWExNHJaUm1zeUxPV2lnZz09IiwibWFjIjoiYWM5YTY1ZTY4MGQxYTg2ZWIyMDdjNDA4MTQ0NDZmOGFlZDAxNmEyZjgwOGJhYzZiYzIzYzAxOGZiNDk0ZDVhZCJ9', 'eyJpdiI6ImNNVE5TaVNJb2E4NUJKbk91ZVZwS2c9PSIsInZhbHVlIjoiMlFwU1NSc3BzNVI2MFwvNTM3eGducFNSR0tWeURsQzhWNjZ2c3c4OTllcDA9IiwibWFjIjoiZDgyZTIzY2JhNTMzYTEyNTJiMWQwZTNlZTI2ZTkwZDBlNmYxNTQ4ZDllMzhhZmEzZmVlMjg2MDI4YzQzOTI4MCJ9', 'eyJpdiI6IkNoRjBCQ05uTzRkR0VKV0k0UDI2VUE9PSIsInZhbHVlIjoiREQzaTREWUJ0R2RIKzNleHhpallrZz09IiwibWFjIjoiYTVkZmQ4OTA2MTZjZTcxMzc5YzllMmNlMjM5Y2VmMzlmZTIwMDhiNzhlNzI5Y2I1ZmY3MjhkYTdhNmJjZGMxMSJ9', 'eyJpdiI6IndNMFwvRXJHY1QzOVFWUzFiMkozRDZBPT0iLCJ2YWx1ZSI6ImhGNmJkM3FpVVwvN3BMTmhEbFRHOVRnPT0iLCJtYWMiOiI3NGUxZDg2OGMxN2I2MTAxMmNlZTJkMjRjOTg1ZDljNmRhYjFlMTkwNjg0MGNlODMyY2UwZGM4MzJlYjc5OGI1In0=', 0),
(8, 'jessy@cegeptr.qc.ca', '$2y$10$lkPIKheb7Sv.07CHAd4B0e6uJTIjvBmCVQ5TudANmAg8P92UbCVOS', 'eyJpdiI6IlFRN3VFbENXeFdQNm9RSzBCVjgyMnc9PSIsInZhbHVlIjoiMjY1WktjNFwvak5ZbkNtT2k5NmtuWFdKaloyMTBOaUZLdXo3YTJmWDlid1U9IiwibWFjIjoiMTE3ZmVmNmRlMGY5YmRmNGM0YWFjZWI0YWRiY2MzMzcwYTQwMTQ0NjRhOWY4NjFhNzQ5ZjhhMWIyNjAyODgwNCJ9', 'eyJpdiI6IkFwaEFRSFZXSkFvZVFVelRYTCthbHc9PSIsInZhbHVlIjoiRkI4N0pLOFoyVllQakxBVkFcL2JkV1E9PSIsIm1hYyI6IjBiMWRlZmY5MWJmM2MxNzRiMzMwYmE0ZGZiOWQ3ZGQzMGIyYjY4OTdhMjBhMjc1ZGQxOGE2NTAyMjZkYTE5MmQifQ==', 'eyJpdiI6InJNN3plTE1Ia0d1Z0ZCbkhvckhDWkE9PSIsInZhbHVlIjoiQzlPQ0NrS2c1STZ5VEF4XC9VWEFMYlE9PSIsIm1hYyI6IjE0NGRiYjhmMjY3OTRmNjQ1ZDAyNWM1Y2I2NWYzODkzYjg0MzAzMjgzNTYxYzE1MjgxMTdhMTEyZDI0ZTU4NGYifQ==', 'eyJpdiI6Imgrb3hSRXBWNCtIKzRvc2YzYXJKdnc9PSIsInZhbHVlIjoiWjk2SzdrM2Q4VExKTTJBck5RMEx5UT09IiwibWFjIjoiMzgyNDBkZTM5Y2YxNGE4ZWQ2ZjhmN2FmNjQ1ODJiNWY1YWQ0MjAyMTZmOTQ1MzFmNzdiODBlMjU5ZWM0YzZiOCJ9', 'eyJpdiI6Imp1YXl5QVNqXC9QYjVab2ZsV01hSUZRPT0iLCJ2YWx1ZSI6InhtZUVEV0RSNlwvZXdsT1puNlM5dUk5YThwWENzWmFtam1aXC9hMW9seVkrdG8rRTYzQlVHNXcxXC9EQnBreWZVUWkiLCJtYWMiOiI4ZjhmYTc4ZDNkOWNiODlhMzg1YWQxN2U0ZjBhOTJiZjc2NzVmYmE2MGJkNzg4NDRmMzNlODU3ZTBiZDA2NDk1In0=', 'eyJpdiI6IkNMTHpKbVFqeDFsZ1h1YjZMS002VUE9PSIsInZhbHVlIjoiQ0QxOW1kXC9QcnJ4V2RFVXZUMGhXYmc9PSIsIm1hYyI6IjE0OTI0YzQyM2JhYzQ1MGQ2ZWMyMmE1NjZkNTdjMjI4ZmM2YmIyNjFkNjg1MDg0M2QyOTBiMWMxMDNjYTQwNWYifQ==', 'eyJpdiI6IlhKSUMrSHF1N1YrZUJ6cXhzOUNwQWc9PSIsInZhbHVlIjoidEhhRmQ5cW1xTlpiQlg4bndPN2hwdz09IiwibWFjIjoiZWE1YWNmZjZmMzRmMzg0NTNlNjcxNzNiZWE4ODYwOTQ4NTcxNGU0NTJmOGU3MGNhODU4YTk1YjRjY2RhMmFmMiJ9', 'eyJpdiI6Ijd3WW1GUnowcE1FZURESWs5Skp6N2c9PSIsInZhbHVlIjoiSkQ2VWxOajkxa0FORU96NHN3eFBabzJpU2NDaGd1MVlHbmxRUTVHNVl0TT0iLCJtYWMiOiI4ZjJjODBiMDNjZjMxNTRmMThlODBjNTRjYTZjYzJiMDFlODQ1ZTk0NDk5YzJmNDU4ZjY2MGZjNWI3MjU1Y2RkIn0=', 'eyJpdiI6IkpnVTRqNXBOcGFXSUpPVGxpZWdka2c9PSIsInZhbHVlIjoiUFwvY05CWko4NHBNcE5ERmNqR09PNGc9PSIsIm1hYyI6IjA2MTllMzAxZGYyZDQ5NWZiNjFlYjI2MWJjY2RiYTVjOWI2M2I0NTAyZDU1ZGY4MjUzMThhNzdhNWJjYjAyYWMifQ==', 'eyJpdiI6ImZuZzhOaWxDNnNxblV5YTd1VXZmTXc9PSIsInZhbHVlIjoicURzNnU5VDlhZ3hGSEdjYjZoNkt5QT09IiwibWFjIjoiNzExM2JlNzNiMzIwNTY5NGZiNTM1NmNkMDk0NGY5Yjg2ZDU1MTM2NzAwNzE0ZDBkNjA0ZDEzOTcyNzhmZDI0NCJ9', 1),
(9, 'gerard@hotmail.com', '$2y$10$0POJSJlbXx3hBM6VNKASvOKZiPgaK4oJnUQdgoydwr5B4h.HCbpCG', 'eyJpdiI6InMwNDVcL1JXUGtHcVZaUFZhUnI2T3Z3PT0iLCJ2YWx1ZSI6ImpoRlJ4dnhMeHZWbmx2cis1NFpQaHNIblhkNk0xMVZSWFFUMFZyRlk1QVU9IiwibWFjIjoiZjg2NTllNjQxMDRiODdiZTU4ZGJlN2ZiODU3MTViNTBmMTU3Y2YxNWMzMTZjZGU0YTdkOWEyNjUwMWJkNzY1NyJ9', 'eyJpdiI6IkRCSEowXC9RNGpQZERWK2ZGRTBoUVd3PT0iLCJ2YWx1ZSI6IlwvNWRvbUtCQ2Nic3Q0TEJ2NEpTVUxBPT0iLCJtYWMiOiI3ZDUzNjhlODBmNjczZTM2OTMxZTNlYTBkMWVhNDc3NTMxODA3NTRhZWFkYzJjOTAxYmI0MGE2NGIzZWVmZDE3In0=', 'eyJpdiI6IlowMDdiQjN0a0pMUGRCUG0xelc5c2c9PSIsInZhbHVlIjoibzVuYWxjMW16SUFvNTNTWWNnbHZMQT09IiwibWFjIjoiNmUyNGY3YTYzZDFmNGRhYzIyYWQ0Y2ZlOTQxZjFiMThhNDllYmNlNTQ1NDU4ZWM3ZjE2MzZlNzZmNjI2M2QwNyJ9', 'eyJpdiI6IlZsNnZ6ZjFMaWxKR1RsQkxicFwvYUJnPT0iLCJ2YWx1ZSI6Ik82NTlHdjB5NHQ4SlpCUlBxU05GUWc9PSIsIm1hYyI6ImYwZjE4ZTg2ZWY3YjQxYzU0ZTgyMzM2NjU5MWY2NTI0NjBjMTZiMTcwMDYzZWNkOWI2MGNkYTc0NThlZjk3ZGEifQ==', 'eyJpdiI6ImRwaktZUWNyeXJFUHFmYlRTMHVkYWc9PSIsInZhbHVlIjoiMTJSdEZGM0RPOHBMdVd5Z1dnNXRqNWJtQmloTFh6SHZSWE5QeExxYll2QT0iLCJtYWMiOiI3NTU4MTAzMDAxOGFjY2M0ZDI1NjczNjgxMzA5MGI3NDcxNzdkOTVmYmI5M2Q1OWVjZTFkNjU3YWQ3MTQxMWYzIn0=', 'eyJpdiI6IlZQU2huVitQcTB1NGFoVmdrVnJBUkE9PSIsInZhbHVlIjoiMHR5bGxJSVwvUHFCbDhZcWpLbU5jTkE9PSIsIm1hYyI6ImQ0MGEyZTRhZWNhMzYwNzhhMmNkYTkxNGE2OGMxNjg2NTA4MDU0OGVmM2Y3MDg0YjQ1Zjc5YjhlYzEyNzc5NGUifQ==', 'eyJpdiI6IjNRazZEejNCVlpxUWpFNmJYOUxPZkE9PSIsInZhbHVlIjoiajJNZ0g2YWhuUElESU1vRE9MZ3V6Zz09IiwibWFjIjoiNWE2OTk4ZjcxNGY5ZmM0ODc3Yzk0OGY2MDIyNzZlZWYwOTVhMzgxMDc2MjQyM2Q1MGIxODMyNDRiNzY0MDM4NCJ9', 'eyJpdiI6ImJ6REZQSjB1MlwvQ0VuQ05OQlZhREVnPT0iLCJ2YWx1ZSI6InZrQnV5Q3Jkd0lOd0xRY1RacmxCd0x4NjlielwvanVZR2V0Q2lZa2x1STJVPSIsIm1hYyI6IjVjNzBjNzhjNDc2MDk3MjdmNzhhNjE2MDBlNzk4MWNkZjQzMDQ5NWM0YTkyNTllZWRmMGNmZDk3YmE2ZjllOWUifQ==', 'eyJpdiI6ImZ5UVwvZ1dcLzR5YXIwbGllY3dBMElHQT09IiwidmFsdWUiOiJGMk1kckh6ZWg2Z3ViS1Vra2I5Z0ZnPT0iLCJtYWMiOiJhODRlYmQ3ODc2MzllMGEzN2U5NDY4OGVmYTc4ZTI4YzVjOWU0MDNlYjEyOGNiZjI4YzMzZTcxODViNmIyNjdhIn0=', 'eyJpdiI6ImJEaTh3YzJXeHFiVkRiY2NcL1FcL1N4QT09IiwidmFsdWUiOiJvMmtUSjJHRGg0NmRyUDZhRFNNdnR3PT0iLCJtYWMiOiJkMDExZjU4OTkwODQ0ZjQzZjIwNmJmNzY1MjZjZmUzYWQwMWFkM2QxZWFhM2RjZTZhNjg0M2Q2NmRkMGFhNTk0In0=', 1),
(10, 'tarantule@hotmail.com', '$2y$10$V1JC3U38QM4XHdTgBKgwPOT3YM69jQXki61Am7Ht2LO/NBlVq8EUK', 'eyJpdiI6IkZ3MWxKRG9ZdGd4Mno5OURZdEhBUUE9PSIsInZhbHVlIjoiNlBaeTVWMEg2c25cL3JhYnYxbCswbVMwaStqcEpNcnVqck9MTjcyRXNXcTQ9IiwibWFjIjoiNzQyMTA0Yjc0MTRhOTk4NTE0MTY2MTFmZjFkZmMzYzg1ZDA5YzJhOTNlNjFiNjA3YThjODFiMjQwYzVlNjc3YSJ9', 'eyJpdiI6ImwrT1k5VXRTZ0NhVzFrNVh3XC9CbkF3PT0iLCJ2YWx1ZSI6ImpPU1lxcTJxWmgrY1dwRTZqUGZJbGc9PSIsIm1hYyI6IjM2MWFmMTAxODM4YjA1NWZkYTk3Y2ZmNDJmNTA4NWNmN2Q5MDQxZDRkZmU5Yzg5OTBkYTQ5YWUwZjVmY2Q1NmEifQ==', 'eyJpdiI6IlZTMnR3WUdBOUZxeGgzQkNtN2E0NFE9PSIsInZhbHVlIjoiOW4wSm5cL0hSaWFtUFRtM3hDdHJ1N1E9PSIsIm1hYyI6IjIyMDNhNjAxMmM2YTk2YmI0YTkzMjgyZTE0NjM3ZTdkYTIwNGVjNDQxMWUwNTdlOGJjZmY1MGMwN2I5ZjFkNGIifQ==', 'eyJpdiI6IjA3NVwvNHNtcFN5aWFzNHBtbGlXdDZBPT0iLCJ2YWx1ZSI6Ilp1endkQkVjSUF5Q0J2dzc3cjNGcWc9PSIsIm1hYyI6ImM0ODU3NjMzYWQwNDYxYTRiZGZmMWU3ZmRkZGM0MzE5NmY0MDJkNzUzYWNjMmE4NTc5MGNiYzFhNDYwMTRiYzQifQ==', 'eyJpdiI6IldlY29jUSswMHBsYWZaQXN1Zm8xWFE9PSIsInZhbHVlIjoid1pGZndHaVV4bHNGWTFwbGZIQjRoR2wxam5nY3hISkNsRVA1VWI2MUprNjFHejRqMDJWNk8wVnNTbDlMR3ppdCIsIm1hYyI6ImUzOTk3MzY1NTdkNjJjNjM4NzhhMDM3ZmU1ZDA5YTUyM2I0MmZmZGQ2YWI3Mzg1NWIyZmUwZjZmZGE5OGQ3NTIifQ==', 'eyJpdiI6IkZPaEV2ODBlb1NpblhrbjZGamRxcGc9PSIsInZhbHVlIjoibEpQc1hDTXpFMEYweFVQSk9oNGFvUT09IiwibWFjIjoiOGYxNmMzZDhkZjg1N2E4MjJmMjgzZjlmMGFlZmFiNjJiNjhlODQ0NWRiNjQ1NGExNTNiNWJlMGVjYzA4MDY2OCJ9', 'eyJpdiI6IlJXUnlySUhSc04rT1ZrV2NEQ0JSOGc9PSIsInZhbHVlIjoiOGQ2VG16dTJBT2ludlwvR2xxSnlHTnc9PSIsIm1hYyI6IjExYTQ0ZGJjZTY3OTQ0NmFmYThlY2U5ZmMzOTNhM2RiNWUzOTYyZjFhNjA2NmYyNTk0ZjhiYTVjOWZmNWE2M2QifQ==', 'eyJpdiI6IkV2dCs2bUpabGZGOEU4WmNXNVozRFE9PSIsInZhbHVlIjoiVEVtMDJ0T0NKUldubytZaFBBcEJ2cytZTERsV0pIcGF6a3FDMU1lcnJQUT0iLCJtYWMiOiJlNWZhNTg2YWRjODVhM2VlM2Y4YzBlMjViOTlkYzA0MDVjOTQyYTY5ZWVkZTc1Mjc0NzNjNGYxMTVkYTFjNjgyIn0=', 'eyJpdiI6IkdDSVNyT0xuNHZsREE5a2cwM2o2dlE9PSIsInZhbHVlIjoienF5TTJaYnlIdXlIZXk1S09JUU1RUT09IiwibWFjIjoiYzQ3ZWEzMjkyNmMxYWZmYWUwODUzYTllMGFiNTdhYjExZDQ5NWQxYzEyYmU3NWE4ZDRkNzUwNjA5MDExZDNjYSJ9', 'eyJpdiI6IkZRN0ppdjZGXC9OS3pNejZcL3hzbkh5dz09IiwidmFsdWUiOiJPak5RRE9TZGIwcStrd0JyQ3VCejN3PT0iLCJtYWMiOiI1MTk1MDZhMjc0NTIwNzQzMTA4NzBkODkyZjBhM2IwNjlkYjM2NDY4MGYxNjE4YzQ1ZTkxMjYzYzYwNjBlN2ZjIn0=', 1),
(11, 'hugo@hotmail.com', '$2y$10$05VGySnT8QUM/nyCwRqe..sWgZM2gpQexxtyBKO1rKFOz5k8CdS2m', 'eyJpdiI6Ik5NcG5yVzRFQkNKa3FKYkY0eFZrY1E9PSIsInZhbHVlIjoiNUF3clViWTlORjFvdVBTdzlNcWtLSWltVXA1cGVMV3c1amRkU0EzT3E5Zz0iLCJtYWMiOiJiNzk1NWEzNjM2YmMzN2Y1NzYyZmIyNzQ2YmI1OWU4MzBjNzc3NTg3MTc2ZDY4ZDlhNzgzNmZmMjZkZGZkNDUxIn0=', 'eyJpdiI6InQwMmM3OEo5NnMzSDZDUXFwQW85d3c9PSIsInZhbHVlIjoieHgwRjNZU0hyOWpGYlV1VXloVzJydz09IiwibWFjIjoiZmU0YjI4MzE0MGQ5ZDdlNjIyY2M2YWZlZTU1MzBmMWFjZTczMDAzN2RiN2VlZmQ2ZWI0ZmEwMzBiZWQyN2IzZCJ9', 'eyJpdiI6IlVWcUliaWNTTmxGUWErRm1HSHkzcVE9PSIsInZhbHVlIjoia2hqR1B6d2w0Y3ZoWkcwZGxjd01QUT09IiwibWFjIjoiY2JkOTk1YTI0MmY3NTBlMTNiYzJjN2JiYTM5OTA4MjFmNWRiOTNlYzQ2ZmViYWUzNmNkMGJmYzhmM2IyNDA3YSJ9', 'eyJpdiI6ImVuTzljaEpjVVNNQ0oxaUxVRnZyOFE9PSIsInZhbHVlIjoiV1BFMjM2Sm56U2Q5MDliUG9vejNKQT09IiwibWFjIjoiZGY3N2E4MzAyMjM3ODhiYzc5NWNkYWUwNWI3MTczMDI0ODI3MjVjNGJjODM5MTlhY2FmODZlNzhlZTY4NDU3MCJ9', 'eyJpdiI6InpEenF4VDdYaHN5YlNOQktQdlI0V1E9PSIsInZhbHVlIjoiSXNZTVBNZmFKU0taVzFhSFJxY3g2NmNGUnp3SFFtd0lRb0RNc1dhZ3RLVDRSTjBDXC8rNkNLYTVVR0twMkdGMXYiLCJtYWMiOiJkOThlNzdkNDZkNWM0Zjc4MjRjMzFiMzdmNDNlN2VhZDI5OWYzMjkyMjgxYjEyM2YyOGJlMDMxZDU4OWZmMzQzIn0=', 'eyJpdiI6InNnV2V5Q25taFJjdmVtZG43RXhlSlE9PSIsInZhbHVlIjoiWHJyMmlRakZyUUpIMFlWRzhqUm8wUT09IiwibWFjIjoiYTg4MGVlOWU1ZGNkMzAzN2JlOWM0ZjFhOWM0ZDBlZWQzZDgyMmRjZjJkM2E2OWQxNjQyMDBmNjA0NDhjZTI2YSJ9', 'eyJpdiI6IkR4bnVJYjJGMWttTjFnelwvaW5zdzFBPT0iLCJ2YWx1ZSI6ImZ3V2t2aTJtT0dOb0F3Q1wvTEpjMjRRPT0iLCJtYWMiOiJmY2I1YjQzZTQxOTI2ZGFkMzIzNTJhNGY3ZTBhMGY1YTRlMDRmZTRiMzMxYzQyNmMwY2Y3NzgwYzg5NWRlMzFkIn0=', 'eyJpdiI6InVKWWxmdjQzeHlWQnhwQVhRY1c4TlE9PSIsInZhbHVlIjoiRzNuSm5oTzhwNEl0eTNXdlBVc1drdFZaZDhPcG9meTdpTU8zekJlMEpOYz0iLCJtYWMiOiIyMTc4Y2Y2ZmI2YzA0NWFiOTkzMWY4NTIyN2RkYzY5ZmRhNzk2YzAwMTEyMTU0N2UwMDM4ZWEwM2NiMTQyNzM5In0=', 'eyJpdiI6IjFEUXhETXVyRHB6TVhucUpXN1NFY3c9PSIsInZhbHVlIjoiWWY4aEZyaU9uYUErM0pFZ3Npa1FCZz09IiwibWFjIjoiZmJhNGNmYzc4ZDUyZmJiM2M2ZjMzMDJiMGViNmNiOThiYmVmMWE0MmNlZGViMWUyMmQ5Mjc0YjA2YjJiMzU2NSJ9', 'eyJpdiI6IkluR240Sm5cL0pCWW5QNEpyM1wvMXZXZz09IiwidmFsdWUiOiJsNlEzYVJXRGtXbWNsQWdSczM5RTZnPT0iLCJtYWMiOiI1NDkyOWE4YzhiMWZkYWZlNTY5Y2FiNDY5ZmNhMzYwMzc5ZmE5YWE1MjVhMjE3OTA2ODlmYTFmMjM4NjlmMjcxIn0=', 1),
(12, 'banane@gmail.com', '$2y$10$cm7rgagdT5SMD8j3FJrjWu7OYnNnjRXSd1eFeqA7tYMqUs1i1nJAa', 'eyJpdiI6InlaWXlnbVwvdmhyeU16K0NuUlBYZlZ3PT0iLCJ2YWx1ZSI6IlBEd0lDemNtMlVMNlhPZDdOWjU2SnFiR2NGaXNKRTJicWdBZGpmWElPa009IiwibWFjIjoiM2Q2ODkwYjE0ZGE4MGQ1Y2M4MzJiMjQyYmQzOWQzN2NhMTY2NWI3MWMxMGIyZDc1MTBiNzIxZDA4MjRlYzMwZSJ9', 'eyJpdiI6ImF0bEhzN2hFT1RvZ2ZBRG1qUW9cL2FRPT0iLCJ2YWx1ZSI6InBiZko4anp0amhLdEhxVFBkNFFjTlE9PSIsIm1hYyI6ImM3NTE5MGZiOGRjM2RjY2UyNDBhZThlNmZlYTRkNTA5ZDMxZWNlZWYzMjJhOTMwMTdjYTg3MzNjMTMzZjYxNDIifQ==', 'eyJpdiI6IjEwdTNKR2NURDRQZ0VPeGEwcXY4cmc9PSIsInZhbHVlIjoiRnFTcHVTamo3Q0hRSEFRK1dDVVIzQT09IiwibWFjIjoiMjRiOThjMjllY2M1NDc3NTVlMjhhNDY1ZDYxMGIwZjhmOTI5YWE3ZmE5ZjI2NzY3ZTU2YzQ1YWE1ZmE0MjJiZiJ9', 'eyJpdiI6InRCYnVXSUFoaWxqbVgrcE80QnBRUFE9PSIsInZhbHVlIjoiRXdnRmFcL1o3NnM4UmlCS3VmYVZaM1E9PSIsIm1hYyI6ImFlMDJhYTIzY2VjNTIzNTMyMTk2MzgwMGJlNGYyNDZmYWQxM2FjZTY4M2RlOGY4NmJmYTlhNTdkYzAzOWUyODYifQ==', 'eyJpdiI6Im1BZm5iXC9KQlRDUUpjRzA3cGRYcEVnPT0iLCJ2YWx1ZSI6InAzczV1dGFFN1VqcXNIT24zTkY3K1E9PSIsIm1hYyI6IjQwYTg4MDY5MWRlYjk3Y2E1YzkxODUwNWQwZDY4YzBlMjAzY2M1M2YyZDI1MWM0M2NhODYzNzI2N2Q1MmY3MTYifQ==', 'eyJpdiI6IkJCOFNlN2RwU2JnSmhyK09hZnQrTGc9PSIsInZhbHVlIjoiS3NRM2kxZUphYWYrSUZBQWxSYVZNdz09IiwibWFjIjoiODc5OTMxMjE1MmM5ODg2OTcwZjU1ZTVlMjE3ZmZiMmE1NTg4ZWNhYjAxZTEwOTEwMzU5MmJiMzU2ZWQxZTRkYSJ9', 'eyJpdiI6ImZIc1BlVXRPbk1LZlhSMmI1WU0yNlE9PSIsInZhbHVlIjoiQmlMeFRsTmhlckpyc1NMUHlHdkJTdz09IiwibWFjIjoiY2JjYjU4YTQ3NThhZDQxMzFiM2ZjYWQ2OTdjYjg1NjBjNjIxYTcwMjViNzQzMzUxNGUwYTJlYTcwZTNlYTY5MCJ9', 'eyJpdiI6IkdwSlVYKzhRUTdjS0g0WElEcVE2VGc9PSIsInZhbHVlIjoiUmNhRHMyaEU2dnpNVElSM29mNk41UT09IiwibWFjIjoiMmY2OTBmZDUwZDk1NWM1NjI3ZTQ4ZjdjY2Y0MzQ2Njc1MmNkMDJjYjY0ODBlOTc4NTNlZWY4NDY4NTRiZjA0MiJ9', 'eyJpdiI6InhuWVNVYlQzQnFVTDN4QjgxQUdReXc9PSIsInZhbHVlIjoiUGp2V3E2aU4zbHA2OFpQaFB4RTIzQT09IiwibWFjIjoiZTc2ZmYwNzE0Zjk4NzdjNmRmNDM1ZDcwZTk0OWEwZjYwZWQ4MTdhODExOGQ3ZGQ5ZjBjMjc4ZGE5YmE4Nzc4MSJ9', 'eyJpdiI6ImplZVJlcEJUOHUxakhvYUV1UkhEdkE9PSIsInZhbHVlIjoiNWR5NWJcL09ZR0F2TWQybTg1K0l0MkE9PSIsIm1hYyI6IjY3YWI4NGM5MTc3MDA3ZGM0ZDA0ZTAwMGEyNjY2MzcxZGI1OGM0NjlkZDgxMjIxODU5YzAxMjRjNzA4ODY4MzUifQ==', 1),
(13, 'banane@email.com', '$2y$10$5khBHUVtcXJ9XrMR4Z1Fbejpe7PZHKO256lZvUviece.YnA6DMt5K', 'eyJpdiI6IkdUSnpPdmVYRlljR1BCSkJhYWpXUUE9PSIsInZhbHVlIjoiYnNtQUxuR3hQaVlkNlBGODlJOCtnd2tSelB5U1FNVzVTaVliS29OZnJ1cz0iLCJtYWMiOiJlZjNiNzdhZjQ1N2YwZWQ2MDM3YmQwNGY2OGQ4ZDZiNzc2NmExOTA1Y2Y1NDQyMWVlYjY2Yjk0NDdjYjMxZjdjIn0=', 'eyJpdiI6Img0S2VlT3JZb2xQVWphYjk1dnNqbVE9PSIsInZhbHVlIjoiNDVsVUJmcm53TUpIS2NMTmRhbDFyZz09IiwibWFjIjoiNGY0YmVlNDc1MTYyYTFmODhmY2VjNDc1MDY0ZTNiYjc2YWM3NGYzODM0MDJiYWNiMGRiNzUyNjcwNjI2NzAwNCJ9', 'eyJpdiI6InZPTjNGenNIQjJiSWZIRWdXb2ZNU3c9PSIsInZhbHVlIjoieWxOdzRkUGM2MUM1bWttK1QwdmhUUT09IiwibWFjIjoiYzMxNjQ4ZTk5OTRhYjI4NjAyM2M5YjBhYTY1N2QwZDM2MmU0NGNkODQ4NTRmMDQxNGYzYzNhNTk2YmM0Mzc1NyJ9', 'eyJpdiI6Ik84ZUNJUm9DQW1mdW5BTkg3bUd4K2c9PSIsInZhbHVlIjoiRjdUNEdZRUJxT1llVDdOeFwvQUN1YlN5NllXc25HdmRFc01oVHQ2dllJa2c9IiwibWFjIjoiZDQ0ZmI5NTljZTJkMGMzMWM5OWY2YzkyZDg3MTE5Mzc1NGUyODQ1Y2Q2MGY2Yjg4MGMxMjdmNzA1ODVkNjUxNSJ9', 'eyJpdiI6ImZkK0Jpc3orek1aa1J4b2dzcFhicmc9PSIsInZhbHVlIjoiekM1MTl5WU55YmJ0TDZoWVNrUTBQTDJkZHdWeStDRXJ6Q2VBQ2tKOE1KbzladCtCdkoxeExsVzhNUFVlTVgwSyIsIm1hYyI6ImFjMjk2YmFiYTA1YzQ0NmY3MWNlMzRkNTQ4ODgwMzU3MGZhNzAzNGRhY2FmOTc0NzE5Y2I2OWUwOTcwYjI3MDAifQ==', 'eyJpdiI6IkJOM21ld3c2UkU2a3htOGh3d3crRXc9PSIsInZhbHVlIjoiMmdPY0NJSzM2cXJIRmlQVndwMmFUQT09IiwibWFjIjoiODA0NTQ4OTdhZWFjNjY3MGIwMzk0YjM0ZTljZjM4Mzk2OGVkYzZhYmVlYjk5NmFmYmFjOGM2ZTViNjFhODRjOSJ9', 'eyJpdiI6IlpScWNrYlZMRE5LU3BQXC9aSEYxNTNBPT0iLCJ2YWx1ZSI6IjUrZVU0eXNRaTVOTkkxXC8wVlFcLzlVdz09IiwibWFjIjoiYjUwYWUxYTdjNTUwNjI5YTUwY2Q3MjA2MjVjOTNiN2VlYmY0MDE4MGIwOWU2ZTk4MzEwODIyYzkyNDcxNDFiZiJ9', 'eyJpdiI6InNoaFhsd3ZLbFwvQUswQjc4OGY3WU9RPT0iLCJ2YWx1ZSI6InJzZ2UydkFNeGNyT2o1eXdOUUordVE9PSIsIm1hYyI6IjMzYTQ2NzdiOWZjZWMxOGEzZTljOTcxMGY4NjYwODI3MTkzZWIyZTYwYmMxMmMwNDhmMzc2MTYzNmVkOTIwZTIifQ==', 'eyJpdiI6IjByMXpcL0FrT0RuSmxDaHhQMmdrSGh3PT0iLCJ2YWx1ZSI6IjErR0o2R043RGF3ZlBERUY2bWwwYXc9PSIsIm1hYyI6IjQ4ZmJkNTBiYWVlYTEwNzhmYzFkZTczOThkZGQzZWQ5OGU2NzYzNTE0MTNiMTA2OTBmNTVjNmZiOTE4M2Y3NTQifQ==', 'eyJpdiI6IndvUGZBcnNiWEFBN095M2hNYkZxelE9PSIsInZhbHVlIjoiV01sVW1rR1pBeGNuMElJZFgrRzZOUT09IiwibWFjIjoiZWVlZTUyODdhY2FmOTRmOGFjODljMTY4MzM2N2UwNTQ1ZWRiOTA1YmYyM2ExN2Q1OGUwMjYxMThmZWQ2MDI4OSJ9', 1);

-- --------------------------------------------------------

--
-- Table structure for table `commande`
--

CREATE TABLE `commande` (
  `rowid` int(11) NOT NULL,
  `rowid_client` int(11) NOT NULL,
  `rowid_periode` int(11) NOT NULL,
  `nom_client` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sous_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `date_commande` datetime NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pays` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify` tinyint(1) NOT NULL,
  `facture` blob,
  `identifiant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paye` tinyint(1) NOT NULL,
  `prise` tinyint(1) NOT NULL DEFAULT '0',
  `signature` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commande`
--

INSERT INTO `commande` (`rowid`, `rowid_client`, `rowid_periode`, `nom_client`, `sous_total`, `total`, `date_commande`, `email`, `adresse`, `no_app`, `ville`, `province`, `pays`, `code_postal`, `telephone`, `poste`, `notify`, `facture`, `identifiant`, `paye`, `prise`, `signature`) VALUES
(20, 7, 1, 'Zachary Veillette', '98.37', '106.66', '2019-02-12 20:11:05', 'eyJpdiI6ImdTWWVJQnVxRHBDVmVzWkpUSmFYZHc9PSIsInZhbHVlIjoiQXhIY0lCZXdDeWNJdjhWVmpSMHFMMFwvODVBMVlHSkFDSXljTjd5RitIcUk9IiwibWFjIjoiMDMwYmQxOGI4MDEwOTQzYzg3ZjYxZjI1NzQ3ZjM5NDljN2FmMTM5MGUwYjJlOTdhMWEzNTFlMWMyODM3MDNiZiJ9', 'eyJpdiI6InAyYWhJMGs2ZW4rb2owc1wvSDVMZU1nPT0iLCJ2YWx1ZSI6IlhqZWxndVJnc0hSR1B4eVwvR281eUJVUDk4NktSWnduajd4R0NSZ2dDRVFrPSIsIm1hYyI6ImM3OGRiZmU5ZmNiNjZiZDJmNWI3Mjc3YzFhNTNjMzIyZTExOGZmMjM0ZGJjMzkxYTJmOTRhM2ViNDNhNjIwMzgifQ==', 'eyJpdiI6IlpoTGdmQmJxdXZTTUg2TWp2dlBvRVE9PSIsInZhbHVlIjoiK3piREhNUUhmZDBRTlwvdkJuZFNBZVE9PSIsIm1hYyI6Ijk1NDY4MzRhOTA5MWM3YjViYTJhMmJkNTE4ZTc4NWM4OGViYmU1ZGRhYTAwZmM2MWU5YzIyMjUyMzhhOTA5ZTkifQ==', 'eyJpdiI6ImtsY3dzaGNoMkVBYno5VE91NzJpYVE9PSIsInZhbHVlIjoiM2R0NmpcL251S1JQV2hzVk5KdGhqMnFOUVZrVndET1hBdzZMd1o1NldOY2s9IiwibWFjIjoiMDg2ZjM5NDRiMDg1OWE0MzFkYjU1NTZkMjMzYjAyNmVjOGY0ODM4M2RjOGUzNWFkZTA1MzkxMDNmOGJiMzVkMyJ9', 'eyJpdiI6IkxmVXZQRjFnclh2aUREOVJrczVOZUE9PSIsInZhbHVlIjoiUmY1dXdyMEExbGVrSENYa3V4TytkUT09IiwibWFjIjoiMzFlMDVlZDMxMmViN2UyYTkyOGM0MjY5MjY0NmJjMTE1NWEzZjRjZTg0MmM3YmU3M2Y0MjBlZWUwNWViYmIyNyJ9', 'eyJpdiI6InBsTWVadkJxTkF3amdxcUg0SEZVd2c9PSIsInZhbHVlIjoiM0lDYmVsYjNKMTg4UExlZ29sSFFOUT09IiwibWFjIjoiOWQ2OTA1ZTYwYTkzYWVmNDcwNmU0YWZhMjAwYjc4YzM5MDJlOWQwNjVlZTM4NTM5Nzg4M2MzOTFkMTU2YzRkNyJ9', 'eyJpdiI6InZKbkFOZGwxTlRPRFJVV04wNEczamc9PSIsInZhbHVlIjoiNGQyVjZTTGhiaEpSdjZcLzRmN1pGOHc9PSIsIm1hYyI6IjIxMWYzNzJkMDBlYTY0YTE4OWVmZDM4M2NhODUzZTU0ZGIwNWNhZjU4MmY3YzVjN2Y5ZDQyMDY5NzkyYTIxNjAifQ==', 'eyJpdiI6InVBVlwvUk1sTGJESHZqZTlpdmtoRytBPT0iLCJ2YWx1ZSI6IjYyR0dcL1RaVTNjaEdmRFFwdjB1Skl6QWNVZTVadE1cL3FmdG9PdlNYOU91UT0iLCJtYWMiOiIxNGY0ZmQ5YWEzYTgzYjk5MjAxNzFhMTYxYzc4YjIzYWZiYWFhM2YzMzNmODFjMmMyNTM5MTNjZmJkOThkOTMyIn0=', 'eyJpdiI6InJTdzVCME5WZDFlUFBCbGp3RFF5ZEE9PSIsInZhbHVlIjoiN2d0NWRMcEFYUEZJeEJabHdKcCtIUT09IiwibWFjIjoiMTQ3MDJiMGY3ZjA3OWZjMGNkZGIzZDViMDYyNDY1YTJkN2ZiN2IxMDZkOTE4ZGFkMzQxNWZhZWVmZDc5NDY3YSJ9', 1, NULL, '', 1, 1, 0x646174613a696d6167652f706e673b6261736536342c6956424f5277304b47676f414141414e535568455567414141763441414144494341594141414351326555674141416741456c45515652345875326443667839587a6e7650365a4c5249617251616f727856575271534b4542686f6f4b564d704b6970454b426f6f77323265532b342f59364e5a41366c304a5a5378534e496b446553476b6b4b6b71344858752f61366c745865352b777a375833324f652f6e3966712f3648663258734e3737652f657a3372574d37785046416c493442514a76446a4a465a4d3850386c56543347437a6b6b43457044416e676838624a49374a726e7a447532394e636d483748432f7430706745674c764d306b76646949424355784634484a4a66697a4a463351642f6b57536a357571632f75526741534f6a73427a6b6e78796b75636c2b654b6a47393238412b4a39435a65504744474d78795a3556704b2f54664b70536236772b2b3943535a36623549496b507a4f6948532b52774b77455650786e78572f6e457467726753736b655861536933577476694c4a4a2b36314278755467415357524f446d535a37514466686453533665354f2b574e4945446a665671535236663550494437543831795832532f45475366362b752b52394a766a4c4a2f5a4e6756506e4e4a442b6635426b484771664e536d447642465438393437554269557743774755666a3543483933312f6f5a71417a444c674f785541684b596c6342584a4c6c666b7374576f376a6f6d53762b31303979373835693379374f69354c634a4d6b72423162742b37702f2f2f37755863734a77474e6d585745376c384157424654387434446d4c5249344d674a386b4c366c5576722f4f4d6c3364682b6e49787571773547414243596767475561692f58584e483264367a636656353566532f495a506578666b4f536d5356367a596c312b4f4d6b336431622b483144686e2b414a746f754445546a586c384442674e71774243596d67432f2f3979533564746676507952355742492b546f6f454a484365424971692b76596b483941682b49584f54655763694b446a3343584a665a744a342f62303069533353504c43465542756d2b524875742b7837764e65786356486b63426943616a344c336270484c67453875564a486c646c6b6c4470393647516741532b4e636b6a656a44676d34377966793543514450754f3332437938387176337979383343536572736b48395a6c2b336e777559427a6e71644e514d582f744e6658325a30756753736c2b644e6d6576696561756b2f335456335a684a59527744586e702f754c6e70616b6874554e357a54392f36526e66746a792b744253653665684a4f51495348394d554739434736544d5079626465443958514a4c4958424f4c344b6c72496e6a6c4d4136416d5474496156634c527848333337646a66347541516d634c4148632f6c4273385750484c51552f2f354c576c38422f306b2b65756d43702f2b654253664c654a4f6e424b766e474a442f6158634470794663315758314f6e5a2f7a4f774d434b76356e734d684f3861514958446a4a57356f5a6361785038526c464168493458774b2f30536e3635655376546b4f4a4c33744a36336d71684b6858387571657958487963614d6b373177783851394d5167725036335458594551707676326e797374356e536b424666387a5858696e765567433739397a525031375361362b794e6b3461416c495946384548704c6b4f37704d586c6a32363644556c7954424e66435568594a61754f5730386b6c4a5872356d347554302f2f33716d6b3866614f75552b546d334d794b67346e3947692b315546302b41594c54724e724e3458342b6946372b75546b414375784167434255726635323135772b72314a576e4876767a38514f35393748692f3973617341394e387533564e57506367585a5a4b2b2b56774f77455650786e587749484949465242446832786f705843376d707965536a5345414335306d674b50326b6d507a7554766b762f77595258462b756b75524e4a34716e4c335050723363754f375772557a763953795635626657504c30364370583956304f2b4a496e526135305a4178662f6356747a354c7045412f76766b35712b46447834664b305543456a686641685364496f693374756f2f723150326f55496850367a617079696b32667a485a6d4a6a616858634c4d6c505666656457357254553377576e4e4d47424654384e34446c70524b59675143352b702f593950765a6a552f71444d4f7953776c49594559436c306a793843526630536a395835726b6c37747859526a415148434b67753543456135614b467032687a57542f54395641432b5858692f4a72353469494f636b675345434b76342b477849345867496f2b4c2f62444f2b43726e5438385937616b556c41416f636d634f736b503946563471324c6370584d5076522f71723739784457314758725770544f2b534939623545636e65654f68463872324a584273424654386a32314648493845336b50674d354d3876344642455a6d50455a41454a48445742496f50663575622f3075366c4a5441495a4d5071536c50736641554a786c58724a36413330377965537565694d736d655658312b3975536647695364357a31552b546b7a35614169762f5a4c72305450324943564e2b395a382f342f487339346b567a61424b59674d44584a3646754230722f4461762b57697634747958356f516e474d3355586262775456597076506a41493370632f31376c446c5576756e4f544255772f612f695277544152554a4935704e52794c424e376a6e3475666269755854504c5841704b41424d36574150373844306a7939306c515946482b69397774795832362f2f47794a46633451557158532f4c6e31627a57576671666c655261316657636f76375243584a7853684c596949434b2f3061347646674342794e776f6535346e757154725878746b34586959494f7759516c493443674a2f4d386b50356d45754a38324338334647356565557978413956474e502f37664a57486562594176692f66666b2f42374c64782f71696c4e6a2f4b4264564448533044462f336a58787047644434476855764d51494f30636972386941516d634a7746797a6c4f686d314f2f4c307279617732475a33622f7a6a2b5474352b435673636b443078432f4d47396b7a7868693446526941752f2f434a553647567a3079655853554a4e67316f2b4f4d6d2f6274477674306a674a416d6f2b4a2f6b736a71704252466f41382f616f66733375714446644b675332444d42676c442f7157767a6c6b6b653337542f47556d6f306c766b38674e5662506338724e484e506256542b726e686e3776306f713169767171783930767942315556346c64326762313946586e62314d632f6e2b53727257772b657132383845774971465363795549377a614d6b384d565644756d2f54494b31716859723878376c736a6b6f4355784334454f36416c556f762f644b636f2b6558736e616738734c6775582f75704f4d6246776e78435367664e64432f4e4b766a4c733942437a2f544f666178433055362b4930677869485774426a3770766b4c74552f3369374a6a3437737838736b6346594556507a5061726d64374245526f4b4a6d795336425661774e364c316f6a352f71455133666f5568414167636b514937354e3354747637524a58316d36725130482f4274785172564c7a41474874375a705968484975454e5634534a59376a3972375a332f656345334a36456f462f4b7a6e63746a6d372f2f776b6d656b2b5454716e6174617234425a433839507749712f7565333573353458674c387a564539387472644d43697977332b31664837334d5a74337050597541516e4d51614374537676666b727939476367484a4b6e6458663758514172674f635a506e316a3673666a5863754d6b54786b356f4a736b65585153574a4357394f34393931485435485856767a3876795532542f4658507457796b727462565153456c366975532f464b53467a55352f6b634f7a38736b734677434b76374c58547448766a7743376366366d6b6e756c4f51473156532b7477754357393773484c45454a4c41724162374a704f3074376a74444b53674a2b72395a31646e373931537a3358557332393766702f5254585a687352474d457852796c48324544384b53656d7a34314355472b52573666684f713972587842562b6d38335954553178467a674b7551726b466a56736472466b394178582f78532b6745466b49413135335856325046416b574b767564572f3862483774594c6d592f446c494145396b2f674e5a5637544a7532732f5457707262386d7334565a682b6a2b62496b7635376b4c567330686c73503951527532397a3766354e3830776a66666e7a365563432f7537736656795a4f5232746867304d4e41363472386a6c4a6672653537684a4a79435930564e79726233716376464938555a484153524e5138542f70355856795230494171394e7664474e422b6239303875374174547246484d473974542f736b517a645955684141684d52654861534c2b7a362b73596b507a3751373738332f373676372f6a446b6c415a397a464a627258466e50484878792b2f465254776f73797661765a2b56594475395a4d386f376d59744a7950724d623244306c512b6f6d42714958556f52636b2b6469657a6a684e776332483979337a4a48443675354b77796141675775472f7866533952514c4c494c4376463859795a75736f4a544139676271695a703131342b564a507245627a6c75546b4d46446b6343784576672b726145485852704f2b334278515842747763576c54366a7055616630334665784c767a6f37394231694e57376a5474614e586c4f4c312b636846504e5673673668432f2b4f73464e70357755334b496e337a384b50705636697a7939382b65766a53656356744447395a724f7150614c6139526a65334c3863796e5a676a3473435a562b72374e756f50347567615554555046662b676f362f6d4d6d38494971323853334a336c344e31677365626570427637683363666e6d4f6669324d3658414572644662764b306c6336587777486d7a6e5a76636a795657546f753478312b6833566456536e37564f324e78316f6d33617a375a394e333544537a4c33387a764f424549524d4c464d52336e4e55484234533573536d423255664952662f6b36754c795654307636744e4554383971484d70716c6c77326b42676238304446794f436e6c6635376a4e324e6a6e342b524e382f4d4a4e34586d39424a5a47514d562f6153766d654a644167507a37484364544f6834684349393831416a7550506a7846766d674a503976435a4e796a47644a6f453472715376452f682b424f6d556c72654d43324c72796c46374a6631386e4173424b76593076666a304c336b31597734765537797069427a425738417a3056515175536e4f3546305737647138686f42665849617a2b512f4b794c74614a3339745968553949386d664e6a64517975452b5364795568774a66596754616d6741304247355676574c4e637451736d4c6a343833346f455470364169762f4a4c3745546e4a68416d324c756b6c3257446f5a425772352f53554b41476c4c2f4e764577375534436f776a55475661777648374c714c753861417742616e6638636e5868717377383758756c506b4563303166664e5a644c6768744d6b5671352f38476d594669722b424f7a684f4b4d6f44447a624e5357662f35394b44695a33394139534c764a4f784268672f434937762f6e464f414a5864586465747a55414b4157414e4a754f73703175436d526f724d76453144645668325059464476746b2b51397932536749722f4970664e515238706755394a3869665632443479795a75726a786b5a6643687367354231346d2b506442344f53774b46514b33676152586433334e783155714a7056557939627870526650374475697454334a4b74775450346a50664636544c32704d6e6e7942646c473445697a362b39735163634d4a35325772385a4e6e424c3739505770656c656f507767514e46794969486f672f3839776b574c713546706633486453634237516c42322f396e644a6e544f476e357661342b674a622b2f543358747251414169722b4331676b68376749416c6450386a7664534f2b6468487a38745a4135346748645035444763393048616847546470416e546541795454436b7a2b312b6c68756c6c62694a49696a6862317a524e4f3473574e534c394b57763347526b66636f31626a4834317639716b7173306a61456e744d473157507466306f306248337179454255683967433348644b43396b6d396d5551424a774d50636f306b7639567a517a6b4a5963504269524f38696a414767704c484b4f2b4d73666a3773326d42365a6a374e6d487274524934656749712f6b652f52413577415152756c2b525233546a35694c5570376367555156456233487977575033704175626b4543565142353253427247345a55686d65774c452f39535766647874587257694f594a6279667056424d5734754e68734d777179682f317a63795078527754673469614441614d573467673441536a42742f79475a622f454b6432777134426233304f476f4738624742794b4e70584a455a543473714570416554316263512b4d58394f4f796a576855392f4c5a7534364a5373515151536b3871544463437132494e743248715042425a42514d582f4d4d7645692f6b4e3351753735475a2f62526367645a67656258554f41675469386348387571357a4c4539384a477668772f35485862713461362b776773307866767555774241427250745955336e4745647738726961756e5169307761706a556e4769704e6570666e644e427443364450316245764c652f32775358424f4c764c4c4c73494f2f6643333171553937496c537547347056774469436b5151685a7a2b706a693838454b444d65446731774642435459473638753554753430466d58685743617877545370467564676f3843346d53467152774e6b535550486662656b3565735236675041534a46674c502b38366e566e64773939337670775544696b764c594b3265426e683838304c695130435236584b63524e676a516b302b3752756d506a735072385a4d685a534d6c30676650417343582f63612b726f2f704d41775a476b4e797843454f714e424c5131415a5268556c305736587466744932546c37354f62666d356c5476684e675042466247313647504a722b73436c486178734a6433472f2f4771515475507158364f436342354c397635664a4a3244533055727336467157666d414a4f5156764274596c4e5168764179336352525a3776357a71424656562f4f54464171505a4c48494466316e586b2f50336b43616a3462372f45745a2f693971333033386c784b43386f4c4735734c4d6f6d67534e536a6e72315339773338633361512b6e48462f616133573139487a732b6a4b77644f6672766d75542b6d33586831524b596c5542724753614e3472316d486446794f3066424a65316d4f5431422b536177644a56677261364c55354549414f5051746f4c7254616b6a55747234755352664e614a424b7670535a36413845387a6a6e54333363584c77744a352f72394e6d6c694b473149506f63336e387a4d3451566866306f736c4e3348724933563969724267504a776537706a306467636c4c4a4c414d4169722b32362f544952582f64614f6956446b66595878776c576b4a584c7a79445831326c316637626330512b4c76434e594b50474146756c4950762b31424f4f334a376b38413441706575546a4c4c485a2f63424b534f61386d72534f464c494839782b61547962703033663467517034653850347173537657356a6a497557722f6658455363516533614d3952476e554b7a584d4d377252673979723968324d4441305572394c5031456c3175666b77534b47376243753555546859636b2b5937717837464b5038572f6e6c6a646877736d566e354641684b6f434b6a34372f5934624f4c716739582b517a74586e324c4e78787263426978744d694a63523472503543623365653132425042767065414d6767574e494c592b65576753386d79544978764c466e363069675357517541355354367647617a66697331586a354e426c4f544373726977724775705451743833535259797265524e6f50514a6d305169497952715a6132796d2f3572652f353450745730686d2f726976755259457638766658776b6c434f5130684457684a6563773171326f426c4459753073556f774b6c4933345a6c6b376c377251524f6c6f417638384d734c556562646541523170342b397878656a4c78592b5a332f4f49346b6e7a4d764d674b366942756742446d4277676a5a5964726379466254504d7761747131693453727036573665354b6348757357696838387331534e5a337a3466316d6c476243385332493541362b627a77736266653774577a2b73756c4836737a3852394958657030766d754974486d754d636c7150584c483074797949642b336632634e684344304172666f754c6a582f394768584c69313172684a4a54556f5169756a38774446386c6169472f6a524949545564716d6a794c726c48345963394a516e773767767654563679626f377849345a7749712f7374622f547431575137714169626b4a4c364e666f77485755794f366e4772346d53464d7645453350586c6d71627a3270655654527542326f6f456c6b5467343371796a3345715752656d57394a3835686f7274547a75336e572b5357412f36533035475369796252596676753238727a61565779636835575572512b3331785375676b424d4d6a67582b6e354a637250762f3630426c32692f56687a6c464b502b56667465353932414549336933466c31374e6c3174727a394c4169722b793178327167382b714d6e6e6246584e2f61386c31696f7155785a2f56726a332b6162536333326b54765954506e794b424a5a47674d3371705a70422b35305976347134724479694b3269463061444f56622b7546664c6a317a6e39643346586563704146695a4f6c58453537524e5361773664554a4b6f41482f395772343179534e3747716f3350566a36535835417a424f6e4755584b71576d62755963546344592b705050734539496a34304c5a796c41326f58584d2f5630435a306641462f70796c35784d426257374365346c74317a7564493575354f31486d4c5372517756663674523258352f6b735563334777636b67584545576a6366377649374d5934645639574271566a38377a762b316e646e386348436a2f4275527a6e65526b68642b63446d526f4a35636176425574344b726a316b6a4b757a434e58583445724476476f5a71757451436e7052695a68594264366266396a636979734f4c6a6b554e617a7a387850306a4b746b6e37434a496a5679586257583633367061364e4f6c626f4e4d2b2b52774e6b513849572b334b56754c535659654f7138323875643266776a78394a577039746235625a44544159664f595163302b3048642f375a4f41494a6a434e41624645627a496e3732704272323768577a2b657132744c3934353356662b7a7363597435526e557869753432796d796479724c756d2b773266595968464836437559666b3433767938684f2f784f6b416c5856724b59472f4642306a7467472f6637354c47464551597455776a47443578325731726b424d724e704e6d71724733414d484e6b4838316f715a7073592b5856346e67597141697638794877654f57446c4f726f566379335751307a4a6e4e752b6f535a6c482b66716264734f67514130664c61786c66564c3776574970355950575a7a4764643162324c6f467842506f4b50506d4e474d654f7650456f3363677179335666613231414c78562b2b39785a316f3245585074394b5a34706e7455614a4d67736839734d507668446771746a6d36715955774743686c736839676b476a4a31344d2b4c4f794e354446683845617a325a3044674677482b2f46724b674d665a5762707545665036746c4e694164547a6133356b506d646c496b494537472b3933696f6b70456a6772417237556c3758635a50376835646b712b4751517773642f58516e7a5a633132327448794d634e4356593761735878534d4b6639384a565238626444356436726450394154757953756d37616b64756242505a446f4e3230596e4847347171734a76434c6c555836705632387a79624d366d4a566d3534556c483736334848346a647a354b4f4b31594f48483072394f587448353539665839656b4d4b50304531764a2f62395656317132664a656f596f4f7a7a337964576a6245354b502f566656423139376b4467787354374578774d643944416f38356b6555457436364a554a702b555a497272345067377849344e5149712f737459555a524b4c446b636b2f5a4a715861346a4e6b633379684a47346650615a457843672f425a3658717055722f3861327049397163514b76346d356c7150554f4357776e67525467784957662f4a71642b72567368426f6768582f7568305478716f4a344c567633574a2f376c535435702f6254656658705271742b57792f76636a32716c6e38334859786f4443456f2f78533768556d6569363750796f37446a39302b62726642762b504f33776a4d4b517a5965317873784c39706755304647494e4b486c6c545a4932373145676d63426745562f2b4e6552797a4f484866696d316e384a4f73522f335758456732726962496467545a496d6771586451475a766c59356175624468574178776e4b6b5347444a424641477356625834766468395972696c3134733578543251374864524f6c7655325469686f4b69504562496e55392f575054787778387259395955537a6d626d4670776b326b4c45654b6a6638644f5553394b5030556c533841754a3943305577637063354a3668365a364d565a382f506a375974526f6e304b4a6853747a78656a535a384876593843336b63304970787a45486967534f487343593134435a7739704a67426b4e3842766e36504b566c44344b513544786f696854444d7a44587452335a4a79737854595965426b52754a555a56582b617978504a5238316134512f72794b4270524d6774715575664453557a33337038397a582b416c34766b6258474b342b64586161735831677055597052616a4d5731656558645547365955354157367276684f4c784f6e6a6b42424c7343363350776f2b6867783839524543654446417457364d2f50366b62764e526c4836436d372b3575342f54426c4966663345316d4c37632f4c545474396d686273514e756a6745596854342f3863497077763354504c625979373247676d63497745562f2b4e6339522f75506954744d533170313537576251674954464b3249304132694e5a4b6a2b587347395930683357765647432b57526349764e30497645734378305767745654376252686548393456624977513373646673735653386d352f635657706c6b7274623133547a695537617a632b384b333075665855313178386f4f7075665132755048786a434d41744d7552546a344b4e7862386f3879576a54376d505447656353685270712f4253314f76586b764175626f574e56456d7773413474526a4371397a356852524b47645733347577544f696f4176392b4e6162724c3159415769554651524c4454346e33506357616559504b36524c3263304a63393050574a53785747395769555569434859446148732f42682f307556516361546e544b42314f646b6c682f797063385134384750644a4645343737726c68444875594233486f733670493072776b4641556a424d5a6c4f636877593246464a7439676e764d7130654d3834496b742b2b756532716e66506535392f414f7863715045595167576a59412b4f59585178564a4573704a4e646c38435068395674562f7a584445734e377245716f626b38363670464865706733766b63445a456c44785034366c3538584a523641394c68374b656e41636f3137654b48434e616a2f5559314c6e59626e436f6f5a776e4e376e66725538476f355941753868304a36416b63392f565a72484f6268526541716664767a44555a5a2f6369423135534848786a736168523335675a36306c4750377271336a424f5a5371585a4963443138394a71474364676c5071425078727a6675413966656c784c45616f33592b686f6c58352b5979794d6957395432596973696d736f4f6761427537783766334173704f6f364e6857342b7a417552514953324a474169762b4f414865384864394a696a34524c466f4c5369596647414b6c394f4866455849534c4759454c74617035476a316f7056435039524c6d324e376a4a2f7337694f3242516c4d5236425761492f744e41756a434a6c7a366f7777685579746642366146686d3843437046734859584a586d62666b7646576f4a665036326e61427074346d36446e2f736d387336756c6b6935687951464a4374594a2b336d6776646b4f643273372b556b6d697138424f6869365839685a336c76382f4a7a44384730394d317078706773516e552f72506344756a6f413638627537784b517749594556507733424c624879362f51485a57326676776f2b37784956666a3342377650497357782b4c2b7336614a562b6a396934434f39763548616b67536d4a2f41725866416b4267637378304d463636596547652f496c367a706441726c7638373874577473543131786e54535531414b7042535046387870337a3232346a303378664c636b39366b36594950565a6e63715035657834335a4b50526d453035653231674f75507351736c4a6f6f71385a506b44517845332f63785478734d3166766b5941454e6943673472384272443165536959595069433138414844643745456a2b367875374e744370634672464f3159455844796a596d3752352b74396675627634344336536437584e3079684e4851537470446f7650396a484d46356353596d382b74426f4d4268465354544a6d7171385775555558334c6e76636250784a33734f66752f7636414a36736462764971394a51694647306748666f7a452b4d4b6568624452737a7359474558396a45677142725a4d374e78563965533969785238533874385457457a324d317974324a69784357437a4d6c596532786d324c4459356c706a5853574450424654383977783054584d453733353546777856583472316842656f736a3843462b724a6b6f47502f393148646b476150514b746b585566784a464e65706b456a6f3441525a6f6f316f514d46556d616574416f725857313254366a43417235643359444f3853476857386a5369716243675233475037334c764b517a6b326f64705570376656567953322f39565866485272485744656b37306c7972366f52436d79745334465a584a52574d5941626566765a564a52306f332b6235426c4a376d523138313065482b2b567748344971506a76682b4f36567642544a66315a4c52797234393966386a6976612b4e596632647578335a4b51657136316c574b41446f433663594947374748647865574576526a37764d614353794e414a5a734c4e76496879563579347754774b4b4e4a626c3266367a64537571683153347a2f50732b763258776f4c49722b66495233675674484e616d6d4d674339765475706c6f355832586c35325159537a385a6663594937592f4a64313976396d6a3361703137305a672b617557666a44312f6c59542f533641765163766c503972435a5178666664795a6a7530624d57617558694f426b7953777a35666c53514c6163564a4467576c39685578323747725332356b5867563663586e443058677378437552574a75764648494c53304a5a687831336e3130634f68767a5270524979482f7579415268357535644a5944454553505034796d363075507655626a5654542b4a487569726c7056385552594a702b66632b4b59476d356264396e736f523446774b543545785a3950673148613874654548792f66314f39616b7a437956663974375072324c7479433161702f674b6f504c554247552f6874314c6b6c44613866336e6c536b39576b4b6d3576574b4c567537636c79526f7855636464686671546f7243763034737055546d54577465667645704441684152552f4138446d327739742b314a3933594b667679344b7a31694a4c61706e792b7156684a5956737456753649305934614d48332f4a6434323741623679796e494a6b4e5745414d704c4a65453551504150763748784775396d55566672485a734235684250513347424b57336a5a34346976306f59372b395746324341324565644532716d31506e79392f454f4b796b774753376a4a42683279414a65306f545757595261446c52594c686d472b4f31745354363843375264785978695932773669724335494b68325779477a447965697852324b6a51436247574b6a2b4c2b4b424352776841543238564937776d6e4e4f6953734867527431616b6a4f5172466a785a722b4a4c6c636c305a3972475777536c504e6a36344a3076505a5a4d515444644763484d6f315a41356d6b5a68564a5a48414639734d704e77796c4e386a4e745a76436f4a7a2f4935533175306977444e4d5148762b325447476a7775435570386b6248766a4e5a396b6a7a2f645a476f62636235676d6244675348687a647330564e32444730314a71636e634d437777357a3468686565666471634e6e4471307776754a6b776773394d517746626c79547958792b6c35636c376748502f3469753579513041376a3435316235485a4a2f6a4c4a4d33666b356530536b4d434243616a3437786377566e36552f6f2b746d7357486e34445355306a5079556351793149724650766870562b585879656e394757537647362f6941632f6d47334f3631567036647047734d43526c67346845493178397857766d574171647245424152517a2f496a78393261397235486b596a333359346e6b503554464975662b37734e4e37346b646a4a643147566f32514c2b585331732f2f553253484e53314278674d567672696f72664e344d6737587976476d78674e6876726a65534e564a687362584b726754463737566e4244784d57516f4638324d4d5158744649436d4f744641516b41414341415355524256457671316649374376637167784a4b2f35386c77613272434273754e722b62434838766e445430755237746d754a306b3346347251516b73434f42632f2f3437596a762f392f4f522b49756a59387150724f3478537739654c644d7376584235642f353450417834364e62572b333437556c644f7235394d523571353071646c617a2b6e61715478586435586639314f6b4d4348646b45544733355844644766332b50346b496442667954735872696c6f574c51312b7538442f71584133656d75545a335a6f5355466b71592f4d33695976434f5576396a474f6f6d474b4458764e754d38534d7a547466326d67334462696462424e41796b6b486d2f775334457a37784139672f6439562b43626372327545736455627a39493251622f46757338636546356249526a3333703262304f3955502f4b4f343130334a4d794a47495836644776643655446246753944696b6e697739384b376b6937706a66646c6248335330414347784a5138643851574d2f6c764d79785068576c676b76497876424c4f31716764682f5a2f6c726f793070454e67634331546a6c6147587363663275492b516a317561644a6e662f41377673466e7955316757593853457441636f6f6b635879762b76597648383341726a7163464b474d6a5247554b79772b4f4b7a585074363833645a4b796454464877614d393435727945346b3678695261622b447142493873347373736e704850657755586c2b45724a3349577861366c50575464672b7555746a5775376865666e4654526f59754a6241322b4a6d6943572f50696d6c6343415a786b68355757516f75302f4a75464f66536e4950477a6353476251785466567758747855504e346b65772f74664574584e626d64346939334e513157396230486844596841516b63677344554c2f78447a4748754e696c686a6f576f43465a776a6c3950536537667052347463384964706e783036336e69546f45316459714d50766a674534683336576f4175483451734667796371787a4e364a714a4f346843433469625461675531724459352f4c2b336562534a544373634970464970626e3138784a3144342b3963625577494f627a693238524f2b446b5835593772356b567564344e6f7068417732424b5757647765624d4e34746e4e42734975316d446d73345676464e70533751783730584e4275535464737231364f517636696235377561416c63556153537a546d327761414f56537a745555635a4e422b45456d64504a496d7763534645384a4a7747314f34395a4362432b6a39476868522b376931784347506138526f4a534f414943616a346237386f354a78754d78644d5a656e656674546233556c3851702b69583766475278782f316c5756483766722f623376516d6c7058524e495a5866724c715664666365514e5a484e79543237437a2b7138785866312f68735a7879425333627044446b684779747654494b4c515a394c52476d446a54687546765570334b6e2b6259376c567136624b366958354161316372354c305330435659766244496b544f4433594e49744d37595944473554787674504c54666c7950566e50635050454b6c392f592f74637a4e6949636a7263436e4647722b332b6b666671335a6f4c654e396865652b544e6c3642642f667252307945557a593251333343427532754939727745676c49344d674a7150687676304334757454487936656f5748784364795339797432695a4b6b67337a3148326f6357557156534a36414978396d6b71654f557051303848764c6c726774304d63632f502f536762662f64424c434534744c414b51732b3932506c385a3362443072654f766d6162674e61357a6a66317639375856394c2f4a305574535559464a366b4f6a3230334c367a7070642b58704b45324a787470485537334f6139533542715852514c64372f5033575977335430383137684f4558644174687955386c6f6f6973626d35416e4e763764632b506d3531536b6b2f3776764e4742566a41707257366368356b53556239557134665230565370553573654757354741424536416749722f646f745942386168684a4b5259523835704c63627a5735336f537754344d5952376857534542434a59484569414a4c2f32796438764c47614d2b387843746c756f337a5033626a6a34475a5543366e79384632746e3256533475486e6a384c5943682f6c7033542f5741665737574e3874764865424c43366f2f5367504844364d6c61776d6e375868746d563649634e4b473544434d2f6f4862594d2b6877377a71566456372b3774736e7573756c38735572586d577a492f6f58536a2b764b4e764b384a4666706273514e426e655954595363383355367a57315375354c4f6d464d6c6a415a5558372f6f69754a6e78417a786653416254793166314f4f6939736a75704b42635233417543516461475171457271734463382b716c4a32383837383279574e5877434d313956414273553259653630454a484245424654384e31384d6a6b783530534f34747a79344b306d2b6555763776515033462f366a6f694966477a354f57443278474747746f6135416e582b666a3959724f697635554c377a7668486978342f43503357326f6a6f6664686c58367a2f4c767a4d6e6a746e3755754c5642627175613837702f543641565773386431672f55555436306d763264597a724471344542473175492f557054726c2f6c317a6c32347a68324f397061313063387631662f3630564c6854633473526e5732487a79416b6a377a696b7a397250706f4a3345346f3573514d6f723856502f695939676275624d4342474161556267386a595453776e4165563755655a4e746479326b6a684b654f7679787438432f64557946454e575a79666a2b69465849446246764c384a6e422b534830707952374f6262667559657038456a7076414a692b393435374a345565485777397031386f4c66386f71764152706352534e627a4e572b5345722f4f45707643654e59696e56506b562f394946566a3753683677512f564e616f4c36316648526441304f637153396536667679396e7744504b456f4669733036515946447766694a4a4c684337434a59564e7363376969596457585858646f2f6c58733544574744684242457a576e49496151764c53577850327a6564366d5030516231397031594546526231784d7067622b6b5847357a31324e5278336977547644444a3246416e596d6f767166313561392f77343247676f42464d4c4c5531584a4a5145433857467577717a325a3448365344777874704f7554484f49584874424d4373505074796642765768496944586762366e766c47456449332b5867415157516b4446663978436b54386333387569394b393641593972386232762b73416b424474696963667969585670573848466751384b487a5a796e5a65734762526469686b52734974316e445278394933795253614934696142315a614e5276326834496a38363759643142623345585437343032367661466d566f324e4f664852783938567852524c6f62492f416a77725a426a4237574756384679696f4f507276476b426f61463255663761474132736d666664332f524f707156614f635243584e7a36396a56427669656b7771513457433337794854474d34597957774b322b367a392f4935375743305036397856634245717772754f5374337268424f4747796368653953516f4d527a736c514c36546f4a7873586c73445a43594c43706a53616378484961304e594e4954745a5836704d5842647632544d512f766249466f53307155317873786f4b4169354e6b614f6655784b2b47596f454a48446942465438317939776132586942557536733130464b772b56496a6e694c616e31646d6b54483149794d7279304b7a354452644e64356265626f2f6d2b342b68642b2b69374830736b31754378516c41785159704466735050536e4b744a464f6c4c7554445451477a65764f47417343616f7853636b75445377776c4b485578627a342f4e4a496f2b436a2f50356a36466a5745626445674635303163312f59356e6d4e7536774d61612f752b332f3059447a4179744e4a6e6664364745382f596f367362363278643761616758496162445570746d7a55486c3864314d51593831796a5a6e425330776a50482b2f6236505734386e4469774b53484e624333744270584e4277616c506d576275436c4f436d7268373465614a473038565a766676367772764e6941386337705732744f4f6a4469744148483236794e3930684141677369734f2b582f344b6d506d716f376374366d2b4e78506b705965586752383649746757476a426a42774563724e44335a2b374f732b594c76305536664e6f353144505339593169686d733835693344635855743742654b6a614c6a45596644444a56593769663068422b614659564a7372766c5359506158734d70776559576e456661425073504b69564842536467687030314b57506c436d707367756459673548624c4e4f68764f4e706c77566f32744c373075312b50797854746b48344c6256716b4f586864684938735847587236546b673541576a664b574f4b39474846373973386b6a4b5544446b6f2f5056476c2b634e425a362f68543546756b314b7750766751674e5171494a4f665a4a572b5066576e59317236504f68336358454e574468583357695364705355717575792f537a6a7a577a44516c493441674a484571524f384b70626a556b73697930316e3363466571716c3358444b50676333664c52344750492f7836796849345a45426c725546774a664e79484258394d6e2f55314b4e563175723939756c43514d3572383143564478365a6a343372345970456255766f4a386955377a4c344b3877794e4556636946483763416d703565684c576b4530617a777978436e4f73347a5a73563933546c7a4b54363147594f48584362783833736b4d4b6d5a6e6174496e387a6548437072773367667230446d57357a5936314c624f2b7174363068534a4f50766c395348767169695737574e5378674f4d693251714b4d706d39366e676f33487457785a4e67484d4274703951494b47317957736647677772663957393843334144576e55367966757a354f4f6e7662636e5966505246317541415952544d536f72313849336f4b3779572f39576a444e77364b74652f492f64324446616b665a596b5941457a70794169762f714234444d4c38383430444f437977642b6e507866464361735675536b6e7a70776474583073447a784161304636784c483259797a4c346832714432733458773471647a5a5a726e59426a48754f36544547314c365338416e796763663636487274756d377675634754616f2b2b734e46674c534675507a777633464c49474d48726c4c484c6968734b4f346f5354795870516f793432617a6867555674796e53416462433567596c5a4a4e6e596c735766582b587250635474323377444f36726e2f39397666645a38337630734e763342717932396e4e36787a75497a51414b64312b7761776b77783555534953614c2b4236796e66554a626a333862584c43573454334d676b413374775a6238674f56415246487064506c4f6c564a36346f2b502f61644568476f69463376314c3471373646774f44573761662b6e624558437a396a526445766d64643454352b436f65454d2f6a79646f67536d49374376443842304935362b702f7246756f2f6553533248596f5753516d6154597865434d50763858426b33483254535a714c492f32546e786f52466a57446945734f41653950596c49356a5761777159454d624a563065466a6e634133624e47744d334c6c79324f506f764c6759633957505278366539434e62415567333047502f5736734a445650664558356a7178305849774949566b6573496d73526472525555494e774870716a59544e393979685442766267654b6630456176392b54672f5a754f307172446e2b38363351317a367a777254765877712f5952482f6e6f454a34482b50736c314f4b6e6e666f6a6a3375582b7865536a2f31633039716a757834675367466c78746346306245332f454b5344572f5672342b32722f7266794f63594b357471634e4e32754b6a66564e7539797a366a5236312f583266676c4934455149484b4d796371786f556644776e38536169794c554a3168614f454c48756c52636656434d555170526a48422f6d4d4969756b2b472b3937343949324e49326973566d4e79754850453332594e7164736b377a58747742714c4839612b665574642f5a53327359533365626c72393452392b3152764d782b5541367a3475444c67666b624b517a5a302b4d515043516f555934647079615a53727557306173696e655a76786a62326e372b51474a63754d4a4d4d4536377a785a4f72434c573162345a76424f34316e76686173793767343776746b6a5650516b6b324e444639733549647936424d55572b494147427562384d7633544a53544f4b7a7274525766793744777336474856366b436a67734e37787a6555632f634146724c59643233746f326e6f7174564c6a3462444d564c4a5341424366776e6758557649316b4e453267744d3179354e4b562b3750716939484773506254684764744f7559365566316a44555a623551504b782f6163526a634358414e6b686f63594137614c673369344a5164443746494a3234564238616245364571665153683177687858754f6850376e724e4f504a38452b334879676e4b2f4c6e4d5534325244514a72484969687a4b466d6b686132467a5144427531503730354f54765330385249324c4a5a796337664d35334c5374372b30434f726e764969502f31767236364c4e696378315a75506937324c653038514f6b4836554957537534322b4261313234492b6d714f554163464a62364e76634a31734e374d73496c4130642f6d4f616439416f434c34426133616b50554679664236514a756463666b2b726e763962553943556867426749712f6a4e4158326958574d437736484879675a2f746d447a592b4d4c79556162734f2f2b39706d66755138704565796e483732535247524b432b5068346335784f304f632b58553951664c45474667736847785a6359757167765449754e6b6d346370557179666841332b7641613134325a4367514b4741775857584a5a7a6934592b45546a4555567477694344567572666a76737164313636763762484f6a3878765041633647734a73437a79306b6c6565552f5a557459754e66304a54584162516a336f554d495261646576715a68306d7269336c4f376f6145306f37447a50696a7936563042727475736159396b44726733626d4c64723575736c58683445586738354e3554376d75742f577a4376367878477a7745583975556741544f6b49434b2f786b752b70366d7a4165754b4a776f6a415373595558446e35374d464367447541537345314c6245666937536769777269316f3762556f75566a6f5542527753796b467939623176653533724e2b3446564563444d4674694c6d75796c5a43567074694f63516435704e575a494661312f2b7133396d4134645a51366b41514e49682f395a4277576f496967764a65346a43346c6d716b724f557170522f464261737579745163777671325159706b71536e426d334f4d61556c397368466c6f34707665716e6375386e343266537a5357794644574f625633365464736463533041756d2f6c574d434b67784e3871435656756936413038367a577036393178654b6850746c41384c6445634f7975776961456a5370427875754564326a746a766a364a4639397771664836336a347577516b634741434b763448426d7a7a4b776c51555a4b3469565643554f6d44566c7a41706f48734d796a59704a6b6b6139412b6848475261516e6c487757656173706b356d6d7a644a532b2b6f4c7a686e4a76377a492b584864774a566f566f496c3741454749784470517352514c5a6c766f696a4555532f437138644157376a57776d4576363343545775552f4d4e645a6a374c6677327962573542704a6671746e55757453592b374b595656733066303669337762444d757a796b61674b5031597a647443577532343242547866423869466d674d67316278503851375938773476455943456a67544169722b5a374c51527a684e4c506a72386b71544e595150387971686d696357746a485a4c395a68774b57487243486b2f793643745239723661726755666f6e7a336274593877482f4666336c464749594552714572415a77572b2f547a6a7849494d4a6973383634615141463654727262757736784e72364678797a78374c3652535735726e6d752b392b363471336d7861514936616c72586f37464443377a33485832624471646e4868755839336f74646567365566437a7658385065434f39367143733755676542763552694b76624878774932517a47674539436f536b494145446b5a417866396761473134425945726a2f44425a324f77716f5943626931734841696378664a485476466435484f536f417955346a6d34785844307638356469583735634e6342734f515a4a7735695679464f344335645374436877476f554866346273386b677348456f4c576337566a59537a47764f674858636a31434763474d717776707741714f4d4934417246334566574d4d4a6468307262485a4a32566e58612b425a704272756f59526e6e4f6359662f78615367724e55726d325051326f6e315863666f7072587438344b616a484f2b4f5146633850786364324a5341424365784d514d562f5a345132734347424d526c3853446d354b6a67586e3138736a31682b5355314969734a74685462492f6c4d487732494a704662414f756c7a52396a476e614c746830305063514b63477652564a655636546b495949373775597753463532706a4c7579737169686763797639645935313468492b747973654e33496158746164586e456168644c6346754d62416c5366457052725347464c4f73314479653037332f7a363149783341456f38472b6b6962525666356b5746624461306247794842466331356b5674436b55434570444132524a5138542f62705a396c346c6a46317858335756585a7367796162423334754b5038553768706d397a68354c4448393732574a79524241526d7172466c6669394b504b314a4a37596b72454b6b396355335a566c427143466973712b585762574731705a7279706b6f354c6b4c4555367754676e665a544742426e56506767485735646d765339336d3746666d64726d49326130754b3233564377546e6361496f51584d74472b464178486e32784d5a79326b63715345376a614659644e775239325264775948332b6e6466725a76726d5272352f544e2b73387246743566356541424d36436749722f57537a7a305579533148786b33756b546c50444c6a526a7049354f5163672f6c414b562f557839644649572b593336713775496a50305a61717950336a44306c614e764876514633444e6f634b6b7a4550566a664b51354878672f532f7846777966566b5736454751736c6c336e64536761556669332b66344535464a7045787351466a324f78364459726e335a6f38362f754d6c396831664575377632794b313157375a6c35397a2f552b547244366d50464d636c4a4854453074712f716a556e68646f4776565776434f494e557261576f564355684141684c6f434b6a342b79684d526144506661443050645a33754b516c354c35747172583270665844716e6a6a44534267366164775635303663394f675362706a303049574968536466516d626f4b48382f52524e773772365564306d676f304551627637536e323636787849425973766570753264563352746c333750655837595670715a367a626d484b3677716c5158656a7455456f2f66304e6b7069725663566b4458485a7736526b3662656f4c4e4f35624f317a2f2b4c745334542f6c4a3975355355414357784e51386438616e54647551494473477154636249564b6e41515234723679546d716c487974355833724b6f545a51374a2f5538794d2b7753396231334831653132526c33386d794a54633647507964646664594f5638794162396a723255457858536d693552536e616d6575776f673951646d44505759496b7379356a484b76346f796d786d36784f6e51323234326c4d2f54746c77492b723747794c5768784d75334e2f59734b3654693832596c6e50643250786441684b51774645515550452f696d55343655466349736c6639387751685273586c7a464b48522f2b456c69495258757365772f57532b49414c74543054795965724a6d62784162674f6f43625552474b577430787951556272683742756e2b565a4e4f2f50616f45583754796232363778564a4b6a762b32304e57477735763863736238314a356544365634546a37426d5473737a7a69467539696b74744c3639505037495449366364704550593436626750336f7a73317a79785a6d3744756b35324865385949626d2f585455497851455543457043414246595132465435454b59454e694577464d784c6742355a4f4d596f2f615355764866584b645a734c495a6a424e6347556b485738706564347246704f6b6863554e696b314c4b744777545a65716a755777765654396d675549774d4e70784f63427078366337316f71354d327334645679564f45416a38585a7267557737624e6c5570566e35714b53787854736532426b4d2b2f6a7866504d4e7478655a5872496a4432575a754b4f386f3872557650374571784847556542524f4a6a69564b396d734e756d486b34436c625859336d5a2f58536b4143457467724152582f76654b307359594157572f4942643471333767576a4d6b6351346164596c456e384c664e77744d486e424d476c4a6336397a765859524638356859726845744f473441344e6b504b55486345314449652f4f75786471506f636f4b417a2f505872536e555256417a78597134683871385331574f43564475732b687545792b7878624b657a5331734a4c4779767a724a7831657a7868652b56666f3353666b354269414b50586e2f36333759624641782b715a645a69372b4474594a3778445369526268784f7847412b36443639727964776c4951414a6e545544462f3679582f2b435466337858424b7675614b7869392b564a6e746a64654a584f45723571774a77756b4c71767463796a65424138764b3355376b425935682b32702b71615746787833666e385467456d324c634f72477a4869314a474d54464f427361636c47773733305066783771534c724955536976396c554a6b70556a546f6364784c753358477979713370494b746e57356763556830715879724e616244646157303532784c6a7a74715272334c666e5a5035646e7a6e6c4b51414a48544544462f3467583577534752756f39717576696c34393744634633592f4a70313337485650487343777975385643524649746d4b2b54714c316c4e747346353953546b51532f4353514b576554494b6657536e784742785279456e6a71456f37696a784243787950554c674a4558485874786c382b482b4d636f50726842595a6c2f5170647a635a67374863672f766d752f747162414d4e2b62596e716f6379376958506f35537558646f48696a6a397a704159625176532f4c6b4c6542526f345073516e337843467330357930536b4941454a464154555048336554673041667a574b584931786b3248735679767138544a2f332b7a4559574443466f6b545763744e786e493472507058416e455a654e51702b37637449314e7273654e353265544d4b64546b723441586b35534f42484374556b354c41453233577855612b4830696f4235334e622b3567446459396c2f3363692f48546134424f68694a4f4276514a47414243516767514d525550452f4546696233596f416676782f3374324a587a4335353463456633695568552b704c6e686856365830583766717666386d33464934675469555948484666654870432f625848324c7a76743170785a56374c746a583575785136334a4b37584b36684d7450455a36357833563148413435542f36474f553367424b77497a336f3544534f5635794532485965636b32314c5141495357445142466639464c39394a4462374f39552f774c35627650754545345136646e334c394f3556416e336367496c2f6235524b6e6556783779435479575a57727a305547436d6578456547306745304b6451656f41344267446355313656694b5a783043472f6e58556642617766724d426d397353745a446a4f3163327954496434356e6a694466705161686e2b757a3472776c494945544a6144696636494c7537427058536f4a656571525656563872356e6b666b6b49396932434c2f4463726a456f3872676f6f466968324b4c6f764b567a627a6f33426665446b67796475424472634a2b46505a734f567749536b4941454a4841794246543854325970467a73526649394c586e304b613331667a3077496d73566675425579346a786e73544d2f765948666f79643474387953514f6b784b5678506a346f7a6b6f41454a4341424352774a4152582f49316d494d783447376a6c59384d6e73386c553948423561756369556e796e2b5265724f667a746a627363303961464362597a7854354c6778715649514149536b4941454a44417a4152582f6d5266677a4c73766762506b46373972772b4b4b5866724c4674475854684355654f624c737448306830356a614551722f30596f765667434570434142435277574149712f6f666c612b7644424f36633549464a71474a372f655979696d356831572b465741445345437248516543376b374270612b576e6567713348636549485955454a43414243556a676a416d6f2b4a2f783473383464624c322f4853533579636847302b706a73767a2b4b364263564541692b4a5a796e4551494c62693833714751736165735455626a6d4d6d6a6b494345704341424352774a6752552f4d396b6f59396f6d74644b516e564f307675524265644e336467754d35447944783978596742552b6f396e45646d6374652b4f76704f6234786d78493547414243516741516c49344c302b33694b527743454a554e4348494e36584a726c6c6c565038646b6b6531645078375a4d51423641634234453641314d396f68736d65657078444e46525345414345704341424351775245434c76382f475641532b4a4d6e336431553862354f453671455576714c4946586e7657794533506b57766c4f4d6838506f6b4632324751334779667a796549546f534355684141684b51674152552f483047356952776963366e2f7775533343724a5935494d5759385a4a395675337a486e674f333776784334587264756e4e67556f52415842626b554355684141684b5167415157516b434c2f30495761734844524f6c4853667a3653756e2f7969512f317a4f6e6f567a2b4335372b346f644f5154564f616f7067335763746e374c346d546b424355684141684b51774a6b525550452f7377576659627150376852466c4d65665366496453664464622b55365864447644454f30797745436e4e44385276586253354a6353566f536b4941454a434142435379546749722f4d7464744b614f2b56354a76532f4c454a482b5435473439412f2b394a4e644d3872616c544f714d78766e694a4252534b2b4c37346f775733366c4b514149536b4d447045664244666e70726569777a776b586b746b6e65334158773372775a3246755366464d53696a3070783063416478354f6178434b7074306979573865337a41646b51516b4941454a5345414359776d6f2b49386c3558576245734261664f47754f4665627465646853533549386f704e472f58367951695162656e4b58572b34616633415a4433626b51516b4941454a5345414342794767346e3851724766644b48376842504e2b64704a334a6e6d2f697361666458372b4b7048482f596951537658563363614e6b56342f4351573646416c49514149536b494145466b784178582f426933656b5179637a5435333273517a7a77556e75664b526a646c6a2f6c55416231507378585979476e43516741516c495141495357444142466638464c3934524472314e2f6367512f79484a74795a357768474f31794831457941326f31524d4e704f5054346b454a43414243556a675241696f2b4a2f49516837424e466f724d5550434e2f79486b727a70434d626e454d5954514f6c482b55636f746b62524e555543457043414243516767595554555046662b4149653066444a2f484c4a616a7755366671464978716651786c50344e2b725377337348632f4e4b79556741516c4951414a48545544462f3669585a7a4744612f4f396b2f76397059735a76514e744364534b2f78656178744d48524149536b4941454a4841614246543854324d64353537463037724d4c2f6a7a6b2b2f39562b59656b503376524f414e53636a73553875726b6a7938327752516a4f324e472f52773053515854334c314a4a2b63354b336463334b787067314f6a54346e79577333614e744c4a5341424355684141684959535544466679516f4c31744a3448704a7270726b73556e2b516c614c4a39415870443356704b3656354e6c546457592f457043414243516767584d696f4f4a2f54717674584355776a73446c6b6a777a7957584858623658717967593973676b6a3076793972323061434d536b4941454a434142436677584169722b506841536b454166415171776b643048313578447944325450446b4a3853474b4243516741516c4951414954454644786e77437958556867345153756c75526d53573655354449627a75583153523661354e464a694231514a4341424355684141684b596959434b2f307a6737565943457043414243516741516c4951414a54456c44786e354b3266556c4141684b516741516b4941454a5347416d4169722b4d34473357776c49514149536b4941454a4341424355784a514d562f537472324a51454a534541434570434142435167675a6b4971506a50424e35754a5341424355684141684b516741516b4d435542466638707642326d4e6741414265524a52454655616475584243516741516c49514149536b4941455a694b67346a3854654c75566741516b4941454a53454143457044416c4152552f4b656b625638536b4941454a4341424355684141684b596959434b2f307a6737565943457043414243516741516c4951414a54456c44786e354b3266556c4141684b516741516b4941454a5347416d4169722b4d34473357776c49514149536b4941454a4341424355784a514d562f537472324a51454a534541434570434142435167675a6b4971506a50424e35754a5341424355684141684b516741516b4d43554246663870616475584243516741516c49514149536b4941455a694b67346a3854654c75566741516b4941454a53454143457044416c4152552f4b656b625638536b4941454a4341424355684141684b596959434b2f307a6737565943457043414243516741516c4951414a54456c44786e354b3266556c4141684b516741516b4941454a5347416d4169722b4d34473357776c49514149536b4941454a4341424355784a514d562f537472324a51454a534541434570434142435167675a6b4971506a50424e35754a5341424355684141684b516741516b4d43554246663870616475584243516741516c49514149536b4941455a694b67346a3854654c75566741516b4941454a53454143457044416c4152552f4b656b625638536b4941454a4341424355684141684b596959434b2f307a6737565943457043414243516741516c4951414a54456c44786e354b3266556c4141684b516741516b4941454a5347416d4169722b4d34473357776c49514149536b4941454a4341424355784a514d562f537472324a51454a534541434570434142435167675a6b4971506a50424e35754a5341424355684141684b516741516b4d43554246663870616475584243516741516c49514149536b4941455a694b67346a3854654c75566741516b4941454a53454143457044416c4152552f4b656b625638536b4941454a4341424355684141684b596959434b2f307a6737565943457043414243516741516c4951414a54456c44786e354b3266556c4141684b516741516b4941454a5347416d4169722b4d34473357776c49514149536b4941454a4341424355784a514d562f537472324a51454a534541434570434142435167675a6b4971506a50424e35754a5341424355684141684b516741516b4d43554246663870616475584243516741516c49514149536b4941455a694b67346a3854654c75566741516b4941454a53454143457044416c4152552f4b656b625638536b4941454a4341424355684141684b596959434b2f307a6737565943457043414243516741516c4951414a54456c44786e354b3266556c4141684b516741516b4941454a5347416d4169722b4d34473357776c49514149536b4941454a4341424355784a514d562f537472324a51454a534541434570434142435167675a6b4971506a50424e35754a5341424355684141684b516741516b4d43554246663870616475584243516741516c49514149536b4941455a694b67346a3854654c75566741516b4941454a53454143457044416c4152552f4b656b625638536b4941454a4341424355684141684b596959434b2f307a6737565943457043414243516741516c4951414a54456c44786e354b3266556c4141684b516741516b4941454a5347416d4169722b4d34473357776c49514149536b4941454a4341424355784a514d562f537472324a51454a534541434570434142435167675a6b4971506a50424e35754a5341424355684141684b516741516b4d43554246663870616475584243516741516c49514149536b4941455a694b67346a3854654c75566741516b4941454a53454143457044416c4152552f4b656b625638536b4941454a4341424355684141684b596959434b2f307a6737565943457043414243516741516c4951414a54456c44786e354b3266556c4141684b516741516b4941454a5347416d4169722b4d34473357776c49514149536b4941454a4341424355784a514d562f537472324a51454a534541434570434142435167675a6b4971506a50424e35754a5341424355684141684b516741516b4d43554246663870616475584243516741516c49514149536b4941455a694b67346a3854654c75566741516b4941454a53454143457044416c4152552f4b656b625638536b4941454a4341424355684141684b596959434b2f307a6737565943457043414243516741516c4951414a54456c44786e354b3266556c4141684b516741516b4941454a5347416d4169722b4d34473357776c49514149536b4941454a4341424355784a514d562f537472324a51454a534541434570434142435167675a6b4971506a50424e35754a5341424355684141684b516741516b4d43554246663870616475584243516741516c49514149536b4941455a694b67346a3854654c75566741516b4941454a53454143457044416c4152552f4b656b625638536b4941454a4341424355684141684b596959434b2f307a6737565943457043414243516741516c4951414a54456c44786e354b3266556c4141684b516741516b4941454a5347416d4169722b4d34473357776c49514149536b4941454a4341424355784a514d562f537472324a51454a534541434570434142435167675a6b4971506a50424e35754a5341424355684141684b516741516b4d43554246663870616475584243516741516c49514149536b4941455a694c7748366836666c2f34503049324141414141456c46546b5375516d4343);
INSERT INTO `commande` (`rowid`, `rowid_client`, `rowid_periode`, `nom_client`, `sous_total`, `total`, `date_commande`, `email`, `adresse`, `no_app`, `ville`, `province`, `pays`, `code_postal`, `telephone`, `poste`, `notify`, `facture`, `identifiant`, `paye`, `prise`, `signature`) VALUES
(21, 8, 1, 'Jessy Walker', '59.76', '66.33', '2019-02-05 20:56:02', 'eyJpdiI6InN6b2lzUXlBUGJUWGVSVTF5UDJpU1E9PSIsInZhbHVlIjoiQjNldmlQMmVFM3NkY3JcL1FYeEM3R0h3aDJNQ1FLRGk5UjM0WEJMb1RIS3c9IiwibWFjIjoiNTEyYWFjYzYzZmE3MDA1ZjBkMzFlOTkyN2Q0NTM0MTE5MDE5NmI5Mjk1YjljYWI2OTIxNGNjZTgzM2U4ZDgwZiJ9', 'eyJpdiI6IjJhRDdnM2JMNHVJZitDVlk1UDd6M1E9PSIsInZhbHVlIjoiZGxXWkp0S09WdXlldURuUW9CTnJkOWdVN25oc2VVYVA3T2xkSTBWOXliTkxISTVReDcyck16M1FyQmxqRWVHSyIsIm1hYyI6IjdlMDU2YTE1NGZmMTdmYWFhNDk0NDViNWFjMjk2OWZhY2JhOTBjZWQxY2NiYjU4NGU3YTJjMmFlNzJlM2QwOTYifQ==', 'eyJpdiI6ImJ5bVE0WndCOEpRRHYyUFlqcnpDM3c9PSIsInZhbHVlIjoiY1h3TGk3ZE9mckIzQm1MSlZQQkNzUT09IiwibWFjIjoiY2FjZDQ5MjU4NjZmMWRiZjAzZDMxNTNjOWViY2MyNjgzZWNmNDU2NjhiZDg0NjRhNjM0MjllNTZiODYxMmQ2ZiJ9', 'eyJpdiI6IjN1VTNmeXBSSlRKYWlyQW5qXC9vTkhBPT0iLCJ2YWx1ZSI6IlpEU20rTktMR0lvUGt6eExLRDlsa3NmanV5N2JRXC93QU5xaytjdE9qRVZZPSIsIm1hYyI6IjRiYmI2NWUwZDEyMDQyZTVkN2QwMWQxODVlMzEyNzFkY2Q2M2NjYTc5OWY1M2Q0ZWM3N2I1MDhjNWIyZmUzM2UifQ==', 'eyJpdiI6IkY2YzdKQm5hUlJcL3Ftd21VZ1l6WWx3PT0iLCJ2YWx1ZSI6IlwvU0ljVHpGWFVVazA3Zkl1Z2gyQW1BPT0iLCJtYWMiOiI0OTU0NGFhNTBjMmRkY2FjMzk1ZDIxYzZkNzZiNTQyNmM5MDNiZmEzOGQ4MDlhNTMyYjE2ZmEyZTMyMTIzMDRhIn0=', 'eyJpdiI6ImYzTEVETzd6VTM2YWFNb2t6dWVcL0RRPT0iLCJ2YWx1ZSI6IkxUUjJ0cEFiSWNrMkQ2TWR1c3VyQWc9PSIsIm1hYyI6ImNiMzRmZGVlYTNjZGQwZDJmZjkyYWY2MzMyN2ZiZTJmMjFlZTQ2ZGQzZDlmODU5NGI2NWVlYzU0OTY3ZDcwYzcifQ==', 'eyJpdiI6Ilo5MnJram5UZkNMbHFJNVVOSTlcL2lBPT0iLCJ2YWx1ZSI6ImJUT2ZcL3JWS1llWFVSQjlTVlVBQndRPT0iLCJtYWMiOiJiNzZiODgzMmYxNjFmNzhkYWY5YWI1MDNlOWEwYWM3OGUxYTZkY2FhOTgzYWEwNzI0NGFhMjNlZTY0OGJmMDA2In0=', 'eyJpdiI6ImdZYlRjT2NvUzBaWUJ4cmtJbERpZWc9PSIsInZhbHVlIjoiWURRS1FlWldqTzlBM3NLcUoxeXlMWTc0eU10eEo1b1wvXC9QN2MzdWEwK0VBPSIsIm1hYyI6IjA0NWU1YWI2OWMwYjIyMTE1ZDJjMzFkMGM5YThjZDFmZTk0Mzg5MGE2YzA5NDhkYzljNDY1YjBiOTNjNmIxYmEifQ==', 'eyJpdiI6IjJWcHNlSzVNYXlUdlJYTmkwMDkrbHc9PSIsInZhbHVlIjoiXC9QXC9GQmYra2h5S2VDaEhEMjBUWUxBPT0iLCJtYWMiOiI2MTI4NWE2NDRhMTIzODNmMzRmOWRkYjYzYzNlY2IzN2VjYzI5MDhlMjBmNWQ3MWE0YTk4NjM5YTJlN2Q1YWU2In0=', 1, NULL, '', 1, 1, 0x646174613a696d6167652f706e673b6261736536342c6956424f5277304b47676f414141414e535568455567414141763441414144494341594141414351326555674141416741456c45515652345875326443646832587a33767630704b5255564b455a6f5552656e496b4a536a51686c4b366967616a43654f6f554d70777a486b47464a456d52316a2b4d74553055515a547049684361575142757155516963566d6854584a33733536367a7535336e3276596637337676656e3939317664643776652b7a3131712f39566e3775652f665775733376454d554355684141684b516741516b4941454a534f446b43627a4479632f514355704141684b516741516b4941454a53454143306644334a5a43414243516741516c49514149536b4d414743476a34623243526e6149454a4341424355684141684b51674151302f4830484a4341424355684141684b516741516b73414543477634625747536e4b41454a5345414345704341424351674151312f3377454a53454143457043414243516741516c73674943472f77595732536c4b514149536b4941454a4341424355684177393933514149536b4941454a43414243556841416873676f4f472f6755563269684b516741516b4941454a5345414345744477397832516741516b4941454a53454143457044414267686f2b4739676b5a32694243516741516c49514149536b4941454e5078394279516741516c49514149536b4941454a4c41424168722b4731686b707967424355684141684b516741516b4941454e663938424355684141684b516741516b4941454a624943416876384746746b70536b4143457043414243516741516c49514d506664304143457043414243516741516c4951414962494b446876344646646f6f536b4941454a4341424355684141684c51385063646b4941454a4341424355684141684b517741594961506876594a47646f67516b4941454a5345414345704341424454386651636b4941454a534541434570434142435377415149612f6874595a4b636f41516c49514149536b4941454a43414244582f6641516c49514149536b4941454a434142435779416749622f4268625a4b55704141684b516741516b4941454a534544443333644141684b516741516b4941454a534541434779436734622b425258614b457043414243516741516c495141495330504433485a43414243516741516c49514149536b4d414743476a34623243526e6149454a4341424355684141684b51674151302f4830484a4341424355684141684b516741516b73414543477634625747536e4b41454a5345414345704341424351674151312f3377454a53454143457043414243516741516c73674943472f77595732536c4b514149536b4941454a4341424355684177393933514149536b4941454a43414243556841416873676f4f472f6755563269684b516741516b4941454a5345414345744477397832516741516b4941454a53454143457044414267686f2b4739676b5a32694243516741516c49514149536b4941454e5078394279516741516c49514149536b4941454a4c41424168722b4731686b707967424355684141684b516741516b4941454e663938424355684141684b516741516b4941454a624943416876384746746b70536b4143457043414243516741516c49514d506664304143457043414243516741516c4951414962494b446876344646646f6f536b4941454a4341424355684141684c51385063646b4941454a4341424355684141684b517741594961506876594a47646f67516b4941454a5345414345704341424454386651636b4941454a534541434570434142435377415149612f6874595a4b636f41516c49514149536b4941454a43414244582f6641516c49514149536b4941454a434142435779416749622f76497438307954387558575339307679563931772f357a6b756b6d756b2b54715364346c795252723855644a6e706e6b31355038595a49724a486c656b6a66504f3031376c3441454a4341424355684141684a594f6f45706a4d326c7a2f48512b7430707952636c75653268427834774868754370796435644a4b6e4a506e7a41583359524149536b4941454a43414243556867425151302f4b6462704c736c75552b536a356d75793650327849626754354d384f636d664a4c6d6974776448585138486c3441454a4341424355684141714d496150695077766532786e644e3872416b3739327a7135636e6558575364303243793838314f35656376307a796f695176366635392b653576334941756d2b54334f3050384c643034745033484a482b663546564a3370446b556b6c6f3936596b622b33612f314f6e32343254334b787a50634c3961457035616166444f7965354a4d6b66644f4d2f4b386e6654546d5166556c4141684b516741516b4941454a44434f6734542b4d57326e316d326563384750595879584a2f30377978306e2b4c4d6e7a6b2f787464326f2b627454705737396235357030717953333662712f5954584d6137754e797469524835766b6856304d776e4f3754633759506d307641516c49514149536b4941454a4e434467495a2f44306a6e5046494d66344a3263596e684e502f6e463272636a356e705a5a4c634b4d6c566b3378514635523867795333473945704e784c63554d447546354a634f736e76646a6345624a67554355684141684b516741516b4949454a43576a346a34663539556b654e4c36626b2b754247343933372f376376737463784d3343545a4a38514a495864332f766d6a69626753636b65554753377a6f354d6b354941684b516741516b4941454a48494741687638526f43397779474e75586e417034673862416d3456694a6d6f35624f542f5067436d616d534243516741516c495141495357425542446639564c64637379704b3542344d626e3373436749387431447534655263376359636b317a3632516f347641516c49514149536b494145546f474168763870724f4c774f567935797a4245442f6a562f2b666858646c5341684b516741516b4941454a5347444a424454386c377736382b74326c7934596d5a467770384774527047414243516741516c49514149534f4545434776346e754b6837544b6c4f523470627a5a5032614f756a457043414243516741516c49514149724971446876364c466d6b4856662b3336704f3441475863554355684141684b516741516b494945544a6144686636494c32324e6131326f4b61506b753949446d49784b516741516b4941454a534743744244543231727079342f572b56354b66364c72356f53543347642b6c5055684141684b516741516b4941454a4c4a57416876395356325a2b7657722f2f6f636b65654438517a71434243516741516c49514149536b4d437843476a3448347638636366396d4351592f6b564934306b3654305543457043414243516741516c493445514a61506966364d4a654d4b31765433492f446639744c72367a6c6f41454a434142435568676d7751302f4c6535376d394963746c75366e2b5935454f336963465a53304143457043414243516767653051305044667a6c71586d62616e2f63394f6370507459584447457043414243516741516c49594673454e5079337464374d3970496b64362b6d2f52314a37723839444d355941684b516741516b4941454a6249754168762b32317676715356375254506c36535636344c517a4f5667495347456d41474b45374a336c304567345046416c495141495357414542446638564c4e4b454b6e35396b6d2b6f2b694f5444786c3946416c4951414a394364772b79524f3768392b61354f5a4a6e7457337363394a514149536b4d447843476a3448342f394d555a2b664a4a5072415a6d452f436759796a696d424b5177476f4a2f46795375316261507a544a41315937477857586741516b7343454347763762576578724a586c4a4d3930624a586e656468413455776c495943534236795a35516450486a796635374a4839326c77434570434142413541514d502f414a41584d7353584a486c457063766a6b6e7a4b516e525444516c736b514375643275376366756b4a48783231504b694a47774946416c49514149535744674244662b464c3943453675475469323975456237416e7a42682f33596c41516e304a2f436e5362687865323653472f64766476516e4f547a67454b45575934574f766977714941454a534b41664151332f66707a572f7451746b7a79746d675342654c64493873613154307a394a62425341722b5a35474f53724d6c6f766b61536c3165383335546b6e5a4c38664a4c2f73744a315547304a534541436d794b6734622b4e3557364438653654354965324d58566e4b594646456e684d6b6a74316d70465a69773341306d57586d302f5232652b5370612b652b6b6c4141684a49346f6631366238473739326c326e7550627171637a74303379642b632f7453646f5151575336424f72627557496e70744f75412f36464a352f6c7153327932577449704a514149536b4d422f454e4477502f3258676252376e50675834556f6534312b526741534f52774133483978396b4b636e7752317636664c4d4a502b7055764b31536434317954386b756372536c56632f4355684141684c777848384c373843664a626c684e394576542f4b645735693063355441436769384f4d6e37645872654d636b764c31686e4448344d2f794b34436e3571456d34532f79374a315261737536704a514149536b4542487742502f3033345676697a4a77376f707669774a626a2b4b424353774441492f6c75537a4f6c575748694437583550385949554e66657369586e36584c4f4f645567734a5345414335784c77772f7030583544724a666e564a4e6670706b694248517274624633576d4474393632743271764e76666561582f486c38535a4b3756777542793244745172686b33552f312f584665457043414250596d344966313373674f3067436a6e5a7a37564d68383073415261364f4344434a3348746850322b7a396b3979324f753337695a56734b4d686b644c6356706c4363614e6e735a6f454557734e2f79646c392f72586878336448635658367179545858694266565a4b414243516767523066336b4a5a466747432f71696f6935734f51723739333931547851636d655844565a6b676670546e36594f782f5a4f575773457564357964355a4a636d464a2f66705167624b41797344363855576c5075394b567756492f70436679334a4e396264667446536235762b6d456d36664631536137593959522f507876705976695866303879304d593638515a79597776756443567762414b652b4239374264352b2f44626e2f7234754f753070346a636b656443416157496f73336e41384e39582f6a444a357952353972344e4a337965594d51763372465a3055695a454c4a646a534c7730306b2b6f2b706871576b3943654239535a4c4c646272656f7776302f66507533376751386a6d6c37456467726457623935756c54307441416f73696f4f472f714f563457364166415839467669334a562b3668597475654444356b38746c58326b432b666475583534396c5a4e64426b375875517a6442512b652f71313270316a706c6e2f613154674b63396e50715832514a372b645a3732784a5063725072356e6b426c55363071587176665333596f33566d35664f5650306b4949454c43476a344c2b63566562636b7635486b4a70314b722b373838767457394b54397130596145546674584139774454704c6669664a4d37726333662b59684771653539304b48506f3073453652574f5a4142684a4f55332f2f774d7439382b374734514f545844624a4e6272306a57394b4173645354344554315347334d6765656a734e4e544b4339335674715a702f36494b443438396475536e3650375039696b4748747056327a5133394737712b744c535167675a4d6834416632637061796464485a31392b336e42347849324943766a704a33303044626644684a334e48795374657946436438777537712f367a66506470632b3875494c6e3270533939584c384c564a36544e7134396a30704359485174682f546e703567524844483053787256766e4d2b704a353964664b3565516e5576374f4d744e52336f4e617a36466a6356417a734866614f454d6631537a4d592f73594d4446735057306c674d7751302f4a6578314a7a5759324358314a7650375537532b564c744978542b2b65547577622f70626733324362437471346a573477303569576f334d505433784353663247636949353570733437514654454735515a6c524e63584e6d566a38392b543344724a6a533538657663445134772b762b5148776c3549732f5a335a63673763496970314c39627861324847386b724a2f6e72485963466839427037575055747a3154755551614d37443274304c394a5841414168722b423444635977674b34334364586f51633263554e354b4c6d7466487766354a63363649477a63383572656445727a37703538614172454a4458574f656d7552573154687a4667386a745367705266453772755551526852426a726535494e7652727558346879546631526c4e6c2b2b716e2b376a3673504e416f78787a574b54654f4d3931397a486c3046674459592f626d682f572b486967414558762b4c7a5037562f2f7a736e656630796c6d64574c584433753077337774425972466242636a4f7a564a6578575948617551516b30492b41686e382f546e4d2b6863474b5956786b6e394f66527954356b6f45626874494d39356850722f7241594c356e456a59525177512f64744a36597041586d644d3433585853502b64347a416c33707673316c55743373634b506e30304a54506762492f3971653770673765713344677164653635443367486239434e4138503444716b6548334c443147326e3455376a5150624e717a76754c667a3847507a4a563751467548596b5834756273306c3378775538597276616957333537392f6c526c4a79697a6b6f644d2f435453653631614149714a77454a4849324168762f5230502f48775058702b4e393370373939745071704a4a395a5066696c53623637543850714762356f4d667976305030664c6b5030322f65326f52304f6c36556632474551633376414366665530767049302f2b6368764364756a536c786132716e51396a772b2f334a6a44756437473653684c5775526864504f5076384e52763165483661302f3848354b454768784c6b6c33566865766675796e65507a624744392f787554483162634a537545365663726d655478307a34496e2f556c5a615053537751414a54664767766346717255616e39417543452b4b4e36614e2b6d3278787956587a587272425638556e6e65703269504a7757445a483353764c624f2f78393577722b51332f385a46755a2b70322b61704b5053504b70585745312f74304b7479543354304c39676a6d46344f75375677507334784932703137325059784175334764367652386d446137572b304b3743323362464f35303756705457744e707635396e704c4e304c376157386f5054764b636f5a313137577147517736425267357663776c495943304554764644645333737159623746355779424f4f2b543549335844434264724f413063394a2f54345a6642696942494b563466593149726d61782f385875574e7a2b3142505964392b2b3677666d584d656e345162686c724756436a654e65355a3951446530675573733147623633532f315964695365524f4c7a4b563064574874382f4d513641312f4a663465567937416e497777487458507266324c53363469324c37656362764671342b526136623545587a3444394b722f584a5041713850416d484a6d4f464f497a79655879714e79566a47646c6541684c515465436f3730447235396e6e77356f76694264324f654652766b2b62585a4e7344513675686b6b66326d5943346b755a72423146336a634a7758656b3979544139434c35304a6c4f7764763835324e5974485041375941626c63394c677439734b7a446e644f37524630312b77702f76636d6e6942754b78453435685634636e554a2f384c6e456a64366b6b722b7469557a44497956374661544a2b2b4d6a596a51716649376a346c434258676f6176574333444b354f38352b47585a6459526679624a33616f526349484546584b4d6b4532735076695a793756796a4936326c594145466b4a673741663351716178536a5871516c4f346947416b6e796634647a2b745368665a703832752f746f544e767268692b67467a634f376a4d322b6f50387943546361557774472b564e32704d77637567467139534e6d346d75626b33576565564b5834656858526d513647734b4341476e6d32386f54756b4449495833615a6a6b45586c4e746f446c4e4a37422f53634a425178336b2f3747644b79442f502f5a336a687644787a574750706d742b4877714d6d65387a7245343179667a3644444634516948415056427846636b3457424a6b5941454a504232424454386a2f4e534542794b65303652506c2b6939536b335878355848364136563846386d5a597259626f347978566e714f465055527143594f65514e6f7352592f5268313063586a477863652b7054666c683963784a4f365134745a4454352f6830784536646f4442326137564c472b37394a324e416a784e6f3862796d4b6458715175616f596b4b5367665844335a34714e666631353975596b392b335365504937574f5455676c512f714b737455756233394353336e47444e63666373747a446e66615a504d4a5264534541436179656734582b634661774c627148425261632b62664162365353664d554231307644525635474c6a4f62613165634f3359614266502b632b484539542b4175663968493843584e7454576e6d485049726f30494a2f43336e3241776650574a4779694379774670556b6d76654179424a3448533761324a52763878566d4f654d536e5768397365386f6f75356573384977337674643659554953503935484b32426439626c7730497347736631493952467254722b784f2b2b754d565574306637706f627566397645324e2b74416d6e6576517674745970446e69716f62715a6a734a53474268424454386a374d6774572f7661354e633652773132757731592f77333634446573562b71584e5876473141386844626a734747425179305935562b6468457246512b55444f794f6d3770734e54506b7a744e2b7837585a6c4c4f4a556a78755076745763782b70672b336b4a314335335933385835394b302f707a434c59656279696c30726439764e724e663350564c51626f2f71695a44686a4d3235616369625958307162493474515567722b336e784b6d384d73354441744d54305043666e756c4650625a5a4863347232744f65454831664634523730526937666c36664d504c7a73616432513354597477326e6937675674464a4f43506674723336654c2b48766165494659494b66395a6a4e78426964614d754e79724d714678442b6a7744492b69702f37426932507a36422b675a7269622b4c48456267336c4f45644c3845396c4d72684144666f594a4c457758424c746431384f564a324e51573466655354524775503838654f736843323832567668576566466355306642663641756757684a594167454e2f384f76516e7374653137526e767145666d677762356c6875346e346e4d366e2f6641452b6f2f595a75386836784146736a41572b676747424b354e46455a7242582f6c756c6a534571374872397a46464844445559546242347967593235472b7244326d663049314b667055353338377166422b553854494d706e5579745547682f7a4c6c4b706d493037676973526e307462756357614b3331726e536869717653675537354c39695542435379496749622f3452656a4e667a50796f586456755964617879303138784c4443617356364f393975646e666650304536424c49544c6d544c59693068445730715a5358594c526a33344552542b6d5570534d4b7463362f43767169444d5471472f39324d786562656278686e546646706d696a353974556c454f3662633266736c4538326c444f6c6c706d31636e59584f5034447246657a425732707463596f4d2b656d796e74706541424536586749622f3464653272627137793642766a665170444e4d3273506569674f4c446b2f6e2f523778586b702b6f2f7575525365376455366b36574864583557424f7855674e69767871456a4c6f4c4548614538456c756f417367645061646167332f325063392b626b734d7677782f32486d4b53685174413647636d4b454a6850675035575a49363644626747346737356a6831453470362b645374416e6163454a4c412f4151332f2f5a6d4e6266464a33576c5036576458734737394259482f61312f586c764e30612f50334c39326f62414e632b32352b4c764b6a2f656b6b6e394742776f6935565a4e685a4f7a36446d3366766863455064357570467646554631734e78384243742f686d6c477154742b7343576964622b542b50563868435a6d746169474c474e6e45786b7235624e7461687172324d4765717a392f50616c7732783770696a563166323074414167736e6f4f462f2b41586961767358716d472f49386e397133395472624a632f552f31355544336e4178686342535a496a76486e50543254543361667247695737745a61414f722b32346d3570786e3662754f3539696c2b79463063497a354358444b5457704d5a4b6d2f673776382b386565396a50662b6e64366951584c356c7a394e73614b53756e63396f79562b71426a7157356a592b646f65776c4959454943477634547774796a717a6f4c51333356582b66334878764d753075644e7533626b674e485362584a71574152334850756b7552336430794d32347a506134707637584b68716c3073794e3644386245457159314239466d53626c5079595a326f7a72706c71642f424b546632557a4a743358796576364f61395a447836733374316a4c507449622f324a69747772396571314d726544626b48624f4e424352774151454e2f2b4f38497655704455476f2b4c505852763875762f51704e435634464b4f797274794c6a7931473838756d47474469506e595637634a7772497638744a6c2f43496a39776953503336454c2f73566c37747751504856696659643231775a38587a664a69345a32747342324744316b554d4a3961366d6e334966437876744a4162782f36594c4f6c356a52706a5838536550356870474143476f6c75425835387951664d4c4b2f745456765853326e2b4f3574437a744f745a6c59473176316c594145396941777859665048735035614565672f524c41474d4951526559326a4e717847664f386c4b4c4858445359734445695330387475437864307455304b466b792b506c356d544c614734536c6e446a6936303177587648352f70467549335a4d376c4f4e5457616d4f7a596274626e6637366c306e364d664173713575547245372f6c512f6476382f5654554c656b33682f5a4a75362b72626e764f796d513270762b6c74353344384b664f51696e2b754f5866713657767666704a594645454e5079507378776665553546796b4f7353587453564177527375695135575a4d6e753670696536715974754f3855394a3370546b4e5a582f4e4e66656e4f357a637637434a4151736372704b3967744f57346c3561473835434c79385a524c536656366c71786d41762f4f635567666e6f642b6e4a766e6a4f516338554e3973594b6756556374535856734f684f527468616e4b62645653445458657766657467457a786566542b5358362f5332564a51443262626e4c3462306e716247376e46573373793454504c3177684c39383134455a7456393246767633356e41516b734245435533796f62775456354e4d6b573039626a66565136384758426b5949527563757753442b78695434396d4b67484676594b4432704f74326151702f2f3263554c66466a6e38764d4653664331782f67763874596b377a4f7a47785231426e447451646f71706c504d38354239344d49435637495274626330624d52774b52755444764b5163356c6a724b565836373166742b6b7463353871315333764e556b4d6b4b6e366e474e393575787a3672586e382b72374b34586666594f6271546e587937346c634c4945446d566f6e697a416b52506a69785a6a3649326432387254522f613354334f4d2f36394b776b6c55482b456b6b442b6372474f6f586a584a3837722f2b365075524f3931335733424b2f703032504d5a4e69673336553743657a615a374c477a6267616d4771412b425a776a6d48737150532f7142786357337556376e6c474d697666364868757130486f577237724336744c71614878516b6d6333696e50792f354b4c46722f487a32756a643670734e6a32475864516a4e594d706650486e71674b384b4767544b2f4f4c6e55736c4e31436b506556516931747536723577794b564959424d454e50773373637a6e54684a586d764a6e4c68717653764c594a502f636e51526a43484a4e545a41666d776d4350392b6c47787a3348437050386e383354334b5a48557268696f5378535676363572534c6b2f503337445a526245424b7a4d53594f6332644a595035333642546349304730655753334b5a7a6961706a4c57726d76395464624330786948584d75374676327a6264374a492b65776e653558657a6c536c302f506971534e66576376665850462f6166666139766e4c503266636471702b666f786a5947483257334a59614b642b633549505055484c4c372b5753313033645a69497778516637544b725a3759454a5948426a6e48416178516e374c5134382f6b58445964672f64412f33492b5a44586d7479683250556348714a2f2f35747134476f7038427443396c2b4b4b52574377474e4244624f4a585873776c386b75654663413833514c2b2f4a783361334d44632b6f33393875682b653547646d47482b4e586461472f354c382b792f6278634467706c554c61584f6e2b41796f34787132484f4e5255706c4f73665a7a46514e62342b39564835336247696d37326b787843394e4846352b52774e454a6150676666516b577177425a635069434b55597a56364f34766c7976632f557066756c7a5459417653417833546d4f6d7976334f664f694c6172304938796d6c3775743554424638647847584f72586f6b67714a6e616633666276717258632f3579454d2f63636b34625a452b58384561674e344b566c744d506f666e65514f6e5a70553579586d4257474e487a567941636c55395a7a4f72594b7572742f647a493373646e584e7039373074526d434e46703376784b3877392f534a58496f5435424f393965362f797533776d743273317a644c344d4b48352b4168762f78312b41554e4d416468784e724d7565516c59626754716f503433707a365334754144394b2f495678382f6e484c7131686952756f58583334507a4a5645443877683143303633764f4d50673535535434634b714e786c6e36747857456c2f3537694b48424463574e7a7067515276375475745372704268553370354162617774596150483779782b7a63543649426a6f47456e6c686f626635563375502f7573625a32786173756e2f565066656e7872645273355638325866645a35616339794f465869352b71446e6662336a734d744e726f633943675332417942705273636d316b494a336f51416d7777534e5735792f2b6667475179306c424a65573568383150634b4468782f62533542787a515036355375456d645a2f427a4b384d66697349744b515873674f6e4f337154327954373235793662386c64574d3661514862373439302f7954556d656b41532f364c4643762b56323763376454644459507466596675724133726f434f376569724b6553594d6a7a65565558654378634f50446863307152774f594a4850734c61504d4c494943444543434c41395638322f537037654238695a4a7042324e3272704e7244477271437052675a744b4645766933464f45456d4e6f4666494857465a35722f546a685a344f3068465376532b46326b52374638443932494348754e77544531304a674e6a5577536e3250722b6c4f2f792b6130336b2f352b615054533279785571394e5a75704e3332316d79432f68795147554a4b32346a524d4f4e4468382b796e42435142436677374151312f333452544a3041527165382b49354d474f655872765032464251573079454b45675475317939466e4e6c39436a4538576f6947437763616671544c6d344e503934593150624e474c4d62675366334a582f324349766c7474773033544d37764a48394e51773330484e377461634e3871373367786e4b627747563969544d4d78336a2f53486e4f67674577564f315162754674326f547072633156592f3178582f2b5559362b3659456c67734151332f7853364e696b3141594665465972703965586674697976434a33536e32396335597a784f61456b582b6f61714b6a416e3453554c4573326f484577786e543556666e2b725331644b7536455a506e444234512f5668516d36486e4f4e6a56484b5a7552447a6e4342777243416b36663777312f49715832386832687978523062544736332f6c6658575232414f765a376756736a616e75774b6556336a526f42573676555739616f6a6e4f59597450585a76535a59704d32354831615568732b6f33487a516477494c576c6c31475752424d5a2b774339795569713161514a6b4b43466c4a363471626656594168674a3748316334354e4f48764d7636644a3966764a41656e32712f4c5a427658322f7448454c4b6873557168693338787179676341676f612b32674e75624f782f7653387a4d4d2f424e6550746d552f743437367359726d366b6a4b32462b677033717634442f333753355535784b6c306270317333784b5a652b336f54756454346f4833667a7a4850507a6a4a4137734f2b467a6e4d3161526741544f49614468372b74784b67547758635a3476316553397651656c353366546b4a5148486e377a784f4d6f632f6f4e67373773726e496b4b397649503667537033596a6c4f79545842532b68464a626e704f30522f533031464571322f6c79624d793947335830546341414265365355524256424233384a4d48794769304c394e54654a3461464b532f4a573645573570444370584279366c2b4752642f2f72626732684f543348366945394d36622f6f6e566a646c68357a335573616132722f2f78354b776155653276716d71613648416735765a726434734c65563956343856454e44775838456971654b464244426d4353616b38466774722b37635945704e674173376168376769775868466f4155636267766b4161526a447a58617549442b707936313046355a453535514f64666a61485071537a47474365765a36584e724e586a6570734e54642f556f2f693763684c6242757879776b757872522f594634375039795a51546e305047646a4c5a7a74754e715455626438624e705274494754354e7a644c704c51644b726a35764c6872664d6a35447456337a6e616b4e53596d42756e7a2b644248467a6236706467616d3869356b6844303061562b686c7454346f4e4b345554534e37397033303732654a35627a312b705069752f4d386d583739486552795777575149612f7074642b704f612b4b374b6a425270495833625649576b6976745136786f4453417763306f52656c412b614c305063613271686541782b396e3246504f756c4b75354662573757785231515a62644f2b5565774951594a4e7844554c6c446d49314337765a42393552447059713930686b48492b2f64524f36624b2f33456a686c78375a4c423466534a4e4a71307462796a4a496f5a42696e4349514b58774d634942415a38313554506e724b725a75386234344735444e6a535277466c366379424331664e3764335663796e4d5552305258474d77524839536539746442366d4d59323159434a303941772f2f6b6c3367544579526a536a4765795437445354716e51574f2b354d6f5847763731397a79484968734c4b74723279574f504b394c443938796d7858796f394d704a57742b626978736b2b6134754c7142576e5130447870386e5934663774616839764c6b706d6e756a396158644f39624f6b4f4a63704f6e634a562f567065394574314a66596769682b7253663745456c5a653251766b36684451554c75526e45765242587235654e6e4e54444f694f62626a43322b52322f5344675a4a78626735743247377075542f5042466a6662344f54654a3557623072475a386275452b3265637a73752f5139652f56564c6370666366324f516d736d6f43472f367158542b5537416752305559446f52556b3451523937776b544f5a7a4c646e4364734b6a3533774930435839675938726862374a4a53464175336f6c2f65633457352b762f524a6d69544c76423735662f375a4233616330676650346341363046514c5a73324e6f68554470314c4d4c4a4a513773724f78565674647667336c7150596b534e4e61444931382b6d453848514b3157413535727a6b48363567634546716d394d7a4a4178614950624871364779465175543756373173636c65636f46796d475163367458752f667832586265515562662b624c4a65337850743854534a3539374639324b6e6a552b37707a46725a483534445a5a354641336158335a2b4a7745466b314177332f5279364e79527944517073766270514a6658745147654e5a412f533662424465635976786a494f43374f2f51302b504b6457394f757a516f75505268686264476d67617262624538437856676a563336663249303975332f6234385367374e6f6b45737a2b42543036704941634a385063467642654478466334426876536b4e336942356e74534567466e6555746d6f3376387345326d4e4d396f325836615058586170446762366e382b6631652b7671514f4f695453534a44746a4d346437547968525a6d36375233524356494f4d2b5048694741346837444d6974583177357977614b564d5a7361497267786c68714a6654567865636b73466b434776366258586f6e666736422b68713550495a786a767351426b3562424f6c594d4e38707964636d2b523837464f4130447639617376556f787947414d596c37773657547644374a2b3835676f4f425454796172537a565476475753702f65594e675a2f715278392f53526b494e70586349763779367252324469426663632f37336b4d66564c3439746c306a6233787150576f503050366e4d35664e476343706a6c6c52376735326857375248417437774b473853366854666c7a30586a6e2f62794f34796a5034554c3472556b7730736c45526f596f2f72544a4248425059695055563272337362492b745876526c477657567965666b38437143576a34723372355648346d4170795934543545595337636833436a474f732b4e4c5771664c6d6564555077305657773574546a326c392f4171314c7774694d4f6658496e4d356a354c57423462683073546c395330383170776a73725933636930366a65366f3179574e317a76732b485535705245365a78684d446d6e537252585a39622b38797873767a3343592b7072765236635068764765495533704538774148496c5249623333345359324d586e5871574249755548437772395133536155416d70574c2b394c7a4f516e73494b4468373273686758555259464e796c74764f4746654e645646596a376231366551596f35683072365372355151624e7a45326672586774343652524a72596665516856657a486b4f2b444e72764b52625573397446747a4c506e7565793949736e647138374a6c6a4f6c71303839397067314c7970534f66745733543932706133383953526b37746f6c33506878497a4f4637474c4b2f45696d51424b4349707a5363387642753343335a75444864716d582b2b704474716d53695970336e73307a3951754b4c4f5639367a73666e35504130516b4d2b61412f757449714949454e45734264684e53494647527142566366736e556f797950516e6a716635616252616f34686973484461652b75464c4c31382f6874662f584172436c55384f563269347850375762694970705853454a4162366b6b76653970376b5839442f3035764844766f655a474c65684877504863743366313658766639543576726d7a71384b7448616e6373626e742b6f366b6e30723458424e524f4a5730476e335a547737764f3571434e6f796a6a76364572737467337178437563665747416c392b4e716f6c7475415a5865324171655a6e50784c594241454e2f30307373354e634f5948324337424d682b42692f48724a6d6130736c77436e726d3232485978516a4f626e644837316e4a4b575533314f4e58646c35326c6e53464171376739444d3658513334393062686f4539584a6a74492b306d3571684d514c376a486e6573786a43754a7a73386e452f354d6e776c47342b764164316e45344a63443376526f4e62675a2f744e6e4e54735355646152332f6755464f305551795353473734714c7173646b6b554a7951464b643968646f6f392b73655a76504472556574413448724a6143386235382b4a34484e45394477332f777249494346452f69364d374b4e554a48316c5176585866582b6e5141474d6a6331355752384c426669546834355552616159715469507246505668744f58546e5a4c724a76304f5a59426e56374e6b335530694359765258695950424c4a38337649615432783538695a71446458474834733036637672654355637a4735326b7a544a544e524d323366563971777838645352764b35674333717145334c4e77516b41454e6f5138534672415a514d6751784b48486b474430476644597051545751304444667a317270616262496b434b546b3730533137304d6e7663503869676f61794c514f734c76362f324646306a534c4e764562632b2f564e467572784c66624d413053397a49544e4c71554c4e6153366e7278686a68785a3077546875732f616745796c5561332f77512b6857752f6e7375356e61705639376b763450546242734d5972786853664431787843634335315255705749624a4638623630557566614836744866647050583851786f45634a5a69643730466d31554d614f6258734a6e44514244662b54586c346e7431494346415a722f5741357961745057466336745532727a536b356137764c614b724263474a4b51436575504a7a657a70575346654f64617243637a4249342f4e6f6571304f474b79724233725a36396b4d50654b4a6571336857317034684a2b30666c75535372734c756d45717a624d376571314f53416d3459366d4f6b5475505a39734f6d674a75664d61356566585237564a4a50502f423673785a314144622f7068354a6b536b325658336d376a4d534f446b434776346e7436524f614d5545434f436a47465074742f6f646e572f736971656c366a7349334c513751533270446f6e6a4942306942646671674d5935345a5667546478686274467a6f50626d34686747474959317269666f55677675504e38326f4a6f32426962427747544d4b734c764961346c2b3069646333364b6172336e2b66465055596972373978656c75536133634e734e4369454e72665563524b4d5256423276524534316d5a7a376e6e627677526d4a364468507a746942354441685151772b4d6c4567694654584262776b66312b33586f755a4f634477776d5569723366314e4e4e704d36707a716745466e2f4e424b66612b38364150504c3437646379354a5366397276793076502f517a5930395133456b50597468374e754e48423771573963397557337a2f4e5579433342306851757645305373756e4d4b57535a497474554554624337316a4679507a396a734a67632b706a337849344b5149612f6965316e45356d68515177397373663143645937664e48424d53744549457148344641665a714d47386575594e46614c5679426671744b48546c4666766f683036344e30644c2b6356314b306e333732335853582f706776732f656f304e7543386a51784d6e3433335562655034654937766d536e395462437236364558774c4735643562522f6974536b7538596c494a706141302f7150762f61656639656b32713246504c714d7765666b594145476749612f723453456a674f4151797657316642682f67474538434a516156495947344339576c796e3153586453456c337456727a6133676a76353342556750725746776c795434726c4d666f355568747763317a366b4d30374f7138523671586b4a62664f364c756b334e6c45746642356954577062735632316341356d4279474a57684e734f626a3055435568674141454e2f77485162434b426b5151772b722b6c796f71436a7a55466d49616d765275706a733033534b424f78336e74632b494b4b4354472b3172664344793071395a365347796343442b6c79697a4432506a7a662b55414a5a675057574e4b6870693243344a4938536e66522f3632636a2b5a79762b3839584d762b68444c7742687a5375747563346675524837714d5776334d5678366542634a694c35534e7842784c365634476639566e706c61442f755477475949615068765a716d643645494963444c4979526b4746584b6f612f754654463831466b4b676476576859692b566533644a3632642b4c4265663168642f794b6c386d562b6449764f74535335565452796a6b3277382b306a4e63703941365976474f4d7677703931356d37574c2b75337a383972645a6f704135625047724e65434e615849317a4f7268386e6c66376e7133334f35472f5668346a4d534f416b434776346e7359784f596755454b4554443657544a4b3635727a776f5737595256724e316d37706a6b6c33664d645666673635445438436b775569455776337545333531374472776834356267415a56437232344d665178504d6d6e74492f586d614d79477042365432346869414250447747616b54674d37702b462f39613777567447486a5133705a656351356c687558746855736745745262726138546a744a2b3371324e694a4f655a686e784a594451454e2f3955736c5971756d41416e717077454675454c446a396758587457764b676e6f486f355561614946345868696c43686c633041426c3874787a707476583453416a784c75733268726b5a74656b774d79564b55696e6c53674f77446b75433273342f55626c4e543359693047594934396137646d7559302f48487265554948594b714e7a4336653348725772496b315964376c7658744c45344e787245336e50752b437a307067385151302f42652f5243713463674a31384270542b59736b4e317a356e46542f4e4168516762586b377964676c4544586a7a736a63486571674e5568354b674b5447726249686a726637316e52786954354f71764b2f7a2b5757666f6c36352b4f736b393975793350493742537661622b777873337a61725857412b50676e6631622f53505952426a4e2f37584366663964687a75694b534a6853586f694c454c645275506a57545937352f4579327033556867475151302f4a6578446d7078656751776f6837535a656f7073364d4b362b3253765048307075754d566b674174346d7a6650764c644843786566674271734f65682b395a5354366b653444435857532f326c63656e4f5342565350635a796a53566375516f6c3337367448332b564a6a3466564a4c742f46424e576e343165623066437671772b7a5558706558365833664b374f477351616b2f44674638376f59367141365431563948454a6e42344244662f5457314e6e64487743584d75546b71354f515565714f6b34444f6131544a4c41554176685866316d537a2b775577756564643552695466772f726834457642354c4f49482f795770776269544937724f507449584863504868744a2f383855562b7264755537395076584d2f574c6a444631615a315535724c494b2b7244382f70356f50623171737167477a6d2f716d70576c352b7a4f636d4e314b4b424351774151454e2f776b67326f55454b674c766e595454756c6f34616554305835484155676c67694f486a6a7445356c77764a6b4c6d3357562f493462375035766c726b33786a4e54436e32542f636e666258365479585a467a57473555535639466d562b70546532454937337073616952383970424f6572543537695266334433336d69513337654b6736734d53666e7949314b553931505552435a774f4151332f30316c4c5a334a38416c7a4a6332705679354c63423435505341306b304a3941653871397237383578764a396d367739785a416d69314874366f50422b63722b71733336354a39577351676c695063486b32435549327a4d6350575a5177376833332b6e4a492b706c432f423558783238686c61793549325a485077746b384a484a794168762f426b54766769524c67432f70467a64796f7a5074624a7a70667079574275516d30686a2f784270783039334539496c337077354a774131656b44524239524f66753836557a46616361776766334b765247616e3372744a647a6e7354587451506d6343647169786379547a4a4934574a454c456b7459344b74683743336a515132515544446678504c3743526e4a74446d76576134362b3759434d79736874314c344f5149634c7039315770572b4a31545666664a5366346c7954743250384f672f4f6775312f3337373642416d6b3379392b5066763253704131377267466253415a4d5747486c7464794f4132394b555574634f4f4b39434c76556459493268546d616f766f496247566c38324a51564b656c5032324a6c6a4538326f2b663337647a6e4a434342666751302f50747838696b4a6e45574137443059494c5738652b63764c54554a534741636753636c2b59527858655146536167467341596861784631464a44362b2f6b5a535735655457434f6d67713134553877625a7561744337366869706b5276715550614332516459305a563359764246735863752b626c3137714f476a457467324151332f62612b2f73783948674f7739704e757235623253764878637437615767415136417269416b4f627837674f4a724d6e6f5a346f4557574d4d7679544a3331527a2f6f6f6d5151416e346a644c5168616d716152327261726469596954494a735368623171495541584e366b2b556d396f7976503039397464757444614a6574486b3378756e30353952674953324a2b4168762f2b7a477768415168634a676c42654c5662776274306152416c4a41454a54457541302b5a50532f4c705062766c78507170535337702b667a5348324d4452476177326b304759356f6735616b71674e635a7964683045414f4241583639426736785449394d3871436530416979706c6858485a426362697a61514e2f584a626c626b696632374e76484a43434250516c6f2b4f384a7a4d636c3042483478535233726d6a4d5756424836424b51774c3854494a36474451436e3079574446746c347147704c676173334a57397a37546c46616175414d3064382f346c35774356717a4c797633426e35333334427548304469317633494c71766735625a574a42657451673347786670634970723635776b6344414347763448512b31414a3049415678367574776b55524a3654424e2f594e352f492f4a79474243537758414c33542f4c514d39546a6c6f4f383939644938746456544143336b7151724a5450514653723349487a755337447a4f33656271724e6d5468417566766439712f696941796c49322b7249625a41317477792f32695644344961476746354641684b596b5943472f3478773766726b434844646a744650796a336b32354a776a6633476b35757045354b41424a5a4b6f4d36315036654f56444e2b56424b716a753872625a304532724d707763576e545875386239382b4c77454a6a434367345438436e6b30335251445841713667533756507330357361766d64724151575259444d5033644a386c4554617657794a46644b6373577554394b4659716a6a5472535031436c4a53377439585954324763396e4a5343425051686f2b4f384279306333533443542f6864587339666f332b7972344d516c734367437550365164724d4e774b3256784b4448465a4567344f4c61633830754d51482f52363045676d6e355766745a527a2b2f31503338686b6b2b70497576345053655077685a7a4967524945765072686f4b334278382f6770714b43787159565647416e4d523050436669367a396e684942664539763155316f6a767a5a7038544b755568414173636838466d64582f384856735a3862657a333165714253523763392b454c6e764f515a434b5164694f427151686f2b453946306e354f6d514258336754314976374f6e504a4b4f7a634a53414143464e546968482b4d37436f434e71592f3230704141684d513049695a414b4a646e447942556b372b75556c7566504b7a645949536b4d4457436544793837336444634a3756444277432b4c662f453036565a35726856754772306e794f3175483650776c734551434776354c58425631576871424679613554706347377762646c3937536446516643556841416c4d54774d696e2b4e5a726b6a7835527a70503349716f59554a387744736c6566375543746966424351774c51454e2f326c3532747470457142433734323671656d7a65707072374b776b4941454a534541434a303941772f2f6b6c39674a546b4341585030592f4556756c34524d46596f454a4341424355684141684a594451454e2f3955736c596f656d63424c6b31426c737367744275533350764955484634434570434142435167675330543050446638756f37393330496b43727678366f477076586368353750536b41434570434142435277644149612f6b64664168565945514779584e77317965393141572b6b2b56516b4941454a53454143457044414b67686f2b4b39696d56525341684b516741516b4941454a534541433477686f2b492f6a5a32734a53454143457043414243516741516d73676f43472f7971575353556c4941454a5345414345704341424351776a6f43472f7a682b747061414243516741516c49514149536b4d417143476a3472324b5a56464943457043414243516741516c4951414c6a43476a346a2b4e6e61776c49514149536b4941454a434142436179436749622f4b705a4a4a53556741516c49514149536b4941454a44434f6749622f4f4836326c6f41454a4341424355684141684b5177436f496150697659706c55556749536b4941454a4341424355684141754d4961506950343264724355684141684b516741516b4941454a72494b41687638716c6b6b6c4a5341424355684141684b516741516b4d49364168763834667261576741516b4941454a53454143457044414b67686f2b4b39696d56525341684b516741516b4941454a534541433477686f2b492f6a5a32734a53454143457043414243516741516d73676f43472f7971575353556c4941454a5345414345704341424351776a6f43472f7a682b747061414243516741516c49514149536b4d417143476a3472324b5a56464943457043414243516741516c4951414c6a43476a346a2b4e6e61776c49514149536b4941454a434142436179436749622f4b705a4a4a53556741516c49514149536b4941454a44434f6749622f4f4836326c6f41454a4341424355684141684b5177436f496150697659706c55556749536b4941454a4341424355684141754d4961506950343264724355684141684b516741516b4941454a72494b41687638716c6b6b6c4a5341424355684141684b516741516b4d49364168763834667261576741516b4941454a53454143457044414b67686f2b4b39696d56525341684b516741516b4941454a534541433477686f2b492f6a5a32734a53454143457043414243516741516d73676f43472f7971575353556c4941454a5345414345704341424351776a6f43472f7a682b747061414243516741516c49514149536b4d417143476a3472324b5a56464943457043414243516741516c4951414c6a43476a346a2b4e6e61776c49514149536b4941454a434142436179436749622f4b705a4a4a53556741516c49514149536b4941454a44434f6749622f4f4836326c6f41454a4341424355684141684b5177436f496150697659706c55556749536b4941454a4341424355684141754d4961506950343264724355684141684b516741516b4941454a72494b41687638716c6b6b6c4a5341424355684141684b516741516b4d49374176774546627a42426d785565556741414141424a52553545726b4a6767673d3d),
(22, 9, 5, 'Gérard Turmel', '19.80', '20.76', '2019-03-02 12:20:36', 'eyJpdiI6Imp1QzlcL2kzckNDZ09BWFlwVEhBeG5RPT0iLCJ2YWx1ZSI6InNkS2FQN3BjQUFGSnBTckdRYlFCR2VkTmxzb2ErMWF2ZFBLU0x3OURyZkE9IiwibWFjIjoiYTI0YjFjZWZiZTUyZWRhYTNkNGI2YzYyMDVjNTNiOGQ3MThhY2JhOGE5ZGQxMWZhYzk4MmE3N2NkYzU3YjY5ZSJ9', 'eyJpdiI6IjBFaUV2cUFkRjRibVRXcmdDanlhMVE9PSIsInZhbHVlIjoiSDdvVnp6d2NBTHJ2a0FPN1VJUWkwSldqN1FOTEw5YW9TQ05oMnVMSDVMZz0iLCJtYWMiOiI3NGUzNWFhY2QzOTllNTY2YmNhOTBjMjg4ZjJiNzg4Yjg0OTBmZWYyNjRjZGIzYjViMzhjMjkxYzAyZTQxOTBkIn0=', 'eyJpdiI6InplZmZcL2NPYVJJXC9uZlNvZ3k1SEdaZz09IiwidmFsdWUiOiJIOG1oY2lyc2Y4QUQyMDVJNkFjRW5nPT0iLCJtYWMiOiI3ZTc5MDIxMzdlYjY0ZTNjNWM2N2QzMGQ1OTk2NGNkOWY2NTk1MGUwNzE2M2U2ZDdiZDcxNjlkOTRjMTlkYTQxIn0=', 'eyJpdiI6IklmRVUxMzV3NWRXUlVNV2FaUTZTZ0E9PSIsInZhbHVlIjoiR0laSVVjeDdvZnBmMFVJbEJnclBQeDhZZTh1NVFXR3FrSmxNWld6dW03OD0iLCJtYWMiOiJmOTA2Y2IyMjk0NjExMDY2NTM2Y2UyMjI3Y2ZhYTM3ZWFiZjk5YzFjZTVjZGZlNmRiMTdlNTBmNDdlMzg0MDE0In0=', 'eyJpdiI6ImhwVkwzZTQ5TUtPK3c0NjFiTmNcL3JRPT0iLCJ2YWx1ZSI6IkU2R1V5TzdFUVY5YzlOQUFRZU9NSHc9PSIsIm1hYyI6ImZiZDdhZjFkNGI3ZTQ3NWQyOTA1YTFkYzE5ZjhhNzVkMjhkOTY1ZjBlNTY5NjhhODhjMTE5NzUyMWNiNmQ5MjAifQ==', 'eyJpdiI6IkFqMlJXMG8rc1QwU2VGeitJOHJKRkE9PSIsInZhbHVlIjoienk1Z1JRamlGQjFSb2ZBbld2MDRJUT09IiwibWFjIjoiYjgxYTgwYThlZjExNDg5YWIwNmJiM2ZhYTNmMDRkMGFlMDAwOWIwYmY5MTM4NjVkMmQ1NTZlY2YzMDNlNjljYSJ9', 'eyJpdiI6IkNoZUthSndWekc3amVWVkd4N3IyMlE9PSIsInZhbHVlIjoidUNHRW9hdlhrTUtmb2p3SmpDTlhuQT09IiwibWFjIjoiYzAxZTQ4NjM1ZDQ1MTIwZGYyYTE1MWE1Y2U4M2QzMDcyOWI2M2ViYThhMzdlNmQ1NGM3ZmNiMWY4MjQ0NDRlOSJ9', 'eyJpdiI6InYrMlwvbjZvQzQzcWg4TUlyc2s0WGJBPT0iLCJ2YWx1ZSI6IlwvSlowbW9IajVMeE1MZXVLb1BaVDZvTTBpTGl3Z0NTNnp4a2ZCNm1yejd3PSIsIm1hYyI6ImMxNmNiOTdkNDM5YzU0NTMwODE1ZGYzMTgyODEyODk4OGFkZmJjYTkyMmQxYWRhYzIwYzhmYWUxMDRjOGNmMjMifQ==', 'eyJpdiI6IjBldmVyZk05OERUczFrQXFZTEhEbEE9PSIsInZhbHVlIjoiV1ppUEM4T1FxdUhmRHUzNFVqa0p1UT09IiwibWFjIjoiMDAwNTgzZmVhODVlYzg1NTkyODZjOTVlNjUyYzI1NjAwYWRkM2FkYWQ1MDk4YTE1NGVlNmQyNjU2Yzc5ZDRiZiJ9', 1, NULL, '', 1, 0, NULL),
(23, 9, 5, 'Gérard Turmel', '24.55', '25.75', '2019-03-02 12:25:05', 'eyJpdiI6ImdxOXBpYzl6bDF0Z09qUmFrbXV6dlE9PSIsInZhbHVlIjoiYStuV29TTG9Benc4aW1VN21aUHQyNE9ocTJ0K2lYN2EydWFyajMyOCttVT0iLCJtYWMiOiIzYjNmODY2ODVjMThlN2E0NDEzZDE4MzczNTM1NWRmZjIyMzE2ZjJhNmI3ZTA1ODkxMzRlNzg5OTA5ZTZlOGEwIn0=', 'eyJpdiI6InR2dnN1SU9nQW14T1JYMEJtRkFiU1E9PSIsInZhbHVlIjoidzRpeWRMRXV2dFZLTzZMWkh3WnFJeUVcL2tuQ21URFhEM25aYlc5VURWOU09IiwibWFjIjoiODc4NzIyNDgyYjhmZjk1OTI3YjAzMzBkYTk0NDE5N2NiOGY5OTEzZDc5ZWYyZTExN2IxMDYzNTc4OGYwN2E2MSJ9', 'eyJpdiI6Ik5RQkthMXRSdktRZ1k2V0YzOGU2UkE9PSIsInZhbHVlIjoidXFKcndRQWpWRTZ6R3JMMDBhOUJOQT09IiwibWFjIjoiZDdmNjM2YTZmMzIxODcyZDliYTk0MzAwMDZiZWUyOTU5NGE3Yzc2YWJjZmQxZWM3MmFlY2UyMmQxY2IxMzYzZiJ9', 'eyJpdiI6Iko5emx1QjhGcW42eXkrTWlac1RHdGc9PSIsInZhbHVlIjoiWW0yOU9PeXNUbUtJVk5Eb3dZZ0lvXC8wY096cnpKbTN5Nm5FNGtxdDVFVjg9IiwibWFjIjoiMWI1NTc2ZGM4YWI2NWEyOGY0MmJjZWYwN2FhY2E5MTQ5ZThjNDc0MzczYjU5MTE4MTM2ZDQzY2I3MzgwNjVkMCJ9', 'eyJpdiI6IllJbStLbVJlVXB0UXFIT1R6SlNkaVE9PSIsInZhbHVlIjoiaTlEalhWaE9LeWI5dGsraUdHOStcL1E9PSIsIm1hYyI6IjRhMmU0NmZhY2M0MTQ5MmNiNWM1YjFiM2ZjZjZmMWFjNTBlNDI5ODIwYTY2OWRkNTgyZDMzODBiZWI1OWJmMWQifQ==', 'eyJpdiI6IlQyYmZZSDBIOE1udXV0NW1mTzZFaHc9PSIsInZhbHVlIjoiMzA4TDc1NnFmZnAwS29HbnZ5UURPQT09IiwibWFjIjoiZDU0MjY2NDQ2NWI3MjdhMzNlMWZlYTkwNWQzNzc2YThhOWU5MWJlODdkOTQ2OTZmYTMxZmFmZDFlODQxYjIyOSJ9', 'eyJpdiI6IlVhN20rM1Q5aEhoRHNCQ1B4em04aHc9PSIsInZhbHVlIjoiVGc1VW1EVmdQWVFldEQ4dkFQRVBUQT09IiwibWFjIjoiNzUyMmI4YWY0YWRhMmYwYzhhMzUzMWE3OThiMWYwZDliZjliZWQ4NGE5OTAxY2RhM2M3YTAyNDVmZThlMDAwOSJ9', 'eyJpdiI6IkFrWVYxVXlYc2xDRXVZXC80WU9ZVGtnPT0iLCJ2YWx1ZSI6ImhHdnFMQmg0YXI5ZTdzRnNvVGRPN0hPOHdWcmNjM2MxdmlzVWRnbThSY0E9IiwibWFjIjoiMDkwZjUzMjkyNjAxNTRiMjZlMjAyYzdlMGYxMjY0ZjJiNmFhYjg3Njc1ODhlZWE0MzhiZDQzODExNDU4MzcxZSJ9', 'eyJpdiI6ImxLXC9zRTk0cFF2XC9PZ1hscGRZTEhCUT09IiwidmFsdWUiOiJtdkxFZzJCd2licWlUQUwwVTNKUDlRPT0iLCJtYWMiOiI3YWY3ZjBlNTk5MjEyOWMzZTg0OTdlZjAzYWU2OTNmNzQ5YjdhMmYzNWZmNDNmNGI4YzdhNDkyYWRmNmM2YzljIn0=', 1, NULL, '', 1, 0, NULL),
(24, 10, 5, 'Claude Gagnon', '18.31', '19.72', '2019-03-02 12:30:52', 'eyJpdiI6InNZUnZGRG0wZ0FnbTdtcVJoa1Q2R2c9PSIsInZhbHVlIjoiY0NhQkxmXC9sRVZ0c05DU2YrVGZsdENuaGpHWWRVSzU1TFRlVzhsTkMzc2s9IiwibWFjIjoiM2YxNzAzYjIzZTNmZTA0YTk1MWNkM2YyNGE1YjY3YzhhMzQ4Njc5OGE2OWRiMDQ5ZWE3NWFmMTllMWQ2OWRlMyJ9', 'eyJpdiI6ImtkRERXMnF0dU1yTlpcL3pQZCsyT3dRPT0iLCJ2YWx1ZSI6IlZsNDRUWnFySGx0YW5wQTRqNWJNWFNma2dxZ3NcL1JWUlYwdDhISG9QQ2lWT1J1ZjVxY3dMR1MyZGM0K2RGYjVyIiwibWFjIjoiNGJmMGY1YzQ0NjM1ZjRjMGYyYzNiYmE0NTFiNDdkZmI5ODE5NWViZDhmNjk0ZGQ0MDQwYmIwYTc2ZmM3MDM2MSJ9', 'eyJpdiI6Ik4xRHcxZWtcLzN1MkN0XC9UYU1jSld6QT09IiwidmFsdWUiOiJnOVZcL0FJWkJKKzkxKzFhaU9qRHpqdz09IiwibWFjIjoiNTY3YWU3YmZiOWU1NDBiYTZhY2Q0ZDJmYzU3MzVhMTRjOTQ5YmJjM2Q2OGMxNzg0OTA4NGFhNDk4ODQwYzg5OCJ9', 'eyJpdiI6IkZBYllxWjFSUGdwc21GS2N6eDBja0E9PSIsInZhbHVlIjoieDg0ZUN2MElocWZhNFFLVWpYTFp4VjFwT3c0NVZHVk1hekZ6OUs5UHFwUT0iLCJtYWMiOiJhMmFjYjgxNzE5OWU2ZGVkZWZhMjQ5Mzk1MDM1MDU0MGExYzNmMWEyM2ZhNTQ4NzZjMGU5MWFkMTRlOTFjMzUzIn0=', 'eyJpdiI6IlZrbUdyQkc3ZSsrZTY1R0g2Njg2UVE9PSIsInZhbHVlIjoiWm1YR056MjdyWTFCT0ZoMDA3XC93dUE9PSIsIm1hYyI6IjJkOWVjOGQzNjUyODk3MGE2N2MwMDBjMDNlNDU5OGYxNWJjYjk2YzU1YjU1MDdmNDg2MjRkY2JiNWJkNjQ0OWIifQ==', 'eyJpdiI6IlZxQW8ramNsNkN2VWx0R21hSlJ2Smc9PSIsInZhbHVlIjoiMlE5Qys3MlJZRVFFM1FDdURDbW5QQT09IiwibWFjIjoiNTYwOWNjMDE4ZmVhMWE1NjYyZGNmMTQ0Yjk1ZGJmYmU2YjhhMThjNTYwOTQ4NTllYWE4Zjc2ZDcxNTU0NGIyNCJ9', 'eyJpdiI6Ikl4ZFQyN01YdytNek1GME9LYXQ5S2c9PSIsInZhbHVlIjoiaTF3SjNJenNaTUJtUWZCMVNpMzJvQT09IiwibWFjIjoiNjg4MGQ4MjQ0YzYyMmU4MzdhODc1NjY4YzM4Y2JlMzMxOGExMWNhZjUyMWZjYTMxNmFiNmU2Y2E1M2U2NTA4NCJ9', 'eyJpdiI6IkhTXC9cL1l4aHEraFFuVVNRTnZCYzgxUT09IiwidmFsdWUiOiJVaXlIXC8ySFg4SnlDZUhyYVJDXC9PRHVSdk1HNlJpc0llbnlLdWc2VkNVY2s9IiwibWFjIjoiYzg0NDk1MWZiMzZjYmMxZGVkZTA5ZmNkZDA2Yjg1YjUzZGY3ZGRlYjFmY2U2NGEwMTA5MmI0NjY2ODc2NDFkZSJ9', 'eyJpdiI6Ijl1NnJcL2hHUkNmSVwvcG1TdkQ4OVNadz09IiwidmFsdWUiOiJQRTVTWGtMZk8wZ1JSbkJ3NHYwcEhnPT0iLCJtYWMiOiIyNDhjZTY1NzI5ZGMxZDEwY2Q4YmZiZTM4Njk2MjY3OWUwNTJjZmIzMDc2ZGU1YjNmYzY4ZGE4MzMyN2VlMzNhIn0=', 1, NULL, '', 1, 0, NULL),
(25, 11, 5, 'Hugo Lesage', '106.49', '117.06', '2019-03-02 12:32:30', 'eyJpdiI6ImdTaEFaeHdMeDNmTGlMUGV3S25xUXc9PSIsInZhbHVlIjoiQ2Z4eXRGUVozTXRQYkU1MEVJNlEyU2d0Y1VQaStNRTYwWEMySVMzRFVaND0iLCJtYWMiOiIzN2YzZDBiODVhMWU0MDQzNzRlNTVjZGY4MjY1MWRlNTczNjFjZmJlMmY1YjllNGZkNzg2NjhiOTg0NDUyMGQxIn0=', 'eyJpdiI6IlFUK2VRRmZidmZwa2VcL2RYTFhmc0tnPT0iLCJ2YWx1ZSI6IjM5OGtZQ0xxazhNNWF0WDVpS2pqZlFyMCtpdUFLR1hXMEJQcmJmbGVNWFwvRnZpVHcySFFDNFJRZGlDeCswQ20xIiwibWFjIjoiNWNlYzc3N2ZhMTUzOGJkYmYxMDA1YTFlZTMxY2RjNmVkZmNjMjkxNDMwMTNhZTcyZWI0MmZkNzU1NjA5ZDFiMCJ9', 'eyJpdiI6ImUrWENvY1EwWjJ4NlRmdDQxelBPYUE9PSIsInZhbHVlIjoicThZRmQ3ZU5cL1BjMkJ3b002Sm9sTkE9PSIsIm1hYyI6ImYzZGU5M2Y5Zjc0N2FhNDc0ZjhjODYxYjk2MGM0NzI0MTI4OTBiNDU3MjkzZDRhOTFkM2Y5MWQ5M2MzYWI1MTIifQ==', 'eyJpdiI6Ik1FdWpwdmthVEtmYWIxMTFiOXk3anc9PSIsInZhbHVlIjoiOVpIa1wvQ1wvbXA5WWdsTVhkTjNlVVR2YlNYZE5mY2VHQm9iN0JkQW5sclpBPSIsIm1hYyI6IjJkN2I5Njg1MTJlMmYzOTYzNzZkNzdiNTFjZWQ0YmM0OGYzOTg4NDRmMjRjODNiODdmMGMxYzM3NWQ0NGExN2YifQ==', 'eyJpdiI6IjhUbkdUXC9PMFZKcE1FSHdBMnRwMkhRPT0iLCJ2YWx1ZSI6IllLREpNXC9OT0RVMzRWbTZTXC9pQW5cL2c9PSIsIm1hYyI6ImI4ZjZmYzc2YTEzZjNmNTNjZGZmMmY2MmVkZTkyZTljNTcyNjM5MzBmZjAxNWU0NGVjYTAxYmZlOWJkMDg2YzkifQ==', 'eyJpdiI6Im9BY1wvSTUrQ211SVwvdWp2c2ZVZFVZZz09IiwidmFsdWUiOiJCb1EyRGp1N2lWUXNwU3N4V2ZoZUVnPT0iLCJtYWMiOiI3NzZjYTc0NTNkOTAzMzc5YzkyOTk5YTFiOGIxOWE5ZmQzNjkwMzQ1ZWU1ZTQ5NWI5ZDIzMTkwMDQ1M2M1NDI3In0=', 'eyJpdiI6Ild0S2I5R1JxMG53QzBPT0VaQWJDenc9PSIsInZhbHVlIjoibEFVVTBcLyt2aWdrXC9vRXRpV0c4RitRPT0iLCJtYWMiOiIyMzQzMDMwNDg1ODg2NDZlNDdlMGZmOTdmNDg3NDA2NDYxM2Q1NDlhNmJlNGZlZmJiMzllY2Y3NmE0OTAxOTNkIn0=', 'eyJpdiI6IlVmZ0h5ZVdNXC9wWGVaem41ek9SaTl3PT0iLCJ2YWx1ZSI6IlZ1RWlZdUxaNnVVZHJUV2hHYmVXUzVBdnYxOUdcL1YyMGw0OUdPbTBvbHljPSIsIm1hYyI6Ijg2MDdiMzhmNzQzMDM0MmI3ZWI3OTljMTU0OTA2ZTg3ZTllMTg0OGQ4OGI5ZTJmNjI0NTVmZWI5NGVjOTBmOTYifQ==', 'eyJpdiI6InNxaXdYWUhMK1JRbnBTcnRGVFJ6Ymc9PSIsInZhbHVlIjoibjc2UW1JdFo5dlNWaHZuM1NoRzdDZz09IiwibWFjIjoiMmM1MDM4NTRlM2M4YWVkYjExM2E1ZWViYWYxYjBmNmVmMjhlZjQ0YWNlMjM5ZmVhYWNkMTFhNTVkOTRhOGM2MyJ9', 1, NULL, '', 1, 0, NULL);
INSERT INTO `commande` (`rowid`, `rowid_client`, `rowid_periode`, `nom_client`, `sous_total`, `total`, `date_commande`, `email`, `adresse`, `no_app`, `ville`, `province`, `pays`, `code_postal`, `telephone`, `poste`, `notify`, `facture`, `identifiant`, `paye`, `prise`, `signature`) VALUES
(26, 12, 5, '1 1', '574.20', '602.04', '2019-03-06 01:42:08', 'eyJpdiI6IkljZ2VrdGtWcDd3eGJoZmx0Y09aNGc9PSIsInZhbHVlIjoid1J1TUo1NFlmdm11RjNHZ1wvTWRFRGZlUWorUityeU9RRnVZdWhEZCsrSkE9IiwibWFjIjoiZGE0YzE0ZTk2ZDRmMTQzMGVlMzdiYTJiZWU4ZTVlYjAyZGRhMWVkODQ3ZDlkMzUwMDgxZjYzNDI3YjlkMzc1MyJ9', 'eyJpdiI6Imp6RjUzcTN1eHlvY29pZmZZYld4bmc9PSIsInZhbHVlIjoiemM2VWhcL1JiYzN0V2Fvb2UyWjJ0NVE9PSIsIm1hYyI6ImRlZmEwOWEyYjMwMTYxZjJmOWRlMjY4N2Q0ZWM4OWY0Mjg1MDM5YWYxNzJlYmFiZGUyZGNhNjZjODk3Y2Y0MTUifQ==', 'eyJpdiI6Im9zVEphR3drd1RtdHA2dEJJM3hhRHc9PSIsInZhbHVlIjoiNU9xd2M5VlhSOGJPREVtWnJ2QUc2QT09IiwibWFjIjoiMDE5Y2YwNDA3YmM1ZWMyZDQ1YTBlNTcyMWNjNTFlMjVlNmU2YTZkOWI4OWI4ZGI2NGFkOGYyNmViZjNjNTZhNiJ9', 'eyJpdiI6IlhEbmN5QnVRWkhZSk52N1p1Q1FYaVE9PSIsInZhbHVlIjoiTmVqQnpCbWhPZEFTNW50WUUxeWJhQT09IiwibWFjIjoiZTEyNGIxYWE5NjY1N2Q4YzAwNzE2MjYxNzRjYTRmYTYyNGQxZGQ2ODM3MjgzOGY1ZmUxNjU0NzZkMGU2ODBjOSJ9', 'eyJpdiI6ImNTNkFqSFJtSElBTDBkRVRqWStNa1E9PSIsInZhbHVlIjoiK1VteDRSMWtcL0sybmRvNTVIcjhoR0E9PSIsIm1hYyI6ImNiMzk3Njc5NzNmMGE5ZGFiNGRhODFlNDVmNjUwNWRmMDY2ODQ1MDhlM2RiNDJmMTg5ZjcyYzk2NTVmYWE3OWEifQ==', 'eyJpdiI6ImZuRG1LXC9XT2tiT3pNY0NIbXNkWk9BPT0iLCJ2YWx1ZSI6IktRV0FjYjhKQVY5QVdyQ0d1Q3pIN3c9PSIsIm1hYyI6ImViNTgxZDg2YWIyNzJmMzA3NWFlMzgxNDYyMTVlNjJmYzJhYWYzNDUxNDdkZjY5Y2Y3NzU5OThiYTBjYmE0MDQifQ==', 'eyJpdiI6Ik5jRUtwSWZPQUpDbzBQbGROUWdMY3c9PSIsInZhbHVlIjoiM2U1aXk3bGptRUVmeW5uYzdlQjZ0dz09IiwibWFjIjoiOTllZjJhMzUzOTEzY2UwN2M4NmQ0MWMzNzU2YWQ4OTU5YjMxM2RiMGY0YTliMWE3Y2U3NWU0YjBiYTczZTM5NSJ9', 'eyJpdiI6ImlEOHAxMUU3dSsxakFXUDBQQTFpRlE9PSIsInZhbHVlIjoiYUJuaWxpaXV6MHY0bzFFbUN5QmkxMlU4SnJYK2FEdzh1NzlnZE9wR0U2MD0iLCJtYWMiOiI5NDA5ZGZlMjY4ZWU0YWM4NDk0N2FiYmNkMjZiZWI4ZDY0NGUxZmExYTM2N2Y3NmNiNTA0ZDM0MjQ1NTI3Y2Y0In0=', 'eyJpdiI6Ik1MYXdHc21vMU9wQUpXN2FBdFlQZ0E9PSIsInZhbHVlIjoiS0txSE1obUJ2aHFHWnZQZWVOUFVjQT09IiwibWFjIjoiZjk0NDM1YjM4YWEzYjg5NWRiOWQ2NzM2OGY0ODc2N2QxNWE2MGIyODkzY2M1OTkyYmExZmJmMTI0NmFjN2FkMCJ9', 1, NULL, '7687F65A-4E5D-45BA-BDD4-926D2520D71F', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `commande_article`
--

CREATE TABLE `commande_article` (
  `rowid` int(11) NOT NULL,
  `commande_rowid` int(11) NOT NULL,
  `article_rowid` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format_quantite` decimal(10,2) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provenance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allergene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perissable` tinyint(1) DEFAULT NULL,
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `quebec` tinyint(4) NOT NULL DEFAULT '0',
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_categorie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantite_commande` int(11) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `frais_fixe` decimal(10,2) NOT NULL DEFAULT '1.00',
  `frais_variable` decimal(10,2) NOT NULL DEFAULT '0.00',
  `prix_majore` decimal(10,2) DEFAULT NULL,
  `economie` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commande_article`
--

INSERT INTO `commande_article` (`rowid`, `commande_rowid`, `article_rowid`, `image`, `nom`, `format_quantite`, `format`, `provenance`, `allergene`, `perissable`, `bio`, `quebec`, `nom_marque`, `nom_categorie`, `nom_fournisseur`, `quantite_commande`, `prix`, `frais_fixe`, `frais_variable`, `prix_majore`, `economie`) VALUES
(24, 20, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 2, '19.92', '6.00', '1.00', NULL, NULL),
(25, 20, 5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 3, '14.88', '5.00', '0.00', NULL, NULL),
(26, 20, 8, 'public/products/images/yepAc3CrDZsNuFSxHG4h1hFbgYuqwsq7lI72nlSV.jpeg', 'Levure alimentaire en flocons', '500.00', 'g', 'États-Unis', NULL, 0, 0, 0, 'Ayam', 'Condiments et autres', 'Farine Quebec', 1, '13.89', '5.00', '1.00', NULL, NULL),
(27, 21, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 3, '19.92', '6.00', '1.00', NULL, NULL),
(28, 22, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 4, '4.95', '5.00', '0.00', NULL, NULL),
(29, 23, 2, 'public/products/images/aKxliPxIIy0PREv07R4UHxQsM6vQ5wgNI8O4V26t.jpeg', 'Couscous de blé entier tamisé', '1.00', 'Cuillère(s) à table', 'Canada', NULL, 0, 1, 0, 'Gerble', 'Aliments céréaliers', 'Aliments en vrac Quebec', 5, '4.91', '5.00', '0.00', NULL, NULL),
(30, 24, 3, 'public/products/images/N0AvyopsFGqYKEbFV5t1Pso4YSV20M190CNfcO40.jpeg', 'Flocons d’avoine', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Carrefour', 'Aliments céréaliers', 'Noix Quebec', 1, '3.43', '5.00', '0.50', NULL, NULL),
(31, 24, 5, 'public/products/images/2XECO90TRmMTtJib5OcEZNwfmmgnVM1QDaU5ibrZ.jpeg', 'Cacao en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Nestle', 'Cacao et ses acolytes', 'Chocolat Quebec', 1, '14.88', '5.00', '0.00', NULL, NULL),
(32, 25, 4, 'public/products/images/qI9OPNWxnYWZXfQQ4N3rm0fhUiQIDu0w6C0BLk9P.jpeg', 'Cacao cru en poudre', '1.00', 'kg', 'Pérou', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 4, '19.92', '6.00', '1.00', NULL, NULL),
(33, 25, 6, 'public/products/images/fxtpyETXUMXk2OswDJ3T1SparCXp9c4caKKNOa8J.jpeg', 'Capuchons de chocolat 70%', '500.00', 'g', 'Fèves du Pérou, chocolat produit en Europe', NULL, 0, 1, 0, 'Fleury-michon', 'Cacao et ses acolytes', 'Chocolat Quebec', 1, '23.93', '7.00', '0.00', NULL, NULL),
(34, 25, 11, 'public/products/images/uwhju93QU1tzQNJO5xXD9iu1JDSzw7w00tRcRFEe.jpeg', 'Farine blanche tout-usage non blanchie', '1.00', 'kg', 'Québec', NULL, 0, 1, 1, 'Sacla', 'Farines', 'Farine Quebec', 1, '2.88', '5.00', '0.00', NULL, NULL),
(35, 26, 1, 'public/products/images/XA26OCfth516NxCLed2cPCeTjXlueWWVOyI0mIit.jpeg', 'Amarante', '1.00', 'kg', 'Inde', NULL, 0, 1, 0, 'Ayam', 'Aliments céréaliers', 'Aliments en vrac Quebec', 116, '4.95', '5.00', '0.00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `rowid` int(11) NOT NULL,
  `rowid_theme` int(11) NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reponse` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`rowid`, `rowid_theme`, `question`, `reponse`) VALUES
(4, 1, 'Qu\'est-ce qu\'un regroupement d\'achat?', 'Un groupement d’achat est un regroupement d’entreprise, matérialisé ou non par la création d’un organisme dédié, qui a pour vocation d’optimiser les achats communs réalisés par ses membres.'),
(5, 1, 'Quand pourrais-je récupérer ma commande?', 'Des périodes de cueillette seront émises dès que la période de commande sera terminée. Les cueillettes seront affichés dans l\'onglet calendrier.'),
(6, 1, 'Êtes-vous un organisme à but lucratif?', 'Oui, aucun profit n\'est réalisé.');

-- --------------------------------------------------------

--
-- Table structure for table `fournisseur`
--

CREATE TABLE `fournisseur` (
  `nom_fournisseur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fournisseur`
--

INSERT INTO `fournisseur` (`nom_fournisseur`, `actif`) VALUES
('Aliments en vrac Quebec', 1),
('Chocolat Quebec', 1),
('Confitures et gelées Quebec', 1),
('Croustilles Quebec', 1),
('Détaillants de boissons Quebec', 1),
('Distributeurs et fabricants de mets chinois Quebec', 1),
('Dole Canada', 1),
('Eau embouteillée et en vrac Quebec', 1),
('Farine Quebec', 1),
('Fruits du Québec', 1),
('Fruits secs Quebec', 1),
('Glace Quebec', 1),
('Grossistes en aliments congelés Quebec', 1),
('Grossistes en café Quebec', 1),
('Grossistes et fabricants de produits naturels Quebec', 1),
('Huile d\'olive Quebec', 1),
('Huiles végétales Quebec', 1),
('Levure Quebec', 1),
('Maïs soufflé Quebec', 1),
('Malt et houblon Quebec', 1),
('Miel Quebec', 1),
('Noix Quebec', 1),
('Oeufs Quebec', 1),
('Produits alimentaires Quebec', 1),
('Produits biologiques Quebec', 1);

-- --------------------------------------------------------

--
-- Table structure for table `marque`
--

CREATE TABLE `marque` (
  `nom_marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marque`
--

INSERT INTO `marque` (`nom_marque`, `actif`) VALUES
('Aucune', 1),
('Ayam', 1),
('Babybio', 1),
('Bledina', 1),
('Bret-s', 1),
('Carrefour', 1),
('Citadelle', 1),
('Danone', 1),
('Dole', 1),
('Fleury-michon', 1),
('Gerble', 1),
('Herta', 1),
('Jardin-bio', 1),
('L-angelys', 1),
('Le-bon-paris', 1),
('Lea-nature', 1),
('Marque-repere', 1),
('Materne', 1),
('Monique-ranou', 1),
('Nestle', 1),
('Prana', 1),
('Propiedad-de', 1),
('Sacla', 1),
('Schar', 1),
('Sojasun', 1),
('Yoplait', 1);

-- --------------------------------------------------------

--
-- Table structure for table `panier_article`
--

CREATE TABLE `panier_article` (
  `rowid` int(11) NOT NULL,
  `rowid_panier` int(11) NOT NULL,
  `rowid_article` int(11) NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `panier_article`
--

INSERT INTO `panier_article` (`rowid`, `rowid_panier`, `rowid_article`, `quantite`) VALUES
(25, 12, 1, 116),
(26, 12, 10, 20),
(27, 12, 24, 15);

-- --------------------------------------------------------

--
-- Table structure for table `panier_client`
--

CREATE TABLE `panier_client` (
  `rowid` int(11) NOT NULL,
  `rowid_client` int(11) NOT NULL,
  `actuel` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `panier_client`
--

INSERT INTO `panier_client` (`rowid`, `rowid_client`, `actuel`) VALUES
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `periode_article`
--

CREATE TABLE `periode_article` (
  `rowid` int(11) NOT NULL,
  `article_rowid` int(11) NOT NULL,
  `periode_rowid` int(11) NOT NULL,
  `quantite_commande` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_article`
--

INSERT INTO `periode_article` (`rowid`, `article_rowid`, `periode_rowid`, `quantite_commande`) VALUES
(15, 4, 1, 2),
(16, 5, 1, 3),
(17, 8, 1, 1),
(18, 4, 1, 3),
(19, 1, 5, 4),
(20, 2, 5, 5),
(21, 3, 5, 1),
(22, 5, 5, 1),
(23, 4, 5, 4),
(24, 6, 5, 1),
(25, 11, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `periode_commande`
--

CREATE TABLE `periode_commande` (
  `rowid` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_commande`
--

INSERT INTO `periode_commande` (`rowid`, `date_debut`, `date_fin`) VALUES
(1, '2019-02-01 00:00:00', '2019-02-19 12:00:00'),
(5, '2019-03-01 12:00:00', '2019-03-20 12:00:00'),
(7, '2019-04-01 00:00:00', '2019-04-12 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `periode_plage`
--

CREATE TABLE `periode_plage` (
  `rowid` int(11) NOT NULL,
  `rowid_periode` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `emplacement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `periode_plage`
--

INSERT INTO `periode_plage` (`rowid`, `rowid_periode`, `date_debut`, `date_fin`, `emplacement`) VALUES
(8, 1, '2019-03-04 12:00:00', '2019-03-04 15:00:00', 'Pavillon des sciences');

-- --------------------------------------------------------

--
-- Table structure for table `personnaliser`
--

CREATE TABLE `personnaliser` (
  `rowid` int(11) NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banniere` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_desc` varchar(1028) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_entreprise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_code_postal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_copyright` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1_msg` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2_msg` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3_msg` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car1_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car2_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car3_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_theme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Nouveau Thème',
  `nom_article` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_article_plur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titre_onglet` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'RegroupÉcole',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_html` longtext COLLATE utf8mb4_unicode_ci,
  `termsofuse` longtext COLLATE utf8mb4_unicode_ci,
  `affichage_decimal` char(1) COLLATE utf8mb4_unicode_ci DEFAULT ',',
  `affichage_separator` char(1) COLLATE utf8mb4_unicode_ci DEFAULT '_',
  `actif` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personnaliser`
--

INSERT INTO `personnaliser` (`rowid`, `logo`, `banniere`, `footer_desc`, `footer_entreprise`, `footer_address`, `footer_city`, `footer_code_postal`, `footer_telephone`, `footer_copyright`, `car1`, `car2`, `car3`, `car1_msg`, `car2_msg`, `car3_msg`, `car1_link`, `car2_link`, `car3_link`, `nom_theme`, `nom_article`, `nom_article_plur`, `titre_onglet`, `icon`, `about_html`, `termsofuse`, `affichage_decimal`, `affichage_separator`, `actif`) VALUES
(1, 'public/themes/logos/oi1ctvvQkW7fhBQXCBoR4g1yBddTIPEkCqKNMmUW.png', 'public/themes/bannieres/iaBcDoXy7Tyz4LV9SMascATAAB1a0psO3sjyWndP.jpeg', 'Regroupement d\'achat offert par le département de Diététique du Cégep de Trois-Rivières.', 'Cégep de Trois-Rivières', '3500, rue de Courval', 'Trois-Rivières, QC', 'G9A 5E6', '8193761721', 'Charles-William Morency - Jessy Walker-Mailly - Zachary Veillette', 'public/themes/carrousel/7ygMabRFrJIpWSdU2zLOpZR7aSBtVwIptvAdMoql.jpeg', NULL, NULL, 'Aller voir nos produit(s)!', NULL, NULL, 'boutique', '#', '#', 'Principal', 'produit', 'produit(s)', 'RegroupÉcole', 'public/themes/icons/VFL9fx4VgqBZNZrPuYcWbCMU4kaTguSdWFAGRyr9.ico', '<h1><img src=\"http://conceptionweb-5.expertiseweb.ca/storage/hosted_images/m8x6vHjdDTeczhctPdApDGC8mPWDhuDIFRUxtbUf.png\" style=\"width:340px;\" alt=\"m8x6vHjdDTeczhctPdApDGC8mPWDhuDIFRUxtbUf.png\"></h1>\n\n\n\n<p class=\"MsoNormal\"><span style=\"font-family:Tahoma;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>\n\n<ul><li><a href=\"https://guide-alimentaire.canada.ca/fr/\" style=\"color:rgb(74,123,140);\">Guide alimentaire</a></li><li><span style=\"font-size:10.5pt;font-family:Tahoma;\"> dolor sit </span></li><li><span style=\"font-size:10.5pt;font-family:Tahoma;\">exercitation ullamc</span></li></ul>\n\n<p><img src=\"https://images.ecosia.org/1nONYrYMf0bwDh73RN02Z2dZacQ=/0x390/smart/http%3A%2F%2Fwww.lechodetroisrivieres.ca%2Fupload%2F12%2Fevenements%2F2018%2F7%2F342125%2Fle-cegep-de-trois-rivieres-celebre-ses-50-ans-001.jpg\" style=\"width:351.831px;float:left;\" class=\"note-float-left\" alt=\"http%3A%2F%2Fwww.lechodetroisrivieres.ca%2Fupload%2F12%2Fevenements%2F2018%2F7%2F342125%2Fle-cegep-de-trois-rivieres-celebre-ses-50-ans-001.jpg\"></p>\n\n<h2><span style=\"font-family:Tahoma;\">Cégep de Trois-Rivières</span></h2>\n\n<p><span style=\"font-family:Tahoma;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. </span><span style=\"font-family:Tahoma;font-size:1rem;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. </span><span style=\"font-family:Tahoma;font-size:1rem;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</span><br></p>\n\n\n\n\n\n<p><span style=\"font-family:Tahoma;\"> </span></p>\n\n<p><span style=\"font-family:Tahoma;\"><br></span></p>\n\n<p><span style=\"font-family:Tahoma;\"><br></span></p>', '<h2>Politique modèle de confidentialité.</h2>\n\n<h3>Introduction</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\"> Devant le développement des nouveaux outils de communication, il est nécessaire de porter une attention particulière à la protection de la vie privée. C’est pourquoi, nous nous engageons à respecter la confidentialité des renseignements personnels que nous collectons.</p>\n\n<h3>Collecte des renseignements personnels</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous collectons les renseignements suivants (lors du dépôt d’une plainte) :</p>\n\n<ul><li>Nom</li><li>Prénom</li><li>Adresse postale</li><li>Code postal</li><li>Adresse électronique</li><li>Numéro de téléphone / télécopieur</li><li>Genre / Sexe</li></ul>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Les renseignements personnels que nous collectons sont recueillis au travers de formulaires. Nous utilisons également, comme indiqué dans la section suivante, des fichiers témoins et/ou journaux pour réunir des informations vous concernant.</p>\n\n<h3>Formulaires et interactivité:</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Vos renseignements personnels sont collectés par le biais de formulaire, à savoir :</p>\n\n<ul><li>Formulaire de dépôt de plainte</li></ul>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous utilisons les renseignements ainsi collectés pour les finalités suivantes :</p>\n\n<ul><li>Suivi de la plainte</li><li>Informations</li><li>Statistiques</li></ul>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous utilisons les renseignements ainsi collectés pour les finalités suivantes :</p>\n\n<ul><li>Forum ou aire de discussion</li><li>Commentaires</li><li>Correspondance</li><li>Informations</li></ul>\n\n<h3>Fichiers journaux et témoins</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous recueillons certaines informations par le biais de fichiers journaux (log file) et de fichiers témoins (cookies). Il s’agit principalement des informations suivantes :</p>\n\n<ul><li>Adresse IP</li><li>Système d’exploitation</li><li>Pages visitées et requêtes</li><li>Heure et jour de connexion</li><li>Lieu de connexion</li></ul>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Ces fichiers sont utilisés pour :</p>\n\n<ul><li>Amélioration du service</li><li>Statistique</li></ul>\n\n<h3>Droit d’opposition et de retrait</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous nous engageons à vous offrir un droit d’opposition et de retrait quant à vos renseignements personnels.  Le droit d’opposition s’entend comme étant la possiblité offerte aux internautes de refuser que leurs renseignements personnels soient utilisées à certaines fins mentionnées lors de la collecte.</p>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Le droit de retrait s’entend comme étant la possiblité offerte aux internautes de demander à ce que leurs renseignements personnels ne figurent plus, par exemple, dans une liste de diffusion.</p>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Pour pouvoir exercer ces droits, vous pouvez contacter :</p>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Geneviève Fortin<br>1000, rue Fullum (Bureau A.208)<br>Montréal (Québec)<br>H2K 3L7 <br>Courriel : <a href=\"mailto:genevieve.fortin@conseildepresse.qc.ca\" style=\"color:rgb(149,31,43);\">genevieve.fortin@conseildepresse.qc.ca</a><br>Téléphone : 514 529-2818 (poste 3)</p>\n\n<h3>Droit d’accès</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous nous engageons à reconnaître un droit d’accès et de rectification aux personnes concernées désireuses de consulter ou modifier les informations les concernant.  L’exercice de ce droit se fera par téléphone, par courriel ou par la poste auprès de : </p>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Geneviève Fortin<br>1000, rue Fullum (Bureau A.208)<br>Montréal (Québec)<br>H2K 3L7 <br>Courriel : <a href=\"mailto:genevieve.fortin@conseildepresse.qc.ca\" style=\"color:rgb(149,31,43);\">genevieve.fortin@conseildepresse.qc.ca</a><br>Téléphone : 514 529-2818 (poste 3)</p>\n\n<h3>Sécurité</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Les renseignements personnels que nous collectons sont conservés dans un environnement sécurisé. Les personnes travaillant pour nous sont tenues de respecter la confidentialité de vos informations.  Pour assurer la sécurité de vos renseignements personnels, nous avons recours aux mesures suivantes :</p>\n\n<ul><li>Gestion des accès – personne autorisée</li><li>Gestion des accès – personne concernée</li><li>Sauvegarde informatique</li><li>Identifiant / mot de passe</li><li>Pare-feu (Firewalls)</li></ul>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous nous engageons à maintenir un haut degré de confidentialité en intégrant les dernières innovations technologiques permettant d’assurer la confidentialité de vos renseignements. Toutefois, comme aucun mécanisme n’offre une sécurité maximale, une part de risque est toujours présente lorsque l’on utilise Internet pour transmettre des renseignements personnels.</p>\n\n<h3>Législation</h3>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\">Nous nous engageons à respecter les dispositions législatives énoncées dans :  L.R.Q., chapitre P-39.1</p>\n\n<p style=\"font-size:14px;color:rgb(46,46,46);font-family:arial, verdana, sans-serif;\"><img src=\"http://conceptionweb-5.expertiseweb.ca/storage/hosted_images/SUd2bfYQorG6xg1XBvJ76CT1aDVwchZ5uHc639Xy.jpeg\" style=\"width:100%;\" alt=\"SUd2bfYQorG6xg1XBvJ76CT1aDVwchZ5uHc639Xy.jpeg\"><br></p>', ',', '_', 1),
(17, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '#', '#', '#', 'Aucun thème', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `recette`
--

CREATE TABLE `recette` (
  `rowid` int(11) NOT NULL,
  `article_rowid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recette`
--

INSERT INTO `recette` (`rowid`, `article_rowid`, `name`, `link`) VALUES
(17, 1, 'Amarante en taboulé', 'http://chefsimon.com/gourmets/cuisinealcaline/recettes/amarante-en-taboule'),
(18, 1, 'Marrant petit déjeuner d\'amarante!', 'http://brutalimentation.ca/2014/03/12/marrant-petit-dejeuner-damarante/'),
(19, 24, 'Gâteau froid aux ananas', 'https://www.recettes.qc.ca/recettes/recette/gateau-froid-aux-ananas-3864'),
(21, 25, 'Meilleur recette de pomme', 'http://www.canalvie.com/recettes/idees-repas/articles-idees-repas/recettes-desserts-pommes-1.1243075');

-- --------------------------------------------------------

--
-- Table structure for table `unite_mesure`
--

CREATE TABLE `unite_mesure` (
  `nom_unite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `unite_mesure`
--

INSERT INTO `unite_mesure` (`nom_unite`, `actif`) VALUES
('Cuillère(s) à table', 1),
('Cuillère(s) à thé', 1),
('g', 1),
('galon(s)', 1),
('Kg', 1),
('L', 1),
('lb', 1),
('mg', 1),
('ml', 1),
('oz', 1),
('pinte(s)', 1),
('tasse(s)', 1),
('test', 0),
('Unité(s)', 1);

-- --------------------------------------------------------
--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `admin_board`
--
ALTER TABLE `admin_board`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `rowid_admin` (`username_admin`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `article_fk0` (`nom_marque`),
  ADD KEY `article_fk1` (`nom_categorie`),
  ADD KEY `article_fk2` (`nom_fournisseur`),
  ADD KEY `article_unite` (`format`);

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`nom_categorie`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`rowid`);

--
-- Indexes for table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `fk_periode_commande` (`rowid_periode`),
  ADD KEY `fk_client_commande` (`rowid_client`);

--
-- Indexes for table `commande_article`
--
ALTER TABLE `commande_article`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `fk_commande_article` (`commande_rowid`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `fk_theme` (`rowid_theme`);

--
-- Indexes for table `fournisseur`
--
ALTER TABLE `fournisseur`
  ADD PRIMARY KEY (`nom_fournisseur`);

--
-- Indexes for table `marque`
--
ALTER TABLE `marque`
  ADD PRIMARY KEY (`nom_marque`);

--
-- Indexes for table `panier_article`
--
ALTER TABLE `panier_article`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `panier_article_fk0` (`rowid_panier`),
  ADD KEY `panier_article_fk1` (`rowid_article`);

--
-- Indexes for table `panier_client`
--
ALTER TABLE `panier_client`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `panier_client_fk1` (`rowid_client`);

--
-- Indexes for table `periode_article`
--
ALTER TABLE `periode_article`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `article_rowid` (`article_rowid`),
  ADD KEY `periode_rowid` (`periode_rowid`);

--
-- Indexes for table `periode_commande`
--
ALTER TABLE `periode_commande`
  ADD PRIMARY KEY (`rowid`);

--
-- Indexes for table `periode_plage`
--
ALTER TABLE `periode_plage`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `periode_plage_fk0` (`rowid_periode`);

--
-- Indexes for table `personnaliser`
--
ALTER TABLE `personnaliser`
  ADD PRIMARY KEY (`rowid`);

--
-- Indexes for table `recette`
--
ALTER TABLE `recette`
  ADD PRIMARY KEY (`rowid`),
  ADD KEY `article_rowid` (`article_rowid`);

--
-- Indexes for table `unite_mesure`
--
ALTER TABLE `unite_mesure`
  ADD PRIMARY KEY (`nom_unite`),
  ADD KEY `nom_unite` (`nom_unite`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_board`
--
ALTER TABLE `admin_board`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=262;

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `commande`
--
ALTER TABLE `commande`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `commande_article`
--
ALTER TABLE `commande_article`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `panier_article`
--
ALTER TABLE `panier_article`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `panier_client`
--
ALTER TABLE `panier_client`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `periode_article`
--
ALTER TABLE `periode_article`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `periode_commande`
--
ALTER TABLE `periode_commande`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `periode_plage`
--
ALTER TABLE `periode_plage`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `personnaliser`
--
ALTER TABLE `personnaliser`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `recette`
--
ALTER TABLE `recette`
  MODIFY `rowid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_board`
--
ALTER TABLE `admin_board`
  ADD CONSTRAINT `fk_admin` FOREIGN KEY (`username_admin`) REFERENCES `admin` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_fk0` FOREIGN KEY (`nom_marque`) REFERENCES `marque` (`nom_marque`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk1` FOREIGN KEY (`nom_categorie`) REFERENCES `categorie` (`nom_categorie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fk2` FOREIGN KEY (`nom_fournisseur`) REFERENCES `fournisseur` (`nom_fournisseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_unite` FOREIGN KEY (`format`) REFERENCES `unite_mesure` (`nom_unite`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_client_commande` FOREIGN KEY (`rowid_client`) REFERENCES `client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_commande` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commande_article`
--
ALTER TABLE `commande_article`
  ADD CONSTRAINT `fk_commande_article` FOREIGN KEY (`commande_rowid`) REFERENCES `commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `faq`
--
ALTER TABLE `faq`
  ADD CONSTRAINT `fk_theme` FOREIGN KEY (`rowid_theme`) REFERENCES `personnaliser` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `panier_article`
--
ALTER TABLE `panier_article`
  ADD CONSTRAINT `fk_article_panier` FOREIGN KEY (`rowid_article`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_panier_client` FOREIGN KEY (`rowid_panier`) REFERENCES `panier_client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `panier_client`
--
ALTER TABLE `panier_client`
  ADD CONSTRAINT `fk_client` FOREIGN KEY (`rowid_client`) REFERENCES `client` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_article`
--
ALTER TABLE `periode_article`
  ADD CONSTRAINT `fk_article_periode` FOREIGN KEY (`article_rowid`) REFERENCES `article` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_periode_article` FOREIGN KEY (`periode_rowid`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periode_plage`
--
ALTER TABLE `periode_plage`
  ADD CONSTRAINT `fk_periode` FOREIGN KEY (`rowid_periode`) REFERENCES `periode_commande` (`rowid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
